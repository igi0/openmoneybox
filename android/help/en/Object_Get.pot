# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2018-05-27 14:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Object_Get.xml:1
#, no-c-format
msgid "Store an object reception"
msgstr ""

#. Tag: para
#: Object_Get.xml:2
#, no-c-format
msgid ""
"Tap the button <guiicon>Received object</guiicon> in the Dashboard tab to "
"store the reception of an object."
msgstr ""

#. Tag: para
#: Object_Get.xml:3
#, no-c-format
msgid "Type in the first edit-box the received object."
msgstr ""

#. Tag: para
#: Object_Get.xml:4
#, no-c-format
msgid "Type in the second edit-box the donor of the object."
msgstr ""

#. Tag: sect1
#: Object_Get.xml:4
#, no-c-format
msgid "&browsecontact;"
msgstr ""

#. Tag: para
#: Object_Get.xml:6
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""
