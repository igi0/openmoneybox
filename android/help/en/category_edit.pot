# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2016-11-01 11:57+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: category_edit.xml:1
#, no-c-format
msgid "Edit the categories"
msgstr ""

#. Tag: para
#: category_edit.xml:2
#, no-c-format
msgid ""
"Tap the options menu item <guimenuitem>Edit categories</guimenuitem> to add "
"or remove categories."
msgstr ""

#. Tag: para
#: category_edit.xml:3
#, no-c-format
msgid ""
"Categories can be added to profits and expenses so they can be better "
"organized."
msgstr ""
