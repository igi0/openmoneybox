# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2018-05-27 14:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Object_Given.xml:1
#, no-c-format
msgid "Store an object donation"
msgstr ""

#. Tag: para
#: Object_Given.xml:2
#, no-c-format
msgid ""
"Tap the button <guiicon>Given object</guiicon> in the Dashboard tap to store "
"an object donation."
msgstr ""

#. Tag: para
#: Object_Given.xml:3
#, no-c-format
msgid "Type in the first edit-box the donated object."
msgstr ""

#. Tag: para
#: Object_Given.xml:4
#, no-c-format
msgid "Type in the second edito-box the object receiver."
msgstr ""

#. Tag: sect1
#: Object_Given.xml:4
#, no-c-format
msgid "&browsecontact;"
msgstr ""

#. Tag: para
#: Object_Given.xml:6
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""
