# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Igor Calì <igor.cali0@gmail.com>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-08-31 13:34+0000\n"
"PO-Revision-Date: 2014-08-31 16:33+0200\n"
"Last-Translator: Igor Calì <igor.cali0@gmail.com>\n"
"Language-Team: Italiano <>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Gtranslator 2.91.6\n"

#. Tag: title
#: Object_Get.xml:1
#, no-c-format
msgid "Store an object reception"
msgstr "Memorizzare la ricezione di un oggetto"

#. Tag: para
#: Object_Get.xml:2
#, no-c-format
msgid "Tap the button <guiicon>Received object</guiicon> in the Dashboard tab to store the reception of an object."
msgstr "Toccare il pulsante <guiicon>Ricevuto oggetto</guiicon> per memorizzare la ricezione di un oggetto."

#. Tag: para
#: Object_Get.xml:7
#, no-c-format
msgid "Type in the first edit-box the received object."
msgstr "Inserire nella prima casella di testo l'oggetto ricevuto."

#. Tag: para
#: Object_Get.xml:8
#, no-c-format
msgid "Type in the second edit-box the donor of the object."
msgstr "Inserire nella seconda casella di testo il donatore dell'oggetto."

#. Tag: sect1
#: Object_Get.xml:8
#, no-c-format
msgid "&systime;"
msgstr ""

#. Tag: para
#: Object_Get.xml:10
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""
"In caso di inserimenti mancanti od errati verrà generato un messaggio di "
"errore."

#. Tag: para
#: Object_Get.xml:11
#, no-c-format
msgid ""
"<link linkend=\"scuts\">Shortcut</link>: <keycombo action=\"simul\"> "
"<keycap>SHIFT</keycap> <keycap>F1</keycap> </keycombo>"
msgstr ""
"<link linkend=\"scuts\">Tasto di scelta rapida</link>: <keycombo "
"action=\"simul\"> <keycap>SHIFT</keycap>  <keycap>F1</keycap></keycombo>"
