# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-09-09 16:06+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Options.xml:1
#, no-c-format
msgid "Program customization"
msgstr ""

#. Tag: para
#: Options.xml:2
#, no-c-format
msgid ""
"The program can be customised through the menu item <guimenuitem>Options</"
"guimenuitem>."
msgstr ""

#. Tag: para
#: Options.xml:3
#, no-c-format
msgid ""
"The dialog window will be shown, where it is possible to choose settings."
msgstr ""

#. Tag: title
#: Options.xml:8
#, no-c-format
msgid "'OpenMoneyBox' section"
msgstr ""

#. Tag: listitem
#: Options.xml:11
#, no-c-format
msgid ""
"<emphasis>Default document</emphasis>: type the full path of default "
"document;"
msgstr ""

#. Tag: listitem
#: Options.xml:17
#, no-c-format
msgid ""
"<emphasis>Export monthly</emphasis>: check this box if you want the program "
"to convert documents to XML on month change;"
msgstr ""

#. Tag: listitem
#: Options.xml:20
#, no-c-format
msgid ""
"<emphasis>Document prefix</emphasis>: type the prefix you want to use for "
"your documents, when generating xml's or backups;"
msgstr ""

#. Tag: title
#: Options.xml:27
#, no-c-format
msgid "'Behaviour' section"
msgstr ""

#. Tag: listitem
#: Options.xml:30
#, no-c-format
msgid ""
"<emphasis>Save positions</emphasis>: check this box to save known positions "
"for operations."
msgstr ""

#. Tag: listitem
#: Options.xml:33
#, no-c-format
msgid ""
"<emphasis>Group funds</emphasis>: Fund groups are shown in simplified "
"summary when this setting is enabled and any group is defined."
msgstr ""

#. Tag: title
#: Options.xml:40
#, no-c-format
msgid "'Appearance' section"
msgstr ""

#. Tag: listitem
#: Options.xml:43
#, no-c-format
msgid ""
"<emphasis>Night mode</emphasis>: check this box to apply a dark look to the "
"app."
msgstr ""

#. Tag: title
#: Options.xml:50
#, no-c-format
msgid "'Security' section"
msgstr ""

#. Tag: listitem
#: Options.xml:53
#, no-c-format
msgid ""
"<emphasis>Security code</emphasis>: check this box for personal security "
"access when the app is started."
msgstr ""

msgid "'Database' section"
msgstr ""

msgid "<emphasis>SqlCipher compatibility version</emphasis>: allows to select the new SqlCipher defaults v4."
msgstr ""

msgid ""
"A database migration will be attempted at next application startup if 'v4' "
"is selected. See <ulink url=\"https://igisw-bilancio.sourceforge.net/wiki/"
"CipherCompat.pdf\">this page</ulink> for the compatibility on different "
"operating systems."
msgstr ""


