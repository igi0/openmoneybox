# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-09-01 11:56+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: custom_currency.xml:1
#, no-c-format
msgid "Default currency"
msgstr ""

#. Tag: para
#: custom_currency.xml:2
#, no-c-format
msgid "Uncheck this box to use a custom currency for the operation."
msgstr ""

#. Tag: para
#: custom_currency.xml:3
#, no-c-format
msgid "Insert the currency symbol in the first edit box."
msgstr ""

#. Tag: para
#: custom_currency.xml:4
#, no-c-format
msgid "Insert the change rate in the second edit box."
msgstr ""
