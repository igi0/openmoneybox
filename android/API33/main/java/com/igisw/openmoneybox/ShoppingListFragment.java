/* **************************************************************
 * Name:
 * Purpose:   Shopping List fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-08-19
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShoppingListFragment extends Fragment {
    public int shop_pos;
    public final List<ShoppingList_Wrapper> ShoppingList = new ArrayList<>();
    public RecyclerView sv;    // sv: ShoppingList view

    public ShoppingListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity activity = (MainActivity) requireActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shoplist, container, false);

        // ShoppingList tab
        int thisYear, alarmYear;
        thisYear = Calendar.getInstance().get(Calendar.YEAR);
        GregorianCalendar alarmCalendar;
        String str_tmp;

        // Create the OnClickListener
        FloatingActionButton newItemButton = view.findViewById(R.id.shopActionButton);
        View.OnClickListener clickListener = activity::addShopItemClick;
        newItemButton.setOnClickListener(clickListener);

        sv = view.findViewById(R.id.sv);
        LinearLayoutManager llm1 = new LinearLayoutManager(Objects.requireNonNull(activity).getApplicationContext());
        sv.setLayoutManager(llm1);
        Recycler_Adapter_ShoppingList ShoppingList_Adapter = new Recycler_Adapter_ShoppingList(ShoppingList);
        ShoppingList_Adapter.frame = activity;
        sv.setAdapter(ShoppingList_Adapter);
        ShoppingList_Adapter.llManager = llm1;

        ShoppingList.clear();
        for(int i = 0; i < activity.Data.NSho; i++) {
            alarmCalendar = activity.Data.ShopItems.get(i).Alarm;
            alarmYear = alarmCalendar.get(Calendar.YEAR);
            if((alarmYear - thisYear) < 80) str_tmp = omb_library.omb_DateToStr(alarmCalendar);
            else str_tmp = getResources().getString(R.string.alarm_none);

            ShoppingList.add(new ShoppingList_Wrapper(/*activity.Data.ShopItems.get(i).Id,*/
                    activity.Data.ShopItems.get(i).Name,
                    str_tmp));
        }
        ShoppingList_Adapter.notifyDataSetChanged();

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

}
