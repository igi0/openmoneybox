/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-11-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Recycler_Adapter_CreditDebt extends RecyclerView.Adapter<Recycler_Adapter_CreditDebt.ViewHolder>{

    public MainActivity frame;
    private final List<CreditDebt_wrapper> items;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ViewHolder(View v) {
            super(v);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //int item = getAdapterPosition();
            int item = getAbsoluteAdapterPosition();
            frame.updateCredDebRecyclerItem(item);
        }

    }

    public class ObjectViewHolder extends ViewHolder{

        final CardView cv;
        final TextView itemName, itemValue;
        final RadioButton itemChecked;
        final ImageView itemBadge;

        ObjectViewHolder(View itemView) {
            super(itemView);

            LinearLayout ll = itemView.findViewById(R.id.ll_cv_credit);
            ConstraintLayout rl = itemView.findViewById(R.id.rl_cv_credit );
            RelativeLayout rl2 = itemView.findViewById(R.id.rl_cv_credit_2 );
            this.cv = itemView.findViewById(R.id.cv);
            this.itemName = itemView.findViewById(R.id.object_name);
            this.itemValue = itemView.findViewById(R.id.object_contact);
            this.itemChecked = itemView.findViewById(R.id.object_checked);
            this.itemBadge = itemView.findViewById(R.id.quickContactBadge);

            if(frame.Opts.getBoolean("GDarkTheme", false))
            {
                ll.setBackgroundColor(0xFF000000);
                cv.setCardBackgroundColor(0xFF333333);
                rl.setBackgroundColor(0xFF333333);
                rl2.setBackgroundColor(0xFF333333);
                itemName.setBackgroundColor(0xFF333333);
                itemValue.setBackgroundColor(0xFF333333);
                itemBadge.setBackgroundColor(0xFF333333);
            }

        }

    }

    public Recycler_Adapter_CreditDebt(List<CreditDebt_wrapper> items){
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public @NonNull ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_creddeb, viewGroup, false);
        return new ObjectViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ObjectViewHolder holder = (ObjectViewHolder) viewHolder;
        holder.itemName.setText(items.get(i).name);
        holder.itemValue.setText(items.get(i).value);
        holder.itemChecked.setChecked(false);

        String b = items.get(i).badgeUri;
        if(b != null) {
            Bitmap bitmap;
            if(b.equals("-1"))
                bitmap = BitmapFactory.decodeResource(frame.getResources(), R.drawable.accountbox);
            else {
                bitmap = omb_library.loadContactPhotoThumbnail(b);
            }
            holder.itemBadge.setImageBitmap(bitmap);
        }

    }

}
