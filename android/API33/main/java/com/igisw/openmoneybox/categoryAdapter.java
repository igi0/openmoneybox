/* **************************************************************
 * Name:
 * Purpose:   Category Adapter for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-03-06
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import static com.igisw.openmoneybox.constants._OMB_TOPCATEGORIES_OEMICON;
import static com.igisw.openmoneybox.omb_library.getCustomIcons;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

import java.util.ArrayList;

public class categoryAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final ArrayList<String> values;
	private final ArrayList<Integer> iconIndexes;
	private final TypedArray catIcons;
	private final ArrayList<Bitmap> customIcons;

	public categoryAdapter(Context context, ArrayList<String> values, ArrayList<Integer> icons) {
		super(context, R.layout.categoryitem, values);

		this.context = context;
		this.values = values;
		this.iconIndexes = icons;

		catIcons = context.getResources().obtainTypedArray(R.array.category_drawables_values);
		customIcons = getCustomIcons();
	}

	@Override
	public @NonNull	View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.categoryitem, parent, false);
		TextView textView = rowView.findViewById(R.id.itemName);
		ImageView imageView = rowView.findViewById(R.id.icon);
		textView.setText(values.get(position));

		int ic = iconIndexes.get(position);
		if (ic < 0)
			imageView.setImageResource(catIcons.getResourceId(0, -1));
		else {
			if (ic < _OMB_TOPCATEGORIES_OEMICON) {
				int l = catIcons.length();
				if (ic < l) {
					imageView.setImageResource(catIcons.getResourceId(ic, -1));

				}
			} else {
				imageView.setImageBitmap(customIcons.get(ic - 100));
			}

			SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(context);
			if (Opts.getBoolean("GDarkTheme", false)) {
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					imageView.setBackgroundColor(context.getResources().getColor(R.color.white, omb_library.appContext.getTheme()));
				}
				else{
					imageView.setBackgroundColor(context.getResources().getColor(R.color.white));
				}
			}
		}

		return rowView;
	}

}
