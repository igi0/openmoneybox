/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

public class MainOptions extends AppCompatActivity {

    public static boolean disableSqlCompatSwitch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
        if(Opts.getBoolean("GDarkTheme", false)) {
            this.setTheme(R.style.DarkTheme);
        }

        try {
            getClass().getMethod("getFragmentManager");
            getSupportFragmentManager().beginTransaction().replace(android.R.id.content,
                new PF()).commit();
        } catch (NoSuchMethodException e) { //Api < 11

        }
    }

    @SuppressWarnings("WeakerAccess")
    public static class PF extends PreferenceFragmentCompat
    {
        @Override
        public void onCreatePreferences(Bundle bundle, String S)
        {
            //super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.options);

            Preference pref;

            // Hide unsupported settings
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                pref = getPreferenceScreen().findPreference("GSecurity");
                if (pref != null) pref.setVisible(false);
            }

            if(disableSqlCompatSwitch){
                pref = getPreferenceScreen().findPreference("DB_SqlCipherCompatVersion"); //NON-NLS
                if (pref != null) pref.setEnabled(false);
            }
        }
    }

}
