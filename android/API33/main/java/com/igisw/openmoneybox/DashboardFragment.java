/* **************************************************************
 * Name:
 * Purpose:   Dashboard fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-15
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import static com.igisw.openmoneybox.constants._OMB_TOPCATEGORIES_NUMBER;
import static com.igisw.openmoneybox.constants._OMB_TOPCATEGORIES_OEMICON;
import static com.igisw.openmoneybox.omb_library.FormDigits;
import static com.igisw.openmoneybox.omb_library.getCustomIcons;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {
    private MainActivity activity;
    private DatePicker calendar;
    private ImageButton profitBtn, receivedBtn, giftedBtn;
    private FloatingActionButton expenseBtn;

    public ArrayList<MainActivity.TCategorySummary> summary;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        ArrayList<Bitmap> customIcons = getCustomIcons();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        // Create the OnClickListener
        profitBtn = view.findViewById( R.id.ProfitButton );
        expenseBtn = view.findViewById( R.id.expenseActionButton );
        receivedBtn = view.findViewById( R.id.ReceivedButton );
        giftedBtn = view.findViewById( R.id.GiftedButton );
        View.OnClickListener clickListener = v -> {
            if (v == profitBtn) activity.profitClick(v);
            else if (v == expenseBtn) activity.expenseClick(v);
            else if (v == receivedBtn) activity.receivedClick(v);
            else if (v == giftedBtn) activity.giftedClick(v);
        };
        profitBtn.setOnClickListener(clickListener);
        expenseBtn.setOnClickListener(clickListener);
        receivedBtn.setOnClickListener(clickListener);
        giftedBtn.setOnClickListener(clickListener);

        calendar = view.findViewById( R.id.Calendar );
        calendar.init(calendar.getYear(), calendar.getMonth(), calendar.getDayOfMonth(),
                (arg0, arg1, arg2, arg3) -> giveDateTime()
        );

        LinearLayout catLayout = view.findViewById( R.id.catLayout );
        if(summary != null) {

            // Update view
            int iconIndex;
            TextView category, value;
            ImageView icon;
            Currency curr = Currency.getInstance(Locale.getDefault());
            TypedArray catIcons = getResources().obtainTypedArray(R.array.category_drawables_values);

            for(int i = 0; i < _OMB_TOPCATEGORIES_NUMBER; i++){
                switch(i){
                    case 1:
                        category = view.findViewById(R.id.teCategory2);
                        value = view.findViewById(R.id.teValue2);
                        icon = view.findViewById(R.id.teIcon2);
                        break;
                    case 2:
                        category = view.findViewById(R.id.teCategory3);
                        value = view.findViewById(R.id.teValue3);
                        icon = view.findViewById(R.id.teIcon3);
                        break;
                    default:
                        category = view.findViewById(R.id.teCategory1);
                        value = view.findViewById(R.id.teValue1);
                        icon = view.findViewById(R.id.teIcon1);
                }
                if (summary.get(i).Init) {
                    category.setText(summary.get(i).Name);
                    value.setText(String.format("%s %s", FormDigits(summary.get(i).Value, true), curr.getSymbol())); //NON-NLS
                    iconIndex = summary.get(i).IconIndex;
                    if (iconIndex > 0)
                        if(iconIndex < _OMB_TOPCATEGORIES_OEMICON)
                            icon.setImageResource(catIcons.getResourceId(iconIndex, -1));
                        else {
                            iconIndex -= 100;
                            if(iconIndex < customIcons.size())
                                icon.setImageBitmap(customIcons.get(iconIndex));
                        }
                    else
                        icon.setImageResource(catIcons.getResourceId(0, -1));
                    if (summary.get(i).Value > 0) {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                            Resources.Theme theme = this.requireContext().getTheme();
                            category.setBackgroundColor(getResources().getColor(R.color.green, theme));
                            value.setBackgroundColor(getResources().getColor(R.color.green, theme));
                            icon.setBackgroundColor(getResources().getColor(R.color.green, theme));
                        }
                        else {
                            category.setBackgroundColor(getResources().getColor(R.color.green));
                            value.setBackgroundColor(getResources().getColor(R.color.green));
                            icon.setBackgroundColor(getResources().getColor(R.color.green));
                        }
                    } else {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            Resources.Theme theme = this.requireContext().getTheme();
                            category.setBackgroundColor(getResources().getColor(R.color.red, theme));
                            category.setTextColor(getResources().getColor(R.color.white, theme));
                            value.setBackgroundColor(getResources().getColor(R.color.red, theme));
                            value.setTextColor(getResources().getColor(R.color.white, theme));
                            icon.setBackgroundColor(getResources().getColor(R.color.red, theme));
                        }
                        else {
                            category.setBackgroundColor(getResources().getColor(R.color.red));
                            category.setTextColor(getResources().getColor(R.color.white));
                            value.setBackgroundColor(getResources().getColor(R.color.red));
                            value.setTextColor(getResources().getColor(R.color.white));
                            icon.setBackgroundColor(getResources().getColor(R.color.red));
                        }
                    }
                }
                if (summary.get(i).Init) {
                    category.setVisibility(View.VISIBLE);
                    value.setVisibility(View.VISIBLE);
                    icon.setVisibility(View.VISIBLE);
                } else {
                    category.setVisibility(View.GONE);
                    value.setVisibility(View.GONE);
                    icon.setVisibility(View.GONE);
                }
            }
            if (summary.get(0).Init) catLayout.setVisibility(View.VISIBLE);
            else catLayout.setVisibility(View.GONE);

            catIcons.recycle();

        }

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    private void giveDateTime(){
        int day = calendar.getDayOfMonth();
        int month = calendar.getMonth();
        int year =  calendar.getYear();

        activity.Data.Day.set(year, month, day, 0, 0, 0);
    }

}
