/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-15
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import java.io.File;
import java.util.GregorianCalendar;
import java.util.Objects;

public class DocumentSelection extends AppCompatActivity {

	private static final int PICK_FILE_RESULT_CODE = 1;
	private int RequestCode;

	final ActivityResultLauncher<Intent> mLauncher = registerForActivityResult(
			new ActivityResultContracts.StartActivityForResult(),
			new ActivityResultCallback<ActivityResult>() {
				@Override
				public void onActivityResult(ActivityResult result) {
					// Do your code from onActivityResult
					my_onActivityResult(RequestCode, result.getResultCode(), result.getData());
				}
			});

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.documentselection);

		// Create the OnClickListener
		Button wizardButton, browseButton;
		wizardButton = findViewById(R.id.wizardBtn);
		browseButton = findViewById(R.id.browseBtn);
		View.OnClickListener clickListener = v -> {
			if (v == wizardButton) WizardClick(v);
			else if (v == browseButton) BrowseClick(v);
		};
		wizardButton.setOnClickListener(clickListener);
		browseButton.setOnClickListener(clickListener);

		getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
			@Override
			public void handleOnBackPressed() {
				// Do nothing on BACK press
			}
		});
	}
	
	public void BrowseClick(@SuppressWarnings("unused") View view) { //NON-NLS
		// ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
		// browser.
		Intent fileIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

		// Filter to only show results that can be "opened", such as a
		// file (as opposed to a list of contacts or timezones)
		fileIntent.addCategory(Intent.CATEGORY_OPENABLE);

		fileIntent.setType("*/*");
		RequestCode = PICK_FILE_RESULT_CODE;
        try {
			mLauncher.launch(fileIntent);
        } catch (ActivityNotFoundException e) {
			omb_library.Error(-1, null);
		}
	}
	
	public void WizardClick(@SuppressWarnings("unused") View view) { //NON-NLS
		RequestCode = 2;
		mLauncher.launch(new Intent(this, wizard.class));
	}

	public void my_onActivityResult(int requestCode,int resultCode, Intent data){
        if (data != null) {
			switch (requestCode) {
				case PICK_FILE_RESULT_CODE:
					if (resultCode == RESULT_OK) {
						String filePath ;
						Uri uri = data.getData();

						filePath = RealPathUtil.getRealPath(getApplicationContext(), uri);

						// FilePath is your file as a string
						if (!Objects.requireNonNull(filePath).isEmpty()) {
							SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
							Editor edit = Opts.edit();
							edit.putString("GDDoc", filePath);
							edit.apply();
						}

						setResult(resultCode);
						finish();
					}
					break;
				case 2:
					Bundle bundle = data.getExtras();
					double saved = bundle.getDouble("saved"); //NON-NLS
					double cash = bundle.getDouble("cash"); //NON-NLS

					String extState = Environment.getExternalStorageState();

					if (extState.equals(Environment.MEDIA_MOUNTED)) {
						File sd;
						if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
							sd = getApplicationContext().getExternalFilesDir(null);
						}
						else {
							sd = Environment.getExternalStorageDirectory();
						}

						// Following block defines a not file that does not already exist
						int fileTrail = 0;
						String testPath = sd.getAbsolutePath() + "/openmoneybox"; //NON-NLS
						String filename = testPath + ".omb";
						File testFile = new File(filename);
						while(testFile.exists()){
							fileTrail++;
							filename = String.format(testPath + "%d.omb", fileTrail); //NON-NLS
							testFile = new File(filename);
						}

						omb35core Data_Auto = new omb35core(filename, null);
						String cash_fund = getResources().getString(R.string.wizard_cash_fund);
						Data_Auto.addValue(omb35core.TTypeVal.tvFou, -1,
							getResources().getString(R.string.wizard_saved_fund), saved, -1);
						Data_Auto.addValue(omb35core.TTypeVal.tvFou, -1, cash_fund, cash, -1);
						Data_Auto.setDefaultFund(cash_fund);
						Data_Auto.addDate(new GregorianCalendar(), omb_library.FormDigits(saved + cash, true));
						Data_Auto.database.execSQL("RELEASE roll_back;"); //NON-NLS
						Data_Auto.database.close();

						SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
						Editor edit = Opts.edit();
						edit.putString("GDDoc", filename);
						edit.apply();

						finish();
					}
					break;
				default:
			}
		}
	}

}
