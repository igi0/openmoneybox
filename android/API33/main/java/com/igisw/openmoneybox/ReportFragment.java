/* **************************************************************
 * Name:
 * Purpose:   Report fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-08-19
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReportFragment extends Fragment {
    private final List<lines_wrapper> operations = new ArrayList<>();

    public ReportFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity activity = (MainActivity) getActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report, container, false);

        // Report
        double val;
        Currency curr = Currency.getInstance(Locale.getDefault());

        RecyclerView rv = view.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(Objects.requireNonNull(activity).getApplicationContext());
        rv.setLayoutManager(llm);
        recycler_adapter_operations operations_adapter = new recycler_adapter_operations(operations);
        operations_adapter.frame = activity;
        rv.setAdapter(operations_adapter);

        operations.clear();
        int last_date_index = activity.Data.NLin;
        for(int i = activity.Data.NLin - 1; i >= 0 ; i--) {
            while (! activity.Data.Lines.get(i).IsDate) {
                if(i > 0) i--;
                else break;
            }

            if(activity.Data.Lines.get(i).IsDate) {
                val = Double.parseDouble(activity.Data.Lines.get(i).Value);
                operations.add(new lines_wrapper(0,
                    curr.getSymbol() + " " + omb_library.FormDigits(val, false),
                    "", omb_library.omb_DateToStr(activity.Data.Lines.get(i).Date),
                    "", -1, null, 0, 0,
                    -1/*, 1, ""*/));
            }
            else i--;

            for (int j = i + 1; j < last_date_index; j++) {
                int type;
                int opType = -1;
                String val_string;
                String cat_string;
                switch (activity.Data.Lines.get(j).Type) {
                    case toGain:
                        type = 1;
                        opType = R.drawable.greenplus;
                        break;
                    case toExpe:
                        type = 2;
                        opType = R.drawable.redminus;
                        break;
                    case toSetCre:
                        type = 3;
                        opType = R.drawable.creditnew;
                        break;
                    case toRemCre:
                        type = 4;
                        opType = R.drawable.creditremove;
                        break;
                    case toConCre:
                        type = 5;
                        opType = R.drawable.credit_remit;
                        break;
                    case toSetDeb:
                        type = 6;
                        opType = R.drawable.debtnew;
                        break;
                    case toRemDeb:
                        type = 7;
                        opType = R.drawable.debtremove;
                        break;
                    case toConDeb:
                        type = 8;
                        opType = R.drawable.debt_remit;
                        break;
                    case toGetObj:
                        type = 9;
                        opType = R.drawable.object_received;
                        break;
                    case toGivObj:
                        type = 10;
                        opType = R.drawable.object_given;
                        break;
                    case toLenObj:
                        type = 11;
                        opType = R.drawable.object_lent;
                        break;
                    case toBakObj:
                        type = 12;
                        opType = R.drawable.object_getback;
                        break;
                    case toBorObj:
                        type = 13;
                        opType = R.drawable.object_borrow;
                        break;
                    case toRetObj:
                        type = 14;
                        opType = R.drawable.object_giveback;
                        break;
                    default:
                        type = -1;
                }
                if (type < 9) {
                    val = 0;
                    String weak_str = activity.Data.Lines.get(j).Value;
                    try {
                        val = Double.parseDouble(weak_str);
                    } catch (NumberFormatException e) {
                        NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
                        try {
                            val = Objects.requireNonNull(nf.parse(weak_str)).doubleValue();
                        } catch (ParseException ex) {
                            // TODO Auto-generated catch block
                            ex.printStackTrace();
                        }
                    }

                    if(activity.Data.Lines.get(j).currencyIndex == -1)
                        val_string = curr.getSymbol() + " " + omb_library.FormDigits(val, false);
                    else
                        val_string = "( " + omb_library.FormDigits(val, false)
                            + " " +activity.Data.Lines.get(j).currencySymbol + " )";

                } else val_string = activity.Data.Lines.get(j).Value;

                cat_string = "";
                if (activity.Data.Lines.get(j).CategoryIndex != -1)
                    for(int k = 0; k < activity.Data.NCat; k++)
                        if(activity.Data.CategoryDB.get(k).Id == activity.Data.Lines.get(j).CategoryIndex){
                            cat_string = activity.Data.CategoryDB.get(k).Name;
                            break;
                        }

                long c_id = activity.Data.Lines.get(j).ContactIndex;
                String badgeUri = null;
                if(c_id > 0){
                    badgeUri = activity.Data.getContactImage(c_id);
                    if(badgeUri == null) badgeUri = "-1";
                }

                operations.add(new lines_wrapper(1, val_string,
                    activity.Data.Lines.get(j).Reason,
                    omb_library.omb_TimeToStr(activity.Data.Lines.get(j).Time),
                    cat_string, opType, badgeUri, activity.Data.Lines.get(j).Latitude,
                    activity.Data.Lines.get(j).Longitude,
                    activity.Data.Lines.get(j).currencyIndex
                    /*
                    ,
                    activity.Data.Lines.get(j).currencyRate,
                    activity.Data.Lines.get(j).currencySymbol
                    */
                ));

            }

            last_date_index = i;

        }

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

}
