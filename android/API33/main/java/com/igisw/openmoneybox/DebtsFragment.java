/* **************************************************************
 * Name:
 * Purpose:   Debts fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-11-07
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class DebtsFragment extends Fragment {
    public int debt_pos;
    private final List<CreditDebt_wrapper> debts = new ArrayList<>();
    public ImageButton btn_removeDebt, btn_remitDebt;
    public RecyclerView dv;    // dv: debts view
    private FloatingActionButton newDebtBtn;

    public DebtsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity activity = (MainActivity) requireActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_debts, container, false);

        // Debts tab
        String str_tmp;
        Currency curr = Currency.getInstance(Locale.getDefault());
        TextView total_view;

        // Create the OnClickListener
        btn_removeDebt = view.findViewById( R.id.delDebt );
        btn_remitDebt = view.findViewById( R.id.remitDebt );
        newDebtBtn = view.findViewById( R.id.debtActionButton );
        View.OnClickListener clickListener = v -> {
            if (v == newDebtBtn) activity.NewDebtClick(v);
            else if (v == btn_removeDebt) activity.RemoveDebtClick(v);
            else if (v == btn_remitDebt) activity.RemitDebtClick(v);
        };
        newDebtBtn.setOnClickListener(clickListener);
        btn_removeDebt.setOnClickListener(clickListener);
        btn_remitDebt.setOnClickListener(clickListener);

        btn_removeDebt.setEnabled(false);
        btn_remitDebt.setEnabled(false);
        dv = view.findViewById(R.id.dv);
        dv.setLayoutManager(new LinearLayoutManager(Objects.requireNonNull(activity).getApplicationContext()));
        Recycler_Adapter_CreditDebt debts_adapter = new Recycler_Adapter_CreditDebt(debts);
        debts_adapter.frame = activity;
        dv.setAdapter(debts_adapter);

        debts.clear();
        for(int i = 0; i < activity.Data.NDeb; i++){
            str_tmp = curr.getSymbol() + " " + omb_library.FormDigits(activity.Data.Debts.get(i).Value, false);

            long c_id = activity.Data.Debts.get(i).ContactIndex;
            String badgeUri = null;
            if(c_id > 0){
                badgeUri = activity.Data.getContactImage(c_id);
                if(badgeUri == null) badgeUri = "-1";
            }

            debts.add(new CreditDebt_wrapper(/*activity.Data.Debts.get(i).Id, */activity.Data.Debts.get(i).Name,
                    str_tmp, badgeUri));

        }
        debts_adapter.notifyDataSetChanged();

        total_view = view.findViewById(R.id.total_debt);
        str_tmp = getResources().getString(R.string.total) + "<br><small>" + curr.getSymbol() + " ";
        str_tmp += omb_library.FormDigits(activity.Data.Tot_Debts, false) + "</small>";
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            total_view.setText(HtmlCompat.fromHtml(str_tmp, HtmlCompat.FROM_HTML_MODE_COMPACT));
        else total_view.setText(HtmlCompat.fromHtml(str_tmp, HtmlCompat.FROM_HTML_MODE_LEGACY));

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

}
