/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-15
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

public class object_add extends Activity {

	private final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 104;

	private boolean return_contact, return_reminder;
	private boolean browsed_contacts;
	private String id;
	private EditText objText, perText;
	private CheckBox almCheck;
	private DatePicker almText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.objects_add);
		
		return_contact = true;
		return_reminder = false;
		browsed_contacts = false;

		TextView caption = findViewById(R.id.Title);
		objText = findViewById(R.id.Obj);
		perText = findViewById(R.id.Per);

		almCheck = findViewById(R.id.AlmCB);
		almCheck.setOnCheckedChangeListener((buttonView, isChecked) -> almText.setEnabled(isChecked));
		almText = findViewById(R.id.Alarm);
		almText.setEnabled(false);
		ImageButton findButton = findViewById(R.id.searchButton);

		// Create the OnClickListener
		TextView OkButton = findViewById(R.id.OkBtn);
		View.OnClickListener clickListener = v -> {
			if (v == findButton) searchContact(v);
			else if (v == OkButton) okBtnClick(v);
		};
		findButton.setOnClickListener(clickListener);
		OkButton.setOnClickListener(clickListener);

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			LinearLayout ll = findViewById(R.id.ll_objects);
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				Resources.Theme theme = getTheme();
				ll.setBackgroundColor(getResources().getColor(R.color.black, theme));
				objText.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				objText.setTextColor(getResources().getColor(R.color.white, theme));
				perText.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				perText.setTextColor(getResources().getColor(R.color.white, theme));

				almCheck.setTextColor(getResources().getColor(R.color.white, theme));
			}
			else{
				ll.setBackgroundColor(getResources().getColor(R.color.black));
				objText.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				objText.setTextColor(getResources().getColor(R.color.white));
				perText.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				perText.setTextColor(getResources().getColor(R.color.white));

				almCheck.setTextColor(getResources().getColor(R.color.white));
			}

		}

		Bundle bundle=getIntent().getExtras();
		
		int labels = bundle.getInt("labels"); //NON-NLS
		switch(labels){
			case 1:
				caption.setText(getResources().getString(R.string.object_receive));
				hideAlarm();
				break;
			case 2:	// Gift object
				caption.setText(getResources().getString(R.string.object_give));
				objText.setHint(getResources().getString(R.string.object_given_name));
				perText.setHint(getResources().getString(R.string.object_receiver));
				hideAlarm();
				break;
			case 3: // Lend object
				caption.setText(getResources().getString(R.string.object_lend));
				objText.setHint(getResources().getString(R.string.object_lent_name));
				perText.setHint(getResources().getString(R.string.object_receiver));
				return_reminder = true;
				break;
			case 4: // Borrow object
				caption.setText(getResources().getString(R.string.object_borrow));
				objText.setHint(getResources().getString(R.string.object_borrow_name));
				perText.setHint(getResources().getString(R.string.object_donor));
				return_reminder = true;
				break;
			case 5: // Add shop item
				caption.setText(getResources().getString(R.string.shoplist_caption));
				objText.setHint(getResources().getString(R.string.shoplist_item));
				//per_label.setVisibility(View.GONE);
				perText.setVisibility(View.GONE);
				findButton.setVisibility(View.GONE);
				return_contact = false;
				return_reminder = true;
				//break;

		}

	}
	
	public void okBtnClick(@SuppressWarnings("unused") View view){ //NON-NLS
		int ReturnCode;
		
		omb_library.appContext = getApplicationContext();

		String obj = objText.getText().toString();
		String per;
		if(return_contact){
			per = perText.getText().toString();
		}
		else per = "";
		//TextView value = (TextView) findViewById(R.id.opValue);
		if(obj.isEmpty()){
			omb_library.Error(32, "");
			objText.requestFocus();
			return;}
		
		if(return_contact){
			if(per.isEmpty()){
				omb_library.Error(33, "");
				perText.requestFocus();
				return;}
		}
		
		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		obj = omb_library.iUpperCase(obj);
		obj = obj.trim();
		bundle.putString("object", obj); //NON-NLS
		if(! per.isEmpty()){
			per = omb_library.iUpperCase(per);
			per = per.trim();
		}
		bundle.putString("person", per); //NON-NLS
		if(return_reminder){
			bundle.putInt("day", almText.getDayOfMonth()); //NON-NLS
			bundle.putInt("month", almText.getMonth()); //NON-NLS
			int year = almText.getYear();
			if(! almCheck.isChecked()) year += 100;
			bundle.putInt("year", year); //NON-NLS
		}

		boolean found = false;
		String name = null;
		if(browsed_contacts){
			int colInd;
			Cursor phones = getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI,
                null, null, null, ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY +
				" ASC"); //NON-NLS
			while (phones.moveToNext()) {
				colInd = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
				if(colInd != -1) name = phones.getString(colInd);
				else name = null;

				colInd = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
				if(colInd != -1) id = phones.getString(colInd);
				else id = null;

				if((id != null) && (name != null)) {
					if(name.compareTo(perText.getText().toString()) == 0){
						found = true;
						break;
					}
				}

			}
			phones.close();
		}

		if(found){
			if(name.compareTo(perText.getText().toString()) == 0) {
				bundle.putString("contact_id", id); //NON-NLS
				bundle.putString("contact_name", name); //NON-NLS
			}
		}
		else bundle.putString("contact_id", "-1"); //NON-NLS

		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}
	
	private void hideAlarm(){
		almCheck.setVisibility(View.GONE);
		almText.setVisibility(View.GONE);
	}

	public void searchContact(@SuppressWarnings("unused") View view){ //NON-NLS
		if(! checkContactPermission()) return;

		Intent intent = new Intent(this, ContactActivity.class);
		Bundle bundle = new Bundle();
		bundle.putString("str", perText.getText().toString()); //NON-NLS
		intent.putExtras(bundle);
		startActivityForResult(intent, 1);
	}

	@Override
	public void onActivityResult(int requestCode,int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode != RESULT_OK) return;
		if(requestCode == 1){
			Bundle bundle = data.getExtras();

			perText.setText(bundle.getString("contact")); //NON-NLS
			id = bundle.getString("id"); //NON-NLS
			browsed_contacts = true;
		}
	}

	private boolean checkContactPermission(){
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
				!= PackageManager.PERMISSION_GRANTED) {

			// Request the permission.
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.READ_CONTACTS},
					MY_PERMISSIONS_REQUEST_READ_CONTACTS);
			// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
			// app-defined int constant. The callback method gets the
			// result of the request.

			return false;
		}
		else return true;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		if (requestCode == MY_PERMISSIONS_REQUEST_READ_CONTACTS) {
			boolean canUseContacts = grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED;

            if (!canUseContacts) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.contacts_cant_access), Toast.LENGTH_LONG).show();
				finish();
			}
		}
	}

}
