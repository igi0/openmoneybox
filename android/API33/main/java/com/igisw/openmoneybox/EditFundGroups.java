/* **************************************************************
 * Name:      
 * Purpose:   Fund groups management for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-08-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.preference.PreferenceManager;

import java.util.ArrayList;

public class EditFundGroups extends Activity {

	private boolean AddedOrModifiedGroups, DeletedGroups;
	private int group_item_pos;
	private ArrayList<String> groupList;
	private ArrayList<Integer> groupIDList;

	private ArrayList<String> fundList;

	private ArrayList<Integer> idList;
	private ArrayList<Integer> ownerList;
	private ArrayList<String> ownernameList;

	private ArrayAdapter<String> groupAdapter;
	private FundGroupAdapter fundAdapter;

	private ListView groupView;
	private ListView fundView;

	private ImageButton addGroupButton;
	private ImageButton delGroupButton;
	private EditText nameEdit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fundsgroups);

		group_item_pos = -1;

		TextView label = findViewById(R.id.Title);
		label.setText(getResources().getString(R.string.fundGroups_form_title));

		Bundle bundle=getIntent().getExtras();

		groupList = bundle.getStringArrayList("groups");
		groupIDList = bundle.getIntegerArrayList("groupids");

		fundList = bundle.getStringArrayList("funds");

		idList = bundle.getIntegerArrayList("ids");
		ownerList = bundle.getIntegerArrayList("owners");
		ownernameList = bundle.getStringArrayList("ownernames");

	    // Create ArrayAdapters
		groupAdapter = new ArrayAdapter<>(this,
				android.R.layout.simple_list_item_single_choice, groupList);
		groupView = findViewById(R.id.GroupList);
		groupView.setAdapter(groupAdapter);

		fundAdapter = new FundGroupAdapter(this,
		fundList, ownerList);
		fundView = findViewById(R.id.FundList);
		fundView.setAdapter(fundAdapter);

		// Create the OnClickListener
		addGroupButton = findViewById(R.id.AddGroup);
		delGroupButton = findViewById(R.id.removeGroup);
		TextView OkButton = findViewById(R.id.OkBtn);
		nameEdit = findViewById(R.id.NameEdit);

		View.OnClickListener clickListener = v -> {
			if (v == delGroupButton) deleteGroupClick(v);
			else if (v == addGroupButton) addGroupClick(v);
			else if (v == OkButton) okBtnClick(v);
		};
		addGroupButton.setOnClickListener(clickListener);
		delGroupButton.setOnClickListener(clickListener);
		OkButton.setOnClickListener(clickListener);

		delGroupButton.setEnabled(false);

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			LinearLayout ll = findViewById(R.id.ll1);
			ll.setBackgroundColor(0xff000000);

			TextView textView = findViewById(R.id.noteText);

			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				Resources.Theme theme = getTheme();
				nameEdit.setHintTextColor(getResources().getColor(R.color.green_dark, theme));
				textView.setTextColor(getResources().getColor(R.color.green_dark, theme));
			}
			else{
				nameEdit.setHintTextColor(getResources().getColor(R.color.green_dark));
				textView.setTextColor(getResources().getColor(R.color.green_dark));
			}

		}

		groupView.setOnItemClickListener((adapter, view, position, arg3) -> {
			group_item_pos = position;
			if (group_item_pos > -1)
				fundAdapter.activeGroup = groupIDList.get(group_item_pos);
			else fundAdapter.activeGroup = -1;

			groupAdapter.notifyDataSetChanged();
			fundAdapter.notifyDataSetChanged();
			refreshControls();
		});

		fundView.setOnItemClickListener((adapter, view, position, arg3) -> {
			if(group_item_pos > -1){
				String GroupName = groupList.get(group_item_pos);
				int GroupID = groupIDList.get(group_item_pos);

				boolean Checked;
				String FundName;
				CheckBox checkBox;
				TextView item;
				checkBox = view.findViewById(R.id.checkbox);
				Checked = ! checkBox.isChecked();
				checkBox.setChecked(Checked);

				item = view.findViewById(R.id.itemName);
				FundName = item.getText().toString();

//			bool Found = false;

				for(int i = 0; i < fundList.size(); i++){
					if(FundName.equalsIgnoreCase(/*name*/fundList.get(i))){
//					Found = true;
						if(Checked){
							ownernameList.set(i, GroupName);
							ownerList.set(i, GroupID);
						}
						else{
							ownernameList.set(i, "");
							ownerList.set(i, -1);
						}
						break;
					}
				}

				fundAdapter.notifyDataSetChanged();
				AddedOrModifiedGroups = true;

			}

		});

		refreshControls();

		AddedOrModifiedGroups = false;
		DeletedGroups = false;
	}
	
	public void okBtnClick(@SuppressWarnings("unused") View view){
		if((fundView.isEnabled()) && (fundView.getCount() > 0)){
			// Look for groups with less than 2 funds
			int Pos = 0, Sum = -1;
			int FTG = fundList.size();
			int OwnerID;
			String OwnerName;
			while(Pos < FTG){

				if(! ownernameList.get(Pos).isEmpty()){
					OwnerID = ownerList.get(Pos);
					OwnerName = ownernameList.get(Pos);
					Sum = 0;
					for(int i = 0; i < FTG; i++)
						if(((ownerList.get(i) == OwnerID) && (ownerList.get(i) > -1)) || (ownernameList.get(i).equalsIgnoreCase(OwnerName)))
							Sum++;
					if(Sum < 2){
						omb_library.Error(50, "");
						return;
					}
				}

				Pos++;
			}
			if(Sum == -1){
				omb_library.Error(50, "");
				return;
			}
		}

		int ReturnCode;
		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putBoolean("addedormodifiedgroups", AddedOrModifiedGroups);
		bundle.putBoolean("deletedgroups", DeletedGroups);
		if(AddedOrModifiedGroups || DeletedGroups)
			bundle.putStringArrayList("groups", groupList);
		if(AddedOrModifiedGroups){
			bundle.putIntegerArrayList("ids", idList);
			bundle.putStringArrayList("fundnames", /*name*/fundList);
			bundle.putIntegerArrayList("owners", ownerList);
			bundle.putStringArrayList("ownernames", ownernameList);
		}
		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}
	
	public void addGroupClick(@SuppressWarnings("unused") View view){

		String Name = nameEdit.getText().toString();

		if(Name.isEmpty()){
			omb_library.Error(51, null);
			nameEdit.requestFocus();
			return;
		}

		boolean Found = false;
		for(int i = 0; i < groupList.size(); i++)
			if(groupList.get(i).equalsIgnoreCase(Name)){
				Found = true;
				break;
			}
		if(Found){
			omb_library.Error(52, Name);
			nameEdit.requestFocus();
			return;
		}

		Name = omb_library.iUpperCase(Name);
		groupList.add(Name);

		// Add new fake group ID
		int fakeID = 1;
		do{
			Found = false;
			for(int i = 0; i < groupIDList.size(); i++)
				if(i == fakeID){
					Found = true;
					break;
				}
			if(Found) fakeID++;
		}
		while(Found);
		groupIDList.add(fakeID);

		AddedOrModifiedGroups = true;
		groupView.setSelection(groupList.size() - 1);
		groupAdapter.notifyDataSetChanged();
		nameEdit.setText("");
		refreshControls();

	}

	public void deleteGroupClick(@SuppressWarnings("unused") View view){
		if(group_item_pos >= 0){
			groupList.remove(group_item_pos);
			DeletedGroups = true;
			groupAdapter.notifyDataSetChanged();
			refreshControls();
		}
	}

	void refreshControls(){
		boolean GroupSelection = group_item_pos >= 0;
		delGroupButton.setEnabled(GroupSelection);
		fundView.setEnabled(GroupSelection);
	}
}
