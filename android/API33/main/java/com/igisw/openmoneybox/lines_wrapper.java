/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-08-19
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

class lines_wrapper {
    final int type;
    final String value;
    String reason;
    final String time;
    String category;
    int iconId;
    String badgeUri;
    double latitude;
    double longitude;

    long currencyIndex;
    /*
    double currencyRate;
    String currencySymbol;
    */

    lines_wrapper(int type, String value, String reason, String time, String category, int iconId,
                  String badgeUri, double latitude, double longitude,
                  long currencyIndex/*, double currencyRate, String currencySymbol*/) {
        this.type = type;
        this.value = value;
        this.time = time;
        if (type == 1) {
            this.reason = reason;
            this.category = category;
            this.iconId = iconId;
            this.badgeUri = badgeUri;
            this.latitude = latitude;
            this.longitude = longitude;

            this.currencyIndex = currencyIndex;
            /*
            this.currencyRate = currencyRate;
            this.currencySymbol = currencySymbol;
            */
        }
    }
}
