/* **************************************************************
 * Name:
 * Purpose:   Category Adapter for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-08-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class FundGroupAdapter extends ArrayAdapter<String> {
	public int activeGroup;
	private final Context context;
	private final ArrayList<String> values;
	private final ArrayList<Integer> ownerIDs;

	public FundGroupAdapter(Context context, ArrayList<String> values, ArrayList<Integer> checked) {
		super(context, R.layout.categoryitem, values);

		this.context = context;
		this.values = values;
		this.ownerIDs = checked;
		activeGroup = -1;
	}

	@Override
	public @NonNull	View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.fundgroupitem, parent, false);
		TextView textView = rowView.findViewById(R.id.itemName);
		textView.setText(values.get(position));
		CheckBox checkBox = rowView.findViewById(R.id.checkbox);
		int id = ownerIDs.get(position);
		checkBox.setChecked((id > -1) && (id == activeGroup));

		return rowView;
	}

}
