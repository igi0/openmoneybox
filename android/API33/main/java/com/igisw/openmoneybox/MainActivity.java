/* *************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_WEAK;
import static androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL;
import static com.igisw.openmoneybox.constants._OMB_TOPCATEGORIES_NUMBER;
import static com.igisw.openmoneybox.omb_library.getCustomIcons;
import static com.igisw.openmoneybox.omb_library.setCustomIcons;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.method.DigitsKeyListener;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.IntentSanitizer;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import org.jetbrains.annotations.NonNls;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

public class MainActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener//,
		{

	private final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 102;
	private final int ASK_LOCATION_PERMISSION = 103;
	private final int MY_PERMISSIONS_REQUEST_OVERLAY = 104;

	private boolean HasAlarms = false;
	private boolean askForLogin = true; // false when launching at boot or for one minute after successful login

	public omb35core Data;
	private String document;
	private String tmp_string;
	public SharedPreferences Opts;

	public double LastLatitude = constants.ombInvalidLatitude;
	public double LastLongitude = constants.ombInvalidLongitude;
	private boolean LocationPermission = false;

	private int fragmentIndex;
	private boolean viewIsAtHome;
	private FundsFragment FunFragment;
	private CreditsFragment CreFragment;
	private DebtsFragment DebFragment;
	private ObjectsFragment ObjFragment;
	private ShoppingListFragment ShoFragment;
	private ChartFragment ChaFragment;

	@NonNls
	private final String dummyId = "some_channel_id";
	@NonNls
	private final String alarmId = "some_other_channel_id";
	private NotificationManager notificationManager;
	NotificationChannel serviceNotificationChannel;
	NotificationChannel alarmNotificationChannel;

	public static ArrayList<Bitmap> customIcons;

	private LocationManager mLocationManager;
	private final LocationListener mLocationListener = new LocationListener() {
		@Override
		public void onLocationChanged(final Location location) {
			LastLatitude = location.getLatitude();
			LastLongitude = location.getLongitude();
		}

		@Override
		public void onProviderDisabled(@NonNull String provider){

		}

		@Override
		public void onProviderEnabled(@NonNull String provider){

		}

		// Removing this call causes crashes on API < 29
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras){

		}
	};

	public static class TCategorySummary{
		boolean Init;
		int IconIndex;
		String Name;
		double Value;
		TCategorySummary(){
			Init = false;
			IconIndex = -1;
			Name = "";
			Value = 0;
		}
	}

	private int RequestCode;
	final ActivityResultLauncher<Intent> mLauncher = registerForActivityResult(
			new ActivityResultContracts.StartActivityForResult(),
			new ActivityResultCallback<ActivityResult>() {
				@Override
				public void onActivityResult(ActivityResult result) {
					// Do your code from onActivityResult
					my_onActivityResult(RequestCode, result.getResultCode(), result.getData());
				}
			});

	final AtomicBoolean allFilesPermissionDialogActive = new AtomicBoolean(false);
	final AtomicBoolean bootPermissionDialogActive = new AtomicBoolean(false);
	AlertDialog.Builder alertAllFilesPermission;
	AlertDialog.Builder alertBoot;

	@SuppressLint("DefaultLocale")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// float density = getResources().getDisplayMetrics().density;	// http://stackoverflow.com/questions/5099550/how-to-check-an-android-device-is-hdpi-screen-or-mdpi-screen

		customIcons = new ArrayList<>();

		setContentView(R.layout.activity_main);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();

		NavigationView navigationView = findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);

		Opts = PreferenceManager.getDefaultSharedPreferences(this);

		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			navigationView.setBackgroundColor(0xff000000);
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
				navigationView.setItemIconTintList(getResources().getColorStateList(R.color.state_list, this.getTheme()));
				navigationView.setItemTextColor(getResources().getColorStateList(R.color.state_list, this.getTheme()));
			}
			else {
				navigationView.setItemIconTintList(AppCompatResources.getColorStateList(getApplicationContext(), R.color.state_list));
				navigationView.setItemTextColor(AppCompatResources.getColorStateList(getApplicationContext(), R.color.state_list));
			}
		}
		else this.setTheme(R.style.LightTheme);

		new SimpleEula(this).show(false);

		// Check permissions

		// Write external SD permission
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
			if(! Environment.isExternalStorageManager()){
				// Request the permission.
				try {
					alertAllFilesPermission = new AlertDialog.Builder(this, R.style.ombDialogTheme);

					alertAllFilesPermission.setTitle(getResources().getString(R.string.app_name));
					alertAllFilesPermission.setMessage(getResources().getString(R.string.allfiles_access_permission));

					alertAllFilesPermission.setPositiveButton(getResources().getString(android.R.string.ok), (dialog, whichButton) -> {
							// Show alert dialog to the user saying a separate permission is needed
							// Launch the settings activity if the user prefers
						Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
						intent.addCategory("android.intent.category.DEFAULT");
						intent.setData(Uri.parse(String.format("package:%s",getApplicationContext().getPackageName())));
						mLauncher.launch(intent);
						allFilesPermissionDialogActive.set(false);
					});

					alertAllFilesPermission.setNegativeButton(getResources().getString(
						R.string.dialog_cancel), (dialog, whichButton)
							-> allFilesPermissionDialogActive.set(false));

					alertAllFilesPermission.show();
					allFilesPermissionDialogActive.set(true);

				} catch (Exception e) {
					Intent intent = new Intent();
					intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
					mLauncher.launch(intent);
				}

			}
		}
		else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
				!= PackageManager.PERMISSION_GRANTED) {

			// Request the permission.
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
					MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
			// MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE is an
			// app-defined int constant. The callback method gets the
			// result of the request.
			return;
		}

		// Location permission
		if(Opts.getBoolean("GSavePositions", false)){
			LocationPermission = (ContextCompat.checkSelfPermission(this,
				Manifest.permission.ACCESS_FINE_LOCATION)
				== PackageManager.PERMISSION_GRANTED) ||
				(ContextCompat.checkSelfPermission(this,
				Manifest.permission.ACCESS_COARSE_LOCATION)
				== PackageManager.PERMISSION_GRANTED);
			if(! LocationPermission){
				ActivityCompat.requestPermissions(this,
						new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
								Manifest.permission.ACCESS_COARSE_LOCATION},
						ASK_LOCATION_PERMISSION);

			}

		}

		// Overlay permission (to start on boot)
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
			String Key = "Overlay_permission_request";
			int Tries = Opts.getInt(Key, 3);
			if(Settings.canDrawOverlays(getApplicationContext()))
			{
				if(Tries <= 0){
					Tries = 3;
					SharedPreferences.Editor editor = Opts.edit();
					editor.putInt(Key, Tries);
					editor.apply();
				}
			}
			else {
				// Limit to ask three times to avoid annoying the user
				if (Tries > 0) {
					alertBoot = new AlertDialog.Builder(this, R.style.ombDialogTheme);

					alertBoot.setTitle(getResources().getString(R.string.app_name));
					alertBoot.setMessage(getResources().getString(R.string.dialog_overlay_permission));

					alertBoot.setPositiveButton(getResources().getString(android.R.string.ok), (dialog, whichButton) -> {
						// Show alert dialog to the user saying a separate permission is needed
						// Launch the settings activity if the user prefers
						Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
								Uri.parse("package:" + getApplicationContext().getPackageName()));
						RequestCode = MY_PERMISSIONS_REQUEST_OVERLAY;
						mLauncher.launch(intent);
						bootPermissionDialogActive.set(false);
					});

					alertBoot.setNegativeButton(getResources().getString(R.string.dialog_cancel),
						(dialog, whichButton) -> bootPermissionDialogActive.set(false));

					alertBoot.show();
					bootPermissionDialogActive.set(true);
					if(allFilesPermissionDialogActive.get()) alertAllFilesPermission.show();

					Tries--;
					SharedPreferences.Editor editor = Opts.edit();
					editor.putInt(Key, Tries);
					editor.apply();
				}
			}
		}

		notificationManager =
			(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
			createNotificationChannels();
		}

		document = Opts.getString("GDDoc", "");
		Bundle bundle = getIntent().getExtras();

		// Load sqlcipher libs
//		SQLiteDatabase.loadLibs(this);
		System.loadLibrary("sqlcipher");
		if(document.isEmpty())
		{
			// Browse for file or launch wizard
			RequestCode = 100;
			mLauncher.launch(new Intent(getApplicationContext(), DocumentSelection.class));
		}
		else
		{
			openDocument();

		    if(bundle != null){
	
				int kind = bundle.getInt("kind");
				int index = bundle.getInt("index");
				boolean snooze_all = bundle.getBoolean("snooze_all");
				int year = bundle.getInt("year");
				int month = bundle.getInt("month");
				int day = bundle.getInt("day");

				ContentValues cv = new ContentValues();

				GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
				cal.set(year, month, day);
				
				switch(kind){
					case 1:
						Data.Lent.get(index).Alarm = cal;
						cv.clear();
						cv.put("alarm", cal.getTimeInMillis() / 1000);
						Data.database.update("Loans", cv, "id = " + String.format("%d", Data.Lent.get(index).Id), null);
						if(snooze_all)
						{
							boolean res;
							do{
								res = doChecks(true, 1, cal);
							}
							while(res);
						}
						Data.FileData.Modified = true;
						break;
					case 2:
						Data.Borrowed.get(index).Alarm = cal;
						cv.clear();
						cv.put("alarm", cal.getTimeInMillis() / 1000);
						Data.database.update("Borrows", cv, "id = " + String.format("%d", Data.Borrowed.get(index).Id), null);
						if(snooze_all)
						{
							boolean res;
							do{
								res = doChecks(true, 2, cal);
							}
							while(res);
						}
						Data.FileData.Modified = true;
						break;
					case 3:
						Data.ShopItems.get(index).Alarm = cal;
						cv.clear();
						cv.put("alarm", cal.getTimeInMillis() / 1000);
						Data.database.update("Shoplist", cv, "id = " + String.format("%d", Data.ShopItems.get(index).Id), null);
						if(snooze_all)
						{
							boolean res;
							do{
								res = doChecks(true, 3, cal);
							}
							while(res);
						}
						Data.FileData.Modified = true;
						break;
				}
				if(Data.FileData.Modified)displayView(R.id.nav_dashboard);

		    }
		}

		if(savedInstanceState == null) displayView(R.id.nav_dashboard);
		else {
			// Restore active fragment
			displayView(savedInstanceState.getInt("fragindex"));
		}

		if(Opts.getBoolean("GSavePositions", false)) {
			if (LocationPermission) {
				mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

				mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000,
					10, mLocationListener);

				Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				if(location != null) {
					LastLatitude = location.getLatitude();
					LastLongitude = location.getLongitude();
				}
			}
		}

		if(bundle == null) {
			// Check if launched from shortcut
			switch (getIntent().getAction()) {
				case "EXPENSE":
					expenseClick(null);
					break;
				case "PROFIT":
					profitClick(null);
					break;
				case "GROCERY":
					displayView(R.id.nav_shoplist);
					break;
			}
		}

		if(getIntent().getAction().equals("android.intent.action.BOOT_COMPLETED")) {
			my_onBackPressed();	// This launches the notification service if necessary
			askForLogin = false;
			new Thread(new ResetLoginTask()).start();
		}
		//else Authenticate();

		getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
			@Override
			public void handleOnBackPressed() {
				my_onBackPressed();
			}
		});
	}

	@Override
	protected void onStart(){
		super.onStart();

		if(Data != null)
			if(! Data.successfulKey) insertPass();

		if(askForLogin)
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
				Authenticate();
	}

	@Override
	public boolean onCreateOptionsMenu(@NonNull Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.mainmenu, menu);
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
		
		Intent browserIntent;

		int itemId = item.getItemId();
		if (itemId == R.id.editcategories) {
			EditCategoriesClick();
			return true;
		} else if (itemId == R.id.options) {
			MainOptions mainOptions = new MainOptions();
			MainOptions.disableSqlCompatSwitch = Data.DatabaseIsSqlCipher_V4;
			startActivity(new Intent(this, mainOptions.getClass()));
			return true;
		} else if (itemId == R.id.setTotal) {
			SetTotalClick();
			return true;
		} else if (itemId == R.id.defaultFund) {
			DefaultFundClick();
			return true;
		} else if (itemId == R.id.about) {
			about.Show(MainActivity.this);
			return true;
		} else if (itemId == R.id.help) {
			String guideAddress = "http://igisw-bilancio.sourceforge.net/android/guide/35/" + getResources().getString(R.string.lang) + "/help/openmoneybox-android.html"; //NON-NLS
			browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(guideAddress));
			startActivity(browserIntent);
			return true;
		} else if (itemId == R.id.donate) {
			String donateAddress = "https://www.paypal.com/paypalme/igCali";
			browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(donateAddress));
			startActivity(browserIntent);
			return true;
		} else if (itemId == R.id.bugReport) {
			String bugAddress = "https://gitlab.com/igi0/openmoneybox/-/issues";
			browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(bugAddress));
			startActivity(browserIntent);
			return true;
		} else if (itemId == R.id.exportArchive) {
			File fMaster = this.getDatabasePath(constants.archive_name);
			if (fMaster.exists()) {
				String dest = Data.FileData.FileName.getParent();
				dest = dest + "/" + constants.archive_name;
				try {
					omb_library.copyFile(fMaster, dest);
				} catch (IOException e) {
					omb_library.Error(-1, null);
				}
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.export_archive_complete), Toast.LENGTH_LONG).show();

			}
			return true;
		}
		else if (itemId == R.id.oss_licenses) {
			new SimpleEula(this).show(true);
		}
		return super.onOptionsItemSelected(item);
	}

//	@Override
	public void my_onBackPressed() {
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		}
		else if (viewIsAtHome) {
			if(Data != null) {
				HasAlarms = Data.HasAlarms();

				if (Data.FileData.Modified) {
					AlertDialog.Builder alert = new AlertDialog.Builder(this, R.style.ombDialogTheme);

					alert.setTitle(getResources().getString(R.string.app_name));
					alert.setMessage(getResources().getString(R.string.file_changed));

					alert.setPositiveButton(getResources().getString(android.R.string.ok), (dialog, whichButton) -> {
						Data.database.execSQL("RELEASE roll_back;");
						Data.FileData.Modified = false;
						stopLocation();
						if (HasAlarms) moveTaskToBack(true);
						else {
							Data.database.close();
							finish();
						}
					});

					alert.setNegativeButton(getResources().getString(R.string.dialog_no), (dialog, whichButton) -> {
						Data.FileData.Modified = false;
						stopLocation();
						if (HasAlarms) moveTaskToBack(true);
						else {
//							onBackPressed();
							Data.database.close();
							finish();
						}
					});

					alert.setNeutralButton(getResources().getString(R.string.dialog_cancel), (dialog, whichButton) -> {
						//
					});
					alert.show();
				}
				else {
					Data.database.close();
					stopLocation();
//					super.onBackPressed();
					finish();
				}

				// Start timer to check alarms
				if(HasAlarms) {

					if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
						ServiceNotification();

					new Thread(new Task(this)).start();
				}
			}
			else{
//				super.onBackPressed();
				finish();
			}
		}
		else { //if the current view is not the Dashboard fragment
			displayView(R.id.nav_dashboard); //display the Dashboard fragment
		}
	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);

		if((document != null) && (! document.isEmpty())){

			// Save active fragment
			outState.putInt("fragindex", fragmentIndex);
	   }
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(! isFinishing())
			if(Data.database != null)
				Data.database.close();	// close the database on orientation change
	}

	private void operation(int Op){
		int i;
		if(Op == 0 || Op > 2) return;

		Bundle bundle = new Bundle();
		Intent intent = new Intent(this, operation.class);
		
		if(Op == 1){
			bundle.putBoolean("change_labels", false);
			Data.updateMatters(omb35core.TOpType.toGain);
		}
		else{
			bundle.putBoolean("change_labels", true);
			Data.updateMatters(omb35core.TOpType.toExpe);
		}
		bundle.putStringArrayList("matters", Data.MattersBuffer);

		// Parse funds
		ArrayList<String> funds = new ArrayList<>();
		for(i = 0; i < Data.NFun; i++)funds.add(Data.Funds.get(i).Name);
		bundle.putStringArrayList("funds", funds);
		
		// Default fund index
		for(i = 0; i < funds.size(); i++)if(funds.get(i).compareToIgnoreCase(Data.FileData.DefFund) == 0){
			bundle.putInt("defaultfundindex", i);
			break;}
		
		// Parse categories
		ArrayList<String> categories = new ArrayList<>();
		categories.add("-");
		for(i = 0; i < Data.NCat; i++)categories.add(Data.CategoryDB.get(i).Name);
		bundle.putStringArrayList("categories", categories);
		intent.putExtras(bundle);
		
		RequestCode = Op;
		mLauncher.launch(intent);
	}

	public void profitClick(@SuppressWarnings("unused") View view){
		operation(1);
	}

	public void expenseClick(@SuppressWarnings("unused") View view){
		operation(2);
	}

	//@Override
    public void my_onActivityResult(int requestCode,int resultCode, Intent data)
    {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode != RESULT_OK) return;
		Bundle bundle = null;
		if(requestCode != 100) bundle = data.getExtras();
		
		int IDef = -1, IExi = -1, c_id
			, i;
		double value, Fon = -1, Def = -1, Val, Tot1, Fou = -1
			, Tot;
		String obj, per;
		GregorianCalendar Alm;
		
		switch(requestCode){
			case 100:	// Browse document
				document = Opts.getString("GDDoc", "NULL");
				if(! document.isEmpty()) openDocument();
				break;
	
			case 1:	// Gain
			case 2:	// Expense
				omb_library.appContext = getApplicationContext();
				
				int cat;
				String Fund = null, OpValue;

				long currencyIndex;
				double currencyRate;
				String currencySymbol;

				String SelFund = bundle.getString("fund");
				Val = bundle.getDouble("value");

				currencyIndex = bundle.getLong("currency", -1);
				currencyRate = bundle.getDouble("rate", 1);
				currencySymbol = bundle.getString("symbol", "");

				if(currencyIndex == 1)
					Val *= currencyRate;

				OpValue = omb_library.FormDigits(Val, true);
				for(i = 0; i < Data.NFun; i++)if(Data.Funds.get(i).Name.compareToIgnoreCase(SelFund) == 0){
					Fund = Data.Funds.get(i).Name;
					Fon = Data.Funds.get(i).Value;
					break;}
				Tot = Data.getTot(omb35core.TTypeVal.tvFou);
				if(Fon == -1 || Tot == -1){
					omb_library.Error(10, "");
					return;}

				int catIndex = bundle.getInt("category");
				if(catIndex > 0)
					cat = Data.CategoryDB.get(catIndex - 1).Id;
				else cat = -1;

				switch(requestCode){
					case 1:
						if(LocationPermission)
							Data.AddOperation(Data.Day, new GregorianCalendar(),
									omb35core.TOpType.toGain, OpValue, bundle.getString("matter"),
									cat, -1, true,
									LastLatitude, LastLongitude,
									currencyIndex, currencyRate, currencySymbol);
						else
							Data.AddOperation(Data.Day, new GregorianCalendar(),
									omb35core.TOpType.toGain, OpValue, bundle.getString("matter"),
									cat, -1, false,
									-1, -1,
									currencyIndex, currencyRate, currencySymbol);
						for(i = 0; i < Data.NFun; i++)if(Data.Funds.get(i).Name.compareTo(Fund) == 0){
							Data.Funds.get(i).Value += Val;
							Data.changeFundValue(omb35core.TTypeVal.tvFou, Data.Funds.get(i).Id, Data.Funds.get(i).Value);
							break;}
						break;
					case 2:
						if(Val > Fon){
							omb_library.Error(13, "");
							return;}
						String MatValue = bundle.getString("matter");
						if(LocationPermission)
							Data.AddOperation(Data.Day, new GregorianCalendar(),
									omb35core.TOpType.toExpe, OpValue, MatValue,
									cat, -1, true,
									LastLatitude, LastLongitude,
									currencyIndex, currencyRate, currencySymbol);
						else
							Data.AddOperation(Data.Day, new GregorianCalendar(),
									omb35core.TOpType.toExpe, OpValue, MatValue,
									cat, -1, false,
									-1, -1,
									currencyIndex, currencyRate, currencySymbol);
						for(i = 0; i < Data.NFun; i++)if(Data.Funds.get(i).Name.compareTo(Fund) == 0){
							Data.Funds.get(i).Value -= Val;
							Data.changeFundValue(omb35core.TTypeVal.tvFou, Data.Funds.get(i).Id, Data.Funds.get(i).Value);
							break;}
						if((Fon - Val == 0) && (! Fund.equals(Data.FileData.DefFund))){
							String Msg = String.format(getResources().getString(R.string.fund_exhaust), SelFund);
							tmp_string = Fund;
							
					 		AlertDialog.Builder alert = new AlertDialog.Builder(this, R.style.ombDialogTheme);

							alert.setTitle(getResources().getString(R.string.app_name));
							alert.setMessage(Msg);

							alert.setPositiveButton(getResources().getString(android.R.string.ok), (dialog, whichButton) -> {
								for(int i1 = 0; i1 < Data.NFun; i1++)if(Data.Funds.get(i1).Name.compareTo(tmp_string) == 0){
									Data.delValue(omb35core.TTypeVal.tvFou, i1);
									break;}
						  });

							alert.setNegativeButton(getResources().getString(R.string.dialog_no), (dialog, whichButton) -> {
							  // Canceled.
							  //return false;
							});
							
							alert.show();

						}
						Remove_Obtained_Shopping_Item(MatValue.toLowerCase());
				}
				displayView(R.id.nav_dashboard);

				break;
			
			case 3: // Categories edited
				if(bundle.getBoolean("modified")){
					ArrayList<String> CategoryList = bundle.getStringArrayList("categories");
					ArrayList<Integer> iconList = bundle.getIntegerArrayList("icons");

					boolean customIconsAdded = bundle.getBoolean("custom_added");
					if(customIconsAdded){

						ArrayList<String> iconImages = bundle.getStringArrayList("custom_files");

						boolean newTable = false;
						if(! Data.tableExists(Data.database,"CustomIcons")){
							newTable = true;
							Data.database.execSQL(constants.cs_customicons);
						}
						for(i = 0; i < iconImages.size(); i++){
							ContentValues cv = new ContentValues();

							if(newTable)
								cv.put("id", 100);

							cv.put("image", iconImages.get(i));
							Data.database.insert("CustomIcons", null, cv);
						}

						customIcons = getCustomIcons();

					}

					Data.updateCategories(CategoryList, iconList);
					Data.FileData.Modified = true;
					displayView(fragmentIndex);
				}
				break;
			
			case 4:  // Add fund
				Def = 0;
				String fund = bundle.getString("fund");
				value = bundle.getDouble("value");
				
				for(i = 0; i < Data.NFun ; i++)if(Data.Funds.get(i).Name.compareToIgnoreCase(fund) == 0){
					omb_library.Error(11, fund);
					return;
				}
				else if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0)Def = Data.Funds.get(i).Value;

				if(value > Def){
					omb_library.Error(12, "");
					return;}

				Data.setDefaultFundValue(Def - value);

				if(! Data.addValue(omb35core.TTypeVal.tvFou, -1, fund, value, -1)){
					omb_library.Error(10, "");
					return;}
				displayView(R.id.nav_funds);
				break;
				
			case 5:	// add credit
				String credit = bundle.getString("credit");
				Val = bundle.getDouble("value");
				for(i = 0; i < Data.NFun ; i++)
					if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
						IDef = i;
						Def = Data.Funds.get(i).Value;
						break;}
				Tot = Data.getTot(omb35core.TTypeVal.tvFou);
				Tot1 = Data.getTot(omb35core.TTypeVal.tvCre);
				if((Def == -1) || (Tot == -1) || (Tot1 == -1)){
					omb_library.Error(18, "");
					return;}
				if(Def < Val){
					omb_library.Error(19, "");
					return;}

				c_id = getContactIndex(bundle);

				Data.addValue(omb35core.TTypeVal.tvCre, -1, credit, Val, c_id);

				if(! bundle.getBoolean("old")) {
					Data.setDefaultFundValue(Data.Funds.get(IDef).Value - Val);
					if (LocationPermission)
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toSetCre, omb_library.FormDigits(Val, true),
								credit, -1, c_id, true, LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toSetCre, omb_library.FormDigits(Val, true),
								credit, -1, c_id, false, -1, -1,
								-1, 1, "");
				}
				displayView(R.id.nav_credits);
				break;
			
			case 6:	// remove credit
				for(i = 0; i < Data.NFun; i++)
					if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
						IDef = i;
						Def = Data.Funds.get(i).Value;
						break;}
				Tot = Data.getTot(omb35core.TTypeVal.tvFou);
				for(i = 0; i < Data.NCre; i++)
					if(Data.Credits.get(i).Name.compareToIgnoreCase(Data.Credits.get(CreFragment.credit_pos).Name) == 0){
						IExi = Data.Credits.get(i).Id;
						Fou = Data.Credits.get(i).Value;
						break;}
				Tot1 = Data.getTot(omb35core.TTypeVal.tvCre);
				if((Def == -1) || (Tot == -1) || (Fou == -1) || (Tot1 == -1)){
					omb_library.Error(18, "");
					return;}
				if(bundle.getBoolean("total")){
					if(LocationPermission)
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toRemCre, omb_library.FormDigits(Fou, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toRemCre, omb_library.FormDigits(Fou, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.delValue(omb35core.TTypeVal.tvCre, IExi);
					Data.setDefaultFundValue(Data.Funds.get(IDef).Value + Fou);
				}
				else{
					double Par = bundle.getDouble("value");
					if(Par > Fou){
						omb_library.Error(21, getResources().getString(R.string.credit_term));
						return;}
					if(LocationPermission)
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toRemCre, omb_library.FormDigits(Par, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, true,
								LastLatitude, LastLatitude,
								-1, 1, "");
					else
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toRemCre, omb_library.FormDigits(Par, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.changeFundValue(omb35core.TTypeVal.tvCre, IExi, Fou - Par);
					Data.setDefaultFundValue(Data.Funds.get(IDef).Value + Par);
				}
				Data.parseDatabase();
				//showF();
				displayView(R.id.nav_credits);
				break;
				
			case 7:	// Remit credit
				int ICre = -1;
				double Cre = -1;
				for(i = 0; i < Data.NCre; i++)
					if(Data.Credits.get(i).Name.compareToIgnoreCase(Data.Credits.get(CreFragment.credit_pos).Name) == 0){
						ICre = Data.Credits.get(i).Id;
						Cre = Data.Credits.get(i).Value;
						break;}
				Tot = Data.getTot(omb35core.TTypeVal.tvCre);
				if(Cre == -1 || Tot == -1){
					omb_library.Error(10, "");
					return;}
				if(bundle.getBoolean("total")){
					if(LocationPermission)
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toConCre, omb_library.FormDigits(Cre, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toConCre, omb_library.FormDigits(Cre, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.delValue(omb35core.TTypeVal.tvCre, ICre);}
				else{
					double Par = bundle.getDouble("value");
					if(Par > Cre){
						omb_library.Error(21, getResources().getString(R.string.credit_term));
						return;}
					if(LocationPermission)
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toConCre, omb_library.FormDigits(Par, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toConCre, omb_library.FormDigits(Par, true),
								Data.Credits.get(CreFragment.credit_pos).Name, -1,
								Data.Credits.get(CreFragment.credit_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.changeFundValue(omb35core.TTypeVal.tvCre, ICre, Cre - Par);
				}
				Data.parseDatabase();
				displayView(R.id.nav_credits);
				break;
				
			case 8:	// add debt
				String debt = bundle.getString("debt");
				Val = bundle.getDouble("value");
				for(i = 0; i < Data.NFun; i++)
					if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
						IDef = i;
						Def = Data.Funds.get(i).Value;
						break;}
				Tot = Data.getTot(omb35core.TTypeVal.tvFou);
				Tot1 = Data.getTot(omb35core.TTypeVal.tvDeb);
				if((Def == -1) || (Tot == -1) || (Tot1 == -1)){
					omb_library.Error(18, "");
					return;}

				c_id = getContactIndex(bundle);

				Data.addValue(omb35core.TTypeVal.tvDeb, -1, debt, Val, c_id);

				if(! bundle.getBoolean("old")) {
					Data.setDefaultFundValue(Data.Funds.get(IDef).Value + Val);
					if (LocationPermission)
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toSetDeb, omb_library.FormDigits(Val, true),
								debt, -1, c_id, true, LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toSetDeb, omb_library.FormDigits(Val, true),
								debt, -1, c_id, false, -1, -1,
								-1, 1, "");
				}
				displayView(R.id.nav_debts);
				break;
			
			case 9:	// remove debt
				for(i = 0; i < Data.NFun; i++)
					if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
						IDef = i;
						Def = Data.Funds.get(i).Value;
						break;}
				Tot = Data.getTot(omb35core.TTypeVal.tvFou);
				for(i = 0; i < Data.NDeb; i++)
					if(Data.Debts.get(i).Name.compareToIgnoreCase(Data.Debts.get(DebFragment.debt_pos).Name) == 0){
						IExi = Data.Debts.get(i).Id;
						Fou = Data.Debts.get(i).Value;
						break;}
				Tot1 = Data.getTot(omb35core.TTypeVal.tvDeb);
				if((Def == -1) || (Tot == -1) || (Fou == -1) || (Tot1 == -1)){
					omb_library.Error(18, "");
					return;}
				if(bundle.getBoolean("total")){
					if(Fou > Def){
						omb_library.Error(24, "");
						return;}
					if(LocationPermission)
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toRemDeb, omb_library.FormDigits(Fou, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
				    else
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toRemDeb, omb_library.FormDigits(Fou, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.delValue(omb35core.TTypeVal.tvDeb, IExi);
					Data.setDefaultFundValue(Data.Funds.get(IDef).Value - Fou);
				}
				else{
					double Par = bundle.getDouble("value");
					if(Par > Fou){
						omb_library.Error(21, getResources().getString(R.string.debt_term));
						return;}
					if(Par > Def){
						omb_library.Error(24, "");
						return;}
					if(LocationPermission)
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toRemDeb, omb_library.FormDigits(Par, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toRemDeb, omb_library.FormDigits(Par, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.changeFundValue(omb35core.TTypeVal.tvDeb, IExi, Fou - Par);
					Data.setDefaultFundValue(Data.Funds.get(IDef).Value - Par);
				}
				Data.parseDatabase();
				displayView(R.id.nav_debts);
				break;
				
			case 10:	// Remit debt
				int IDeb = -1;
				double Deb = -1;
				for(i = 0; i < Data.NDeb; i++)
					if(Data.Debts.get(i).Name.compareToIgnoreCase(Data.Debts.get(DebFragment.debt_pos).Name) == 0){
						IDeb = Data.Debts.get(i).Id;
						Deb = Data.Debts.get(i).Value;
						break;}
				Tot = Data.getTot(omb35core.TTypeVal.tvDeb);
				if(Deb == -1 || Tot == -1){
					omb_library.Error(10, "");
					return;}
				if(bundle.getBoolean("total")){
					if(LocationPermission)
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toConDeb, omb_library.FormDigits(Deb, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toConDeb, omb_library.FormDigits(Deb, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.delValue(omb35core.TTypeVal.tvDeb, IDeb);}
				else{
					double Par = bundle.getDouble("value");
					if(Par > Deb){
						omb_library.Error(21, getResources().getString(R.string.debt_term));
						return;}
					if(LocationPermission)
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toConDeb, omb_library.FormDigits(Par, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, true,
								LastLatitude, LastLongitude,
								-1, 1, "");
					else
						Data.AddOperation(Data.Day, new GregorianCalendar(),
								omb35core.TOpType.toConDeb, omb_library.FormDigits(Par, true),
								Data.Debts.get(DebFragment.debt_pos).Name, -1,
								Data.Debts.get(DebFragment.debt_pos).ContactIndex, false,
								-1, -1,
								-1, 1, "");
					Data.changeFundValue(omb35core.TTypeVal.tvDeb, IDeb, Deb - Par);
				}
				Data.parseDatabase();
				displayView(R.id.nav_debts);
				break;
				
			case 11:	// Get object
            case 12:	// Give object
                omb35core.TOpType objType;
                if(requestCode == 11) objType = omb35core.TOpType.toGetObj;
                else objType = omb35core.TOpType.toGivObj;

				c_id = getContactIndex(bundle);

				obj = bundle.getString("object");
				if(LocationPermission)
					Data.AddOperation(Data.Day, new GregorianCalendar(), objType, obj,
						bundle.getString("person"), -1, c_id, true, LastLatitude,
						LastLongitude,
							-1, 1, "");
				else
					Data.AddOperation(Data.Day, new GregorianCalendar(), objType, obj,
							bundle.getString("person"), -1, c_id, false, -1,
							-1,
							-1, 1, "");
				if(requestCode == 11) Remove_Obtained_Shopping_Item(obj.toLowerCase());
				displayView(R.id.nav_dashboard);
				break;

			case 13:	// Lend object
				c_id = getContactIndex(bundle);
				obj = bundle.getString("object");
				per = bundle.getString("person");
				Alm = (GregorianCalendar) GregorianCalendar.getInstance();
				Alm.set(bundle.getInt("year"), bundle.getInt("month"), bundle.getInt("day"));
				Data.addObject(omb35core.TObjType.toPre, -1, per, obj, Alm, c_id);
				if(LocationPermission)
					Data.AddOperation(Data.Day, new GregorianCalendar(),
							omb35core.TOpType.toLenObj, obj, per, -1, c_id, true,
							LastLatitude, LastLongitude,
							-1, 1, "");
				else
					Data.AddOperation(Data.Day, new GregorianCalendar(),
							omb35core.TOpType.toLenObj, obj, per, -1, c_id, false,
							-1, -1,
							-1, 1, "");
				displayView(R.id.nav_objects);
				break;

			case 14:	// Borrow object
				c_id = getContactIndex(bundle);
				obj = bundle.getString("object");
				per = bundle.getString("person");
				Alm = (GregorianCalendar) GregorianCalendar.getInstance();
				Alm.set(bundle.getInt("year"), bundle.getInt("month"), bundle.getInt("day"));
				Data.addObject(omb35core.TObjType.toInP, -1, per, obj, Alm, c_id);
				if(LocationPermission)
					Data.AddOperation(Data.Day, new GregorianCalendar(),
							omb35core.TOpType.toBorObj, obj, per, -1, c_id, true,
							LastLatitude, LastLongitude,
							-1, 1, "");
				else
					Data.AddOperation(Data.Day, new GregorianCalendar(),
							omb35core.TOpType.toBorObj, obj, per, -1, c_id, false,
							-1, -1,
							-1, 1, "");
				displayView(R.id.nav_objects);
				break;

			case 15:	// Add shop item
				obj = bundle.getString("object");
				boolean item_new = true;
				for(i = 0; i < Data.NSho; i++){
					if(Data.ShopItems.get(i).Name.compareToIgnoreCase(obj) == 0){
						omb_library.Error(48, obj);
						item_new = false;
						break;
					}
				}
				if(item_new){
					Alm = (GregorianCalendar) GregorianCalendar.getInstance();
					Alm.set(bundle.getInt("year"), bundle.getInt("month"), bundle.getInt("day"));
					Data.addShopItem(-1, obj, Alm);
					displayView(R.id.nav_shoplist);
				}
				break;

			case 16:	// Edit fund groups
				boolean Changed = false;

				Cursor GroupTable = Data.database.query("select * from FundGroups where owner = '-1';", null);
				int GTC = GroupTable.getCount();

				if(bundle.getBoolean("addedormodifiedgroups", false)){
					ArrayList<String> groupList = bundle.getStringArrayList("groups");
					ArrayList<String> fundNames = bundle.getStringArrayList("fundnames");
					ArrayList<Integer> owners = bundle.getIntegerArrayList("owners");
					ArrayList<String> ownerNames = bundle.getStringArrayList("ownernames");

					boolean GroupFound;
					int GroupID = -1;
					for(i = 0; i < groupList.size(); i++){
						GroupFound = false;
						String Group = groupList.get(i);
						for(int j = 0; j < GroupTable.getCount(); j++){
							GroupTable.moveToPosition(j);
							if(Group.equalsIgnoreCase(GroupTable.getString(1))){
								GroupFound = true;
								GroupID = GroupTable.getInt(0);
								break;
							}
						}
						if(! GroupFound){
							// Add Group
							ContentValues cv = new ContentValues();
							cv.clear();
							cv.put("name", Group);
							cv.put("owner", -1);
							cv.put("child", -1);
							Data.database.insert("FundGroups", null, cv);

							// Update group table
							GroupTable = Data.database.query("select * from FundGroups where owner = '-1';", null);
						}

						// Add Children
						boolean ChildFound;
						String ChildName;
						Cursor ChildTable = Data.database.query("select * from FundGroups where owner > '-1';", null);
						int CTC = ChildTable.getCount();
//						Cursor FundTable = Data.database.query("select * from Funds;", null);
						Cursor FundTable = Data.database.query("select * from Funds where name != '" + Data.FileData.DefFund + "';", null);
						int FTC = FundTable.getCount();
						for(int k = 0; k < FTC; k++){
							ChildName =  fundNames.get(k);
							ChildFound = false;
							for(int l = 0; l < CTC; l++){
								ChildTable.moveToPosition(l);
								if(ChildName.equalsIgnoreCase(ChildTable.getString(1))){
									ChildFound = true;
									int ChildID = ChildTable.getInt(3);
									int OwnerID = owners.get(k);
									if(OwnerID > -1){
										if((ChildID > -1) && (GroupID == OwnerID)) {
											ContentValues cv = new ContentValues();
											cv.clear();
											cv.put("owner", OwnerID);
											Data.database.update("FundGroups", cv, "child = " + String.format(Locale.US, "%d", ChildID), null);
										}
									}
									else{
										if((ChildID > -1) && ((Group.equalsIgnoreCase(ownerNames.get(k))) ||
												(ownerNames.get(k).isEmpty()))){
											Data.database.delete("FundGroups", "child=?", new String[]{Integer.toString(ChildID)});
											ChildTable = Data.database.query("select * from FundGroups where owner > '-1';", null);
											CTC = ChildTable.getCount();
										}
									}
									break;
								}
							}
							if((! ChildFound) && (ownerNames.get(k).equalsIgnoreCase(Group))){

								GroupID = -1;
								int ChildID = -1;
								for(int m = 0; m < GroupTable.getCount(); m++){
									GroupTable.moveToPosition(m);
									String GroupName = GroupTable.getString(1);
									if(GroupName.equalsIgnoreCase(Group)){
										GroupID = GroupTable.getInt(0);
										break;
									}
								}
								for(int m = 0; m < FTC; m++){
									FundTable.moveToPosition(m);
									if(ChildName.equalsIgnoreCase(FundTable.getString(1))){
										ChildID = FundTable.getInt(0);
										break;
									}
								}
								if((GroupID > -1) && (ChildID > -1)){
									ContentValues cv = new ContentValues();
									cv.clear();
									cv.put("name", ChildName);
									cv.put("owner", GroupID);
									cv.put("child", ChildID);
									Data.database.insert("FundGroups", null, cv);
								}
							}
						}

					}

					Changed = true;
				}

				if(bundle.getBoolean("deletedgroups", false)){
					ArrayList<String> groupList = bundle.getStringArrayList("groups");

					boolean Found;
					int GroupID;
					String GroupName;
					for(i = 0; i < GTC; i++){
						Found = false;
						GroupTable.moveToPosition(i);
						GroupID = GroupTable.getInt(0);
						GroupName = GroupTable.getString(1);
						for(int j = 0; j < groupList.size(); j++)
							if(GroupName.equalsIgnoreCase(groupList.get(j))){
								Found = true;
								break;
							}
						if(! Found)
							Data.database.delete("FundGroups", "id=?", new String[]{Integer.toString(GroupID)});
					}

					Changed = true;
				}

				if(Changed){
					Data.FileData.Modified = true;
//					ShowF();
					displayView(R.id.nav_funds);
				}

				break;

			case MY_PERMISSIONS_REQUEST_OVERLAY:
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					if (!Settings.canDrawOverlays(getApplicationContext())) {
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.permission_boot_no), Toast.LENGTH_LONG).show();
					}
				}
				break;

			default:
		}

    }

	private void EditCategoriesClick(){
		Bundle bundle = new Bundle();
		
		Intent intent = new Intent(this, EditCategories.class);

		ArrayList<String> cats = new ArrayList<>();
		ArrayList<Integer> iconIndexes = new ArrayList<>();
		for(int i = 0; i < Data.NCat; i++){
			cats.add(Data.CategoryDB.get(i).Name);
			iconIndexes.add(Data.CategoryDB.get(i).iconId);
		}
		bundle.putStringArrayList("categories", cats);
		bundle.putIntegerArrayList("icons", iconIndexes);

		intent.putExtras(bundle);

		setCustomIcons(customIcons);

		RequestCode = 3;
		mLauncher.launch(intent);

	}

	private void FundOperation(int Op){
		if(Op == 0 || Op > 3) return;
		int i;
		double Fon;
		if(Op > 1 && Data.NFun < 2){
			if(Op == 2) omb_library.Error(8, null);
			else omb_library.Error(9, null);
			return;}

		switch(Op){
			case 1:
				RequestCode = 4;
				mLauncher.launch(new Intent(this, fund_add.class));
				break;
			case 2:
				Fon = Data.Funds.get(FundsFragment.fundPositions[FunFragment.fund_pos]).Value;
				int id = Data.Funds.get(FundsFragment.fundPositions[FunFragment.fund_pos]).Id;

				for(i = 0; i < Data.NFun; i++)if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
					Data.setDefaultFundValue(Data.Funds.get(i).Value + Fon);
					break;}

				Data.delValue(omb35core.TTypeVal.tvFou, id);

				displayView(R.id.nav_funds);
				break;
			case 3:
				AlertDialog.Builder alert = new AlertDialog.Builder(this, R.style.ombDialogTheme);
				alert.setTitle(getResources().getString(R.string.app_name));
				alert.setMessage(String.format(this.getResources().getString(R.string.fund_reset),
					Data.Funds.get(FundsFragment.fundPositions[FunFragment.fund_pos]).Name));

				// Set an EditText view to get user input
				final EditText input = new EditText(this);

				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
					input.setTextColor(getResources().getColor(R.color.white, this.getTheme()));

				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
					input.setKeyListener(new DigitsKeyListener(Locale.ENGLISH, false, true));
				else input.setKeyListener(new DigitsKeyListener(false, true));
				input.setText(omb_library.FormDigits(Data.Funds.get(FundsFragment.fundPositions[FunFragment.fund_pos]).Value, true));
				alert.setView(input);

				alert.setPositiveButton(getResources().getString(android.R.string.ok), (dialog, whichButton) -> {

					double Fon1 = Data.Funds.get(FundsFragment.fundPositions[FunFragment.fund_pos]).Value;
					double NFun = Double.parseDouble(input.getText().toString());
					double Tot = Data.getTot(omb35core.TTypeVal.tvFou);

					for(int i1 = 0; i1 < Data.NFun; i1++)if(Data.Funds.get(i1).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
						double Def = Data.Funds.get(i1).Value;
						if(NFun < Fon1){
							Data.setDefaultFundValue(Def + Fon1 - NFun);
						}
						else{
							if(Tot < (NFun - Fon1)){
								omb_library.Error(13, "");
								return;}
							if(Def < (NFun - Fon1)){
								omb_library.Error(24, "");
								return;}
							Data.setDefaultFundValue(Def - NFun + Fon1);
						}}
					else if(i1 == FundsFragment.fundPositions[FunFragment.fund_pos]) {
						Data.Funds.get(i1).Value = NFun;
						Data.changeFundValue(omb35core.TTypeVal.tvFou, Data.Funds.get(i1).Id, Data.Funds.get(i1).Value);
					}
					Data.FileData.Modified = true;
					displayView(R.id.nav_funds);
			});

			alert.setNegativeButton(getResources().getString(R.string.dialog_cancel), (dialog, whichButton) -> {
			  // Canceled.
			});
			
			alert.show();

			break;
		}
		
	}

	public void NewFundClick(@SuppressWarnings("unused") View view){
		FundOperation(1);}

	public void RemoveFundClick(@SuppressWarnings("unused") View view){
		FundOperation(2);}

	public void ResetFundClick(@SuppressWarnings("unused") View view){
		FundOperation(3);}

	private void SetTotalClick(){
		AlertDialog.Builder alert = new AlertDialog.Builder(this, R.style.ombDialogTheme);
		alert.setTitle(getResources().getString(R.string.app_name));
		alert.setMessage(this.getResources().getString(R.string.fund_settotal));

		// Set an EditText view to get user input
		final EditText input = new EditText(this);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			input.setTextColor(getResources().getColor(R.color.white, this.getTheme()));

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
			input.setKeyListener(new DigitsKeyListener(Locale.ENGLISH, false, true));
		else input.setKeyListener(new DigitsKeyListener(false, true));
		input.setText(omb_library.FormDigits(Data.getTot(omb35core.TTypeVal.tvFou), true));
		alert.setView(input);

		alert.setPositiveButton(getResources().getString(android.R.string.ok), (dialog, whichButton) -> {

			int i, IP = -1;
			double OldTot, Tot, Pre = -1;	// OldTot: previous total value
											// Tot: new total value
											// Pre: Default fund value
			OldTot = Data.getTot(omb35core.TTypeVal.tvFou);
			Tot = Double.parseDouble(input.getText().toString());

			omb_library.appContext = getApplicationContext();

			if(OldTot > Tot){
				omb_library.Error(17, "");
				return;}
			for(i = 0; i < Data.NFun; i++)if(Data.Funds.get(i).Name.compareToIgnoreCase(Data.FileData.DefFund) == 0){
				Pre = Data.Funds.get(i).Value;
				IP = i;
				break;}
			if(IP > -1){
				Data.Funds.get(IP).Value = Pre + Tot - OldTot;
				Data.changeFundValue(omb35core.TTypeVal.tvFou, Data.Funds.get(IP).Id, Data.Funds.get(IP).Value);
			}
			else{
				String def = getResources().getString(R.string.default_name);
				Data.addValue(omb35core.TTypeVal.tvFou, -1, def, Tot, -1);
				//Data.FileData.DefFund = def;
				Data.setDefaultFund(def);
			}
			Data.FileData.Modified = true;
			displayView(fragmentIndex);
		});

		alert.setNegativeButton(getResources().getString(R.string.dialog_cancel), (dialog, whichButton) -> {
		  // Canceled.
		  //return false;
		});

		alert.show();
		
	}

	private void DefaultFundClick(){
		int i;

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.ombDialogTheme);
        builder.setTitle(this.getResources().getString(R.string.fund_default_choose));
        
        ArrayList<String> list = new ArrayList<>();
        for(i = 0; i < Data.NFun; i++) list.add(Data.Funds.get(i).Name);
        final CharSequence[] items = list.toArray(new CharSequence[0]);

        builder.setItems(items, (dialog, item) -> Data.setDefaultFund(items[item].toString()));
        AlertDialog alert = builder.create();
        alert.show();

	}

	public void NewCreditClick(@SuppressWarnings("unused") View view){
		RequestCode = 5;
		mLauncher.launch(new Intent(this, credit_add.class));
	}

	public void RemoveCreditClick(@SuppressWarnings("unused") View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 0);
		bundle.putDouble("max_value", Data.Credits.get(CreFragment.credit_pos).Value);
		Intent intent = new Intent(this, RemoveCreditDebt.class);
		intent.putExtras(bundle);		
		RequestCode = 6;
		mLauncher.launch(intent);
	}
	
	public void RemitCreditClick(@SuppressWarnings("unused") View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 1);
		bundle.putDouble("max_value", Data.Credits.get(CreFragment.credit_pos).Value);
		Intent intent = new Intent(this, RemoveCreditDebt.class);
		intent.putExtras(bundle);		
		RequestCode = 7;
		mLauncher.launch(intent);
	}
	
	public void NewDebtClick(@SuppressWarnings("unused") View view){
		RequestCode = 8;
		mLauncher.launch(new Intent(this, debt_add.class));
	}

	public void RemoveDebtClick(@SuppressWarnings("unused") View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 2);
		bundle.putDouble("max_value", Data.Debts.get(DebFragment.debt_pos).Value);
		Intent intent = new Intent(this, RemoveCreditDebt.class);
		intent.putExtras(bundle);		
		RequestCode = 9;
		mLauncher.launch(intent);
	}
	
	public void RemitDebtClick(@SuppressWarnings("unused") View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 3);
		bundle.putDouble("max_value", Data.Debts.get(DebFragment.debt_pos).Value);
		Intent intent = new Intent(this, RemoveCreditDebt.class);
		intent.putExtras(bundle);		
		RequestCode = 10;
		mLauncher.launch(intent);
	}

	public void receivedClick(@SuppressWarnings("unused") View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 1);
		Intent intent = new Intent(this, object_add.class);
		intent.putExtras(bundle);
		RequestCode = 11;
		mLauncher.launch(intent);
	}

	public void giftedClick(@SuppressWarnings("unused") View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 2);
		Intent intent = new Intent(this, object_add.class);
		intent.putExtras(bundle);
		RequestCode = 12;
		mLauncher.launch(intent);
	}

	public void LendClick(@SuppressWarnings("unused") View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 3);
		Intent intent = new Intent(this, object_add.class);
		intent.putExtras(bundle);		
		RequestCode = 13;
		mLauncher.launch(intent);
	}

	public void getBackClick(@SuppressWarnings("unused") View view){
		String obj = null;
		String per = null;
		for(int i = 0; i < Data.NLen; i++){
			if(Data.Lent.get(i).Id == ObjFragment.lent_pos){
				obj = Data.Lent.get(i).Object;
				per = Data.Lent.get(i).Name;
				break;
			}
		}
		Data.delObject(omb35core.TObjType.toPre, ObjFragment.lent_pos);
		if(LocationPermission)
			Data.AddOperation(Data.Day, new GregorianCalendar(), omb35core.TOpType.toBakObj, obj, per,
					-1, -1, true, LastLatitude, LastLongitude,
					-1, 1, "");
		else
			Data.AddOperation(Data.Day, new GregorianCalendar(), omb35core.TOpType.toBakObj, obj, per,
				-1, -1, false, -1, -1,
					-1, 1, "");
		displayView(R.id.nav_objects);
	}

	public void BorrowClick(@SuppressWarnings("unused") View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 4);
		Intent intent = new Intent(this, object_add.class);
		intent.putExtras(bundle);		
		RequestCode = 14;
		mLauncher.launch(intent);
	}

	public void giveBackClick(@SuppressWarnings("unused") View view){
		String obj = null;
		String per = null;
		for(int i = 0; i < Data.NBor; i++){
			if(Data.Borrowed.get(i).Id == ObjFragment.borrowed_pos){
				obj = Data.Borrowed.get(i).Object;
				per = Data.Borrowed.get(i).Name;
				break;
			}
		}
		Data.delObject(omb35core.TObjType.toInP, ObjFragment.borrowed_pos);
		if(LocationPermission)
			Data.AddOperation(Data.Day, new GregorianCalendar(), omb35core.TOpType.toRetObj, obj, per,
					-1, -1, true, LastLatitude, LastLongitude,
					-1, 1, "");
		else
			Data.AddOperation(Data.Day, new GregorianCalendar(), omb35core.TOpType.toRetObj, obj, per,
					-1, -1, false, -1, -1,
					-1, 1, "");
		displayView(R.id.nav_objects);
	}

	public void addShopItemClick(@SuppressWarnings("unused") View view){
		Bundle bundle = new Bundle();
		bundle.putInt("labels", 5);
		Intent intent = new Intent(this, object_add.class);
		intent.putExtras(bundle);		
		RequestCode = 15;
		mLauncher.launch(intent);
	}
	
	public void delShopItemClick(@SuppressWarnings("unused") View view){
		int id = Data.ShopItems.get(ShoFragment.shop_pos).Id;
		Data.delShopItem(id);
		displayView(R.id.nav_shoplist);
	}

	private void openDocument(){
		omb_library.appContext = getApplicationContext();

		File f = new File(document);
		if(f.exists()) {
			// Document memory-space creation
			Data = new omb35core(document, this);
			Data.frame = this;

		}
		else startActivity(new Intent(this, DocumentSelection.class));

	}
	
	private void Remove_Obtained_Shopping_Item(String obj){
		for(int i = 0; i < Data.NSho; i++){
			if(obj.compareToIgnoreCase(Data.ShopItems.get(i).Name) == 0){
				String Msg = String.format(getResources().getString(R.string.shoplist_bought), Data.ShopItems.get(i).Name);
				final int tmp_int = Data.ShopItems.get(i).Id;

		 		AlertDialog.Builder alert = new AlertDialog.Builder(this, R.style.ombDialogTheme);

				alert.setTitle(getResources().getString(R.string.app_name));
				alert.setMessage(Msg);

				alert.setPositiveButton(getResources().getString(android.R.string.ok), (dialog, whichButton) -> {
					Data.delShopItem(tmp_int);
					displayView(fragmentIndex);
				});

				alert.setNegativeButton(getResources().getString(R.string.dialog_no), (dialog, whichButton) -> {
				});
				
				alert.show();

			}
		}
		
	}

	private void alarm_notification(int kind, int index, String text)
	{
		Notification.Builder mBuilder;
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
			mBuilder =
		        new Notification.Builder(this, alarmId)
		        .setContentTitle(getResources().getString(R.string.alarm))
		        .setContentText(text)
		        //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), Notification.AUDIO_ATTRIBUTES_DEFAULT)
		    ;
		}
		else{
			mBuilder =
				new Notification.Builder(this)
					.setContentTitle(getResources().getString(R.string.alarm))
					.setContentText(text)
					.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
			;
		}
        switch (android.os.Build.VERSION.SDK_INT ) {
			case Build.VERSION_CODES.LOLLIPOP_MR1:
			case Build.VERSION_CODES.M:
				mBuilder.setColor(0xff35ff);
				mBuilder.setSmallIcon(R.drawable.ic_launcher_white);
				break;
			case Build.VERSION_CODES.N:
			case Build.VERSION_CODES.N_MR1:
			case Build.VERSION_CODES.O:
			case Build.VERSION_CODES.O_MR1:
			case Build.VERSION_CODES.P:
			case Build.VERSION_CODES.Q:
			case Build.VERSION_CODES.R:
			case Build.VERSION_CODES.S:
			case Build.VERSION_CODES.S_V2:
			case Build.VERSION_CODES.TIRAMISU:
			case Build.VERSION_CODES.UPSIDE_DOWN_CAKE:
				int Clr;
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
					Clr = getResources().getColor(R.color.green_dark, this.getTheme());
				else Clr = 0x803f00;
				mBuilder.setColor(/*0x803f00*/Clr);
				mBuilder.setSmallIcon(R.drawable.ic_launcher_brown);
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
					mBuilder.setChannelId(alarmId);
				break;
			default:
        		mBuilder.setSmallIcon(R.mipmap.ic_launcher);
		}

		// Creates an explicit intent for an Activity in your app
		Bundle bundle = new Bundle();
		Intent resultIntent = new Intent(this, alarm.class);
		bundle.putInt("kind", kind);
		bundle.putInt("index", index);
		bundle.putString("message", text);
		resultIntent.putExtras(bundle);
		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(MainActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		int flags;
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
			flags = PendingIntent.FLAG_UPDATE_CURRENT + PendingIntent.FLAG_IMMUTABLE;
		else
			flags = PendingIntent.FLAG_UPDATE_CURRENT;
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            /*PendingIntent.FLAG_UPDATE_CURRENT*/flags
		        );
		mBuilder.setContentIntent(resultPendingIntent);

		Notification notification = mBuilder.build();
		if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1 ) {
			@SuppressLint("DiscouragedApi") int smallIconViewId = getResources().getIdentifier("right_icon", "id", Objects.requireNonNull(android.R.class.getPackage()).getName());

		    if (smallIconViewId != 0) {
		        if (notification.contentIntent != null)
					notification.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
		
		        if (notification.headsUpContentView != null)
					notification.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
		
		        if (notification.bigContentView != null)
					notification.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
		    }
		}

		// mId allows you to update the notification later on.
		notificationManager.notify(10000, notification);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
			notificationManager.cancel(1);
	}
	
	private static class Task implements Runnable {
		// Weak reference: https://stackoverflow.com/a/27825703/1118383
		private final WeakReference<MainActivity> activityWeakRef;

    	public Task(MainActivity activityInstance){
			activityWeakRef = new WeakReference<>(activityInstance);
		}

		@Override
		public void run() {
			MainActivity act = activityWeakRef.get();
			if(act.HasAlarms){
				new Handler(Looper.getMainLooper()).postDelayed(() -> act.doChecks(false,
					-1, null), act.getResources().getInteger(R.integer.mainTimerInt));
			}
		}
	}

	private boolean doChecks(boolean auto, int kind, GregorianCalendar cal)
	{
		int result = Data.checkLentObjects();
		if(result >= 0){
			if(auto && (kind == 1))
			{
				Data.Lent.get(result).Alarm = cal;
				ContentValues cv = new ContentValues();
				cv.put("alarm", cal.getTimeInMillis() / 1000);
				Data.database.update("Loans", cv, "id = " + String.format(Locale.US, "%d", Data.Lent.get(result).Id), null);
			}
			else {
				alarm_notification(1, result,
						String.format(getResources().getString(R.string.alarm_detail_lent), Data.Lent.get(result).Object)
				);
				Data.database.close();
			}
			return true;
		}

		result = Data.checkBorrowedObjects();
		if(result >= 0){
			if(auto && (kind == 2)) {
				Data.Borrowed.get(result).Alarm = cal;
				ContentValues cv = new ContentValues();
				cv.put("alarm", cal.getTimeInMillis() / 1000);
				Data.database.update("Borrows", cv, "id = " + String.format(Locale.US, "%d", Data.Borrowed.get(result).Id), null);
			}
			else {
				alarm_notification(2, result,
						String.format(getResources().getString(R.string.alarm_detail_borrowed), Data.Borrowed.get(result).Object)
				);
				Data.database.close();
			}
			return true;
		}

		result = Data.checkShopList();
		if(result >= 0){
			if(auto && (kind == 3))
			{
				Data.ShopItems.get(result).Alarm = cal;
				ContentValues cv = new ContentValues();
				cv.put("alarm", cal.getTimeInMillis() / 1000);
				Data.database.update("Shoplist", cv, "id = " + String.format(Locale.US, "%d", Data.ShopItems.get(result).Id), null);
			}
			else {
				alarm_notification(3, result,
						String.format(getResources().getString(R.string.alarm_detail_shoplist), Data.ShopItems.get(result).Name)
				);
				Data.database.close();
			}
			return true;
		}

		return false;
	}

	public void updateObjectRecyclerItem(int item){
		int itemCount = ObjFragment.ov.getChildCount();
		int type = -1;
		recycler_adapter_objects.ObjectViewHolder holder;
		for(int i = 0; i < itemCount; i++){
			holder = (recycler_adapter_objects.ObjectViewHolder) ObjFragment.ov.findViewHolderForAdapterPosition(i);
			if(i == item){
				Objects.requireNonNull(holder).objectChecked.setChecked(true);
				type = ObjFragment.objects.get(item).type;
			}
			else Objects.requireNonNull(holder).objectChecked.setChecked(false);
		}

		boolean enableGiveBack = (type == 1);
		ObjFragment.btn_givebackObj.setEnabled(enableGiveBack);
		ObjFragment.btn_getbackObj.setEnabled(! enableGiveBack);
		if(type == 1){
			ObjFragment.borrowed_pos = ObjFragment.objects.get(item).id;
		}
		else{
			ObjFragment.lent_pos = ObjFragment.objects.get(item).id;
		}

	}

	public void updateCredDebRecyclerItem(int item){
		RecyclerView view;
		if (fragmentIndex == R.id.nav_debts) {
			view = DebFragment.dv;
		} else {
			view = CreFragment.cv;
		}

		int itemCount = view.getChildCount();
		Recycler_Adapter_CreditDebt.ObjectViewHolder holder;
		for(int i = 0; i < itemCount; i++){
			holder = (Recycler_Adapter_CreditDebt.ObjectViewHolder) view.findViewHolderForAdapterPosition(i);
			Objects.requireNonNull(holder).itemChecked.setChecked(i == item);
		}

		boolean enabled = (item >= 0);
		if (fragmentIndex == R.id.nav_debts) {
			DebFragment.btn_removeDebt.setEnabled(enabled);
			DebFragment.btn_remitDebt.setEnabled(enabled);
			if (item >= 0) DebFragment.debt_pos = item;
		} else {
			CreFragment.btn_removeCredit.setEnabled(enabled);
			CreFragment.btn_remitCredit.setEnabled(enabled);
			if (item >= 0) CreFragment.credit_pos = item;
		}

	}

	public void UpdateShoppingListRecyclerItem(int item){
		ShoFragment.shop_pos = item;
	}

	private int getContactIndex(Bundle bundle){
		String contact_id = bundle.getString("contact_id");
		int c_id = Integer.parseInt(contact_id);
		String contact_name;
		if(c_id > 0){
			contact_name = bundle.getString("contact_name");
			if(! Data.findContact(contact_id, contact_name)) c_id = -1;
		}
		return c_id;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
										   @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
				boolean canUseExternalStorage = grantResults.length > 0
					&& grantResults[0] == PackageManager.PERMISSION_GRANTED;

				if (!canUseExternalStorage) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.permission_write_needed), Toast.LENGTH_LONG).show();
					finish();
				}
				else {
					// user now provided permission

					//this.recreate();	// recreate() is not working
					Intent intent = this.getIntent();
					Intent sanitized = new  IntentSanitizer.Builder()
							.allowPackage("com.igisw.openmoneybox")
							//.allowData("com.igisw.openmoneybox")
							.allowType("text/plain")
							.build()
							.sanitizeByThrowing(intent);
					this.finish();
					startActivity(sanitized);
				}
				break;

			case ASK_LOCATION_PERMISSION:
				if (grantResults.length > 0) {
					boolean gpsPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
					boolean coarsePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;

					if ((gpsPermission || coarsePermission)) LocationPermission = true;
					else
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.locations_wont_store_warning), Toast.LENGTH_LONG).show();
				}
				break;

			case constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS:
				boolean canUseContacts = grantResults.length > 0
					&& grantResults[0] == PackageManager.PERMISSION_GRANTED;

				if (!canUseContacts) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.permission_contacts_no), Toast.LENGTH_LONG).show();
				}
				break;
		}
	}

	private void displayView(int viewId) {

		// lock orientation if needed to avoid change loss in the database
		// TODO (igor#1#): rework for tablets which have different natural orientation
		if(Data != null) {
			if (Data.FileData.Modified) {
				int orientation; // = this.getRequestedOrientation();
				int rotation;
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
					rotation = getDisplay().getRotation();
				else rotation = ((WindowManager) this.getSystemService(
					Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
				switch (rotation) {
					case Surface.ROTATION_90:
						orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
						break;
					case Surface.ROTATION_180:
						orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
						break;
					case Surface.ROTATION_270:
						orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
						break;
					case Surface.ROTATION_0:
					default:
						orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
						break;
				}

				this.setRequestedOrientation(orientation);
			} else this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}

		//Fragment fragment = null;
		String title = getString(R.string.app_name);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		if (viewId == R.id.nav_dashboard) {
			ArrayList<TCategorySummary> summary = new ArrayList<>();
			if ((Data != null) && Data.successfulKey) {
				// Top Categories

				TCategorySummary add;
				for (int i = 0; i < _OMB_TOPCATEGORIES_NUMBER; i++) {
					add = new TCategorySummary();
					add.Init = false;
					add.IconIndex = -1;
					add.Name = null;
					add.Value = 0;
					summary.add(add);
				}

				boolean foundAny;
				int summaryIndex, firstExpenseIndex = 0, Rows,
						parseId;
				double val, categoryTotal;
				String value;
				Cursor table;

				// Search biggest income category
				for (int i = -1; i <= Data.NCat; i++) {    // -1: unassigned category
					foundAny = false;
					categoryTotal = 0;

					if (i == -1) parseId = i;
					else parseId = i + 1;

					table = Data.database.query("Transactions", null,
						"cat_index = ? and isdate = 0 and type in ( 1, 2 )",
						new String[]{String.format(Locale.US, "%d", parseId)}, null,
						null, null);

					Rows = table.getCount();
					if (Rows > 0) {
						for (int j = 0; j < Rows; j++) {
							table.moveToPosition(j);
							value = table.getString(5);
							val = Double.parseDouble(value);

							if (table.getInt(4) == 1)
								categoryTotal += val;
							else
								categoryTotal -= val;
						}
						if (categoryTotal > 0) foundAny = true;

						if (foundAny) {
							if ((!summary.get(0).Init) || (summary.get(0).Value < categoryTotal)) {
								summary.get(0).Init = true;
								if (i < 1)
									summary.get(0).Name = getString(R.string.topcategories_none);
								else {
									for (int j = 0; j < Data.NCat; j++) {
										if (Data.CategoryDB.get(j).Id == parseId) {
											summary.get(0).Init = true;
											summary.get(0).Name = Data.CategoryDB.get(j).Name;
											summary.get(0).IconIndex = Data.CategoryDB.get(j).iconId;
											break;
										}
									}
								}
								summary.get(0).Value = categoryTotal;
							}

							firstExpenseIndex = 1;
						}
					}
				}

				// Search biggest expense categories
				for (int i = -1; i <= Data.NCat; i++) {    // -1: unassigned category
					foundAny = false;
					categoryTotal = 0;

					if (i == -1) parseId = i;
					else parseId = i + 1;

					table = Data.database.query("Transactions", null,
						"cat_index = ? and isdate = 0 and type in ( 1, 2 )",
						new String[]{String.format(Locale.US, "%d", parseId)}, null,
						null, null);

					Rows = table.getCount();
					if (Rows > 0) {
						for (int j = 0; j < Rows; j++) {
							table.moveToPosition(j);
							value = table.getString(5);
							val = Double.parseDouble(value);

							if (table.getInt(4) == 1)
								categoryTotal += val;
							else
								categoryTotal -= val;
						}
						if (categoryTotal < 0) foundAny = true;

						if (foundAny) {
							summaryIndex = -1;
							for (int j = firstExpenseIndex; j < _OMB_TOPCATEGORIES_NUMBER; j++) {
								if (!summary.get(j).Init) {
									summaryIndex = j;
									break;
								}
							}
							if (summaryIndex == -1) {
								double MaxSummary = summary.get(firstExpenseIndex).Value;
								summaryIndex = firstExpenseIndex;
								for (int j = firstExpenseIndex + 1; j < _OMB_TOPCATEGORIES_NUMBER; j++) {
									if (summary.get(j).Value > MaxSummary) {
										MaxSummary = summary.get(j).Value;
										summaryIndex = j;
									}
								}
							}

							if ((!summary.get(summaryIndex).Init) || (summary.get(summaryIndex).Value > categoryTotal)) {
								summary.get(summaryIndex).Init = true;
								if (i < 1)
									summary.get(summaryIndex).Name = getString(R.string.topcategories_none);
								else {
									for (int j = 0; j < Data.NCat; j++) {
										if (Data.CategoryDB.get(j).Id == parseId) {
											summary.get(summaryIndex).Init = true;
											summary.get(summaryIndex).Name = Data.CategoryDB.get(j).Name;
											summary.get(summaryIndex).IconIndex = Data.CategoryDB.get(j).iconId;
											break;
										}
									}
								}
								summary.get(summaryIndex).Value = categoryTotal;
							}
						}
					}
				}

				// Sort top categories by Value
				boolean TmpInit;
				String TmpName;
				double TmpValue;
				int MinIndex, TmpIndex;
				for (int i = 0; i < _OMB_TOPCATEGORIES_NUMBER; i++) {
					MinIndex = i;
					TmpValue = summary.get(i).Value;
					for (int j = i; j < _OMB_TOPCATEGORIES_NUMBER; j++) {
						if ((summary.get(j).Init) && (summary.get(j).Value < TmpValue)) {
							TmpValue = summary.get(j).Value;
							MinIndex = j;
						}
					}
					if (MinIndex != i) {
						TmpInit = summary.get(i).Init;
						TmpName = summary.get(i).Name;
						TmpValue = summary.get(i).Value;
						TmpIndex = summary.get(i).IconIndex;

						summary.get(i).Init = summary.get(MinIndex).Init;
						summary.get(i).Name = summary.get(MinIndex).Name;
						summary.get(i).Value = summary.get(MinIndex).Value;
						summary.get(i).IconIndex = summary.get(MinIndex).IconIndex;

						summary.get(MinIndex).Init = TmpInit;
						summary.get(MinIndex).Name = TmpName;
						summary.get(MinIndex).Value = TmpValue;
						summary.get(MinIndex).IconIndex = TmpIndex;
					}
				}

			}

			setCustomIcons(customIcons);
			DashboardFragment dashFragment = new DashboardFragment();

			if ((Data != null) && Data.successfulKey) {
				dashFragment.summary = summary;
			}

			ft.replace(R.id.content_frame, dashFragment);
			ft.commit();

			viewIsAtHome = true;
		} else if (viewId == R.id.nav_funds) {
			FunFragment = new FundsFragment();
			title = getString(R.string.funds);

			ft.replace(R.id.content_frame, FunFragment);
			ft.commit();

			viewIsAtHome = false;
		} else if (viewId == R.id.nav_credits) {
			CreFragment = new CreditsFragment();
			title = getString(R.string.credits);

			ft.replace(R.id.content_frame, CreFragment);
			ft.commit();

			viewIsAtHome = false;
		} else if (viewId == R.id.nav_debts) {
			DebFragment = new DebtsFragment();
			title = getString(R.string.debts);

			ft.replace(R.id.content_frame, DebFragment);
			ft.commit();

			viewIsAtHome = false;
		} else if (viewId == R.id.nav_objects) {
			ObjFragment = new ObjectsFragment();
			title = getString(R.string.objects);

			ft.replace(R.id.content_frame, ObjFragment);
			ft.commit();

			viewIsAtHome = false;
		} else if (viewId == R.id.nav_report) {
			ReportFragment repFragment = new ReportFragment();
			title = getString(R.string.report);

			ft.replace(R.id.content_frame, repFragment);
			ft.commit();

			viewIsAtHome = false;
		} else if (viewId == R.id.nav_shoplist) {
			ShoFragment = new ShoppingListFragment();
			title = getString(R.string.shoplist);

			ft.replace(R.id.content_frame, ShoFragment);
			ft.commit();

			viewIsAtHome = false;
		} else if (viewId == R.id.nav_chart) {
			ChaFragment = new ChartFragment();
			title = getString(R.string.charts);

			ft.replace(R.id.content_frame, ChaFragment);
			ft.commit();

			viewIsAtHome = false;
		} else if (viewId == R.id.nav_map) {
			MapFragment mapFragment = new MapFragment();
			title = getString(R.string.navigation_drawer_map);

			ft.replace(R.id.content_frame, mapFragment);
			ft.commit();

			viewIsAtHome = false;
		} else if (viewId == -1){
			NullFragment nullFragment = new NullFragment();
			ft.replace(R.id.content_frame, nullFragment);
			ft.commit();
		}

		if(viewId != -1) fragmentIndex = viewId;

		// set the toolbar title
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(title);
		}

		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);

	}

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
		// Handle navigation view item clicks here.
		displayView(item.getItemId());
		return true;
	}

	public void textConvClick(@SuppressWarnings("unused") View view){
		if(ChaFragment != null){
			// TODO implement chart saving when ChaFragment is null
			Bitmap bmp1 = ChaFragment.chartToBitmap(R.id.fund_graph/*, Ch1*/);
			Bitmap bmp2 = ChaFragment.chartToBitmap(R.id.trend_graph/*, Ch2*/);
			Data.xmlExport(bmp1, bmp2);
        }
		else Data.xmlExport(null, null);

		Toast.makeText(getApplicationContext(), getResources().getString(R.string.xml_completed), Toast.LENGTH_LONG).show();
	}

	@RequiresApi(Build.VERSION_CODES.O)
	private void createNotificationChannels() {
		List<NotificationChannelGroup> notificationChannelGroups = new ArrayList<>();
		notificationChannelGroups.add(new NotificationChannelGroup("group_one", getResources().getString(R.string.notification_channel_service)));
		notificationChannelGroups.add(new NotificationChannelGroup("group_two", getResources().getString(R.string.notification_channel_alarm)));
		notificationManager.createNotificationChannelGroups(notificationChannelGroups);

		int importance = NotificationManager.IMPORTANCE_DEFAULT;
		serviceNotificationChannel = new NotificationChannel(dummyId,
				getResources().getString(R.string.notification_channel_service), importance);
		alarmNotificationChannel = new NotificationChannel(alarmId,
				getResources().getString(R.string.notification_channel_alarm), importance);

		serviceNotificationChannel.setGroup(notificationChannelGroups.get(0).getId());
		alarmNotificationChannel.setGroup(notificationChannelGroups.get(1).getId());
		notificationManager.createNotificationChannel(serviceNotificationChannel);
		notificationManager.createNotificationChannel(alarmNotificationChannel);

		serviceNotificationChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), Notification.AUDIO_ATTRIBUTES_DEFAULT);
		alarmNotificationChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), Notification.AUDIO_ATTRIBUTES_DEFAULT);
	}

	@RequiresApi(Build.VERSION_CODES.O)
	private void ServiceNotification(){
		int Clr;
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
			Clr = getResources().getColor(R.color.green_dark, this.getTheme());
		else Clr = 0x803f00;
		Notification notification = new Notification.Builder(this, dummyId)
				.setContentTitle(getResources().getString(R.string.app_name))
				.setContentText(getResources().getString(R.string.notification_channel_service))
				.setColor(Clr)
				.setSmallIcon(R.drawable.ic_launcher_brown)
				.setChannelId(dummyId)
				.build();

		notificationManager.notify(1, notification);
	}

	private void insertPass()
	{
		AlertDialog.Builder alert = new AlertDialog.Builder(this, R.style.ombDialogTheme);

		alert.setTitle(getResources().getString(R.string.password_change));

		// Set an EditText view to get user input
		final EditText input = new EditText(this);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			input.setTextColor(getResources().getColor(R.color.white, this.getTheme()));

		input.setTransformationMethod(PasswordTransformationMethod.getInstance());
		alert.setView(input);

		alert.setPositiveButton(getResources().getString(android.R.string.ok),
				(dialog, whichButton) -> {
					String value = input.getText().toString();
					SharedPreferences.Editor editor = Opts.edit();
					editor.putString("key_main", value);
					editor.apply();
					recreate();
		});

		alert.setNegativeButton(getResources().getString(R.string.dialog_cancel), (dialog, whichButton) -> finish());

		alert.show();
		if(bootPermissionDialogActive.get()) alertBoot.show();
		if(allFilesPermissionDialogActive.get()) alertAllFilesPermission.show();
	}

	/*
	public void modPass()
	{
		//final boolean arch = archive;

		AlertDialog.Builder alert = new AlertDialog.Builder(this, R.style.ombDialogTheme);

		alert.setTitle(getResources().getString(R.string.password_change));
		//alert.setMessage("Message");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			input.setTextColor(getResources().getColor(R.color.white, this.getTheme()));

		input.setTransformationMethod(PasswordTransformationMethod.getInstance());
		alert.setView(input);
		String cancel = getResources().getString(R.string.dialog_cancel);

		alert.setPositiveButton(getResources().getString(android.R.string.ok),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						String oldPass = Data.getKey(false);
						String newPass = input.getText().toString();

						if(oldPass.isEmpty()){
							// database not encrypted
							if(newPass.isEmpty()) return;

							File originalFile = new File(document);
							String crypted = originalFile.getParent() + "/encrypted.db";
							String sql;

                            try {
								sql = String.format("ATTACH DATABASE '%s' AS encrypted KEY '%s';", crypted, newPass);
								Data.database.rawExecSQL(sql);

								Data.database.rawExecSQL("SELECT sqlcipher_export('encrypted');");

								Data.database.rawExecSQL("DETACH DATABASE encrypted;");
							}
							catch(Exception e){
								int i =1;
							}
							File cryptedFile = new File(crypted);

							int i = originalFile.getName().lastIndexOf('.');
							String name = originalFile.getName().substring(0,i);
							File backupFile = new File(originalFile.getParent() + "/" + name + "-un.bak");

							originalFile.renameTo(backupFile);

							//File export = new File("encrypted.db");
							cryptedFile.renameTo(originalFile);
						}
						else{
							// encrypted database
							String sql = String.format("PRAGMA rekey = '%s';", newPass);
							Data.database.execSQL(sql);

						}

						SharedPreferences.Editor editor = Opts.edit();
						editor.putString("key_main", newPass);
						editor.commit();
					}
				});

		alert.setNegativeButton(cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.

			}
		});

		alert.show();

	}
	*/

	private void stopLocation(){
		if(Opts.getBoolean("GSavePositions", false)) {
			if (LocationPermission && (mLocationManager != null)) {
				mLocationManager.removeUpdates(mLocationListener);
				mLocationManager = null;
			}
		}

	}

	@SuppressWarnings("CallToThreadRun")
	@RequiresApi(api = Build.VERSION_CODES.R)
	private void Authenticate(){
		// Unlock app if requested
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			if (Opts.getBoolean("GSecurity", false)) {
				int authenticators = BIOMETRIC_WEAK | DEVICE_CREDENTIAL;
				BiometricManager biometricManager = BiometricManager.from(this);
				int res2 = biometricManager.canAuthenticate(authenticators);
				switch (res2) {
					case BiometricManager.BIOMETRIC_SUCCESS:
					case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
					case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
						break;
					case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
						final Intent enrollIntent;
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
							// Prompts the user to create credentials that your app accepts.
							enrollIntent = new Intent(Settings.ACTION_BIOMETRIC_ENROLL);
							enrollIntent.putExtra(Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED,
									authenticators);
						}
						else{
							enrollIntent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
						}
						startActivity(enrollIntent);
//						break;
					case BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED:
					case BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED:
					case BiometricManager.BIOMETRIC_STATUS_UNKNOWN:
						break;
				}

				Thread thread = new Thread(new ResetLoginTask());

				Executor executor = ContextCompat.getMainExecutor(this);
				BiometricPrompt biometricPrompt = new BiometricPrompt(this,
						executor, new BiometricPrompt.AuthenticationCallback() {
					@Override
					public void onAuthenticationError(int errorCode,
													  @NonNull CharSequence errString) {
						super.onAuthenticationError(errorCode, errString);
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.authentication_error) + errString, Toast.LENGTH_LONG)
								.show();
						finish();
					}

					@Override
					public void onAuthenticationSucceeded(
							@NonNull BiometricPrompt.AuthenticationResult result) {
						super.onAuthenticationSucceeded(result);
						displayView(fragmentIndex);

						// Disable login for one minute (also to avoid crashes for continuous login requests
						askForLogin = false;
						thread.run();
					}

					@Override
					public void onAuthenticationFailed() {
						super.onAuthenticationFailed();
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.authentication_failed),
								Toast.LENGTH_SHORT)
								.show();
						finish();
					}
				});

				// Allows user to authenticate using either a biometric or
				// their lock screen credential (PIN, pattern, or password).
				//.setSubtitle("Log in using your biometric credential")
				BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
						.setTitle(getResources().getString(R.string.authentication_title))
						//.setSubtitle("Log in using your biometric credential")
						.setAllowedAuthenticators(authenticators)
						.build();

				displayView(-1);
				biometricPrompt.authenticate(promptInfo);
				askForLogin = false;
				thread.run();
			}
		}
	}

	class ResetLoginTask implements Runnable {
		@Override
		public void run() {
			int loginTimerInt = 60000;
			new Handler(Looper.getMainLooper()).postDelayed(MainActivity.this::ResetLogin, loginTimerInt);
		}
	}

	private void ResetLogin()
	{
		askForLogin = true;
	}

	public void FundGroupClick(View v){
		Bundle bundle = new Bundle();

		// Create intent;
		Intent intent = new Intent(this, EditFundGroups.class);

		ArrayList<String> GroupList = new ArrayList<>();
		ArrayList<Integer> GroupIDList = new ArrayList<>();
		ArrayList<String> FundList = new ArrayList<>();
//		ArrayList<Integer> iconIndexes = new ArrayList<>();

		Cursor GroupTable = Data.database.query("select * from FundGroups where owner = '-1';", null);
		int GTC = GroupTable.getCount();

		// Fill group list
//		FundGroups->GroupList->Clear();
		for(int i = 0; i < GTC; i++){
			GroupTable.moveToPosition(i);
			GroupList.add(GroupTable.getString(1));
			GroupIDList.add(GroupTable.getInt(0));
		}

		Cursor ChildTable = Data.database.query("select * from FundGroups where owner > '-1';", null);
		int CTC = ChildTable.getCount();

		// Create fund indexes arrays
		ArrayList<Integer> IDs = new ArrayList<>();
		ArrayList<Integer> Owners = new ArrayList<>();
		ArrayList<String> OwnerNames = new ArrayList<>();

		// Fill fund list
		Cursor FundTable = Data.database.query("select * from Funds;", null);
		int FTC = FundTable.getCount();
		if(FTC > 0){
			String FundName;
			for(int i = 0; i < FTC; i++){
				FundTable.moveToPosition(i);
				FundName = FundTable.getString(1);
				if(FundName.compareToIgnoreCase(Data.FileData.DefFund) != 0) {
					boolean Found = false;

					// Fill-in with fund information
					FundList.add(FundName);
					IDs.add(FundTable.getInt(0));

					// Search for owner index in the child table
					int OwnerID = -1;
					for (int j = 0; j < CTC; j++) {
                        //noinspection DataFlowIssue
						Found = false;
						ChildTable.moveToPosition(j);
						if (FundName.equalsIgnoreCase(ChildTable.getString(1))) {
							OwnerID = ChildTable.getInt(2);
//							Owners.set(ListIndex, OwnerID);
							Owners.add(OwnerID);
							Found = true;
							break;
						}
					}
					if(! Found) Owners.add(-1);

					// Search for owner name in the group table
					for (int j = 0; j < GTC; j++) {
						Found = false;
						GroupTable.moveToPosition(j);
						if (GroupTable.getInt(0) == OwnerID) {
//							OwnerNames.set(ListIndex, GroupTable.getString(1));
							OwnerNames.add(GroupTable.getString(1));
							Found = true;
							break;
						}
					}
					if(! Found) OwnerNames.add("");
				}
			}
		}

		bundle.putStringArrayList("groups", GroupList);
		bundle.putIntegerArrayList("groupids", GroupIDList);

		bundle.putStringArrayList("funds", FundList);

		bundle.putIntegerArrayList("ids", IDs);
		bundle.putIntegerArrayList("owners", Owners);
		bundle.putStringArrayList("ownernames", OwnerNames);

		intent.putExtras(bundle);

		RequestCode = 16;
		mLauncher.launch(intent);

	}

}

