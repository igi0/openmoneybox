/* **************************************************************
 * Name:
 * Purpose:   Credits fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-11-07
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class CreditsFragment extends Fragment {
    public int credit_pos;
    private final List<CreditDebt_wrapper> credits = new ArrayList<>();
    public ImageButton btn_removeCredit, btn_remitCredit;
    public RecyclerView cv;    // cv: credits view
    private FloatingActionButton newCreditBtn;

    public CreditsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       MainActivity activity = (MainActivity) requireActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_credits, container, false);

        // Credits tab
        String str_tmp;
        Currency curr = Currency.getInstance(Locale.getDefault());
        TextView total_view;

        // Create the OnClickListener
        btn_removeCredit = view.findViewById( R.id.removeCredit );
        btn_remitCredit = view.findViewById( R.id.remitCredit );
        newCreditBtn = view.findViewById( R.id.creditActionButton );
        View.OnClickListener clickListener = v -> {
            if (v == newCreditBtn) activity.NewCreditClick(v);
            else if (v == btn_removeCredit) activity.RemoveCreditClick(v);
            else if (v == btn_remitCredit) activity.RemitCreditClick(v);
        };
        newCreditBtn.setOnClickListener(clickListener);
        btn_removeCredit.setOnClickListener(clickListener);
        btn_remitCredit.setOnClickListener(clickListener);

        btn_removeCredit.setEnabled(false);
        btn_remitCredit.setEnabled(false);
        cv = view.findViewById(R.id.cv);
        cv.setLayoutManager(new LinearLayoutManager(Objects.requireNonNull(activity).getApplicationContext()));
        Recycler_Adapter_CreditDebt credits_adapter = new Recycler_Adapter_CreditDebt(credits);
        credits_adapter.frame = activity;
        cv.setAdapter(credits_adapter);

        credits.clear();
        for(int i = 0; i < activity.Data.NCre; i++){
            str_tmp = curr.getSymbol() + " " + omb_library.FormDigits(activity.Data.Credits.get(i).Value, false);

            long c_id = activity.Data.Credits.get(i).ContactIndex;
            String badgeUri = null;
            if(c_id > 0){
                badgeUri = activity.Data.getContactImage(c_id);
                if(badgeUri == null) badgeUri = "-1";
            }

            credits.add(new CreditDebt_wrapper(/*activity.Data.Credits.get(i).Id, */activity.Data.Credits.get(i).Name,
                    str_tmp, badgeUri));
        }
        credits_adapter.notifyDataSetChanged();

        total_view = view.findViewById(R.id.total_credit);
        str_tmp = getResources().getString(R.string.total) + "<br><small>" + curr.getSymbol() + " ";
        str_tmp += omb_library.FormDigits(activity.Data.Tot_Credits, false) + "</small>";
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            total_view.setText(HtmlCompat.fromHtml(str_tmp, HtmlCompat.FROM_HTML_MODE_COMPACT));
        else total_view.setText(HtmlCompat.fromHtml(str_tmp, HtmlCompat.FROM_HTML_MODE_LEGACY));

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

}
