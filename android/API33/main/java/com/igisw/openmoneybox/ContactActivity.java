/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Contact search
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-11-08
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.text.HtmlCompat;

import java.util.ArrayList;

public class ContactActivity extends Activity {

    private final ArrayList<String> contactIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_list);

        Bundle b = getIntent().getExtras();
        String searchString;
        if(b != null) {
            searchString = b.getString("str", "");
            searchString = searchString.toLowerCase();
        }
        else searchString = "";

        ListView contactView = findViewById( R.id.contactList );

        String name, id;
        ArrayList<String> contactList = new ArrayList<>();
        Cursor phones = getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI
                /*ContactsContract.CommonDataKinds.Callable.CONTENT_URI*/, /*PROJECTION*/null, /*ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME + " LIKE ?"*/null, /*mSelectionArgs*/null, ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY + " ASC"/*null*/);

        boolean found;
        int colInd;

        while (phones.moveToNext())
        {
            colInd = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            if(colInd != -1) name = phones.getString(colInd);
            else name = null;

            colInd = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
            if(colInd != -1) id = phones.getString(colInd);
            else id = null;

            if((id != null) && (name != null)) {

                found = false;
                for(int i = 0; i < contactList.size(); i ++){
                    if(contactList.get(i).compareTo(name) == 0){
                        found = true;
                        break;
                    }
                }

                if(! found) {
                    if(name.toLowerCase().contains(searchString)) {
                        contactList.add(name);
                        contactIds.add(id);
                    }
                }
            }
        }
        phones.close();

        // Create ArrayAdapter
        ArrayAdapter<String> contactAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, contactList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    textView.setText(Html.fromHtml(textView.getText().toString(), Html.FROM_HTML_MODE_COMPACT));
                else textView.setText(HtmlCompat.fromHtml(textView.getText().toString(), HtmlCompat.FROM_HTML_MODE_LEGACY));

                textView.setTag(position);
                textView.setOnClickListener(view -> {
                    int pos = (Integer) view.getTag();
                    TextView item = (TextView) view;
                    int ReturnCode /*= RESULT_CANCELED*/;

                    //omb_library.appContext = getApplicationContext();
                    // Create intent w/ result
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putString("id", contactIds.get(pos));
                    bundle.putString("contact", item.getText().toString());

                    intent.putExtras(bundle);

                    ReturnCode = RESULT_OK;
                    setResult(ReturnCode, intent);
                    finish();
                });

                return textView;
            }
        };
        // Set the ArrayAdapter as the ListView's adapter.
        contactView.setAdapter(contactAdapter);

    }

}
