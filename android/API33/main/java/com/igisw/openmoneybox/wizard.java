/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-11-08
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class wizard extends Activity {

	private EditText savedText, cashText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wizard);

		// Create the OnClickListener
		TextView OkButton = findViewById(R.id.OkBtn);
		View.OnClickListener clickListener = this::okBtnClick;
		OkButton.setOnClickListener(clickListener);

		savedText = findViewById(R.id.SavedEdit);
		cashText = findViewById(R.id.CashEdit);

		TextView caption = findViewById(R.id.Title);
		caption.setText(getResources().getString(R.string.wizard_caption));
	}
	
	public void okBtnClick(@SuppressWarnings("unused") View view){
		double saved, cash;
		int ReturnCode;
		
		omb_library.appContext = getApplicationContext();

		if(savedText.getText().toString().isEmpty()){
			omb_library.Error(25, "");
			savedText.requestFocus();
			return;}
		
		if(cashText.getText().toString().isEmpty()){
			omb_library.Error(25, "");
			cashText.requestFocus();
			return;}

		saved = Double.parseDouble(savedText.getText().toString());
		cash = Double.parseDouble(cashText.getText().toString());

		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putDouble("saved", saved);
		bundle.putDouble("cash", cash);
		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();

	}
}
