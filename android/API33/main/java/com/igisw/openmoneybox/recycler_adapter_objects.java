/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-11-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class recycler_adapter_objects extends RecyclerView.Adapter<recycler_adapter_objects.ViewHolder>{

    public MainActivity frame;
    private final List<objects_wrapper> objects;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ViewHolder(View v) {
            super(v);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int item = getAbsoluteAdapterPosition();
            frame.updateObjectRecyclerItem(item);
        }

    }

    public class ObjectViewHolder extends ViewHolder{

        final CardView cv;
        final TextView objectName;
        final TextView objectContact;
        final TextView objectAlarm;
        final ImageView objectIcon;
        final RadioButton objectChecked;
        final ImageView objectBadge;

        ObjectViewHolder(View itemView) {
            super(itemView);

            LinearLayout ll = itemView.findViewById(R.id.ll_cv_object);
            ConstraintLayout rl = itemView.findViewById(R.id.rl_cv_object );
            RelativeLayout rl2 = itemView.findViewById(R.id.rl_cv_object_2 );
            this.cv = itemView.findViewById(R.id.cv);
            this.objectName = itemView.findViewById(R.id.object_name);
            this.objectContact = itemView.findViewById(R.id.object_contact);
            this.objectAlarm = itemView.findViewById(R.id.object_alarm);
            this.objectIcon = itemView.findViewById(R.id.object_icon);
            this.objectChecked = itemView.findViewById(R.id.object_checked);
            this.objectBadge = itemView.findViewById(R.id.quickContactBadge);

            if(frame.Opts.getBoolean("GDarkTheme", false))
            {
                ll.setBackgroundColor(0xFF000000);
                cv.setCardBackgroundColor(0xFF333333);
                rl.setBackgroundColor(0xFF333333);
                rl2.setBackgroundColor(0xFF333333);
                objectName.setBackgroundColor(0xFF333333);
                objectContact.setBackgroundColor(0xFF333333);
                objectAlarm.setBackgroundColor(0xFF333333);
                objectIcon.setBackgroundColor(0xFF333333);
                objectBadge.setBackgroundColor(0xFF333333);
            }
        }

    }

    public recycler_adapter_objects(List<objects_wrapper> objects){
        this.objects = objects;
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    @Override
    public @NonNull ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_object, viewGroup, false);
        return new ObjectViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ObjectViewHolder holder = (ObjectViewHolder) viewHolder;
        holder.objectName.setText(objects.get(i).name);
        holder.objectContact.setText(objects.get(i).contact);
        holder.objectAlarm.setText(objects.get(i).alarm);
        holder.objectIcon.setImageResource(objects.get(i).iconId);
        holder.objectChecked.setChecked(false);

        String b = objects.get(i).badgeUri;
        if(b != null) {
            Bitmap bitmap;
            if(b.equals("-1"))
                bitmap = BitmapFactory.decodeResource(frame.getResources(), R.drawable.accountbox);
            else {
                bitmap = omb_library.loadContactPhotoThumbnail(b);
            }
            holder.objectBadge.setImageBitmap(bitmap);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return objects.get(position).type;
    }
}
