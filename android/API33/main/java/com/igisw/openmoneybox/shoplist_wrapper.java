/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-08-19
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

class ShoppingList_Wrapper {
    //final int id;
    final String item, alarm;

    ShoppingList_Wrapper(/*int id, */String item, String alarm) {
        //this.id = id;
        this.item = item;
        this.alarm = alarm;
    }
}
