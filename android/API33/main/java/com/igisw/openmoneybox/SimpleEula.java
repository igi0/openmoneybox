/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * forked from https://github.com/Agilevent/Eula-Sample
 * Created:   2024-10-08
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.preference.PreferenceManager;

import org.jetbrains.annotations.NonNls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

class SimpleEula {

    private final Activity mActivity;
	
	public SimpleEula(Activity context) {
		mActivity = context; 
	}
	
	private PackageInfo getPackageInfo() {
        PackageInfo pi = null;
        try {
             pi = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            // e.printStackTrace();
        }
        return pi; 
    }

     public void show(boolean showLicense) {
        PackageInfo versionInfo = getPackageInfo();

        // the eulaKey changes every time you increment the version number in the AndroidManifest.xml
        @NonNls String EULA_PREFIX = "eula_";
        final String eulaKey;
        final String oldKey;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            long ver = versionInfo.getLongVersionCode();
            eulaKey = EULA_PREFIX + ver;
            oldKey = EULA_PREFIX + (ver - 1);
        }
        else{
            int ver = versionInfo.versionCode;
            eulaKey = EULA_PREFIX + ver;
            oldKey = EULA_PREFIX + (ver - 1);
        }

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        boolean hasBeenShown = prefs.getBoolean(eulaKey, false);
        
        if((! hasBeenShown) || showLicense){

        	// Show the Eula
            String title = mActivity.getString(R.string.app_name) +
                " v" + //NON-NLS
                versionInfo.versionName;
            
            //Includes the updates as well so users know what changed. 

            StringBuilder eula = new StringBuilder();

            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(mActivity.getAssets().open(
                        mActivity.getString(R.string.lang) +
                        "/license.txt"), StandardCharsets.UTF_8))) { //NON-NLS

                // do reading, usually loop until end of file reading
                String mLine;
                while ((mLine = reader.readLine()) != null) {
                    //process line
                    eula.append(mLine).append("\n");
                }
            } catch (IOException e) {
                //log the exception
            }

            String message = "";
            if(! showLicense)
                message = mActivity.getString(R.string.updates) + "\n\n";
            message += eula;

            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.ombDialogTheme)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                        // Mark this version as read.
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putBoolean(eulaKey, true);
                        editor.apply();
                        dialogInterface.dismiss();

                        if(prefs.getBoolean(oldKey, false)){
                            editor.remove(oldKey);
                            editor.apply();}
                    })
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                        // Close the activity as they have declined the EULA
                        mActivity.finish();
                    });
            builder.create().show();
        }
    }
	
}
