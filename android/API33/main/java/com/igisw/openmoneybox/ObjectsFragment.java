/* **************************************************************
 * Name:
 * Purpose:   Objects fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-11-07
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class ObjectsFragment extends Fragment {
    public int lent_pos, borrowed_pos;
    public final List<objects_wrapper> objects = new ArrayList<>();
    public ImageButton lendObjectButton, btn_getbackObj, borrowObjectButton, btn_givebackObj;
    public RecyclerView ov;    // ov: objects view

    public ObjectsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity activity = (MainActivity) requireActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_objects, container, false);

        // Objects tab
        String str_tmp;

        // Create the OnClickListener
        lendObjectButton = view.findViewById(R.id.lendObj);
        btn_getbackObj = view.findViewById(R.id.getbackObj);
        borrowObjectButton = view.findViewById(R.id.borrowObj);
        btn_givebackObj = view.findViewById(R.id.givebackObj);
        View.OnClickListener clickListener = v -> {
            if (v == lendObjectButton) activity.LendClick(v);
            else if (v == btn_getbackObj) activity.getBackClick(v);
            else if (v == borrowObjectButton) activity.BorrowClick(v);
            else if (v == btn_givebackObj) activity.giveBackClick(v);
        };
        lendObjectButton.setOnClickListener(clickListener);
        btn_getbackObj.setOnClickListener(clickListener);
        borrowObjectButton.setOnClickListener(clickListener);
        btn_givebackObj.setOnClickListener(clickListener);

        btn_getbackObj.setEnabled(false);
        btn_givebackObj.setEnabled(false);
        ov = view.findViewById(R.id.ov);
        LinearLayoutManager llm1 = new LinearLayoutManager(Objects.requireNonNull(activity).getApplicationContext());
        ov.setLayoutManager(llm1);
        recycler_adapter_objects objects_adapter = new recycler_adapter_objects(objects);
        objects_adapter.frame = activity;
        ov.setAdapter(objects_adapter);

        objects.clear();
        int thisYear, alarmYear;
        thisYear = Calendar.getInstance().get(Calendar.YEAR);
        GregorianCalendar alarmCalendar;
        for(int i = 0; i < activity.Data.NLen; i++) {
            alarmCalendar = activity.Data.Lent.get(i).Alarm;
            alarmYear = alarmCalendar.get(Calendar.YEAR);
            if((alarmYear - thisYear) < 80) str_tmp = omb_library.omb_DateToStr(alarmCalendar);
            else str_tmp = getResources().getString(R.string.alarm_none);

            long c_id = activity.Data.Lent.get(i).ContactIndex;
            String badgeUri = null;
            if(c_id > 0){
                badgeUri = activity.Data.getContactImage(c_id);
                if(badgeUri == null) badgeUri = "-1";
            }

            objects.add(new objects_wrapper(0, activity.Data.Lent.get(i).Id,
                    activity.Data.Lent.get(i).Object, activity.Data.Lent.get(i).Name,
                    str_tmp, R.drawable.object_lent, badgeUri));
        }
        for(int i = 0; i < activity.Data.NBor; i++) {
            alarmCalendar = activity.Data.Borrowed.get(i).Alarm;
            alarmYear = alarmCalendar.get(Calendar.YEAR);
            if((alarmYear - thisYear) < 80) str_tmp = omb_library.omb_DateToStr(alarmCalendar);
            else str_tmp = getResources().getString(R.string.alarm_none);

            long c_id = activity.Data.Borrowed.get(i).ContactIndex;
            String badgeUri = null;
            if(c_id > 0){
                badgeUri = activity.Data.getContactImage(c_id);
                if(badgeUri == null) badgeUri = "-1";
            }

            objects.add(new objects_wrapper(1, activity.Data.Borrowed.get(i).Id,
                    activity.Data.Borrowed.get(i).Object, activity.Data.Borrowed.get(i).Name,
                    str_tmp, R.drawable.object_borrow, badgeUri));
        }
        objects_adapter.notifyDataSetChanged();

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

}
