/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-11-07
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.preference.PreferenceManager;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class alarm extends Activity {

	private int kind, index;
	private DatePicker almText;
	private CheckBox almCheck;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alarm);

		// Create the OnClickListener
		Button cancelButton, postponeButton;
		cancelButton = findViewById(R.id.CancelBtn);
		postponeButton = findViewById(R.id.PostponeBtn);
		View.OnClickListener clickListener = v -> {
			if (v == cancelButton) CancelBtnClick(v);
			else if (v == postponeButton) PostponeBtnClick(v);
		};
		cancelButton.setOnClickListener(clickListener);
		postponeButton.setOnClickListener(clickListener);

		TextView msg = findViewById(R.id.Text);
		almText = findViewById(R.id.Alarm);
		almCheck = findViewById(R.id.cb_PostponeAll);

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			LinearLayout ll = findViewById(R.id.ll_alarm);

			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				Resources.Theme theme = getTheme();
				ll.setBackgroundColor(getResources().getColor(R.color.black, theme));
				msg.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				msg.setTextColor(getResources().getColor(R.color.white, theme));
				almCheck.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				almCheck.setTextColor(getResources().getColor(R.color.white, theme));
			}
			else{
				ll.setBackgroundColor(getResources().getColor(R.color.black));
				msg.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				msg.setTextColor(getResources().getColor(R.color.white));
				almCheck.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				almCheck.setTextColor(getResources().getColor(R.color.white));
			}
		}

		Bundle bundle = getIntent().getExtras();
		
		kind = bundle.getInt("kind");
		index = bundle.getInt("index");
		
		msg.setText(bundle.getString("message"));
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.add(Calendar.DAY_OF_MONTH, 1);
		almText.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
		cal.add(Calendar.SECOND, -1);
		
		final int minYear = cal.get(Calendar.YEAR);
		final int minMonth = cal.get(Calendar.MONTH);
		final int minDay = cal.get(Calendar.DAY_OF_MONTH);
		final GregorianCalendar cal2 = new GregorianCalendar(minYear, minMonth, minDay);

		almText.init(minYear, minMonth, minDay,
				new OnDateChangedListener() {

					public void onDateChanged(DatePicker view, int year,
							int month, int day) {
						Calendar newDate = Calendar.getInstance();
						newDate.set(year, month, day);

						if (cal2.after(newDate)) {
							view.init(minYear, minMonth, minDay, this);
						}
					}
				});

		NotificationManager mNotificationManager =
			(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancelAll();

	}
	
	public void PostponeBtnClick(@SuppressWarnings("unused") View view){
		omb_library.appContext = getApplicationContext();

		// Create intent w/ result
		Intent intent = new Intent(this, MainActivity.class);
		intent.setAction("Update alarms");

		Bundle bundle = createBundle(false);
		
		intent.putExtras(bundle);
				
		startActivity(intent);
		finish();
	}

	public void CancelBtnClick(@SuppressWarnings("unused") View view){
		int ReturnCode;
		
		omb_library.appContext = getApplicationContext();

		// Create intent w/ result
		Intent intent = new Intent();

		Bundle bundle = createBundle(true);
		
		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}
	
	private Bundle createBundle(boolean cancel){
		Bundle bundle = new Bundle();

		bundle.putInt("kind", kind);
		bundle.putInt("index", index);
		if(cancel) bundle.putBoolean("snooze_all", false);
		else bundle.putBoolean("snooze_all", almCheck.isChecked());
		int year = almText.getYear();
		if(cancel) year += 100;
		bundle.putInt("year", year);
		bundle.putInt("month", almText.getMonth());
		bundle.putInt("day", almText.getDayOfMonth());

		return bundle;
	}
}
