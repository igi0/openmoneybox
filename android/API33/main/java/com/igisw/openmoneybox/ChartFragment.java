/* **************************************************************
 * Name:
 * Purpose:   Chart fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-01
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChartFragment extends Fragment {
    private MainActivity activity;
    private View view;

    public ChartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (MainActivity) requireActivity();

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chart, container, false);

        // Create the OnClickListener
        ImageButton exportButton = view.findViewById(R.id.xml_export);
        View.OnClickListener clickListener = activity::textConvClick;
        exportButton.setOnClickListener(clickListener);

        paintChart();

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    private void paintChart(){

        boolean default_is_small = false;
        int i;
        double percent, sum_small_funds = 0;
        String color;

        Legend legend;
        SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(activity);
        boolean dark_theme = Opts.getBoolean("GDarkTheme", false);

        // Fund Chart construction
        List<PieEntry> pie_entries = new ArrayList<>();
        PieChart FundPlot = view.findViewById(R.id.fund_graph);

        int inserted_entries = 0;
        int default_entry = -1;

        // NOTE (igor#1#): Hidden feature
        double TotFundAttached = 0;
        double currentTotal = activity.Data.getTot(omb35core.TTypeVal.tvFou);
        double attached_value;
        Cursor AttachedFundTable;
        if(activity.Data.ExternalFileAttached){
            AttachedFundTable = activity.Data.getExternalFundTable();
            for(int j = 0; j < AttachedFundTable.getCount(); j++){
                AttachedFundTable.moveToPosition(j);
                attached_value = AttachedFundTable.getDouble(2);
                TotFundAttached += attached_value;
            }
            currentTotal += TotFundAttached;
            for(i = 0; i < AttachedFundTable.getCount(); i++){
                AttachedFundTable.moveToPosition(i);
                attached_value = AttachedFundTable.getDouble(2);
                percent = attached_value / currentTotal * 100;
                if((percent) > 1){
                    pie_entries.add(new PieEntry((float) attached_value, AttachedFundTable.getString(1) + " (*)" + String.format(Locale.US, " %.1f", percent) + "%"));
                    inserted_entries++;
                }
                else
                    sum_small_funds += attached_value;
            }
        }
        // ------------------------------

        double Tot_Funds = activity.Data.getTot(omb35core.TTypeVal.tvFou)
            + TotFundAttached;
        // FIXME: replace getTot calls with Tot_Funds

        if(Opts.getBoolean("GGroupFunds", false)){
            Cursor GroupTable, ChildTable;
            GroupTable = activity.Data.database.query("select * from FundGroups where owner = '-1';", null);
            int GTC = GroupTable.getCount();

            double[] GroupTotals = new double[GTC];
            for( i = 0; i < GTC; i++)
                GroupTotals[i] = 0;
            ArrayList<String> GroupNames = new ArrayList<>();
            for(i = 0; i < GTC; i++){
                GroupTable.moveToPosition(i);
                GroupNames.add(GroupTable.getString(1));
            }

            ChildTable = activity.Data.database.query("select * from FundGroups where owner > '-1';", null);
            int CTC = ChildTable.getCount();

            boolean Found;
            int Group_ID;
            String FundName;
            for(i = 0; i < activity.Data.NFun; i++){
                Found = false;
                FundName = activity.Data.Funds.get(i).Name;
                for(int j = 0; j < CTC; j++){
                    ChildTable.moveToPosition(j);
                    if(FundName.equalsIgnoreCase(ChildTable.getString(/*"name"*/1))){
                        Group_ID = ChildTable.getInt(/*"owner", -1*/2);
                        for(int k = 0; k < GTC; k++){
                            GroupTable.moveToPosition(k);

                            if(GroupTable.getInt(/*"id", -1*/0) == Group_ID){
                                Found = true;
                                GroupTotals[k] += activity.Data.Funds.get(i).Value;
                                break;
                            }

                        }

                        break;
                    }
                }
                if(! Found){
                    percent = activity.Data.Funds.get(i).Value / Tot_Funds * 100;
                    if((percent) > 1){
                        pie_entries.add(new PieEntry((float) activity.Data.Funds.get(i).Value, activity.Data.Funds.get(i).Name + String.format(Locale.US, " %.1f", percent) + "%"));
                        inserted_entries++;
                    }
                    else{
                        sum_small_funds += activity.Data.Funds.get(i).Value;
                        if(activity.Data.Funds.get(i).Name.equalsIgnoreCase(activity.Data.FileData.DefFund)) default_is_small = true;}

                }
            }

            for(i = 0; i < GTC; i++){
                percent = GroupTotals[i] / Tot_Funds * 100;
                if((percent) > 1){
                    pie_entries.add(new PieEntry((float) GroupTotals[i], GroupNames.get(i) + " - [" +
                        getResources().getString(R.string.fundGroups_group_marker) + "]" + String.format(Locale.US, " %.1f", percent) + "%"));
                    inserted_entries++;
                }
                else{
                    sum_small_funds += GroupTotals[i];
                }

            }

        }
        else {


            for (i = 0; i < activity.Data.NFun; i++) {
                percent = activity.Data.Funds.get(i).Value / activity.Data.getTot(omb35core.TTypeVal.tvFou) * 100;
                if (percent > 1) {
                    pie_entries.add(new PieEntry((float) activity.Data.Funds.get(i).Value, activity.Data.Funds.get(i).Name + String.format(Locale.US, " %.1f", percent) + "%"));
                    inserted_entries++;
                    if (activity.Data.Funds.get(i).Name.compareToIgnoreCase(activity.Data.FileData.DefFund) == 0)
                        default_entry = inserted_entries - 1;
                } else {
                    sum_small_funds += activity.Data.Funds.get(i).Value;
                    if (activity.Data.Funds.get(i).Name.compareToIgnoreCase(activity.Data.FileData.DefFund) == 0)
                        default_is_small = true;
                }
            }

        }

        if(sum_small_funds > 0){
            pie_entries.add(new PieEntry((float) sum_small_funds, getResources().getString(R.string.fund_others) + String.format(Locale.US, " %.2f", sum_small_funds / Tot_Funds) + "%"));
            inserted_entries++;
            if(default_is_small)default_entry = inserted_entries - 1;
        }
        PieDataSet set = new PieDataSet(pie_entries, getResources().getString(R.string.fund_chart));

        /*
        set.setValueFormatter(new ValueFormatter() {
        public String getFormattedValue(float value)  {
        */
        set.setValueFormatter((value, e, i1, v) -> String.format(Locale.US, "%.02f", value));

        set.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        set.setValueLinePart1OffsetPercentage(100.f);
        set.setValueLinePart1Length(0.8f);
        set.setValueLinePart2Length(0.8f);

        for(i = 1; i < 7; i++){
            switch(i){
                case 1:
                    color = "#FFFF99";
                    break;
                case 2:
                    color = "#3DEB3D";
                    break;
                case 3:
                    color = "#FF0000";
                    break;
                case 4:
                    color = "#00FF00";
                    break;
                case 5:
                    color = "#0000FF";
                    break;
                default:
                    //noinspection SpellCheckingInspection
                    color = "#99CCFF";
            }
            if(i == 1) set.setColor(Color.parseColor(color));
            else set.addColor(Color.parseColor(color));
        }

        PieData data = new PieData(set);
        FundPlot.setData(data);
        FundPlot.getDescription().setEnabled(false);
        if(dark_theme) {
            int cl;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Resources.Theme theme = this.requireContext().getTheme();
                cl = getResources().getColor(R.color.white, theme);
            }
            else cl = getResources().getColor(R.color.white);
            set.setValueLineColor(cl);
            FundPlot.setEntryLabelColor(cl);
            legend = FundPlot.getLegend();
            legend.setTextColor(cl);
        }
        else
            FundPlot.setEntryLabelColor(Color.parseColor("#000000"));
        FundPlot.setEntryLabelTextSize(7f);

        // Highlight default fund
        Highlight h = new Highlight((float) default_entry, 0, 0); // dataset index for pie chart is always 0
        FundPlot.highlightValues(new Highlight[] { h });
        FundPlot.setDrawHoleEnabled(false);
        FundPlot.invalidate(); // refresh

        // Trend chart construction
        GregorianCalendar month = activity.Data.Day;
        GregorianCalendar lastDay = month;
        month.set(Calendar.MONTH, (int) activity.Data.FileData.Month);
        int number_of_days = month.getActualMaximum(Calendar.DAY_OF_MONTH);
        List<Entry> trendValues = new ArrayList<>();
        double LastValue = 0;
        double FirstValue = 0;
        boolean firstDateFound = false;

        NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
        String value;

        for(i = 0; i < activity.Data.NLin; i++){
            if(activity.Data.isDate(i)){

                if(! firstDateFound){
                    // Robustness code to handle localized or unlocalized strings
                    value = activity.Data.Lines.get(i).Value;
                    if(value.contains(".")) FirstValue = Double.parseDouble(value);
                    else{
                        try {
                            FirstValue = Objects.requireNonNull(nf.parse(value)).doubleValue();
                        } catch (ParseException e) {
                            FirstValue = -1;
                        }
                    }
                    firstDateFound = true;
                }

                lastDay = activity.Data.Lines.get(i).Date;

                // Robustness code to handle localized or unlocalized strings
                value = activity.Data.Lines.get(i).Value;
                if(value.contains(".")) LastValue = Double.parseDouble(value);
                else{
                    try {
                        LastValue = Objects.requireNonNull(nf.parse(value)).doubleValue();
                    } catch (ParseException e) {
                        LastValue = -1;
                    }
                }
                Entry day_entry = new Entry((float) lastDay.getTimeInMillis(), (float) LastValue);
                trendValues.add(day_entry);
            }}

        LineChart TrendPlot = view.findViewById(R.id.trend_graph);
        LineDataSet setComp1 = new LineDataSet(trendValues, getResources().getString(R.string.trend_chart));

        setComp1.setValueFormatter((value1, e, i12, v) -> String.format(Locale.US, "%.02f", value1));

        setComp1.setAxisDependency(YAxis.AxisDependency.LEFT);
        XAxis x_axis = TrendPlot.getXAxis();
        x_axis.setPosition(XAxis.XAxisPosition.BOTTOM);
        if(dark_theme) {
            int cl;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Resources.Theme theme = this.requireContext().getTheme();
                cl = getResources().getColor(R.color.white, theme);
            }
            else cl = getResources().getColor(R.color.white);
            setComp1.setColor(cl);
            setComp1.setValueTextColor(cl);
            x_axis.setTextColor(cl);
            TrendPlot.getAxisLeft().setTextColor(cl);
            TrendPlot.getAxisRight().setTextColor(cl);
            legend = TrendPlot.getLegend();
            legend.setTextColor(cl);
        }
        else
            setComp1.setColor(Color.parseColor("#000000"));
        // use the interface ILineDataSet
        List<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(setComp1);

        if(! activity.Data.FileData.ReadOnly){
            // trend addition
            month.set(Calendar.DAY_OF_MONTH, number_of_days);
            List<Entry> trendTrendValues = new ArrayList<>();
            trendTrendValues.add(new Entry((float) lastDay.getTimeInMillis(), (float) LastValue));
            trendTrendValues.add(new Entry((float) month.getTimeInMillis(), (float) currentTotal));
            LineDataSet setComp2 = new LineDataSet(trendTrendValues, getResources().getString(R.string.trend_last));
            setComp2.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataSets.add(setComp2);

            if(FirstValue == currentTotal) color = "#FFFF00";
            else if(FirstValue < currentTotal) color = "#006400";
            else color = "#FF0000";
            setComp2.setColor(Color.parseColor(color));
        }

        LineData trendData = new LineData(dataSets);

        //TrendPlot.getXAxis().setValueFormatter(new DateAxisValueFormatter());
        TrendPlot.getXAxis().setValueFormatter((value12, b) -> new SimpleDateFormat("dd/MM", Locale.US).format(new Date((long) value12)));
        TrendPlot.setData(trendData);
        TrendPlot.getDescription().setEnabled(false);
        TrendPlot.invalidate(); // refresh

    }

    public Bitmap  chartToBitmap(int id){
        View plot = view.findViewById(id);
        Bitmap bitmap = Bitmap.createBitmap(plot.getWidth(), plot.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        plot.draw(canvas);
        return bitmap;
    }

}
