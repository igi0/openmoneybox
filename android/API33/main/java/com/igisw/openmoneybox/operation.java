/* **************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-15
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.preference.PreferenceManager;

import java.util.Currency;
import java.util.Locale;

public class operation extends Activity {

	private EditText value_edit;
	private AutoCompleteTextView matters_View;
	private Spinner fund_spinner;
	private Spinner category_spinner;
	private CheckBox currency_checkbox;
	private EditText currency_symbol;
	private EditText currency_rate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operation);
		
		Bundle bundle=getIntent().getExtras();

		TextView caption = findViewById(R.id.Title);
		TextView fund_label = findViewById(R.id.fundLabel);
		TextView category_label = findViewById(R.id.categoryLabel);
		value_edit = findViewById(R.id.opValue);
		matters_View = findViewById(R.id.Matter);

		currency_checkbox = findViewById(R.id.currencyCheckbox);
		currency_symbol = findViewById(R.id.currency);
		currency_rate = findViewById(R.id.rate);

		// Create the OnClickListener
		TextView OkButton = findViewById(R.id.OkBtn);
		View.OnClickListener clickListener = v -> {
			if (v == currency_checkbox) currencyClick(v);
			else if (v == OkButton) okBtnClick(v);
		};
		currency_checkbox.setOnClickListener(clickListener);
		OkButton.setOnClickListener(clickListener);

		currency_symbol.setText(Currency.getInstance(Locale.getDefault()).getSymbol());

		SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(this);
		if(Opts.getBoolean("GDarkTheme", false)) {
			this.setTheme(R.style.DarkTheme);
			LinearLayout ll = findViewById(R.id.ll_operation);
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				Resources.Theme theme = getTheme();
				ll.setBackgroundColor(getResources().getColor(R.color.black, theme));
				fund_label.setBackgroundColor(getResources().getColor(R.color.black, theme));
				fund_label.setTextColor(getResources().getColor(R.color.white, theme));
				category_label.setBackgroundColor(getResources().getColor(R.color.black, theme));
				category_label.setTextColor(getResources().getColor(R.color.white, theme));
				value_edit.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				value_edit.setTextColor(getResources().getColor(R.color.white, theme));
				matters_View.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				matters_View.setTextColor(getResources().getColor(R.color.white, theme));

				currency_checkbox.setTextColor(getResources().getColor(R.color.white, theme));
				currency_symbol.setTextColor(getResources().getColor(R.color.white, theme));
				currency_symbol.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
				currency_rate.setTextColor(getResources().getColor(R.color.white, theme));
				currency_rate.setHintTextColor(getResources().getColor(R.color.highlight_dark, theme));
			}
			else{
				ll.setBackgroundColor(getResources().getColor(R.color.black));
				fund_label.setBackgroundColor(getResources().getColor(R.color.black));
				fund_label.setTextColor(getResources().getColor(R.color.white));
				category_label.setBackgroundColor(getResources().getColor(R.color.black));
				category_label.setTextColor(getResources().getColor(R.color.white));
				value_edit.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				value_edit.setTextColor(getResources().getColor(R.color.white));
				matters_View.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				matters_View.setTextColor(getResources().getColor(R.color.white));

				currency_checkbox.setTextColor(getResources().getColor(R.color.white));
				currency_symbol.setTextColor(getResources().getColor(R.color.white));
				currency_symbol.setHintTextColor(getResources().getColor(R.color.highlight_dark));
				currency_rate.setTextColor(getResources().getColor(R.color.white));
				currency_rate.setHintTextColor(getResources().getColor(R.color.highlight_dark));
			}

		}

		if(bundle.getBoolean("change_labels")){ //NON-NLS
			caption.setText(getResources().getString(R.string.expense_caption));
			fund_label.setText(getResources().getString(R.string.expense_fund));
			value_edit.setHint(getResources().getString(R.string.hint_insert_value));
			matters_View.setHint(getResources().getString(R.string.hint_insert_reason));
			category_label.setText(getResources().getString(R.string.expense_category));
		}
		else caption.setText(getResources().getString(R.string.profit_caption));

		// Autocomplete matters
		ArrayAdapter<String> matters_adapter = new ArrayAdapter<>(this,
            android.R.layout.simple_dropdown_item_1line, bundle.getStringArrayList("matters")); //NON-NLS
        matters_View.setAdapter(matters_adapter);

        // Fill spinner w/ funds
        fund_spinner = findViewById(R.id.Fund);
        ArrayAdapter<String> funds_adapter = new ArrayAdapter<>(this,
             android.R.layout.simple_spinner_item, bundle.getStringArrayList("funds")); //NON-NLS
        funds_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fund_spinner.setAdapter(funds_adapter);
        //noinspection SpellCheckingInspection
        fund_spinner.setSelection(bundle.getInt("defaultfundindex")); //NON-NLS

        // Fill spinner w/ categories
        category_spinner = findViewById(R.id.Category);
        ArrayAdapter<String> categories_adapter = new ArrayAdapter<>(this,
             android.R.layout.simple_spinner_item, bundle.getStringArrayList("categories")); //NON-NLS
        categories_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category_spinner.setAdapter(categories_adapter);

        currencyClick(null);
	}
	
	public void okBtnClick(@SuppressWarnings("unused") View view){ //NON-NLS
		double cur;
		int ReturnCode;
		
		omb_library.appContext = getApplicationContext();

		if(value_edit.getText().toString().isEmpty()){
			omb_library.Error(25, "");
			value_edit.requestFocus();
			return;}
		cur = Double.parseDouble(value_edit.getText().toString());
		String matter = matters_View.getText().toString();
		if(matter.isEmpty()){
			omb_library.Error(29, "");
			matters_View.requestFocus();
			return;}

		if(! currency_checkbox.isChecked()){
			if(currency_symbol.getText().toString().isEmpty()){
				omb_library.Error(49, "");
				currency_symbol.requestFocus();
				return;}
			if(currency_rate.getText().toString().isEmpty()){
				omb_library.Error(25, "");
				currency_rate.requestFocus();
				return;}

		}

		matter = omb_library.iUpperCase(matter);
		matter = matter.trim();

		// Create intent w/ result
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putString("fund", fund_spinner.getSelectedItem().toString()); //NON-NLS
		bundle.putDouble("value", cur); //NON-NLS
		bundle.putString("matter", matter); //NON-NLS
		bundle.putInt("category", category_spinner.getSelectedItemPosition()); //NON-NLS

		long curr_index;
		double curr_rate;
		if(currency_checkbox.isChecked()){
			curr_index = -1;
			curr_rate = 1;
		}
		else{
			curr_index = 1;
			curr_rate = Double.parseDouble(currency_rate.getText().toString());
		}
		bundle.putLong("currency", curr_index); //NON-NLS
		bundle.putDouble("rate", curr_rate); //NON-NLS
		bundle.putString("symbol", currency_symbol.getText().toString()); //NON-NLS

		intent.putExtras(bundle);
				
		ReturnCode = RESULT_OK;
		setResult(ReturnCode, intent);
		finish();
	}

	public void currencyClick(@SuppressWarnings("unused") View view){ //NON-NLS
		boolean show = ! currency_checkbox.isChecked();

		if(show){
			currency_symbol.setVisibility(View.VISIBLE);
			currency_rate.setVisibility(View.VISIBLE);
		}
		else{
			currency_symbol.setVisibility(View.GONE);
			currency_rate.setVisibility(View.GONE);
		}

	}
}
