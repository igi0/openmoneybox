/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-08-19
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

class CreditDebt_wrapper {
    //private final int id;
    final String name, value;
    final String badgeUri;

    CreditDebt_wrapper(/*int id, */String name, String value, String badgeUri) {
        //this.id = id;
        this.name = name;
        this.value = value;
        this.badgeUri = badgeUri;
    }
}
