/* *************************************************************
 * Name:      
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-10-03
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import static com.igisw.openmoneybox.constants.cs_fundgroups;
import static com.igisw.openmoneybox.constants.ombFileFormat.bilFFORMAT_UNKNOWN;
import static com.igisw.openmoneybox.constants.ombFileFormat.ombFFORMAT_34;
import static com.igisw.openmoneybox.constants.ombFileFormat.ombFFORMAT_35;
import static com.igisw.openmoneybox.constants.ombFileFormat.ombFFORMAT_35_CipherV4;
import static com.igisw.openmoneybox.constants.ombFileFormat.ombWRONGPASSWORD;
import static com.igisw.openmoneybox.constants.shortVersion;
import static com.igisw.openmoneybox.omb_library.getDocumentFolder;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.provider.ContactsContract;
import android.util.Xml;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

import net.zetetic.database.sqlcipher.SQLiteConnection;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import net.zetetic.database.sqlcipher.SQLiteDatabaseHook;

import org.jetbrains.annotations.NonNls;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Objects;

public class omb35core
{
	private SharedPreferences Opts;
	public MainActivity frame;

	public SQLiteDatabase database;
	private SQLiteDatabase AttachedDB;	// NOTE (igor#1#): Hidden feature
	boolean successfulKey = false;	//	set to true when Key is ok

	public enum TTypeVal {tvFou, tvCre, tvDeb}
	//public TTypeVal typeVal;

	public static class TVal{
		public int Id;
		public String Name;
		public double Value;
		public long ContactIndex;
		TVal(){
			Name = "";
			Value = 0;
			ContactIndex = -1;
		}
	}

	public enum TOpType{
		toNULL,
		toGain, /** @noinspection SpellCheckingInspection*/ toExpe,
		toSetCre, toRemCre, toConCre,
		toSetDeb, toRemDeb, toConDeb,
		toGetObj, toGivObj,
		toLenObj, toBakObj,
		toBorObj, toRetObj
	}
	//public TOpType opType;

	public enum TObjType {toPre, toInP}
	//public TObjType objType;

	public static class TObj{
		public int Id;
		public String Name, Object;
		public GregorianCalendar Alarm;
		public long ContactIndex;
		TObj(){
			Id = -1;
			Name = "";
			Object = "";
			Alarm = new GregorianCalendar();
			Alarm.set(1900, 1, 1);
			ContactIndex = -1;
		}
	}

	public static class TShopItem{
		public int Id;
		public String Name;
		public GregorianCalendar Alarm;
		TShopItem(){
			Id = -1;
			Name = "";
			Alarm = new GregorianCalendar();
			Alarm.set(1900, 1, 1);
		}
	}

	public static class TLine{
		//int Id;
		public boolean IsDate;
		public GregorianCalendar Date, Time;
		public TOpType Type;
		public String Value, Reason;
		public long CategoryIndex;

		public long ContactIndex;
		public double Latitude;
		public double Longitude;

		public long currencyIndex;
		protected double currencyRate;
		public String currencySymbol;

		TLine(){
			//Id = -1;
			IsDate = false;
			Date = new GregorianCalendar();
			Date.set(1900, 1, 1);
			Time = new GregorianCalendar();
			Time.set(1900, 1, 1);
			Type = TOpType.toNULL;
			Value = "";
			Reason = "";
			CategoryIndex = -1;

			ContactIndex = -1;
			Latitude = constants.ombInvalidLatitude;
			Longitude = constants.ombInvalidLongitude;

			currencyIndex = -1;
			currencyRate = 1;
			currencySymbol = "";
		}
	}

	public static class TCategory{
		int Id;
		String Name;
		boolean test;
		int iconId;
		TCategory(){
			Id = -1;
			Name = "";
			test = false;
			iconId = -1;
		}
	}

	private boolean Parsing;	// Set to true during database parsing
	public boolean ExternalFileAttached;	// NOTE (igor#1#): Hidden feature
	public boolean DatabaseIsSqlCipher_V4;	// Document SqlCipher Compatibility is V4
	public GregorianCalendar Day;
	public int NFun, NCre, NDeb, NLen, NBor, NSho, NLin, NCat;
	public static class ACC{
		public boolean Modified,ReadOnly;
		int Year;
		//public GregorianCalendar DateStamp;
		public long Month;
		public File FileName;
		public String DefFund;}

	public final ACC FileData;

	double Tot_Funds;
	double Tot_Credits;
	double Tot_Debts;

	public final ArrayList<TVal> Funds;
	public final ArrayList<TVal> Credits;
	public final ArrayList<TVal> Debts;
	public final ArrayList<TObj> Lent;
	public final ArrayList<TObj> Borrowed;
	public final ArrayList<TShopItem> ShopItems;
    public final ArrayList<TLine> Lines;
	public final ArrayList<String> MattersBuffer;
	public final ArrayList<TCategory> CategoryDB;

    public omb35core(String document, MainActivity mainframe){
		if(mainframe != null){
			Opts = PreferenceManager.getDefaultSharedPreferences(mainframe);
			frame = mainframe;
		}
		// Data creation
		Funds = new ArrayList<>();
		Credits = new ArrayList<>();
		Debts = new ArrayList<>();
		Lent = new ArrayList<>();
		Borrowed = new ArrayList<>();
		ShopItems = new ArrayList<>();
		Lines = new ArrayList<>();
		MattersBuffer = new ArrayList<>();
		MattersBuffer.add(null);

		CategoryDB = new ArrayList<>();

		FileData = new ACC();

		initialize(document);

		openDatabase(document);
	}

	public boolean findContact(String contact_id, String contact_name){
		boolean contact_exists = false;
		if(! tableExists(database, "Contacts"))
			database.execSQL(constants.cs_contacts);
		else{
			Cursor contactsTable = database.query(
					"Contacts",	//NON-NLS
					new String[] { "contact", "status" },	//NON-NLS
					"mobile_id = " + contact_id,	//NON-NLS
					null, null, null, null);
			while (contactsTable.moveToNext()) {
				String name = contactsTable.getString(0);
				if(contactsTable.getInt(1) > 0){
					if(contact_name.compareTo(name) == 0){
						contact_exists = true;
						break;
					}
				}
			}
			contactsTable.close();
		}

		if(! contact_exists){
			ContentValues con = new ContentValues();
			con.put("mobile_id", contact_id);	//NON-NLS
			con.put("contact", contact_name);	//NON-NLS
			con.put("status", true);	//NON-NLS
			if(database.insert("Contacts", null, con) == -1)	//NON-NLS
				return false;

			generateContactImage(contact_id);
		}

		return true;
	}

	private void updateContactTable(String contact_id){
		boolean contact_exists = false;
		String name = null;
		//String valid = null;
		// No need to check table existence
		Cursor contactsTable = database.query(
				"Contacts",	//NON-NLS
				new String[] { "mobile_id", "contact" },	//NON-NLS
				"mobile_id = " + contact_id, 	//NON-NLS
				null, null, null, null);
		while (contactsTable.moveToNext()) {
			if(contact_id.compareTo(contactsTable.getString(0)) == 0){
				name = contactsTable.getString(1);
				//valid = contactsTable.getString(2);
				contact_exists = true;
				break;
			}
		}
		contactsTable.close();

		if(contact_exists){

			// The index of the _ID column in the Cursor
			int mIdColumn;
			int mNameColumn;

			Cursor phones = frame.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
					null, null, null,
					ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY + " ASC");	//NON-NLS
			boolean found_name = false;
			String phone_id = null;
			while (phones.moveToNext()) {
				// Gets the primary name column index
				mNameColumn = phones.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY);
				String phone_name = phones.getString(mNameColumn);
				if(phone_name != null) {
					if (phone_name.compareTo(name) == 0) {
						found_name = true;

						mIdColumn = phones.getColumnIndex(ContactsContract.Contacts._ID);
						phone_id = phones.getString(mIdColumn);

						break;
					}
				}
			}
			phones.close();

			ContentValues cv = new ContentValues();
			if(found_name){
				// Update Contacts table
				cv.put("mobile_id", phone_id);	//NON-NLS
				database.update("Contacts", cv, "mobile_id=?",	//NON-NLS
						new String[]{contact_id});

				cv.clear();
				cv.put("contact_index", phone_id);	//NON-NLS

				// Update Transactions table
				database.update("Transactions", cv, "contact_index=?",	//NON-NLS
						new String[]{contact_id});
				// Update Credits table
				if(tableExists(database, "Credits"))
					database.update("Credits", cv, "contact_index=?",	//NON-NLS
							new String[]{contact_id});
				// Update Debts table
				if(tableExists(database, "Debts"))
					database.update("Debts", cv, "contact_index=?",	//NON-NLS
							new String[]{contact_id});
				// Update Borrows table
				if(tableExists(database, "Borrows"))
					database.update("Borrows", cv, "contact_index=?",	//NON-NLS
							new String[]{contact_id});
				// Update Loans table
				if(tableExists(database, "Loans"))
					database.update("Loans", cv, "contact_index=?",	//NON-NLS
							new String[]{contact_id});

				// TODO: update master database

				boolean modified = FileData.Modified;
				parseDatabase();
				FileData.Modified = modified;

				generateContactImage(phone_id);

			}
			else{
				cv.put("status", false);	//NON-NLS
				database.update("Contacts", cv, "mobile_id=?", new String[]{contact_id});	//NON-NLS
			}

		}
	}

	private void generateContactImage(String id){
		// Retrieve contact image
		String img = getContactImage(Integer.parseInt(id));
		if(img == null) return;
        // Path selection
		omb_library.appContext = frame.getApplicationContext();
        //noinspection SpellCheckingInspection
        String dir = getDocumentFolder() + "/ombContactImgs";	//NON-NLS
        File file = new File (dir);
        if(! file.exists())
            if (! file.mkdir()) return;

        String filename = dir + "/" + id + ".png";

        Bitmap bmp = omb_library.loadContactPhotoThumbnail(img);
        if (bmp == null) return;
        OutputStream os;
        try {
            os = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            return;
        }
        bmp.compress(Bitmap.CompressFormat.PNG, 100, os);
        try {
            os.close();
        } catch (IOException ignored) {

        }
	}

	public boolean AddOperation(GregorianCalendar D, GregorianCalendar O, TOpType T, String V,
						   String M, long N, long c_index,
						   boolean HasLocation, double lat, double lon,
						   long curr_Id, double curr_Rate, String Curr_Symbol)
	{
		if(T == TOpType.toNULL || V.isEmpty() || M.isEmpty())return false;
		String Tot = omb_library.FormDigits(Tot_Funds, true);
		if(Parsing){
			TLine temp = new TLine();
			//temp.Id = id;
			temp.IsDate = false;
			temp.Date = D;
			temp.Time = O;
			temp.Type = T;
			temp.Value = V;
			temp.Reason = M;
			temp.CategoryIndex = N;
			temp.ContactIndex = c_index;
			temp.Latitude = lat;
			temp.Longitude = lon;
			temp.currencyIndex = curr_Id;
			temp.currencyRate = curr_Rate;
			temp.currencySymbol = Curr_Symbol;
			Lines.add(NLin, temp);
		}
		else{
			// Last date check
			boolean found = false;
			if(NLin < 1){
				addDate(D, Tot);
				found = true;
			}
			else for(int i = NLin - 1; i >= 0; i--)if(Lines.get(i).IsDate){
				GregorianCalendar cal1 = Lines.get(i).Date;
				boolean SameDay = cal1.get(Calendar.YEAR) == D.get(Calendar.YEAR) &&
						cal1.get(Calendar.DAY_OF_YEAR) == D.get(Calendar.DAY_OF_YEAR);
				if(SameDay){
					found = true;
					break;}}
			if (! found) {
				double TotFundAttached = Tot_Funds;
				// NOTE (igor#1#): Hidden feature
				if (ExternalFileAttached) {
					Cursor AttachedFundTable = getExternalFundTable();
					for (int j = 0; j < AttachedFundTable.getCount(); j++) {
						AttachedFundTable.moveToPosition(j);
						TotFundAttached += AttachedFundTable.getDouble(2);
					}
				}
				// ------------------------------
				addDate(D, /*Tot*/omb_library.FormDigits(TotFundAttached, true));
			}

			ContentValues cv = new ContentValues();

            //noinspection SpellCheckingInspection
			cv.put("isdate", false);	//NON-NLS
			cv.put("date", D.getTimeInMillis() / 1000);	//NON-NLS
			cv.put("time", O.getTimeInMillis() / 1000);	//NON-NLS

			int value = 0;
			switch (T) {
				case toGain:
					value = 1;
					break;
				case toExpe:
					value = 2;
					break;
				case toSetCre:
					value = 3;
					break;
				case toRemCre:
					value = 4;
					break;
				case toConCre:
					value = 5;
					break;
				case toSetDeb:
					value = 6;
					break;
				case toRemDeb:
					value = 7;
					break;
				case toConDeb:
					value = 8;
					break;
				case toGetObj:
					value = 9;
					break;
				case toGivObj:
					value = 10;
					break;
				case toLenObj:
					value = 11;
					break;
				case toBakObj:
					value = 12;
					break;
				case toBorObj:
					value = 13;
					break;
				case toRetObj:
					value = 14;
					break;
			}

			cv.put("type", value);	//NON-NLS
			cv.put("value", V);	//NON-NLS
			cv.put("reason", M);	//NON-NLS
			cv.put("cat_index", N);	//NON-NLS
			cv.put("contact_index", c_index);	//NON-NLS
			if(HasLocation) {
				cv.put("latitude", lat);	//NON-NLS
				cv.put("longitude", lon);	//NON-NLS
			}
			else{
				cv.put("latitude", constants.ombInvalidLatitude);	//NON-NLS
				cv.put("longitude", constants.ombInvalidLongitude);	//NON-NLS
			}

            //noinspection SpellCheckingInspection
            cv.put("currencyid", curr_Id);	//NON-NLS
			//noinspection SpellCheckingInspection
			cv.put("currencyrate", curr_Rate);	//NON-NLS
			//noinspection SpellCheckingInspection
			cv.put("currencysymb", Curr_Symbol);	//NON-NLS
			database.insert("Transactions", null, cv);	//NON-NLS

			FileData.Modified=true;
			parseDatabase();
		}
		return true;
	}

	private void openDatabase(String File){
		String pwd = getKey(false);

		java.io.File f = new File(File);
		boolean file_exist = f.exists();

		SQLiteDatabaseHook hook = null, usedHook;
		if(! file_exist) {
			hook = new SQLiteDatabaseHook() {
				public void preKey(SQLiteConnection connection) {
					connection.execute(constants.cipher_compat_sql_v4, null, null);
				}

				public void postKey(SQLiteConnection connection) {
				}
			};
			if(pwd.isEmpty()) usedHook = null;
			else usedHook = hook;
			database = SQLiteDatabase.openOrCreateDatabase(File, pwd, null, null, usedHook);
			database.close();
		}

		constants.ombFileFormat format = omb_library.checkFileFormat(File, pwd);
		if((format == bilFFORMAT_UNKNOWN) && file_exist){
			omb_library.Error(2, File);
			Parsing = false;
			return;
		} else if (format == ombWRONGPASSWORD) {
			Parsing = false;
			return;
		}
		else if(format == ombFFORMAT_35_CipherV4){
			frame.finish();
			return;
		}

		if(pwd.isEmpty()) usedHook = null;
		else usedHook = hook;
		database = SQLiteDatabase.openOrCreateDatabase(File, pwd, null, null, usedHook);

		boolean CompatVersion;
		if(Opts != null)
			CompatVersion = Opts.getBoolean("DB_SqlCipherCompatVersion", false);	//NON-NLS
		else CompatVersion = true;
		boolean Database_v4 = (format == ombFFORMAT_35);

		File fMaster;
		SQLiteDatabase db;

		boolean Archive_v4;
		if(frame != null) {
			fMaster = frame.getDatabasePath(constants.archive_name);
			String master = fMaster.toString();

			boolean master_exist = fMaster.exists();

			if(master_exist) {
				// Check and try migration of master database
				String pwdMaster = getKey(true);
				constants.ombFileFormat formatMaster = omb_library.checkFileFormat(master, pwdMaster);
				if (/*(*/formatMaster == bilFFORMAT_UNKNOWN/*) && master_exist*/) {
					omb_library.Error(2, File);
					Parsing = false;
					return;
				} else if (formatMaster == ombWRONGPASSWORD) {
					Parsing = false;
					return;
				}

				db = SQLiteDatabase.openOrCreateDatabase(master, pwdMaster, null, null);
				if (formatMaster == ombFFORMAT_34) {
					db.setVersion(constants.dbVersion);
					formatMaster = ombFFORMAT_35;
				}
				db.close();

				Archive_v4 = (formatMaster == ombFFORMAT_35);
			}
			else Archive_v4 = false;
		}
		else Archive_v4 = false;

		DatabaseIsSqlCipher_V4 = (CompatVersion && Database_v4 && Archive_v4);

		// Set restore savepoint
		database.execSQL("SAVEPOINT roll_back;");	//NON-NLS

		if(file_exist){

			successfulKey = true;

			switch(format){
				/*
				case bilFFORMAT_UNKNOWN:	// Unknown format
					omb_library.Error(2, File);
					Parsing = false;
					return;
				*/
				case ombFFORMAT_35:
					break;
				case ombFFORMAT_34:
					database.execSQL(cs_fundgroups);
					database.setVersion(constants.dbVersion);
					database.execSQL("RELEASE roll_back;");	//NON-NLS
					break;
				// Uncomment following lines to compile with bilFFORMAT_32x support
				/*
				case ombFFORMAT_32:
					database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Categories ADD COLUMN iconid INTEGER;");
					cv = new ContentValues();
					cv.put("iconid", -1);
					database.update("Categories", cv, null, null);
					database.setVersion(constants.dbVersion);
					database.execSQL_IGI_INSIDER_OVERRIDE("RELEASE roll_back;");
					if(format == ombFFORMAT_332)
						break;
				case ombFFORMAT_332:
				*/

				// Uncomment following lines to compile with bilFFORMAT_33x support
				/*
				case ombFFORMAT_33:
					database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN currencyid INTEGER;");
					database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN currencyrate REAL;");
					database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN currencysymb TEXT;");

					cv = new ContentValues();
					cv.put("currencyid", -1);
					database.update("Transactions", cv, "isdate = 0", null);
					database.setVersion(constants.dbVersion);
					database.execSQL_IGI_INSIDER_OVERRIDE("RELEASE roll_back;");

					checkMaster();

					break;
				*/

				// Uncomment following lines to compile with bilFFORMAT_31x support
				/*
				case ombFFORMAT_31:
					database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN contact_index INTEGER;");
					database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN latitude REAL;");
					database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN longitude REAL;");

					cv = new ContentValues();
					cv.put("latitude", omb_library.FormDigits(constants.ombInvalidLatitude, true));
					cv.put("longitude", omb_library.FormDigits(constants.ombInvalidLongitude, true));
					database.update("Transactions", cv, "isdate = 0", null);

					if(tableExists(database, "Credits"))
						database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Credits ADD COLUMN contact_index INTEGER;");
					if(tableExists(database, "Debts"))
						database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Debts ADD COLUMN contact_index INTEGER;");
					if(tableExists(database, "Borrows"))
						database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Borrows ADD COLUMN contact_index INTEGER;");
					if(tableExists(database, "Loans"))
						database.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Loans ADD COLUMN contact_index INTEGER;");
					database.setVersion(constants.dbVersion);
					database.execSQL_IGI_INSIDER_OVERRIDE("RELEASE roll_back;");

					fMaster = frame.getDatabasePath(constants.archive_name);
					master = fMaster.toString();
					if(fMaster.exists()){
						if(omb_library.checkFileFormat(master, pwd_archive) == ombFFORMAT_31) {
							String colName;
							Cursor tableList;
							db = SQLiteDatabase.openOrCreateDatabase(master, pwd_archive, null);

							db.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN contact_index INTEGER;");
							db.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN latitude REAL;");
							db.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN longitude REAL;");

							db.update("Transactions", cv, "isdate = 0", null);

							tableList = db.query("SELECT name FROM sqlite_master WHERE type='table';", null);
							for(int i = 0; i < tableList.getCount(); i++){
								tableList.moveToPosition(i);
								colName = tableList.getString(0);
								if(colName.startsWith("Credits"))
									db.execSQL_IGI_INSIDER_OVERRIDE(String.format("ALTER TABLE %s ADD COLUMN contact_index INTEGER;", colName));
								else if(colName.startsWith("Debts"))
									db.execSQL_IGI_INSIDER_OVERRIDE(String.format("ALTER TABLE %s ADD COLUMN contact_index INTEGER;", colName));
								else if(colName.startsWith("Loans"))
									db.execSQL_IGI_INSIDER_OVERRIDE(String.format("ALTER TABLE %s ADD COLUMN contact_index INTEGER;", colName));
								else if(colName.startsWith("Borrows"))
									db.execSQL_IGI_INSIDER_OVERRIDE(String.format("ALTER TABLE %s ADD COLUMN contact_index INTEGER;", colName));
							}
							db.setVersion(constants.dbVersion);
							db.execSQL_IGI_INSIDER_OVERRIDE("RELEASE roll_back;");

							tableList.close();
						}
					}

					break;
				*/

				// Uncomment following lines to compile with bilFFORMAT_302 support
				/*
				case bilFFORMAT_302:
					omb30core olddoc = new omb30core(File);
					int i;
					//double cur;

					database = SQLiteDatabase.openOrCreateDatabase(File, null);

					// Write Funds
					if(! tableExists(database, "Funds")) database.execSQL_IGI_INSIDER_OVERRIDE(constants.cs_funds);
					for(i = 0; i < olddoc.NFon; i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.Funds.get(i).Name);
						cv.put("value", olddoc.Funds.get(i).Value);
						database.insert("Funds", null, cv);
					}
					// Write default fund
					if(! tableExists(database, "Information")){
						database.execSQL_IGI_INSIDER_OVERRIDE(constants.cs_information);

						ContentValues cv = new ContentValues();

						String metadata = "OS: Android " + android.os.Build.VERSION.CODENAME + " "
								+ android.os.Build.VERSION.RELEASE;
						cv.put("id", String.format("%d", constants.dbMeta_application_info));
						cv.put("data", metadata);
						database.insert("Information", null, cv);

						cv.clear();
						cv.put("id", String.format("%d", constants.dbMeta_default_fund));
						cv.put("data", olddoc.FileData.DefFund);
						database.insert("Information", null, cv);
					}

					// Write Credits
					if(! tableExists(database, "Credits")) database.execSQL_IGI_INSIDER_OVERRIDE(constants.cs_credits);
					for(i = 0; i < olddoc.NCre; i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.Credits.get(i).Name);
						cv.put("value", olddoc.Credits.get(i).Value);
						database.insert("Credits", null, cv);
					}

					// Write Debts
					if(! tableExists(database, "Debts")) database.execSQL_IGI_INSIDER_OVERRIDE(constants.cs_debts);
					for(i = 0; i < olddoc.NDeb; i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.Debts.get(i).Name);
						cv.put("value", olddoc.Debts.get(i).Value);
						database.insert("Debts", null, cv);
					}

					// Write Loans
					if(! tableExists(database, "Loans")) database.execSQL_IGI_INSIDER_OVERRIDE(constants.cs_loans);
					for(i = 0; i < olddoc.NLen; i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.Lent.get(i).Name);
						cv.put("item", olddoc.Lent.get(i).Object);
						cv.put("alarm", olddoc.Lent.get(i).Alarm.getTimeInMillis() / 1000);
						database.insert("Loans", null, cv);
					}

					// Write Borrows
					if(! tableExists(database, "Borrows")) database.execSQL_IGI_INSIDER_OVERRIDE(constants.cs_borrows);
					for(i = 0; i < olddoc.NBor; i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.Lent.get(i).Name);
						cv.put("item", olddoc.Lent.get(i).Object);
						cv.put("alarm", olddoc.Lent.get(i).Alarm.getTimeInMillis() / 1000);
						database.insert("Borrows", null, cv);
					}

					// Write Categories
					if(! tableExists(database, "Categories")) database.execSQL_IGI_INSIDER_OVERRIDE(constants.cs_categories);
					for(i = 0; i < olddoc.CategoryDB.size(); i++){
						ContentValues cv = new ContentValues();
						cv.put("name", olddoc.CategoryDB.get(i));
						cv.put("active", 1);
						database.insert("Categories", null, cv);
					}

					// Write Transactions
					if(! tableExists(database, "Transactions")) database.execSQL_IGI_INSIDER_OVERRIDE(constants.cs_transactions);
					for(i = 0; i < olddoc.NLin; i++){
						ContentValues cv = new ContentValues();
						cv.put("isdate", olddoc.Lines.get(i).IsDate);
						cv.put("date", olddoc.Lines.get(i).Date.getTimeInMillis() / 1000);
						cv.put("time", olddoc.Lines.get(i).Time.getTimeInMillis() / 1000);

						int value = 0;
						omb30core.TOpType T = olddoc.Lines.get(i).Type;
						if(T == omb30core.TOpType.toGain)value = 1;
						else if(T == omb30core.TOpType.toExpe)value = 2;
						else if(T == omb30core.TOpType.toSetCre)value = 3;
						else if(T == omb30core.TOpType.toRemCre)value = 4;
						else if(T == omb30core.TOpType.toConCre)value = 5;
						else if(T == omb30core.TOpType.toSetDeb)value = 6;
						else if(T == omb30core.TOpType.toRemDeb)value = 7;
						else if(T == omb30core.TOpType.toConDeb)value = 8;
						else if(T == omb30core.TOpType.toGetObj)value = 9;
						else if(T == omb30core.TOpType.toGivObj)value = 10;
						else if(T == omb30core.TOpType.toLenObj)value = 11;
						else if(T == omb30core.TOpType.toBakObj)value = 12;
						else if(T == omb30core.TOpType.toBorObj)value = 13;
						else if(T == omb30core.TOpType.toRetObj)value = 14;

						cv.put("type", value);
						String cnv = olddoc.Lines.get(i).Value.replace(",", ".");
						cv.put("value", cnv);
						cv.put("reason", olddoc.Lines.get(i).Reason);
						cv.put("cat_index", olddoc.Lines.get(i).CategoryIndex + 1);
						database.insert("Transactions", null, cv);

					}

					// Write shopping list
					if(! tableExists(database, "Shoplist")) database.execSQL_IGI_INSIDER_OVERRIDE(constants.cs_shoplist);
					for(i = 0; i < olddoc.NSho; i++){
						ContentValues cv = new ContentValues();
						cv.put("item", olddoc.ShopItems.get(i).Name);
						cv.put("alarm", olddoc.ShopItems.get(i).Alarm.getTimeInMillis() / 1000);
						database.insert("Shoplist", null, cv);

					}

					database.close();

					break;
				*/ // bilFFORMAT_302 ---
				default:
					Parsing = false;
					return;}
		}
		else database.setVersion(constants.dbVersion);

		// Write file metadata
		String metadata = "OS: Android " + android.os.Build.VERSION.CODENAME + " "	//NON-NLS
				+ android.os.Build.VERSION.RELEASE;

		ContentValues cv = new ContentValues();

		if (!tableExists(database, "Information")) {
			database.execSQL(constants.cs_information);

			cv.put("id", String.format(Locale.US, "%d", constants.dbMeta_application_info));	//NON-NLS
			cv.put("data", metadata);	//NON-NLS
			database.insert("Information", null, cv);	//NON-NLS
			cv.clear();
			cv.put("id", String.format(Locale.US, "%d", constants.dbMeta_default_fund));	//NON-NLS
			cv.put("data", "default");	//NON-NLS
			database.insert("Information", null, cv);	//NON-NLS
			cv.clear();
			cv.put("id", String.format(Locale.US, "%d", constants.dbMeta_mobile_export));	//NON-NLS
			cv.put("data", "");	//NON-NLS
			database.insert("Information", null, cv);	//NON-NLS
		} else {
			cv.put("data", metadata);	//NON-NLS
			database.update("Information", cv, "id = " + String.format(Locale.US, "%d", constants.dbMeta_application_info), null);	//NON-NLS

			ExternalFileAttached = false;
			// NOTE (igor#1#): Hidden feature
			Cursor Table = database.query("Information", new String[] { "data" },	//NON-NLS
					"id = " + String.format(Locale.US, "%d",	//NON-NLS
					constants.dbMeta_attached_db), null, null,
					null, null);
			if(Table.getCount() > 0){
				Table.moveToPosition(0);
				String attached_db_filename = Table.getString(0);

				if(! attached_db_filename.isEmpty()) {
					attached_db_filename = getDocumentFolder() +  "/" + attached_db_filename;
					if(omb_library.checkFileFormat(attached_db_filename, pwd) == ombFFORMAT_35) {
						File AttachedFile = new File(attached_db_filename);
						if (AttachedFile.exists()) {
							AttachedDB = SQLiteDatabase.openOrCreateDatabase(AttachedFile, pwd, null, null, hook);
							ExternalFileAttached = true;
						}
					}
				}
			}
			// ----------------------------------
		}

		// Set currency if not present (added later in dbVersion 31)
		Cursor currencyTable = database.query(
				"Information",	//NON-NLS
				new String[] { "data" }, 	//NON-NLS
				"id = " + String.format(Locale.US, "%d",	//NON-NLS
				constants.dbMeta_currency), null, null, null, null);
		if(currencyTable.getCount() == 0){
			String curr = Currency.getInstance(Locale.getDefault()).getSymbol();
			cv.clear();
			cv.put("id", String.format(Locale.US, "%d", constants.dbMeta_currency));	//NON-NLS
			cv.put("data", curr);	//NON-NLS
			database.insert("Information", null, cv);	//NON-NLS
		}

		// Transaction table init
		if (!tableExists(database, "Transactions"))
			database.execSQL(constants.cs_transactions);

		// Category table init
		if (!tableExists(database, "Categories"))
			database.execSQL(constants.cs_categories);

		parseDatabase();
		FileData.Modified = false;

		currencyTable.close();

		//checkMaster();
	}

    void parseDatabase(){
        Parsing = true;

        boolean is_date;
        int Rows, id, i;	// Rows: number of rows in the query table
        long ind,			// ind: category index of transaction
            contact_in;     // contact_in: contact index of transaction or object
        double val, latitude, longitude;
        TOpType type;
        String str, reason;
        GregorianCalendar date, time, LastDate = new GregorianCalendar();
        Cursor Table;

        // Read Funds
        if(tableExists(database, "Funds")){
            Table = database.query("Funds", null, null, null, null,null, "name");	//NON-NLS
            Funds.clear();
            NFun = 0;
            Tot_Funds = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                val = Table.getDouble(2);
                if(addValue(TTypeVal.tvFou, id, str, val, -1)){
                    NFun++;
                    Tot_Funds += val;}}}

        // Read default fund
        Table = database.query(
				"Information",	//NON-NLS
				new String[] { "data" }, "id = " + String.format(Locale.US, "%d",	//NON-NLS
				constants.dbMeta_default_fund), null, null, null, null);
        Table.moveToPosition(0);
        FileData.DefFund = Table.getString(0);

        // Read Credits
        if(tableExists(database, "Credits")){
            Table = database.query("Credits", null, null, null, null,null, "name");	//NON-NLS
            Credits.clear();
            NCre = 0;
            Tot_Credits = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
				Table.moveToPosition(i);
		        id = Table.getInt(0);
				str = Table.getString(1);
	            val = Table.getDouble(2);
				contact_in = Table.getInt(3);
                if(addValue(TTypeVal.tvCre, id, str, val, contact_in)){
                    NCre++;
                    Tot_Credits += val;}}}

        // Read Debts
        if(tableExists(database, "Debts")){
            Table = database.query("Debts", null, null, null, null,null, "name");	//NON-NLS
            Debts.clear();
            NDeb = 0;
            Tot_Debts = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                val = Table.getDouble(2);
				contact_in = Table.getInt(3);
                if(addValue(TTypeVal.tvDeb, id, str, val, contact_in)){
                    NDeb++;
                    Tot_Debts += val;}}}

        // Read Loans
        if(tableExists(database, "Loans")){
            Table = database.query("Loans", null, null, null, null,null, "name");	//NON-NLS
            Lent.clear();
            NLen = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                reason = Table.getString(2);
                ind = Table.getInt(3);
                date = new GregorianCalendar();
                if(ind == -1) date.add(Calendar.YEAR, 100);
                else date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
				contact_in = Table.getInt(4);
                if(addObject(TObjType.toPre, id, str, reason, date, contact_in)) NLen++;}}

        // Read Borrows
        if(tableExists(database, "Borrows")){
            Table = database.query("Borrows", null, null, null, null,null, "name");	//NON-NLS
            Borrowed.clear();
            NBor = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                reason = Table.getString(2);
                ind = Table.getInt(3);
                date = new GregorianCalendar();
                if(ind == -1) date.add(Calendar.YEAR, 100);
                else date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
				contact_in = Table.getInt(4);
                if(addObject(TObjType.toInP, id, str, reason, date, contact_in)) NBor++;}}

        // Read Categories
		int iconIndex;
        Table = database.query("Categories", null, null, null, null,null, "name");	//NON-NLS
        CategoryDB.clear();
        NCat = 0;
        Rows = Table.getCount();
        for (i = 0; i < Rows; i++){
            Table.moveToPosition(i)	;
			is_date = (Table.getInt(2) != 0);	// is_date used for 'active'
			if(is_date){
				id = Table.getInt(0);
				str = Table.getString(1);
				iconIndex = Table.getInt(3);
				addCategory(id, str, iconIndex);
			}
        }

        // Read custom category icons
		MainActivity.customIcons.clear();
		if(tableExists(database, "CustomIcons")){
            //noinspection SpellCheckingInspection
            String path = getDocumentFolder() + "/ombCategoryImgs/";
			@NonNls String imageFile;
			File file;
			Bitmap bmp;
			Table = database.query("CustomIcons", null, null, null, null,null, "id");	//NON-NLS
			Rows = Table.getCount();
			for(i = 0; i < Rows; i++){
				Table.moveToPosition(i);
				imageFile = path + Table.getString(1) + ".png";
				file = new File(imageFile);
				if(file.exists()){
					bmp = BitmapFactory.decodeFile(imageFile);
					if(bmp != null) MainActivity.customIcons.add(bmp);
				}
				else {
					TypedArray TArray = frame.getResources().obtainTypedArray(
						R.array.category_drawables_values);
					MainActivity.customIcons.add(
						((BitmapDrawable)
							TArray.getDrawable(0)).getBitmap());
					TArray.recycle();
				}
			}
		}

        // Read Transactions
        Table = database.query("Transactions", null, null, null, null,null, "date");	//NON-NLS
        if(Lines != null) Lines.clear();
        NLin = 0;
        Rows = Table.getCount();
        for (i = 0; i < Rows; i++){
            Table.moveToPosition(i);
            //id = Table.getInt(0);
            is_date = (Table.getInt(1) != 0);
            date = new GregorianCalendar();
            ind = Table.getInt(2);
            date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
            ind = Table.getInt(3);
            time = new GregorianCalendar();
            time.setTimeInMillis(ind * 1000);	// dates are stored in seconds

            str = Table.getString(5);
            reason = Table.getString(6);
            ind = Table.getInt(7);

            contact_in = Table.getInt(8);
			latitude = Table.getDouble(9);
			longitude = Table.getDouble(10);

			// Read Currency information
			int currId = Table.getInt(11);
			double currRate = 1;
			String CurrSymbol = "";
			if(currId >= 0){
				currRate = Table.getDouble(12);
				CurrSymbol = Table.getString(13);
			}

			if(is_date){
                if(addDate(date, str)) NLin++;
				LastDate = date;}
            else{
                switch(Table.getInt(4)){
                    case 1:
                        type = TOpType.toGain;
                        break;
                    case 2:
                        type = TOpType.toExpe;
                        break;
                    case 3:
                        type = TOpType.toSetCre;
                        break;
                    case 4:
                        type = TOpType.toRemCre;
                        break;
                    case 5:
                        type = TOpType.toConCre;
                        break;
                    case 6:
                        type = TOpType.toSetDeb;
                        break;
                    case 7:
                        type = TOpType.toRemDeb;
                        break;
                    case 8:
                        type = TOpType.toConDeb;
                        break;
                    case 9:
                        type = TOpType.toGetObj;
                        break;
                    case 10:
                        type = TOpType.toGivObj;
                        break;
                    case 11:
                        type = TOpType.toLenObj;
                        break;
                    case 12:
                        type = TOpType.toBakObj;
                        break;
                    case 13:
                        type = TOpType.toBorObj;
                        break;
                    case 14:
                        type = TOpType.toRetObj;
						break;
					case 0:
					default:
						type = TOpType.toNULL;
                }

                if(AddOperation(date, time, type, str, reason, ind, contact_in, true,
						latitude, longitude, currId, currRate, CurrSymbol))
                	NLin++;}
        }

        // Read shopping list
        //noinspection SpellCheckingInspection
        if(tableExists(database, "Shoplist")){
            //noinspection SpellCheckingInspection
            Table = database.query("Shoplist", null, null, null, null,null, "item collate nocase");	//NON-NLS
            ShopItems.clear();
            NSho = 0;
            Rows = Table.getCount();
            for (i = 0; i < Rows; i++){
                Table.moveToPosition(i);
                id = Table.getInt(0);
                str = Table.getString(1);
                ind = Table.getInt(2);
                date = new GregorianCalendar();
                if(ind == -1) date.add(Calendar.YEAR, 100);
                else date.setTimeInMillis(ind * 1000);	// dates are stored in seconds
                if(addShopItem(id, str, date)) NSho++;}}

        FileData.Year = LastDate.get(Calendar.YEAR);
        FileData.Month = LastDate.get(Calendar.MONTH);

        Parsing = false;

        Table.close();

    }

	public boolean addDate(GregorianCalendar D, String T)
	{
		if(! Parsing){
			int M, Y;
			Y = Day.get(GregorianCalendar.YEAR);
			M = Day.get(GregorianCalendar.MONTH);
			if((Y != FileData.Year) || (M != FileData.Month)){
				if(Opts.getBoolean("GConv", false))
					frame.textConvClick(null);

				String Path = FileData.FileName.getParent();
				String month = String.format(Locale.US, "%02d", FileData.Month + 1);	//NON-NLS
				String File = Opts.getString("GDPrefix", "OpenMoneyBox") + "_";
				File += String.format(Locale.US, "%d", FileData.Year);	//NON-NLS
				File += "-" + month + ".omb";
				FileData.Modified = true;

				File fMaster, fExport;
				SQLiteDatabase db, exp;
				fMaster = frame.getDatabasePath(constants.archive_name);
				String master = fMaster.toString();

				String MonthString = String.format(Locale.US, "%d_%02d", FileData.Year,	//NON-NLS
						FileData.Month + 1);
				String TailName = "_" + MonthString;
				String export = Path + "/" + TailName;
				fExport = new File(export);

				boolean master_exist = fMaster.exists();
				boolean export_exist = fExport.exists();

				db = SQLiteDatabase.openOrCreateDatabase(master, getKey(true), null, null);
				exp = SQLiteDatabase.openOrCreateDatabase(export, getKey(false), null, null);

				if(! master_exist) db.setVersion(constants.dbVersion);
				if(! export_exist) exp.setVersion(constants.dbVersion);

				// Write file metadata
				String metadata = "OS: Android " + android.os.Build.VERSION.CODENAME + " "	//NON-NLS
						+ android.os.Build.VERSION.RELEASE;

				ContentValues cv = new ContentValues();

				if(! tableExists(db, "Information")){
					db.execSQL(constants.cs_information);

					cv.put("id", String.format(Locale.US, "%d", constants.dbMeta_application_info));	//NON-NLS
					cv.put("data", metadata);	//NON-NLS
					db.insert("Information", null, cv);	//NON-NLS
					cv.clear();
					cv.put("id", String.format(Locale.US, "%d", constants.dbMeta_default_fund));	//NON-NLS
					cv.put("data", "default");	//NON-NLS
					db.insert("Information", null, cv);	//NON-NLS
				}
				else{
					cv.put("data", metadata);	//NON-NLS
					db.update("Information", cv, "id = " + String.format(Locale.US, "%d", constants.dbMeta_application_info), null);	//NON-NLS
				}

				if(! tableExists(exp, "Information")){
					exp.execSQL(constants.cs_information);

					cv.clear();
					cv.put("id", String.format(Locale.US, "%d", constants.dbMeta_application_info));	//NON-NLS
					cv.put("data", metadata);	//NON-NLS
					exp.insert("Information", null, cv);	//NON-NLS
					cv.clear();
					cv.put("id", String.format(Locale.US, "%d", constants.dbMeta_default_fund));	//NON-NLS
					cv.put("data", "default");	//NON-NLS
					exp.insert("Information", null, cv);	//NON-NLS
				}
				else{
					cv.put("data", metadata);	//NON-NLS
					exp.update("Information", cv, "id = " + String.format(Locale.US, "%d", constants.dbMeta_application_info), null);	//NON-NLS
				}

				// Transaction table init
				if(! tableExists(db, "Transactions")) db.execSQL(constants.cs_transactions);
				if(! tableExists(exp, "Transactions")) exp.execSQL(constants.cs_transactions);

				// Category table init
				boolean master_had_categories = tableExists(db, "Categories");
				if(! master_had_categories) db.execSQL(constants.cs_categories);
				if(! tableExists(exp, "Categories")) exp.execSQL(constants.cs_categories);

				if(! tableExists(db, "Funds" + TailName)) db.execSQL(String.format(constants.cs_funds_master, TailName));
				if(! tableExists(db, "Credits" + TailName)) db.execSQL(String.format(constants.cs_credits_master, TailName));
				if(! tableExists(db, "Debts" + TailName)) db.execSQL(String.format(constants.cs_debts_master, TailName));
				if(! tableExists(db, "Loans" + TailName)) db.execSQL(String.format(constants.cs_loans_master, TailName));
				if(! tableExists(db, "Borrows" + TailName)) db.execSQL(String.format(constants.cs_borrows_master, TailName));
				if(! tableExists(db, "Categories" + TailName)) db.execSQL(String.format(constants.cs_categories_master, TailName));
				if(! tableExists(db, "FundGroups" + TailName)) db.execSQL(String.format(constants.cs_fundgroups_master, TailName));

				if(! tableExists(exp, "Funds" + TailName)) exp.execSQL(String.format(constants.cs_funds_master, TailName));
				if(! tableExists(exp, "Credits" + TailName)) exp.execSQL(String.format(constants.cs_credits_master, TailName));
				if(! tableExists(exp, "Debts" + TailName)) exp.execSQL(String.format(constants.cs_debts_master, TailName));
				if(! tableExists(exp, "Loans" + TailName)) exp.execSQL(String.format(constants.cs_loans_master, TailName));
				if(! tableExists(exp, "Borrows" + TailName)) exp.execSQL(String.format(constants.cs_borrows_master, TailName));
				if(! tableExists(exp, "Categories" + TailName)) exp.execSQL(String.format(constants.cs_categories_master, TailName));
				if(! tableExists(exp, "FundGroups" + TailName)) exp.execSQL(String.format(constants.cs_fundgroups_master, TailName));

				database.execSQL("RELEASE roll_back;");	//NON-NLS
				// backup database
				try {
					omb_library.copyFile(FileData.FileName, Path + "/" + File);
				} catch (IOException e) {
					return false;
				}

				database.execSQL(String.format("ATTACH DATABASE '%s' AS master KEY '%s';", master, getKey(true)));	//NON-NLS
				database.execSQL(String.format("ATTACH DATABASE '%s' AS export KEY '%s';", export, getKey(false)));	//NON-NLS

				// Archive data in master database
				database.execSQL("INSERT INTO master.Transactions (isdate, date, time, type, value, " +	//NON-NLS
						"reason, cat_index, contact_index, latitude, longitude" +	//NON-NLS
						", currencyid, currencyrate, currencysymb" +	//NON-NLS
						") SELECT isdate, date, " +	//NON-NLS
						"time, type, value, reason, cat_index, contact_index, latitude, longitude" +	//NON-NLS
						", currencyid, currencyrate, currencysymb " +	//NON-NLS
						"FROM Transactions;");	//NON-NLS
				try {
					database.execSQL(String.format("INSERT INTO master.Funds%s SELECT * FROM Funds;", TailName));	//NON-NLS

					// NOTE (igor#1#): Hidden feature
					if(ExternalFileAttached){
						Cursor AttachedFundTable = getExternalFundTable();
						for(int j = 0; j < AttachedFundTable.getCount(); j++){
							AttachedFundTable.moveToPosition(j);
							cv.clear();
							cv.put("name", AttachedFundTable.getString(1) + " (*)");	//NON-NLS
							cv.put("value", AttachedFundTable.getDouble(2));	//NON-NLS

							db.insert(String.format("Funds%s", TailName), null, cv);	//NON-NLS
						}
					}
					// ------------------------------

					database.execSQL(String.format("INSERT INTO master.Credits%s SELECT * FROM Credits;", TailName));	//NON-NLS
					database.execSQL(String.format("INSERT INTO master.Debts%s SELECT * FROM Debts;", TailName));	//NON-NLS
					database.execSQL(String.format("INSERT INTO master.Loans%s SELECT * FROM Loans;", TailName));	//NON-NLS
					database.execSQL(String.format("INSERT INTO master.Borrows%s SELECT * FROM Borrows;", TailName));	//NON-NLS
					database.execSQL(String.format("INSERT INTO master.Categories%s SELECT * FROM Categories;", TailName));	//NON-NLS
					database.execSQL(String.format("INSERT INTO master.FundGroups%s SELECT * FROM FundGroups;", TailName));	//NON-NLS
				}
				catch(SQLiteConstraintException e){
					// Table in the try block already exist
					omb_library.Error(-1, null);
				}
				if(! master_had_categories) database.execSQL("INSERT INTO master.Categories SELECT * FROM Categories;");	//NON-NLS

				// Archive data in export database
				database.execSQL("INSERT INTO export.Transactions (isdate, date, time, type, value, " +	//NON-NLS
					"reason, cat_index, contact_index, latitude, longitude" +	//NON-NLS
					", currencyid, currencyrate, currencysymb" +	//NON-NLS
					") SELECT isdate, date, " +	//NON-NLS
					"time, type, value, reason, cat_index, contact_index, latitude, longitude " +	//NON-NLS
					", currencyid, currencyrate, currencysymb " +	//NON-NLS
					"FROM Transactions;");	//NON-NLS
				database.execSQL(String.format("INSERT INTO export.Funds%s SELECT * FROM Funds;", TailName));	//NON-NLS

				// NOTE (igor#1#): Hidden feature
				if(ExternalFileAttached){
					Cursor AttachedFundTable = getExternalFundTable();
					for(int j = 0; j < AttachedFundTable.getCount(); j++){
						AttachedFundTable.moveToPosition(j);
						cv.clear();
						cv.put("name", AttachedFundTable.getString(1) + " (*)");	//NON-NLS
						cv.put("value", AttachedFundTable.getDouble(2));	//NON-NLS
						exp.insert(String.format("Funds%s", TailName), null, cv);	//NON-NLS
					}
				}
				// ------------------------------

				database.execSQL(String.format("INSERT INTO export.Credits%s SELECT * FROM Credits;", TailName));	//NON-NLS
				database.execSQL(String.format("INSERT INTO export.Debts%s SELECT * FROM Debts;", TailName));	//NON-NLS
				database.execSQL(String.format("INSERT INTO export.Loans%s SELECT * FROM Loans;", TailName));	//NON-NLS
				database.execSQL(String.format("INSERT INTO export.Borrows%s SELECT * FROM Borrows;", TailName));	//NON-NLS
				if(! master_had_categories) database.execSQL("INSERT INTO export.Categories SELECT * FROM Categories;");	//NON-NLS
				database.execSQL(String.format("INSERT INTO export.Categories%s SELECT * FROM Categories;", TailName));	//NON-NLS
				database.execSQL(String.format("INSERT INTO export.FundGroups%s SELECT * FROM FundGroups;", TailName));	//NON-NLS

				db.close();
				exp.close();

				cv.clear();
				cv.put("data", MonthString);	//NON-NLS
				database.update("Information", cv, "id = " + String.format(Locale.US, "%d", constants.dbMeta_mobile_export), null);	//NON-NLS

				database.execSQL("DELETE FROM Transactions");	//NON-NLS
				// ________________________________

				database.execSQL("DETACH DATABASE 'master';");	//NON-NLS
				database.execSQL("DETACH DATABASE 'export';");	//NON-NLS
				database.execSQL("VACUUM");	//NON-NLS
				database.execSQL("SAVEPOINT roll_back;");	//NON-NLS

				Lines.clear();
				NLin = 0;
				FileData.Month = M;
				FileData.Year = Y;
			}}

		else	// Robustness code to avoid creation of duplicate date entries
			for(int i = NLin - 1; i >= 0; i--)if(Lines.get(i).IsDate)
				if(Lines.get(i).Date == D) return true;

		if(Parsing){
			TLine temp = new TLine();
			//temp.Id = id;
			temp.IsDate = true;
			temp.Date = D;
			//temp.Time.set(1900, 1, 1);
			temp.Type = TOpType.toNULL;
			temp.Value = T;
			temp.Reason = null;
			temp.CategoryIndex = -1;
			temp.ContactIndex = -1;

			temp.currencyIndex = -1;
			temp.currencyRate = 1;
			temp.currencySymbol	= "";
			Lines.add(NLin, temp);
		}
		else{
			ContentValues cv = getContentValues(D, T);
			database.insert("Transactions", null, cv);	//NON-NLS
		}
		FileData.Modified=true;
		return true;
	}

	@NonNull
	private static ContentValues getContentValues(GregorianCalendar D, String T) {
		ContentValues cv = new ContentValues();

		//noinspection SpellCheckingInspection
		cv.put("isdate", true);	//NON-NLS
		cv.put("date", D.getTimeInMillis() / 1000);	//NON-NLS
		cv.put("time", -1);	//NON-NLS
		cv.put("type", 0);	//NON-NLS
		cv.put("value", T);	//NON-NLS
		cv.put("reason", "");	//NON-NLS
		cv.put("cat_index", -1);	//NON-NLS
		cv.put("contact_index", -1);	//NON-NLS

		//noinspection SpellCheckingInspection
		cv.put("currencyid", -1);	//NON-NLS
		//noinspection SpellCheckingInspection
		cv.put("currencyrate", 1);	//NON-NLS
		//noinspection SpellCheckingInspection
		cv.put("currencysymb", "");	//NON-NLS
		return cv;
	}

	public boolean isDate(int R)
	{
		return Lines.get(R).IsDate;
	}

	//public QuickContactBadge createContactBadge(long c_id){
	public String getContactImage(long c_id){

		int mIdColumn;
		String mThumbnailUri = null;

		Cursor contactsTable = database.query("Contacts", new String[] { "status" },	//NON-NLS
				"mobile_id = " + String.format(Locale.US, "%d", c_id), null, null, null, null);	//NON-NLS
		boolean valid = false;
		while (contactsTable.moveToNext()) {
			//String name = contactsTable.getString(2);
			valid = (contactsTable.getInt(0) > 0);
		}
		contactsTable.close();

		if(! valid) return null;

		omb_library.appContext = frame.getApplicationContext();
		if(omb_library.needContactPermission(frame)) return null;

		Cursor phones = frame.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI
                , null, null, null, ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY + " ASC");	//NON-NLS
		boolean found_id = false;
		while (phones.moveToNext()) {
			// Gets the _ID column index
			mIdColumn = phones.getColumnIndex(ContactsContract.Contacts._ID);
			String id = phones.getString(mIdColumn);

			if(c_id == Long.parseLong(id)) {
				found_id = true;

				// Gets the LOOKUP_KEY index
				//mLookupKeyColumn = phones.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY);
				// Gets a content URI for the contact
				//mContactUri = ContactsContract.Contacts.getLookupUri(phones.getLong(mIdColumn), phones.getString(mLookupKeyColumn));
				//mBadge.assignContactUri(mContactUri);

				// The column in which to find the thumbnail ID
				int mThumbnailColumn;
				{
					mThumbnailColumn = mIdColumn;
				}
				mThumbnailUri = phones.getString(mThumbnailColumn);

				break;

			}

		}

		if(! found_id) updateContactTable(String.format(Locale.US, "%d", c_id));	//NON-NLS

		phones.close();

		//return mBadge;
		return mThumbnailUri;
	}

	public boolean addObject(TObjType T, int id, String N, String O, GregorianCalendar D, long c_index)
	{
		if(N.isEmpty() || O.isEmpty())return false;
		//if (D == invalidDate) return false;	// TODO: to be implemented
		switch(T){
			case toPre:
				if(Parsing){
					TObj temp = new TObj();
					temp.Id = id;
					temp.Name = N;
					temp.Object = O;
					temp.Alarm = D;
					temp.ContactIndex = c_index;
					Lent.add(temp);
				}
				else{
					if(! tableExists(database, "Loans"))
						database.execSQL(constants.cs_loans);
					ContentValues cv = new ContentValues();
					cv.put("name", N);	//NON-NLS
					cv.put("item",  O);	//NON-NLS
					if(omb_library.checkAlarm(D))
						cv.put("alarm", D.getTimeInMillis() / 1000);	//NON-NLS
					else cv.put("alarm", -1);	//NON-NLS
					cv.put("contact_index", c_index);	//NON-NLS
					database.insert("Loans", null, cv);	//NON-NLS
					parseDatabase();
				}
				break;
			case toInP:
				if(Parsing){
					TObj temp = new TObj();
					temp.Id = id;
					temp.Name = N;
					temp.Object = O;
					temp.Alarm = D;
					temp.ContactIndex = c_index;
					Borrowed.add(temp);}
				else{
					if(! tableExists(database, "Borrows"))
						database.execSQL(constants.cs_borrows);
					ContentValues cv = new ContentValues();
					cv.put("name", N);	//NON-NLS
					cv.put("item",  O);	//NON-NLS
					if(omb_library.checkAlarm(D))
						cv.put("alarm", D.getTimeInMillis() / 1000);	//NON-NLS
					else
						cv.put("alarm", -1);	//NON-NLS
					cv.put("contact_index", c_index);	//NON-NLS
					database.insert("Borrows", null, cv);	//NON-NLS
					parseDatabase();
				}
		}
		FileData.Modified=true;
		return true;}

	public boolean HasAlarms(){
		boolean result = false;
		int i;
		//noinspection SpellCheckingInspection
		GregorianCalendar checkdate = new GregorianCalendar();
		checkdate.add(Calendar.YEAR, 90);
		for(i = 0; i < NSho; i++)if(ShopItems.get(i).Alarm.compareTo(checkdate) < 0){
			result = true;
			break;}
		if(result) return true;
		for(i = 0; i < NLen; i++)if(Lent.get(i).Alarm.compareTo(checkdate) < 0){
			result = true;
			break;}
		if(result) return true;
		for(i = 0; i < NBor; i++)if(Borrowed.get(i).Alarm.compareTo(checkdate) < 0){
			result = true;
			break;}
		return result;}

	public boolean addValue(TTypeVal T, int id, String N, double V, long c_index)
	{
		if(! Parsing) if(N.isEmpty() || V == 0) return false;
		boolean E = false;
		int x;
		TVal temp = new TVal();
		switch(T){
			case tvFou:
				for(x = 0; x < NFun; x++)if(Funds.get(x).Name.equals(N)){
					omb_library.Error(11, N);
					return false;}
				if(Parsing){
					TVal last = new TVal();
					last.Id = id;
					last.Name = N;
					last.Value = V;
					Funds.add(NFun, last);
				}
				else{
					if(! tableExists(database, "Funds"))
						database.execSQL(constants.cs_funds);

					ContentValues cv = new ContentValues();

					cv.put("name", N);	//NON-NLS
					cv.put("value", V);	//NON-NLS
					database.insert("Funds", null, cv);	//NON-NLS
					FileData.Modified = true;
				}
				break;
			case tvCre:
				for(x = 0; x < NCre; x++)if(Credits.get(x).Name.compareToIgnoreCase(N) == 0){
					Credits.get(x).Value += V;
					changeFundValue(TTypeVal.tvCre, Credits.get(x).Id, Credits.get(x).Value);
					E = true;
					break;}
				if(!E){
					if(Parsing){
						temp.Id = id;
						temp.Name = N;
						temp.Value = V;
						temp.ContactIndex = c_index;
						Credits.add(NCre, temp);
					}
					else{
						if(! tableExists(database, "Credits"))
							database.execSQL(constants.cs_credits);
						ContentValues cv = new ContentValues();
						cv.put("name", N);	//NON-NLS
						cv.put("value", V);	//NON-NLS
						cv.put("contact_index", c_index);	//NON-NLS
						database.insert("Credits", null, cv);	//NON-NLS
						FileData.Modified = true;
					}
				}
				break;
			case tvDeb:
				for(x = 0; x < NDeb; x++)if(Debts.get(x).Name.compareToIgnoreCase(N) == 0){
					Debts.get(x).Value += V;
					changeFundValue(TTypeVal.tvDeb, Debts.get(x).Id, Debts.get(x).Value);
					E = true;
					break;}
				if(!E){
					if(Parsing){
						temp.Id = id;
						temp.Name = N;
						temp.Value = V;
						temp.ContactIndex = c_index;
						Debts.add(NDeb, temp);
					}
					else{
						if(! tableExists(database, "Debts"))
							database.execSQL(constants.cs_debts);
						ContentValues cv = new ContentValues();
						cv.put("name", N);	//NON-NLS
						cv.put("value", V);	//NON-NLS
						cv.put("contact_index", c_index);	//NON-NLS
						database.insert("Debts", null, cv);	//NON-NLS
						FileData.Modified = true;
					}
				}
				break;
			default:
				return false;}
		if(! Parsing) parseDatabase();
		return true;}

	public void updateMatters(TOpType T)
	{
		boolean Ex;
		int i;
		int j;
		MattersBuffer.clear();
		for(i = 0; i < Lines.size(); i++)if(Lines.get(i).Type == T){
			Ex=false;
			for(j = 0; j < MattersBuffer.size(); j++)if(MattersBuffer.get(j).equalsIgnoreCase(Lines.get(i).Reason)){
				Ex=true;
				break;}
			if(!Ex)MattersBuffer.add(Lines.get(i).Reason);}
	}

	public void  xmlExport(Bitmap B1, Bitmap B2){
		boolean FirstDate = false;	// set when first date is inserted
		String App;	// line to append

		// path selection
		String document = Opts.getString("GDDoc", "NULL");
		File file = new File(document);
		String dir = file.getParent() + "/" + Opts.getString("GDPrefix", "OpenMoneyBox")
			+ "_" + String.format(Locale.US, "%d", FileData.Year);	//NON-NLS
		dir = dir + "-" + String.format(Locale.US, "%02d", (int) FileData.Month +1);	//NON-NLS
		file = new File (dir);
		if(! file.exists()) {
			if(! file.mkdir()){
				omb_library.Error(-1, null);
				return;
			}
		}

		// XSL template copy
		String XSL_template;

        //noinspection SpellCheckingInspection
        XSL_template = frame.getResources().getString(R.string.lang) + "/ombexport.xsl";
		try {
			//noinspection SpellCheckingInspection
			omb_library.copyAsset(XSL_template, dir + "/ombexport.xsl");
		} catch (IOException ignored) {

		}

		// xml document creation
		int ci;
		file = new File(dir + "/" + Opts.getString("GDPrefix", "OpenMoneyBox") +".xml");
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file);
			XmlSerializer xmlSerializer = Xml.newSerializer();
			StringWriter writer = new StringWriter();

			xmlSerializer.setOutput(fileWriter);

			xmlSerializer.startDocument("UTF-8", true);	//NON-NLS
			xmlSerializer.setFeature(
					"http://xmlpull.org/v1/doc/features.html#indent-output", true);
			GregorianCalendar now = new GregorianCalendar();
			xmlSerializer.comment(frame.getString(R.string.export_created_by) + " " + shortVersion +
				" " + frame.getString(R.string.export_created_on) + " " + omb_library.omb_DateToStr(now) + " " + omb_library.omb_TimeToStr(now));
            //noinspection SpellCheckingInspection
            xmlSerializer.processingInstruction("xml-stylesheet type=\"text/xsl\" href=\"ombexport.xsl\"");	//NON-NLS
			xmlSerializer.startTag("", "groups");	//NON-NLS
			xmlSerializer.startTag("", "headers");	//NON-NLS
			String Header = frame.getResources().getString(R.string.xml_title);
			now.set(Calendar.MONTH, (int) FileData.Month);
			xmlSerializer.startTag("", "title");	//NON-NLS
			xmlSerializer.attribute("", "heading", Header);	//NON-NLS
			xmlSerializer.attribute("", "month", now.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())	//NON-NLS
				+ " " + String.format(Locale.US, "%d", FileData.Year));	//NON-NLS
			xmlSerializer.endTag("", "title");	//NON-NLS
			xmlSerializer.endTag("", "headers");	//NON-NLS
			xmlSerializer.startTag("", "days");	//NON-NLS
			// line parsing
			for(int i = 0; i < NLin; i++){
				if(IsDate(i)){
					if(! FirstDate) {
						FirstDate = true;
					}
					else {
						xmlSerializer.endTag("", "day");	//NON-NLS
					}
					xmlSerializer.startTag("", "day");	//NON-NLS
					xmlSerializer.attribute("", "date",	//NON-NLS
						Lines.get(i).Date.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault()) + " "
						+ omb_library.omb_DateToStr(Lines.get(i).Date));
					xmlSerializer.attribute("", "total", Lines.get(i).Value);	//NON-NLS
				}
				else{
					xmlSerializer.startTag("", "item");	//NON-NLS
					xmlSerializer.attribute("", "time", omb_library.omb_TimeToStr(Lines.get(i).Time));	//NON-NLS
					switch(Lines.get(i).Type){
						case toGain:
							App = "1";
							break;
						case toExpe:
							App = "2";
							break;
						case toSetCre:
							App = "3";
							break;
						case toRemCre:
							App = "4";
							break;
						case toConCre:
							App = "5";
							break;
						case toSetDeb:
							App = "6";
							break;
						case toRemDeb:
							App = "7";
							break;
						case toConDeb:
							App = "8";
							break;
						case toGetObj:
							App = "9";
							break;
						case toGivObj:
							App = "10";
							break;
						case toLenObj:
							App = "11";
							break;
						case toBakObj:
							App = "12";
							break;
						case toBorObj:
							App = "13";
							break;
						case toRetObj:
							App = "14";
							break;
						default:
							App = "0";
							break;}
					xmlSerializer.attribute("", "type", App);	//NON-NLS

					if(Lines.get(i).Value.contains("\"")){
						String Values = Lines.get(i).Value;
						//noinspection SpellCheckingInspection
						Values = Values.replace("\"", "[OMB_ESCAPEQUOTES]");	//NON-NLS // intermediate string to be processed by SubstSpecialChars
						App = Values;
					}
					else App = Lines.get(i).Value;
					App = SubstSpecialChars(App);
					xmlSerializer.attribute("", "value", App);	//NON-NLS

					if(Lines.get(i).Reason.contains("\"")){
						String ReasonList = Lines.get(i).Reason;
						//noinspection SpellCheckingInspection
						ReasonList = ReasonList.replace("\"", "[OMB_ESCAPEQUOTES]");		//NON-NLS // intermediate string to be processed by SubstSpecialChars
						App = ReasonList;
					}
					else App = Lines.get(i).Reason;
					App = SubstSpecialChars(App);
					xmlSerializer.attribute("", "reason", App);	//NON-NLS

					App = "";
					ci = (int) Lines.get(i).CategoryIndex;
					if(ci >= 0){
						for(int j = 0; j < NCat; j++) if(CategoryDB.get(j).Id == ci){
							App = CategoryDB.get(j).Name;
							break;}

					}
					xmlSerializer.attribute("", "category", App);	//NON-NLS
					xmlSerializer.endTag("", "item");	//NON-NLS
				}}

			// file completion
			xmlSerializer.endTag("", "day");	//NON-NLS
			xmlSerializer.endTag("", "days");	//NON-NLS
			xmlSerializer.endTag("", "groups");	//NON-NLS
			xmlSerializer.endDocument();

			fileWriter.write(writer.toString());
		} catch (IOException ignored) {

		}

		// file closure
		try {
			Objects.requireNonNull(fileWriter).flush();
			fileWriter.close();
		} catch (IOException ignored) {

		}

		// Logo picture copy
		try {
			omb_library.copyAsset("logo.png", dir + "/logo.png");
		} catch (IOException ignored) {

		}

		if(B1 != null) {
			OutputStream os = null;
			try {
				os = new FileOutputStream(dir + "/chart1.png");	//NON-NLS
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			if(os != null) {
				B1.compress(Bitmap.CompressFormat.PNG, 100, os);
				try {
					os.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			}
		}

		if(B2 != null) {
			OutputStream os = null;
			try {
				os = new FileOutputStream(dir + "/chart2.png");	//NON-NLS
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			if (os != null) {
				B2.compress(Bitmap.CompressFormat.PNG, 100, os);
				try {
					os.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			}
		}
 	}

	private boolean IsDate(int R)
	{
		return Lines.get(R).IsDate;
	}

	public int checkShopList(){
		GregorianCalendar Now = new GregorianCalendar();
		for(int i = 0; i < NSho; i++)
			if(ShopItems.get(i).Alarm.compareTo(Now) < 0) return i;
		return -1;
	}

	public int checkLentObjects(){
		GregorianCalendar Now = new GregorianCalendar();
		for(int i = 0; i < NLen; i++)
			if(Lent.get(i).Alarm.compareTo(Now) < 0) return i;
		return -1;
	}

	public int checkBorrowedObjects(){
		GregorianCalendar Now = new GregorianCalendar();
		for(int i = 0; i < NBor; i++)
			if(Borrowed.get(i).Alarm.compareTo(Now) < 0) return i;
		return -1;
	}

	private String getKey(boolean archive){
		String pwd ;

		try {
			if (archive) pwd = Opts.getString("key_archive", "");	//NON-NLS
			else pwd = Opts.getString("key_main", "");	//NON-NLS
		}
		catch(Exception e){
			return "";
		}

		return pwd;
	}

	private void initialize(String document)
	{
		NFun = NCre = NDeb = NLen = NBor = NSho = NLin = NCat = 0;

		//TODO: removing following dummy initializations
		// Funds initialization
		TVal temp = new TVal();
		Funds.add(temp);
		// Credits initialization
		Credits.add(temp);
		// Debts initialization
		Debts.add(temp);

		// Totals zeroing
		Tot_Funds = Tot_Credits = Tot_Debts = 0;
		// File data initialisation
		FileData.FileName = new File(document);
		FileData.DefFund = null;
		FileData.Modified=false;
		FileData.ReadOnly=false;
		Day = (GregorianCalendar) GregorianCalendar.getInstance();
		FileData.Year = Day.get(GregorianCalendar.YEAR);
		FileData.Month = Day.get(GregorianCalendar.MONTH);
	}

	public boolean tableExists(SQLiteDatabase db, @NonNls String tableName){
		Cursor cursor = db.query("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ tableName + "'", null);	//NON-NLS
//		if(cursor != null) {
			if(cursor.getCount() > 0) {
				cursor.close();
				return true;
			}
			cursor.close();
//		}
		return false;
	}

	private void addCategory(int id, String name, int iconIndex){
		TCategory temp = new TCategory();
		temp.Id = id;
		temp.Name = name;
		temp.test = false;
		temp.iconId = iconIndex;
		CategoryDB.add(temp);
		NCat++;}

	public boolean addShopItem(int id, String N, GregorianCalendar A)
	{
		if(N.isEmpty())return false;
		//if(A == invalidDate) return false; TODO: to be implemented
		if(Parsing){
			TShopItem temp = new TShopItem();
			temp.Id = id;
			temp.Name = N;
			temp.Alarm = A;
			ShopItems.add(temp);
			//NSho++;
		}
		else{
			//noinspection SpellCheckingInspection
			if(! tableExists(database, "Shoplist"))
				database.execSQL(constants.cs_shoplist);
			ContentValues cv = new ContentValues();
			cv.put("item",  N);	//NON-NLS

			if(omb_library.checkAlarm(A))
				cv.put("alarm", A.getTimeInMillis() / 1000);	//NON-NLS
			else
				cv.put("alarm", -1);	//NON-NLS
			//noinspection SpellCheckingInspection
			database.insert("Shoplist", null, cv);	//NON-NLS
			parseDatabase();
		}
		FileData.Modified=true;
		return true;}

	void changeFundValue(TTypeVal type, int id, double V){

		String table;
		switch(type){
			case tvFou:
				table = "Funds";	//NON-NLS
				break;
			case tvCre:
				table = "Credits";	//NON-NLS
				break;
			case tvDeb:
				table = "Debts";	//NON-NLS
				break;
			default:
				table = "";}

		ContentValues cv = new ContentValues();
		cv.clear();
		cv.put("value", V);	//NON-NLS
		database.update(table, cv, "id = " + String.format(Locale.US, "%d", id), null);	//NON-NLS

		parseDatabase();
		FileData.Modified = true;
	}

	private String SubstSpecialChars(String S){
		// source: http://www.mrwebmaster.it/xml/xml-lettere-accentate-perche-errore_8390.html
		S = S.replace("&", "&amp;");
		S = S.replace("à", "&#224;");	//NON-NLS
		S = S.replace("á", "&#225;");	//NON-NLS
		S = S.replace("è", "&#232;");	//NON-NLS
		S = S.replace("é", "&#233;");	//NON-NLS
		S = S.replace("ì", "&#236;");	//NON-NLS
		S = S.replace("í", "&#237;");	//NON-NLS
		S = S.replace("ò", "&#242;");	//NON-NLS
		S = S.replace("ó", "&#243;");	//NON-NLS
		S = S.replace("ù", "&#249;");	//NON-NLS
		S = S.replace("ú", "&#250;");	//NON-NLS
		S = S.replace("Ì", "&#204;");	//NON-NLS
		//noinspection SpellCheckingInspection
		S = S.replace("[OMB_ESCAPEQUOTES]", "&quot;");	//NON-NLS
		return S;}

	public double getTot(TTypeVal T)
	{
		double Ret;
		switch(T){
			case tvFou:
				Ret=Tot_Funds;
				break;
			case tvCre:
				Ret=Tot_Credits;
				break;
			case tvDeb:
				Ret=Tot_Debts;
				break;
			default:Ret=-1;}
		return Ret;}

	public void delValue(TTypeVal T,int I)
	{
		if(I < 0)return;

		switch(T){
			case tvFou:
				database.delete("Funds", "id=?", new String[]{Integer.toString(I)});	//NON-NLS
				break;
			case tvCre:
				database.delete("Credits", "id=?", new String[]{Integer.toString(I)});	//NON-NLS
				break;
			case tvDeb:
				database.delete("Debts", "id=?", new String[]{Integer.toString(I)});	//NON-NLS
		}
		parseDatabase();
		FileData.Modified = true;
	}

	void updateCategories(ArrayList<String> StringList, ArrayList<Integer> icons){
		boolean exists;
		int i, x;	// x: pointer in Data->Lines
		String value;
		ContentValues cv = new ContentValues();

		boolean master_exist = false;
		SQLiteDatabase db = null;

		File fMaster = frame.getDatabasePath(constants.archive_name);
		if(fMaster.exists()){
			String master = fMaster.toString();
			db = SQLiteDatabase.openOrCreateDatabase(master, "", null, null);
			if(tableExists(db, "Categories")) master_exist = true;
			else db.close();
		}

		// Check removed items
		for(i = NCat - 1; i >= 0; i--){
			value = CategoryDB.get(i).Name;
			for (x = 0; x < StringList.size(); x++)if(value.compareToIgnoreCase(StringList.get(x)) == 0){
				CategoryDB.get(i).test = true;
				break;}}
		for(i = NCat - 1; i >= 0; i--) if(! CategoryDB.get(i).test){
			cv.clear();
			cv.put("cat_index", -1);	//NON-NLS
			database.update("Transactions", cv, "cat_index=?", new String[]{Integer.toString(CategoryDB.get(i).Id)});	//NON-NLS

			cv.clear();
			cv.put("active", false);	//NON-NLS
			database.update("Categories", cv, "id=?", new String[]{Integer.toString(CategoryDB.get(i).Id)});	//NON-NLS

			if(master_exist){
				cv.clear();
				cv.put("cat_index", -1);	//NON-NLS
				db.update("Transactions", cv, "cat_index=?", new String[]{Integer.toString(CategoryDB.get(i).Id)});	//NON-NLS

				cv.clear();
				cv.put("active", false);	//NON-NLS
				db.update("Categories", cv, "id=?", new String[]{Integer.toString(CategoryDB.get(i).Id)});	//NON-NLS

			}
		}

		// Check added items
		for(i = 0; i < StringList.size(); i++){
			exists = false;
			value = StringList.get(i);
			for(x = 0; x < NCat; x++) if(value.compareToIgnoreCase(CategoryDB.get(x).Name) == 0){
				exists = true;
				break;}
			if(! exists){
				cv.clear();
				cv.put("name", value);	//NON-NLS
				cv.put("active", true);	//NON-NLS
				//noinspection SpellCheckingInspection
				cv.put("iconid", icons.get(i));	//NON-NLS
				database.insert("Categories", null, cv);	//NON-NLS

				if(master_exist) db.insert("Categories", null, cv);	//NON-NLS
			}
		}

		// Update icon indexes
		for(i = 0; i < StringList.size(); i++){
			value = StringList.get(i);
			for(x = 0; x < NCat; x++)if(value.compareTo(CategoryDB.get(x).Name) == 0){
				cv.clear();
				//noinspection SpellCheckingInspection
				cv.put("iconid", icons.get(i));	//NON-NLS
				database.update("Categories", cv, "id=?", new String[]{Integer.toString(CategoryDB.get(x).Id)});	//NON-NLS
			}
		}

		// TODO: Update icon indexes in master

		if(master_exist) db.close();

		FileData.Modified = true;
		parseDatabase();
	}

	public void setDefaultFundValue(double value){
		for(int i = 0; i < NFun; i++)
			if(Funds.get(i).Name.compareToIgnoreCase(FileData.DefFund) == 0){
				changeFundValue(TTypeVal.tvFou, Funds.get(i).Id, value);

				parseDatabase();
				break;}
	}

	public void setDefaultFund(String def){
		ContentValues cv = new ContentValues();
		cv.put("data", def);	//NON-NLS
		database.update("Information", cv, "id = " + String.format(Locale.US, "%d", constants.dbMeta_default_fund), null);	//NON-NLS

		FileData.DefFund = def;
		FileData.Modified = true;
		parseDatabase();
	}

	public void delObject(TObjType T, int I)
	{
		if(I<0)return;
		switch(T){
			case toPre:
				//if(I > Lent.size() - 1)return false;
				database.delete("Loans", "id=?", new String[]{Integer.toString(I)});	//NON-NLS
				break;
			case toInP:
				database.delete("Borrows", "id=?", new String[]{Integer.toString(I)});	//NON-NLS
				break;
			default:
				return;}
		parseDatabase();
		FileData.Modified = true;
	}

	public void delShopItem(int I)
	{
		if(I < 0) return;

		//noinspection SpellCheckingInspection
		database.delete("Shoplist", "id=?", new String[]{Integer.toString(I)});	//NON-NLS
		parseDatabase();

		FileData.Modified=true;
	}

	// Review and enable this function to upgrade the master database from older formats
	/*
	private void checkMaster(){
		if(frame == null) return;

		String pwd_archive = getKey(true);
		File fMaster;
		String master;

		fMaster = frame.getDatabasePath(constants.archive_name);
		master = fMaster.toString();
		if(fMaster.exists()){
			if(omb_library.checkFileFormat(master, pwd_archive) == ombFFORMAT_33) {
				ContentValues cv = new ContentValues();
				cv.put("currencyid", -1);

				SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(master, pwd_archive, null);

				db.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN currencyid INTEGER;");
				db.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN currencyrate REAL;");
				db.execSQL_IGI_INSIDER_OVERRIDE("ALTER TABLE Transactions ADD COLUMN currencysymb TEXT;");

				db.update("Transactions", cv, "isdate = 0", null);

				db.setVersion(constants.dbVersion);
			}
		}
	}
	*/

	public Cursor getExternalFundTable() // NOTE (igor#1#): Hidden feature
	{
		return AttachedDB.query("Funds", null, null, null, null, null, "name");	//NON-NLS
	}

}

