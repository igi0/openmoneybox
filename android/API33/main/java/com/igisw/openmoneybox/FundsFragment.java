/* **************************************************************
 * Name:
 * Purpose:   Funds fragment for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-08-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class FundsFragment extends Fragment {
    public int fund_pos;
    private MainActivity activity;
    private ImageButton btn_removeFund, btn_editFund;
    private FloatingActionButton newFundBtn;
    public FloatingActionButton groupBtn;
    public static int[] fundPositions;

    public FundsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_funds, container, false);

        // Funds tab
        String str_tmp;
        Currency curr = Currency.getInstance(Locale.getDefault());
        TextView total_view;

        // Create the OnClickListener
        btn_removeFund = view.findViewById( R.id.removeFund );
        btn_editFund = view.findViewById( R.id.editFund );
        newFundBtn = view.findViewById( R.id.fundActionButton );
        groupBtn = view.findViewById( R.id.groupActionButton );
        View.OnClickListener clickListener = v -> {
            if (v == newFundBtn) activity.NewFundClick(v);
            else if (v == btn_removeFund) activity.RemoveFundClick(v);
            else if (v == btn_editFund) activity.ResetFundClick(v);
            else if (v == groupBtn) activity.FundGroupClick(v);
        };
        btn_removeFund.setOnClickListener(clickListener);
        btn_editFund.setOnClickListener(clickListener);
        newFundBtn.setOnClickListener(clickListener);
        groupBtn.setOnClickListener(clickListener);

        SharedPreferences Opts = PreferenceManager.getDefaultSharedPreferences(activity);
        boolean groupBtnShow = Opts.getBoolean("GGroupFunds", false);
        groupBtn.setEnabled(groupBtnShow);
        if(! groupBtnShow) groupBtn.hide();

        btn_removeFund.setEnabled(false);
        btn_editFund.setEnabled(false);

        // Find the ListView resource.
        ListView fundView = view.findViewById(R.id.FundView);

        ArrayList<String> fundList = new ArrayList<>();

        int addedGroups = 0;
        int currentFundPosition = 0;
        fundPositions = new int[activity.Data.NFun];

        if(Opts.getBoolean("GGroupFunds", false)){
            Cursor GroupTable, ChildTable;
            GroupTable = activity.Data.database.query("select * from FundGroups where owner = '-1';", null);
            int GTC = GroupTable.getCount();

            double[] GroupTotals = new double[GTC];
            for(int i = 0; i < GTC; i++)
                GroupTotals[i] = 0;
            ArrayList<String> GroupNames = new ArrayList<>();
            for(int i = 0; i < GTC; i++){
                GroupTable.moveToPosition(i);
                GroupNames.add(GroupTable.getString(1));
            }

            ChildTable = activity.Data.database.query("select * from FundGroups where owner > '-1';", null);
            int CTC = ChildTable.getCount();

            boolean Found;
            int Group_ID;
            String FundName;
            for(int i = 0; i < activity.Data.NFun; i++){
                Found = false;
                FundName = activity.Data.Funds.get(i).Name;
                for(int j = 0; j < CTC; j++){
                    ChildTable.moveToPosition(j);
                    if(FundName.equalsIgnoreCase(ChildTable.getString(/*"name")*/1))){
                        Group_ID = ChildTable.getInt(/*"owner", -1*/2);
                        for(int k = 0; k < GTC; k++){
                            GroupTable.moveToPosition(k);

                            if(GroupTable.getInt(/*"id", -1*/0) == Group_ID){
                                Found = true;
                                GroupTotals[k] += activity.Data.Funds.get(i).Value;

                                addedGroups++;
                                break;
                            }

                        }

                        break;
                    }
                }
                if(! Found) {
                    str_tmp = activity.Data.Funds.get(i).Name + "<br><small>" + curr.getSymbol() + " ";
                    str_tmp += omb_library.FormDigits(activity.Data.Funds.get(i).Value, false) + "</small>";
                    fundList.add(str_tmp);

                    fundPositions[currentFundPosition] = i;
                    currentFundPosition++;
                }
            }

            for(int i = 0; i < GTC; i++){
                str_tmp = GroupNames.get(i) + " - [" +
                    getResources().getString(R.string.fundGroups_group_marker) + "]" + "<br><small>" + curr.getSymbol() + " ";
                str_tmp += omb_library.FormDigits(GroupTotals[i], false) + "</small>";
                fundList.add(str_tmp);
            }

        }
        else {

            for (int i = 0; i < activity.Data.NFun; i++) {
                str_tmp = activity.Data.Funds.get(i).Name + "<br><small>" + curr.getSymbol() + " ";
                str_tmp += omb_library.FormDigits(activity.Data.Funds.get(i).Value, false) + "</small>";
                fundList.add(str_tmp);

                fundPositions[currentFundPosition] = i;
                currentFundPosition++;
            }

        }

        // NOTE (igor#1#): Hidden feature
        double TotFundAttached = 0, attached_value;
        if(activity.Data.ExternalFileAttached){
            Cursor AttachedFundTable = activity.Data.getExternalFundTable();
            for(int j = 0; j < AttachedFundTable.getCount(); j++){
                AttachedFundTable.moveToPosition(j);
                attached_value = AttachedFundTable.getDouble(2);
                str_tmp = AttachedFundTable.getString(1) + " (*)" + "<br><small>" + curr.getSymbol() + " ";
                str_tmp += omb_library.FormDigits(attached_value, false) + "</small>";
                fundList.add(str_tmp);
                TotFundAttached += attached_value;
            }
        }
        // ------------------------------

        // Create ArrayAdapter
        // NOTE (igor#1#): Hidden feature
        Resources.Theme theme = this.requireContext().getTheme();
        // ------------------------------
        int finalAddedGroups = addedGroups;
        ArrayAdapter<String> fundAdapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_list_item_single_choice, fundList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    textView.setText(Html.fromHtml(textView.getText().toString(), Html.FROM_HTML_MODE_COMPACT));
                else textView.setText(HtmlCompat.fromHtml(textView.getText().toString(), HtmlCompat.FROM_HTML_MODE_LEGACY));
                // NOTE (igor#1#): Hidden feature
                int marker;
                if(Opts.getBoolean("GGroupFunds", false)) marker = activity.Data.NFun - finalAddedGroups + 1;
                else marker = activity.Data.NFun;
                if(position >= marker){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        textView.setBackgroundColor(getResources().getColor(R.color.green_dark, theme));
                    else textView.setBackgroundColor(getResources().getColor(R.color.green_dark));
                }
                else if(textView.getText().toString().contains("[" + getResources().getString(R.string.fundGroups_group_marker) + "]")){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        textView.setBackgroundColor(getResources().getColor(R.color.blue_dark, theme));
                    else textView.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                }
                else{
                    if(Opts.getBoolean("GDarkTheme", false)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                            textView.setBackgroundColor(getResources().getColor(R.color.black, theme));
                        else textView.setBackgroundColor(getResources().getColor(R.color.black));
                    }
                    else{
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                            textView.setBackgroundColor(getResources().getColor(R.color.white_bright, theme));
                        else textView.setBackgroundColor(getResources().getColor(R.color.white_bright));
                    }
                }
                // ------------------------------

                notifyDataSetChanged();
                return textView;
            }
        };
        // Set the ArrayAdapter as the ListView's adapter.
        fundView.setAdapter(fundAdapter);
        total_view = view.findViewById(R.id.total_fund);
        str_tmp = getResources().getString(R.string.total) + "<br><small>" + curr.getSymbol() + " ";
        str_tmp += omb_library.FormDigits(activity.Data.Tot_Funds + TotFundAttached, false) + "</small>";
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            total_view.setText(HtmlCompat.fromHtml(str_tmp, HtmlCompat.FROM_HTML_MODE_COMPACT));
        else total_view.setText(HtmlCompat.fromHtml(str_tmp, HtmlCompat.FROM_HTML_MODE_LEGACY));

        fundView.setOnItemClickListener((adapter, arg1, position, arg3) -> {
            boolean enabled = true;
            int marker;
            if(Opts.getBoolean("GGroupFunds", false)) marker = activity.Data.NFun - finalAddedGroups + 1;
            else marker = activity.Data.NFun;
            if (position >= 0) {
                // NOTE (igor#1#): Hidden feature
                if(position >= /*activity.Data.NFun*/marker)
                    enabled = false;
                else {
                    // Disable if selected item is a group
                    if (fundAdapter.getItem(position).contains("[" + getResources().getString(R.string.fundGroups_group_marker) + "]<br>"))
                        enabled = false;
                    // ------------------------------
//                    if (activity.Data.Funds.get(position).Name.compareToIgnoreCase(activity.Data.FileData.DefFund) == 0)
                    if (activity.Data.Funds.get(fundPositions[position]).Name.compareToIgnoreCase(activity.Data.FileData.DefFund) == 0)
                        enabled = false;
                }
            }
            btn_removeFund.setEnabled(enabled);
            btn_editFund.setEnabled(enabled);
            if (position >= 0) fund_pos = position;
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

}
