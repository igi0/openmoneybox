/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-11-08
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Objects;

public class Recycler_Adapter_ShoppingList extends RecyclerView.Adapter<Recycler_Adapter_ShoppingList.ViewHolder>{

    public MainActivity frame;
    private final List<ShoppingList_Wrapper> ShoppingList;
    public LinearLayoutManager llManager;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ViewHolder(View v) {
            super(v);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int item = getAbsoluteAdapterPosition();
            ImageButton Image;

            View CheckView;
            for(int i = 0; i < getItemCount(); i++){
                try {
                    CheckView = llManager.findViewByPosition(i);
                    Image = Objects.requireNonNull(CheckView).findViewById(R.id.delShopItem);
                    Image.setVisibility(View.INVISIBLE);
                }
                catch(Exception e){
                    //break;
                }
            }

            Image = view.findViewById(R.id.delShopItem);

            // Create the OnClickListener
            View.OnClickListener clickListener = frame::delShopItemClick;
            Image.setOnClickListener(clickListener);

            Image.setVisibility(View.VISIBLE);

            frame.UpdateShoppingListRecyclerItem(item);
        }

    }

    public class ObjectViewHolder extends ViewHolder{

        final CardView cv;
        final TextView itemName;
        final TextView itemAlarm;

        ObjectViewHolder(View itemView) {
            super(itemView);

            LinearLayout ll = itemView.findViewById(R.id.ll_cv_shoplist);
            RelativeLayout rl = itemView.findViewById(R.id.rl_cv_shoplist );
            RelativeLayout rl2 = itemView.findViewById(R.id.rl_cv_shoplist_2 );
            this.cv = itemView.findViewById(R.id.cv);
            this.itemName = itemView.findViewById(R.id.item_name);
            this.itemAlarm = itemView.findViewById(R.id.item_alarm);


            if(frame.Opts.getBoolean("GDarkTheme", false))
            {
                ll.setBackgroundColor(0xFF000000);
                cv.setCardBackgroundColor(0xFF333333);
                rl.setBackgroundColor(0xFF333333);
                rl2.setBackgroundColor(0xFF333333);
                itemName.setBackgroundColor(0xFF333333);
                itemAlarm.setBackgroundColor(0xFF333333);
            }
        }

    }

    public Recycler_Adapter_ShoppingList(List<ShoppingList_Wrapper> shoppingList){
        this.ShoppingList = shoppingList;
    }

    @Override
    public int getItemCount() {
        return ShoppingList.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_shopitem, viewGroup, false);
        return new ObjectViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@androidx.annotation.NonNull ViewHolder viewHolder, int i) {
        ObjectViewHolder holder = (ObjectViewHolder) viewHolder;
        holder.itemName.setText(ShoppingList.get(i).item);
        holder.itemAlarm.setText(ShoppingList.get(i).alarm);
    }

}
