/* **************************************************************
 * Name:
 * Purpose:   Core Code for OpenMoneyBox Application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2018-07-23
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

class objects_wrapper {
    final int type, id, iconId;
    final String name, contact, alarm, badgeUri;

    objects_wrapper(int type, int id, String name, String contact, String alarm, int iconId, String badgeUri) {
        this.type = type;
        this.id = id;
        this.name = name;
        this.contact = contact;
        this.alarm = alarm;
        this.iconId = iconId;
        this.badgeUri = badgeUri;
    }
}
