/* **************************************************************
 * Name:      
 * Purpose:   Header with constants for OpenMoneyBox
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import org.jetbrains.annotations.NonNls;

public class constants{

	final static String shortVersion = "3.4";

	public final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 104;

	@NonNls
	final static String cipher_compat_sql = "PRAGMA cipher_default_compatibility = 3;";
	@NonNls
	final static String cipher_compat_sql_v4 = "PRAGMA cipher_default_compatibility = 4;";
//	final static int dbVersion = 37;	// OpenMoneyBox database version
	final static int dbVersion = 39;	// OpenMoneyBox database version

	final static int dbMeta_application_info	= 1;
	final static int dbMeta_default_fund		= 2;
	final static int dbMeta_mobile_export		= 3;
	final static int dbMeta_currency			= 4;
	final static int dbMeta_attached_db			= 5;

	public enum ombFileFormat{
		bilFFORMAT_UNKNOWN,
		/*
		bilFFORMAT_301,	// bilancio 3.0.1 format
		bilFFORMAT_302,	// bilancio 3.0.2 format
		ombFFORMAT_31,		// openmoneybox 3.1 format
		ombFFORMAT_32,		// openmoneybox 3.2 format
		ombFFORMAT_33,		// openmoneybox 3.3 format (encryption support)
		ombFFORMAT_332,		// openmoneybox 3.3.2 format
		*/
		ombWRONGPASSWORD,	// wrong password
		ombFFORMAT_34,		// openmoneybox 3.4 format
		ombFFORMAT_35,		// openmoneybox 3.5 format

		ombFFORMAT_35_CipherV4,	// SqlCipher format higher than user selected
	}

	final static double ombInvalidLatitude = 91;
	final static double ombInvalidLongitude = 181;

	final static String archive_name = "omb_master_mobile.ombdb";

	final static int _OMB_TOPCATEGORIES_NUMBER = 3;
	final static int _OMB_TOPCATEGORIES_OEMICON = 12;

	// Main db definition

	// Metadata table create statement
	@NonNls
	final static String cs_information = "create TABLE Information(" +
			"id INTEGER PRIMARY KEY," +
			"data TEXT);";

	// Funds table create statement
	@NonNls
	final static String cs_funds = "create TABLE Funds(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"value REAL);";

	// Credits table create statement
	@NonNls
	final static String cs_credits = "CREATE TABLE Credits(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"value REAL," +
			"contact_index INTEGER);";

	// Debts table create statement
	@NonNls
	final static String cs_debts = "CREATE TABLE Debts(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"value REAL," +
			"contact_index INTEGER);";

	// Loans table create statement
	@NonNls
	final static String cs_loans = "CREATE TABLE Loans(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"item TEXT," +
			"alarm INTEGER," +
			"contact_index INTEGER);";

	// Borrows table create statement
	@NonNls
	final static String cs_borrows = "CREATE TABLE Borrows(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"item TEXT," +
			"alarm INTEGER," +
			"contact_index INTEGER);";

	// Shopping list table create statement
	@NonNls
	final static String cs_shoplist = "CREATE TABLE Shoplist(" +
			"id INTEGER PRIMARY KEY," +
			"item TEXT," +
			"alarm INTEGER);";

	// Transactions list table create statement
	@NonNls
	final static String cs_transactions = "create TABLE Transactions(" +
			"id INTEGER PRIMARY KEY," +
			"isdate INTEGER," +
			"date INTEGER," +
			"time INTEGER," +
			"type INTEGER," +
			"value TEXT," +
			"reason TEXT," +
			"cat_index INTEGER," +
			"contact_index INTEGER," +
			"latitude REAL," +
			"longitude REAL," +
			"currencyid INTEGER," +
			"currencyrate REAL," +
			"currencysymb TEXT);";

	// Categories list table create statement
	@NonNls
	final static String cs_categories = "create TABLE Categories(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"active INTEGER," +
			"iconid INTEGER);";

	// Contact list table create statement
	@NonNls
	final static String cs_contacts = "create TABLE Contacts(" +
			"id INTEGER PRIMARY KEY," +
			"mobile_id INTEGER," +
			"contact TEXT," +
			"status INTEGER);";

	@NonNls
	final static String cs_customicons = "create TABLE CustomIcons(" +
			"id INTEGER PRIMARY KEY," +
			"image TEXT);";

	// Fund groups table create statement
	@NonNls
	final static String cs_fundgroups = "create TABLE FundGroups(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"owner INTEGER," +
			"child INTEGER);";
	// Master db definition

	// Funds table create statement (master)
	@NonNls
	final static String cs_funds_master = "create TABLE Funds%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"value REAL);";

	// Credits table create statement (master)
	@NonNls
	final static String cs_credits_master = "CREATE TABLE Credits%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"value REAL," +
			"contact_index INTEGER);";

	// Debts table create statement (master)
	@NonNls
	final static String cs_debts_master = "CREATE TABLE Debts%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"value REAL," +
			"contact_index INTEGER);";

	// Loans table create statement (master)
	@NonNls
	final static String cs_loans_master = "CREATE TABLE Loans%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"item TEXT," +
			"alarm INTEGER," +
			"contact_index INTEGER);";

	// Borrows table create statement (master)
	@NonNls
	final static String cs_borrows_master = "CREATE TABLE Borrows%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"item TEXT," +
			"alarm INTEGER," +
			"contact_index INTEGER);";

	// Categories list table create statement (master)
	@NonNls
	final static String cs_categories_master = "CREATE TABLE Categories%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"active INTEGER," +
			"iconid INTEGER);";

	// Fund groups table create statement (master)
	@NonNls
	final static String cs_fundgroups_master = "create TABLE FundGroups%s(" +
			"id INTEGER PRIMARY KEY," +
			"name TEXT," +
			"owner INTEGER," +
			"child INTEGER);";

}
