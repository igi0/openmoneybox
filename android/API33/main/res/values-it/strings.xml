<!-- /***************************************************************
* Name:
* Purpose:   OpenMoneyBox string resources (it_it)
* Author:    Igor Calì (igor.cali0@gmail.com)
* Created:   2024-12-06
* Copyright: Igor Calì (igor.cali0@gmail.com)
* License:		GNU
**************************************************************/
-->

<resources>
	<string name="lang">it</string>
    
    <string name="app_name">Portamonete</string>

    <string name="dashboard">Panoramica</string>

    <string name="funds">Fondi</string>

    <string name="credits">Crediti</string>

    <string name="debts">Debiti</string>

    <string name="objects">Oggetti</string>

    <string name="report">Resoconto</string>

    <string name="shoplist">Lista della spesa</string>

    <string name="charts">Grafici</string>
    <string name="fund_chart">Grafico dei fondi</string>
    <string name="fund_others">Altri</string>
    <string name="trend_chart">Grafico dell\'andamento</string>
    <string name="trend_last">Andamento</string>

    <string name="options">Opzioni</string>
    <string name="xml_autoexport">Esporta XML automaticamente alla fine del mese</string>
    <string name="xml_export">Esporta XML</string>
    <string name="xml_title">Resoconto mensile di</string>
    <string name="xml_completed">Documento esportato</string>
    <string name="defaultdocument">Documento predefinito</string>
    <string name="document">Documento</string>
    <string name="monthly">Esporta mensilmente</string>
    <string name="prefix">Prefisso documenti</string>
    <string name="prefix_description">Prefisso per i documenti generati (XML, backup, ecc…)</string>
    <string name="setting_dark_theme_title">Modalità notturna</string>
    <string name="setting_dark_theme_description">Imposta tema scuro</string>
    <string name="setting_security">Codice di sicurezza</string>
    <string name="setting_security_description">Credenziali del dispositivo richieste all\'avvio</string>

    <string name="wizard">Creazione guidata</string>

    <string name="browse">Sfoglia</string>

    <string name="documentselector">Seleziona il documento attivo.\nÈ possibile cercare un documento di Portamonete esistente o lanciare la creazione guidata:</string>
    <!--
    <string name="lent">Prestati</string>
    <string name="borrowed">In prestito</string>
    <string name="credit_new">Nuovo credito</string>
    <string name="debt_new">Nuovo debito</string>
    <string name="object_received">Ricevuto oggetto</string>
    <string name="object_given">Donato oggetto</string>
    <string name="object_lent">Prestato oggetto</string>
    <string name="object_borrowed">Oggetto in prestito</string>
    -->
    <string name="profit">Guadagno</string>
    <string name="expense">Spesa</string>
    <string name="total">Totale</string>
    <string name="credit_remove">Rimosso credito</string>
    <string name="credit_remit">Condonato credito</string>
    <string name="debt_remove">Rimosso debito</string>
    <string name="debt_remit">Condonato debito</string>
    <string name="object_gotback">Restituito oggetto</string>
    <string name="object_givenback">Ridato oggetto</string>
    <!-- old label setting (versionCode <= 11)
        <string name="labels">Etichette</string>
    <string name="save">Salva</string>
    <string name="password">Password</string>
    <string name="password_description">Password del documento</string>
    -->
    <string name="password_change">Cambia password</string>
    <!--  <string name="password_insert">Inserisci la password:</string> -->
    <string name="file_changed">Il documento è stato modificato.\nVuoi salvare?</string>
    <string name="profit_caption">Memorizza un guadagno</string>
    <string name="profit_fund">Seleziona il fondo a cui aggiungere il guadagno:</string>
    <!-- <string name="profit_value">Inserisci il valore del guadagno</string>
    <string name="profit_reason">Inserisci la motivazione del guadagno:</string>
    <string name="expense_value">Inserisci il valore della spesa:</string>
    <string name="expense_reason">Inserisci la motivazione della spesa:</string>
    <string name="profit_category">Scegli la categoria del guadagno:</string>
    -->

    <string name="expense_caption">Memorizza una spesa</string>
    <string name="expense_fund">Seleziona il fondo da cui sottrarre la spesa:</string>
    <string name="expense_category">Scegli la categoria della spesa:</string>
    <string name="categories_edit">Modifica le categorie</string>
    <string name="categories_add">Aggiungi categoria</string>
    <string name="categories_remove">Rimuovi categora</string>
    <string name="fund_add">Imposta un fondo</string>
    <!--
    <string name="fund_name">Seleziona un fondo:</string>
    -->
    <string name="fund_value">Imposta il valore del fondo</string>
    <string name="fund_insert">Inserisci il nome del fondo</string>
    <string name="fund_reset">Reimposta il fondo %s:</string>
    <string name="fund_settotal">Imposta totale</string>
    <string name="fund_default">Fondo predefinito</string>
    <string name="fund_exhaust">Il fondo \"%s\" è esaurito. Vuoi rimuoverlo?</string>
    <string name="default_name">Predefinito</string>
    <string name="fund_default_choose">Scegli il fondo predefinito:</string>

    <string name="credit_add">Imposta un credito</string>
    <string name="credit_value">Imposta il valore del credito:</string>
    <string name="credit_insert">Inserisci il nome del credito:</string>
    <string name="credit_delete">Rimuovi un credito</string>
    <string name="credit_remit2">Condona un credito</string>
    <string name="credit_term">credito</string>
    <string name="credit_keep_budget">Mantieni l\'attuale budget</string>

    <string name="remove_total">Rimuovi il valore totale</string>
    <string name="remove_partially">Rimuovi un valore parziale</string>
    <string name="remit_total">Condona il valore totale</string>
    <string name="remit_partially">Condona un valore parziale</string>
    <string name="debt_add">Imposta un debito</string>
    <string name="debt_value">Imposta il valore del debito:</string>
    <string name="debt_insert">Inserisci il nome del debito:</string>
    <string name="debt_delete">Rimuovi un debito</string>
    <string name="debt_remit2">Condona un debito</string>
    <string name="debt_term">debito</string>
    
    <string name="object_receive">Ricevi un oggetto</string>
    <string name="object_received_name">Inserisci l\'oggetto ricevuto</string>
    <string name="object_donor">Imposta il donatore</string>
    <string name="object_give">Dona un oggetto</string>
    <string name="object_given_name">Inserisci l\'oggetto donato</string>
    <string name="object_receiver">Imposta il ricevente</string>
    <string name="object_lend">Presta un oggetto</string>
    <string name="object_lent_name">Inserisci l\'oggetto prestato</string>
    <string name="object_borrow">Prendi un oggetto in prestito</string>
    <string name="object_borrow_name">Inserisci l\'oggetto preso in prestito</string>
    <string name="shoplist_caption">Aggiungi una voce alla lista della spesa</string>
    <string name="shoplist_item">Inserisci la voce da aggiungere</string>
    <string name="shoplist_bought">Vuoi rimuovere la voce %s dalla lista della spesa?</string>

    <string name="wizard_caption">Creazione guidata documento</string>
    <string name="wizard_saved">Inserisci l\'ammontare del denaro risparmiato:</string>
    <string name="wizard_cash">Inserisci l\'ammontare del denaro contante:</string>
    <string name="wizard_saved_fund">Soldi risparmiati</string>
    <string name="wizard_cash_fund">Contanti</string>

    <string name="about">Informazioni</string>
    <string name="about_title">Informazioni su</string>
    <string name="about_text">Supporto email</string>
    <string name="about_version">Versione</string>
    <string name="about_privacy">Informativa sulla Privacy</string>

    <string name="dialog_cancel">Cancella</string>
    <!-- <string name="dialog_ok">Ok</string> -->
    <string name="dialog_no">No</string>
    <string name="dialog_confirm">Conferma</string>
    
	<string name="alarm">Promemoria</string>
	<string name="alarm_postpone">Posticipa</string>
	<string name="alarm_postpone_all">Posticipa tutti</string>
	<string name="alarm_detail_lent">Il promemoria per l\'oggetto prestato \"%s\" è scaduto!</string>
	<string name="alarm_detail_borrowed">Il promemoria per l\'oggetto in prestito \"%s\" è scaduto!</string>
	<string name="alarm_detail_shoplist">Il promemoria per l\'oggetto in lista della spesa \"%s\" è scaduto!</string>
	<string name="alarm_none">Nessun promemoria</string>
			
	<string name="error_01">Il documento %s è già aperto!</string>
	<string name="error_02">Il documento %s non contiene dati di Portamonete validi!</string>
	<string name="error_03">Il documento è già nel formato Bil2.x!</string>
	<string name="error_04">Password errata!</string>
	<string name="error_05">Devi selezionare un debito!</string>
	<string name="error_06">Errore di configurazione! È necessario reinstallare l\'applicazione.</string>
	<string name="error_08">Nessun fondo può essere rimosso!</string>
	<string name="error_09">Nessun fondo può essere modificato!</string>
	<string name="error_10">Errore nei dati di bilancio!</string>
	<string name="error_11">Il fondo %s esiste già!</string>
	<string name="error_12">Il valore del fondo deve essere minore o uguale al fondo predefinito!</string>
	<string name="error_13">Denaro insufficiente per l\'operazione!</string>
	<string name="error_14">Nessun oggetto prestato!</string>
	<string name="error_15">Nessun oggetto in prestito!</string>
	<string name="error_16">Nessuna voce nella lista della spesa!</string>
	<string name="error_17">Quest\'operazione è valida solo per aumentare il bilancio!</string>
	<string name="error_18">Errore nei dati di bilancio! Verificare che il fondo predefinito sia impostato.</string>
	<string name="error_19">Impossibile impostare un credito maggiore del fondo predefinito!</string>
	<string name="error_20">Nessun credito memorizzato!</string>
	<string name="error_21">Il valore inserito è maggiore del %s selezionato!</string>
	<string name="error_22">Nessun debito memorizzato!</string>
	<string name="error_23">Nessun fondo selezionato!</string>
	<string name="error_24">Denaro insufficiente nel fondo predefinito!</string>
	<string name="error_25">Devi specificare un valore!</string>
	<string name="error_28">Impossibile aprire l\'ultimo documento!</string>
	<string name="error_29">Devi inserire una motivazione!</string>
	<string name="error_30">Devi inserire un nome!</string>
	<string name="error_31">Nessuna voce selezionata nella lista della spesa!</string>
	<string name="error_32">Devi inserire un oggetto!</string>
	<string name="error_33">Devi inserire un contatto!</string>
	<string name="error_34">Due etitchette sono identiche!</string>
	<string name="error_35">La cartella non esiste! Inserire un percorso valido o cliccare il tasto Sfoglia</string>
	<string name="error_36">Impossibile aprire l\'ultimo documento!</string>
	<string name="error_37">Impossibile ordinare i dati!</string>
	<string name="error_38">Stringa vuota!</string>
	<string name="error_39">Devi selezionare un credito!</string>
	<string name="error_40">Devi inserire la password!</string>
	<string name="error_41">Il documento %s è di sola lettura!</string>
	<string name="error_45">Impossibile trovare il documento %s</string>
	<string name="error_46">Data non valida</string>
	<string name="error_48">La voce %s esiste già!</string>
    <string name="error_49">Inserire il simbolo di valuta!</string>
	<string name="error_unk">Errore sconosciuto</string>

	<string name="options_donate">Dona</string>
	<string name="options_help">Guida in linea</string>

	<!--
    <string name="report_date">Data</string>
	<string name="report_operation">Operazione</string>
	<string name="report_value">Valore</string>
	<string name="report_reason">Motivazione</string>
	<string name="report_category">Categoria</string>
    -->

	<string name="export_archive_menu">Esporta archivio</string>
	<string name="export_archive_complete">Archivio esportato</string>
	<string name="options_bugreport">Segnala problema</string>
    <string name="options_license">Licenza</string>

    <string name="permission_write_needed">Non è possibile usare questa app senza i permessi richiesti.</string>
    <string name="locations_wont_store_warning">Le posizioni non saranno salvate.</string>
    <string name="contacts_cant_access">Nessun accesso ai contatti.</string>
    <string name="contacts_selection">Scegli un contatto:</string>

    <string name="navigation_drawer_open">Apri pannello di navigazione</string>
    <string name="navigation_drawer_close">Chiudi pannello di navigazione</string>
    <string name="navigation_drawer_map">Mappa</string>

    <string name="setting_position_title">Salva le posizioni</string>
    <string name="setting_position_description">Memorizza le posizioni conosciute per le operazioni</string>

    <string name="abbreviation_latitude">Lat:</string>
    <string name="abbreviation_longitude">Long:</string>

    <string name="app_icon">icona app</string>
    <string name="confirm">Conferma</string>
    <string name="contact_picture">Foto del contatto</string>
    <string name="search">Cerca</string>
    <string name="object_icon">Icona dell\'oggetto</string>
    <string name="operation_icon">Icona dell\'operazione</string>
    <string name="location_icon">Icona della posizione</string>
    <string name="hint_insert_value">Inserire il valore</string>
    <string name="hint_insert_reason">Inserire la motivazione</string>
    <string name="hint_insert_category">Scegli la categoria</string>
    <!-- <string name="hint_insert_name">Inserire il nome</string>
    -->

    <string name="notification_channel_alarm">Promemoria</string>
    <string name="notification_channel_service">Servizio notifiche</string>

    <string name="default_currency">Valuta predefinita</string>
    <string name="currency_hint">Symbolo valuta</string>
    <string name="rate_hint">Tasso di cambio (predefinita/personalizzata)</string>

    <string name="iconLabel">Icona</string>
    <string name="topcategories">Categorie in evidenza</string>
    <string name="icon_choose_title">Scegli l\'icona</string>
    <string name="topcategories_none">Altri</string>

    <string name="shopitem_delete">Tocca una voce per cancellarla.</string>

    <string name="website">Sito web:</string>

    <string name="authentication_title">Verifica la tua identità</string>
    <string name="authentication_error">Errore di autenticazione: </string>
    <string name="authentication_failed">Autenticazione fallita!</string>

    <string name="dialog_overlay_permission">L\'applicazione ha bisogno dei permessi per partire all\'avviamento del dispositivo (servizio notifiche).\n\nPremi \'OK\' per concederli.</string>
    <string name="allfiles_access_permission">L\'applicazione potrebbe necessitare dei permessi di accesso a tutti i file sul dispositivo.\n\nPremi \'OK\' per concederli.</string>

    <string name="accessibility_fund_action_button">Aggiunge un nuovo fondo</string>
    <string name="accessibility_fundGroup_action_button">Aggiunge un nuovo gruppo</string>
    <string name="preference_category_behaviour">Comportamento</string>
    <string name="preference_category_look">Aspetto</string>
    <string name="preference_security">Sicurezza</string>

    <string name="permission_boot_no">L\'app non partirà all\'avvio.</string>
    <string name="permission_contacts_no">Non sarà possibile accedere ai contatti.</string>
    <string name="export_created_by">Documento creato da Portamonete</string>
    <string name="export_created_on">il</string>

    <!-- Fund groups strings from here-->
    <string name="fundGroups_form_title">Imposta i gruppi di fondi</string>
    <string name="fundGroups_avail_groups">Gruppi disponibili:</string>
    <string name="fundGroups_remove_group">Rimuove un gruppo</string>
    <string name="fundGroups_new_fund_hint">Digita qui il nome del nuovo gruppo</string>
    <string name="fundGroups_add_group">Aggiunge un gruppo</string>
    <string name="fundGroups_avail_funds">Fondi:</string>
    <string name="fundGroups_remarks">Nota: Ogni fondo può appertenere ad un solo gruppo.\nNota: il fondo predefinito non può essere raggruppato.</string>
    <string name="fundGroups_error_few_funds">Selezionare almeno due fondi!</string>
    <string name="fundGroups_error_no_name">Inserire un nome per il gruppo!</string>
    <string name="fundGroups_error_existing_group">Il gruppo \'%s\' esiste già!</string>
    <string name="fundGroups_pref_title">Raggruppa i fondi</string>
    <string name="fundGroups_pref_description">Con questa impostazione attiva e se sono definiti dei gruppi di fondi, essi vengono visualizzati in una vista semplificata.</string>
    <string name="fundGroups_group_marker">Gruppo</string>
    <!-- -->

    <!-- SqlCipher compatibility v4 strings from here-->
    <string name="sqlcipher_v4_migration_done">Migrazione del database \'%s\' alla versione v4 di SqlCipher avvenuta con successo.</string>
    <string name="sqlcipher_v4_pref_title">Versione di compatibilità SqlCipher</string>
    <string name="sqlcipher_v4_pref_warning">Formato SqlCipher v3\n
        ⚠️ Se spuntato, una migrazione del database verrà tentata al prossimo avvio dell\'app.
        Assicurarsi che la compatibilità sia disponibile per tutti i sistemi operaratici con cui viene utilizzata l\'app.
        Vedere https://igisw-bilancio.sourceforge.net/wiki/CipherCompat.pdf per la compatibilità di Portamonete nei sistemi operativi disponibili.</string>
    <string name="sqlcipher_v4_pref_on">Formato SqlCipher v4</string>
    <string name="sqlcipher_error_database_v4">Il formato del database \'%s\' è SqlCipher version 4.\n\nLe impostazioni dell\'app verranno aggiornate ed il programma chiuso.\nIl programma dovrebbe funzionare correttamente al prossimo avvio.</string>
    <!-- -->

    <string name="updates">Modifiche:\n\nv3.5.1.2:
            \n  - Traduzione in Francese.
	</string>

</resources>
