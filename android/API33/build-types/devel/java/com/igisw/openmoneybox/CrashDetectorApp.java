/* **************************************************************
 * Name:
 * Purpose:   OpenMoneyBox Application development crash detector app
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-07-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

package com.igisw.openmoneybox;

import androidx.multidex.MultiDexApplication;
import android.content.Context;

import org.acra.*;
import org.acra.annotation.*;

@AcraCore(buildConfigClass = BuildConfig.class)
@AcraMailSender(mailTo = "igor.cali@vfemail.net")
public class CrashDetectorApp extends MultiDexApplication {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        // The following line triggers the initialization of ACRA
        ACRA.init(this);
    }
}