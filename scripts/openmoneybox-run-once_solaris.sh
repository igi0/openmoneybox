#!/usr/bin/sh
# last modified: 2024-10-01

. /lib/svc/share/smf_include.sh
assembled=$(/usr/bin/svcprop -p config/assembled $SMF_FMRI)
if [ "$assembled" == "true" ] ; then
    exit $SMF_EXIT_OK
fi
svccfg -s $SMF_FMRI setprop config/assembled = true
svccfg -s $SMF_FMRI refresh

echo "Updating glib schemas..."
glib-compile-schemas /usr/share/glib-2.0/schemas
echo "glib schemas updated."

echo "Updating mime types..."
update-mime-database /usr/share/mime
echo "mime types updated."

