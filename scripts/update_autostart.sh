#!/bin/bash
# Author:    Igor Calì (igor.cali0@gmail.com)
# last change 2023-03-18

USR_HOME="$(getent passwd $SUDO_USER | cut -d: -f6)"
DIR=/.config/autostart/
FILE=ombtray.desktop
NEW_FILE=openmoneybox.desktop
FULL_PATH=$USR_HOME$DIR$FILE

if [ -f "$FULL_PATH" ]; then
    echo "Autostart file $FULL_PATH found..."
    sed -i "s/ombtray/openmoneybox --tray --force/g" $FULL_PATH
    mv $FULL_PATH $USR_HOME$DIR$NEW_FILE
    echo "Autostart file converted in $USR_HOME$DIR$NEW_FILE."
fi

