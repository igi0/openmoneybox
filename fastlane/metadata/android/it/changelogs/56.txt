Modifiche v3.4.2.6:
  - Aggiunti colori splash screen;
  - Rimossi permessi inutilizzati (stato telefono e rete);
  - compileSdk 34;
  - AndroidChart v3.1.0.18;
  - osmdroid 6.1.17.

