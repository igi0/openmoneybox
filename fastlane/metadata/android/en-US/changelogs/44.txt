Changelog v3.4.1.11:
  - Global file permission fixed on Android 11 [gitlab #13];
  - Fixed crash when active locale has non-latin numbers [gitlab #16];
  - Italian translation updated;
  - sqlcipher 4.4.3;
  - osmdroid 6.1.11;
  - New icon for Add Shopping-list item.

