﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML 5.0b3//EN" "http://www.oasis-open.org/docbook/xml/5.0b3/dtd/docbook.dtd" [

<!-- last update: 2024-08-26 -->

<!ENTITY auth SYSTEM "en/Author.xml">
<!ENTITY intro SYSTEM "en/Introduction.xml">

  <!ENTITY globdate SYSTEM "en/Global_datetime.xml">
  <!ENTITY customcurr SYSTEM "en/custom_currency.xml">
   
  <!ENTITY savef SYSTEM "en/Save_file.xml">
  <!ENTITY revertf SYSTEM "en/Revert_file.xml">
  <!ENTITY exportf SYSTEM "en/Export_file.xml">
  <!ENTITY pass SYSTEM "en/Password.xml">
  <!ENTITY archive SYSTEM "en/browse_archive.xml">
  <!ENTITY exitp SYSTEM "en/Exit.xml">

  <!ENTITY copyl SYSTEM "en/Copy.xml">
  <!ENTITY findt SYSTEM "en/Find.xml">

  <!ENTITY newfund SYSTEM "en/New_fund.xml">
  <!ENTITY remfund SYSTEM "en/Remove_fund.xml">
  <!ENTITY editfund SYSTEM "en/Edit_fund.xml">
  <!ENTITY totfund SYSTEM "en/Total_fund.xml">
  <!ENTITY deffund SYSTEM "en/Default_fund.xml">
  <!ENTITY fundgrps SYSTEM "en/FundGroups.xml">

  <!ENTITY systime SYSTEM "en/Manual_time.xml">
  <!ENTITY gain SYSTEM "en/Gain_operations.xml">
  <!ENTITY expense SYSTEM "en/Expense_operations.xml">
  <!ENTITY cat_select SYSTEM "en/category_select.xml">
  <!ENTITY cat_edit SYSTEM "en/category_edit.xml">

  <!ENTITY objget SYSTEM "en/Object_Get.xml">
  <!ENTITY objgiv SYSTEM "en/Object_Given.xml">
  <!ENTITY objlen SYSTEM "en/Lend.xml">
  <!ENTITY objgetback SYSTEM "en/GetBack.xml">
  <!ENTITY objbor SYSTEM "en/Borrow.xml">
  <!ENTITY objgiveback SYSTEM "en/GiveBack.xml">

  <!ENTITY alarms SYSTEM "en/Alarms.xml">

  <!ENTITY keepbdg SYSTEM "en/keep_budget.xml">
  <!ENTITY credset SYSTEM "en/Credit_set.xml">
  <!ENTITY credrem SYSTEM "en/Credit_remove.xml">
  <!ENTITY credcon SYSTEM "en/Credit_remit.xml">

  <!ENTITY debset SYSTEM "en/Debt_set.xml">
  <!ENTITY debrem SYSTEM "en/Debt_remove.xml">
  <!ENTITY debcon SYSTEM "en/Debt_remit.xml">

  <!ENTITY addshop SYSTEM "en/AddShopItem.xml">
  <!ENTITY remshop SYSTEM "en/Remove_item.xml">

  <!ENTITY opt SYSTEM "en/Options.xml">
  <!ENTITY ombtray SYSTEM "en/ombtray.xml">
  <!ENTITY ombwiz SYSTEM "en/Wizard.xml">
  <!ENTITY ombconvert SYSTEM "en/convert.xml">
  <!ENTITY ombimport SYSTEM "en/import.xml">
  <!ENTITY ombupdate SYSTEM "en/Update.xml">
  <!ENTITY ombmap SYSTEM "en/map.xml">

<!ENTITY command SYSTEM "en/command_line.xml">
<!ENTITY shortc SYSTEM "en/Shortcuts.xml">
<!ENTITY histo SYSTEM "en/History.xml">

<!ENTITY lang "en">

]>

<book lang="en">
  <title>Budget management</title>
  <titleabbrev>OpenMoneyBox</titleabbrev>
  
&auth;
&intro;

<part>
<title>Application time and date</title>
  &globdate;
</part>

<part>
<title>File operations</title>
  &savef;
  &revertf;
  &exportf;
  &pass;
  &archive;
  &exitp;
</part>

<part>
<title>Monthly report operations</title>
  &copyl;
  &findt;
</part>

<part>
<title>Funds operations</title>
  &newfund;
  &remfund;
  &editfund;
  &totfund;
  &deffund;
  &fundgrps;
</part>

<part>
<title>Profits and expenses</title>
  &gain;
  &expense;
  &cat_select;
  &cat_edit;
</part>

<part>
<title>Objects operations</title>
  <chapter>
  <title>Objects reception and donations</title>
    &objget;
    &objgiv;
  </chapter>
  <chapter>
  <title>Lent objects</title>
      &objlen;
      &objgetback;
  </chapter>
  <chapter>
  <title>Borrowed objects</title>
      &objbor;
      &objgiveback;
  </chapter>
</part>

<part>
<title>Alarms</title>
  &alarms;
</part>

<part>
<title>Credits</title>
  &credset;
  &credrem;
  &credcon;
</part>

<part>
<title>Debts</title>
  &debset;
  &debrem;
  &debcon;
</part>

<part>
<title>Shopping list</title>
  &addshop;
  &remshop;
</part>

<part>
<title>Utilities</title>
  &opt;
  &ombtray;
  &ombwiz;
  &ombconvert;
  &ombimport;
  &ombupdate;
   &ombmap;
</part>

&command;
&shortc;
&histo;
 </book>
