# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2016-10-31 10:32+0000\n"
"PO-Revision-Date: 2017-03-05 21:27+0200\n"
"Last-Translator: Igor Calì <igor.cali0@gmail.com>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: import.xml:1
#, no-c-format
msgid "Import document"
msgstr ""

#. Tag: para
#: import.xml:2
#, no-c-format
msgid ""
"Click the menu item <guimenu>Tools</guimenu> -&gt; <guimenuitem>Import</"
"guimenuitem> to import an OpenMoneyBox document into the master database."
msgstr ""
