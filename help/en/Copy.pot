# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-08-31 20:36+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Copy.xml:1
#, no-c-format
msgid "Copy part of the report"
msgstr ""

#. Tag: para
#: Copy.xml:2
#, no-c-format
msgid "Click the menu item <guimenu>Edit </guimenu> -&gt;<guimenuitem> Copy</guimenuitem> (or the related button in the toolbar) to copy currently selected line of the monthly report in <application os=\"linux\">Linux</application> <application os=\"windows\"><trademark>Windows</trademark></application> clipboard."
msgstr ""

#. Tag: para
#: Copy.xml:8
#, no-c-format
msgid "<link linkend=\"scuts\">Shortcut</link>: <keycombo action=\"simul\"> <keycap>CTRL</keycap> <keycap>C</keycap> </keycombo>"
msgstr ""

