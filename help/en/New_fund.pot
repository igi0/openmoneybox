# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-09-06 14:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: New_fund.xml:1
#, no-c-format
msgid "Store a new fund"
msgstr ""

#. Tag: para
#: New_fund.xml:2
#, no-c-format
msgid "Click the menu item <guimenu>Funds</guimenu> -&gt; <guimenuitem>New</guimenuitem> (or the related button in the toolbar) to store a new fund."
msgstr ""

#. Tag: para
#: New_fund.xml:9
#, no-c-format
msgid "Type in the first edit box the name to identify the new fund: it will be added in the list on the left of the client window."
msgstr ""

#. Tag: para
#: New_fund.xml:11
#, no-c-format
msgid "Type in the second edit box the value to be assigned to the fund: it will be subtracted to the <link linkend=\"deffund\">default fund</link>; in case the specified value is bigger of that fund an error message will be shown."
msgstr ""

#. Tag: para
#: New_fund.xml:15
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""

#. Tag: para
#: New_fund.xml:17
#, no-c-format
msgid "<link linkend=\"scuts\">Shortcut</link>: <keycombo action=\"simul\"> <keycap>CTRL</keycap> <keycap>ALT</keycap> <keycap>N</keycap> </keycombo>"
msgstr ""

#. Tag: para
#: New_fund.xml:24
#, no-c-format
msgid "Related topics: <link linkend=\"deffund\">Set the defaut fund</link>."
msgstr ""

