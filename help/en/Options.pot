# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-09-05 15:33+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Options.xml:1
#, no-c-format
msgid "Program customisation"
msgstr ""

#. Tag: para
#: Options.xml:2
#, no-c-format
msgid ""
"The program can be customised through the menu item <guimenu>Tools</guimenu> "
"-&gt; <guimenuitem>Options</guimenuitem>."
msgstr ""

#. Tag: para
#: Options.xml:3
#, no-c-format
msgid ""
"The dialog window will be shown, where it is possible to choose settings."
msgstr ""

#. Tag: title
#: Options.xml:7
#, no-c-format
msgid "'General' page"
msgstr ""

#. Tag: listitem
#: Options.xml:10
#, no-c-format
msgid ""
"<emphasis>Default document</emphasis>: type the full path of default "
"document or click the Browse button to search for it;"
msgstr ""

#. Tag: listitem
#: Options.xml:13
#, no-c-format
msgid ""
"<emphasis>Show command toolbar</emphasis>: check this box to show the "
"command toolbar;"
msgstr ""

#. Tag: listitem
#: Options.xml:16
#, no-c-format
msgid ""
"<emphasis>Automatically convert as XML at the end of each month</emphasis>: "
"check this box if you want the program to convert documents to XML on month "
"change;"
msgstr ""

#. Tag: listitem
#: Options.xml:19
#, no-c-format
msgid ""
"<emphasis>Enable tray-bar icon</emphasis>: check this box if you want to run "
"the Alarm management utility on program shutdown to work in background."
msgstr ""

#. Tag: listitem
#: Options.xml:24
#, no-c-format
msgid ""
"<emphasis>Prefix for generated documents</emphasis>: type the prefix you "
"want to use for your documents, when generating xml's or backups."
msgstr ""

#. Tag: listitem
#: Options.xml:27
#, no-c-format
msgid ""
"<emphasis>Group funds</emphasis>: Fund groups are shown in simplified "
"summary when this setting is enabled and any group is defined."
msgstr ""

#. Tag: title
#: Options.xml:34
#, no-c-format
msgid "'Charts' page"
msgstr ""

#. Tag: listitem
#: Options.xml:37
#, no-c-format
msgid ""
"<emphasis>Show fund chart</emphasis>: check this box if you want to show the "
"pie chart related to bugdet distribution;"
msgstr ""

#. Tag: listitem
#: Options.xml:40
#, no-c-format
msgid ""
"<emphasis>Show trend chart</emphasis>: check this box if you want to show "
"the chart related to budget trend in current month;"
msgstr ""

#. Tag: title
#: Options.xml:46
#, no-c-format
msgid "'Advanced' page"
msgstr ""

#. Tag: listitem
#: Options.xml:49
#, no-c-format
msgid ""
"<emphasis>Search for new version on startup</emphasis>: check this box if "
"you want the program to verify online the availability of possible updates"
msgstr ""

#. Tag: note
#: Options.xml:51
#, no-c-format
msgid ""
"This function is useful only if the <link linkend=\"upd\">Web update "
"Utility</link> is installed."
msgstr ""

#. Tag: title
#: Options.xml:57
#, no-c-format
msgid "'External tools' page"
msgstr ""

#. Tag: para
#: Options.xml:58
#, no-c-format
msgid ""
"Click the Add button to browse and add an application or document as an "
"external tool."
msgstr ""

#. Tag: para
#: Options.xml:59
#, no-c-format
msgid ""
"Select an external tool in the list and click the Remove button to remove it "
"from the list."
msgstr ""

#. Tag: para
#: Options.xml:60
#, no-c-format
msgid ""
"Stored external tools can be accessed through the menu item <guimenu>Tools</"
"guimenu> -&gt; <guimenuitem>External tools</guimenuitem>."
msgstr ""

#. Tag: para
#: Options.xml:64
#, no-c-format
msgid ""
"<link linkend=\"scuts\">Shortcut</link>: <keycombo action=\"simul\"> "
"<keycap>CTRL</keycap> <keycap>O</keycap> </keycombo>"
msgstr ""

#. Tag: para
#: Options.xml:71
#, no-c-format
msgid ""
"Related topics: <phrase><link linkend=\"tray\">Alarm management Utility</"
"link>;</phrase> <phrase><link linkend=\"upd\">Web update Utility</link>.</"
"phrase>"
msgstr ""

msgid "'Database' page"
msgstr ""

msgid "<emphasis>SqlCipher compatibility version</emphasis>: allows to select the new SqlCipher defaults v4."
msgstr ""

msgid ""
"This page is available only on distributions which support SqlCipher v4 "
"compatibility. See <ulink url=\"https://igisw-bilancio.sourceforge.net/wiki/"
"CipherCompat.pdf\">this page</ulink> for the compatibility on different "
"operating systems."
msgstr ""

msgid ""
"A database migration will be attempted at next application startup if 'v4' "
"is selected."
msgstr ""


