# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2014-08-31 20:36+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: Revert_file.xml:1
#, no-c-format
msgid "Revert document changes"
msgstr ""

#. Tag: para
#: Revert_file.xml:3
#, no-c-format
msgid "Click the menu item <guimenu>File</guimenu> -&gt;<guimenuitem> Revert</guimenuitem> to reopen a document, losing all changes."
msgstr ""

#. Tag: para
#: Revert_file.xml:5
#, no-c-format
msgid "All changes will not be recovereable."
msgstr ""

