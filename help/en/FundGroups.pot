# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-08-26 14:14+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: FundGroups.xml:1
#, no-c-format
msgid "Fund groups"
msgstr ""

#. Tag: para
#: FundGroups.xml:2
#, no-c-format
msgid ""
"Click the menu item <guimenu>Funds</guimenu> -&gt; <guimenuitem>Fund groups</"
"guimenuitem> to create or delete the fund groups."
msgstr ""

#. Tag: para
#: FundGroups.xml:8
#, no-c-format
msgid ""
"Type the name of the new group in the edit box and click the Plus button to "
"add a new group."
msgstr ""

#. Tag: para
#: FundGroups.xml:9
#, no-c-format
msgid ""
"Select an existing fund in the group list and click the Bin button to remove "
"an existing group."
msgstr ""

#. Tag: para
#: FundGroups.xml:10
#, no-c-format
msgid ""
"To group the children of a group, select the group in the list and check the "
"desired funds in the fund list in the bottom of the window."
msgstr ""

#. Tag: para
#: FundGroups.xml:11
#, no-c-format
msgid "Press the OK button to confirm the edited configuration."
msgstr ""

#. Tag: para
#: FundGroups.xml:12
#, no-c-format
msgid "In case of missing or wrong data entry an error message will be shown."
msgstr ""

#. Tag: para
#: FundGroups.xml:14
#, no-c-format
msgid "Related topics: <link linkend=\"fundgroups\">Group fund setting</link>."
msgstr ""
