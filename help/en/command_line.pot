# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-18 14:22+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: command_line.xml:2
#, no-c-format
msgid "Command line parameters"
msgstr ""

#. Tag: para
#: command_line.xml:4
#, no-c-format
msgid "Syntax: openmoneybox [[--tray] [--force]]"
msgstr ""

#. Tag: listitem
#: command_line.xml:8
#, no-c-format
msgid "--tray: starts minimized in the system tray."
msgstr ""

#. Tag: listitem
#: command_line.xml:11
#, no-c-format
msgid "--force: forces new instance."
msgstr ""
