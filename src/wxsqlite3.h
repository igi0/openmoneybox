/***************************************************************
 * Name:      wxsqlite3.h
 * Purpose:   sqlite3 wrapper
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2019-04-02
 * Source derived from https://github.com/utelle/wxsqlite3/blob/master/include/wx/wxsqlite3.h
 * License:		GNU
 **************************************************************/

#ifndef fSQLITE3_H
#define fSQLITE3_H

#include <wx/wx.h>

/// Holds the complete result set of a SQL query
class /*WXDLLIMPEXP_SQLITE3*/ wxSQLite3Table
{
public:
  /// Constructor
  wxSQLite3Table();

  wxSQLite3Table(const wxSQLite3Table& table);

  wxSQLite3Table(char** results, int rows, int cols);

  virtual ~wxSQLite3Table();

  wxSQLite3Table& operator=(const wxSQLite3Table& table);

  /// Get the number of columns in the result set
  /**
  * \return the number of columns
  */
  //int GetColumnCount() const;

  /// Get the number of rows in the result set
  /**
  * \return the number of rows
  */
  int GetRowCount() const;

  /// Find the index of a column by name
  /**
  * \param columnName name of the column
  * \return the index of the column
  */
  int FindColumnIndex(const wxString& columnName) const;

  /// Get the name of a column
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \return name of the column
  */
  //wxString GetColumnName(int columnIndex) const;

  /// Get a column as a string using the column index
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \return value of the column as a string
  *
  * \note This method returns values of type <code>double</code>
  * always using the point character as the decimal separator.
  * This is SQLite default behaviour. Use method wxSQLite3Table::GetDouble
  * to apply correct conversion from <code>string</code> to <code>double</code>.
  */
  wxString GetAsString(int columnIndex) const;

  /// Get a column as a string using the column name
  /**
  * \param columnName name of the column
  * \return value of the column as a string
  *
  * \note This method returns values of type <code>double</code>
  * always using the point character as the decimal separator.
  * This is SQLite default behaviour. Use method wxSQLite3Table::GetDouble
  * to apply correct conversion from <code>string</code> to <code>double</code>.
  */
  wxString GetAsString(const wxString& columnName) const;

  /// Get a column as an integer using the column index
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \param nullValue value to be returned in case the column is NULL
  * \return value of the column
  */
  int GetInt(int columnIndex, int nullValue = 0) const;

  /// Get a column as an integer using the column name
  /**
  * \param columnName name of the column
  * \param nullValue value to be returned in case the column is NULL
  * \return value of the column
  */
  int GetInt(const wxString& columnName, int nullValue = 0) const;

  /// Get a column as a 64-bit integer using the column index
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \param nullValue value to be returned in case the column is NULL
  * \return value of the column
  */
  //wxLongLong GetInt64(int columnIndex, wxLongLong nullValue = 0) const;

  /// Get a column as an integer using the column name
  /**
  * \param columnName name of the column
  * \param nullValue value to be returned in case the column is NULL
  * \return value of the column
  */
  //wxLongLong GetInt64(const wxString& columnName, wxLongLong nullValue = 0) const;

  /// Get a column as a double using the column index
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \param nullValue value to be returned in case the column is NULL
  * \return value of the column
  */
  double GetDouble(int columnIndex, double nullValue = 0.0) const;

  /// Get a column as a double using the column name
  /**
  * \param columnName name of the column
  * \param nullValue value to be returned in case the column is NULL
  * \return value of the column
  */
  double GetDouble(const wxString& columnName, double nullValue = 0.0) const;

  /// Get a column as a string using the column index
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \param nullValue value to be returned in case the column is NULL
  * \return value of the column
  */
  wxString GetString(int columnIndex, const wxString& nullValue = wxEmptyString) const;

  /// Get a column as a string using the column name
  /**
  * \param columnName name of the column
  * \param nullValue value to be returned in case the column is NULL
  * \return value of the column
  */
  wxString GetString(const wxString& columnName, const wxString& nullValue = wxEmptyString) const;

  /// Get a column as a date value using the column index
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \return value of the column
  */
  //wxDateTime GetDate(int columnIndex) const;

  /// Get a column as a date value using the column name
  /**
  * \param columnName name of the column
  * \return value of the column
  */
  //wxDateTime GetDate(const wxString& columnName) const;

  /// Get a column as a time value using the column index
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \return value of the column
  */
  //wxDateTime GetTime(int columnIndex) const;

  /// Get a column as a time value using the column name
  /**
  * \param columnName name of the column
  * \return value of the column
  */
  //wxDateTime GetTime(const wxString& columnName) const;

  /// Get a column as a date/time value using the column index
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \return value of the column
  */
  //wxDateTime GetDateTime(int columnIndex) const;

  /// Get a column as a date/time value using the column name
  /**
  * \param columnName name of the column
  * \return value of the column
  */
  //wxDateTime GetDateTime(const wxString& columnName) const;

  /// Get a column as a boolean using the column index
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \return value of the column
  */
  //bool GetBool(int columnIndex) const;

  /// Get a column as a boolean using the column name
  /**
  * \param columnName name of the column
  * \return value of the column
  */
  //bool GetBool(const wxString& columnName) const;

  /// Check whether the column selected by index is a NULL value
  /**
  * \param columnIndex index of the column. Indices start with 0.
  * \return TRUE if the value is NULL, FALSE otherwise
  */
  bool IsNull(int columnIndex) const;

  /// Check whether the column selected by name is a NULL value
  /**
  * \param columnName name of the column
  * \return TRUE if the value is NULL, FALSE otherwise
  */
  bool IsNull(const wxString& columnName) const;

  /// Set the current row
  /**
  * \param row index of the requested row. Indices start with 0.
  */
  void SetRow(int row);

  /// Finalize the result set
  /**
  */
  void Finalize();

  /// Validate associated SQLite resultset
  /**
  * \return TRUE if SQLite resultset is associated, FALSE otherwise
  */
  //bool IsOk() const;

private:
    /// Check for valid results
    //void CheckResults() const;

    int m_cols;        ///< Number of columns
    int m_rows;        ///< Number of rows
    int m_currentRow;  ///< Index of the current row
    char** m_results;  ///< SQLite3 result buffer
};

#endif // fSQLITE3_H
