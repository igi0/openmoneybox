/***************************************************************
 * Name:      omberr.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef __OMBCONVERT_BIN__
	#include <wx/wx.h>
	#include <wx/filename.h> // For wxFileName

	#include "../types.h"

	// Internal routines
	#ifdef _OMB_FORMAT2x
		bool Check2x(wxString File);
	#endif // _OMB_FORMAT2x
	#ifdef _OMB_FORMAT30x
		bool Check301(wxString File);
		bool Check302(wxString File);
	#endif // _OMB_FORMAT30x
	#ifdef _OMB_FORMAT31x
		bool Check31(wxString file);
	#endif // _OMB_FORMAT31x

	#ifdef __WXMSW__
		// Calls to module igiomb.dll
		#ifdef _OMB_MONOLITHIC
      extern wxString GetShareDir(void);
		#else
      WXIMPORT wxString GetShareDir(void);
    #endif // _OMB_MONOLITHIC
	#endif // __WXMSW__

	#ifdef __WXMAC__
		// Calls to module igiomb.dll
		WXIMPORT wxString GetShareDir(void);
	#endif // __WXMSW__

	#ifdef _OMB_USE_CIPHER
		#ifdef _OMB_SQLCIPHER_v4
			extern void Set_SqlCipher_v4(int Value);
		#endif // _OMB_SQLCIPHER_v4
	#endif // _OMB_USE_CIPHER

	WXEXPORT void Error(int Err, const wxString &Opt);
	#ifdef _OMB_USE_CIPHER
		WXEXPORT int CheckFileFormat(const wxString& File, const wxString& pwd = wxEmptyString, bool archive = false
																								#ifdef _OMB_SQLCIPHER_v4
																									, bool Target_SqlCipherCompatibility_V4 = false
																								#endif // _OMB_SQLCIPHER_v4
																								);
		WXEXPORT bool CheckAndPromptForConversion(const wxString& File, bool master, const wxString& pwd
																								#ifdef _OMB_SQLCIPHER_v4
																									, bool Target_SqlCipherCompatibility_V4
																								#endif // _OMB_SQLCIPHER_v4
																								);
		WXEXPORT bool IsEncryptedDB(wxString File, const wxString& Pwd, wxString OldPwd = wxEmptyString, bool CheckOnly = false);
	#else
		WXEXPORT int CheckFileFormat(const wxString& File);
		WXEXPORT bool CheckAndPromptForConversion(const wxString& File, bool master);
	#endif // _OMB_USE_CIPHER
#endif // __OMBCONVERT_BIN__

