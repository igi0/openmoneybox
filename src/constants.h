/***************************************************************
 * Name:      constants.h
 * Purpose:   Header with constants for OpenMoneyBox
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/datetime.h>

#ifndef OMB_CONSTANTS
	#define OMB_CONSTANTS

	// file format identifiers
	enum ombFileFormat{

		ombFFORMAT_UNKNOWN = 0,

		#ifdef _OMB_FORMAT2x
			bilFFORMAT_2		= 2,	// bilancio 2.0 format
		#endif // _OMB_FORMAT2x
		#ifdef _OMB_FORMAT30x
			bilFFORMAT_301	= 3,	// bilancio 3.0.1 format
			bilFFORMAT_302	= 4,	// bilancio 3.0.2 format
		#endif // _OMB_FORMAT30x
		#ifdef _OMB_FORMAT31x
			ombFFORMAT_31		= 5,	// openmoneybox 3.1 format
		#endif // _OMB_FORMAT31x
		#ifdef _OMB_FORMAT32x
			ombFFORMAT_32		= 6,	// openmoneybox 3.2 format
		#endif // _OMB_FORMAT32x
		#ifdef _OMB_USE_CIPHER
			#ifdef _OMB_FORMAT33x
				ombFFORMAT_33 = 7,	// openmoneybox 3.3 format (encryption support)
				ombFFORMAT_332 = 9,	// openmoneybox 3.3.2 format
			#endif // _OMB_FORMAT33x

			ombWRONGPASSWORD = 8,	// wrong password
		#endif // _OMB_USE_CIPHER


		ombFFORMAT_34 = 10,	// openmoneybox 3.4 format
		ombFFORMAT_35 = 11,	// openmoneybox 3.5 format
};

	// date constants
	const wxDateTime invalidDate = wxDateTime(double(2415020));

	const double ombInvalidLatitude = 91;
	const double ombInvalidLongitude = 181;

	const int _OMB_TOPCATEGORIES_NUMBER = 3;
	const int _OMB_TOPCATEGORIES_OEMICON = 12;

#endif // OMB_CONSTANTS

