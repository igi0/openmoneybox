/***************************************************************
 * Name:      opzio.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OPZIO_CPP_INCLUDED
#define OPZIO_CPP_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/fileconf.h> // For wxFileConfig

#include "opzio.h"

#include "../../types.h"

#ifndef _OMB_USEREGISTRY
  extern wxFileConfig *INI;
#endif // _OMB_USEREGISTRY

GenPanel::GenPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : General( parent, id, pos, size, style ){}

ToolsPanel::ToolsPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : ToolsSheet( parent, id, pos, size, style ){
}

void ToolsPanel::ToolBrowseClick(wxCommandEvent& event){
	wxFileDialog *Dialog=new wxFileDialog(this, _("Select a file"), GetOSDocDir(), wxEmptyString, L"*.*", wxFD_DEFAULT_STYLE,
																					wxDefaultPosition, wxDefaultSize, L"Dialog");
	if(Dialog->ShowModal()==wxID_OK){
		bool Ex=false;
		for(unsigned int i=0;i<ExtList->GetCount();i++){
			if(ExtList->GetString(i)==Dialog->GetFilename()){
				Ex=true;
				break;}}
		if(!Ex){
			ExtList->Append(Dialog->GetPath());
			//Changed=true;
	}}
  delete Dialog;}

void ToolsPanel::ExtListClick(wxCommandEvent& event){
	Remove->Enable(ExtList->GetSelection()>-1);}

void ToolsPanel::RemoveClick(wxCommandEvent& event){
	int OldIndex=ExtList->GetSelection();
	ExtList->Delete(ExtList->GetSelection());
	if(((int)ExtList->GetCount())>=(OldIndex+1))ExtList->SetSelection(OldIndex);
	else if(ExtList->GetCount())ExtList->SetSelection(ExtList->GetCount()-1);
	wxCommandEvent evt(wxEVT_NULL,0);
	ExtListClick(evt);
	//Changed=true;
}

TOptionsF::TOptionsF(wxWindow* parent):OptionsF(parent, -1, wxEmptyString, wxDefaultPosition, wxSize(470,500),
																									wxCAPTION|wxCLOSE_BOX|wxDEFAULT_DIALOG_STYLE){
	// GUI components creation

	// Language Initialization

	// General Page
	GeneralP=new GenPanel(TabPages,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	TabPages->AddPage(GeneralP,_("General"),true,-1);
	// Chart Page
	ChartsP=new Charts(TabPages,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	TabPages->AddPage(ChartsP,_("Charts"),true,-1);

	#ifdef _OMB_INSTALLEDUPDATE
		// Advanced Page
		AdvSheetP=new AdvSheet(TabPages,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
		TabPages->AddPage(AdvSheetP,_("Advanced"),true,-1);
	#endif // _OMB_INSTALLEDUPDATE

	// External Tools Page
	ToolsSheetP=new ToolsPanel(TabPages,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	TabPages->AddPage(ToolsSheetP,_("External tools"),true,-1);

	#ifdef _OMB_USE_CIPHER
		#ifdef _OMB_SQLCIPHER_v4
			// Database page
			DBCompatSheet = new DBCompat(TabPages, -1, wxDefaultPosition, wxSize(500,300), wxTAB_TRAVERSAL);
			TabPages->AddPage(DBCompatSheet,_("Database"), true, -1);
		#endif // _OMB_SQLCIPHER_v4
	#endif // _OMB_USE_CIPHER

	TabPages->SetSelection(0);
	SetTitle(_("Options"));
	//Changed=false;
}

#endif // OPZIO_CPP_INCLUDED
