///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxadvsheet.h"

///////////////////////////////////////////////////////////////////////////

AdvSheet::AdvSheet( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxVERTICAL );

	cb_CheckUpdates = new wxCheckBox( this, wxID_ANY, _("Look for updates at startup"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( cb_CheckUpdates, 0, wxALL, 5 );


	this->SetSizer( bSizer9 );
	this->Layout();
}

AdvSheet::~AdvSheet()
{
}
