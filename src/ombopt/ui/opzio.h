/***************************************************************
 * Name:      opzio.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OpzioH
#define OpzioH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/string.h>

#include "../../types.h"

#include "wxoption.h"

#ifdef _OMB_INSTALLEDUPDATE
	#include "wxadvsheet.h"
#endif // _OMB_INSTALLEDUPDATE

class GenPanel : public General{
public:
	//bool Changed;
	GenPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 460,300 ), long style = wxTAB_TRAVERSAL );
};

class ToolsPanel : public ToolsSheet{
public:
	//bool Changed;
	ToolsPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 460,300 ), long style = wxTAB_TRAVERSAL );
	void ToolBrowseClick(wxCommandEvent& event) override;
	void ExtListClick(wxCommandEvent& event) override;
	void RemoveClick(wxCommandEvent& event) override;
};

class TOptionsF : public /*TForm*/OptionsF
{
//__published:
private:
	// Routines
protected:
public:
	// GUI components
	GenPanel *GeneralP;
	Charts *ChartsP;

	#ifdef _OMB_INSTALLEDUPDATE
		AdvSheet *AdvSheetP;
	#endif // _OMB_INSTALLEDUPDATE

	ToolsPanel *ToolsSheetP;

	#ifdef _OMB_USE_CIPHER
		#ifdef _OMB_SQLCIPHER_v4
			DBCompat *DBCompatSheet;
		#endif // _OMB_SQLCIPHER_v4
	#endif // _OMB_USE_CIPHER

	// Non-GUI components

	// Routines
	explicit TOptionsF(wxWindow* parent);
};

#ifdef _OMB_MONOLITHIC
	extern wxLanguage FindLang(void);
  extern wxString GetOSDocDir(void);
#else
	WXIMPORT wxLanguage FindLang(void);
  WXIMPORT wxString GetOSDocDir(void);
#endif // _OMB_MONOLITHIC

#endif // OpzioH


