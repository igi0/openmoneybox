///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxoption.h"

///////////////////////////////////////////////////////////////////////////

General::General( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* General_;
	General_ = new wxBoxSizer( wxVERTICAL );

	DefDocument = new wxStaticText( this, wxID_ANY, _("Default document"), wxDefaultPosition, wxDefaultSize, 0 );
	DefDocument->Wrap( -1 );
	General_->Add( DefDocument, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	BrowseButton2 = new wxFilePickerCtrl( this, wxID_ANY, wxEmptyString, _("Select a file"), _("*.omb;*.OMB"), wxDefaultPosition, wxDefaultSize, wxFLP_FILE_MUST_EXIST|wxFLP_OPEN|wxFLP_USE_TEXTCTRL );
	General_->Add( BrowseButton2, 0, wxALIGN_RIGHT|wxEXPAND|wxRIGHT|wxLEFT, 5 );

	ShowBar = new wxCheckBox( this, wxID_ANY, _("Show toolbar"), wxDefaultPosition, wxDefaultSize, 0 );
	General_->Add( ShowBar, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	Text = new wxCheckBox( this, wxID_ANY, _("Automatically export XML at the end of the month"), wxDefaultPosition, wxDefaultSize, 0 );
	General_->Add( Text, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	Sys = new wxCheckBox( this, wxID_ANY, _("Activates icon in the system tray"), wxDefaultPosition, wxDefaultSize, 0 );
	General_->Add( Sys, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	Lbl_DocPrefix = new wxStaticText( this, wxID_ANY, _("Prefix for generated documents (XML, backups, etc.):"), wxDefaultPosition, wxDefaultSize, 0 );
	Lbl_DocPrefix->Wrap( -1 );
	General_->Add( Lbl_DocPrefix, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );

	DocPrefix = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifdef __WXGTK__
	if ( !DocPrefix->HasFlag( wxTE_MULTILINE ) )
	{
	DocPrefix->SetMaxLength( 30 );
	}
	#else
	DocPrefix->SetMaxLength( 30 );
	#endif
	General_->Add( DocPrefix, 0, wxBOTTOM|wxRIGHT|wxLEFT|wxEXPAND, 5 );

	GroupFunds = new wxCheckBox( this, wxID_ANY, _("Group funds"), wxDefaultPosition, wxDefaultSize, 0 );
	GroupFunds->Hide();

	General_->Add( GroupFunds, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( General_ );
	this->Layout();
}

General::~General()
{
}

Charts::Charts( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* Charts_;
	Charts_ = new wxBoxSizer( wxVERTICAL );

	FundGraph = new wxCheckBox( this, wxID_ANY, _("Show fund chart"), wxDefaultPosition, wxDefaultSize, 0 );
	Charts_->Add( FundGraph, 0, wxTOP|wxRIGHT|wxLEFT, 5 );

	TrendGraph = new wxCheckBox( this, wxID_ANY, _("Show trend chart"), wxDefaultPosition, wxDefaultSize, 0 );
	Charts_->Add( TrendGraph, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );


	this->SetSizer( Charts_ );
	this->Layout();
}

Charts::~Charts()
{
}

BEGIN_EVENT_TABLE( ToolsSheet, wxPanel )
	EVT_LISTBOX( wxID_ANY, ToolsSheet::_wxFB_ExtListClick )
	EVT_BUTTON( bilAddTool, ToolsSheet::_wxFB_ToolBrowseClick )
	EVT_BUTTON( bilRemTool, ToolsSheet::_wxFB_RemoveClick )
END_EVENT_TABLE()

ToolsSheet::ToolsSheet( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxFlexGridSizer* fgSizer4;
	fgSizer4 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer4->AddGrowableCol( 0 );
	fgSizer4->AddGrowableRow( 0 );
	fgSizer4->SetFlexibleDirection( wxBOTH );
	fgSizer4->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	ExtList = new wxListBox( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	fgSizer4->Add( ExtList, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );

	btn_ToolBrowse = new wxButton( this, bilAddTool, _("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer11->Add( btn_ToolBrowse, 0, wxALL, 5 );

	Remove = new wxButton( this, bilRemTool, _("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	Remove->Enable( false );

	bSizer11->Add( Remove, 0, wxALL, 5 );


	fgSizer4->Add( bSizer11, 0, wxEXPAND, 5 );


	this->SetSizer( fgSizer4 );
	this->Layout();
}

ToolsSheet::~ToolsSheet()
{
}

DBCompat::DBCompat( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxFlexGridSizer* fgSizer3;
	fgSizer3 = new wxFlexGridSizer( 4, 1, 0, 0 );
	fgSizer3->AddGrowableCol( 0 );
	fgSizer3->AddGrowableRow( 1 );
	fgSizer3->AddGrowableRow( 3 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText3 = new wxStaticText( this, wxID_ANY, _("SqlCipher compatibility version:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	bSizer8->Add( m_staticText3, 0, wxALL|wxALIGN_BOTTOM, 5 );

	wxString CompatSelectionChoices[] = { _("v3"), _("v4") };
	int CompatSelectionNChoices = sizeof( CompatSelectionChoices ) / sizeof( wxString );
	CompatSelection = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, CompatSelectionNChoices, CompatSelectionChoices, 0 );
	CompatSelection->SetSelection( 0 );
	bSizer8->Add( CompatSelection, 0, wxALL|wxALIGN_BOTTOM|wxEXPAND, 5 );


	fgSizer3->Add( bSizer8, 1, wxEXPAND, 5 );

	m_collapsiblePane1 = new wxCollapsiblePane( this, wxID_ANY, _("Important information:"), wxDefaultPosition, wxDefaultSize, wxCP_DEFAULT_STYLE );
	m_collapsiblePane1->Collapse( false );

	wxFlexGridSizer* fgSizer6;
	fgSizer6 = new wxFlexGridSizer( 3, 1, 0, 0 );
	fgSizer6->AddGrowableCol( 0 );
	fgSizer6->AddGrowableRow( 0 );
	fgSizer6->AddGrowableRow( 1 );
	fgSizer6->AddGrowableRow( 2 );
	fgSizer6->SetFlexibleDirection( wxBOTH );
	fgSizer6->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText4 = new wxStaticText( m_collapsiblePane1->GetPane(), wxID_ANY, _("This OpenMoneyBox build supports the latest SqlCipher compatibility level."), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( 400 );
	fgSizer6->Add( m_staticText4, 1, wxEXPAND|wxALIGN_BOTTOM|wxTOP|wxRIGHT|wxLEFT, 5 );

	m_staticText5 = new wxStaticText( m_collapsiblePane1->GetPane(), wxID_ANY, _("If 'v4' is selected, a database migration will be attempted next time Openmoneybox is started.\nBefore selecting 'v4', please make sure its compatibility is available on the operating system of all the devices you use OpenMoneyBox with."), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5->Wrap( 400 );
	fgSizer6->Add( m_staticText5, 1, wxRIGHT|wxLEFT|wxEXPAND, 5 );

	m_textCtrl2 = new wxTextCtrl( m_collapsiblePane1->GetPane(), wxID_ANY, _("See https://igisw-bilancio.sourceforge.net/wiki/CipherCompat.pdf for OpenMoneyBox compatibility on available operating systems."), wxDefaultPosition, wxDefaultSize, wxTE_AUTO_URL|wxTE_BESTWRAP|wxTE_MULTILINE|wxTE_READONLY|wxBORDER_NONE );
	fgSizer6->Add( m_textCtrl2, 1, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 5 );


	m_collapsiblePane1->GetPane()->SetSizer( fgSizer6 );
	m_collapsiblePane1->GetPane()->Layout();
	fgSizer6->Fit( m_collapsiblePane1->GetPane() );
	fgSizer3->Add( m_collapsiblePane1, 1, wxALL|wxEXPAND|wxALIGN_RIGHT, 5 );


	this->SetSizer( fgSizer3 );
	this->Layout();
}

DBCompat::~DBCompat()
{
}

BEGIN_EVENT_TABLE( OptionsF, wxDialog )
	EVT_BUTTON( wxID_OK, OptionsF::_wxFB_OKClick )
END_EVENT_TABLE()

OptionsF::OptionsF( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( -1,-1 ), wxDefaultSize );

	wxFlexGridSizer* fgSizer3;
	fgSizer3 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer3->AddGrowableCol( 0 );
	fgSizer3->AddGrowableRow( 0 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	TabPages = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNB_MULTILINE );

	fgSizer3->Add( TabPages, 1, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );

	OK = new wxButton( this, wxID_OK, _("OK"), wxPoint( 142,271 ), wxSize( 75,25 ), 0 );
	bSizer3->Add( OK, 0, wxALL, 5 );

	Cancel = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxSize( 75,25 ), 0 );
	bSizer3->Add( Cancel, 0, wxALL, 5 );


	fgSizer3->Add( bSizer3, 1, wxALIGN_RIGHT, 5 );


	this->SetSizer( fgSizer3 );
	this->Layout();
}

OptionsF::~OptionsF()
{
}
