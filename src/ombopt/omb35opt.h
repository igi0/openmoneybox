/***************************************************************
 * Name:      omb35opt.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMB35OptH
#define OMB35OptH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

//#include <wx/string.h>
#include <wx/fileconf.h> // For wxFileConfig

#ifdef __WXMSW__
	#include "../platformsetup.h"
  #ifdef _OMB_USEREGISTRY
    #include <wx/msw/registry.h> // For wxRegKey
  #endif // _OMB_USEREGISTRY
#endif

#include "../types.h"
//#include "constants.h"

class TOpt
{
//private:
public:
	bool SuccessfulCreated;     // true if Option are properly created
	#ifdef __WXMSW__
		// HKLM Options
		wxString SWKey;
		wxString InstallationPath;    // Program Folder
	#endif
	#ifdef __WXMAC__
		wxString InstallationPath;    // Program Folder
	#endif	// __WXMAC__
	//bool InstConv;              // true if Conv utility is installed	// DON'T REMOVE - May be restore with new Product version
	//long InstWiz;     				  // true if Wizard utility is installed
	//long InstTray;              // true if OmbTray utility is installed
	//long InstUpdate;						// true if Update utility is installed
	// HKCU Options
	long FirstRunEver;          // Very first launch of application;
	//bool FirstRunCurrVersion;   // First lauch of new version;
                              // Preparation for future updates
	//long GAutoOpen;             // Automatic opening of last used file
	//wxString GLastFile;           // Latest opened document
	wxString	GDoc_folder,			// Document default folder
						GDDoc,						// Default document
						GMaster_db,				// Master database
						GDoc_prefix;			// Prefix for self generated documents (XML's and related folders, backups, etc.)
	long GBar;                  // Toolbar display
	long GConv;                 // Automatic report export in text on new months
	long GIcon;                 // Open TrayIcon on exit

	long GGroupFunds;						// Group funds

	long CTrendGraph;           // Trend Graph display
	long CFundGraph;						// Fund Pie display
	#ifdef _OMB_INSTALLEDUPDATE
		long CheckUpdates;					// Verify new version availability in the web
	#endif // _OMB_INSTALLEDUPDATE
	wxArrayString *Tools;

	#ifdef _OMB_USE_CIPHER
		#ifdef _OMB_SQLCIPHER_v4
			int SqlCipherCompatVersion;	// SqlCipher compatibility level
		#endif // _OMB_SQLCIPHER_v4
	#endif // _OMB_USE_CIPHER

	TOpt(void);

	//void LastFile(wxString Name);
	bool SetDefaultDoc(wxString Doc);
	void SetCurrSymbol(void);
	void FirstRunDone(void);

	#ifdef _OMB_USE_CIPHER
		#ifdef _OMB_SQLCIPHER_v4
			bool SetSqlCipher_v4(int Value);
		#endif // _OMB_SQLCIPHER_v4
	#endif // _OMB_USE_CIPHER

	~TOpt();
};

#ifdef __WXMSW__
  // Calls to module igiomb
  WXIMPORT TVersion GetInstalledVersion(void);
  WXIMPORT TVersion GetLastRunVersion(void);
  // Calls to module omberr.DLL
  WXIMPORT void Error(int Err, wxString Opt);
#endif

#ifdef _OMB_MONOLITHIC
  extern wxString GetOSDocDir(void);
#else
  WXIMPORT wxString GetOSDocDir(void);
#endif // _OMB_MONOLITHIC
WXIMPORT wxString GetUserLocalDir(void);

#ifdef __WXMAC__
	WXIMPORT wxString GetUserConfigDir(void);
#endif	// __WXMAC__

#endif // OMB35OptH


