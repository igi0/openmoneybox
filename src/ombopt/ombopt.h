/***************************************************************
 * Name:      ombopt.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef BILOPT_HEADER_INCLUDED
#define BILOPT_HEADER_INCLUDED

#include "../types.h"

  #include "../platformsetup.h"
#ifdef __WXMSW__
  #include "../platformsetup.h"
#endif // __WXMSW__

/*
#ifdef __WXMSW__
  int wxEntry(int& argc, wxChar **argv);
  //BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fwdreason, LPVOID lpvReserved);
#endif
*/

// Imported functions
#ifdef _OMB_MONOLITHIC
  extern wxString GetOSDocDir(void);
#else
  WXIMPORT wxString GetOSDocDir(void);
#endif // _OMB_MONOLITHIC
// ------------------------------------

#ifndef _OMB_PORTABLE_MSW
  WXEXPORT void CreateTrayAutostart(void);
#endif // _OMB_PORTABLE_MSW

#ifdef __WXMSW__
  #ifndef _OMB_MONOLITHIC
  	WXEXPORT
  #endif // _OMB_MONOLITHIC
      wxString GetInstallationPath(void);
#endif	// __WXMSW__

WXEXPORT bool ShowBar(void);
WXEXPORT bool IsFirstRunEver(void);
WXEXPORT wxString GetDocPath(void);
WXEXPORT void Set_DefaultDocument(const wxString& Document);
WXEXPORT void DoFirstRun(void);
//WXEXPORT wxString GetLogString(int I);
WXEXPORT bool ShowOptionsDialog(
																	#ifdef _OMB_USE_CIPHER
																		#ifdef _OMB_SQLCIPHER_v4
																			bool IsOpenDB_v4 = false
																		#else
																			void
																		#endif // _OMB_SQLCIPHER_v4
																	#endif // _OMB_USE_CIPHER
																	);
WXEXPORT bool ShowTrendChart(void);
WXEXPORT bool ShowFundChart(void);
WXEXPORT bool TrayActive(void);
WXEXPORT bool AutoConvert(void);
#ifdef _OMB_INSTALLEDUPDATE
	WXEXPORT bool GetCheckUpdates(void);
#endif // _OMB_INSTALLEDUPDATE
WXEXPORT int GetToolCount(void);
WXEXPORT wxString GetTool(int i);
#ifdef _OMB_USEREGISTRY
  WXEXPORT wxString GetSWKey(void);
#endif // _OMB_USEREGISTRY

WXEXPORT wxString GetDocPrefix(void);

#ifdef _OMB_USE_CIPHER
	WXEXPORT bool SetKey(const wxString &Key, bool Archive);
	WXEXPORT wxString GetKey(bool Archive = false);
#endif // _OMB_USE_CIPHER

#ifndef _OMB_MONOLITHIC
	WXEXPORT
#else
		wxString GetDefaultDocument(void);
#endif // _OMB_MONOLITHIC

	wxLanguage FindLang(void);

	bool ShowFundGroups(void);
#ifdef _OMB_USE_CIPHER
	#ifdef _OMB_SQLCIPHER_v4
		int GetTargetSqlCipherCompatibility(void);
		void Set_SqlCipher_v4(int Value);
	#endif // _OMB_SQLCIPHER_v4
#endif // _OMB_USE_CIPHER

// Imported calls from igiomb module
extern wxString GetShareDir(void);

#endif // BILOPT_HEADER_INCLUDED
