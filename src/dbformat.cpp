/***************************************************************
 * Name:      platformsetup.cpp
 * Purpose:   OpenMoneyBox sqlite db structure
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef DBFORMAT_CPP_INCLUDED
#define DBFORMAT_CPP_INCLUDED

	#include <wx/string.h>

	// Main db definition

	// Metadata table create statement
	wxString cs_information = L"create TABLE Information(" \
																						"id INTEGER PRIMARY KEY," \
																						"data TEXT);";

	// Funds table create statement
	wxString cs_funds = L"create TABLE Funds(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"value REAL);";

	// Credits table create statement
	wxString cs_credits = L"CREATE TABLE Credits(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"value REAL," \
																						"contact_index INTEGER);";

	// Debts table create statement
	wxString cs_debts = L"CREATE TABLE Debts(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"value REAL," \
																						"contact_index INTEGER);";

	// Loans table create statement
	wxString cs_loans = L"CREATE TABLE Loans(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"item TEXT," \
																						"alarm INTEGER," \
																						"contact_index INTEGER);";

	// Borrows table create statement
	wxString cs_borrows = L"CREATE TABLE Borrows(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"item TEXT," \
																						"alarm INTEGER," \
																						"contact_index INTEGER);";

	// Shopping list table create statement
	wxString cs_shoplist = L"CREATE TABLE Shoplist(" \
																						"id INTEGER PRIMARY KEY," \
																						"item TEXT," \
																						"alarm INTEGER);";

	// Transactions list table create statement
	wxString cs_transactions = L"create TABLE Transactions(" \
																						"id INTEGER PRIMARY KEY," \
																						"isdate INTEGER," \
																						"date INTEGER," \
																						"time INTEGER," \
																						"type INTEGER," \
																						"value TEXT," \
																						"reason TEXT," \
																						"cat_index INTEGER," \
																						"contact_index INTEGER," \
																						"latitude REAL," \
																						"longitude REAL," \

																						"currencyid INTEGER," \
																						"currencyrate REAL," \
																						"currencysymb TEXT);";

	// Categories list table create statement
	wxString cs_categories = L"create TABLE Categories(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"active INTEGER,"	\
																						"iconid INTEGER);";

	/*
	// Contact list table create statement
	wxString cs_contacts = "create TABLE Contacts(" \
																						"id INTEGER PRIMARY KEY," \
																						"mobile_id INTEGER," \
																						"contact TEXT," \
																						"status INTEGER);";
	*/

	// Fund Groups table create statement
	wxString cs_fundgroups = L"create TABLE FundGroups(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"owner INTEGER," \
																						"child INTEGER);";

	// Custom icons table create statement
	wxString cs_customicons = L"create TABLE CustomIcons(" \
																						"id INTEGER PRIMARY KEY," \
																						"image TEXT);";

	// Funds table create statement (master)
	wxString cs_funds_master = L"create TABLE Funds%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"value REAL);";

	// Credits table create statement (master)
	wxString cs_credits_master = L"CREATE TABLE Credits%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"value REAL," \
																						"contact_index INTEGER);";

	// Debts table create statement (master)
	wxString cs_debts_master = L"CREATE TABLE Debts%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"value REAL," \
																						"contact_index INTEGER);";

	// Loans table create statement (master)
	wxString cs_loans_master = L"CREATE TABLE Loans%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"item TEXT," \
																						"alarm INTEGER," \
																						"contact_index INTEGER);";

	// Borrows table create statement (master)
	wxString cs_borrows_master = L"CREATE TABLE Borrows%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"item TEXT," \
																						"alarm INTEGER," \
																						"contact_index INTEGER);";

	// Categories list table create statement (master)
	wxString cs_categories_master = L"CREATE TABLE Categories%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"active INTEGER,"	\
																						"iconid INTEGER);";

	// Fund groups table create statement (master)
	wxString cs_fundgroups_master = L"create TABLE FundGroups%s(" \
																						"id INTEGER PRIMARY KEY," \
																						"name TEXT," \
																						"owner INTEGER," \
																						"child INTEGER);";

#endif // DBFORMAT_CPP_INCLUDED
