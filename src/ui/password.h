/***************************************************************
 * Name:      password.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/fileconf.h>

#ifndef PasswordH
#define PasswordH

#include "wxpassword.h"

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class TPassF : public wxTPassF
{
protected:
	void OKClick(wxCommandEvent& event) override;
public:
	// Routines
	explicit TPassF(wxWindow *parent);
	void ResizeF(void);
};

#ifdef _OMB_MONOLITHIC
  extern void Error(int Err, const wxString &Opt);
#else
  WXIMPORT void Error(int Err, const wxString &Opt);
#endif // _OMB_MONOLITHIC

#endif // PasswordH

