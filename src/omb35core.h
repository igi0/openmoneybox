/***************************************************************
 * Name:      omb35core.h
 * Purpose:   Core Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMB35CORE_HEADER_INCLUDED
#define OMB35CORE_HEADER_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#ifndef __OMBCONVERT_BIN__
	#include <wx/gauge.h>
	#include <wx/listctrl.h>
#endif

#ifdef _OMB_USE_CIPHER
	#include <sqlite3.h>
	#include "wxsqlite3.h" // NOTE (igor#1#): Hidden feature
#else
	#include <wx/wxsqlite3.h>
#endif // _OMB_USE_CIPHER

#include "constants.h"

// Product Capabilities
const int MAX_FUNDS     	=   100;
const int MAX_CREDITS   	=   100;
const int MAX_DEBTS     	=   100;
const int MAX_LENT      	=  1000;
const int MAX_BORROWED  	=  1000;
const int MAX_SHOPS     	=  1000;
const int MAX_LINES     	= 10000;
const int MAX_CATEGORIES	=   100;
//______________________________

typedef enum TTypeVal{tvFou=1,tvCre,tvDeb}TypeVal;
typedef struct TVal{
	int Id;
	wxString Name;
	double Value;	long ContactIndex;

	/* TODO -cImprovement : To be implemented
	TDate CreationDate;
	TDate DeletionDate;
	*/
}Val;

typedef enum TObjType{toPre=1,toInP}ObjType;
typedef struct TObj{
	int Id;
	wxString Name, Object;
	wxDateTime Alarm;
	long ContactIndex;

	/* TODO -cImprovement : To be implemented
	wxDateTime CreationDate;
	wxDateTime DeletionDate;
	*/
}Obj;

typedef struct TShopItem{
	int Id;
	wxString Name;
	wxDateTime Alarm;}ShopItem;

typedef enum TOpType{
  toNULL=0,
  toGain=1,toExpe,
  toSetCre,toRemCre,toConCre,
  toSetDeb,toRemDeb,toConDeb,
  toGetObj,toGivObj,
  toLenObj,toBakObj,
  toBorObj,toRetObj}OpType;

typedef struct TLine{
	int Id;
	bool IsDate;
	wxDateTime Date,Time;
	TOpType Type;
	wxString Value,
	// FIXME (igor#1#):	Known limitation: Value is defined as WxString also in case it contains numeric data. ...
	//									So it is saved as a locale-dependent string.
	Reason;
	long CategoryIndex;
	#ifdef __OMBCONVERT_BIN__
		wxString Remark0301;	// "Bil3.0"
	#endif // __OMBCONVERT_BIN__

	long ContactIndex;
	double Latitude;
	double Longitude;

	long CurrencyIndex;
	double CurrencyRate;
	wxString CurrencySymbol;
}Line;

typedef struct TCategory{
	int Id;
	wxString Name;
	bool test;	// used during category update

	int IconIndex;
	}Category;

// Database information
enum dbMetaDataEntry{
	dbMeta_application_info = 1,
	dbMeta_default_fund,
	dbMeta_mobile_export,
	dbMeta_currency,
	dbMeta_attached_db	// NOTE (igor#1#): Hidden feature
};

// Column identifiers in the database, 'Transactions' table
enum dbTransactionColId{
	ombTColIdId = 0,
	ombTColIdIsDate,
	ombTColIdDate,
	ombTColIdTime,
	ombTColIdType,	// = 4,
	ombTColIdValue,
	ombTColIdReason,
	ombTColIdCategoryIndex,
	ombTColIdContactIndex, //= 8,
	ombTColIdLatitude,
	ombTColIdLongitude,

	ombTColIdCurrencyId,
	ombTColIdCurrencyRate,
	ombTColIdCurrencySymbol,
};

class TData
{
	friend class ombMainFrame;
	private:
		// Non-GUI components

		double Tot_Credits,Tot_Debts;

		#ifdef _OMB_USE_CIPHER
			bool	IsEncrypted,
						IsEncrypted_master;
			bool PasswordPrompt(sqlite3 *db, bool Archive);
		#endif // _OMB_USE_CIPHER

		wxString SoundFile;

		// Routines
		void Initialize(bool Creating);
		bool AddDate(int id, wxDateTime D, double T);
		// void CleanMaster(void);
		bool Prepare_MasterDB(const wxString &dbPath, const wxString &trailname);
		void Archive_inMaster(const wxString& master, const wxString &trailname, bool archive_categories);
	protected:
		// Non-GUI components
		double Tot_Funds;

		// Routines
		#ifndef __OMBCONVERT_BIN__
			void UpdateProgress(int i);
		#endif // __OMBCONVERT_BIN__

		wxString ombFromCDouble(double value);

	public:
		// Non-GUI components

		#ifdef _OMB_USE_CIPHER
			sqlite3 *database;
			sqlite3 *AttachedDB;	// NOTE (igor#1#): Hidden feature
		#else
			wxSQLite3Database *database;
			wxSQLite3Database *AttachedDB;	// NOTE (igor#1#): Hidden feature
		#endif // _OMB_USE_CIPHER

		bool Parsing; // Set to true during file parsing
		bool ExternalFileAttached;	// NOTE (igor#1#): Hidden feature

		#ifdef _OMB_USE_CIPHER
			#ifdef _OMB_SQLCIPHER_v4
				bool DatabaseIsSqlCipher_V4;	// Document SqlCipher Compatibility is V4
			#endif // _OMB_SQLCIPHER_v4
		#endif // _OMB_USE_CIPHER

		wxDateTime Day;
		wxDateTime Hour;
		int NFun, NCre, NDeb, NLen, NBor, NSho, NLin, NCat;
		struct ACC{
			bool	Modified,
						ReadOnly;
			int Year;
			wxDateTime DateStamp;
			long Month;
			wxString	FileName,
								FileView,
								DefFund;}FileData;
		TVal *Funds, *Credits, *Debts;
		TObj *Lent, *Borrowed;
		TShopItem *ShopItems;
		TLine *Lines;
		TCategory *Categories;
		wxArrayString *MattersBuffer;

		#ifdef __OMBCONVERT_BIN__
			wxArrayString *CategoryDB;
			TData(void);
		#else
			wxGauge *Progress;
			explicit TData(wxGauge *ProgressBar);
		#endif // __OMBCONVERT_BIN__
		~TData(void);

		bool UpdateMatters(TOpType T);

		#if defined (__OMBCONVERT_BIN__) && defined (_OMB_USE_CIPHER)
			bool OpenDatabase(const wxString &File, wxString pwd);
		#else
			bool OpenDatabase(const wxString &File);
		#endif // defined

		void ParseDatabase(void);
		bool IsDate(int R);
		bool AddValue(TTypeVal T, int id, wxString N, double V, long c_index = -1);
		bool AddOper(int id, wxDateTime D, wxDateTime O, TOpType T, wxString V, wxString M, long N, long c_index,
									bool hasLocation = false, double lat = ombInvalidLatitude, double lon = ombInvalidLongitude, long Curr_id = -1, double Curr_rate = 1, const wxString& Curr_Symb = wxEmptyString);
		bool AddObject(TObjType T, int id, wxString N, wxString O, wxDateTime D, long c_index);
		bool ChangeFundValue(TTypeVal type, int id, double V);
		bool FindContact(long contact_id);
		#ifndef __OMBCONVERT_BIN__
			void ModPass(void);
			void XMLExport(const wxString &F1, const wxString &F2);
			void XMLExport_archive(const wxString &F1, const wxString &F2, wxDateTime date);
			bool AttachMaster(void);
			void UpdateCategories(wxListCtrl *lbox);
		#endif // __OMBCONVERT_BIN__
		void ChangeTransactionCategory(int id, int new_index);

		void SetDefaultFund(const wxString& def);
		void SetDefaultFundValue(double value);
		void AddCategory(int id, const wxString& name, int IconIndex);
		bool AddShopItem(int id, wxString N, wxDateTime A);
		double GetTot(TTypeVal T);
		bool DelValue(TTypeVal T,int I);
		bool DelShopItem(int I);
		bool DelObject(TObjType T,int I);
		wxString GenerateTrailName(const wxString &dbPath);
		void Import_inMaster(const wxString &master, const wxString &trailname);

		bool HasAlarms(void);
		void CheckShopList(void);
		void CheckLentObjects(void);
		void CheckBorrowedObjects(void);

	#ifdef _OMB_USE_CIPHER
		wxSQLite3Table GetExternalFundTable(void); // NOTE (igor#1#): Hidden feature
	#endif // _OMB_USE_CIPHER
};

/*
#ifndef __OMBCONVERT_BIN__
	WXIMPORT bool CheckAndPromptForConversion(wxString File, bool master);
#endif // __OMBCONVERT_BIN__
*/

#ifndef __OMBCONVERT_BIN__

  #ifdef _OMB_MONOLITHIC
    extern wxString FormDigits(double Val);
    extern wxString DoubleQuote(wxString S);
    extern bool AutoConvert(void);
    extern wxString GetDocPrefix(void);
    extern wxString Get_MasterDB(void);
    extern void Error(int Err, const wxString &Opt);
    extern wxString GetBilDocPath(void);
    extern wxString IntToMonth(int M);
    extern wxString SubstSpecialChars(wxString S);
    extern wxString GetOSDocDir(void);
    extern wxString GetDataDir(void);
    extern wxString GetLogString(int I);
  #else
    WXIMPORT wxString FormDigits(double Val);
    WXIMPORT wxString DoubleQuote(wxString S);
    WXIMPORT bool AutoConvert(void);
    WXIMPORT wxString GetDocPrefix(void);
    WXIMPORT wxString Get_MasterDB(void);
    WXIMPORT void Error(int Err, const wxString &Opt);
    WXIMPORT wxString GetBilDocPath(void);
    WXIMPORT wxString IntToMonth(int M);
    WXIMPORT wxString SubstSpecialChars(wxString S);
    WXIMPORT wxString GetOSDocDir(void);
    WXIMPORT wxString GetDataDir(void);
    WXIMPORT wxString GetLogString(int I);
 #endif // _OMB_MONOLITHIC

	#ifdef _OMB_USE_CIPHER
		WXIMPORT bool CheckAndPromptForConversion(const wxString& File, bool master, const wxString& pwd = wxEmptyString
																								#ifdef _OMB_SQLCIPHER_v4
																									, bool Target_SqlCipherCompatibility_V4 = false
																								#endif // _OMB_SQLCIPHER_v4
																								);
		#ifdef _OMB_SQLCIPHER_v4
			WXIMPORT int GetTargetSqlCipherCompatibility(void);
		#endif // _OMB_SQLCIPHER_v4
	#else
    #ifdef _OMB_MONOLITHIC
      extern bool CheckAndPromptForConversion(const wxString& File, bool master);
    #else
      WXIMPORT bool CheckAndPromptForConversion(const wxString& File, bool master);
    #endif // _OMB_MONOLITHIC
	#endif // _OMB_USE_CIPHER

	#ifdef _OMB_USE_CIPHER
		WXIMPORT int CheckFileFormat(wxString File, wxString pwd = wxEmptyString, bool archive = false);
	#else
		WXIMPORT int CheckFileFormat(wxString File);
	#endif // _OMB_USE_CIPHER

	#ifdef __WXMSW__
		#ifdef _OMB_MONOLITHIC
			extern wxLanguage FindLang(void);
      extern wxString GetInstallationPath(void);
      #ifndef NDEBUG
        extern void ombLogMessage(const wxString& Text);
      #endif // NDEBUG
    #else
			WXIMPORT wxLanguage FindLang(void);
      WXIMPORT wxString GetInstallationPath(void);
    #endif // _OMB_MONOLITHIC
	#endif //__WXMSW__

#else
	extern wxString FormDigits(double Val);
	extern wxString DoubleQuote(wxString S);
	#ifdef _OMB_USE_CIPHER
		extern int CheckFileFormat(wxString File, wxString pwd = wxEmptyString, bool archive = false);
	#else
		extern int CheckFileFormat(wxString File);
	#endif // _OMB_USE_CIPHER
	extern void Error(int Err, const wxString &Opt);
#endif // __OMBCONVERT_BIN__

#ifdef _OMB_MONOLITHIC
  extern wxString GetCurrencySymbol(void);
#else
  WXIMPORT wxString GetCurrencySymbol(void);
#endif // _OMB_MONOLITHIC

#ifdef _OMB_USE_CIPHER
	WXIMPORT bool SetKey(const wxString &Key, bool Archive = false);
	WXIMPORT wxString GetKey(bool Archive = false);
	WXIMPORT bool IsEncryptedDB(wxString File, const wxString& Pwd, wxString OldPwd = wxEmptyString, bool CheckOnly = false);
#endif // _OMB_USE_CIPHER

#ifdef __WXMAC__
	extern wxString AppDir;
  WXIMPORT wxLanguage FindLang(void);
#endif // __WXMAC__

#endif	// OMB35CORE_HEADER_INCLUDED
