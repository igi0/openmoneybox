/***************************************************************
 * Name:      igiomb.cpp
 * Purpose:   Code for Library Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-05-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License: GNU
 **************************************************************/

#ifndef IGIOMB_CPP_INCLUDED
	#define IGIOMB_CPP_INCLUDED

		#ifndef WX_PRECOMP
			#include <wx/wx.h>
		#else
			#include <wx/wxprec.h>
		#endif

	#ifndef __OMBCONVERT_BIN__
		#include <wx/fileconf.h> // For wxFileConfig
		#include <wx/string.h>
		#include <wx/filename.h>
		//#include <wx/dlimpexp.h>
		#include <wx/stdpaths.h> // For wxStandardPathsBase
	#endif // __OMBCONVERT_BIN__

	#include "../platformsetup.h"

	extern wxString ShareDir;

	#ifndef __OMBCONVERT_BIN__
		#ifndef __WXMSW__
			#include <wx/intl.h>  // For wxLocale
		#endif

		#include <wx/datetime.h>	// For wxDateTime_t
		/*	// DON'T REMOVE - May be restored with new Product version
		#include <wx/utils.h>	// For wxSleep
		#include <wx/gdicmn.h>	// For wxSetCursor
		*/

		#include "igiomb.h"

	/*
	#ifndef _OMB_USE_GSETTINGS
		extern wxFileConfig *INI;
	#endif // _OMB_USE_GSETTINGS
	*/
	extern wxString DataDir;

	#ifdef _OMB_INSTALLEDUPDATE
		extern int MajorVersion;
		extern int MinorVersion;
		extern int ReleaseVersion;
		extern char __BUILD_NUMBER;
		extern char   __BUILD_DATE;
		extern wxString VersionFile;
		extern wxString ShortVersion;
	#endif // _OMB_INSTALLEDUPDATE
	#endif // __OMBCONVERT_BIN__

	#if defined (__WXMAC__) && defined (__OMBCONVERT_BIN__)
		#include <locale.h>  // For wxLocale
	#endif

	#if defined (__FREEBSD__) && defined (__OMBCONVERT_BIN__)
		#include <locale.h>  // For wxLocale
	#endif

	wxString CheckValue(const wxString& Val){
		// NOTE (igor#1#): Do not remove: this function returns an error if the wrong decimal separator is entered
		double p=0;
		wxString Str=Val;
		if(Str.ToDouble(&p))return Str;
		return wxEmptyString;}

	wxString FormDigits(double Val){
		wxString Ret;
		return Ret.Format(L"%#.2hf",Val);}

	wxString IntToMonth(int M){
		// M: 1-based month

		if((M > 0) && (M < 13)){
			wxString month_name = wxDateTime::GetMonthName((wxDateTime::Month) (M - 1));
			return month_name.Capitalize();
		}
		else return wxEmptyString;
	}

	#if defined (bilFFORMAT_301) || defined (bilFFORMAT_302) || defined (_OMB_INSTALLEDUPDATE)
		#if (wxCHECK_VERSION(3, 2, 0) )
			WXEXPORT wxDateTime Omb_StrToDate(wxString S){
		#else
			wxDateTime Omb_StrToDate(wxString S){
		#endif
			wxDateTime date;
			#ifdef __WXMSW__
				date.ParseFormat(S,"%d/%m/%Y",wxDateTime::Today());
			#else
				date.ParseFormat(S, L"%d/%m/%Y", wxDateTime::Today());
			#endif
			if(date.IsValid()){
				int Y = date.GetYear();
				if(Y < 10) date.SetYear(Y + 2000);	// workaround for document with single digit year
				return date;}
			return wxInvalidDateTime;}
	#endif // defined

	#ifdef _OMB_INSTALLEDUPDATE
		TVersion GetInstalledVersion(void){
			// Version calculation
			TVersion Version;
			Version.Maj = MajorVersion;
			Version.Min = MinorVersion;
			Version.Rel = ReleaseVersion;
			#if (defined __amd64__ ) && (defined __WXMSW__)
                Version.Bui = (unsigned long long) &__BUILD_NUMBER;
            #else
                Version.Bui = (unsigned long) &__BUILD_NUMBER;
            #endif // __amd64__
			return Version;
		}
	#endif // _OMB_INSTALLEDUPDATE

	/* TODO -cEnhancement : Preparation for future enhancement */
	/*
	#ifdef __WXMSW__
	TVersion GetLastRunVersion(void){	// TODO (igor#1#): Rework using ProductVersions.h ???
		// Version calculation
		TVersion lrVersion;
		TVersion instVersion=GetInstalledVersion();
		#ifdef _OMB_USEREGISTRY
			//wxString Key=_T("HKEY_LOCAL_MACHINE\\")+SWKey();
			wxString SWKey="SOFTWARE\\igiSW\\OpenMoneyBox\\3.1";
			//wxRegKey *K=new wxRegKey(Key);
			wxRegKey *K=new wxRegKey(wxRegKey::HKLM,SWKey);
			if(!K->QueryValue(_T("MajVer_lr"),&lrVersion.Maj)){
				lrVersion.Maj=instVersion.Maj;
				K->SetValue(_T("MajVer_lr"),lrVersion.Maj);}
			if(!K->QueryValue(_T("MinVer_lr"),&lrVersion.Min)){
				lrVersion.Min=instVersion.Min;
				K->SetValue(_T("MinVer_lr"),lrVersion.Min);}
			if(!K->QueryValue(_T("RelVer_lr"),&lrVersion.Rel)){
				lrVersion.Rel=instVersion.Rel;
				K->SetValue(_T("RelVer_lr"),lrVersion.Rel);}
			if(!K->QueryValue(_T("BuiVer_lr"),&lrVersion.Bui)){
				lrVersion.Bui=instVersion.Bui;
				K->SetValue(_T("BuiVer_lr"),lrVersion.Bui);}
			delete K;
		#endif // _OMB_USEREGISTRY
		#ifdef __WXGTK__
			INI->SetPath(L"/Version lr");
			if(INI->HasEntry(L"MajVer_lr"))INI->Read(L"MajVer_lr",lrVersion.Maj);
			else{
				INI->Write(L"MajVer_lr",instVersion.Maj);
				lrVersion.Maj=instVersion.Maj;}
			if(INI->HasEntry(L"MinVer_lr"))INI->Read(L"MinVer_lr",lrVersion.Min);
			else{
				INI->Write(L"MinVer_lr",instVersion.Min);
				lrVersion.Min=instVersion.Min;}
			if(INI->HasEntry(L"RelVer_lr"))INI->Read(L"RelVer_lr",lrVersion.Rel);
			else{
				INI->Write(L"RelVer_lr",instVersion.Rel);
				lrVersion.Rel=instVersion.Rel;}
			if(INI->HasEntry(L"BuiVer_lr"))INI->Read(L"BuiVer_lr",lrVersion.Bui);
			else{
				INI->Write(L"BuiVer_lr",instVersion.Bui);
				lrVersion.Bui=instVersion.Bui;}
			INI->Flush();
			//delete Ver;
		#endif // __WXGTK__
		return lrVersion;}
	#endif // __WXMSW__
	*/

	/*
	#ifdef _OMB_INSTALLEDUPDATE
		void CheckUpdate(void){
			//UPDPROC Proc;
			#ifdef __WXMSW__
				wxDynamicLibrary Proc("OmbUpdate.DLL");
			#else
				wxDynamicLibrary Proc("./libombupdate.so");

				wxString name=Proc.CanonicalizeName("ombupdate");

			#endif // __WXMSW__
			if(Proc.IsLoaded()){
				typedef void (*CheckUpdate_func)(void);
				wxDYNLIB_FUNCTION(CheckUpdate_func,fnCheckUpdate,Proc);
				//if(pfnCheckUpdate)pfnCheckUpdate();
				if(pfnfnCheckUpdate)pfnfnCheckUpdate();
				Proc.Detach();
				//delete Proc;
			}
		}
	#endif // _OMB_INSTALLEDUPDATE
	*/

	wxString SubstSpecialChars(wxString S){
			// source: http://www.mrwebmaster.it/xml/xml-lettere-accentate-perche-errore_8390.html
		#ifdef __WXMSW__
			S.Replace("&", "&amp;", true);
			S.Replace("à", "&#224;", true);
			S.Replace("á", "&#225;", true);
			S.Replace("è", "&#232;", true);
			S.Replace("é", "&#233;", true);
			S.Replace("ì", "&#236;", true);
			S.Replace("í", "&#237;", true);
			S.Replace("ò", "&#242;", true);
			S.Replace("ó", "&#243;", true);
			S.Replace("ù", "&#249;", true);
			S.Replace("ú", "&#250;", true);
			S.Replace("Ì", "&#204;", true);
			S.Replace("[OMB_ESCAPEQUOTES]", "&quot;", true);
	 #else
			S.Replace(L"&", L"&amp;", true);
			S.Replace(L"à", L"&#224;", true);
			S.Replace(L"á", L"&#225;", true);
			S.Replace(L"è", L"&#232;", true);
			S.Replace(L"é", L"&#233;", true);
			S.Replace(L"ì", L"&#236;", true);
			S.Replace(L"í", L"&#237;", true);
			S.Replace(L"ò", L"&#242;", true);
			S.Replace(L"ó", L"&#243;", true);
			S.Replace(L"ù", L"&#249;", true);
			S.Replace(L"ú", L"&#250;", true);
			S.Replace(L"Ì", L"&#204;", true);
			S.Replace(L"[OMB_ESCAPEQUOTES]", L"&quot;", true);
		#endif // __WXMSW__
		return S;}

		wxString GetShareDir(void){
			return ShareDir;}

	#ifndef __OMBCONVERT_BIN__
		wxString GetDataDir(void){
			return DataDir;}

		wxString GetOSDocDir(void){
		  #ifdef _OMB_PORTABLE_MSW
        wxFileName dir(wxGetCwd());
        for(int i = 0; i < 3; i++)
          dir.RemoveLastDir();
        wxString testFolder = dir.GetPath() + L"\\Documents";
        if((! testFolder.IsEmpty()) && (wxDirExists(testFolder))) return testFolder;
        else {
      #endif // _OMB_PORTABLE_MSW
			wxStandardPathsBase& Paths=::wxStandardPaths::Get();
/*
			// NOTE (igor#1#): Useless on GTK. ...
			//Commented out as hit from Flawfinder
			wxString Doc=Paths.GetDocumentsDir();

			#ifdef __WXGTK__
				FILE* pipe = popen("echo -n $(xdg-user-dir DOCUMENTS)", "r");
				if (!pipe) return Doc;
				char buffer[128];
				std::string result = "";
				while(!feof(pipe)) {
					if(fgets(buffer, 128, pipe) != NULL)
						result += buffer;
				}
				pclose(pipe);
				wxString result_string = result;
				if(result_string.IsEmpty()) return Doc;
				else return result_string;
			#else
				return Doc;
			#endif // __WXGTK__
*/
		return Paths.GetDocumentsDir();
    #ifdef _OMB_PORTABLE_MSW
      }
    #endif // _OMB_PORTABLE_MSW
}
	#endif // __OMBCONVERT_BIN__

	wxString GetCurrencySymbol(void){
		setlocale (LC_MONETARY, "");
		struct lconv * lc;
		lc = localeconv();
		return lc->currency_symbol;
	}

	#ifdef _OMB_INSTALLEDUPDATE
		wxString GetVersionFile(void){
			return VersionFile;}
	#endif // _OMB_INSTALLEDUPDATE

	wxString DoubleQuote(wxString S){
		if(S.Find('\'') != wxNOT_FOUND) S.Replace(L"'", L"''", true);
		return S;}

	#ifndef __OMBCONVERT_BIN__
		#ifdef __WXGTK__
			wxString GetUserConfigDir(void){
				wxString usr_config_dir;
				wxGetEnv(L"XDG_CONFIG_HOME",&usr_config_dir);	// https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
				if(usr_config_dir.IsEmpty()) usr_config_dir = wxGetHomeDir() + L"/.config";
				usr_config_dir += L"/openmoneybox";
				if(! wxDirExists(usr_config_dir))
					wxFileName::Mkdir(usr_config_dir, 0777, wxPATH_MKDIR_FULL);
				return usr_config_dir;
			}
		#endif // __WXGTK__
		#ifdef __WXMAC__
			wxString GetUserConfigDir(void){
				wxStandardPathsBase& Paths=::wxStandardPaths::Get();
				wxString usr_config_dir = Paths.GetUserConfigDir() + L"/openmoneybox";
				if(! wxDirExists(usr_config_dir))
					wxFileName::Mkdir(usr_config_dir, 0777, wxPATH_MKDIR_FULL);
				return usr_config_dir;
			}
		#endif //__WXMAC__
	#endif // __OMBCONVERT_BIN__

wxString GetLogString(int S){
	wxString value;
	switch(S){
		case 0:
			value=_("Total");
			break;
		case 1:
			value=_("Profit");
			break;
		case 2:
			value=_("Expense");
			break;
		case 3:
			value=_("New credit");
			break;
		case 4:
			value=_("Removed credit");
			break;
		case 5:
			value=_("Remitted credit");
			break;
		case 6:
			value=_("New debt");
			break;
		case 7:
			value=_("Removed debt");
			break;
		case 8:
			value=_("Remitted debt");
			break;
		case 9:
			value=_("Received object");
			break;
		case 10:
			value=_("Given object");
			break;
		case 11:
			value=_("Lent object");
			break;
		case 12:
			value=_("Object got back");
			break;
		case 13:
			value=_("Borrowed object");
			break;
		case 14:
			value=_("Object given back");
			break;
		default:
			value=wxEmptyString;}
		return value;
	}

	#ifndef __OMBCONVERT_BIN__
		wxString GetUserLocalDir(void){
			wxString usr_data_dir;
			wxGetEnv(L"XDG_DATA_HOME",&usr_data_dir);
			if(usr_data_dir.IsEmpty()) usr_data_dir = wxGetHomeDir() + L"/.local/share";
			usr_data_dir += L"/openmoneybox";
			if(! wxDirExists(usr_data_dir))
				wxFileName::Mkdir(usr_data_dir, 0777, wxPATH_MKDIR_FULL);
			return usr_data_dir;
		}
	#endif // __OMBCONVERT_BIN__

#ifdef __WXGTK__
wxString GetDesktopEnv(void){
	wxString desktop;
	wxGetEnv(L"XDG_CURRENT_DESKTOP",&desktop);
	desktop.MakeLower();
	return desktop;
}
#endif // __WXGTK__

#ifndef NDEBUG
	void ombLogMessage(wxString Text){
		wxLogMessage(Text);
	}
#endif // NDEBUG

#endif  // IGIOMB_CPP_INCLUDED

