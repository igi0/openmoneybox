/***************************************************************
 * Name:      igiomb.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-12-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef IGIOMB_HEADER_INCLUDED
#define IGIOMB_HEADER_INCLUDED

#ifndef __OMBCONVERT_BIN__
	#include <wx/wx.h>

	#include "../types.h"

	WXEXPORT wxString CheckValue(wxString Val);
	WXEXPORT wxString FormDigits(double Val);
	WXEXPORT wxString IntToMonth(int M);

	#if defined (bilFFORMAT_301) || 	defined (bilFFORMAT_302)
		WXEXPORT wxDateTime Omb_StrToDate(wxString S);
	#endif // defined

	#ifdef _OMB_INSTALLEDUPDATE
		WXEXPORT TVersion GetInstalledVersion(void);
	#endif // _OMB_INSTALLEDUPDATE

	/* TODO -cEnhancement : Preparation for future enhancement */
	/*
	#ifdef __WXMSW__
		WXEXPORT TVersion GetLastRunVersion(void);
	#endif // __WXMSW__
	*/

	#ifdef _OMB_MONOLITHIC
    extern wxString GetOSDocDir(void);
  	extern wxString GetCurrencySymbol(void);
    extern wxString GetDataDir(void);
  #else
    WXEXPORT wxString GetOSDocDir(void);
  	WXEXPORT wxString GetCurrencySymbol(void);
    WXEXPORT wxString GetDataDir(void);
  #endif // _OMB_MONOLITHIC

	#ifdef _OMB_INSTALLEDUPDATE
		WXEXPORT void CheckUpdate(void);
		WXEXPORT wxString GetVersionFile(void);
	#endif // _OMB_INSTALLEDUPDATE

	WXEXPORT wxString SubstSpecialChars(wxString S);
	WXEXPORT wxString GetShareDir(void);

	WXEXPORT wxString DoubleQuote(wxString S);

	#ifdef __WXGTK__
		WXEXPORT wxString GetUserConfigDir(void);
		WXEXPORT wxString GetDesktopEnv(void);
	#endif // __WXGTK__

	WXEXPORT wxString GetLogString(int S);
	WXEXPORT wxString GetUserLocalDir(void);

  #ifndef NDEBUG
		WXEXPORT void ombLogMessage(wxString Text);
	#endif // NDEBUG

#endif

#endif // IGIOMB_HEADER_INCLUDED
