/***************************************************************
 * Name:      openmoneybox.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2023-04-14
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMBAPP_H
	#define OMBAPP_H

	#ifdef __WXMSW__
			#include "../platformsetup.h"
	#endif // __WXMSW__

	#ifdef _OMB_MONOLITHIC
		#include <wx/snglinst.h>	// for wxSingleInstanceChecker
//		#include "../ombtray/ombtaskbar.h"
	#endif // _OMB_MONOLITHIC

	class ombApp : public wxApp{
		private:
			// Non-GUI components
			wxSingleInstanceChecker *m_checker_omb;	// checks if another instance is already running
			#ifndef __WXMSW__
				void CheckTimerFire( wxTimerEvent& event );
			#endif // __WXMSW__
			#ifdef _OMB_MONOLITHIC
				#ifdef _OMB_USEINDICATOR
					bool	isIndicator,			// true when the active desktop supports libindicator
								Act,							// Enable status
								indicator_killed;	// true when KillIndicator routine is processed for the first time
					int msqidInd, msqidOmb;

					void SendStatus(void);
					void KillIndicator(long cmd);
				#endif // _OMB_USEINDICATOR

				bool IsTrayCommand;		// received command line flag to start in tray
			#endif // _OMB_MONOLITHIC
		public:
			// Routines
			virtual bool OnInit();
			int OnExit();
			#ifdef _OMB_MONOLITHIC
				wxTimer *MainTimer;

				void TimerFire(wxTimerEvent& event);
				void DoChecks(void);
				void CheckTimerFire();
				int StartTray();
				bool ShowFrame();
				bool ShowOptions();
			#endif // _OMB_MONOLITHIC
	};

	#ifndef _OMB_MONOLITHIC
		WXIMPORT wxLanguage FindLang(void);
		WXIMPORT void Error(int Err,wxString Opt);
		WXIMPORT bool TrayActive(void);
		WXIMPORT wxString GetShareDir(void);
		#ifdef _OMB_INSTALLEDUPDATE
			WXIMPORT bool GetCheckUpdates(void);
			WXIMPORT void fnCheckUpdate(void);
		#endif // _OMB_INSTALLEDUPDATE

		#ifdef __WXGTK__
			WXIMPORT wxString GetUserConfigDir(void);
			WXIMPORT void CreateTrayAutostart(void);
		#endif // __WXGTK__

		#ifdef __WXMAC__
			WXIMPORT wxString GetUserConfigDir(void);
		#endif // __WXMAC__

	#else
		extern wxLanguage FindLang(void);

		#ifdef _OMB_INSTALLEDUPDATE
			extern bool GetCheckUpdates(void);
			extern void fnCheckUpdate(void);
		#endif // _OMB_INSTALLEDUPDATE

		#ifdef __WXGTK__
			extern wxString GetUserConfigDir(void);
			extern void CreateTrayAutostart(void);
		#endif // __WXGTK__

		#ifdef __WXMAC__
			extern wxString GetUserConfigDir(void);
		#endif // __WXMAC__

		extern wxString GetDefaultDocument(void);
		extern bool ShowOptionsDialog(void);
		extern wxString GetShareDir(void);
	#endif

#endif	// OMBAPP_H
