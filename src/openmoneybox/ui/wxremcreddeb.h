///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/combobox.h>
#include <wx/radiobox.h>
#include <wx/textctrl.h>
#include <wx/valtext.h>
#include <wx/sizer.h>
#include <wx/checkbox.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxTRCredDeb
///////////////////////////////////////////////////////////////////////////////
class wxTRCredDeb : public wxDialog
{
	DECLARE_EVENT_TABLE()
	private:

		// Private event handlers
		void _wxFB_OptClick( wxCommandEvent& event ){ OptClick( event ); }
		void _wxFB_OKBtnClick( wxCommandEvent& event ){ OKBtnClick( event ); }


	protected:
		wxStaticText* LNam;
		wxButton* OKBtn;
		wxButton* CancelBtn;

		// Virtual event handlers, override them in your derived class
		virtual void OptClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OKBtnClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxComboBox* Name;
		wxRadioBox* Opt;
		wxTextCtrl* Val;
		wxCheckBox* SysTimeCheck;
		wxString validator_string;

		wxTRCredDeb( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 400,225 ), long style = wxDEFAULT_DIALOG_STYLE );

		~wxTRCredDeb();

};

