/***************************************************************
 * Name:      funds.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "funds.h"

TOPF *OPF;

TOPF::TOPF(wxWindow *parent) : wxTOPF(parent, -1, wxEmptyString, wxDefaultPosition, wxSize(320,260), wxDEFAULT_DIALOG_STYLE){
	Values=new wxArrayString();
	Values->Clear();
	Focus(false);}

void TOPF::Focus(bool Ob){
  if(Ob)Val->SetFocus();
	else Name->SetFocus();}

void TOPF::OKBtnClick(wxCommandEvent& event){
	SetReturnCode(wxID_NONE);
	double cur;
	wxString v;
	if(Name->IsShown()&&(Name->GetValue().IsEmpty())){
		Error(23,wxEmptyString);
		Focus(false);
		return;}
	if(!Val->IsShown())goto EndOKBtnClick;
	if(Val->GetValue().IsEmpty()){
		Error(25,wxEmptyString);
		Focus(true);
		return;}
	v=CheckValue(Val->GetValue());
	if(v.IsEmpty()){
		Error(26,wxEmptyString);
		Focus(true);
		return;}
	else Val->SetValue(v);
	Val->GetValue().ToDouble(&cur);
	if(cur<=0){
		Error(27,wxEmptyString);
	    Focus(true);
	    return;}
	EndOKBtnClick:
	if(Name->IsShown())Name->SetValue(iUpperCase(Name->GetValue()));
	EndModal(wxID_OK);}

void TOPF::InitLabels(int Kind){
	int w,h,	// window height and width
		lnx,lny,
		nx,ny,
		lvx,lvy,
		vx,vy,
		ox,oy,
		cx,cy,
		ctrlw,ctrlh;	// general control height and width
	GetClientSize(&w,&h);
	LNam->GetPosition(&lnx,&lny);
	LVal->GetPosition(&lvx,&lvy);
	Name->GetPosition(&nx,&ny);
	Val->GetPosition(&vx,&vy);
	OKBtn->GetPosition(&ox,&oy);
	CancelBtn->GetPosition(&cx,&cy);
	switch(Kind){
		case 1:	// SetTotal
      LNam->GetClientSize(&ctrlw,&ctrlh);
      h-=ctrlh;
      Name->GetClientSize(&ctrlw,&ctrlh);
      h-=ctrlh;
      mSizer->Show(size_t(0),false);
      mSizer->Show(size_t(1),false);
      SetSize(w,h);
      SetTitle(_("Set the total budget"));
      LVal->SetLabel(_("Set the budget value:"));
      break;
		case 2:	// new fund
      SetTitle(_("Set a fund"));
      LNam->SetLabel(_("Insert the fund name:"));
      break;
		case 3:	// remove fund
      SetTitle(_("Remove a fund"));
      #ifdef __WXMSW__
        MSW_ROCombo();
      #else
        Name->SetWindowStyle(wxTE_PROCESS_ENTER|wxCB_READONLY);
      #endif // __WXMSW__
      LVal->GetClientSize(&ctrlw,&ctrlh);
      LVal->Show(false);
      h-=ctrlh;
      Val->GetClientSize(&ctrlw,&ctrlh);
      Val->Show(false);
      h-=ctrlh;
      OKBtn->SetPosition(wxPoint(ox,86));
      CancelBtn->SetPosition(wxPoint(oy,86));
      SetSize(w,h);
      break;
		case 4:	// Reset fund
      SetTitle(_("Reset a fund"));
      #ifdef __WXMSW__
        MSW_ROCombo();
      #else
        Name->SetWindowStyle(wxTE_PROCESS_ENTER|wxCB_READONLY);
      #endif // __WXMSW__
      break;
		case 5:	// Set default fund
      LVal->GetClientSize(&ctrlw,&ctrlh);
      h-=ctrlh;
      Val->GetClientSize(&ctrlw,&ctrlh);
      h-=ctrlh;
      mSizer->Show(size_t(2),false);
      mSizer->Show(size_t(3),false);
      SetSize(w,h);
      SetTitle(_("Set the default fund"));
      LVal->Show(false);
      Val->Show(false);
      LNam->SetLabel(_("Choose the default fund:"));
      #ifdef __WXMSW__
        MSW_ROCombo();
      #else
        Name->SetWindowStyle(wxTE_PROCESS_ENTER|wxCB_READONLY);
      #endif // __WXMSW__
      break;
		default:
      return;}
    //Layout(); // called by caller
}

TOPF::~TOPF(void){
  Values->Clear();
	delete Values;}

void TOPF::AddValue(const wxString& Val){
	Values->Add(Val);}

void TOPF::NameChange(wxCommandEvent& event){
	if(! Val->IsShown() || Name->GetWindowStyle() != (wxTE_PROCESS_ENTER | wxCB_READONLY)){
		return;}
	Val->SetValue(Values->Item(Name->GetCurrentSelection()));}

void TOPF::NameText( wxKeyEvent& event){
	if(event.GetKeyCode()==9){
		if(Val->IsShown())Val->SetFocus();
		else OKBtn->SetFocus();
		return;}
	if(Name->GetWindowStyle() != (wxTE_PROCESS_ENTER | wxCB_READONLY)) event.Skip();}

#ifdef __WXMSW__
  // On MSW unexpected horizontal scrollbar is shown when changing wxComboBox stile to wxCB_READONLY
  void TOPF::MSW_ROCombo(void){
    delete Name;
    Name = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
    wxSizerFlags flags=wxSizerFlags(0);
    flags.Expand();
    flags.Border(wxRIGHT|wxLEFT,10);
    mSizer->Insert(1,Name,flags);

	// Connect Events
	Name->Connect( wxEVT_CHAR, wxKeyEventHandler( TOPF::NameText ), NULL, this );
	Name->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( TOPF::NameChange ), NULL, this );}
#endif // __WXMSW__

