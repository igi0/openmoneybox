///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/listbox.h>
#include <wx/bmpbuttn.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/checklst.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxFundGroups
///////////////////////////////////////////////////////////////////////////////
class wxFundGroups : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText1;
		wxBitmapButton* DeleteBtn;
		wxTextCtrl* NameEdit;
		wxBitmapButton* m_bpButton1;
		wxStaticText* m_staticText3;
		wxStaticText* m_staticText31;
		wxButton* m_button3;
		wxButton* OKBtn;

		// Virtual event handlers, override them in your derived class
		virtual void GroupSelected( wxCommandEvent& event ) { event.Skip(); }
		virtual void DeleteGroupClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void AddGroupClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void FundToggled( wxCommandEvent& event ) { event.Skip(); }
		virtual void OKBtnClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxListBox* GroupList;
		wxCheckListBox* FundList;

		wxFundGroups( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Groups"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 400,450 ), long style = wxCAPTION|wxCLOSE_BOX|wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER );

		~wxFundGroups();

};

