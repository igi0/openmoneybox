/***************************************************************
 * Name:      wxgridimagerenderer.h
 * Purpose:   Header for custom cell rendered Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2023-04-30
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef GRIDIMAGERENDERER_H_INCLUDED
#define GRIDIMAGERENDERER_H_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/grid.h>

class myImageGridCellRenderer : public wxGridCellStringRenderer
{
private:
	wxString imgpath;
public:
	explicit myImageGridCellRenderer(const wxString& path);
	virtual void Draw(wxGrid& grid, wxGridCellAttr& attr, wxDC& dc, const wxRect& rect, int row, int col, bool isSelected);
};

#endif	// GRIDIMAGERENDERER_H_INCLUDED
