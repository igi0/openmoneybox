/***************************************************************
 * Name:      editcategories.cpp
 * Purpose:   Category Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-08-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/wx.h>

#include "editcategories.h"
#include "../../constants.h"

wxEditCategories::wxEditCategories( wxWindow* parent )
:
wxFEditCategories( parent )
{
	modified = false;
	IconsInitialized = false;
	FirstAddedId = -1;

	#ifdef __WXMSW__
		CategoryList->SetSingleStyle(wxLC_SINGLE_SEL, false);	// This lines prevent a bug that prevents items from being shown
		CategoryList->SetSingleStyle(wxLC_REPORT, false);	// This line prevents a bug that prevents items from being shown
		IconList->SetSingleStyle(wxLC_SINGLE_SEL, false);	// This line prevents a bug that prevents items from being shown
	#endif // __WXMSW__

}

void wxEditCategories::OKBtnClick( wxCommandEvent& event ){
	EndModal(wxID_OK);}

void wxEditCategories::OnListCtrl(wxListEvent& event){
	int id = event.GetId();
	if(id == ombListCat){
		bool Enable = CategoryList->GetSelectedItemCount() > 0;
		Remove->Enable(Enable);
		IconBtn->Enable(Enable);
	}
	else if(id == ombListIco){
		ChooseBtn->Enable(IconList->GetSelectedItemCount() > 0);

		if(ChooseBtn->IsEnabled()){
			int IconIndex = IconList->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED) + 1;

			if(IconIndex == _OMB_TOPCATEGORIES_OEMICON){
				wxFileDialog *Dialog=new wxFileDialog(wxTheApp->GetTopWindow(),wxEmptyString,wxEmptyString,wxEmptyString,wxEmptyString,wxFD_OPEN,wxDefaultPosition,wxDefaultSize,L"Dialog");
				Dialog->SetDirectory(GetOSDocDir());
				wxString filter = _("Images (*.png)");
				filter += L"|*.png;*.PNG";
				Dialog->SetWildcard(filter);
				if(Dialog->ShowModal() == wxID_OK){
					wxString File = Dialog->GetPath();
					delete Dialog;

					wxBitmap Bmp = wxBitmap(File, wxBITMAP_TYPE_PNG);
					if((Bmp.GetWidth() > 24) || (Bmp.GetHeight() > 24)){
						wxImage img = Bmp.ConvertToImage();
						Bmp = wxBitmap(img.Scale(24, 24));
					}

					wxImageList *imgList = CategoryList->GetImageList(wxIMAGE_LIST_NORMAL);

					imgList->Add(Bmp);
					int imgind = imgList->GetImageCount() - 1;
					int index = IconList->GetItemCount();
					IconList->InsertItem(index, L"", imgind);

					if(FirstAddedId == -1) FirstAddedId = imgind;

				}

			}
		}
	}
}

void wxEditCategories::AddButtonClick(wxCommandEvent& event){
	wxString value = iUpperCase(EditBox->GetValue());
	if(value != wxEmptyString){
		bool Ex = false;
		for(int i = 0; i < CategoryList->GetItemCount(); i++){
			if(CategoryList->GetItemText(i) == value){
				Ex = true;
				break;
			}
		}
		if(! Ex){
			CategoryList->InsertItem(CategoryList->GetItemCount(), value, 0);
			modified = true;}}}

void wxEditCategories::RemoveButtonClick(wxCommandEvent& event){
	int OldIndex = CategoryList->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
	if(OldIndex >= 0){
		CategoryList->DeleteItem(OldIndex);
		if(CategoryList->GetItemCount() >= (OldIndex + 1))CategoryList->SetItemState(OldIndex, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);
		else if(CategoryList->GetItemCount())CategoryList->SetItemState(CategoryList->GetItemCount() - 1, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);
		wxListEvent evt(wxEVT_NULL, 0);
		OnListCtrl(evt);
		modified = true;
	}
}

void wxEditCategories::IconButtonClick(wxCommandEvent& event){

	if(! IconsInitialized){
		wxImageList *imgList = CategoryList->GetImageList(wxIMAGE_LIST_NORMAL);
		IconList->SetImageList(imgList, wxIMAGE_LIST_NORMAL);

		for(int i = 0; i < imgList->GetImageCount(); i++)
			IconList->InsertItem(i /*- 1*/, L"", i);

		IconsInitialized = true;
	}

	IconList->Show(true);
	ChooseBtn->Show(true);

	Layout();
}

void wxEditCategories::ChooseBtnClick(wxCommandEvent& event){
	int SelCat = CategoryList->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
	int IconIndex = IconList->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)/* + 1*/;

	CategoryList->SetItemImage(SelCat, IconIndex);

	modified = true;
}
