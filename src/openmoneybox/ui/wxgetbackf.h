///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/combobox.h>
#include <wx/checkbox.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/sizer.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxGetBackF
///////////////////////////////////////////////////////////////////////////////
class wxGetBackF : public wxDialog
{
	private:

	protected:
		wxStaticText* LOgg;
		wxStaticText* LPer;
		wxButton* OKBtn;
		wxButton* m_button2;

		// Virtual event handlers, override them in your derived class
		virtual void PerChange( wxCommandEvent& event ) { event.Skip(); }
		virtual void OKBtnClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxComboBox* Obj;
		wxComboBox* Per;
		wxCheckBox* SysTimeCheck;

		wxGetBackF( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 300,225 ), long style = wxDEFAULT_DIALOG_STYLE );

		~wxGetBackF();

};

