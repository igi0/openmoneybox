///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxgainexpense.h"

///////////////////////////////////////////////////////////////////////////

wxTOperationF::wxTOperationF( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer3 = new wxFlexGridSizer( 11, 1, 0, 0 );
	fgSizer3->AddGrowableCol( 0 );
	fgSizer3->AddGrowableRow( 10 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	LFund = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	LFund->Wrap( -1 );
	LFund->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer3->Add( LFund, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 10 );

	FundName = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN|wxCB_READONLY );
	FundName->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer3->Add( FundName, 0, wxRIGHT|wxLEFT|wxEXPAND, 10 );

	LValue = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	LValue->Wrap( -1 );
	LValue->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer3->Add( LValue, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 10 );

	OpValue = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	OpValue->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	OpValue->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &validator_string ) );

	fgSizer3->Add( OpValue, 0, wxEXPAND|wxRIGHT|wxLEFT, 10 );

	m_panel1 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxBORDER_SIMPLE|wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer31;
	fgSizer31 = new wxFlexGridSizer( 2, 2, 0, 0 );
	fgSizer31->AddGrowableCol( 0 );
	fgSizer31->SetFlexibleDirection( wxBOTH );
	fgSizer31->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	CurrencyCheckbox = new wxCheckBox( m_panel1, wxID_ANY, _("Default currency"), wxDefaultPosition, wxDefaultSize, 0 );
	CurrencyCheckbox->SetValue(true);
	fgSizer31->Add( CurrencyCheckbox, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	Currency = new wxTextCtrl( m_panel1, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Currency->Hide();

	fgSizer31->Add( Currency, 0, wxALL|wxEXPAND, 5 );

	RateLabel = new wxStaticText( m_panel1, wxID_ANY, _("Change rate (default/custom):"), wxDefaultPosition, wxDefaultSize, 0 );
	RateLabel->Wrap( -1 );
	RateLabel->Hide();

	fgSizer31->Add( RateLabel, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	Rate = new wxTextCtrl( m_panel1, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Rate->Hide();

	Rate->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &validator_string1 ) );

	fgSizer31->Add( Rate, 0, wxALL|wxEXPAND, 5 );


	m_panel1->SetSizer( fgSizer31 );
	m_panel1->Layout();
	fgSizer31->Fit( m_panel1 );
	fgSizer3->Add( m_panel1, 1, wxEXPAND | wxALL, 5 );

	LMat = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	LMat->Wrap( -1 );
	LMat->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer3->Add( LMat, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 10 );

	Matter = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN );
	Matter->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer3->Add( Matter, 0, wxEXPAND|wxRIGHT|wxLEFT, 10 );

	LCat = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	LCat->Wrap( -1 );
	fgSizer3->Add( LCat, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 10 );

	Category = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN );
	Category->SetFont( wxFont( 11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer3->Add( Category, 0, wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 10 );

	SysTimeCheck = new wxCheckBox( this, wxID_ANY, _("Use system time"), wxDefaultPosition, wxDefaultSize, 0 );
	SysTimeCheck->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer3->Add( SysTimeCheck, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	wxFlexGridSizer* fgSizer4;
	fgSizer4 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer4->AddGrowableCol( 0 );
	fgSizer4->AddGrowableCol( 1 );
	fgSizer4->AddGrowableRow( 0 );
	fgSizer4->SetFlexibleDirection( wxBOTH );
	fgSizer4->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	OKBtn = new wxButton( this, wxID_OK, _("OK"), wxDefaultPosition, wxDefaultSize, 0 );

	OKBtn->SetDefault();
	OKBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer4->Add( OKBtn, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );

	CancelBtn = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	CancelBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer4->Add( CancelBtn, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	fgSizer3->Add( fgSizer4, 1, wxEXPAND, 5 );


	this->SetSizer( fgSizer3 );
	this->Layout();

	// Connect Events
	FundName->Connect( wxEVT_CHAR, wxKeyEventHandler( wxTOperationF::FundNameKeyPress ), NULL, this );
	CurrencyCheckbox->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( wxTOperationF::CurrencyClick ), NULL, this );
	OKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxTOperationF::OKBtnClick ), NULL, this );
}

wxTOperationF::~wxTOperationF()
{
	// Disconnect Events
	FundName->Disconnect( wxEVT_CHAR, wxKeyEventHandler( wxTOperationF::FundNameKeyPress ), NULL, this );
	CurrencyCheckbox->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( wxTOperationF::CurrencyClick ), NULL, this );
	OKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxTOperationF::OKBtnClick ), NULL, this );

}
