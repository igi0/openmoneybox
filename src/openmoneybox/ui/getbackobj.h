/***************************************************************
 * Name:      getbackobj.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef GetBackObjH
#define GetBackObjH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxgetbackf.h"

class TGetBackF : public wxGetBackF
{
private:
	// GUI components

	//Non-GUI components

	// Routines
	void OKBtnClick(wxCommandEvent& event) override;
	void FormDestroy(void);
	void Focus(bool Err);
public:
	//Non-GUI components
	wxArrayString *Memo;

	// Routines
	explicit TGetBackF(wxWindow *parent);
	~TGetBackF(void);
	void InitLabels(bool O);
	void PerChange(wxCommandEvent& event) override;
};

// Calls to module ombLogo.DLL
#if (wxCHECK_VERSION(3, 2, 0) )
	extern wxString iUpperCase(wxString S);
#else
	WXIMPORT wxString iUpperCase(wxString S);
#endif

#ifdef _OMB_MONOLITHIC
  extern void Error(int Err, const wxString &Opt);
#else
  // Calls to module omberr.DLL
  WXIMPORT void Error(int Err,wxString Opt);
#endif // _OMB_MONOLITHIC

#endif // GetBackObjH


