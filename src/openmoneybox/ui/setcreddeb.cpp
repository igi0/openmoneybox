/***************************************************************
 * Name:      setcreddeb.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2023-01-23
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/fileconf.h> // For wxFileConfig

#include "../../types.h"
#include "wxsetcreddeb.h"
#include "setcreddeb.h"

TSCredDeb::TSCredDeb(wxWindow *parent):wxTSCredDeb(parent, -1, wxEmptyString, wxDefaultPosition, wxSize( 430,290 ), wxDEFAULT_DIALOG_STYLE){
	Focus(false);}

void TSCredDeb::Focus(bool Err){
  if(Err)Val->SetFocus();
	else Name->SetFocus();}

void TSCredDeb::OKBtnClick(wxCommandEvent& event){
	double cur;
	SetReturnCode(wxID_NONE);
	if(Name->GetValue().IsEmpty()){
		Error(30,wxEmptyString);
		Focus(false);
		return;}
	if(Val->GetValue().IsEmpty()){
		Error(25,wxEmptyString);
		Focus(true);
		return;}
	wxString v=CheckValue(Val->GetValue());
	if(v.IsEmpty()){
		Error(26,wxEmptyString);
		Focus(true);
		return;}
	else Val->SetValue(v);
	Val->GetValue().ToDouble(&cur);
	if(cur <= 0){
		Error(27, wxEmptyString);
		Focus(true);
		return;}
	Name->SetValue(iUpperCase(Name->GetValue()));
	EndModal(wxID_OK);}

void TSCredDeb::InitLabels(bool E){
	if(!E){
		SetTitle(_("Set a new credit"));
		LNam->SetLabel(_("Insert the credit name:"));
		LVal->SetLabel(_("Insert the credit value:"));}
	else{
		SetTitle(_("Set a new debt"));
		LNam->SetLabel(_("Insert the debt name:"));
		LVal->SetLabel(_("Insert the debt value:"));}}

