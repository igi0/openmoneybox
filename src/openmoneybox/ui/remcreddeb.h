/***************************************************************
 * Name:      remcreddeb.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef RemCredDebH
#define RemCredDebH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxremcreddeb.h"
#include "../../types.h"

class TRCredDeb : public wxTRCredDeb
{
//__published:
private:
	// Non-GUI components
	bool Debt;

	// Routines
	void Focus(bool Err);
protected:
	void OptClick(wxCommandEvent& event) override;
	void OKBtnClick(wxCommandEvent& event) override;
public:
	// Routines
	explicit TRCredDeb(wxWindow *parent);
	void InitLabels(int E);
};

extern TRCredDeb *RCredDeb;

// Calls to module ombLogo
#if (wxCHECK_VERSION(3, 2, 0) )
	extern wxString iUpperCase(wxString S);
#else
	WXIMPORT wxString iUpperCase(wxString S);
#endif
// Calls to module igiomb
WXIMPORT wxLanguage FindLang(void);

#ifdef _OMB_MONOLITHIC
  extern wxString GetCurrencySymbol(void);
  extern void Error(int Err, const wxString &Opt);
  extern wxString CheckValue(const wxString& Val);
#else
  WXIMPORT wxString GetCurrencySymbol(void);
  WXIMPORT void Error(int Err,wxString Opt);
  WXIMPORT wxString CheckValue(wxString Val);
#endif // _OMB_MONOLITHIC

#endif // RemCredDebH


