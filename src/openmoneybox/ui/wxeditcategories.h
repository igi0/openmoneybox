///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/listctrl.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxFEditCategories
///////////////////////////////////////////////////////////////////////////////
class wxFEditCategories : public wxDialog
{
	DECLARE_EVENT_TABLE()
	private:

		// Private event handlers
		void _wxFB_FormShow( wxActivateEvent& event ){ FormShow( event ); }
		void _wxFB_OnListCtrl( wxListEvent& event ){ OnListCtrl( event ); }
		void _wxFB_AddButtonClick( wxCommandEvent& event ){ AddButtonClick( event ); }
		void _wxFB_RemoveButtonClick( wxCommandEvent& event ){ RemoveButtonClick( event ); }
		void _wxFB_IconButtonClick( wxCommandEvent& event ){ IconButtonClick( event ); }
		void _wxFB_ChooseBtnClick( wxCommandEvent& event ){ ChooseBtnClick( event ); }
		void _wxFB_OKBtnClick( wxCommandEvent& event ){ OKBtnClick( event ); }


	protected:
		enum
		{
			ombListCat = 1000,
			ombAdd,
			ombRemove,
			ombIcon,
			ombListIco,
			ombChoose,
		};

		wxTextCtrl* EditBox;
		wxButton* m_button3;
		wxButton* Remove;
		wxListCtrl* IconList;
		wxButton* ChooseBtn;
		wxButton* OKBtn;
		wxButton* CancelBtn;

		// Virtual event handlers, override them in your derived class
		virtual void FormShow( wxActivateEvent& event ) { event.Skip(); }
		virtual void OnListCtrl( wxListEvent& event ) { event.Skip(); }
		virtual void AddButtonClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void RemoveButtonClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void IconButtonClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void ChooseBtnClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OKBtnClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxListCtrl* CategoryList;
		wxButton* IconBtn;

		wxFEditCategories( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Edit the categories"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 490,280 ), long style = wxCAPTION|wxDEFAULT_DIALOG_STYLE );

		~wxFEditCategories();

};

