/***************************************************************
 * Name:      remcreddeb.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2023-01-23
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxremcreddeb.h"
#include "remcreddeb.h"

TRCredDeb *RCredDeb;

TRCredDeb::TRCredDeb(wxWindow *parent):wxTRCredDeb(parent,-1,wxEmptyString,wxDefaultPosition,wxSize( 400,300 ),wxDEFAULT_DIALOG_STYLE/*,_T("RCredDeb")*/){
	Opt->SetLabel(wxString::Format(_("Remove (%s)"),GetCurrencySymbol()));
	Focus(false);
	Debt=false;}

void TRCredDeb::Focus(bool Err){
  if(Err)Val->SetFocus();
  else Name->SetFocus();}

void TRCredDeb::OptClick(wxCommandEvent& event){
	if(Opt->GetSelection()==1)
		Val->Enable(true);
	else
		Val->Enable(false);}

void TRCredDeb::OKBtnClick(wxCommandEvent& event){
	double cur;
	SetReturnCode(wxID_NONE);
	if(Name->GetValue().IsEmpty()){
		if(!Debt)Error(39,wxEmptyString);
		else Error(5,wxEmptyString);
		Focus(false);
		return;}
	if(Opt->GetSelection()==1){
		if(Val->GetValue().IsEmpty()){
			Error(25,wxEmptyString);
			Focus(true);
			return;}
		else{
			wxString v=CheckValue(Val->GetValue());
			if(v.IsEmpty()){
				Error(26,wxEmptyString);
				Focus(true);
				return;}
			else{
			Val->GetValue().ToDouble(&cur);
			if(cur <= 0){
			Error(27,wxEmptyString);
			Focus(true);
			return;}}}}
	Name->SetLabel(iUpperCase(Name->GetValue()));
	EndModal(wxID_OK);}

void TRCredDeb::InitLabels(int E){
	switch(E){
		case 1:
		SetTitle(_("Delete a credit"));
		LNam->SetLabel(_("Choose the credit to remove:"));
    Debt=false;
    break;
    case 2:
    SetTitle(_("Delete a debt"));
		LNam->SetLabel(_("Choose the debt to remove:"));
    Debt=true;
    break;
    case 3:
    SetTitle(_("Remit a credit"));
		LNam->SetLabel(_("Choose the credit to remit:"));
		Debt=false;
		break;
		case 4:
		SetTitle(_("Remit a debt"));
		LNam->SetLabel(_("Choose the debt to remit:"));
    Debt=true;
    break;
    default:
    return;}}

