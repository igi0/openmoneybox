/***************************************************************
 * Name:      gainexpense.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/dialog.h>

#ifndef GainExpenseH
#define GainExpenseH

#include "wxgainexpense.h"

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class TOperationF : public wxTOperationF
{
private:
	bool CheckValues;	// true if formal checks must be made on OK button press
	// Routines
	void OKBtnClick(wxCommandEvent& event) override;
	void FundNameKeyPress(wxKeyEvent& event) override;
	void Focus(int F);
	void CurrencyClick(wxCommandEvent& event) override;
public:
	explicit TOperationF(wxWindow *parent);
	void InitLabels(int kind);
};

#if (wxCHECK_VERSION(3, 2, 0) )
	extern wxString iUpperCase(wxString S);
#else
	WXIMPORT wxString iUpperCase(wxString S);
#endif

#ifdef _OMB_MONOLITHIC
  extern void Error(int Err, const wxString &Opt);
  extern wxString CheckValue(const wxString& Val);
  extern wxString GetCurrencySymbol(void);
#else
  WXIMPORT void Error(int Err, const wxString &Opt);
  WXIMPORT wxString CheckValue(const wxString& Val);
  WXIMPORT wxString GetCurrencySymbol(void);
#endif // _OMB_MONOLITHIC

#endif // GainExpenseH
