/***************************************************************
 * Name:      main_wx.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igorcali@disroot.org)
 * Created:   2025-03-12
 * Copyright: Igor Calì (igorcali@disroot.org)
 * License:		GNU
 **************************************************************/

#ifndef MAINFRAME_CPP_INCLUDED
#define MAINFRAME_CPP_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/imaglist.h> // For wxImageList
#include <wx/clipbrd.h> // For wxTheClipboard
#include <wx/filename.h>
#include <wx/gdicmn.h>
#include <wx/stdpaths.h>	// For wxStandardPaths::Get()

#ifdef _OMB_CHART_MATLIBPLOT
	#include "wxmainframe_24_mpl.h"
#else
	#include "wxmainframe_24.h"
#endif // _OMB_CHART_MATLIBPLOT

#include "../../omb35core.h"

#include "main_wx.h"
#include "funds.h"
#include "gainexpense.h"
#include "editcategories.h"
#include "setcreddeb.h"
#include "remcreddeb.h"
#include "objct.h"
#include "getbackobj.h"
#include "../../constants.h"

#include "ombFundGroups.h"

#ifndef __WXMSW__
  #include "../../../rsrc/icons/Icon_Application.xpm"
#endif

#ifdef __WXMSW__
  #include "../../../rsrc/toolbar/16/creditnew.xpm"
  #include "../../../rsrc/toolbar/16/debtnew.xpm"
  #include "../../../rsrc/toolbar/16/newshop.xpm"
  #include "../../../rsrc/toolbar/16/objectgiven.xpm"
  #include "../../../rsrc/toolbar/16/objectreceived.xpm"
  #include "../../../rsrc/toolbar/16/newfund.xpm"
#else
	#include "../../../rsrc/toolbar/20/creditnew20.xpm"
	#include "../../../rsrc/toolbar/20/debtnew20.xpm"
	#include "../../../rsrc/toolbar/20/apple_red20.xpm"
	#include "../../../rsrc/toolbar/20/objectgiven20.xpm"
	#include "../../../rsrc/toolbar/20/objectreceived20.xpm"
	#include "../../../rsrc/toolbar/20/cash_line20.xpm"
	#include "../../../rsrc/toolbar/20/cash_line20_light.xpm"
#endif // __WXMSW__

#include "../../../rsrc/toolbar/24/cash_line_light.xpm"
#include "../../../rsrc/toolbar/24/delete_bin_line_light.xpm"
#include "../../../rsrc/toolbar/24/shopping_bag_line_light.xpm"

#include "wxgridimagerenderer.h"

#ifdef _OMB_USE_CIPHER
	#include "../../ui/password.h"
#endif

#if _OMB_HASNATIVEABOUT == 0
    extern wxString ProductName;
    extern wxString ShortVersion;
    extern wxString LongVersion;
    extern char __BUILD_DATE;
#endif // _OMB_HASNATIVEABOUT
extern wxString LongVersion;
extern wxString Copyright;
extern wxString WebAddress;
extern wxString BugAddress;
extern wxString DonateAddress;
extern wxString PrivacyAddress;
extern TData *Data;
extern wxLanguage Lan;

extern wxString cs_customicons;

#ifdef __WXGTK__
	extern wxString Console;
	bool isGnome;
#endif // __WXGTK__

#ifdef _OMB_USE_CIPHER
	extern bool TableExists(const wxString& tableName, const wxString& databaseName, sqlite3 *m_db);
	extern int ExecuteUpdate(sqlite3 *m_db, const char* sql/*, bool saveRC = false*/);
	extern wxSQLite3Table GetTable(sqlite3 *m_db, const char* sql);
#endif // _OMB_USE_CIPHER

wxString MapBinary = L"/bin/ombmapviewer";
long MapPID = -1;

ombMainFrame::ombMainFrame(wxWindow* parent):wxMainFrame(parent, wxID_ANY, _("Budget management"), wxDefaultPosition, wxSize(790, 630), wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL ){

	Master_Attached = false;
	Initialised=false;

	OriginalCategoryIndex = -1;
	Report->SetSelectionMode(wxGrid::wxGridSelectRows);
	// Disable report cell highlighting
	Report->SetCellHighlightPenWidth(0);
	Report->SetCellHighlightROPenWidth(0);
	//Report->SetCellHighlightColour(wxColour(0x33, 0, 0));

	backup_report_colour = Report->GetDefaultCellTextColour();

	// Menu
	#if _OMB_INSTALLEDCONV == 0
		MainMenu_Tools_->Remove(bilOldConv);
	#endif // _OMB_INSTALLEDCONV

	#ifndef _OMB_USE_CIPHER
		MainMenu_File_->Remove(bilModifyPassword);
		MainMenu_File_->Remove(ombRemovePassword);
		MainMenu_File_->Remove(MainMenu_File_->FindItemByPosition(MainMenu_File_->GetMenuItemCount()-2)); // password_separator removal
	#endif // _OMB_USE_CIPHER

	// GUI controls
	// TreeCtrl glyphs
	#ifdef __WXMSW__
		wxImageList *imlist=new wxImageList(16,16,true,0);
	#else
		wxImageList *imlist=new wxImageList();
	#endif // __WXMSW__
	imlist->RemoveAll();

	wxColour BG = ToolBar->GetBackgroundColour();
	// TODO (igor#1#): Change with wxColour.GetLuminance with wx3.2
	int R = BG.Red(), G = BG.Green(), B = BG.Blue();
	double Luminance = (0.299 * R + 0.587 * G + 0.114 * B) / 256;

	#ifdef __WXMSW__
		imlist->Add(wxBitmap(newfund_xpm),wxNullBitmap);
		imlist->Add(wxBitmap(creditnew_xpm),wxNullBitmap);
		imlist->Add(wxBitmap(debtnew_xpm),wxNullBitmap);
		imlist->Add(wxBitmap(newshop_xpm),wxNullBitmap);
		imlist->Add(wxBitmap(objectgiven_xpm),wxNullBitmap);
		imlist->Add(wxBitmap(objectreceived_xpm),wxNullBitmap);

	#else
		if(Luminance < 0.3)
			imlist->Add(wxBITMAP(cash_line20_light),wxNullBitmap);
		else
			imlist->Add(wxBITMAP(cash_line20),wxNullBitmap);
		imlist->Add(wxBITMAP(creditnew20),wxNullBitmap);
		imlist->Add(wxBITMAP(debtnew20),wxNullBitmap);
		imlist->Add(wxBITMAP(apple_red20),wxNullBitmap);
		imlist->Add(wxBITMAP(objectgiven20),wxNullBitmap);
		imlist->Add(wxBITMAP(objectreceived20),wxNullBitmap);
  #endif // __WXMSW__
	ItemList->SetImageList(imlist);

	TreeNames=new wxArrayString;
	ResetTree();

	// Charts
	#ifdef _OMB_CHART_WXPIECTRL
		// create pie chart ctrl for total disk usage
		FundChart = new wxPieCtrl(FundPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize);
		//wxASSERT(FundChart);

		// setup the legend
		FundChart->SetTransparent(true);
		FundChart->SetHorLegendBorder(10);
		//FundChart->SetLabelFont(wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxT("Ubuntu") ));
		//FundChart->SetLabelColour(wxColour(0,0,0));
		FundChart->SetLabel(_("Fund Chart"));

		FundSizer->Add(FundChart, wxSizerFlags(1).Expand());

		TrendChart = new CPaintStatistics(TrendPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize);
		TrendSizer->Add(TrendChart, wxSizerFlags(1).Expand());

	#elif defined ( _OMB_CHART_MATLIBPLOT )
		//FundChart = new wxStaticBitmap(FundPanel, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize);
		//FundChart->Center(wxBOTH);
		//TrendChart = new wxStaticBitmap(FundPanel, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize);
		//TrendChart->Center(wxBOTH);
	#endif // _OMB_CHART_WXPIECTRL

	// Right vertical part
	TopCategoriesPanel = new TEPanel(m_panel9, wxID_ANY, wxDefaultPosition, wxDefaultSize);
	TopCategoriesSizer->Add(TopCategoriesPanel, wxSizerFlags(1).Expand());

	// Find dialog preparation
	FindData=new wxFindReplaceData(0);
	FindData->SetFlags(wxFR_DOWN);
	FindDialog = NULL;

	// Additional Event-Handlers
	Connect(wxEVT_COMMAND_FIND,wxFindDialogEventHandler(ombMainFrame::Find));
	Connect(wxEVT_COMMAND_FIND_NEXT,wxFindDialogEventHandler(ombMainFrame::Find));
	Connect(wxEVT_COMMAND_FIND_CLOSE,wxFindDialogEventHandler(ombMainFrame::FindClose));

	InitLanguage();
	#ifdef __WXMSW__
		SetIcon(wxICON(aaaa1omb_MAINICON));
	#else
		SetIcon(wxICON(Logo));
	#endif

	/*	// DON'T REMOVE - May be restored with new Product version
	MAINMENU->Enable(OldConv,ConvInstalled()); // Conversion menu item setting
	*/

	ToolBar->Show(ShowBar());
	// Wizard Check
	bool FRun=false;
	if(IsFirstRunEver()){
    FRun=true;
		wxCommandEvent evt(wxEVT_NULL,0);
		this->WizClick(evt);}
	RebuildTools();
	Report->SetRowMinimalAcceptableHeight(abs(Report->GetDefaultCellFont().GetPointSize())+5);
	Maximize(true);
	if(!FRun)ShowControls(false);
	Initialised=true;

	/*
	#ifdef __WXGTK__
		MainMenu_Help_->Remove(bilContributions);
	#endif
	*/

	Progress->Reparent(StatusBar);	// Necessary for the correct Progress position
	StatusBar->SetStatusWidths(2,NULL);

	map_viewer_exist = wxFileExists(MapBinary);
	if(! map_viewer_exist) MainMenu_Tools_->Remove(ombShowMap);

	#ifdef __WXGTK__
		wxString Desktop = GetDesktopEnv();
		if(Desktop.Right(5) == L"gnome"){
			isGnome = true;
			Title->Show(false);
			Progress->Show(false);
		}
		else isGnome = false;
	#endif // __WXGTK__

	this->Layout();

	// Necessary for wxImage.LoadImage()
	wxPNGHandler *handler = new wxPNGHandler;
	wxImage::AddHandler(handler);

	CategoryIconList = new wxImageList(24, 24);
	CategoryIconList->RemoveAll();

	wxBitmap Bmp;

	#ifdef __WXGTK__
		wxString ImagePath = GetDataDir() + L"categories/";
	#elif defined (__WXMSW__)
		wxString ImagePath;
		#ifndef _OMB_PORTABLE_MSW
			ImagePath = GetInstallationPath() + L"\\categories\\";
			if(! wxDirExists(ImagePath))
		#endif // _OMB_PORTABLE_MSW
			ImagePath = GetDataDir() + L"categories\\";
	#elif defined (__WXMAC__)
		wxString ImagePath = AppDir + L"/categories/";
	#endif // __WXMSW__
	// Images from https://remixicon.com/

	wxString Icons[] = {
		ImagePath + L"empty.png",
		ImagePath + L"t-shirt-line.png",
		ImagePath + L"home-8-line.png",
		ImagePath + L"restaurant-line.png",
		ImagePath + L"book-2-line.png",
		ImagePath + L"briefcase-line.png",
		ImagePath + L"hospital-line.png",
		ImagePath + L"community-line.png",
		ImagePath + L"goblet-line.png",
		ImagePath + L"phone-line.png",
		ImagePath + L"car-line.png",
		ImagePath + L"add-box-line.png",
	};

	#ifdef __WXMSW__
		wxImage CatImage;
	#endif // __WXMSW__
	for(int i = 0; i < _OMB_TOPCATEGORIES_OEMICON; i++){
		#ifdef __WXMSW__
      Bmp = wxNullBitmap;
			if(CatImage.LoadFile(Icons[i])) Bmp = wxBitmap(CatImage);
			if(Bmp.IsOk())
		#else
			Bmp = wxBitmap(Icons[i], wxBITMAP_TYPE_PNG);
		#endif // __WXMSW__
		CategoryIconList->Add(Bmp);
	}

	// Replace toolbar icons with dark theme
	if(Luminance < 0.3){
		ToolBar->SetToolNormalBitmap(ombNewFund, wxBitmap(cash_line_light_xpm));
		ToolBar->SetToolNormalBitmap(bilRemoveFund, wxBitmap(delete_bin_line_light_xpm));
		ToolBar->SetToolNormalBitmap(bilAddShopItem, wxBitmap(shopping_bag_line_light_xpm));
		ToolBar->SetToolNormalBitmap(bilDelShopItem, wxBitmap(delete_bin_line_light_xpm));

	}
}

ombMainFrame::~ombMainFrame(void){}

void ombMainFrame::InitLanguage(void){
	// Report first row initialisation
	wxString inistr=wxString::Format(_("Value (%s)"),GetCurrencySymbol());
	Report->SetColLabelValue(3,inistr);}

void ombMainFrame::ResetTree(void){
	ItemList->DeleteAllItems();
	TreeNames->Empty();
	TreeNames->Alloc(7);
	TreeNames->Add(_("Funds"),1);
	TreeNames->Add(_("Credits"),1);
	TreeNames->Add(_("Debts"),1);
	TreeNames->Add(_("Objects"),1);
	TreeNames->Add(_("Lent"),1);
	TreeNames->Add(_("Borrowed"),1);
	//TreeNames->Add(_("Shopping list"),1);
	TreeNames->Add(_("Total"),1);
	wxTreeItemId root,Obj;
	root=ItemList->AddRoot(_("Items"),-1,-1,NULL);
	Funds_tid=ItemList->AppendItem(root,TreeNames->Item(0),0,-1,NULL);
	Creds_tid=ItemList->AppendItem(root,TreeNames->Item(1),1,-1,NULL);
	Debts_tid=ItemList->AppendItem(root,TreeNames->Item(2),2,-1,NULL);
	Obj=ItemList->AppendItem(root,TreeNames->Item(3),3,-1,NULL);
	Lends_tid=ItemList->AppendItem(Obj,TreeNames->Item(4),4,-1,NULL);
	Borrowed_tid=ItemList->AppendItem(Obj,TreeNames->Item(5),5,-1,NULL);}

void ombMainFrame::WizClick(wxCommandEvent& event){
	wxString S;
  int res = RunWizard(Progress, S, false);
	switch(res){
    case 1:
			if(!S.IsEmpty()){
				Set_DefaultDocument(S);}
		// no break statement necessary
		case 2:
//			if(! S.IsEmpty()) Data->OpenDatabase(S);  // TODO (igor#1#): to be tested
			// no break statement necessary
    case 3:
      DoFirstRun();
		default:
		  ;}}

void ombMainFrame::ShowControls(bool V){
	#ifdef __WXGTK__
		if (! isGnome)
	#endif // __WXGTK__
	Title->Show(V);
	ItemList->Show(V);
	FundPanel->Show(V);
	TrendPanel->Show(V);
	Report->Show(V);
	ShopLabel->Show(V);
	ShopList->Show(V);

	if(V){
		wxSize s;
		s=FundPanel->GetClientSize();
		FundChart->SetMinSize(s);
		FundSizer->Fit(FundChart);
		s=TrendPanel->GetClientSize();
		#ifndef _OMB_CHART_WXPIECTRL
			TrendChart->SetMinSize(s);
			TrendSizer->Fit(TrendChart);
		#endif // _OMB_CHART_WXPIECTRL
	}

	Container->Layout();
	Update();}

void ombMainFrame::SaveClick(wxCommandEvent& event){
	if(! Data->FileData.Modified) return;
	#ifdef _OMB_USE_CIPHER
		sqlite3_exec(Data->database, "RELEASE rollback;", NULL, NULL, NULL);
	#else
		Data->database->ReleaseSavepoint(L"rollback");
	#endif // _OMB_USE_CIPHER
	Data->FileData.Modified = false;
	#ifdef _OMB_USE_CIPHER
		wxString Sql = "SAVEPOINT rollback;";
		ExecuteUpdate(Data->database, Sql.c_str());
	#else
		Data->database->Savepoint(L"rollback");
	#endif // _OMB_USE_CIPHER
	UpdateMenu();
	ShowF(false);
	return;}

void ombMainFrame::RevertClick(wxCommandEvent& event){
	if(!Data->FileData.Modified) return;
	else if(! Data->FileData.FileName.IsEmpty()){
		wxString Msg;
		wxBell();
		Msg=::wxString::Format(_("File %s has been changed.\nDo you really want to revert to saved file?"), Data->FileData.FileView.c_str());
    if(wxMessageBox(Msg, _("OpenMoneyBox"), wxYES_NO) == wxYES){
			#ifdef _OMB_USE_CIPHER
				ExecuteUpdate(Data->database, "ROLLBACK TO SAVEPOINT rollback;");
			#else
				Data->database->Rollback(L"rollback");
			#endif // _OMB_USE_CIPHER
			Data->ParseDatabase();

			Data->FileData.Modified = false;
			ShowF();
			UpdateMenu();
		}}}

/*
void ombMainFrame::PrintClick(wxCommandEvent& event){
	wxString stsstr;
	StatusBar->SetStatusText(_("Stampa del documento in corso..."));
	::wxBeginBusyCursor(wxHOURGLASS_CURSOR);
	// Temporary bitmaps creation for charts
	//DWORD dwRetVal; // TODO -cImprovement : To be improved
	//DWORD dwBufSize=512;
	//TCHAR lpPathBuffer[512];
	wxString Ch1,Ch2;
	//UINT uRes1,uRes2;
	// ---------------------------------------
		//uRes1=GetTempFileName(lpPathBuffer,"bil",0,Ch1);
		wxString hm=::wxGetHomeDir()+L"/tmp/bil";
		Ch1=::wxFileName::CreateTempFileName(hm);
		//if(uRes1==0){
		if(Ch1.IsEmpty()){
			Error(5,wxEmptyString);
			StatusBar->SetStatusText(wxEmptyString,0);
			::wxEndBusyCursor();
			return;}
		//uRes2=GetTempFileName(lpPathBuffer,"bil",0,Ch2);
		Ch2=::wxFileName::CreateTempFileName(hm);
		//if(uRes2==0){
		if(Ch2.IsEmpty()){
			Error(5,wxEmptyString);
			StatusBar->SetStatusText(wxEmptyString,0);
			::wxEndBusyCursor();
			return;}

		wxBitmap bmp=FundChart->CopyBackbuffer();
		bmp.SaveFile(Ch1,wxBITMAP_TYPE_BMP);	// to be debugged
		bmp=TrendChart->CopyBackbuffer();
		bmp.SaveFile(Ch2,wxBITMAP_TYPE_BMP);	// to be debugged

		//Data->DocExport(true,Ch1,Ch2);
		Data->XMLExport(Ch1,Ch2);
		::wxRemoveFile(Ch1);
		::wxRemoveFile(Ch2);
	//}
	//______________________________________________
	StatusBar->SetStatusText(wxEmptyString,0);
	::wxEndBusyCursor();
	return;}
*/

#ifdef _OMB_USE_CIPHER
	void ombMainFrame::ModifyPasswordClick(wxCommandEvent& event){
		Data->ModPass();
		return;}
#endif // _OMB_USE_CIPHER

#ifdef _OMB_USE_CIPHER
	void ombMainFrame::RemovePasswordClick(wxCommandEvent& event){
		if(wxMessageBox(_("Do you really want to unprotect your document?"), _("OpenMoneyBox"), wxYES_NO) == wxYES){

			// Remove password from document
			sqlite3_close(Data->database);
			IsEncryptedDB(Data->FileData.FileName, wxEmptyString, GetKey());
			#ifdef __OPENSUSE__
        if(sqlite3_open(Data->FileData.FileName.c_str(), &Data->database) == SQLITE_OK)
			#else
        if(sqlite3_open(Data->FileData.FileName, &Data->database) == SQLITE_OK)
      #endif // __OPENSUSE__
				wxRemoveFile(GetUserLocalDir() + L"/.k");

			// TODO (igor#1#): Remove archive password
			wxString MasterPath = Get_MasterDB();

			sqlite3 *Master;
			IsEncryptedDB(MasterPath, wxEmptyString, GetKey(true));
			#ifdef __OPENSUSE__
        if(sqlite3_open(MasterPath.c_str(), &Master) == SQLITE_OK){
			#else
        if(sqlite3_open(MasterPath, &Master) == SQLITE_OK){
      #endif // __OPENSUSE__
				wxRemoveFile(GetUserLocalDir() + L"/.a");
				sqlite3_close(Master);
			}

		}
	}
#endif // _OMB_USE_CIPHER

void ombMainFrame::ExitClick(wxCommandEvent& event){
	Close(false);}

void ombMainFrame::FundOperation(int Op){
	if(Op==0||Op>3)return;
	int i,Ch=0;
	int search_complete = 0x00;	// equals 3 when selected fund and default fund are found
	unsigned int j;
	double Def = -1, Tot/*=-1*/, NFon, Fon = -1;
	if(Op > 1 && Data->NFun < 1){
	if(Op==2)Error(8, wxEmptyString);
	else Error(9, wxEmptyString);
	return;}
	OPF = new TOPF(this);
	OPF->InitLabels(Op+1);
	if(Op>1){
		for(i = 0; i < Data->NFun; i++)
			if(Data->Funds[i].Name != Data->FileData.DefFund)
				OPF->Name->Append(Data->Funds[i].Name);
		if(Op==3)
			for(j=0;j<OPF->Name->GetCount();j++)
				for(int k = 0; k < Data->NFun; k++)
					if(OPF->Name->GetString(j)==Data->Funds[k].Name){
					  OPF->AddValue(FormDigits(Data->Funds[k].Value));
						break;}
		OPF->Name->Select(0);
		wxCommandEvent evt(wxEVT_NULL,0);
		if(Op == 3) OPF->NameChange(evt);}

	if(OPF->ShowModal() == wxID_OK){
		if(Op!=2)OPF->Val->GetValue().ToDouble(&NFon);
		Tot = Data->GetTot(tvFou);

		// Scan existing founds
		for(i = 0; i < Data->NFun; i++){
			if(Data->Funds[i].Name.CmpNoCase(OPF->Name->GetValue())==0){
				Fon=Data->Funds[i].Value;
				Ch=1;
				search_complete += 0x01;}
			else if(Data->Funds[i].Name.CmpNoCase(Data->FileData.DefFund)==0){
				Def=Data->Funds[i].Value;
				search_complete += 0x02;}
			if(search_complete == 0x03) break;}

		if((Fon == -1 || Tot == -1) && Op > 2){
			Error(10,wxEmptyString);
			goto FundOperationEnd;}
		switch(Op){
			case 1:
				if(Ch==1){
					Error(11,OPF->Name->GetValue());
					goto FundOperationEnd;}
				if(NFon>Def){
					Error(12,wxEmptyString);
					goto FundOperationEnd;}
				Data->SetDefaultFundValue(Def - NFon);
				if(!Data->AddValue(tvFou, -1, OPF->Name->GetValue(),NFon)){
					Error(5,wxEmptyString);
					goto FundOperationEnd;}
				break;
			case 2:
				Data->SetDefaultFundValue(Def + Fon);
				for(i = 0; i < Data->NFun; i++)
					if(Data->Funds[i].Name.CmpNoCase(OPF->Name->GetValue()) == 0){
					Data->DelValue(tvFou, Data->Funds[i].Id);
					break;}
				break;
			case 3:
				if(NFon < Fon) Data->SetDefaultFundValue(Def + Fon - NFon);
				else{
					if(Tot < (NFon - Fon)){
						Error(13, wxEmptyString);
						goto FundOperationEnd;}
					if(Def < (NFon - Fon)){
						Error(24, wxEmptyString);
						goto FundOperationEnd;}
					Data->SetDefaultFundValue(Def - NFon + Fon);}
				for(i = 0; i < Data->NFun; i++) if(Data->Funds[i].Name.CmpNoCase(OPF->Name->GetValue()) == 0){
					Data->ChangeFundValue(tvFou, Data->Funds[i].Id, NFon);
					break;}
				Data->FileData.Modified = true;}
		ShowF();}
	FundOperationEnd:
	delete OPF;
	UpdateMenu();}

void ombMainFrame::NewFundClick(wxCommandEvent& event){
	FundOperation(1);}

void ombMainFrame::RemoveFundClick(wxCommandEvent& event){
	FundOperation(2);}

void ombMainFrame::ResetFundClick(wxCommandEvent& event){
	FundOperation(3);}

void ombMainFrame::SetTotalClick(wxCommandEvent& event){
	int i,IP=-1;
	double OldTot,Tot,Pre=-1; // OldTot: previous total value
										// Tot: new total value
										// Pre: Default fund value
	OldTot=Data->GetTot(tvFou);
	if(OldTot==-1){ // This condition should never happen
		Error(5,wxEmptyString);
		return;}
	/*TOPF **/OPF = new TOPF(this);
	OPF->InitLabels(1);
  #ifdef __WXMSW__
    OPF->Val->SetValue(::wxString::Format("%#.2hf",OldTot));
  #else
  	// FIXME (igor#1#): Textbox value is not updated!
    OPF->Val->SetValue(::wxString::Format(L"%#.2hf",OldTot));
  #endif //__WXMSW__
	/*
	if(event.GetString()==L"MainFrame"){
		OPF->btnSizer->Show(1,false);
		OPF->OKBtn->Centre(wxHORIZONTAL);}
	*/
	OPF->Layout();
	//Show:
	if(OPF->ShowModal() == wxID_OK){
		/* This was until v 3.0.2 with != wxID_OK
		if(event.GetString()==L"MainFrame"){
			wxBell();
			goto Show;}
		else goto EndSetTotal1;}
		*/
		OPF->Val->GetValue().ToDouble(&Tot);
		if(OldTot > Tot)
			Error(17,wxEmptyString);
		else{
			for(i = 0; i < Data->NFun; i++)
				if(Data->Funds[i].Name.CmpNoCase(Data->FileData.DefFund) == 0){
				Pre=Data->Funds[i].Value;
				IP=i;
				break;}
			if(IP>-1) Data->ChangeFundValue(tvFou, Data->Funds[IP].Id, Pre + Tot - OldTot);
			else{
				wxString def=_("Default");
				Data->AddValue(tvFou, -1, def,Tot);
				Data->SetDefaultFund(def);
			}
			Data->FileData.Modified=true;
			ShowF();
			UpdateMenu();
		}
	}
	delete OPF;
}

void ombMainFrame::DefaultFundClick(wxCommandEvent& event){
	int i;
	/*TOPF **/OPF = new TOPF(this);
	OPF->InitLabels(5);
	for(i = 0; i < Data->NFun; i++)
		OPF->Name->Append(Data->Funds[i].Name);
	for(i=0;i<(int)OPF->Name->GetCount();i++)if(OPF->Name->GetString(i)==Data->FileData.DefFund){
		OPF->Name->SetSelection(i);
		break;}
	if(OPF->ShowModal() == wxID_OK) Data->SetDefaultFund(OPF->Name->GetValue());
	delete OPF;
}

void ombMainFrame::Operation(int Op){
	int i;
	if(Op == 0 || Op > 2)return;
	wxDateTime H;
	TOperationF *OperationF=new TOperationF(this);
	if(Op==1){
		OperationF->InitLabels(0);
		if(Data->UpdateMatters(toGain)){
			for(i=0;i<(int)Data->MattersBuffer->Count();i++)OperationF->Matter->Append(Data->MattersBuffer->Item(i));
			OperationF->Matter->AutoComplete(OperationF->Matter->GetStrings());
		}}
	else{
		OperationF->InitLabels(1);
		if(Data->UpdateMatters(toExpe)){
			for(i=0;i<(int)Data->MattersBuffer->Count();i++)OperationF->Matter->Append(Data->MattersBuffer->Item(i));
			OperationF->Matter->AutoComplete(OperationF->Matter->GetStrings());
		}}
	for(i = 0; i < Data->NFun; i++)
		OperationF->FundName->Append(Data->Funds[i].Name);
	for(i=0;i<(int)OperationF->FundName->GetCount();i++)if(OperationF->FundName->GetString(i).CmpNoCase(Data->FileData.DefFund)==0){
		OperationF->FundName->SetSelection(i);
		break;}
	OperationF->Category->Append("  -");
	for(int j = 0; j < Data->NCat; j++) OperationF->Category->Append(Data->Categories[j].Name);
	OperationF->Category->SetSelection(0);

	if(OperationF->ShowModal() == wxID_OK){
		double Fon = -1, Tot = -1, Val;
		OperationF->OpValue->GetValue().ToDouble(&Val);

		double CurrencyRate;
		if(! OperationF->CurrencyCheckbox->IsChecked()){
			OperationF->Rate->GetValue().ToDouble(&CurrencyRate);
			Val *= CurrencyRate;
		}

		wxString Fund;
		wxString SelFund = OperationF->FundName->GetString(OperationF->FundName->GetSelection());
		for(i = 0; i < Data->NFun; i++)
			if(Data->Funds[i].Name.CmpNoCase(SelFund) == 0){
			Fund=Data->Funds[i].Name;
			Fon=Data->Funds[i].Value;
			break;}
		Tot=Data->GetTot(tvFou);
		if(Fon==-1||Tot==-1)
			Error(10,wxEmptyString);
		else{
			int Cat;
			if(OperationF->SysTimeCheck->IsChecked()) H = wxDateTime::Now();
			else H=Data->Hour;

			Cat = Data->Categories[OperationF->Category->GetSelection() - 1].Id;

			wxString MatValue;
			switch(Op){
				case 1:
					MatValue=OperationF->Matter->GetValue();
					if(OperationF->CurrencyCheckbox->IsChecked())
						Data->AddOper(-1, Data->Day, H, toGain,
														Data->ombFromCDouble(Val)
													, MatValue, Cat, -1);
					else
						Data->AddOper(-1, Data->Day, H, toGain,
														Data->ombFromCDouble(Val)
													, MatValue, Cat, -1,

													false, ombInvalidLatitude, ombInvalidLongitude,
													1, CurrencyRate, OperationF->Currency->GetValue()

													);
					for(i = 0; i < Data->NFun; i++)
						if(Data->Funds[i].Name == Fund){
						Data->Funds[i].Value += Val;
						Data->ChangeFundValue(tvFou, Data->Funds[i].Id, Data->Funds[i].Value);
						break;}
					break;
				case 2:
					if(Val > Fon)
						Error(13, wxEmptyString);
					else{
						MatValue=OperationF->Matter->GetValue();
						if(OperationF->CurrencyCheckbox->IsChecked())
							Data->AddOper(-1, Data->Day, H, toExpe,
															Data->ombFromCDouble(Val)
														, MatValue, Cat, -1);
						else
							Data->AddOper(-1, Data->Day, H, toExpe,
															Data->ombFromCDouble(Val)
														, MatValue, Cat, -1,

														false, ombInvalidLatitude, ombInvalidLongitude,
														1, CurrencyRate, OperationF->Currency->GetValue()

														);
						for(i = 0; i < Data->NFun; i++)
							if(Data->Funds[i].Name == Fund){
								Data->Funds[i].Value -= Val;
								Data->ChangeFundValue(tvFou, Data->Funds[i].Id, Data->Funds[i].Value);
								break;}
						if((Fon-Val==0)&&Fund!=Data->FileData.DefFund){
							wxString Msg=wxString::Format(_("Fund \"%s\" is exhausted. Do you want to delete it?"),OperationF->FundName->GetValue().c_str());
							wxBell();
							if(wxMessageBox(Msg,_("OpenMoneyBox"),wxYES_NO)==wxYES)
								for(/*int*/ i = 0; i < Data->NFun; i++)
									if(Data->Funds[i].Name == Fund){
									Data->DelValue(tvFou,i);
									break;}}
						remove_obtained_shopitem(MatValue.MakeLower());
				}
			}
			ShowF();
		}
	}
	delete OperationF;
	UpdateMenu();}

void ombMainFrame::GainClick(wxCommandEvent& event){
	Operation(1);}

void ombMainFrame::ExpenseClick(wxCommandEvent& event){
	Operation(2);}

void ombMainFrame::CategoryClick(wxCommandEvent& event){
	int Row = GetFirstSelectedRow();
	bool found;
	int i;
	if(Row < 0) return;	// Error
	if(Data->IsDate(Row))return;

	TOperationF *category = new TOperationF(this);
	category->InitLabels(2);
	category->Category->Append("  -");
	for(i = 0; i < Data->NCat; i++) category->Category->Append(Data->Categories[i].Name);

	found = false;
	wxString cat = wxEmptyString;
	for(i = 0; i < Data->NCat; i++) if(Data->Categories[i].Id == Data->Lines[Row].CategoryIndex){
		cat = Data->Categories[i].Name;
		found = true;
		break;}
	if(found){
		for(i = 1; i < (int) category->Category->GetCount(); i++) if(category->Category->GetString(i) == cat){
			category->Category->SetSelection(i);
			break;}}
	else category->Category->SetSelection(0);

	if(category->ShowModal() == wxID_OK){
		int cat_index = -1;
		for(i = 0; i < Data->NCat; i++) if(Data->Categories[i].Name == category->Category->GetValue()){
			cat_index = Data->Categories[i].Id;
			break;}
		Data->ChangeTransactionCategory(Data->Lines[Row].Id, cat_index);
		Data->FileData.Modified=true;
		ShowF(false);
		Report->SelectRow(Row,false);}
	delete category;}

void ombMainFrame::NewCreditClick(wxCommandEvent& event){
	wxDateTime S;
	TSCredDeb *SCredDeb=new TSCredDeb(this);
	SCredDeb->InitLabels(false);
	if(SCredDeb->ShowModal() == wxID_OK){
		int i;
		double Def = -1, Tot = -1, Tot1 = -1, Val;	// Def : default fund value
																								// Tot : total fund value
																								// Tot1: credit total value
																								// Val : new credit value
		SCredDeb->Val->GetValue().ToDouble(&Val);
		for(i = 0; i < Data->NFun; i++)
			if(Data->Funds[i].Name.CmpNoCase(Data->FileData.DefFund)==0){
				Def=Data->Funds[i].Value;
				break;}
		Tot=Data->GetTot(tvFou);
		Tot1=Data->GetTot(tvCre);
		if((Def==-1)||(Tot==-1)||(Tot1==-1)){
			Error(18,wxEmptyString);
			delete SCredDeb;
			return;
		}
		if(Def<Val){
				Error(19,wxEmptyString);
			delete SCredDeb;
			return;
		}
		Data->AddValue(tvCre, -1, SCredDeb->Name->GetValue(), Val, -1);
		if(! SCredDeb->OldItemCheck->IsChecked()){
			Data->SetDefaultFundValue(Def - Val);
			if(SCredDeb->SysTimeCheck->IsChecked()) S = wxDateTime::Now();
			else S=Data->Hour;
			Data->AddOper(-1, Data->Day, S, toSetCre,
															Data->ombFromCDouble(Val)
														, SCredDeb->Name->GetValue(), -1, -1);
		}
		ShowF();
		UpdateMenu();
	}
	delete SCredDeb;}

void ombMainFrame::RemoveCreditClick(wxCommandEvent& event){
	int i;
	wxDateTime S;
	if(Data->NCre < 1){
		Error(20,wxEmptyString);
		return;}
	/*TRCredDeb **/RCredDeb = new TRCredDeb(this);
	RCredDeb->InitLabels(1);
	for(i = 0; i < Data->NCre; i++)RCredDeb->Name->Append(Data->Credits[i].Name);
	RCredDeb->Name->SetSelection(0);
	if(RCredDeb->ShowModal() == wxID_OK){
		int IDef = -1, IExi = -1;
		double Def = -1, Tot = -1, Fou = -1, Tot1 = -1,	// Pre : default fund value
																										// Tot : fund total value
																										// Tot1: total credit value
																										// Fou : credit value
					cIndex = -1;															// cIndex: credit contact index
		if(RCredDeb->SysTimeCheck->IsChecked()) S = wxDateTime::Now();
		else S=Data->Hour;
		for(i = 0; i < Data->NFun; i++)
			if(Data->Funds[i].Name.CmpNoCase(Data->FileData.DefFund)==0){
				IDef=i;
				Def=Data->Funds[i].Value;
				break;}
		Tot=Data->GetTot(tvFou);
		for(i = 0; i < Data->NCre; i++)
			if(Data->Credits[i].Name.CmpNoCase(RCredDeb->Name->GetValue())==0){
				IExi=i;
				Fou=Data->Credits[i].Value;
				cIndex = Data->Credits[i].ContactIndex;
				break;}
		Tot1=Data->GetTot(tvCre);
		if((Def==-1)||(Tot==-1)||(Fou==-1)||(Tot1==-1))
			Error(18,wxEmptyString);
		else{
			if(RCredDeb->Opt->GetSelection()==0){
				Data->AddOper(-1, Data->Day, S, toRemCre,
															Data->ombFromCDouble(Fou)
														, RCredDeb->Name->GetValue(), -1, cIndex);
				Data->DelValue(tvCre, Data->Credits[IExi].Id);
				Data->Funds[IDef].Value += Fou;
				Data->SetDefaultFundValue(Data->Funds[IDef].Value);}
			else{
				double Par;
				RCredDeb->Val->GetValue().ToDouble(&Par);
				if(Par>Fou)
					Error(21, _("credit"));
				else{
					Data->AddOper(-1, Data->Day, S, toRemCre,
																Data->ombFromCDouble(Par)
															, RCredDeb->Name->GetValue(), -1, cIndex);
					Data->Credits[IExi].Value -= Par;
					Data->ChangeFundValue(tvCre, Data->Credits[IExi].Id, Data->Credits[IExi].Value);
					Data->Funds[IDef].Value += Par;
					Data->SetDefaultFundValue(Data->Funds[IDef].Value);
				}
			}
			ShowF();
			UpdateMenu();
		}
	}
	delete RCredDeb;}

void ombMainFrame::RemitCreditClick(wxCommandEvent& event){
	int i;
	wxDateTime S;
	if(Data->NCre < 1){
		Error(20,wxEmptyString);
		return;}
	/*TRCredDeb **/RCredDeb = new TRCredDeb(this);
	RCredDeb->InitLabels(3);
	for(i = 0; i < Data->NCre; i++)RCredDeb->Name->Append(Data->Credits[i].Name);
	RCredDeb->Name->SetSelection(0);
	if(RCredDeb->ShowModal() == wxID_OK){
		int ICre;
		double Cre = -1, Tot = -1, Val, // Cre: Old credit value
																		// Tot: Total credit value
																		// Val: Value of credit to remit
					cIndex;										// cIndex: credit contact index
		if(RCredDeb->SysTimeCheck->IsChecked()) S = wxDateTime::Now();
		else S=Data->Hour;
		for(i = 0; i < Data->NCre; i++)
			if(Data->Credits[i].Name.CmpNoCase(RCredDeb->Name->GetValue())==0){
				ICre=i;
				Cre=Data->Credits[i].Value;
				cIndex = Data->Credits[i].ContactIndex;
				break;}
		Tot=Data->GetTot(tvCre);
		if(Cre==-1||Tot==-1)
			Error(10,wxEmptyString);
		else{
			if(RCredDeb->Opt->GetSelection()==0){
				Data->AddOper(-1, Data->Day, S, toConCre,
														Data->ombFromCDouble(Cre)
													, RCredDeb->Name->GetValue(), -1, cIndex);
				Data->DelValue(tvCre, Data->Credits[ICre].Id);}
			else{
				RCredDeb->Val->GetValue().ToDouble(&Val);
				if(Val>Cre)
					Error(21, _("credit"));
				else{
					Data->AddOper(-1, Data->Day, S, toConCre,
															Data->ombFromCDouble(Val)
														, RCredDeb->Name->GetValue(), -1, cIndex);
					Data->Credits[ICre].Value -= Val;
					Data->ChangeFundValue(tvCre, Data->Credits[ICre].Id, Data->Credits[ICre].Value);
				}
			}
			ShowF();
			UpdateMenu();
		}
	}
	delete RCredDeb;}

void ombMainFrame::NewDebtClick(wxCommandEvent& event){
	wxDateTime S;
	TSCredDeb *SCredDeb=new TSCredDeb(this);
	SCredDeb->InitLabels(true);
	if(SCredDeb->ShowModal() == wxID_OK){
		int i;
		double Def = -1, Tot = -1, Tot1 = -1,Val; // Def : default fund value
																							// Tot : total fund value
																							// Tot1: debt total value
																							// Val : new debt value
		SCredDeb->Val->GetValue().ToDouble(&Val);
		for(i = 0; i < Data->NFun; i++)
			if(Data->Funds[i].Name.CmpNoCase(Data->FileData.DefFund)==0){
				Def=Data->Funds[i].Value;
				break;}
		Tot=Data->GetTot(tvFou);
		Tot1=Data->GetTot(tvDeb);
		if((Def==-1)||(Tot==-1)||(Tot1==-1)){
			Error(18,wxEmptyString);
			delete SCredDeb;
			return;
		}
		Data->AddValue(tvDeb, -1, SCredDeb->Name->GetValue(), Val, -1);
		if(! SCredDeb->OldItemCheck->IsChecked()){
			Data->SetDefaultFundValue(Def + Val);
			if(SCredDeb->SysTimeCheck->IsChecked()) S = wxDateTime::Now();
			else S=Data->Hour;
			Data->AddOper(-1, Data->Day, S, toSetDeb,
															Data->ombFromCDouble(Val)
														, SCredDeb->Name->GetValue(), -1, -1);
		}
		ShowF();
		UpdateMenu();
	}
	delete SCredDeb;}

void ombMainFrame::RemoveDebtClick(wxCommandEvent& event){
  int i;
	wxDateTime S;
	if(Data->NDeb < 1){
		Error(22,wxEmptyString);
    return;}
	/*TRCredDeb **/RCredDeb = new TRCredDeb(this);
	RCredDeb->InitLabels(2);
	for(i = 0; i < Data->NDeb; i++)RCredDeb->Name->Append(Data->Debts[i].Name);
	RCredDeb->Name->SetSelection(0);
	if(RCredDeb->ShowModal() == wxID_OK){
		int IDef = -1, IExi = -1;
		double Def = -1, Tot = -1, Fou = -1, Tot1 = -1,	// Def : Default fund value
																										// Tot : Total fund value
																										// Fou : Debt value
																										// Tot1: Debt total value
					cIndex = -1;															// cIndex: debt contact index
		if(RCredDeb->SysTimeCheck->IsChecked()) S = wxDateTime::Now();
		else S=Data->Hour;
		for(i = 0; i < Data->NFun; i++)
			if(Data->Funds[i].Name.CmpNoCase(Data->FileData.DefFund)==0){
			IDef=i;
			Def=Data->Funds[i].Value;
			break;}
		Tot=Data->GetTot(tvFou);
		for(i = 0; i < Data->NDeb; i++)if(Data->Debts[i].Name.CmpNoCase(RCredDeb->Name->GetValue())==0){
			IExi=i;
			Fou=Data->Debts[i].Value;
			cIndex = Data->Debts[i].ContactIndex;
			break;}
		Tot1=Data->GetTot(tvDeb);
		if((Def==-1)||(Tot==-1)||(Fou==-1)||(Tot1==-1))
			Error(18,wxEmptyString);
		else{
			if(RCredDeb->Opt->GetSelection()==0){
				if(Fou>Def)
					Error(24,wxEmptyString);
				else{
					Data->AddOper(-1, Data->Day, S, toRemDeb,
																Data->ombFromCDouble(Fou)
															, RCredDeb->Name->GetValue(), -1, cIndex);
					Data->DelValue(tvDeb, Data->Debts[IExi].Id);
					Data->Funds[IDef].Value -= Fou;
					Data->SetDefaultFundValue(Data->Funds[IDef].Value);
				}
			}
			else{
				double Par;
				RCredDeb->Val->GetValue().ToDouble(&Par);
				if(Par>Fou)
					Error(21, _("debit"));
				else{
					if(Par>Def)
						Error(24,wxEmptyString);
					else{
						Data->AddOper(-1, Data->Day, S, toRemDeb,
																	Data->ombFromCDouble(Par)
																, RCredDeb->Name->GetValue(), -1, cIndex);
						Data->Debts[IExi].Value -= Par;
						Data->ChangeFundValue(tvDeb, Data->Debts[IExi].Id, Data->Debts[IExi].Value);
						Data->Funds[IDef].Value -= Par;
						Data->SetDefaultFundValue(Data->Funds[IDef].Value);
					}
				}
			}
			ShowF();
			UpdateMenu();
		}
	}
	delete RCredDeb;}

void ombMainFrame::RemitDebtClick(wxCommandEvent& event){
  int i;
	wxDateTime S;
  if(Data->NDeb < 1){
		Error(22,wxEmptyString);
    return;}
	/*TRCredDeb **/RCredDeb = new TRCredDeb(this);
  RCredDeb->InitLabels(4);
	for(i = 0; i < Data->NDeb; i++)RCredDeb->Name->Append(Data->Debts[i].Name);
	RCredDeb->Name->SetSelection(0);
	if(RCredDeb->ShowModal() == wxID_OK){
		int IDeb;
		double Deb = -1, Tot = -1, Val,	// Deb: Old debt value
																		// Tot: Total debt value
																		// Val: Value of debt to remit
					cIndex;										// cIndex: debt contact index
		if(RCredDeb->SysTimeCheck->IsChecked()) S = wxDateTime::Now();
		else S=Data->Hour;
		for(i = 0; i < Data->NDeb; i++)if(Data->Debts[i].Name.CmpNoCase(RCredDeb->Name->GetValue())==0){
			IDeb=i;
			Deb=Data->Debts[i].Value;
			cIndex = Data->Debts[i].ContactIndex;
			break;}
		Tot=Data->GetTot(tvDeb);
		if(Deb==-1||Tot==-1)
			Error(10,wxEmptyString);
		else{
			if(RCredDeb->Opt->GetSelection()==0){
				Data->AddOper(-1, Data->Day, S, toConDeb,
														Data->ombFromCDouble(Deb)
													, RCredDeb->Name->GetValue(), -1, cIndex);
				Data->DelValue(tvDeb, Data->Debts[IDeb].Id);}
			else{
				RCredDeb->Val->GetValue().ToDouble(&Val);
				if(Val>Deb)
					Error(21, _("debit"));
				else{
					Data->AddOper(-1, Data->Day, S, toConDeb,
															Data->ombFromCDouble(Val)
														, RCredDeb->Name->GetValue(), -1, cIndex);
					Data->Debts[IDeb].Value-=Val;
					Data->ChangeFundValue(tvDeb, Data->Debts[IDeb].Id, Data->Debts[IDeb].Value);
				}
			}
			ShowF();
			UpdateMenu();
		}
	}
	delete RCredDeb;}

void ombMainFrame::ObjectOperation(int Op){
	if(Op<1||Op>4)return;
	wxDateTime S;
	/*TObjects **/Objects = new TObjects(this);
	Objects->InitLabels(Op);
	if(Op<3)Objects->HideAlarm();
	if(Objects->ShowModal() == wxID_OK){
		if(Objects->SysTimeCheck->IsChecked()) S = wxDateTime::Now();
		else S=Data->Hour;
		switch(Op){
			case 1:
				Data->AddOper(-1, Data->Day, S, toGetObj, Objects->Obj->GetValue(), Objects->Per->GetValue(), -1, -1);
				remove_obtained_shopitem(Objects->Obj->GetValue().MakeLower());
				break;
			case 2:
				Data->AddOper(-1, Data->Day, S, toGivObj, Objects->Obj->GetValue(), Objects->Per->GetValue(), -1, -1);
				break;
			default:
				wxDateTime Alm;
				if(Objects->AlmCB->IsChecked())Alm=(Objects->Alarm->GetValue());
				else Alm=wxDateTime::Today().Add(wxDateSpan(100,0,0,0));;
				if(Op==3){
					Data->AddObject(toPre, -1, Objects->Per->GetValue(), Objects->Obj->GetValue(), Alm, -1);
					Data->AddOper(-1, Data->Day, S, toLenObj, Objects->Obj->GetValue(), Objects->Per->GetValue(), -1, -1);}
				else if(Op==4){
					Data->AddObject(toInP, -1, Objects->Per->GetValue(), Objects->Obj->GetValue(), Alm, -1);
					Data->AddOper(-1, Data->Day, S, toBorObj, Objects->Obj->GetValue(), Objects->Per->GetValue(), -1, -1);}}
		Data->FileData.Modified=true;
		ShowF(false);
		UpdateMenu();}
	delete Objects;}

void ombMainFrame::ReceivedClick(wxCommandEvent& event){
	ObjectOperation(1);}

void ombMainFrame::GiftedClick(wxCommandEvent& event){
	ObjectOperation(2);}

void ombMainFrame::LendClick(wxCommandEvent& event){
	ObjectOperation(3);}

void ombMainFrame::BorrowClick(wxCommandEvent& event){
	ObjectOperation(4);}

void ombMainFrame::ShopOperation(int O){
	if(O < 1 || O > 2)return;
	if(O == 2 && Data->NSho < 1){
		Error(16, wxEmptyString);
		return;}
	int i;
	/*TObjects **/Objects = new TObjects(this);
	Objects->InitLabels(6);
	if(O == 2) Objects->HideAlarm();
	//wxDateTime today=wxDateTime::Today();
	wxDateTime Alm;
	switch(O){
		case 1:
			Objects->InitLabels(7);
			break;
		case 2:
			Objects->InitLabels(8);
			for(i = 0; i < Data->NSho; i++)Objects->Obj->Append(Data->ShopItems[i].Name);
			i = ShopList->GetSelection();
			if(i == wxNOT_FOUND) Objects->Obj->SetSelection(0);
			else Objects->Obj->SetSelection(i);}
	if(Objects->ShowModal() == wxID_OK){
		bool new_item;
		wxString item = Objects->Obj->GetValue();
		switch(O){
			case 1:
				new_item = true;
				for(i = 0; i < Data->NSho; i++) if(Data->ShopItems[i].Name.CmpNoCase(item) == 0){
					Error(48, item);
					new_item = false;
					break;}
				if(new_item){
					if(Objects->AlmCB->IsChecked()) Alm = Objects->Alarm->GetValue();
					else Alm = wxDateTime::Today().Add(wxDateSpan(100, 0, 0, 0));
					Data->AddShopItem(-1, item, Alm);}
				break;
			case 2:
				for(i = 0; i < Data->NSho; i++)
				if(Data->ShopItems[i].Name.CmpNoCase(item) == 0){
					Data->DelShopItem(Data->ShopItems[i].Id);
					break;}}
		ShowF(false);}
	//EndShopOperation:
	delete Objects;
	UpdateMenu();}

void ombMainFrame::AddShopItemClick(wxCommandEvent& event){
	ShopOperation(1);}

void ombMainFrame::DelShopItemClick(wxCommandEvent& event){
	ShopOperation(2);}

void ombMainFrame::GetBackClick(wxCommandEvent& event){
	ObjectOperation2(1);}

void ombMainFrame::GiveBackClick(wxCommandEvent& event){
	ObjectOperation2(2);}

void ombMainFrame::ObjectOperation2(int Op){
	int i;
	if(Op<1||Op>2)return;
	bool Ex;
	wxDateTime S;
	switch(Op){
		case 1:
			if(Data->NLen == 0){
				Error(14,wxEmptyString);
				return;}
			break;
		case 2:
			if(Data->NBor == 0){
				Error(15,wxEmptyString);
				return;}
			break;
		default:
			return;}
	TGetBackF *GetBackF=new TGetBackF(this);
	if(Op==1)GetBackF->InitLabels(false);
	else GetBackF->InitLabels(true);
	switch(Op){
		case 1:
			for(i = 0; i < Data->NLen; i++){
				Ex=false;
				for(unsigned int j=0;j<GetBackF->Per->GetCount();j++)if(GetBackF->Per->GetString(j)==Data->Lent[i].Name){
					Ex=true;
					break;}
				if(!Ex)GetBackF->Per->Append(Data->Lent[i].Name);
				GetBackF->Memo->Add(Data->Lent[i].Name+L"|"+Data->Lent[i].Object,1);}
			break;
		case 2:
			for(i = 0; i < Data->NBor; i++){
				Ex=false;
				for(unsigned int j=0;j<GetBackF->Per->GetCount();j++)if(GetBackF->Per->GetString(j)==Data->Borrowed[i].Name){
					Ex=true;
					break;}
				if(!Ex)GetBackF->Per->Append(Data->Borrowed[i].Name);
				GetBackF->Memo->Add(Data->Borrowed[i].Name+L"|"+Data->Borrowed[i].Object);}}
	GetBackF->Per->SetSelection(0);
	wxCommandEvent evt(wxEVT_NULL,0);
	GetBackF->PerChange(evt);
	if(GetBackF->ShowModal() == wxID_OK){
		double cIndex = -1;	// Object contact index
		if(GetBackF->SysTimeCheck->IsChecked()) S = wxDateTime::Now();
		else S=Data->Hour;
		switch(Op){
			case 1:
				for(i = 0; i < Data->NLen; i++)if(Data->Lent[i].Name == GetBackF->Per->GetValue() && Data->Lent[i].Object == GetBackF->Obj->GetValue()){
						cIndex = Data->Lent[i].ContactIndex;
						Data->DelObject(toPre, Data->Lent[i].Id);
						break;}
				Data->AddOper(-1, Data->Day, S, toBakObj, GetBackF->Obj->GetValue(), GetBackF->Per->GetValue(), -1, cIndex);
				break;
			case 2:
				for(i = 0; i < Data->NBor; i++)
					if(Data->Borrowed[i].Name == GetBackF->Per->GetValue() && Data->Borrowed[i].Object == GetBackF->Obj->GetValue()){
						cIndex = Data->Borrowed[i].ContactIndex;
						Data->DelObject(toInP, Data->Borrowed[i].Id);
						break;}
				Data->AddOper(-1, Data->Day, S, toRetObj, GetBackF->Obj->GetValue(), GetBackF->Per->GetValue(), -1, cIndex);}
		Data->FileData.Modified=true;
		ShowF(false);}
	delete GetBackF;}

void ombMainFrame::ReportSelect(wxGridEvent& event){
	if(Data->NLin < 1)return;
	int Row = event.GetRow();
	bool IsDate = Data->IsDate(Row);
	if(IsDate) Report->SelectRow(Row);
	else{
		int Col = event.GetCol();
		if (Col != 5) Report->SelectRow(Row);
		else event.Skip();
	}
	UpdateMenu();
}

void ombMainFrame::ReportRangeSelect(wxGridRangeSelectEvent& event){
  wxGridCellCoordsArray selTop=Report->GetSelectionBlockTopLeft();
  wxGridCellCoordsArray selBottom=Report->GetSelectionBlockBottomRight();
  for(unsigned int i=0;i<selTop.Count();i++ )Report->ClearSelection();}

int ombMainFrame::GetFirstSelectedRow(void){
	wxArrayInt rows = Report->GetSelectedRows();
	if(rows.GetCount() == 0){
		// Error
		return -1;
	}
	return rows.Item(0);
}

void ombMainFrame::CopyClick(wxCommandEvent& event){
	int r=GetFirstSelectedRow();
	if(r>=0){
		double cur;
		// Date copy
    #ifdef __WXMSW__
		wxString Clip=Data->Lines[r].Date.Format("%d/%m/%Y",wxDateTime::Local)+L" ";
    #else
		wxString Clip=Data->Lines[r].Date.Format(L"%d/%m/%Y",wxDateTime::Local)+L" ";
    #endif // __WXMSW__
		wxArrayString *CB=new wxArrayString();
		CB->Clear();
		if(Data->Lines[r].IsDate){ // The line contains a date record
			CB->Add(Clip);
			// Total Copy
			Clip = GetCurrencySymbol();
			Clip += char(32);
			Data->Lines[r].Value.ToDouble(&cur);
			Clip += FormDigits(cur);
			Clip = Clip + L" (" + GetLogString(0) + L")";
			CB->Add(Clip);}
		else{ // The line contains an operation
			// Time copy
      #ifdef __WXMSW__
        Clip=Clip+Data->Lines[r].Time.Format("%H:%M",wxDateTime::Local)+" ";
      #else
        Clip=Clip+Data->Lines[r].Time.Format(L"%H:%M",wxDateTime::Local)+L" ";
      #endif // __WXMSW__
			// Operation type copy
			Clip=Clip+GetLogString(Data->Lines[r].Type)+L": ";
			// Value copy
			if(Data->Lines[r].Type<9){
				Data->Lines[r].Value.ToDouble(&cur);
				Clip=Clip+GetCurrencySymbol()+L" "+FormDigits(cur);}
			else Clip=Clip+Data->Lines[r].Value;
			// Reason copy
			Clip=Clip+L" "+Data->Lines[r].Reason;
			CB->Add(Clip);}
		// Category copy
		if(Data->Lines[r].CategoryIndex != -1)
			for(int k = 0; k < Data->NCat; k++)if(Data->Categories[k].Id == Data->Lines[r].CategoryIndex){
				CB->Add(Data->Categories[k].Name);
				break;}
		Clip=wxEmptyString;
		for(unsigned int z=0;z<=CB->Count()-1;z++){
      if(z>0)Clip+=(L"\n");
      Clip+=CB->Item(z);}
		wxTextDataObject *text=new wxTextDataObject();
		text->SetText(Clip);
		wxTheClipboard->Open();
    wxTheClipboard->SetData(text);
		wxTheClipboard->Flush();
		wxTheClipboard->Close();
		//delete text;	// NOTE (igor#1#): Do not delete data owned by the clipboard, it would make te app crash!
		}}

void ombMainFrame::TextConvClick(wxCommandEvent& event){
	StatusBar->SetStatusText(_("Document conversion i progress..."),0);

	wxString Ch1,Ch2;
  wxString tmp_file = GetTempFile();
	Ch1=::wxFileName::CreateTempFileName(tmp_file);

	if(Ch1.IsEmpty()){
		Error(44,wxEmptyString);
		StatusBar->SetStatusText(wxEmptyString,0);
		::wxEndBusyCursor();
		return;}
	Ch2=::wxFileName::CreateTempFileName(tmp_file);
	if(Ch2.IsEmpty()){
		Error(44,wxEmptyString);
		StatusBar->SetStatusText(wxEmptyString,0);
		::wxEndBusyCursor();
		return;}

	wxBitmap bmp;
	#ifdef _OMB_CHART_WXPIECTRL
		bmp = FundChart->GetBitmap();
	#elif defined ( _OMB_CHART_MATLIBPLOT )
		//bmp = FundChart->GetBitmap();
		bmp = Fund_bmp;
	#endif // _OMB_CHART_WXPIECTRL
	bmp.SaveFile(Ch1, wxBITMAP_TYPE_PNG);

	#ifdef _OMB_CHART_WXPIECTRL
		bmp = TrendChart->m_dc_bmp;
	#elif defined ( _OMB_CHART_MATLIBPLOT )
		//bmp = TrendChart->GetBitmap();
		bmp = Trend_bmp;
	#endif // _OMB_CHART_WXPIECTRL
	bmp.SaveFile(Ch2, wxBITMAP_TYPE_PNG);

	if(Master_Attached && (! IsNewMonthShown())){
		wxDateTime cal_date = Calendar->GetDate();
		Data->XMLExport_archive(Ch1, Ch2, cal_date);
	}
	else Data->XMLExport(Ch1, Ch2);

	// Remove temporary chart files
	::wxRemoveFile(Ch1);
	::wxRemoveFile(Ch2);

	StatusBar->SetStatusText(wxEmptyString,0);}

#if _OMB_INSTALLEDCONV == 1
	void ombMainFrame::OldConv1Click(wxCommandEvent& event){
		wxFileDialog *Dialog=new wxFileDialog(wxTheApp->GetTopWindow(),wxEmptyString,wxEmptyString,wxEmptyString,wxEmptyString,wxFD_OPEN,wxDefaultPosition,wxDefaultSize,L"Dialog");
		Dialog->SetDirectory(GetOSDocDir());
		wxString filter = _("Bilancio documents");
		filter += L"|*.bil;*.BIL";
		Dialog->SetWildcard(filter);
		if(Dialog->ShowModal() != wxID_OK){
			delete Dialog;
			return;}

		wxString File = Dialog->GetPath();

		#ifdef _OMB_FORMAT31x
			#ifdef _OMB_USE_CIPHER
				if(CheckFileFormat(File, wxEmptyString) == ombFFORMAT_31){
			#else
				if(CheckFileFormat(File) == ombFFORMAT_31){
			#endif // _OMB_USE_CIPHER
				delete Dialog;
				return;}
		#endif // _OMB_FORMAT31x

		wxString Proc;
		#ifdef __WXGTK__
			Proc = Console + wxGetCwd() + L"/ombconvert \"" + File + L"\"";
			wxExecute(Proc, wxEXEC_SYNC);
		#elif defined (__WXMSW__)
			Proc = "\"" + GetInstallationPath() + L"\\ombconvert.exe\"";
			wxExecute(Proc, wxEXEC_SYNC | wxEXEC_SHOW_CONSOLE);
		#elif defined (__WXMAC__)
			Proc = L"./ombconvert";
			wxExecute(Proc, wxEXEC_SYNC | wxEXEC_SHOW_CONSOLE);
		#endif	// __WXGTK__

		delete Dialog;}
#endif // _OMB_INSTALLEDCONV

void ombMainFrame::OptionsClick(wxCommandEvent& event){
	if(ShowOptionsDialog(
												#ifdef _OMB_USE_CIPHER
													#ifdef _OMB_SQLCIPHER_v4
														Data->DatabaseIsSqlCipher_V4
													#endif // _OMB_SQLCIPHER_v4
												#endif // _OMB_USE_CIPHER
												)){
		ToolBar->Show(ShowBar());
		::wxBeginBusyCursor(wxHOURGLASS_CURSOR);
		StatusBar->SetStatusText(_("Field update in progress..."));
		RebuildTools();
    ShowF();
		StatusBar->SetStatusText(wxEmptyString);
		::wxEndBusyCursor();}}

void ombMainFrame::RebuildTools(void){
	int i;
	unsigned int j;
	#ifndef __OPENSUSE__
		wxMenuItemList::Node* node;
	#else
		wxMenuItemList::compatibility_iterator node;
	#endif // __OPENSUSE__
	wxMenuItem *item;
	wxMenuItemList sublist=MainMenu_ExtTools->GetMenuItems();
	for(j=0;j<sublist.GetCount();j++){
		node=sublist.Item(j);
		item=node->GetData();
		MainMenu_ExtTools->Delete(item);}
	for(i = 0; i < GetToolCount(); i++){
		item=new wxMenuItem(MainMenu_ExtTools, ombExtTool + i, GetTool(i), wxEmptyString, wxITEM_NORMAL);
		Connect(ombExtTool+i,wxEVT_COMMAND_MENU_SELECTED,wxCommandEventHandler(ombMainFrame::ExtClick));
		MainMenu_ExtTools->Append(item);}}

void ombMainFrame::ShowF(bool doChart){

  #ifndef NDEBUG
		ombLogMessage(L"ShowF routine started");
	#endif // NDEBUG

	int i;
	wxString Obj_Name;
	SetCaption();
	// TreeList construction
	ItemList->DeleteAllItems();
	ResetTree();

	if(ShowFundGroups()){
		wxSQLite3Table GroupTable, ChildTable;
		#ifdef _OMB_USE_CIPHER
			GroupTable = GetTable(Data->database, "select * from FundGroups where owner = '-1';");
		#else
			GroupTable = Data->database->GetTable(L"select * from FundGroups where owner = '-1';");
		#endif // _OMB_USE_CIPHER
		int GTC = GroupTable.GetRowCount();

		double GroupTotals[GTC];
		for( i = 0; i < GTC; i++)
			GroupTotals[i] = 0;
		wxString GroupNames[GTC];
		for(i = 0; i < GTC; i++){
			GroupTable.SetRow(i);
			GroupNames[i] = GroupTable.GetAsString("name");
		}

		#ifdef _OMB_USE_CIPHER
			ChildTable = GetTable(Data->database, "select * from FundGroups where owner > '-1';");
		#else
			ChildTable = Data->database->GetTable(L"select * from FundGroups where owner > '-1';");
		#endif // _OMB_USE_CIPHER
		int CTC = ChildTable.GetRowCount();

		int Group_ID;
		wxString FundName;
		for(i = 0; i < Data->NFun; i++){
			bool Found = false;
			FundName = Data->Funds[i].Name;
			for(int j = 0; j < CTC; j++){
				ChildTable.SetRow(j);
				if(FundName.CmpNoCase(ChildTable.GetAsString("name")) == 0){
					Group_ID = ChildTable.GetInt("owner", -1);
					for(int k = 0; k < GTC; k++){
						GroupTable.SetRow(k);

						if(GroupTable.GetInt("id", -1) == Group_ID){
							Found = true;
							GroupTotals[k] += Data->Funds[i].Value;
							break;
						}

					}

					break;
				}
			}
			if(! Found)
				AddNode(Funds_tid, Data->Funds[i].Name, FormDigits(Data->Funds[i].Value) + L" " + GetCurrencySymbol());
		}

		for(i = 0; i < GTC; i++){
			AddNode(Funds_tid, GroupNames[i] + " - [" + _("Group") + "]", FormDigits(GroupTotals[i]) + L" " + GetCurrencySymbol());
		}

	}
	else{

		for(i = 0; i < Data->NFun; i++)
			AddNode(Funds_tid, Data->Funds[i].Name, FormDigits(Data->Funds[i].Value) + L" " + GetCurrencySymbol());

	}

	// NOTE (igor#1#): Hidden feature
	#ifdef _OMB_USE_CIPHER
		double TotFundAttached = 0, attached_value;
		if(Data->ExternalFileAttached){
			wxSQLite3Table AttachedFundTable = Data->GetExternalFundTable();
			for(int j = 0; j < AttachedFundTable.GetRowCount(); j++){
				AttachedFundTable.SetRow(j);
				attached_value = AttachedFundTable.GetDouble(L"value", 0);
				AddNode(Funds_tid, AttachedFundTable.GetString(L"name", wxEmptyString), FormDigits(attached_value) + L" " + GetCurrencySymbol(), -1, true);
				TotFundAttached += attached_value;
			}
		}
	#endif // _OMB_USE_CIPHER
	// ------------------------------
	int c_id;
	for(i = 0; i < Data->NCre; i++){
		c_id = Data->Credits[i].ContactIndex;
		if(c_id > -1) c_id = GetTreeViewContactImageIndex(c_id);
		AddNode(Creds_tid, Data->Credits[i].Name, FormDigits(Data->Credits[i].Value) + L" " + GetCurrencySymbol(), c_id);
	}
	for(i = 0; i < Data->NDeb; i++){
		c_id = Data->Debts[i].ContactIndex;
		if(c_id > -1) c_id = GetTreeViewContactImageIndex(c_id);
		AddNode(Debts_tid, Data->Debts[i].Name, FormDigits(Data->Debts[i].Value) + L" " + GetCurrencySymbol(), c_id);
	}
	for(i = 0; i < Data->NLen; i++)
	{
		c_id = Data->Lent[i].ContactIndex;
		if(c_id > -1) c_id = GetTreeViewContactImageIndex(c_id);
		if((Data->Lent[i].Alarm.IsValid()) && (Data->Lent[i].Alarm < (wxDateTime::Today().Add(wxDateSpan(100,0,0,0)))))
			Obj_Name = Data->Lent[i].Object + L" (" + Data->Lent[i].Alarm.FormatDate() + L")";
		else
			Obj_Name = Data->Lent[i].Object;
		AddNode(Lends_tid, Data->Lent[i].Name, Obj_Name, c_id);
	}
	for(i=  0; i < Data->NBor; i++)
	{
		c_id = Data->Borrowed[i].ContactIndex;
		if(c_id > -1) c_id = GetTreeViewContactImageIndex(c_id);
		if((Data->Borrowed[i].Alarm.IsValid()) && (Data->Borrowed[i].Alarm < (wxDateTime::Today().Add(wxDateSpan(100,0,0,0)))))
			Obj_Name = Data->Borrowed[i].Object + L" (" + Data->Borrowed[i].Alarm.FormatDate() + L")";
		else
			Obj_Name = Data->Borrowed[i].Object;
		AddNode(Borrowed_tid, Data->Borrowed[i].Name, Obj_Name, c_id);
	}
	// Add totals to TreeList
	// NOTE (igor#1#): Hidden feature
	#ifdef _OMB_USE_CIPHER
		if(Data->ExternalFileAttached)
			AddNode(Funds_tid, TreeNames->Item(6), FormDigits(Data->GetTot(tvFou) + TotFundAttached) + L" " + GetCurrencySymbol());
		else
	#endif // _OMB_USE_CIPHER
	AddNode(Funds_tid,TreeNames->Item(6),FormDigits(Data->GetTot(tvFou)) + L" " + GetCurrencySymbol());
	if(Data->NCre > 0)
		AddNode(Creds_tid,TreeNames->Item(6),FormDigits(Data->GetTot(tvCre)) + L" " + GetCurrencySymbol());
	if(Data->NDeb > 0)
		AddNode(Debts_tid,TreeNames->Item(6),FormDigits(Data->GetTot(tvDeb)) + L" " + GetCurrencySymbol());

	// show report
	if(Report->GetNumberRows() > 0) Report->DeleteRows(0, Report->GetNumberRows(), false);

	Report->SetDefaultCellTextColour(backup_report_colour);

	Report->AppendRows(Data->NLin, false);

	wxFont font = Report->GetCellFont(0, 0);
	font.MakeBold();

	bool anyContactShown = false;
	bool anyLocationShown = false;

	struct Point{
		float lat;
		float lon;
	};

	struct Point point;
	FILE *fp;
	wxString MapFile;

	if(map_viewer_exist){
		fp = NULL;
		MapFile = L"/tmp/ombmapview.dat";
		if(wxFileExists(MapFile)) wxRemoveFile(MapFile);
	}

	// Build Category drop-down list of choices
	CategoryChoices.Clear();
	CategoryChoices.Add(L"  -");
	for(int j = 0; j < Data->NCat; j++) CategoryChoices.Add(Data->Categories[j].Name);
	Report->EnableEditing(true);
	Report->EnableCellEditControl(true);
  #ifndef NDEBUG
		ombLogMessage(L"Category choices created.");
	#endif // NDEBUG

	for(i = 0; i < Data->NLin; i++){
		if(Data->Lines[i].IsDate){
			Report->SetCellValue(i,0,Data->Lines[i].Date.FormatDate());
			Report->SetCellFont(i, 0, font);
			Report->SetCellValue(i,2,GetLogString(0));
			Report->SetCellFont(i, 2, font);
			Report->SetCellFont(i, 3, font);
		}
		else{
			Report->SetCellValue(i,1,Data->Lines[i].Time.Format(L"%H:%M",wxDateTime::Local));
			Report->SetCellValue(i,2,GetLogString(Data->Lines[i].Type));
			Report->SetCellValue(i,4,Data->Lines[i].Reason);
			if(Data->Lines[i].CategoryIndex != -1){
				for(int j = 0; j < Data->NCat; j++) if(Data->Categories[j].Id == Data->Lines[i].CategoryIndex){
					Report->SetCellValue(i, 5, Data->Categories[j].Name);
					break;}
			}
			else Report->SetCellValue(i, 5, CategoryChoices[0]);

			// Set category drop-down editor
			Editor = new wxGridCellChoiceEditor(CategoryChoices, false);
			Report->SetCellEditor(i, 5, Editor);

		  #ifndef NDEBUG
				ombLogMessage(L"Cell editor set.");
			#endif // NDEBUG

			/*long*/ c_id = Data->Lines[i].ContactIndex;
			if(c_id > 0){
				if(Data->FindContact(c_id)){
					wxString imagepath = GetContactImage(c_id);

					#if (wxCHECK_VERSION(3, 1, 5) )
							Report->SetRowMinimalHeight(i, 48);
					#else
							Report->SetRowHeight(i, 48);
					#endif
					if(imagepath.IsEmpty()){
						// Image from https://remixicon.com/
						#ifdef __WXGTK__
							imagepath = GetDataDir() + L"images/account-box-line.png";
						#elif defined (__WXMSW__)
							imagepath = GetDataDir() + L"images\\account-box-line.png";
						#elif defined (__WXMAC__)
							imagepath = AppDir + L"/Resources/images/account-box-line.png";
						#endif // __WXGTK__
					}
					Report->SetCellRenderer(i, gcContact, new myImageGridCellRenderer(imagepath));
					anyContactShown = true;
				}
			}

			if((Data->Lines[i].Latitude < ombInvalidLatitude) && (Data->Lines[i].Longitude < ombInvalidLongitude)){

				if(map_viewer_exist){
					#ifndef __OPENSUSE__
						if(fp == NULL) fp = fopen(MapFile, "w");	// flawfinder: ignore
					#else
						if(fp == NULL) fp = fopen(MapFile.c_str(), "w");	// flawfinder: ignore
					#endif
					if(fp != NULL){
						point.lat = Data->Lines[i].Latitude;
						point.lon = Data->Lines[i].Longitude;
						fwrite(&point, sizeof(point), 1, fp);
					}
				}

				if(Report->GetRowHeight(i) < 24)
                    #if (wxCHECK_VERSION(3, 1, 5) )
                        Report->SetRowMinimalHeight(i, 24);
                    #else
                        Report->SetRowHeight(i, 24);
                    #endif
				#ifdef __WXGTK__
					Report->SetCellRenderer(i, gcLocation, new myImageGridCellRenderer(GetLocationImagePath(Report->GetRowHeight(i))));
				#elif defined (__WXMSW__)
					if(Report->GetRowHeight(i) > 24)
						Report->SetCellRenderer(i, gcLocation, new myImageGridCellRenderer(GetInstallationPath()+L"\\48\\mark-location.png"));
					else
						Report->SetCellRenderer(i, gcLocation, new myImageGridCellRenderer(GetInstallationPath()+L"\\24\\mark-location.png"));
				#elif defined (__WXMAC__)
					if(Report->GetRowHeight(i) > 24)
						Report->SetCellRenderer(i, gcLocation, new myImageGridCellRenderer(AppDir + L"/Resources/icons/48/mark-location.png"));
					else
						Report->SetCellRenderer(i, gcLocation, new myImageGridCellRenderer(AppDir + L"/Resources/icons/24/mark-location.png"));
				#endif // __WXGTK__
				anyLocationShown = true;
			}

		}
		if(Data->Lines[i].Type < 9){
			double cur;
			Data->Lines[i].Value.ToDouble(&cur);

			if((! Data->Lines[i].IsDate) && Data->Lines[i].CurrencyIndex >= 0){
				cur /= Data->Lines[i].CurrencyRate;
				Report->SetCellValue(i, 3, wxString::Format(L"( %s %s )", FormDigits(cur), Data->Lines[i].CurrencySymbol));
				wxColor col = wxColour(L"GREEN");
				Report->SetCellBackgroundColour(i, 3, col);
			}
			else Report->SetCellValue(i,3,FormDigits(cur));
			if(Report->GetCellValue(i,3).IsEmpty())
				Report->SetCellValue(i,3,_("Zero"));}
		else Report->SetCellValue(i,3,Data->Lines[i].Value);
		Report->SetCellAlignment(i,3,wxALIGN_RIGHT,wxALIGN_CENTRE);}

	if(map_viewer_exist){
		if(fp == NULL) Map->Enable(false);
		else{
			fclose(fp);
			Map->Enable();
		}
	}

	Report->AutoSizeColumns(false);
	Report->MakeCellVisible(Data->NLin - 1, 0);
	if(anyContactShown) Report->ShowCol(gcContact);
	else Report->HideCol(gcContact);
	if(anyLocationShown) Report->ShowCol(gcLocation);
	else Report->HideCol(gcLocation);

	// Select last date
	bool RowSet = false;
	for(/*int*/ i = Report->GetNumberRows() -1; i >= 0; i--){
		if((Data->IsDate(i)) && ( ! RowSet)){
			Report->SelectRow(i, false);
			RowSet = true;
		}
		for(int col = 0; col < 6; col++) Report->SetReadOnly(i, col, col != 5);
	}

	if(doChart){
		TrendChart->Show(ShowTrendChart());
		FundChart->Show(ShowFundChart());
		if(TrendChart->IsShown()||FundChart->IsShown())PaintChart();
	}

	// Shopping list
	int thisYear;	// current year and year of item alarm
	wxString tmp_str;					// str to be appended in Shopping List view
	wxDateTime alarmCalendar;	// alarm from the Shop item
	thisYear = ::wxDateTime::Today().GetYear();

	ShopList->Clear();

	// Determine maximum string length in the list;
	int len, max_len = 0;
	wxClientDC dc(this);
	dc.SetFont(ShopList->GetFont());
	int Blank_len = dc.GetTextExtent(L" ").GetWidth();

	for(i = 0; i < Data->NSho; i++){
		len = 	dc.GetTextExtent(Data->ShopItems[i].Name + L"   ").GetWidth();
		if(len > max_len) max_len = len;}

	for(i = 0; i < Data->NSho; i++){
		tmp_str = Data->ShopItems[i].Name;
		len = ((max_len - dc.GetTextExtent(tmp_str).GetWidth()) / Blank_len);
		for(int j = 0; j < len; j++) tmp_str += L" ";
		tmp_str += L"- ";

		alarmCalendar = Data->ShopItems[i].Alarm;
		int alarmYear = alarmCalendar.GetYear();
		if((alarmYear - thisYear) < 80) tmp_str += alarmCalendar.FormatDate();
		else tmp_str += _("No alarm");
		ShopList->Append(tmp_str);}
	Update();

	// Top Categories

	struct TCategorySummary{
		bool Init;
		int IconIndex;
		wxString Name;
		double Value;
	};

	TCategorySummary *Summary = new TCategorySummary[_OMB_TOPCATEGORIES_NUMBER];
	for (/*int*/ i = 0; i < _OMB_TOPCATEGORIES_NUMBER; i++){
		Summary[i].Init = false;
		Summary[i].IconIndex = -1;
		Summary[i].Name = wxEmptyString;
		Summary[i].Value = 0;
	}

	bool FoundAny;
	int SummaryIndex, FirstExpenseIndex = 0, Rows,
			ParseId;
	double Val, CategoryTotal;
	wxString Value;
	wxSQLite3Table Table;

	#ifdef __OPENSUSE__
		wxString Sql;
	#endif // __OPENSUSE__

	// Search biggest income category
	for(/*int*/ i = -1; i <= Data->NCat; i++){	// -1: unassigned category
		FoundAny = false;
		CategoryTotal = 0;

		if(i == -1) ParseId = i;
		else ParseId = i + 1;

		#ifdef __OPENSUSE__
			Sql = L"select * from Transactions where cat_index = " +
															wxString::Format(L"%d", ParseId) +
															L" and isdate = 0 and type in ( 1, 2 );";
			Table = GetTable(Data->database, Sql.c_str());
		#else
			#ifdef _OMB_USE_CIPHER
				Table = GetTable(Data->database, L"select * from Transactions where cat_index = " +
															wxString::Format(L"%d", ParseId) +
															L" and isdate = 0 and type in ( 1, 2 );");
			#else
				Table = Data->database->GetTable(L"select * from Transactions where cat_index = " +
															wxString::Format(L"%d", ParseId) +
															L" and isdate = 0 and type in ( 1, 2 );");
			#endif // _OMB_USE_CIPHER
		#endif // __OPENSUSE__

		Rows = Table.GetRowCount();
		if(Rows > 0){
			for(int j = 0; j < Rows; j++){
				Table.SetRow(j);
				Value = Table.GetString(ombTColIdValue, wxEmptyString);
				Value.ToCDouble(&Val);

				if(Table.GetInt(ombTColIdType, -1) == 1)
					CategoryTotal += Val;
				else
					CategoryTotal -= Val;
				//FoundAny = true;
			}
			if(CategoryTotal > 0) FoundAny = true;

			if(FoundAny){
				if((! Summary[0].Init) || (Summary[0].Value < CategoryTotal)){
					Summary[0].Init = true;
					if(i < 1) Summary[0].Name = _("Others");
					else{
						for(int j = 0; j < Data->NCat; j++)
						{
							if(Data->Categories[j].Id == ParseId){
								Summary[0].Name = Data->Categories[j].Name;
								Summary[0].IconIndex = Data->Categories[j].IconIndex;
								break;
							}
						}
					}
					Summary[0].Value = CategoryTotal;
				}

				FirstExpenseIndex = 1;
			}
		}
	}

	// Search biggest expense categories
	for(/*int*/ i = -1; i <= Data->NCat; i++){	// -1: unassigned category
		FoundAny = false;
		CategoryTotal = 0;

		if(i == -1) ParseId = i;
		else ParseId = i + 1;

		#ifdef __OPENSUSE__
			Sql = L"select * from Transactions where cat_index = " +
															wxString::Format(L"%d", ParseId) +
															L" and isdate = 0 and type in ( 1, 2 );";
			Table = GetTable(Data->database, Sql.c_str());
		#else
			#ifdef _OMB_USE_CIPHER
				Table = GetTable(Data->database, L"select * from Transactions where cat_index = " +
															wxString::Format(L"%d", ParseId) +
															L" and isdate = 0 and type in ( 1, 2 );");
			#else
				Table = Data->database->GetTable(L"select * from Transactions where cat_index = " +
															wxString::Format(L"%d", ParseId) +
															L" and isdate = 0 and type in ( 1, 2 );");
			#endif // _OMB_USE_CIPHER
		#endif // __OPENSUSE__

		Rows = Table.GetRowCount();
		if(Rows > 0){
			for(int j = 0; j < Rows; j++){
				Table.SetRow(j);
				Value = Table.GetString(ombTColIdValue, wxEmptyString);
				Value.ToCDouble(&Val);

				if(Table.GetInt(ombTColIdType, -1) == 1)
					CategoryTotal += Val;
				else
					CategoryTotal -= Val;
				//FoundAny = true;
			}
			if(CategoryTotal < 0) FoundAny = true;

			if(FoundAny){
				SummaryIndex = -1;
				for (int j = FirstExpenseIndex; j < _OMB_TOPCATEGORIES_NUMBER; j++){
					if(! Summary[j].Init){
						SummaryIndex = j;
						break;
					}
				}
				if(SummaryIndex == -1){
					double MaxSummary = Summary[FirstExpenseIndex].Value;
					SummaryIndex = FirstExpenseIndex;
					for (int j = FirstExpenseIndex + 1; j < _OMB_TOPCATEGORIES_NUMBER; j++){
						if(Summary[j].Value > MaxSummary){
							MaxSummary = Summary[j].Value;
							SummaryIndex = j;
						}
					}
				}

				if((! Summary[SummaryIndex].Init) || (Summary[SummaryIndex].Value > CategoryTotal)){
					Summary[SummaryIndex].Init = true;
					if(i < 1) Summary[SummaryIndex].Name = _("Others");
					else{
						for(int j = 0; j < Data->NCat; j++)
						{
							if(Data->Categories[j].Id == ParseId){
								Summary[SummaryIndex].Name = Data->Categories[j].Name;
								Summary[SummaryIndex].IconIndex = Data->Categories[j].IconIndex;
								break;
							}
						}
					}
					Summary[SummaryIndex].Value = CategoryTotal;
				}
			}
		}
	}

	// Sort top categories by Value
	bool TmpInit;
	wxString TmpName;
	double TmpValue;
	int MinIndex, TmpIndex;
	for(/*int*/ i = 0; i < _OMB_TOPCATEGORIES_NUMBER; i++){
		MinIndex = i;
		TmpValue = Summary[i].Value;
		for(int j = i; j < _OMB_TOPCATEGORIES_NUMBER; j++){
			if((Summary[j].Init) && (Summary[j].Value < TmpValue)){
				TmpValue = Summary[j].Value;
				MinIndex = j;
			}
		}
		if(MinIndex != i){
			TmpInit = Summary[i].Init;
			TmpName = Summary[i].Name;
			TmpValue = Summary[i].Value;
			TmpIndex = Summary[i].IconIndex;

			Summary[i].Init = Summary[MinIndex].Init;
			Summary[i].Name = Summary[MinIndex].Name;
			Summary[i].Value = Summary[MinIndex].Value;
			Summary[i].IconIndex = Summary[MinIndex].IconIndex;

			Summary[MinIndex].Init = TmpInit;
			Summary[MinIndex].Name = TmpName;
			Summary[MinIndex].Value = TmpValue;
			Summary[MinIndex].IconIndex = TmpIndex;
		}
	}

	// Update view
	//bool ShowCatPanel = false;
	wxColor Green = wxColour(L"GREEN");
	wxColor Red = wxColour(L"RED");

	wxStaticText *Name, *Value_Text;
	wxStaticBitmap *Icon;

	for(i = 0; i < _OMB_TOPCATEGORIES_NUMBER; i++){
		switch(i){
			case 1:
				Name = TopCategoriesPanel->teCategory2;
				Value_Text = TopCategoriesPanel->teValue2;
				Icon = TopCategoriesPanel->teIcon2;
				break;
			case 2:
				Name = TopCategoriesPanel->teCategory3;
				Value_Text = TopCategoriesPanel->teValue3;
				Icon = TopCategoriesPanel->teIcon3;
				break;
			default:
				Name = TopCategoriesPanel->teCategory1;
				Value_Text = TopCategoriesPanel->teValue1;
				Icon = TopCategoriesPanel->teIcon1;
		}
		if(Summary[i].Init){
			Name->SetLabel(Summary[i].Name);
			Value_Text->SetLabel(FormDigits(Summary[i].Value) + L" " + GetCurrencySymbol());
			if(Summary[i].IconIndex > -1)
				Icon->SetBitmap(CategoryIconList->GetBitmap(Summary[i].IconIndex));
			if(Summary[i].Value > 0){
				Name->SetForegroundColour(Green);
				Value_Text->SetForegroundColour(Green);
			}
			else{
				Name->SetForegroundColour(Red);
				Value_Text->SetForegroundColour(Red);
			}
		}
		Name->Show(Summary[i].Init);
		Value_Text->Show(Summary[i].Init);
		Icon->Show(Summary[i].Init);
	}
	TopCategoryLabel->Show(Summary[0].Init);
	TopCategoriesPanel->Show(Summary[0].Init);

	CheckDate();

  #ifndef NDEBUG
		ombLogMessage(L"ShowF routine complete.");
	#endif // NDEBUG
}

void ombMainFrame::AddNode(wxTreeItemId Node, const wxString &Name, const wxString &Value, int c_index, bool External){
  int index = -1, custom_index;
  wxTreeItemId item;
  if(Node == Funds_tid) index = 0;
  else if(Node == Creds_tid) index = 1;
  else if(Node == Debts_tid) index = 2;
  else if(Node == Lends_tid) index = 4;
  else if(Node == Borrowed_tid) index = 5;
  if(c_index > -1) custom_index = c_index;
  else custom_index = index;
	// NOTE (igor#1#): Hidden feature
	#ifdef _OMB_USE_CIPHER
	  if(External){
			item = ItemList->AppendItem(Node, Name + L" (*)", -1, -1, NULL);
			ItemList->AppendItem(item, Value, -1, -1, NULL);
	  }
	else
	#endif // _OMB_USE_CIPHER
	{
		item = ItemList->AppendItem(Node, Name, custom_index, custom_index, NULL);
		ItemList->AppendItem(item, Value, index, index, NULL);
	}
}

void ombMainFrame::PaintChart(void){
	int i = 0;
	#ifdef _OMB_USE_CIPHER
		double TotFundAttached = 0;
	#endif // _OMB_USE_CIPHER
	if(ShowFundChart()){

		#ifdef _OMB_CHART_WXPIECTRL

			bool default_is_small = false;
			int cindex = 0;	// colour array index
			double percent, sum_small_funds = 0;
			wxColour Colours[] = {
				wxColour(0x99, 0xCC, 0xFF),
				wxColour(0xFF, 0xFF, 0x99),
				wxColour(0x3D, 0xEB, 0x3D),
				wxColour(0xFF, 0x00, 0x00),
				wxColour(0x00, 0xFF, 0xFF),
				wxColour(0x00, 0x00, 0xFF)
			};

			FundChart->m_Series.Clear();

			// Initialize pie control
			FundChart->m_Series.Clear();
			wxPiePart part;

			cindex = 0;

			// NOTE (igor#1#): Hidden feature
			double Tot_Funds = Data->Tot_Funds;
			#ifdef _OMB_USE_CIPHER
				wxSQLite3Table AttachedFundTable;
				if(Data->ExternalFileAttached){
					double attached_value;
					AttachedFundTable = Data->GetExternalFundTable();
					for(int j = 0; j < AttachedFundTable.GetRowCount(); j++){
						AttachedFundTable.SetRow(j);
						attached_value = AttachedFundTable.GetDouble(L"value", 0);
						TotFundAttached += attached_value;
					}
					Tot_Funds += TotFundAttached;
					for(i = 0; i < AttachedFundTable.GetRowCount(); i++){
						AttachedFundTable.SetRow(i);
						attached_value = AttachedFundTable.GetDouble(L"value", 0);
						percent = attached_value / Tot_Funds * 100;
						if((percent) > 1){
							part.SetLabel(AttachedFundTable.GetString(L"name", wxEmptyString) + L" (*)" + wxString::Format(L" %.1f", percent) + L"%");
							part.SetValue(attached_value);

							part.SetColour(Colours[cindex]);
							cindex++;
							if(cindex > 5) cindex = 0;

							FundChart->m_Series.Add(part);
						}
						else
							sum_small_funds += attached_value;
					}
				}
			#endif // _OMB_USE_CIPHER

			if(ShowFundGroups()){
				wxSQLite3Table GroupTable, ChildTable;
				#ifdef _OMB_USE_CIPHER
					GroupTable = GetTable(Data->database, "select * from FundGroups where owner = '-1';");
				#else
					GroupTable = Data->database->GetTable(L"select * from FundGroups where owner = '-1';");
				#endif // _OMB_USE_CIPHER
				int GTC = GroupTable.GetRowCount();

				double GroupTotals[GTC];
				for( i = 0; i < GTC; i++)
					GroupTotals[i] = 0;
				wxString GroupNames[GTC];
				for(i = 0; i < GTC; i++){
					GroupTable.SetRow(i);
					GroupNames[i] = GroupTable.GetAsString("name");
				}

				#ifdef _OMB_USE_CIPHER
					ChildTable = GetTable(Data->database, "select * from FundGroups where owner > '-1';");
				#else
					ChildTable = Data->database->GetTable(L"select * from FundGroups where owner > '-1';");
				#endif // _OMB_USE_CIPHER
				int CTC = ChildTable.GetRowCount();

				int Group_ID;
				wxString FundName;
				for(i = 0; i < Data->NFun; i++){
					bool Found = false;
					FundName = Data->Funds[i].Name;
					for(int j = 0; j < CTC; j++){
						ChildTable.SetRow(j);
						if(FundName.CmpNoCase(ChildTable.GetAsString("name")) == 0){
							Group_ID = ChildTable.GetInt("owner", -1);
							for(int k = 0; k < GTC; k++){
								GroupTable.SetRow(k);

								if(GroupTable.GetInt("id", -1) == Group_ID){
									Found = true;
									GroupTotals[k] += Data->Funds[i].Value;
									break;
								}

							}

							break;
						}
					}
					if(! Found){
						percent = Data->Funds[i].Value / Tot_Funds * 100;
						if((percent) > 1){
							part.SetLabel(Data->Funds[i].Name + wxString::Format(L" %.1f", percent) + L"%");
							part.SetValue(Data->Funds[i].Value);

							part.SetColour(Colours[cindex]);
							cindex++;
							if(cindex > 5) cindex = 0;

							FundChart->m_Series.Add(part);
						}
						else{
							sum_small_funds += Data->Funds[i].Value;
							if(Data->Funds[i].Name.Cmp(Data->FileData.DefFund) == 0) default_is_small = true;}

					}
				}

				for(i = 0; i < GTC; i++){
					percent = GroupTotals[i] / Tot_Funds * 100;
					if((percent) > 1){
						part.SetLabel(GroupNames[i] + " - [" + _("Group") + "]" + wxString::Format(L" %.1f", percent) + L"%");
						part.SetValue(GroupTotals[i]);

						part.SetColour(Colours[cindex]);
						cindex++;
						if(cindex > 5) cindex = 0;

						FundChart->m_Series.Add(part);
					}
					else{
						sum_small_funds += GroupTotals[i];
					}

				}

			}
			else{

				// ------------------------------
				for(i = 0; i < Data->NFun; i++){
					percent = Data->Funds[i].Value / Tot_Funds * 100;
					if((percent) > 1){
						part.SetLabel(Data->Funds[i].Name + wxString::Format(L" %.1f", percent) + L"%");
						part.SetValue(Data->Funds[i].Value);

						part.SetColour(Colours[cindex]);
						cindex++;
						if(cindex > 5) cindex = 0;

						FundChart->m_Series.Add(part);
					}
					else{
						sum_small_funds += Data->Funds[i].Value;
						if(Data->Funds[i].Name.Cmp(Data->FileData.DefFund) == 0) default_is_small = true;}
				}

			}

			if(sum_small_funds){
				percent = sum_small_funds / Data->Tot_Funds * 100;
				wxString title;
				title = _("Others");
				if(default_is_small){
					title += L" (";
					title += Data->FileData.DefFund;
					title += L")";
				}
				title += wxString::Format(L" %.1f", percent) + L"%";
				part.SetLabel(title);
				part.SetValue(sum_small_funds);
				part.SetColour(Colours[cindex]);
				FundChart->m_Series.Add(part);

			}

			FundChart->Refresh();

		#elif defined ( _OMB_CHART_MATLIBPLOT )
			double sum_small_funds = 0;
			bool default_is_small = false;

		  wxString tmp_file, data_fname, chart_fname, line;
		  wxString Proc = L"python " + GetDataDir();
		  Proc += L"pie.py ";
		  Proc += L"/usr/share/locale ";	// TODO (igor#1#): Check if it is possible to get this path programmatically

			tmp_file = GetTempFile();
			data_fname = ::wxFileName::CreateTempFileName(tmp_file);
			wxTextFile *file = new wxTextFile(data_fname);
			file->Create();

			for(i = 0; i < Data->NFun; i++){
				if((Data->Funds[i].Value / Data->Tot_Funds) > 0.01){
					line = Data->Funds[i].Name + ", " + wxString::FromCDouble(Data->Funds[i].Value, 2) + ", 0";
					if(Data->Funds[i].Name.Cmp(Data->FileData.DefFund) == 0) line += L".1";
					file->AddLine(line);}
				else{
					sum_small_funds += Data->Funds[i].Value;
					if(Data->Funds[i].Name.Cmp(Data->FileData.DefFund) == 0) default_is_small = true;}
			}
			if(sum_small_funds){
				line = _("Others") + ", " + wxString::FromCDouble(sum_small_funds, 2) + ", 0";

				if(default_is_small) line += L".1";
				file->AddLine(line);
			}

			file->Write(wxTextFileType_Unix,wxConvUTF8);
			// file closure
			file->Close();
			delete file;

			Proc += data_fname + L" ";

			chart_fname = ::wxFileName::CreateTempFileName(tmp_file);
			::wxRemoveFile(chart_fname);
			chart_fname += L".png";
			Proc += chart_fname;

			wxExecute(Proc, wxEXEC_SYNC);
			::wxRemoveFile(data_fname);

			FundChart->SetBitmap(GetScaledBitmap(chart_fname, FundChart));

			::wxRemoveFile(chart_fname);

		#endif // _OMB_CHART_WXPIECTRL
	}
	if(ShowTrendChart()){
		// Trend chart construction
		wxDateTime::Month m;
		switch(Data->FileData.Month){
			case 1:
				m = wxDateTime::Jan;
				break;
			case 2:
				m = wxDateTime::Feb;
				break;
			case 3:
				m = wxDateTime::Mar;
				break;
			case 4:
				m = wxDateTime::Apr;
				break;
			case 5:
				m = wxDateTime::May;
				break;
			case 6:
				m = wxDateTime::Jun;
				break;
			case 7:
				m = wxDateTime::Jul;
				break;
			case 8:
				m = wxDateTime::Aug;
				break;
			case 9:
				m = wxDateTime::Sep;
				break;
			case 10:
				m = wxDateTime::Oct;
				break;
			case 11:
				m = wxDateTime::Nov;
				break;
			case 12:
				m = wxDateTime::Dec;
				break;
			default:
				m = wxDateTime::Inv_Month;}

		int number_of_days = wxDateTime::GetNumberOfDays(m, Data->FileData.Year, wxDateTime::Gregorian);

		double trendvalues[number_of_days];
		for(i = 0; i < number_of_days; i++) trendvalues[i] = -1;
		const wxChar *trenddates[number_of_days];
		time_t times[WXSIZEOF(trenddates)];
    wxDateTime LastDate = wxDateTime::Today(), dt;
    double	LastValue = 0;

		int j = 0;
		for(i = 0; i < Data->NLin; i++){
			if(Data->IsDate(i)){
				#ifndef _OMB_CHART_WXPIECTRL
					j = Data->Lines[i].Date.GetDay() - 1;
				#endif // _OMB_CHART_WXPIECTRL
				LastDate = Data->Lines[i].Date;
				Data->Lines[i].Value.ToDouble(&LastValue);
				trendvalues[j]=LastValue;
				times[j]=LastDate.GetTicks();
				#ifdef _OMB_CHART_WXPIECTRL
					j++;
				#endif // _OMB_CHART_WXPIECTRL
			}}

		#ifndef _OMB_CHART_WXPIECTRL
			bool	found_before = false,
						found_after;
			int k;
			for(i = 0; i < number_of_days; i++){
				if(trendvalues[i] == -1){
					if(found_before){
						found_after = false;
						for(k = i+1; k < number_of_days; k++)
							if(trendvalues[k] != -1){
								found_after = true;
								break;}

						if(found_after) trendvalues[i] = trendvalues[i-1];
						else trendvalues[i] = 0;}
					else trendvalues[i] = 0;
					dt = wxDateTime(i+1, m, Data->FileData.Year, 0, 0, 0, 0);
					times[i] = dt.GetTicks();}
				else found_before = true;}
		#endif // _OMB_CHART_WXPIECTRL

		#ifdef _OMB_CHART_WXPIECTRL
			DAILY_STATS *ds = new DAILY_STATS[j];
			for(i = 0; i < j; i++){
				ds[i].day = times[i];
				ds[i].total_credit = trendvalues[i];
			}

			TrendChart->nstats = j;
			TrendChart->stats = ds;

			if(Data->FileData.ReadOnly) TrendChart->trend_value = -1;
			else{
				// NOTE (igor#1#): Hidden feature
				#ifdef _OMB_USE_CIPHER
					if(Data->ExternalFileAttached) TrendChart->trend_value = Data->GetTot(tvFou) + TotFundAttached;
					else
				#endif // _OMB_USE_CIPHER
				TrendChart->trend_value = Data->GetTot(tvFou);
				LastDate.Set(LastDate.GetLastMonthDay().GetDay(), LastDate.GetMonth(), LastDate.GetYear(), 23, 59, 59, 0);	// Setting to 23:59 for better visualization
				TrendChart->trend_date = LastDate;
			}

			TrendChart->m_full_repaint = true;
			TrendChart->Refresh();

		#elif defined ( _OMB_CHART_MATLIBPLOT )
		  wxString tmp_file, data_fname, chart_fname, line;
		  wxString Proc = L"python " + GetDataDir();
		  Proc += L"trend.py ";
		  Proc += L"/usr/share/locale ";	// TODO (igor#1#): Check if it is possible to get this path programmatically

			tmp_file = GetTempFile();
			data_fname = ::wxFileName::CreateTempFileName(tmp_file);
			wxTextFile *file = new wxTextFile(data_fname);
			file->Create();

			for(i = 0; i < number_of_days; i++)
				file->AddLine(wxString::Format("%d", int(times[i])) + ", " + wxString::FromCDouble(trendvalues[i], 2));

			file->Write(wxTextFileType_Unix,wxConvUTF8);
			// file closure
			file->Close();
			delete file;

			Proc += data_fname + L" ";

			chart_fname = ::wxFileName::CreateTempFileName(tmp_file);
			::wxRemoveFile(chart_fname);
			chart_fname += L".png";
			Proc += chart_fname;

			if(! Data->FileData.ReadOnly){
				wxChar color;
				double currenttotal = Data->GetTot(tvFou);
				if(LastValue == currenttotal) color = 'c';
				else if(LastValue < currenttotal) color = 'g';
				else color = 'r';

				Proc += " " + wxString(color) + " " + wxString::FromCDouble(currenttotal, 2);
			}

			wxExecute(Proc, wxEXEC_SYNC);
			::wxRemoveFile(data_fname);

			TrendChart->SetBitmap(GetScaledBitmap(chart_fname, TrendChart, true));

			::wxRemoveFile(chart_fname);

		#endif // _OMB_CHART_WXPIECTRL
	}
	FreshChartShown = true;
}

void ombMainFrame::HelpMenuClick(wxCommandEvent& event){
	wxString Page;
	#ifdef __WXGTK__
		#ifdef _YELP
			#ifdef __FREEBSD__
				Page = L"/usr/local/share/help/";
			#else
				Page = GetShareDir() + L"help/";
			#endif // __FREEBSD__
			if((Lan == wxLANGUAGE_ITALIAN)
					#if (wxCHECK_VERSION(3, 2, 0) )
						 || (Lan == wxLANGUAGE_ITALIAN_ITALY)
					#endif
					) Page += L"it/openmoneybox/omb_it.xml";
			else Page +=  L"en_GB/openmoneybox/omb_en.xml";
			wxExecute("yelp " + Page, wxEXEC_ASYNC);
			return;
		#else
			Page = GetDataDir();
			if((Lan == wxLANGUAGE_ITALIAN)
				#if (wxCHECK_VERSION(3, 2, 0) )
					 || (Lan == wxLANGUAGE_ITALIAN_ITALY)
				#endif
				) Page += L"/openmoneybox_it.html";
			else Page += L"/openmoneybox_en.html";
	  #endif // _YELP
	#elif defined (__WXMSW__)
    #ifdef _OMB_PORTABLE_MSW
      Page = wxGetCwd();
    #else
      Page = GetInstallationPath();
    #endif // _OMB_PORTABLE_MSW
    if((Lan == wxLANGUAGE_ITALIAN)
      #if (wxCHECK_VERSION(3, 2, 0) )
         || (Lan == wxLANGUAGE_ITALIAN_ITALY)
      #endif
      ) Page += L"\\openmoneybox-msw_it.html";
    else Page += L"\\openmoneybox-msw_en.html";
	#elif defined (__WXMAC__)
	    Page = wxGetCwd() + AppDir + L"/Resources";
	    if((Lan == wxLANGUAGE_ITALIAN)
				#if (wxCHECK_VERSION(3, 2, 0) )
					 || (Lan == wxLANGUAGE_ITALIAN_ITALY)
				#endif
				) Page += L"/openmoneybox-mac_it.html";
	    else Page += L"/openmoneybox-mac_en.html";
	#endif // __WXGTK
	wxLaunchDefaultBrowser(Page);
}

void ombMainFrame::AuthorClick(wxCommandEvent& event){
	AuthorWindow();}

void ombMainFrame::UpdateCalendar(wxDateTime master_mindate){
	int Month, Year;

	if(Master_Attached) calmindate = master_mindate;
	else{
		Month = Data->FileData.Month - 1;
		Year = Data->FileData.Year;
		if(Month < 1){
			Month = 12;
			Year--;}
		calmindate.Set(25, wxDateTime::Month(Month - 1), Year, 0, 0, 0, 0);}

	Month = Data->FileData.Month + 1;
	Year = Data->FileData.Year;
	if(Month > 12){
		Month = 1;
		Year++;}
  calmaxdate.Set(5, wxDateTime::Month(Month - 1), Year, 23, 59, 59, 999);

  if(calmaxdate < wxDateTime::Today()) calmaxdate = wxDateTime::Today();

	if(! Master_Attached) Calendar->SetDate(wxDateTime::Today());

  CheckDate();
}

void ombMainFrame::GiveTime(wxDateEvent& event){
	Data->Hour = TimePicker->GetValue();}

void ombMainFrame::UpdateMenu(void){
	bool C=!Data->FileData.FileView.IsEmpty();
	bool M=Data->FileData.Modified;
	bool W=!Data->FileData.ReadOnly;

	/*
	if(C&W){
        if(!menu_opendoc){
            // File menu update ------------------------
            MainMenu_File_->Insert(4,Revert);
            MainMenu_File_->Insert(5,XML);

            // -----------------------------------------
            //MAINMENU->Insert(1,MainMenu_Edit_,_("Edit"));
            MAINMENU->Insert(2,MainMenu_Funds_,_("Funds"));
            MAINMENU->Insert(3,MainMenu_Operations_,_("Operations"));
            MAINMENU->Insert(4,MainMenu_Objects_,_("Objects"));
            MAINMENU->Insert(5,MainMenu_Credits_,_("Credits"));
            MAINMENU->Insert(6,MainMenu_Debts_,_("Debts"));
            MAINMENU->Insert(7,MainMenu_ShopList_,_("Shopping list"));
            InitLanguage();
            menu_opendoc=true;}}
        else {
            if(menu_opendoc){
                // File menu update ------------------------
                MainMenu_File_->Remove(bilRevert);

                // -----------------------------------------
                for(int i = 0; i < 6; i++) MAINMENU->Remove(2);
                	// Funds menu removal
                	// Operations menu removal
                	// Objects menu removal
                	// Credits menu removal
                	// Debts menu removal
                	// Shoplist menu removal
                menu_opendoc=false;}}
		*/

    if(C) MainMenu_File_->Enable(bilRevert,M);

		#ifdef __WXMSW__
			if(MainMenu_Help_->FindItem(2000) == NULL){
				MainMenu_Help_->Insert(3, 2000, _("License"), wxEmptyString, NULL);
				Connect(2000, wxEVT_COMMAND_MENU_SELECTED,wxCommandEventHandler(ombMainFrame::LicenseClick));
			}
		#endif // __WXMSW__

	MainMenu_File_->Enable(ombArchive, ::wxFileExists(Get_MasterDB()));

	#ifdef _OMB_USE_CIPHER
		MainMenu_File_->Enable(ombRemovePassword, Data->IsEncrypted);
	#endif // _OMB_USE_CIPHER

	// Toolbar update
  ToolBar->EnableTool(bilXML,C);
  ToolBar->EnableTool(bilNewFund,C&W);
  ToolBar->EnableTool(bilRemoveFund,C&W);
  ToolBar->EnableTool(bilResetFund,C&W);
  ToolBar->EnableTool(bilGain,C&W);
  ToolBar->EnableTool(bilExpense,C&W);

  #ifndef NDEBUG
		ombLogMessage(L"Try to update Category GUI controls.");
	#endif // NDEBUG

	bool IsDate;
	int i = GetFirstSelectedRow();
	if(i >= 0) IsDate = Data->Lines[i].IsDate;
	else IsDate = true;

  #ifndef NDEBUG
		ombLogMessage(L"... checked if last row is a date.");
	#endif // NDEBUG
	EnableCategoryGUIControls( C & W & ( ! IsDate));
  #ifndef NDEBUG
		ombLogMessage(L"Category GUI controls updated.");
	#endif // NDEBUG

  ToolBar->EnableTool(bilReceived,C&W);
  ToolBar->EnableTool(bilGifted,C&W);
  ToolBar->EnableTool(bilLend,C&W);
  ToolBar->EnableTool(bilGetBack,C&W);
  ToolBar->EnableTool(bilBorrow,C&W);
  ToolBar->EnableTool(bilGiveBack,C&W);
  ToolBar->EnableTool(bilNewCredit,C&W);
  ToolBar->EnableTool(bilRemoveCredit,C&W);
  ToolBar->EnableTool(ombRemitCredit,C&W);
  ToolBar->EnableTool(bilNewDebt,C&W);
  ToolBar->EnableTool(bilRemoveDebt,C&W);
  ToolBar->EnableTool(ombRemitDebt,C&W);
  ToolBar->EnableTool(bilAddShopItem,C&W);
  ToolBar->EnableTool(bilDelShopItem,C&W);

	Calendar->Enable(C & W);
	TimePicker->Enable(C & W);
}

void ombMainFrame::EnableCategoryGUIControls(bool Enable){
	MainMenu_Operations_->Enable(ombCategory, Enable);
  ToolBar->EnableTool(ombCategory, Enable);
}

bool ombMainFrame::SaveDatabase(void){
	if(Data->FileData.Modified){
		wxString Msg = wxString::Format(_("Save changes to %s?"), Data->FileData.FileView);
		wxBell();

		wxString Sql = "SAVEPOINT rollback;";

		switch(wxMessageBox(Msg, _("OpenMoneyBox"), wxYES_NO | wxCANCEL)){
			case wxYES:
				#ifdef _OMB_USE_CIPHER
					sqlite3_exec(Data->database, "RELEASE rollback;", NULL, NULL, NULL);
				#else
					Data->database->ReleaseSavepoint(L"rollback");
				#endif // _OMB_USE_CIPHER

				#ifdef _OMB_MONOLITHIC
					Data->FileData.Modified = false;

					#ifdef _OMB_USE_CIPHER
//						wxString Sql = "SAVEPOINT rollback;";
						ExecuteUpdate(Data->database, Sql.c_str());
					#else
						Data->database->Savepoint(L"rollback");
					#endif // _OMB_USE_CIPHER
				#endif // _OMB_MONOLITHIC
				break;
			case wxNO:
				#ifdef _OMB_MONOLITHIC
					#ifdef _OMB_USE_CIPHER
						ExecuteUpdate(Data->database, "ROLLBACK TO SAVEPOINT rollback;");
					#else
						Data->database->Rollback(L"rollback");
					#endif // _OMB_USE_CIPHER
					Data->FileData.Modified = false;
				#endif // _OMB_MONOLITHIC
				break;
			case wxCANCEL:
				return false;
			default:
				Error(5, wxEmptyString);
				return false;}}
	return true;}

void ombMainFrame::FormClose(wxCloseEvent &event){
	if(Data->FileData.Modified){
		if(! SaveDatabase()){
			event.Veto(true);
			return;}
	}
	else
		#ifdef _OMB_USE_CIPHER
			ExecuteUpdate(Data->database, "ROLLBACK TO SAVEPOINT rollback;");
		#else
			Data->database->Rollback(L"rollback");
		#endif // _OMB_USE_CIPHER

	#ifdef _OMB_MONOLITHIC
		if(IsTrayCommand){
			if(event.GetEventType() != wxEVT_END_SESSION){
				// Start indicator
				App->StartTray();
				this->Show(false);
				event.Veto();
			}
			else{
				FormDestroy();
				Destroy();
			}
		}
		else if(TrayActive()){
			if(event.GetId() != 111)	// Id set to 111 in ombApp::TimerFire with cmd_close
				// Start indicator
				App->StartTray();
			this->Show(false);
			event.Veto();
		}
		else{
	#endif // _OMB_MONOLITHIC

	FormDestroy();
	Destroy();

	#ifdef _OMB_MONOLITHIC
		}
//		CloseCallLoops = 0;
	#endif // _OMB_MONOLITHIC
}

void ombMainFrame::FormDestroy(void){
	Initialised=false;
	delete FundChart;
	delete TrendChart;
	TreeNames->Clear();
	delete TreeNames;
	delete FindDialog;}	// Necessary to grant ombApp exits

bool ombMainFrame::AutoOpen(void){

	bool new_document = false;
	wxString Document = GetDefaultDocument();

	if(Document.IsEmpty()){
		// Create document filename
		Document = GetBilDocPath();
		if(Document.IsEmpty()) Document = GetOSDocDir();
		Document += L"/openmoneybox.omb";
		new_document = true;
		// Set default document
		Set_DefaultDocument(Document);
	}
  #ifndef NDEBUG
		ombLogMessage(L"Database: " + Document);
	#endif // NDEBUG

	if(wxFileExists(Document)){
		if(new_document){
			Error(36, wxEmptyString);
			Close(false);
			return false;
		}
	}
	else{
		wxString File;
		switch(wxMessageBox(_("Do you want to browse for an existing document?\n\nYes: Browse\nNo: Run wizard"), _("OpenMoneyBox"), wxYES_NO | wxICON_QUESTION)){
			case wxYES:
				File = BrowseDocument();
				if(File == wxEmptyString){
					Close(false);
					return false;
				}
				Set_DefaultDocument(File);
				Document = File;
				break;
			case wxNO:
			 switch(RunWizard(Progress, Document, true)){	// create default document
				case 2:
					Set_DefaultDocument(Document);
					break;
				default:
					Close(false);
					return false;
			}
		}
	}

	// Open the document
	#ifdef _OMB_MONOLITHIC
		if(! IsTrayCommand)
	#endif // _OMB_MONOLITHIC

	if(! Data->OpenDatabase(Document)){
	  #ifndef NDEBUG
			ombLogMessage(L"Error opening database!");
		#endif // NDEBUG
		return false;}

	ShowControls(true);

	// Load custom category icons
	#ifdef _OMB_USE_CIPHER
		if(TableExists("CustomIcons", wxEmptyString, Data->database)){
	#else
		if(Data->database->TableExists(L"CustomIcons")){
	#endif // _OMB_USE_CIPHER

		wxSQLite3Table Table;
		wxString Path = GetUserLocalDir() + L"/ombCategoryImgs/";
		wxString ImageFile, sql;
		wxBitmap Bmp;

		sql = L"select * from CustomIcons order by id;";

		#ifdef _OMB_USE_CIPHER
			Table = GetTable(Data->database, sql.c_str());
		#else
			Table = Data->database->GetTable(sql);
		#endif // _OMB_USE_CIPHER
		for(int i = 0; i < Table.GetRowCount(); i++){
			Table.SetRow(i);
			ImageFile = Path + Table.GetString(1, wxEmptyString) + L".png";
			if(wxFileExists(ImageFile)){
				if(Bmp.LoadFile(ImageFile, wxBITMAP_TYPE_PNG)) CategoryIconList->Add(Bmp);
			}
			else CategoryIconList->Add(CategoryIconList->GetIcon(0));
		}
	}

	#ifndef _OMB_CHART_MATLIBPLOT
		ShowF();
	#endif // _OMB_CHART_MATLIBPLOT

	UpdateMenu();

	return true;}

void ombMainFrame::ShowFindDialog (wxCommandEvent& event){
	if (FindDialog == NULL)FindDialog=new wxFindReplaceDialog(this,FindData,_("Find"), wxFR_NOUPDOWN | wxFR_NOMATCHCASE | wxFR_NOWHOLEWORD);
	FindDialog->Show();}

void ombMainFrame::Find(wxFindDialogEvent& event){	// to be debugged
	bool Found = false;
	bool loop = false;	// set to true on search from top
	int r, start, stop;	// list indexes
	int findex;	// find index (>0 if string is found)
	wxString find_str = event.GetFindString().MakeLower();
	r = GetFirstSelectedRow();
	if(r < 0)return;

	start = r + 1;
	if(Master_Attached && (! IsNewMonthShown())) stop = Report->GetNumberRows() - 1;
	else stop = Data->NLin;

	MainFindLoop:
	findex = FindLoop(start, stop, find_str);
	if(findex >= 0) Found=true;
	else if(! loop){
		start = 0;
		stop = r + 1;
		loop = true;
		goto MainFindLoop;}
	if(Found){
		wxBell();
		Report->SelectRow(findex, false);
		Report->MakeCellVisible(findex, 0);}
	else{
		wxString Msg=wxString::Format(_("\"%s\" not found."), find_str.c_str());
		wxBell();
		::wxMessageBox(Msg, _("OpenMoneyBox"),wxOK);}}

int ombMainFrame::FindLoop(int StartIndex, int StopIndex, wxString find_str){
	int i;
	if(Master_Attached && (! IsNewMonthShown())){
		int min_date, max_date;
		wxDateTime cal_date, min_datetime, max_datetime;
		cal_date = Calendar->GetDate();
		//int month = cal_date.GetMonth();
		int year = cal_date.GetYear();

		min_datetime = cal_date;
		min_datetime.SetDay(1);
		min_datetime.SetHour(0);
		min_datetime.SetMinute(0);
		min_datetime.SetSecond(0);
		min_datetime.SetMillisecond(0);
		min_date = min_datetime.GetTicks();

		max_datetime = cal_date;
		max_datetime.SetToLastMonthDay(max_datetime.GetMonth(), year);
		max_datetime.SetHour(23);
		max_datetime.SetMinute(59);
		max_datetime.SetSecond(59);
		max_datetime.SetMillisecond(999);
		max_date = max_datetime.GetTicks();

		wxSQLite3Table transaction_table;

		#ifdef _OMB_USE_CIPHER
      #ifdef __OPENSUSE__
        wxString Sql = ::wxString::Format(L"SELECT * FROM master.Transactions WHERE date >= %d and date <= %d order by date;", min_date, max_date);
        transaction_table = GetTable(Data->database, Sql.c_str());
      #else
        transaction_table = GetTable(Data->database, ::wxString::Format(L"SELECT * FROM master.Transactions WHERE date >= %d and date <= %d order by date;", min_date, max_date));
      #endif // __OPENSUSE__
		#else
			transaction_table = Data->database->GetTable(::wxString::Format(L"SELECT * FROM master.Transactions WHERE date >= %d and date <= %d order by date;", min_date, max_date));
		#endif // _OMB_USE_CIPHER

		for(i = StartIndex; i <= StopIndex; i++){
			int f1, f2, f3;	// string find results
			long cat_index;
			wxString S1, S2, S3;

			transaction_table.SetRow(i);
			wxString value_string = transaction_table.GetString(ombTColIdValue, wxEmptyString);
			if(transaction_table.GetInt(ombTColIdType, 0) < 9){
				double cur;

				value_string.ToCDouble(&cur);

				S1 = FormDigits(cur);
			}
			else S1 = value_string;
			S1.MakeLower();

			S2 = transaction_table.GetString(ombTColIdReason, wxEmptyString).MakeLower();
			cat_index = transaction_table.GetInt(ombTColIdCategoryIndex, 0);;
			if(cat_index >= 0){
				for(int j = 0; j < Data->NCat; j++) if(Data->Categories[j].Id == cat_index){
					S3 = Data->Categories[j].Name.MakeLower();
					break;}}
			else S3 = wxEmptyString;
			S3.MakeLower();

			f1 = S1.Find(find_str);
			f2 = S2.Find(find_str);
			f3 = S3.Find(find_str);
			if(! (f1 == wxNOT_FOUND && f2 == wxNOT_FOUND && f3 == wxNOT_FOUND)) return i;}
		return -1;

	}
	else{
		for(i = StartIndex; i < StopIndex; i++){
			int f1, f2, f3;	// string find results
			long cat_index;
			wxString S1, S2, S3;
			S1 = Data->Lines[i].Value.MakeLower();
			S2 = Data->Lines[i].Reason.MakeLower();
			cat_index = Data->Lines[i].CategoryIndex;
			if(cat_index >= 0){
				for(int j = 0; j < Data->NCat; j++) if(Data->Categories[j].Id == cat_index){
					S3 = Data->Categories[j].Name.MakeLower();
					break;}}
			else S3 = wxEmptyString;
			f1 = S1.Find(find_str);
			f2 = S2.Find(find_str);
			f3 = S3.Find(find_str);
			if(! (f1 == wxNOT_FOUND && f2 == wxNOT_FOUND && f3 == wxNOT_FOUND)) return i;}
		return -1;
	}
}

void ombMainFrame::FindClose(wxFindDialogEvent& event){
	FindDialog->Hide();}

void ombMainFrame::WebSiteClick(wxCommandEvent& event){
	wxLaunchDefaultBrowser(WebAddress);}

void ombMainFrame::BugClick(wxCommandEvent& event){
	wxLaunchDefaultBrowser(BugAddress);}

void ombMainFrame::DonateClick(wxCommandEvent& event){
	DonateDialog(DonateAddress);
}

/*
void ombMainFrame::ResizeCols(int C){
  int Current,Max;
  if(C>Report->GetNumberCols())return;
	Max=0;
  for(int i=0;i<Report->GetNumberRows();i++){
    Current=Report->Canvas->TextWidth(Report->Cells[C][i]);
    if((C==1)||(C==3))Current+=5; // Necessary for time widths
    if(Max<Current)Max=Current;}
  if(Max!=Report->ColWidths[C])Report->ColWidths[C]=Max+5;
  Report->Repaint();}
*/

void ombMainFrame::CheckDate(wxCalendarEvent& event){
	CheckDate();
 	if(Master_Attached) ShowMaster();}

/*
void ombMainFrame::PopRepPopup( wxGridEvent& event ){
	if(Data->NLin < 1)return;
	int Row=event.GetRow();
	Report->SelectRow(Row,false);
	if(Data->IsDate(Row))return;
	Report->PopupMenu(PopRep,event.GetPosition());}
*/

wxString ombMainFrame::CreateTitle(bool type){
	// set type to true to make modification and read-only checks
	wxString label = _("Monthly report of ");
	if(Master_Attached) label += IntToMonth(Calendar->GetDate().GetMonth() + 1);
	else label += IntToMonth(Data->FileData.Month);
	label += char(32);

	if(Master_Attached) label += ::wxString::Format(L"%d", Calendar->GetDate().GetYear());
	else
		#ifdef __WXMSW__
			label += ::wxString::Format("%d", Data->FileData.Year);
		#else
			label += ::wxString::Format(L"%d", Data->FileData.Year);
		#endif // __WXMSW__
	if(type){
		if(Master_Attached){
			wxDateTime cal_date = Calendar->GetDate();
			int month = cal_date.GetMonth();
			int year = cal_date.GetYear();
			if((year < Data->FileData.Year) || ((month < Data->FileData.Month - 1) && (year == Data->FileData.Year)) || (year > Data->FileData.Year))
				label = label + _(" [Read-only]");
			else if ((year == Data->FileData.Year) && (month == Data->FileData.Month - 1) && Data->FileData.Modified) label += L" [*]";
		}
		else if(Data->FileData.Modified) label += L" [*]";
	}
	return label;
}

void ombMainFrame::ExtClick(wxCommandEvent& event){
	wxString Tool = GetTool(event.GetId() - ombExtTool);
	if(! wxFileExists(Tool)){
		Error(45, Tool);
		return;
	}

	#ifdef __WXMSW__
		::wxShell(L"\"" + Tool + L"\"");
	#else
		::wxExecute(L"xdg-open \"" + Tool + L"\"", wxEXEC_ASYNC,NULL);
	#endif
}

void ombMainFrame::onResize(wxSizeEvent& event){
	wxRect rect;
	StatusBar->GetFieldRect(1, rect);
	Progress->SetSize(rect.GetSize());
	Progress->SetPosition(rect.GetPosition());
	Progress->CenterOnParent(wxVERTICAL);
	event.Skip();}

void ombMainFrame::EditCategoriesClick(wxCommandEvent& event){
	int i;

	wxEditCategories *edit = new wxEditCategories(this);

	edit->CategoryList->SetImageList(CategoryIconList, wxIMAGE_LIST_NORMAL);
	edit->CategoryList->InsertColumn(0, L"");

	int ImageId;

	for(i = 0; i < Data->NCat; i++){
		ImageId = Data->Categories[i].IconIndex;
		if(ImageId == -1) ImageId = 0;
		if(ImageId > CategoryIconList->GetImageCount() - 1) ImageId = 0;
		edit->CategoryList->InsertItem(i, Data->Categories[i].Name, ImageId);
	}

	edit->CategoryList->SetColumnWidth(0, wxLIST_AUTOSIZE);

	if(edit->ShowModal() == wxID_OK){
		if(edit->modified){
			// Save custom icons
			if(edit->FirstAddedId > 0){
				bool NewTable = false;
				wxBitmap Bmp;
				wxString FileName;
				wxString Path = GetUserLocalDir() + L"/ombCategoryImgs/";
				wxString ImageFile;
				wxString ImagePath;

				#ifdef _OMB_USE_CIPHER
					if(! TableExists(L"CustomIcons", wxEmptyString, Data->database)){
						NewTable = true;
						ExecuteUpdate(Data->database, cs_customicons.c_str());
					}
				#else
					if(! Data->database->TableExists(L"CustomIcons")){
						NewTable = true;
						Data->database->ExecuteUpdate(cs_customicons);
					}
				#endif // _OMB_USE_CIPHER
				for(i = edit->FirstAddedId; i < CategoryIconList->GetImageCount(); i++){
					Bmp = CategoryIconList->GetBitmap(i);
					FileName = ::wxFileName::CreateTempFileName(GetTempFile());
					Bmp.SaveFile(FileName, wxBITMAP_TYPE_PNG);

					// -------
					ImageFile = wxEmptyString;
//					ImagePath = wxEmptyString;

					if(! wxDirExists(Path)) wxFileName::Mkdir(Path, 0777, wxPATH_MKDIR_FULL);
					::wxFileName::SplitPath(FileName, NULL, NULL, &ImageFile, NULL, wxPATH_NATIVE);
					ImagePath = Path + ImageFile + L".png";
					if(wxCopyFile(FileName, ImagePath)){
						wxRemoveFile(FileName);

						#ifdef __OPENSUSE__
							wxString Sql;
						#endif // __OPENSUSE__
						if(NewTable)
							#ifdef _OMB_USE_CIPHER
								#ifdef __OPENSUSE__
									{
										Sql = L"insert into CustomIcons values (100, '" +
																			ImageFile +
																			L"');";
										ExecuteUpdate(Data->database, Sql.c_str());
									}
								#else
									ExecuteUpdate(Data->database, L"insert into CustomIcons values (100, '" +
																		ImageFile +
																		L"');");
								#endif // __OPENSUSE__
							#else
								Data->database->ExecuteUpdate(L"insert into CustomIcons values (100, '" +
																		ImageFile +
																		L"');");
							#endif // _OMB_USE_CIPHER
						else
							#ifdef _OMB_USE_CIPHER
								#ifdef __OPENSUSE__
									{
										Sql = L"insert into CustomIcons values (NULL, '" +
																			ImageFile +
																			L"');";
										ExecuteUpdate(Data->database, Sql.c_str());
									}
								#else
									ExecuteUpdate(Data->database, L"insert into CustomIcons values (NULL, '" +
																		ImageFile +
																		L"');");
								#endif // __OPENSUSE__
							#else
								Data->database->ExecuteUpdate(L"insert into CustomIcons values (NULL, '" +
																		ImageFile +
																		L"');");
							#endif // _OMB_USE_CIPHER
					}
				}
			}

			Data->UpdateCategories(edit->CategoryList);
			ShowF(false);}}
	delete edit;}

void ombMainFrame::remove_obtained_shopitem(const wxString& obj){
	wxString S1;
	for(int i = 0; i < Data->NSho; i++){
		S1 = Data->ShopItems[i].Name.MakeLower();
		if(obj == S1){
			wxString Msg = wxString::Format(_("Do you want to remove item %s from the shopping list?"), S1.c_str());
			wxBell();
			if(wxMessageBox(Msg, _("OpenMoneyBox"), wxYES_NO) == wxYES) Data->DelShopItem(Data->ShopItems[i].Id);
			break;}}}

#ifdef __WXMSW__
  void ombMainFrame::LicenseClick(wxCommandEvent& event){
    wxString LicenseFile = GetInstallationPath();
    switch(Lan){
      case wxLANGUAGE_ITALIAN:
			#if (wxCHECK_VERSION(3, 2, 0) )
				case wxLANGUAGE_ITALIAN_ITALY:
			#endif
        LicenseFile += "\\licenza.txt";
        break;
      default:
        LicenseFile += "\\license.txt";}
    ::wxShell(LicenseFile);}
#endif // __WXMSW__

void ombMainFrame::ViewArchiveClick(wxCommandEvent& event){
	if(ViewArchive->IsChecked()){

		#ifndef _OMB_USE_CIPHER
			wxString master = Get_MasterDB();
			// check if master exists
			if(! ::wxFileExists(master)){
				ViewArchive->Check(false);
				return;
			}
		#endif // _OMB_USE_CIPHER

		// release savepoint
		if(Data->FileData.Modified){
			if(! SaveDatabase()){
				ViewArchive->Check(false);
				return;}
		}
		else
			#ifdef _OMB_USE_CIPHER
				sqlite3_exec(Data->database, "RELEASE rollback;", NULL, NULL, NULL);
			#else
				Data->database->ReleaseSavepoint(L"rollback");
			#endif // _OMB_USE_CIPHER

		// attach master archive
		#ifdef _OMB_USE_CIPHER
			Data->AttachMaster();
		#else
			Data->database->ExecuteUpdate(wxString::Format("ATTACH DATABASE '%s' AS master;", master));
		#endif // _OMB_USE_CIPHER

		/*
		// Code to convert old currency
		{
			const double Rate = 1/1936.27;
			const wxString Symbol = L"£";
			const int Max_Date = 1014940800;
      wxSQLite3Table transaction_table = GetTable(Data->database, ::wxString::Format(L"SELECT * FROM master.Transactions WHERE date < %d and type < %d order by date;", Max_Date, 9));

      int Id;
      double Value;
      wxString Update;

      for(int i = 0; i < transaction_table.GetRowCount(); i++){
      	transaction_table.SetRow(i);
      	Id = transaction_table.GetInt("id", 0);
      	Value = transaction_table.GetDouble("value", 0);
      	Value *= (1 / Rate);

      	// fix round error
      	Value = int (Value / 100);
      	Value++;
      	Value *= 100;

      	Value *= Rate;

      	Update = wxString::Format(L"update master.Transactions set Value = \"%s\" where id = %d;", wxString::FromCDouble(Value), Id);
      	ExecuteUpdate(Data->database, Update);
      }

      Update = wxString::Format(L"update master.Transactions set currencyid = 1 where date < %d and type < %d;", Max_Date, 9);
      ExecuteUpdate(Data->database, Update);

      Update = wxString::Format(L"update master.Transactions set currencyrate = \"%s\" where date < %d and type < %d;", wxString::FromCDouble(Rate), Max_Date, 9);
      ExecuteUpdate(Data->database, Update);

      Update = wxString::Format(L"update master.Transactions set currencysymb = \"%s\" where date < %d and type < %d;", Symbol, Max_Date, 9);
      ExecuteUpdate(Data->database, Update);
		}
		*/

		#ifdef _OMB_USE_CIPHER
			ExecuteUpdate(Data->database, "SAVEPOINT rollback;");
		#else
			Data->database->Savepoint(L"rollback");
		#endif // _OMB_USE_CIPHER

		Master_Attached = true;

		SearchMasterMinDate();

	}
	else{
		// release savepoint
		if(Data->FileData.Modified){
			if(! SaveDatabase()){
				ViewArchive->Check(false);
				return;}
		}
		else
			#ifdef _OMB_USE_CIPHER
				sqlite3_exec(Data->database, "RELEASE rollback;", NULL, NULL, NULL);
			#else
				Data->database->ReleaseSavepoint(L"rollback");
			#endif // _OMB_USE_CIPHER

		#ifdef _OMB_USE_CIPHER
			// detach master database
			ExecuteUpdate(Data->database, "DETACH DATABASE 'master';");
			ExecuteUpdate(Data->database, "SAVEPOINT rollback;");
		#else
			// detach master database
			Data->database->ExecuteUpdate("DETACH DATABASE 'master';");
			Data->database->Savepoint(L"rollback");
		#endif // _OMB_USE_CIPHER
		Master_Attached = false;

		// udpate calendar control minimum date
		UpdateCalendar(wxInvalidDateTime);

		ShowF();
	}

	FullTrendCheck->Enable(ViewArchive->IsChecked());

}

void ombMainFrame::ShowMaster(void){

	if(IsNewMonthShown()){
		ShowF();
		return;
	}

	int i;

	int min_date, max_date;
	wxDateTime cal_date, min_datetime, max_datetime;
	cal_date = Calendar->GetDate();
	int month = cal_date.GetMonth();
	int year = cal_date.GetYear();

	min_datetime = cal_date;
	min_datetime.SetDay(1);
	min_datetime.SetHour(0);
	min_datetime.SetMinute(0);
	min_datetime.SetSecond(0);
	min_datetime.SetMillisecond(0);
	min_date = min_datetime.GetTicks();

	max_datetime = cal_date;
	max_datetime.SetToLastMonthDay(max_datetime.GetMonth(), year);
	max_datetime.SetHour(23);
	max_datetime.SetMinute(59);
	max_datetime.SetSecond(59);
	max_datetime.SetMillisecond(999);
	max_date = max_datetime.GetTicks();

	wxString trail_name = L"_" + ::wxString::Format(L"%d", year) + L"_" + ::wxString::Format(L"%02d", int(month) + 1);

	SetCaption();

	double value, total;
	wxSQLite3Table fund_table, transaction_table, table;

	wxSQLite3Table GroupTable, ChildTable;

	// TreeList construction
	ItemList->DeleteAllItems();
	ResetTree();

	bool has_funds;
	#ifdef _OMB_USE_CIPHER
		if(TableExists(L"Funds" + trail_name, L"master", Data->database)){
	#else
		if(Data->database->TableExists(L"Funds" + trail_name, L"master")){
	#endif // _OMB_USE_CIPHER
		has_funds = true;
		total = 0;
		#ifdef _OMB_USE_CIPHER
			#ifdef __OPENSUSE__
				wxString Sql = ::wxString::Format("select * from master.Funds%s order by name;", trail_name);
				fund_table = GetTable(Data->database, Sql.c_str());
			#else
				fund_table = GetTable(Data->database, ::wxString::Format("select * from master.Funds%s order by name;", trail_name));
			#endif // __OPENSUSE__
		#else
			fund_table = Data->database->GetTable(::wxString::Format("select * from master.Funds%s order by name;", trail_name));
		#endif // _OMB_USE_CIPHER

		if(ShowFundGroups()){
			#ifdef _OMB_USE_CIPHER
				#ifdef __OPENSUSE__
					Sql = wxString::Format("select * from master.FundGroups%s where owner = '-1';", trail_name);
					GroupTable = GetTable(Data->database, Sql.c_str());
				#else
					GroupTable = GetTable(Data->database, wxString::Format("select * from master.FundGroups%s where owner = '-1';", trail_name));
				#endif // __OPENSUSE__
			#else
				GroupTable = Data->database->GetTable(wxString::Format(L"select * from master.FundGroups%s where owner = '-1';", trail_name));
			#endif // _OMB_USE_CIPHER
			int GTC = GroupTable.GetRowCount();

			double GroupTotals[GTC];
			for( i = 0; i < GTC; i++)
				GroupTotals[i] = 0;
			wxString GroupNames[GTC];
			for(i = 0; i < GTC; i++){
				GroupTable.SetRow(i);
				GroupNames[i] = GroupTable.GetAsString("name");
			}

			#ifdef _OMB_USE_CIPHER
				#ifdef __OPENSUSE__
					Sql = wxString::Format("select * from master.FundGroups%s where owner > '-1';", trail_name);
					ChildTable = GetTable(Data->database, Sql.c_str());
				#else
					ChildTable = GetTable(Data->database, wxString::Format("select * from master.FundGroups%s where owner > '-1';", trail_name));
				#endif // __OPENSUSE__
			#else
				ChildTable = Data->database->GetTable(wxString::Format(L"select * from FundGroups%s where owner > '-1';", trail_name));
			#endif // _OMB_USE_CIPHER
			int CTC = ChildTable.GetRowCount();

			int Group_ID;
			wxString FundName;
			for(i = 0; i < fund_table.GetRowCount(); i++){
				fund_table.SetRow(i);
				bool Found = false;
				FundName = fund_table.GetAsString("name");
				for(int j = 0; j < CTC; j++){
					ChildTable.SetRow(j);
					if(FundName.CmpNoCase(ChildTable.GetAsString("name")) == 0){
						Group_ID = ChildTable.GetInt("owner", -1);
						for(int k = 0; k < GTC; k++){
							GroupTable.SetRow(k);

							if(GroupTable.GetInt("id", -1) == Group_ID){
								Found = true;
								GroupTotals[k] += fund_table.GetDouble("value", 0);
								break;
							}

						}

						break;
					}
				}
				if(! Found){
					double fundtot = fund_table.GetDouble("value", 0);
					AddNode(Funds_tid, /*Data->Funds[i].Name*/FundName, FormDigits(fundtot) + L" " + GetCurrencySymbol());
					total += fundtot;
				}
			}

			for(i = 0; i < GTC; i++){
				AddNode(Funds_tid, GroupNames[i] + " - [" + _("Group") + "]", FormDigits(GroupTotals[i]) + L" " + GetCurrencySymbol());
				total += GroupTotals[i];
			}

		}
		else{

			for(i = 0; i < fund_table.GetRowCount(); i++){
				fund_table.SetRow(i);
				value = fund_table.GetDouble(L"value", 0);
				AddNode(Funds_tid, fund_table.GetString(L"name", wxEmptyString), FormDigits(value) + L" " + GetCurrencySymbol());
				total += value;
			}

		}

		AddNode(Funds_tid, TreeNames->Item(6), FormDigits(total) + L" " + GetCurrencySymbol());
	}
	else has_funds = false;

	int c_id;
	#ifdef _OMB_USE_CIPHER
		if(TableExists(L"Credits" + trail_name, L"master", Data->database)){
	#else
		if(Data->database->TableExists(L"Credits" + trail_name, L"master")){
	#endif // _OMB_USE_CIPHER
		total = 0;
		#ifdef _OMB_USE_CIPHER
			#ifdef __OPENSUSE__
				wxString Sql = ::wxString::Format("select * from master.Credits%s order by name;", trail_name);
				table = GetTable(Data->database, Sql.c_str());
			#else
				table = GetTable(Data->database, ::wxString::Format("select * from master.Credits%s order by name;", trail_name));
			#endif // __OPENSUSE__
		#else
			table = Data->database->GetTable(::wxString::Format("select * from master.Credits%s order by name;", trail_name));
		#endif // _OMB_USE_CIPHER
		if(table.GetRowCount() > 0){
			for(i = 0; i < table.GetRowCount(); i++){
				table.SetRow(i);
				value = table.GetDouble(L"value", 0);
				c_id = table.GetInt(L"contact_index", -1);
				if(c_id > -1) c_id = GetTreeViewContactImageIndex(c_id);
				AddNode(Creds_tid, table.GetString(L"name", wxEmptyString), FormDigits(value) + L" " + GetCurrencySymbol(), c_id);
				total += value;
			}
			AddNode(Creds_tid, TreeNames->Item(6), FormDigits(total) + L" " + GetCurrencySymbol());
		}
	}
	#ifdef _OMB_USE_CIPHER
		if(TableExists(L"Debts" + trail_name, L"master", Data->database)){
	#else
		if(Data->database->TableExists(L"Debts" + trail_name, L"master")){
	#endif // _OMB_USE_CIPHER
		total = 0;
		#ifdef _OMB_USE_CIPHER
			#ifdef __OPENSUSE__
				wxString Sql = ::wxString::Format("select * from master.Debts%s order by name;", trail_name);
				table = GetTable(Data->database, Sql.c_str());
			#else
				table = GetTable(Data->database, ::wxString::Format("select * from master.Debts%s order by name;", trail_name));
			#endif // __OPENSUSE__
		#else
			table = Data->database->GetTable(::wxString::Format("select * from master.Debts%s order by name;", trail_name));
		#endif // _OMB_USE_CIPHER
		if(table.GetRowCount() > 0){
			for(i = 0; i < table.GetRowCount(); i++){
				table.SetRow(i);
				value = table.GetDouble(L"value", 0);
				c_id = table.GetInt(L"contact_index", -1);
				if(c_id > -1) c_id = GetTreeViewContactImageIndex(c_id);
				AddNode(Debts_tid, table.GetString(L"name", wxEmptyString), FormDigits(value) + L" " + GetCurrencySymbol(), c_id);
			}
			AddNode(Debts_tid, TreeNames->Item(6), FormDigits(total) + L" " + GetCurrencySymbol());
		}
	}
	#ifdef _OMB_USE_CIPHER
		if(TableExists(L"Loans" + trail_name, L"master", Data->database)){
	#else
		if(Data->database->TableExists(L"Loans" + trail_name, L"master")){
	#endif // _OMB_USE_CIPHER
		#ifdef _OMB_USE_CIPHER
			#ifdef __OPENSUSE__
				wxString Sql = ::wxString::Format("select * from master.Loans%s order by name;", trail_name);
				table = GetTable(Data->database, Sql.c_str());
			#else
				table = GetTable(Data->database, ::wxString::Format("select * from master.Loans%s order by name;", trail_name));
			#endif // __OPENSUSE__
		#else
			table = Data->database->GetTable(::wxString::Format("select * from master.Loans%s order by name;", trail_name));
		#endif // _OMB_USE_CIPHER
		for(i = 0; i < table.GetRowCount(); i++){
			table.SetRow(i);
			c_id = table.GetInt(L"contact_index", -1);
			if(c_id > -1) c_id = GetTreeViewContactImageIndex(c_id);
			AddNode(Lends_tid, table.GetString(L"name", wxEmptyString), table.GetString(L"item", wxEmptyString), c_id);
		}
	}
	#ifdef _OMB_USE_CIPHER
		if(TableExists(L"Borrows" + trail_name, L"master", Data->database)){
	#else
		if(Data->database->TableExists(L"Borrows" + trail_name, L"master")){
	#endif // _OMB_USE_CIPHER
		#ifdef _OMB_USE_CIPHER
			#ifdef __OPENSUSE__
				wxString Sql = ::wxString::Format("select * from master.Borrows%s order by name;", trail_name);
				table = GetTable(Data->database, Sql.c_str());
			#else
				table = GetTable(Data->database, ::wxString::Format("select * from master.Borrows%s order by name;", trail_name));
			#endif // __OPENSUSE__
		#else
			table = Data->database->GetTable(::wxString::Format("select * from master.Borrows%s order by name;", trail_name));
		#endif // _OMB_USE_CIPHER
		for(i = 0; i < table.GetRowCount(); i++){
			table.SetRow(i);
			c_id = table.GetInt(L"contact_index", -1);
			if(c_id > -1) c_id = GetTreeViewContactImageIndex(c_id);
			AddNode(Borrowed_tid, table.GetString(L"name", wxEmptyString), table.GetString(L"item", wxEmptyString), c_id);
		}
	}

	// show report
	int CatIndex;
	wxString value_string;
	wxDateTime date, time;

	if(Report->GetNumberRows()>0)Report->DeleteRows(0,Report->GetNumberRows(),false);
	Report->SetDefaultCellTextColour(wxColour(0x00, 0xAF, 0x00));

	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      wxString Sql = ::wxString::Format(L"SELECT * FROM master.Transactions WHERE date >= %d and date <= %d order by date;", min_date, max_date);
      transaction_table = GetTable(Data->database, Sql.c_str());
    #else
      transaction_table = GetTable(Data->database, ::wxString::Format(L"SELECT * FROM master.Transactions WHERE date >= %d and date <= %d order by date;", min_date, max_date));
    #endif // __OPENSUSE__
	#else
		transaction_table = Data->database->GetTable(::wxString::Format(L"SELECT * FROM master.Transactions WHERE date >= %d and date <= %d order by date;", min_date, max_date));
	#endif // _OMB_USE_CIPHER

	Report->AppendRows(transaction_table.GetRowCount(), false);

	wxFont font = Report->GetCellFont(0, 0);
	font.MakeBold();

	bool anyContactShown = false;
	bool anyLocationShown = false;
	double latitude, longitude;

	struct Point{
		float lat;
		float lon;
	};

	struct Point point;
	FILE *fp;
	wxString MapFile;

	if(map_viewer_exist){
		fp = NULL;
		MapFile = L"/tmp/ombmapview.dat";
		if(wxFileExists(MapFile)) wxRemoveFile(MapFile);
	}

	// Check for specific categories
	bool SpecificCategories = false;
	wxSQLite3Table CatTable;
	#ifdef _OMB_USE_CIPHER
		if(TableExists(L"Categories" + trail_name, L"master", Data->database)){
	#else
		if(Data->database->TableExists(L"Categories" + trail_name, L"master")){
	#endif // _OMB_USE_CIPHER

			#ifdef _OMB_USE_CIPHER
				#ifdef __OPENSUSE__
					wxString Sql = ::wxString::Format("select * from master.Categories%s;", trail_name);
					CatTable = GetTable(Data->database, Sql.c_str());
				#else
					CatTable = GetTable(Data->database, ::wxString::Format("select * from master.Categories%s;", trail_name));
				#endif // __OPENSUSE__
			#else
				CatTable = Data->database->GetTable(::wxString::Format("select * from master.Categories%s;", trail_name));
			#endif // _OMB_USE_CIPHER

			if(CatTable.GetRowCount() > 0) SpecificCategories = true;
		}

	for(i = 0; i < transaction_table.GetRowCount(); i++){
		transaction_table.SetRow(i);
		if(transaction_table.GetInt(L"isdate", false)){
			date = wxDateTime((time_t) transaction_table.GetInt(2, 0));
			Report->SetCellValue(i, 0, date.FormatDate());
			Report->SetCellFont(i, 0, font);
			Report->SetCellValue(i, 2, GetLogString(0));
			Report->SetCellFont(i, 2, font);
			Report->SetCellFont(i, 3, font);}
		else{
			time = wxDateTime((time_t) transaction_table.GetInt(3, 0));
			Report->SetCellValue(i, 1, time.Format(L"%H:%M", wxDateTime::Local));
			Report->SetCellValue(i, 2, GetLogString(transaction_table.GetInt(4, 0)));
			Report->SetCellValue(i, 4, transaction_table.GetString(6, wxEmptyString));

			CatIndex = transaction_table.GetInt(7, 0);
			if(CatIndex != -1){
				if(SpecificCategories){
					for(int j = 0; j < CatTable.GetRowCount(); j++){
						CatTable.SetRow(j);
						if(CatTable.GetInt(0, -1) == CatIndex){
							Report->SetCellValue(i, 5, CatTable.GetString(1, wxEmptyString));
							break;
						}
					}
				}
				else for(int j = 0; j < Data->NCat; j++) if(Data->Categories[j].Id == CatIndex){
					Report->SetCellValue(i, 5, Data->Categories[j].Name);
					break;}
			}

			c_id = transaction_table.GetInt(ombTColIdContactIndex, 0);
			if(c_id > 0){
				if(Data->FindContact(c_id)){
					wxString imagepath = GetContactImage(c_id);

					#if (wxCHECK_VERSION(3, 1, 5) )
							Report->SetRowMinimalHeight(i, 48);
					#else
							Report->SetRowHeight(i, 48);
					#endif
					if(imagepath.IsEmpty()){
						// Image from https://remixicon.com/
						#ifdef __WXGTK__
							imagepath = GetDataDir() + L"images/account-box-line.png";
						#elif defined (__WXMSW__)
							imagepath = GetDataDir() + L"images\\account-box-line.png";
						#elif defined (__WXMAC__)
							imagepath = AppDir + L"/Resources/images/account-box-line.png";
						#endif // __WXGTK__
					}
					Report->SetCellRenderer(i, gcContact, new myImageGridCellRenderer(imagepath));
					anyContactShown = true;

				}
			}

			latitude = transaction_table.GetDouble(ombTColIdLatitude, 0);
			longitude = transaction_table.GetDouble(ombTColIdLongitude, 0);
			if((latitude < ombInvalidLatitude) && (longitude < ombInvalidLongitude)){

				if(map_viewer_exist){
					#ifndef __OPENSUSE__
						if(fp == NULL) fp = fopen(MapFile, "w");	// flawfinder: ignore
					#else
						if(fp == NULL) fp = fopen(MapFile.c_str(), "w");	// flawfinder: ignore
					#endif
					if(fp != NULL){
						point.lat = latitude;
						point.lon = longitude;
						fwrite(&point, sizeof(point), 1, fp);
					}
				}

				if(Report->GetRowHeight(i) < 24)
                        #if (wxCHECK_VERSION(3, 1, 5) )
                            Report->SetRowMinimalHeight(i, 24);
                        #else
                            Report->SetRowHeight(i, 24);
                        #endif
				#ifdef __WXGTK__
					Report->SetCellRenderer(i, gcLocation, new myImageGridCellRenderer(GetLocationImagePath(Report->GetRowHeight(i))));
				#elif defined (__WXMSW__)
					if(Report->GetRowHeight(i) > 24)
						Report->SetCellRenderer(i, gcLocation, new myImageGridCellRenderer(GetInstallationPath()+L"\\48\\mark-location.png"));
					else
						Report->SetCellRenderer(i, gcLocation, new myImageGridCellRenderer(GetInstallationPath()+L"\\24\\mark-location.png"));
				#elif defined (__WXMAC__)
					if(Report->GetRowHeight(i) > 24)
						Report->SetCellRenderer(i, gcLocation, new myImageGridCellRenderer(AppDir + L"/Resources/icons/48/mark-location.png"));
					else
						Report->SetCellRenderer(i, gcLocation, new myImageGridCellRenderer(AppDir + L"/Resources/icons/24/mark-location.png"));
				#endif // __WXGTK__
				anyLocationShown = true;
			}

		}
		value_string = transaction_table.GetString(5, wxEmptyString);
		if(transaction_table.GetInt(4, 0) < 9){
			double cur;

			value_string.ToCDouble(&cur);

			if(transaction_table.GetInt(ombTColIdCurrencyId, -1) >= 0){
				cur /= transaction_table.GetDouble(ombTColIdCurrencyRate, 1);
				Report->SetCellValue(i, 3, wxString::Format(L"( %s %s )", FormDigits(cur), transaction_table.GetString(ombTColIdCurrencySymbol)));
				wxColor col = wxColour(L"GREEN");
				Report->SetCellBackgroundColour(i, 3, col);
				col = wxColour(L"WHITE");
				Report->SetCellTextColour(i, 3, col);
			}
			else Report->SetCellValue(i, 3, FormDigits(cur));
			if(Report->GetCellValue(i, 3).IsEmpty())
				Report->SetCellValue(i, 3, _("Zero"));}
		else Report->SetCellValue(i, 3, value_string);
		Report->SetCellAlignment(i,3,wxALIGN_RIGHT,wxALIGN_CENTRE);}

	if(map_viewer_exist){
		if(fp == NULL) Map->Enable(false);
		else{
			fclose(fp);
			Map->Enable();
		}
	}

	Report->AutoSizeColumns(false);
	Report->SelectRow(Report->GetNumberRows() - 1, false);
	Report->MakeCellVisible(Report->GetNumberRows() - 1, 0);
	if(anyContactShown) Report->ShowCol(gcContact);
	else Report->HideCol(gcContact);
	if(anyLocationShown) Report->ShowCol(gcLocation);
	else Report->HideCol(gcLocation);

	TrendChart->Show(ShowTrendChart());
	FundChart->Show(ShowFundChart());
	if(TrendChart->IsShown()||FundChart->IsShown()){
		PaintMasterChart(has_funds, fund_table, transaction_table, GroupTable, ChildTable);
		if(FullTrendCheck->IsChecked()) PaintFullTrendChart();
	}

	Update();}

void ombMainFrame::PaintMasterChart(bool hasfunds, wxSQLite3Table &funds, wxSQLite3Table &transactions
		, wxSQLite3Table &FundGroups, wxSQLite3Table &FundChildren
		){
	int i;
	if(ShowFundChart() && hasfunds){
		int fund_number = funds.GetRowCount();

		#ifdef _OMB_CHART_WXPIECTRL

			int cindex = 0;	// colour array index
			double percent;
			double sum_small_funds = 0, tot_funds = 0;
			/*
			wxColour colours[funds.GetRowCount()];

			while(cindex < fund_number){
				switch(i){
					case 1:
					colours[cindex] = wxColour(0xFF,0xFF, 0x99);
					break;
					case 2:
					colours[cindex] = wxColour(0x3D, 0xEB, 0x3D);
					break;
					case 3:
					colours[cindex] = wxColour(0xFF, 0x00, 0x00);
					break;
					case 4:
					colours[cindex] = wxColour(0x00, 0xFF, 0x00);
					break;
					case 5:
					colours[cindex] = wxColour(0x00, 0x00, 0xFF);
					break;
					default:
					colours[cindex] = wxColour(0x99, 0xCC, 0xFF);
					i = 0;}
				i++;
				cindex++;}
			*/
						wxColour colours[] = {
				wxColour(0x99, 0xCC, 0xFF),
				wxColour(0xFF, 0xFF, 0x99),
				wxColour(0x3D, 0xEB, 0x3D),
				wxColour(0xFF, 0x00, 0x00),
				wxColour(0x00, 0xFF, 0xFF),
				wxColour(0x00, 0x00, 0xFF)
			};

			// initialize pie control
			FundChart->m_Series.Clear();
			wxPiePart part;

			for(i = 0; i < fund_number; i++){
				funds.SetRow(i);
				tot_funds += funds.GetDouble(L"value", 0);}

			if(ShowFundGroups()){
				int GTC = FundGroups.GetRowCount();

				double GroupTotals[GTC];
				for( i = 0; i < GTC; i++)
					GroupTotals[i] = 0;
				wxString GroupNames[GTC];
				for(i = 0; i < GTC; i++){
					FundGroups.SetRow(i);
					GroupNames[i] = FundGroups.GetAsString("name");
				}

				int CTC = FundChildren.GetRowCount();
				cindex = 0;

				int Group_ID;
				wxString FundName;
				for(i = 0; i < fund_number; i++){
					funds.SetRow(i);
					bool Found = false;
					FundName = funds.GetAsString("name");

					for(int j = 0; j < CTC; j++){
						FundChildren.SetRow(j);
						if(FundName.CmpNoCase(FundChildren.GetAsString("name")) == 0){
							Group_ID = FundChildren.GetInt("owner", -1);
							for(int k = 0; k < GTC; k++){
								FundGroups.SetRow(k);

								if(FundGroups.GetInt("id", -1) == Group_ID){
									Found = true;
									GroupTotals[k] += funds.GetDouble("value", 0);
									break;
								}

							}

							break;
						}
					}
					if(! Found){
						double fund_value = funds.GetDouble("value", 0);
						percent = fund_value / tot_funds * 100;
						if((percent) > 1){
							part.SetLabel(funds.GetAsString("name") + wxString::Format(L" %.1f", percent) + L"%");
							part.SetValue(fund_value);

							part.SetColour(colours[cindex]);
							cindex++;
							if(cindex > 5) cindex = 0;

							FundChart->m_Series.Add(part);
						}
						else{
							sum_small_funds += fund_value;
						}

					}
				}

				for(i = 0; i < GTC; i++){
					percent = GroupTotals[i] / tot_funds * 100;
					if((percent) > 1){
						part.SetLabel(GroupNames[i] + " - [" + _("Group") + "]" + wxString::Format(L" %.1f", percent) + L"%");
						part.SetValue(GroupTotals[i]);

						part.SetColour(colours[cindex]);
						cindex++;
						if(cindex > 5) cindex = 0;

						FundChart->m_Series.Add(part);
					}
					else{
						sum_small_funds += GroupTotals[i];
					}

				}

			}
			else{

				for(i = 0; i < fund_number; i++){
					funds.SetRow(i);
					percent = funds.GetDouble(L"value", 0) / tot_funds * 100;
					if((percent) > 1){
						part.SetLabel(funds.GetString(L"name", wxEmptyString) + wxString::Format(L" %.1f", percent) + L"%");
						part.SetValue(funds.GetDouble(L"value", 0));
						part.SetColour(colours[/*i*/cindex]);
						cindex++;
						if(cindex > 5) cindex = 0;
						FundChart->m_Series.Add(part);
					}
					else sum_small_funds += funds.GetDouble(L"value", 0);
				}

			}

			if(sum_small_funds){
				percent = sum_small_funds / tot_funds * 100;
				wxString title;
				title = _("Others");
				title += wxString::Format(L" %.1f", percent) + L"%";
				part.SetLabel(title);
				part.SetValue(sum_small_funds);
				part.SetColour(colours[/*0*/cindex]);
				FundChart->m_Series.Add(part);

			}

			FundChart->Refresh();

		#elif defined ( _OMB_CHART_MATLIBPLOT )
			double sum_small_funds = 0, tot_funds = 0;

			wxString tmp_file, data_fname, chart_fname, line;
			wxString Proc = L"python " + GetDataDir();
			Proc += L"pie.py ";
			Proc += L"/usr/share/locale ";	// TODO (igor#1#): Check if it is possible to get this path programmatically

			tmp_file = GetTempFile();
			data_fname = ::wxFileName::CreateTempFileName(tmp_file);
			wxTextFile *file = new wxTextFile(data_fname);
			file->Create();

			for(i = 0; i < fund_number; i++){
				funds.SetRow(i);
				tot_funds += funds.GetDouble(L"value", 0);}

			for(i = 0; i < fund_number; i++){
				funds.SetRow(i);
				if((funds.GetDouble(L"value", 0) / tot_funds) > 0.01){
					line = funds.GetString("name", wxEmptyString) + ", " + wxString::FromCDouble(funds.GetDouble("value", 0), 2) + ", 0";
					file->AddLine(line);}
				else sum_small_funds += funds.GetDouble(L"value", 0);
			}
			if(sum_small_funds){
				line = _("Others") + ", " + wxString::FromCDouble(sum_small_funds, 2) + ", 0";
				file->AddLine(line);
			}

			file->Write(wxTextFileType_Unix,wxConvUTF8);
			// file closure
			file->Close();
			delete file;

			Proc += data_fname + L" ";

			chart_fname = ::wxFileName::CreateTempFileName(tmp_file);
			::wxRemoveFile(chart_fname);
			chart_fname += L".png";
			Proc += chart_fname;

			wxExecute(Proc, wxEXEC_SYNC);
			::wxRemoveFile(data_fname);

			FundChart->SetBitmap(GetScaledBitmap(chart_fname, FundChart));

			::wxRemoveFile(chart_fname);

		#endif // _OMB_CHART_WXPIECTRL
	}

	if(ShowTrendChart()){
		// Trend chart construction
		wxDateTime LastDate = Calendar->GetDate();
		LastDate.SetDay(1);
		int number_of_days = wxDateTime::GetNumberOfDays(LastDate.GetMonth(), LastDate.GetYear(), wxDateTime::Gregorian);

		double trendvalues[number_of_days];
		for(i = 0; i < number_of_days; i++) trendvalues[i] = -1;
		const wxChar *trenddates[number_of_days];
		time_t times[WXSIZEOF(trenddates)];
		double	LastValue = 0,
						MinValue = 0,
						MaxValue = 0;

		wxDateTime date, dt;

		int j = 0;
		for(i = 0; i < transactions.GetRowCount(); i++){
			transactions.SetRow(i);
			if(transactions.GetInt(L"isdate", false)){
				date = wxDateTime((time_t) transactions.GetInt(2, 0));
				#ifndef _OMB_CHART_WXPIECTRL
					j = date.GetDay() - 1;
				#endif	// _OMB_CHART_WXPIECTRL
				LastDate = date;
				transactions.GetString(5, wxEmptyString).ToCDouble(&LastValue);
				trendvalues[j] = LastValue;
				times[j] = LastDate.GetTicks();
				#ifdef _OMB_CHART_WXPIECTRL
					j++;
				#endif // _OMB_CHART_WXPIECTRL

				if(LastValue < MinValue) MinValue = LastValue;
				if(LastValue > MaxValue) MaxValue = LastValue;
			}}

		#ifndef _OMB_CHART_WXPIECTRL
			bool	found_before = false,
						found_after;
			int k;
			for(i = 0; i < number_of_days; i++){
				if(trendvalues[i] == -1){
					if(found_before){
						found_after = false;
						for(k = i+1; k < number_of_days; k++)
							if(trendvalues[k] != -1){
								found_after = true;
								break;}

						if(found_after) trendvalues[i] = trendvalues[i-1];
						else trendvalues[i] = 0;}
					else trendvalues[i] = 0;
				dt = wxDateTime(i + 1, Calendar->GetDate().GetMonth(), Calendar->GetDate().GetYear(), 0, 0, 0, 0);
				times[i] = dt.GetTicks();}
				else found_before = true;}
			#endif // _OMB_CHART_WXPIECTRL

		#ifdef _OMB_CHART_WXPIECTRL
			DAILY_STATS *ds = new DAILY_STATS[j];
			for(i = 0; i < j; i++){
				ds[i].day = times[i] + 10000;	// adding about 3 hours for better visualization
				ds[i].total_credit = trendvalues[i];
			}

			TrendChart->nstats = j;
			TrendChart->stats = ds;

			TrendChart->trend_value = -1;

			TrendChart->m_full_repaint = true;
			TrendChart->Refresh();

		#elif defined ( _OMB_CHART_MATLIBPLOT )
			wxString tmp_file, data_fname, chart_fname, line;
			wxString Proc = L"python " + GetDataDir();
			Proc += L"trend.py ";
			Proc += L"/usr/share/locale ";	// TODO (igor#1#): Check if it is possible to get this path programmatically

			tmp_file = GetTempFile();
			data_fname = ::wxFileName::CreateTempFileName(tmp_file);
			wxTextFile *file = new wxTextFile(data_fname);
			file->Create();

			for(i = 0; i < number_of_days; i++)
				file->AddLine(wxString::Format(L"%d", int(times[i])) + L", " + wxString::FromCDouble(trendvalues[i], 2));

			file->Write(wxTextFileType_Unix,wxConvUTF8);
			// file closure
			file->Close();
			delete file;

			Proc += data_fname + L" ";

			chart_fname = ::wxFileName::CreateTempFileName(tmp_file);
			::wxRemoveFile(chart_fname);
			chart_fname += L".png";
			Proc += chart_fname;

			wxExecute(Proc, wxEXEC_SYNC);
			::wxRemoveFile(data_fname);

			TrendChart->SetBitmap(GetScaledBitmap(chart_fname, TrendChart, true));

			::wxRemoveFile(chart_fname);

		#endif // _OMB_CHART_WXPIECTRL
	}
	FreshChartShown = false;
}

wxString ombMainFrame::BrowseDocument(void){
	wxFileDialog *Dialog=new wxFileDialog(wxTheApp->GetTopWindow(),wxEmptyString,wxEmptyString,wxEmptyString,wxEmptyString,wxFD_OPEN,wxDefaultPosition,wxDefaultSize,L"Dialog");
	Dialog->SetDirectory(GetOSDocDir());
	wxString filter = _("OpenMoneyBox documents");
	filter += L"|*.omb;*.OMB";
	Dialog->SetWildcard(filter);
	if(Dialog->ShowModal() == wxID_OK){
		wxString File = Dialog->GetPath();
		delete Dialog;

			#ifdef _OMB_USE_CIPHER
				#ifdef _OMB_SQLCIPHER_v4
				wxString pwd = GetKey();
			#endif // _OMB_SQLCIPHER_v4
		#endif // _OMB_USE_CIPHER

		if(CheckAndPromptForConversion(File, false
																		#ifdef _OMB_USE_CIPHER
																			#ifdef _OMB_SQLCIPHER_v4
																				, pwd, GetTargetSqlCipherCompatibility() == 4
																			#endif // _OMB_SQLCIPHER_v4
																		#endif // _OMB_USE_CIPHER
																		)) return File;
		else return wxEmptyString;
	}

	return wxEmptyString;
}

void ombMainFrame::ImportDocument(wxCommandEvent& event){
	wxString File = BrowseDocument();
	if(File == wxEmptyString) return;

	#ifdef _OMB_USE_CIPHER
		int Response;
		wxString Pwd /*= wxEmptyString*/;
		TPassF *PassF = new TPassF(wxTheApp->GetTopWindow());
		do{
			Response = PassF->ShowModal();}
		while(Response == wxID_RETRY);
		Pwd = PassF->Pass->GetValue();
		delete PassF;
		if(Response == wxID_CANCEL) return;
	#endif // _OMB_USE_CIPHER

	wxString master = Get_MasterDB();
	wxString trailname = Data->GenerateTrailName(File);

	// release savepoint
	if(Data->FileData.Modified){
		if(! SaveDatabase()) return;
	}
	else {
		#ifdef _OMB_USE_CIPHER
			sqlite3_exec(Data->database, "RELEASE rollback;", NULL, NULL, NULL);
			if(Master_Attached) {
				wxString Sql = L"DETACH DATABASE 'master';";
				ExecuteUpdate(Data->database, Sql.c_str());
			}
		#else
			Data->database->ReleaseSavepoint(L"rollback");
			if(Master_Attached) Data->database->ExecuteUpdate(L"DETACH DATABASE 'master';");
		#endif // _OMB_USE_CIPHER
	}

	Data->Prepare_MasterDB(master, trailname);

	#ifdef _OMB_USE_CIPHER
		#ifdef __OPENSUSE__
			wxString Sql = wxString::Format("ATTACH DATABASE '%s' AS import Key '%s';", File, Pwd);
			ExecuteUpdate(Data->database, Sql.c_str());	// TODO (igor#1#): Implement password for imported database
		#else
			ExecuteUpdate(Data->database, wxString::Format("ATTACH DATABASE '%s' AS import Key '%s';", File, Pwd));	// TODO (igor#1#): Implement password for imported database
		#endif // __OPENSUSE__
	#else
		Data->database->ExecuteUpdate(wxString::Format("ATTACH DATABASE '%s' AS import;", File));
	#endif // _OMB_USE_CIPHER
	Data->Import_inMaster(master, trailname);
	#ifdef _OMB_USE_CIPHER
		#ifndef __OPENSUSE__
			wxString
		#endif // __OPENSUSE__
		Sql = L"DETACH DATABASE 'import';";
		ExecuteUpdate(Data->database, Sql.c_str());
	#else
		Data->database->ExecuteUpdate(L"DETACH DATABASE 'import';");
	#endif // _OMB_USE_CIPHER

	#ifdef _OMB_USE_CIPHER
		if(Master_Attached)	Data->AttachMaster();
	#else
		if(Master_Attached)	Data->database->ExecuteUpdate(wxString::Format("ATTACH DATABASE '%s' AS master;", master));
	#endif // _OMB_USE_CIPHER

	#ifdef _OMB_USE_CIPHER
		ExecuteUpdate(Data->database, "SAVEPOINT rollback;");
	#else
		Data->database->Savepoint(L"rollback");
	#endif // _OMB_USE_CIPHER

	if(Master_Attached){
		SearchMasterMinDate();
		ShowMaster();
	}
}

void ombMainFrame::SearchMasterMinDate(void){
	// look for oldest transaction
	wxSQLite3Table Table;
	#ifdef _OMB_USE_CIPHER
		Table = GetTable(Data->database, "select min (date) from master.transactions;");
	#else
		Table = Data->database->GetTable("select min (date) from master.transactions;");
	#endif // _OMB_USE_CIPHER
	Table.SetRow(0);
	int minimum = Table.GetInt(0, 0);

	// udpate calendar control minimum date
	wxDateTime min_date = wxDateTime((time_t) minimum);
	min_date.SetDay(1);
	min_date.SetHour(0);
	min_date.SetMinute(0);
	min_date.SetSecond(0);
	min_date.SetMillisecond(0);
	UpdateCalendar(min_date);
}

#ifdef _OMB_CHART_MATLIBPLOT
	wxBitmap ombMainFrame::GetScaledBitmap(wxString chart, wxStaticBitmap* panel, bool trend){
		int new_x, new_y;
		double scale_ratio;
		wxBitmap chart_bmp = wxBitmap(chart);
		if(trend) Trend_bmp = chart_bmp;
		else Fund_bmp = chart_bmp;
		wxImage image = chart_bmp.ConvertToImage();
		wxSize sz_client = panel->GetSize();
		wxSize sz_image;
	 	sz_image = image.GetSize();
		if(trend || (sz_client.GetX() < sz_client.GetY())){
			new_x = sz_client.GetX();
			scale_ratio = (double) sz_image.GetX() / new_x;
			if(scale_ratio || trend) new_y = sz_image.GetY() / scale_ratio;
			else{
				scale_ratio = (double) new_x / sz_image.GetX();
				new_y = sz_image.GetY() * scale_ratio;
			}
		}
		else{
			new_y = sz_client.GetY();
			scale_ratio = (double) sz_image.GetY() / new_y;
			if(scale_ratio) new_x = sz_image.GetX() / scale_ratio;
			else{
				scale_ratio = (double) new_y / sz_image.GetY();
				new_x = sz_image.GetX() * scale_ratio;
			}
		}

		if(trend) image = image.Scale(sz_client.GetX(), sz_client.GetY(), wxIMAGE_QUALITY_NORMAL);
		else image = image.Scale(new_x, new_y, wxIMAGE_QUALITY_NORMAL);
		/*funds =*/return wxBitmap(image);

	}
#endif // _OMB_CHART_MATLIBPLOT

#ifdef _OMB_CHART_MATLIBPLOT
	void ombMainFrame::FundResize( wxSplitterEvent& event ){
		if(FreshChartShown) PaintChart();
	}
#endif // _OMB_CHART_MATLIBPLOT

void ombMainFrame::CheckDate(void){
	wxDateTime date = Calendar->GetDate();
	if(calmindate.IsValid()){
		if(date < calmindate) date = calmindate;
		else if(date > calmaxdate) date = calmaxdate;
		Calendar->SetDate(date);
	}
	Data->Day = date;
}

wxString ombMainFrame::GetTempFile(void){
	wxString tempfile;
	wxStandardPathsBase& Paths=::wxStandardPaths::Get();
	#ifdef __WXMSW__
		tempfile = Paths.GetTempDir() + L"\\omb";
	#else
		tempfile = Paths.GetTempDir() + L"/omb";
	#endif // __WXMSW__
 	return tempfile;}

wxString ombMainFrame::GetContactImage(long c_id){
	wxString userfolder = GetUserLocalDir();
	wxString path = userfolder + L"/ombContactImgs/";
	wxString imgfile = wxString::Format(L"%ld.png", c_id);
	wxString imagepath = wxEmptyString;

	wxString testfile = path + imgfile;
	if(wxFileExists(testfile)){
		imagepath = testfile;
	}
	else{
		path = ::wxPathOnly(Data->FileData.FileName) + L"/ombContactImgs/";;
		testfile = path + imgfile;
		if(wxFileExists(testfile)){
			path = userfolder + L"/ombContactImgs/";
			if(! wxDirExists(path)) wxFileName::Mkdir(path, 0777, wxPATH_MKDIR_FULL);
			path += imgfile;
			if(wxCopyFile(testfile, path))
				wxRemoveFile(testfile);
			imagepath = path;
		}
	}
	return imagepath;
}

int ombMainFrame::GetTreeViewContactImageIndex(long c_id){
	int index = -1;
	wxString imagepath = GetContactImage(c_id);

	if(imagepath.IsEmpty()){
		// Image from https://remixicon.com/
		#ifdef __WXGTK__
			imagepath = GetDataDir() + L"images/account-box-line.png";
		#elif defined (__WXMSW__)
			imagepath = GetDataDir() + L"images\\account-box-line.png";
		#elif defined (__WXMAC__)
			imagepath = AppDir + L"/Resources/images/account-box-line.png";
		#endif // __WXGTK__
	}

	wxImage cellImage;

	if (cellImage.LoadFile(imagepath))
	{
		cellImage.Rescale(20, 20);	// TODO (igor#1#): Make sure no assertion is raised on MSW.
		wxBitmap cellBitmap(cellImage);

		wxImageList *imgList = ItemList->GetImageList();
		index = imgList->Add(cellBitmap);
		ItemList->SetImageList(imgList);
	}
	return index;
}

void ombMainFrame::ShowMap(wxCommandEvent& event){
	wxString Proc;
	#ifdef __WXMSW__
		// TODO (igor#1#): Implement Windows path
		//Proc="\""+GetInstallationPath()+L"\\ombconvert.exe\"";
	#else
		Proc = MapBinary;
	#endif
		if (MapPID > 0) wxKill(MapPID);
		MapPID = wxExecute(Proc, wxEXEC_ASYNC);
}

bool ombMainFrame::IsNewMonthShown(void){
	wxDateTime cal_date = Calendar->GetDate();
	int month = cal_date.GetMonth();
	int year = cal_date.GetYear();

	return (((month >= Data->FileData.Month - 1) && (year == Data->FileData.Year)) || (year > Data->FileData.Year));
}

void ombMainFrame::SetCaption(void){
	#ifdef __WXGTK__
		if(isGnome) SetTitle(CreateTitle(true));
		else Title->SetLabel(CreateTitle(true));
	#else
		Title->SetLabel(CreateTitle(true));
	#endif // __WXGTK__
}

	#ifndef __OMBCONVERT_BIN__
		void ombMainFrame::AuthorWindow(void){
			#if _OMB_HASNATIVEABOUT == 0
				#ifdef __WXMSW__
                    #ifdef __amd64__
                        About(ProductName,ShortVersion,LongVersion,wxString::Format("%u",(unsigned long long) &__BUILD_DATE));
                    #else
                        About(ProductName,ShortVersion,LongVersion,wxString::Format("%u",(unsigned long) &__BUILD_DATE));
                    #endif // __amd64__
				#else
					About(ProductName,ShortVersion,LongVersion,wxString::Format(L"%u",(unsigned long) &__BUILD_DATE));
				#endif // __WXMSW__
			#else
				wxAboutDialogInfo info;
				info.SetName(_("OpenMoneyBox"));
				info.SetVersion(LongVersion);
				info.SetDescription(_("Budget management"));
				info.SetCopyright(Copyright);
				info.SetWebSite(WebAddress);

				info.SetIcon(wxICON(Logo));

				wxString lic = wxEmptyString;

				#ifdef __WXGTK__
					wxString LicenseFile = GetShareDir() + L"doc/openmoneybox/licenses/";
				#else
					wxString LicenseFile = AppDir + L"/licenses/";
				#endif // __WXGTK

				switch(Lan){
					case wxLANGUAGE_ITALIAN:
					#if (wxCHECK_VERSION(3, 2, 0) )
						case wxLANGUAGE_ITALIAN_ITALY:
					#endif
						LicenseFile += L"it/licenza.txt";
						break;
					default:
						LicenseFile += L"en/license.txt";}

				wxTextFile *license= new wxTextFile(LicenseFile);
				license->Open();
				lic += license->GetFirstLine() + L"\n";
				while(!license->Eof()){
					lic += license->GetNextLine() + L"\n";
				}
				license->Close();
				info.SetLicense(lic);

				info.AddDeveloper(Copyright);
				info.AddDeveloper(wxEmptyString);
				info.AddDeveloper(wxString::Format(_("Coded using %s."),wxGetLibraryVersionInfo().GetVersionString()));
				info.AddTranslator(_("English: ") + Copyright);
				info.AddTranslator(_("Italian: ") + Copyright);
				info.AddTranslator(_("French: ") + L"Jean-Marc <Unknown>");	// https://launchpad.net/~m-balthazar
				info.AddTranslator(_("Swedish: ") + L"Simon Nilsson <Simon@Observeramera.com>");
				wxAboutBox(info);
			#endif // _OMB_HASNATIVEABOUT == 0
		}
	#endif // __OMBCONVERT_BIN__

	wxString ombMainFrame::GetLocationImagePath(int Height){
		wxColour m_BackColour = wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW );
		unsigned char r, g, b;
		r = m_BackColour.Red();
		g = m_BackColour.Green();
		b = m_BackColour.Blue();

		int bg_ref = r + g + b;
		wxString ImagePath = L"/usr/";

		#ifdef __FREEBSD__
			#ifndef __OPENSOLARIS__
				ImagePath += L"local/";
			#endif // __OPENSOLARIS__
		#endif

		if(bg_ref > 120)
			#ifdef __FLATPAK__
				ImagePath = L"/var/lib/flatpak/app/com.igisw.openmoneybox/current/active/files/share/icons/hicolor/";
			#elif defined (__OPENSOLARIS__)
				ImagePath += L"share/icons/ContrastHigh/";
			#else
				ImagePath += L"share/icons/HighContrast/";
			#endif // __FLATPAK__
		else
			ImagePath += L"share/icons/hicolor/";
		if(Height > 24)
			ImagePath += L"48x48/";
		else ImagePath += L"24x24/";
		ImagePath += L"actions/mark-location.png";
		return ImagePath;
	}

	void ombMainFrame::ReportCategoryEdit(wxGridEvent& event){
		if(event.GetCol() != 5) return;

		int Row = event.GetRow();
		wxString Category = Report->GetCellValue(Row, 5);
		for(int j = 0; j < Data->NCat; j++) if(Data->Categories[j].Name.compare(Category) == 0){
			OriginalCategoryIndex = Data->Categories[j].Id;
			break;
		}
	}

	void ombMainFrame::ReportCategorySelected(wxGridEvent& event){
		if(event.GetCol() != 5) return;

		int Row = event.GetRow();
		int CategoryIndex = -1;
		wxString Category = Report->GetCellValue(Row, 5);
		for(int j = 0; j < Data->NCat; j++) if(Data->Categories[j].Name.compare(Category) == 0){
			CategoryIndex = Data->Categories[j].Id;
			break;
		}
		if(OriginalCategoryIndex != CategoryIndex){
			Data->ChangeTransactionCategory(Data->Lines[Row].Id, CategoryIndex);
			Data->FileData.Modified = true;
			ShowF();
		}
	}

void ombMainFrame::PaintFullTrendChart(void){
	int i = 0;

	wxSQLite3Table DateTable;

	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      wxString Sql = L"SELECT * FROM master.Transactions WHERE isdate = 1 order by date;";
      DateTable = GetTable(Data->database, Sql.c_str());
    #else
      DateTable = GetTable(Data->database, wxString(L"SELECT * FROM master.Transactions WHERE isdate = 1 order by date;"));
    #endif // __OPENSUSE__
	#else
		DateTable = Data->database->GetTable(L"SELECT * FROM master.Transactions WHERE isdate = 1 order by date;");
	#endif // _OMB_USE_CIPHER


	if(ShowTrendChart()){
		// Trend chart construction
		wxDateTime::Month m;
		switch(Data->FileData.Month){
			case 1:
				m = wxDateTime::Jan;
				break;
			case 2:
				m = wxDateTime::Feb;
				break;
			case 3:
				m = wxDateTime::Mar;
				break;
			case 4:
				m = wxDateTime::Apr;
				break;
			case 5:
				m = wxDateTime::May;
				break;
			case 6:
				m = wxDateTime::Jun;
				break;
			case 7:
				m = wxDateTime::Jul;
				break;
			case 8:
				m = wxDateTime::Aug;
				break;
			case 9:
				m = wxDateTime::Sep;
				break;
			case 10:
				m = wxDateTime::Oct;
				break;
			case 11:
				m = wxDateTime::Nov;
				break;
			case 12:
				m = wxDateTime::Dec;
				break;
			default:
				m = wxDateTime::Inv_Month;}

		wxDateTime LastDate = Calendar->GetDate();
		LastDate.SetDay(1);
		int number_of_days = DateTable.GetRowCount() + wxDateTime::GetNumberOfDays(m, Data->FileData.Year, wxDateTime::Gregorian);

		double trendvalues[number_of_days];
		for(i = 0; i < number_of_days; i++) trendvalues[i] = -1;
		const wxChar *trenddates[number_of_days];
		time_t times[WXSIZEOF(trenddates)];
		double	LastValue = 0,
						MinValue = 0,
						MaxValue = 0;

		wxDateTime date, dt;

		int j = 0;
		for(i = 0; i < DateTable.GetRowCount(); i++){
			DateTable.SetRow(i);
			if(DateTable.GetInt(L"isdate", false)){
				date = wxDateTime((time_t) DateTable.GetInt(2, 0));
				#ifndef _OMB_CHART_WXPIECTRL
					j = date.GetDay() - 1;
				#endif	// _OMB_CHART_WXPIECTRL
				LastDate = date;
				DateTable.GetString(5, wxEmptyString).ToCDouble(&LastValue);
				trendvalues[j] = LastValue;
				times[j] = LastDate.GetTicks();
				#ifdef _OMB_CHART_WXPIECTRL
					j++;
				#endif // _OMB_CHART_WXPIECTRL

				if(LastValue < MinValue) MinValue = LastValue;
				if(LastValue > MaxValue) MaxValue = LastValue;
			}}

		#ifndef _OMB_CHART_WXPIECTRL
			bool	found_before = false,
						found_after;
			int k;
			for(i = 0; i < number_of_days; i++){
				if(trendvalues[i] == -1){
					if(found_before){
						found_after = false;
						for(k = i+1; k < number_of_days; k++)
							if(trendvalues[k] != -1){
								found_after = true;
								break;}

						if(found_after) trendvalues[i] = trendvalues[i-1];
						else trendvalues[i] = 0;}
					else trendvalues[i] = 0;
				dt = wxDateTime(i + 1, Calendar->GetDate().GetMonth(), Calendar->GetDate().GetYear(), 0, 0, 0, 0);
				times[i] = dt.GetTicks();}
				else found_before = true;}
			#endif // _OMB_CHART_WXPIECTRL

		#ifdef _OMB_CHART_WXPIECTRL
			/*
			DAILY_STATS *ds = new DAILY_STATS[j];
			for(i = 0; i < j; i++){
				ds[i].day = times[i] + 10000;	// adding about 3 hours for better visualization
				ds[i].total_credit = trendvalues[i];
			}

			TrendChart->nstats = j;
			TrendChart->stats = ds;

			TrendChart->trend_value = -1;

			TrendChart->m_full_repaint = true;
			TrendChart->Refresh();
			*/
		#elif defined ( _OMB_CHART_MATLIBPLOT )
			wxString tmp_file, data_fname, chart_fname, line;
			wxString Proc = L"python " + GetDataDir();
			Proc += L"trend.py ";
			Proc += L"/usr/share/locale ";	// TODO (igor#1#): Check if it is possible to get this path programmatically

			tmp_file = GetTempFile();
			data_fname = ::wxFileName::CreateTempFileName(tmp_file);
			wxTextFile *file = new wxTextFile(data_fname);
			file->Create();

			for(i = 0; i < number_of_days; i++)
				file->AddLine(wxString::Format(L"%d", int(times[i])) + L", " + wxString::FromCDouble(trendvalues[i], 2));

			file->Write(wxTextFileType_Unix,wxConvUTF8);
			// file closure
			file->Close();
			delete file;

			Proc += data_fname + L" ";

			chart_fname = ::wxFileName::CreateTempFileName(tmp_file);
			::wxRemoveFile(chart_fname);
			chart_fname += L".png";
			Proc += chart_fname;

			wxExecute(Proc, wxEXEC_SYNC);
			::wxRemoveFile(data_fname);

			TrendChart->SetBitmap(GetScaledBitmap(chart_fname, TrendChart, true));

			::wxRemoveFile(chart_fname);

		#endif // _OMB_CHART_WXPIECTRL


		for(i = 0; i < Data->NLin; i++){
			if(Data->IsDate(i)){
				#ifndef _OMB_CHART_WXPIECTRL
					j = Data->Lines[i].Date.GetDay() - 1;
				#endif // _OMB_CHART_WXPIECTRL
				LastDate = Data->Lines[i].Date;
				Data->Lines[i].Value.ToDouble(&LastValue);
				trendvalues[j]=LastValue;
				times[j]=LastDate.GetTicks();
				#ifdef _OMB_CHART_WXPIECTRL
					j++;
				#endif // _OMB_CHART_WXPIECTRL
			}}

		#ifndef _OMB_CHART_WXPIECTRL
			bool	found_before = false,
						found_after;
			int k;
			for(i = 0; i < number_of_days; i++){
				if(trendvalues[i] == -1){
					if(found_before){
						found_after = false;
						for(k = i+1; k < number_of_days; k++)
							if(trendvalues[k] != -1){
								found_after = true;
								break;}

						if(found_after) trendvalues[i] = trendvalues[i-1];
						else trendvalues[i] = 0;}
					else trendvalues[i] = 0;
					dt = wxDateTime(i+1, m, Data->FileData.Year, 0, 0, 0, 0);
					times[i] = dt.GetTicks();}
				else found_before = true;}
		#endif // _OMB_CHART_WXPIECTRL

		#ifdef _OMB_CHART_WXPIECTRL
			DAILY_STATS *ds = new DAILY_STATS[j];
			for(i = 0; i < j; i++){
				ds[i].day = times[i];
				ds[i].total_credit = trendvalues[i];
			}

			TrendChart->nstats = j;
			TrendChart->stats = ds;

			if(Data->FileData.ReadOnly) TrendChart->trend_value = -1;
			else{
				// NOTE (igor#1#): Hidden feature
				#ifdef _OMB_USE_CIPHER
					if(Data->ExternalFileAttached){
						double TotFundAttached = 0;
						wxSQLite3Table AttachedFundTable = Data->GetExternalFundTable();
						for(j = 0; j < AttachedFundTable.GetRowCount(); j++){
							AttachedFundTable.SetRow(j);
							TotFundAttached += AttachedFundTable.GetDouble(L"value", 0);
						}
						TrendChart->trend_value = Data->GetTot(tvFou) + TotFundAttached;
					}
					else
				#endif // _OMB_USE_CIPHER
				TrendChart->trend_value = Data->GetTot(tvFou);
				LastDate.Set(LastDate.GetLastMonthDay().GetDay(), LastDate.GetMonth(), LastDate.GetYear(), 23, 59, 59, 0);	// Setting to 23:59 for better visualization
				TrendChart->trend_date = LastDate;
			}

			TrendChart->m_full_repaint = true;
			TrendChart->Refresh();

		#elif defined ( _OMB_CHART_MATLIBPLOT )
		  wxString tmp_file, data_fname, chart_fname, line;
		  wxString Proc = L"python " + GetDataDir();
		  Proc += L"trend.py ";
		  Proc += L"/usr/share/locale ";	// TODO (igor#1#): Check if it is possible to get this path programmatically

			tmp_file = GetTempFile();
			data_fname = ::wxFileName::CreateTempFileName(tmp_file);
			wxTextFile *file = new wxTextFile(data_fname);
			file->Create();

			for(i = 0; i < number_of_days; i++)
				file->AddLine(wxString::Format("%d", int(times[i])) + ", " + wxString::FromCDouble(trendvalues[i], 2));

			file->Write(wxTextFileType_Unix,wxConvUTF8);
			// file closure
			file->Close();
			delete file;

			Proc += data_fname + L" ";

			chart_fname = ::wxFileName::CreateTempFileName(tmp_file);
			::wxRemoveFile(chart_fname);
			chart_fname += L".png";
			Proc += chart_fname;

			if(! Data->FileData.ReadOnly){
				wxChar color;
				double currenttotal = Data->GetTot(tvFou);
				if(LastValue == currenttotal) color = 'c';
				else if(LastValue < currenttotal) color = 'g';
				else color = 'r';

				Proc += " " + wxString(color) + " " + wxString::FromCDouble(currenttotal, 2);
			}

			wxExecute(Proc, wxEXEC_SYNC);
			::wxRemoveFile(data_fname);

			TrendChart->SetBitmap(GetScaledBitmap(chart_fname, TrendChart, true));

			::wxRemoveFile(chart_fname);

		#endif // _OMB_CHART_WXPIECTRL

	}
	FreshChartShown = false;
}

void ombMainFrame::FullTrendClick(wxCommandEvent& event){
	if(FullTrendCheck->IsCheck()) PaintFullTrendChart();
//	else ShowMaster();
}

void ombMainFrame::PrivacyClick(wxCommandEvent& event){
	wxLaunchDefaultBrowser(PrivacyAddress);}

	void ombMainFrame::FundGroupClick(wxCommandEvent& event){

		wxSQLite3Table GroupTable, FundTable, ChildTable;
		#ifdef _OMB_USE_CIPHER
			GroupTable = GetTable(Data->database, "select * from FundGroups where owner = '-1';");
		#else
			GroupTable = Data->database->GetTable(L"select * from FundGroups where owner = '-1';");
		#endif // _OMB_USE_CIPHER
		int GTC = GroupTable.GetRowCount();

		// Create dialog window;
		ombFundGroups *FundGroups = new ombFundGroups(this);

		// Fill group list
		FundGroups->GroupList->Clear();
		for(int i = 0; i < GTC; i++){
				GroupTable.SetRow(i);
			FundGroups->GroupList->Append(GroupTable.GetAsString("name"));
		}

		// Fill fund list
		#ifdef _OMB_USE_CIPHER
			FundTable = GetTable(Data->database, "select * from Funds;");
		#else
			FundTable = Data->database->GetTable(L"select * from Funds;");
		#endif // _OMB_USE_CIPHER
		int FTC = FundTable.GetRowCount();
		FundGroups->FundList->Clear();
		if(FTC > 0){
			wxString FundName;
			for(int i = 0; i < FTC; i++){
				FundTable.SetRow(i);
				FundName = FundTable.GetAsString("name");
				if(FundName.CmpNoCase(Data->FileData.DefFund) != 0)
					FundGroups->FundList->Append(FundName);
			}
		}

		#ifdef _OMB_USE_CIPHER
			ChildTable = GetTable(Data->database, "select * from FundGroups where owner > '-1';");
		#else
			ChildTable = Data->database->GetTable(L"select * from FundGroups where owner > '-1';");
		#endif // _OMB_USE_CIPHER
		int CTC = ChildTable.GetRowCount();

		// Create fund indexes arrays
		FundGroups->FundIndexes = new TFundIndexes[FTC];
		// Initialization
		for(int i = 0; i < FTC; i++){
			FundGroups->FundIndexes[i].ID = -1;
			FundGroups->FundIndexes[i].FundName = wxEmptyString;
			FundGroups->FundIndexes[i].Owner = -1;
			FundGroups->FundIndexes[i].OwnerName = wxEmptyString;
		}
		// Fill-in with fund information
		int ListIndex = 0;	// Index in fund list
		int OwnerID;
		wxString Name;
		while(ListIndex < FTC){
			OwnerID = -1;
			FundTable.SetRow(ListIndex);
			FundGroups->FundIndexes[ListIndex].ID = FundTable.GetInt("id", -1);
			Name = FundTable.GetAsString("name");
			FundGroups->FundIndexes[ListIndex].FundName = Name;
			// Search for owner index in the child table
			for(int i = 0; i < CTC; i++){
				ChildTable.SetRow(i);
				if(Name.CmpNoCase(ChildTable.GetAsString("name")) == 0){
					OwnerID = ChildTable.GetInt("owner", -1);
					FundGroups->FundIndexes[ListIndex].Owner = OwnerID;
					break;
				}
			}
			// Search for owner name in the group table
			for(int i = 0; i < GTC; i++){
				GroupTable.SetRow(i);
				if(GroupTable.GetInt("id") == OwnerID){
					FundGroups->FundIndexes[ListIndex].OwnerName = GroupTable.GetAsString("name");
					break;
				}
			}

			ListIndex++;
		}

		FundGroups->RefreshControls();

		if(FundGroups->ShowModal() == wxID_OK){
			bool Changed = false;

			#ifdef __OPENSUSE__
				wxString Sql;
			#endif // __OPENSUSE__

			if(FundGroups->AddedOrModifiedGroups){

				bool GroupFound;
				int GroupID = -1;
				for(unsigned int i = 0; i < FundGroups->GroupList->GetCount(); i++){
					GroupFound = false;
					wxString Group = FundGroups->GroupList->GetString(i);
					for(int j = 0; j < GroupTable.GetRowCount(); j++){
						GroupTable.SetRow(j);
						if(Group.CmpNoCase(GroupTable.GetAsString("name")) == 0){
							GroupFound = true;
							GroupID = GroupTable.GetInt("id", -1);
							break;
						}
					}
					if(! GroupFound){

						// Add Group
						#ifdef _OMB_USE_CIPHER
							#ifdef __OPENSUSE__
								Sql = L"insert into FundGroups values (NULL, '" +
																					Group +
																					L"', -1, -1);";
								ExecuteUpdate(Data->database, Sql.c_str());
								// Update group table
								GroupTable = GetTable(Data->database, "select * from FundGroups where owner = '-1';");
							#else
								ExecuteUpdate(Data->database, L"insert into FundGroups values (NULL, '" +
																					Group +
																					L"', -1, -1);");
								// Update group table
								GroupTable = GetTable(Data->database, "select * from FundGroups where owner = '-1';");
							#endif // __OPENSUSE__
						#else
										Data->database->ExecuteUpdate(L"insert into FundGroups values (NULL, '" +
																				Group +
																				L"', -1, -1);");
							// Update group table
							Data->database->GetTable(L"select * from FundGroups where owner = '-1';");
						#endif // _OMB_USE_CIPHER

					}

					// Add Children
					bool ChildFound;
					wxString ChildName;
					#ifdef _OMB_USE_CIPHER
						ChildTable = GetTable(Data->database, "select * from FundGroups where owner > '-1';");
					#else
						Data->database->GetTable(L"select * from FundGroups where owner > '-1';");
					#endif // _OMB_USE_CIPHER
					CTC = ChildTable.GetRowCount();
					for(int k = 0; k < FTC; k++){
						ChildName =  FundGroups->FundIndexes[k].FundName;
						ChildFound = false;
						for(int l = 0; l < CTC; l++){
							ChildTable.SetRow(l);
							if(ChildName.CmpNoCase(ChildTable.GetAsString("name")) == 0){
								ChildFound = true;
								int ChildID = ChildTable.GetInt("child", -1);
								OwnerID = FundGroups->FundIndexes[k].Owner;
								if(OwnerID > -1){
									if((ChildID > -1) && (GroupID == OwnerID)){
										#ifdef _OMB_USE_CIPHER
											#ifdef __OPENSUSE__
												Sql = L"update FundGroups set owner = " +
																wxString::Format("%d", OwnerID) +
																" where child = " +
																wxString::Format("%d", ChildID) +
																";";
												ExecuteUpdate(Data->database, Sql.c_str());
											#else
												ExecuteUpdate(Data->database, L"update FundGroups set owner = " +
																wxString::Format("%d", OwnerID) +
																" where child = " +
																wxString::Format("%d", ChildID) +
																";");
											#endif // __OPENSUSE__
										#else
											Data->database->ExecuteUpdate(L"update FundGroups set owner = " +
																wxString::Format("%d", OwnerID) +
																" where child = " +
																wxString::Format("%d", ChildID) +
																";");
										#endif // _OMB_USE_CIPHER
									}
								}
								else{
									if((ChildID > -1) && ((Group.CmpNoCase(FundGroups->FundIndexes[k].OwnerName) == 0) || (FundGroups->FundIndexes[k].OwnerName.IsEmpty()))){
										#ifdef _OMB_USE_CIPHER
											#ifdef __OPENSUSE__
												Sql = L"delete from FundGroups where child = " +
																		wxString::Format(L"%d", ChildID) +
																		L";";
												ExecuteUpdate(Data->database, Sql.c_str());
												ChildTable = GetTable(Data->database, "select * from FundGroups where owner > '-1';");
											#else
												ExecuteUpdate(Data->database, L"delete from FundGroups where child = " +
																		wxString::Format(L"%d", ChildID) +
																		L";");
												ChildTable = GetTable(Data->database, "select * from FundGroups where owner > '-1';");
											#endif // __OPENSUSE__
										#else
											Data->database->ExecuteUpdate(L"delete from FundGroups where child = " +
																	wxString::Format(L"%d", ChildID) +
																	L";");
											Data->database->GetTable(L"select * from FundGroups where owner > '-1';");
										#endif // _OMB_USE_CIPHER
										CTC = ChildTable.GetRowCount();
									}
								}
								break;
							}
						}
						if((! ChildFound) && (FundGroups->FundIndexes[k].OwnerName.CmpNoCase(Group) == 0)){
							/*int*/ GroupID = -1;
							int ChildID = -1;
							for(int m = 0; m < GroupTable.GetRowCount(); m++){
								GroupTable.SetRow(m);
								wxString GroupName = GroupTable.GetAsString("name");
								if(GroupName.CmpNoCase(Group) == 0){
									GroupID = GroupTable.GetInt("id");
									break;
								}
							}
							for(int m = 0; m < FundTable.GetRowCount(); m++){
								FundTable.SetRow(m);
								if(ChildName.CmpNoCase(FundTable.GetAsString("name")) == 0){
									ChildID = FundTable.GetInt("id");
									break;
								}
							}
							if((GroupID > -1) && (ChildID > -1)){
								#ifdef _OMB_USE_CIPHER
									#ifdef __OPENSUSE__
										Sql = L"insert into FundGroups values (NULL, '" +
																			ChildName +
																			L"', " +
																			wxString::Format(L"%d", GroupID) +
																			", " +
																			wxString::Format(L"%d", ChildID) +
																			");";
										ExecuteUpdate(Data->database, Sql.c_str());
									#else
										ExecuteUpdate(Data->database, L"insert into FundGroups values (NULL, '" +
																			ChildName +
																			L"', " +
																			wxString::Format(L"%d", GroupID) +
																			", " +
																			wxString::Format(L"%d", ChildID) +
																			");");
									#endif // __OPENSUSE__
								#else
												Data->database->ExecuteUpdate(L"insert into FundGroups values (NULL, '" +
																			ChildName +
																			L"', " +
																			wxString::Format(L"%d", GroupID) +
																			", " +
																			wxString::Format(L"%d", ChildID) +
																			");");
								#endif // _OMB_USE_CIPHER
							}
						}
					}

				}

				Changed = true;
			}

			if(FundGroups->DeletedGroups){
				bool Found;
				int GroupID;
				wxString GroupName;
				for(int i = 0; i < GTC; i++){
					Found = false;
					GroupTable.SetRow(i);
					GroupID = GroupTable.GetInt("id", -1);
					GroupName = GroupTable.GetAsString("name");
					for(unsigned int j = 0; j < FundGroups->GroupList->GetCount(); j++)
						if(GroupName.CmpNoCase(FundGroups->GroupList->GetString(j)) == 0){
							Found = true;
							break;
						}
					if(! Found){
						#ifdef _OMB_USE_CIPHER
							#ifdef __OPENSUSE__
								Sql = L"delete from FundGroups where id = " +
												wxString::Format(L"%d", GroupID) +
												L";";
								ExecuteUpdate(Data->database, Sql.c_str());
							#else
								ExecuteUpdate(Data->database, L"delete from FundGroups where id = " +
												wxString::Format(L"%d", GroupID) +
												L";");
							#endif // __OPENSUSE__
						#else
							Data->database->ExecuteUpdate(L"delete from FundGroups where id = " +
												wxString::Format(L"%d", GroupID) +
												L";");
						#endif // _OMB_USE_CIPHER
					}
				}

				Changed = true;
			}

			if(Changed){
				Data->FileData.Modified = true;
				ShowF();
			}

		}

		delete FundGroups;

	}

#ifdef _OMB_USE_CIPHER
	#ifdef _OMB_SQLCIPHER_v4
		bool ombMainFrame::GetDatabaseIs_v4(void){
			return Data->DatabaseIsSqlCipher_V4;
		}
	#endif // _OMB_SQLCIPHER_v4
#endif // _OMB_USE_CIPHER

#endif //MAINFRAME_CPP_INCLUDED

