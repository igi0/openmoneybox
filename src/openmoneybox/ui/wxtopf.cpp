///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxtopf.h"

///////////////////////////////////////////////////////////////////////////

wxTOPF::wxTOPF( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	mSizer = new wxFlexGridSizer( 5, 1, 0, 0 );
	mSizer->AddGrowableCol( 0 );
	mSizer->AddGrowableRow( 4 );
	mSizer->SetFlexibleDirection( wxBOTH );
	mSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	LNam = new wxStaticText( this, wxID_ANY, _("Select a fund:"), wxDefaultPosition, wxDefaultSize, 0 );
	LNam->Wrap( -1 );
	LNam->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	mSizer->Add( LNam, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 10 );

	Name = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN );
	Name->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	mSizer->Add( Name, 0, wxEXPAND|wxRIGHT|wxLEFT, 10 );

	LVal = new wxStaticText( this, wxID_ANY, _("Set the fund value:"), wxDefaultPosition, wxDefaultSize, 0 );
	LVal->Wrap( -1 );
	LVal->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	mSizer->Add( LVal, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 10 );

	Val = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Val->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	Val->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &validator_string ) );

	mSizer->Add( Val, 0, wxEXPAND|wxRIGHT|wxLEFT, 10 );

	btnSizer = new wxFlexGridSizer( 1, 2, 0, 0 );
	btnSizer->AddGrowableCol( 0 );
	btnSizer->AddGrowableCol( 1 );
	btnSizer->AddGrowableRow( 0 );
	btnSizer->SetFlexibleDirection( wxBOTH );
	btnSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	OKBtn = new wxButton( this, wxID_OK, _("OK"), wxDefaultPosition, wxDefaultSize, 0 );

	OKBtn->SetDefault();
	OKBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	btnSizer->Add( OKBtn, 0, wxALIGN_RIGHT|wxALIGN_BOTTOM|wxALL, 5 );

	CancelBtn = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	CancelBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	btnSizer->Add( CancelBtn, 0, wxALL|wxALIGN_BOTTOM, 5 );


	mSizer->Add( btnSizer, 1, wxEXPAND|wxALIGN_BOTTOM, 5 );


	this->SetSizer( mSizer );
	this->Layout();

	// Connect Events
	Name->Connect( wxEVT_CHAR, wxKeyEventHandler( wxTOPF::NameText ), NULL, this );
	Name->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( wxTOPF::NameChange ), NULL, this );
	OKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxTOPF::OKBtnClick ), NULL, this );
}

wxTOPF::~wxTOPF()
{
	// Disconnect Events
	Name->Disconnect( wxEVT_CHAR, wxKeyEventHandler( wxTOPF::NameText ), NULL, this );
	Name->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( wxTOPF::NameChange ), NULL, this );
	OKBtn->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxTOPF::OKBtnClick ), NULL, this );

}
