/***************************************************************
 * Name:      objct.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <wx/dialog.h>

#ifndef ObjctH
#define ObjctH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxobjct.h"

class TObjects : public wxTObjects
{
private:
	// Non-GUI components
	bool Kind;

	// Routines
	void HideSysTime(void);
//private:
	void Focus(bool F);
	#ifdef __WXMSW__
    void MSW_ROCombo(void);
  #endif // __WXMSW__
protected:
	void OKBtnClick(wxCommandEvent& event) override;
	void AlmCheckBoxClick(wxCommandEvent& event) override;
public:

	// Routines
	explicit TObjects(wxWindow *parent);
	void InitLabels(int O);
	void HideAlarm(void);
};

extern TObjects *Objects;

// Calls to module ombLogo
#if (wxCHECK_VERSION(3, 2, 0) )
	extern wxString iUpperCase(wxString S);
#else
	WXIMPORT wxString iUpperCase(wxString S);
#endif

#ifdef _OMB_MONOLITHIC
  extern void Error(int Err, const wxString &Opt);
#else
  // Calls to module omberr
  WXIMPORT void Error(int Err,wxString Opt);
#endif // _OMB_MONOLITHIC


#endif // ObjctH


