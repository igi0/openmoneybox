///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/sizer.h>
#include <wx/panel.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class TEPanel
///////////////////////////////////////////////////////////////////////////////
class TEPanel : public wxPanel
{
	private:

	protected:

	public:
		wxStaticBitmap* teIcon1;
		wxStaticText* teCategory1;
		wxStaticText* teValue1;
		wxStaticBitmap* teIcon2;
		wxStaticText* teCategory2;
		wxStaticText* teValue2;
		wxStaticBitmap* teIcon3;
		wxStaticText* teCategory3;
		wxStaticText* teValue3;

		TEPanel( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~TEPanel();

};

