/***************************************************************
 * Name:      getbackobj.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2023-01-23
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "getbackobj.h"

//TGetBackF *GetBackF;

TGetBackF::TGetBackF(wxWindow *parent):wxGetBackF(parent,-1,wxEmptyString,wxDefaultPosition,wxSize(300,320),wxDEFAULT_DIALOG_STYLE){
	Memo=new wxArrayString();}

void TGetBackF::Focus(bool Err){
	if(!Err)Obj->SetFocus();
	else Per->SetFocus();}

void TGetBackF::PerChange(wxCommandEvent& event){
	Obj->Clear();
	for(unsigned int i=0;i<Memo->GetCount();i++){
	  int Sep,Len;
		wxString N,O;
		Len=Memo->Item(i).Len();
    #ifdef __WXMSW__
      Sep=Memo->Item(i).Find("|");
    #else
      Sep=Memo->Item(i).Find(L"|");
    #endif
		N=Memo->Item(i).Mid(0,Sep);
		O=Memo->Item(i).Mid(Sep+1,Len-Sep+1);
		if(N==Per->GetValue())Obj->Append(O);}
	Obj->SetSelection(0);}

void TGetBackF::OKBtnClick(wxCommandEvent& event){
	SetReturnCode(wxID_NONE);
	if(Obj->GetValue().IsEmpty()){
		Error(32,wxEmptyString);
		Focus(false);
		return;}
	if(Per->GetValue().IsEmpty()){
		Error(33,wxEmptyString);
		Focus(true);
		return;}
	Obj->SetValue(iUpperCase(Obj->GetValue()));
	Per->SetValue(iUpperCase(Per->GetValue()));
	EndModal(wxID_OK);}

void TGetBackF::InitLabels(bool O){
	if(!O){
		SetTitle(_("Get back an object"));
		LOgg->SetLabel(_("Insert the lent object:"));
		LPer->SetLabel(_("Insert the object receiver:"));}
	else{
		SetTitle(_("Give back an object"));
		LOgg->SetLabel(_("Insert the borrowed object:"));
		LPer->SetLabel(_("Insert the donor:"));}}

TGetBackF::~TGetBackF(void){
	Memo->Clear();
	delete Memo;}


