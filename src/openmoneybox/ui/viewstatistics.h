/***************************************************************
 * Name:      main_wx.cpp
 * Purpose:   Code for OpenMoneyBox trend chart
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2023-04-30
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 *
 ** code derived from BOINC source (http://boinc.berkeley.edu), version 7.5.0
 *
 **************************************************************/


#ifndef VIEWSTATISTICS_H
#define VIEWSTATISTICS_H

#include <wx/wx.h>
#include <wx/numformatter.h>

// statistics at a specific day
//
struct DAILY_STATS {
    double total_credit;
    double day;
};

wxString format_number(double x, int nprec);

class CPaintStatistics : public wxWindow
{
public:
  wxBitmap				m_dc_bmp;
	bool					m_full_repaint;
	bool                    m_bmp_OK;

// Marker
	double                  m_GraphMarker_X1;
  double                  m_GraphMarker_Y1;
  bool                    m_GraphMarker1;
// Zoom
	wxCoord                  m_GraphZoom_X1;
	wxCoord                  m_GraphZoom_Y1;
	wxCoord                  m_GraphZoom_X2;
	wxCoord                  m_GraphZoom_Y2;
	wxCoord                  m_GraphZoom_X2_old;
	wxCoord                  m_GraphZoom_Y2_old;
	bool                     m_GraphZoomStart;

	wxCoord                  m_GraphMove_X1;
	wxCoord                  m_GraphMove_Y1;
	wxCoord                  m_GraphMove_X2;
	wxCoord                  m_GraphMove_Y2;
	bool                     m_GraphMoveStart;
	bool                     m_GraphMoveGo;

	double                  m_Zoom_max_val_X;
	double                  m_Zoom_min_val_X;
	double                  m_Zoom_max_val_Y;
	double                  m_Zoom_min_val_Y;
	bool                    m_Zoom_Auto;
// old
  wxString                heading;
// X'=AX+B; Y'=AY+B;
  double                  m_Ax_ValToCoord;
  double                  m_Bx_ValToCoord;
  double                  m_Ay_ValToCoord;
  double                  m_By_ValToCoord;

	double                  m_Ax_CoordToVal;
  double                  m_Bx_CoordToVal;
  double                  m_Ay_CoordToVal;
  double                  m_By_CoordToVal;
// XY
  double                  m_WorkSpace_X_start;
  double                  m_WorkSpace_X_end;
	double                  m_WorkSpace_Y_start;
  double                  m_WorkSpace_Y_end;
//
	double                  m_main_X_start;
  double                  m_main_X_end;
	double                  m_main_Y_start;
  double                  m_main_Y_end;

  double                  m_Graph_X_start;
	double                  m_Graph_X_end;
  double                  m_Graph_Y_start;
  double                  m_Graph_Y_end;

  double                  m_Graph_draw_X_start;
	double                  m_Graph_draw_X_end;
  double                  m_Graph_draw_Y_start;
  double                  m_Graph_draw_Y_end;
// View
	int					    m_GraphLineWidth;
  int                     m_GraphPointWidth;

  wxFont                  m_font_standard;
  wxFont                  m_font_bold;
// colour
  wxColour                m_pen_MarkerLineColour;
  wxColour                m_pen_ZoomRectColour;
  wxColour                m_brush_ZoomRectColour;

	wxColour                m_brush_AxisColour;
  wxColour                m_pen_AxisColour;
  wxColour                m_pen_AxisColourAutoZoom;
  wxColour                m_pen_AxisColourZoom;
  wxColour                m_pen_AxisXColour;
  wxColour                m_pen_AxisYColour;
  wxColour                m_pen_AxisXTextColour;
  wxColour                m_pen_AxisYTextColour;

  wxColour                m_brush_MainColour;
  wxColour                m_pen_MainColour;

  wxColour                m_pen_HeadTextColour;
  wxColour                m_pen_GraphTotalColour;

  DAILY_STATS *stats;
  double trend_value;
  int nstats;
  wxDateTime trend_date;

	CPaintStatistics();
	CPaintStatistics(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL, const wxString& name = wxT("panel"));
	~CPaintStatistics();

	void DrawMainHead(wxDC &dc, const wxString& head_name);
	void DrawAxis(wxDC &dc, const double max_val_y, const double min_val_y, const double max_val_x, const double min_val_x, wxColour pen_AxisColour, const double max_val_y_all, const double min_val_y_all);
  void DrawGraph(wxDC &dc, DAILY_STATS *stats, const wxColour graphColour, const int typePoint);
	void DrawMarker(wxDC &dc);
	void ClearXY();
	void AB(const double x_coord1, const double y_coord1, const double x_coord2, const double y_coord2, const double x_val1, const double y_val1, const double x_val2, const double y_val2);
//--------------------------
	void DrawAll(wxDC &dc);
//--------------------------

protected:
  void OnPaint(wxPaintEvent& event);
	void OnEraseBackground(wxEraseEvent & event){};
  void OnSize(wxSizeEvent& event);
  void OnLeftMouseDown(wxMouseEvent& event);
  void OnLeftMouseUp(wxMouseEvent& event);
	void OnMouseMotion(wxMouseEvent& event);
  void OnRightMouseDown(wxMouseEvent& event);
  void OnRightMouseUp(wxMouseEvent& event);
	void OnMouseLeaveWindows(wxMouseEvent& event);

	DECLARE_EVENT_TABLE()
};

#endif // VIEWSTATISTICS_H
