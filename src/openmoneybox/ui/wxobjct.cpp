///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxobjct.h"

///////////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE( wxTObjects, wxDialog )
	EVT_CHECKBOX( wxID_ANY, wxTObjects::_wxFB_AlmCheckBoxClick )
	EVT_BUTTON( wxID_OK, wxTObjects::_wxFB_OKBtnClick )
END_EVENT_TABLE()

wxTObjects::wxTObjects( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	Sizer = new wxFlexGridSizer( 8, 1, 0, 0 );
	Sizer->AddGrowableCol( 0 );
	Sizer->AddGrowableRow( 7 );
	Sizer->SetFlexibleDirection( wxBOTH );
	Sizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	LObj = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	LObj->Wrap( -1 );
	LObj->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	Sizer->Add( LObj, 0, wxTOP|wxRIGHT|wxLEFT|wxEXPAND, 10 );

	Obj = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	Obj->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	Sizer->Add( Obj, 0, wxEXPAND|wxRIGHT|wxLEFT, 10 );

	LPer = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	LPer->Wrap( -1 );
	LPer->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	Sizer->Add( LPer, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 10 );

	Per = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Per->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	Sizer->Add( Per, 0, wxRIGHT|wxLEFT|wxEXPAND, 10 );

	Sizer2 = new wxFlexGridSizer( 1, 3, 0, 0 );
	Sizer2->AddGrowableCol( 1 );
	Sizer2->AddGrowableRow( 0 );
	Sizer2->SetFlexibleDirection( wxBOTH );
	Sizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	AlmCB = new wxCheckBox( this, wxID_ANY, _("Alarm"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	AlmCB->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	Sizer2->Add( AlmCB, 1, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxTOP|wxBOTTOM|wxLEFT, 5 );

	Alarm = new wxDatePickerCtrl( this, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, wxDefaultSize, wxDP_DEFAULT );
	Alarm->SetFont( wxFont( 11, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_LIGHT, false, wxT("Ubuntu") ) );
	Alarm->Enable( false );

	Sizer2->Add( Alarm, 1, wxALIGN_RIGHT|wxTOP|wxBOTTOM|wxRIGHT|wxEXPAND, 5 );


	Sizer->Add( Sizer2, 1, wxEXPAND, 5 );

	SysTimeCheck = new wxCheckBox( this, wxID_ANY, _("Use system time"), wxDefaultPosition, wxDefaultSize, 0 );
	SysTimeCheck->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	Sizer->Add( SysTimeCheck, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxRIGHT|wxLEFT, 10 );

	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer2->AddGrowableCol( 0 );
	fgSizer2->AddGrowableCol( 1 );
	fgSizer2->AddGrowableRow( 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	OKBtn = new wxButton( this, wxID_OK, _("OK"), wxDefaultPosition, wxDefaultSize, 0 );

	OKBtn->SetDefault();
	OKBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer2->Add( OKBtn, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	CancelBtn = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	CancelBtn->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );

	fgSizer2->Add( CancelBtn, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	Sizer->Add( fgSizer2, 1, wxEXPAND, 5 );


	this->SetSizer( Sizer );
	this->Layout();
}

wxTObjects::~wxTObjects()
{
}
