/***************************************************************
 * Name:      setcreddeb.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef SetCredDebH
#define SetCredDebH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxsetcreddeb.h"

class TSCredDeb : public wxTSCredDeb
{
private:
	// Routines
	void Focus(bool Err);
protected:
	void OKBtnClick(wxCommandEvent& event) override;
public:
	// Routines
	explicit TSCredDeb(wxWindow *parent);
	void InitLabels(bool E);
};

#if (wxCHECK_VERSION(3, 2, 0) )
	extern wxString iUpperCase(wxString S);
#else
	WXIMPORT wxString iUpperCase(wxString S);
#endif

#ifdef _OMB_MONOLITHIC
  extern wxString CheckValue(const wxString& Val);
  extern void Error(int Err, const wxString &Opt);
#else
  WXIMPORT wxString CheckValue(const wxString& Val);
  WXIMPORT void Error(int Err, const wxString &Opt);
#endif // _OMB_MONOLITHIC

#endif // SetCredDebH

