/***************************************************************
 * Name:      main_wx.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-06
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef MAINFRAME_H_INCLUDED
#define MAINFRAME_H_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/datetime.h>
//#include <wx/fileconf.h> // For wxFileConfig
#include <wx/calctrl.h>
#include <wx/sizer.h>
#include <wx/toolbar.h>
#include <wx/tbarbase.h>
#include <wx/gauge.h>
#include <wx/stattext.h>
#include <wx/treectrl.h>
#include <wx/listbox.h>
#include <wx/grid.h>
#include <wx/statusbr.h>
#include <wx/statbmp.h>
#include <wx/fdrepdlg.h>  // For wxFindReplaceDialog

#include "../../platformsetup.h"

#if _OMB_HASNATIVEABOUT == 1
	#include <wx/aboutdlg.h>
#endif // _OMB_HASNATIVEABOUT == 1

#ifdef _OMB_CHART_WXPIECTRL
	#include "wxPieCtrl.h"
	#include "viewstatistics.h"
#endif // _OMB_CHART_WXPIECTRL

#include <wx/timectrl.h>

#ifdef _OMB_CHART_MATLIBPLOT
	#include "wxmainframe_24_mpl.h"
#else
	#include "wxmainframe_24.h"
#endif // _OMB_CHART_MATLIBPLOT

#include "../../omb35core.h"
#include "topexpensepanel.h"

#ifdef _OMB_USE_CIPHER
	#include "../../wxsqlite3.h"
#endif // _OMB_USE_CIPHER

#ifdef _OMB_MONOLITHIC
	#include "../openmoneybox.h"
#endif // _OMB_MONOLITHIC

typedef enum TGridCol{
  gcDate = 0,
  gcTime,
  gcOperation,
  gcValue,
  gcReason,
  gcCategory,
  gcContact,
  gcLocation,
  }GridCol;

const int ombExtTool=2000;

class ombMainFrame : public wxMainFrame{
	friend class ombApp;	// for ShowF()

	private:
		// GUI Controls

		#ifdef _OMB_CHART_MATLIBPLOT
			wxBitmap Fund_bmp;
			wxBitmap Trend_bmp;
		#elif defined ( _OMB_CHART_WXPIECTRL )
			wxPieCtrl *FundChart;
			CPaintStatistics *TrendChart;
		#endif // _OMB_CHART_MATLIBPLOT

		wxFindReplaceDialog *FindDialog;

		TEPanel *TopCategoriesPanel;
		wxImageList *CategoryIconList;

		// Non-GUI Controls
		bool	Initialised,
					//menu_opendoc,  //  menu_opendoc: true if doc menus are active
					Master_Attached,
					FreshChartShown,	// true if charts are drawn out of last month data
					map_viewer_exist;	// true if ombmapviewer is installed

		int OriginalCategoryIndex;	// Category index for the drop-down list management;

		wxArrayString *TreeNames,	// TreeList branch names
			CategoryChoices;				// Category choices
		wxTreeItemId Funds_tid,Creds_tid,Debts_tid,Lends_tid,Borrowed_tid;
		wxDateTime calmindate, calmaxdate;
		wxFindReplaceData *FindData;
		wxGridCellChoiceEditor *Editor;

		wxColour backup_report_colour;

		// Routines
		void InitLanguage(void);
		void ResetTree(void);
		void ShowControls(bool V);
		void FundOperation(int Op);
		void Operation(int Op);
		void ObjectOperation(int Op);
		void ShopOperation(int O);
		void ObjectOperation2(int Op);
		void RebuildTools(void);
		void ShowF(bool doChart = true);
		void ShowMaster(void);
		void PaintMasterChart(bool hasfunds, wxSQLite3Table &funds, wxSQLite3Table &transactions
				, wxSQLite3Table &FundGroups, wxSQLite3Table &FundChildren
				);
		void PaintFullTrendChart(void);
		void SearchMasterMinDate(void);
		void AddNode(wxTreeItemId Node, const wxString &Name, const wxString &Value, int c_index = -1, bool External = false);
		void PaintChart(void);
		void UpdateMenu(void);
		bool SaveDatabase(void);
		void FormDestroy(void);
		int GetFirstSelectedRow(void);
		//void ResizeCols(int C);
		void ReportSelect(wxGridEvent& event) override;
		void ReportRangeSelect(wxGridRangeSelectEvent& event) override;
		wxString CreateTitle(bool type);
		void remove_obtained_shopitem(const wxString& obj);
		#ifdef _OMB_CHART_MATLIBPLOT
			wxBitmap GetScaledBitmap(wxString chart, wxStaticBitmap* panel, bool trend = false);
		#endif // _OMB_CHART_MATLIBPLOT
		void CheckDate(void);
		wxString GetTempFile(void);
		wxString GetContactImage(long c_id);
		int GetTreeViewContactImageIndex(long c_id);
		bool IsNewMonthShown(void);
		void SetCaption(void);
		wxString BrowseDocument(void);
		void AuthorWindow(void);
		wxString GetLocationImagePath(int Height);
		void EnableCategoryGUIControls(bool Enable);
	protected:
		void SaveClick(wxCommandEvent& event) override;
		void ViewArchiveClick(wxCommandEvent& event) override;
		#ifdef _OMB_USE_CIPHER
			void ModifyPasswordClick(wxCommandEvent& event) override;
			void RemovePasswordClick(wxCommandEvent& event) override;
		#endif // _OMB_USE_CIPHER
		void RevertClick(wxCommandEvent& event) override;
		void ExitClick(wxCommandEvent& event) override;
		void CopyClick(wxCommandEvent& event) override;
		//void Find1Click(wxCommandEvent& event);
		void NewFundClick(wxCommandEvent& event) override;
		void RemoveFundClick(wxCommandEvent& event) override;
		void ResetFundClick(wxCommandEvent& event) override;
		void SetTotalClick(wxCommandEvent& event) override;
		void DefaultFundClick(wxCommandEvent& event) override;
		void GainClick(wxCommandEvent& event) override;
		void ExpenseClick(wxCommandEvent& event) override;
		void CategoryClick(wxCommandEvent& event) override;
		void NewCreditClick(wxCommandEvent& event) override;
		void RemoveCreditClick(wxCommandEvent& event) override;
		void RemitCreditClick(wxCommandEvent& event) override;
		void NewDebtClick(wxCommandEvent& event) override;
		void RemoveDebtClick(wxCommandEvent& event) override;
		void RemitDebtClick(wxCommandEvent& event) override;
		void ReceivedClick(wxCommandEvent& event) override;
		void GiftedClick(wxCommandEvent& event) override;
		void LendClick(wxCommandEvent& event) override;
		void BorrowClick(wxCommandEvent& event) override;
		void AddShopItemClick(wxCommandEvent& event) override;
		void DelShopItemClick(wxCommandEvent& event) override;
		void GetBackClick(wxCommandEvent& event) override;
		void GiveBackClick(wxCommandEvent& event) override;
		void ShowFindDialog (wxCommandEvent& event) override;
		void Find(wxFindDialogEvent& event);
		int FindLoop(int StartIndex,int StopIndex, wxString find_str);
		void FindClose(wxFindDialogEvent& event);
		void WebSiteClick(wxCommandEvent& event) override;
		void BugClick(wxCommandEvent& event) override;
		void DonateClick(wxCommandEvent& event) override;
		void PrivacyClick(wxCommandEvent& event) override;
		void CheckDate(wxCalendarEvent& event) override;
		void WizClick(wxCommandEvent& event) override;
		void OldConv1Click(wxCommandEvent& event) override;
		void OptionsClick(wxCommandEvent& event) override;
		void HelpMenuClick(wxCommandEvent& event) override;
		void AuthorClick(wxCommandEvent& event) override;
		//void PopRepPopup( wxGridEvent& event );
		void ExtClick(wxCommandEvent& event);
		void onResize(wxSizeEvent& event) override;
		void EditCategoriesClick(wxCommandEvent& event) override;
		#ifdef __WXMSW__
			void LicenseClick(wxCommandEvent& event);
		#endif // __WXMSW__
		void ImportDocument(wxCommandEvent& event) override;
		#ifdef _OMB_CHART_MATLIBPLOT
			void FundResize( wxSplitterEvent& event );
		#endif // _OMB_CHART_MATLIBPLOT
		void ShowMap(wxCommandEvent& event) override;
		void GiveTime(wxDateEvent& event) override;
		void ReportCategoryEdit(wxGridEvent& event) override;
		void ReportCategorySelected(wxGridEvent& event) override;
		void FullTrendClick(wxCommandEvent& event) override;

		void FundGroupClick(wxCommandEvent& event) override;

		public:
		// GUI components

		// Routines
		void Draw(wxDC& dc);
		explicit ombMainFrame(wxWindow* parent);
		~ombMainFrame(void);
		bool AutoOpen(void);
		void UpdateCalendar(wxDateTime master_mindate);
		void TextConvClick(wxCommandEvent& event) override;
		void FormClose(wxCloseEvent &event) override;
		#ifdef _OMB_MONOLITHIC
			bool IsTrayCommand;
			ombApp *App;
		#endif // _OMB_MONOLITHIC

		#ifdef _OMB_USE_CIPHER
			#ifdef _OMB_SQLCIPHER_v4
				bool GetDatabaseIs_v4(void);
			#endif // _OMB_SQLCIPHER_v4
		#endif // _OMB_USE_CIPHER

};

/*
#ifdef _OMB_INSTALLEDUPDATE
	WXIMPORT bool GetCheckUpdates(void);
#endif // _OMB_INSTALLEDUPDATE
*/
WXIMPORT bool GetAutoOpen(void);
WXIMPORT void Logo(wxString Product);
#ifdef __OPENSUSE__
	#if (wxCHECK_VERSION(3, 1, 5) )
		extern void DonateDialog(const wxString& olURL);
	#else
		WXIMPORT void DonateDialog(wxString olURL);
	#endif
#else
	#if (wxCHECK_VERSION(3, 2, 0) )
		extern void DonateDialog(const wxString& olURL);
	#else
		WXIMPORT void DonateDialog(wxString olURL);
	#endif
#endif // __OPENSUSE__
WXIMPORT int GetLang(void);
WXIMPORT bool CheckAndPromptForConversion(wxString File);

#ifdef _OMB_MONOLITHIC
  extern wxString GetDefaultDocument(void);
  extern bool ShowBar(void);
  extern bool IsFirstRunEver(void);
  extern int RunWizard(wxGauge* Prog, wxString &S, bool def);
  extern void Set_DefaultDocument(const wxString& Document);
  extern void DoFirstRun(void);
  extern bool ShowTrendChart(void);
  extern bool ShowFundChart(void);
  extern wxString GetUserLocalDir(void);
  extern wxString GetTool(int i);
  extern bool ShowOptionsDialog(
																	#ifdef _OMB_USE_CIPHER
																		#ifdef _OMB_SQLCIPHER_v4
																			bool IsOpenDB_v4 = false
																		#else
																			void
																		#endif // _OMB_SQLCIPHER_v4
																	#endif // _OMB_USE_CIPHER
																);
  extern wxString GetCurrencySymbol(void);
	extern wxString GetLogString(int S);
  extern int GetToolCount(void);
#else
  WXIMPORT wxString GetDefaultDocument(void);
  WXIMPORT bool ShowBar(void);
  WXIMPORT bool IsFirstRunEver(void);
  WXIMPORT int RunWizard(wxGauge* Prog, wxString &S, bool def);
  WXIMPORT void Set_DefaultDocument(const wxString& Document);
  WXIMPORT void DoFirstRun(void);
  WXIMPORT bool ShowTrendChart(void);
  WXIMPORT bool ShowFundChart(void);
  WXIMPORT wxString GetUserLocalDir(void);
  WXIMPORT wxString GetTool(int i);
  WXIMPORT bool ShowOptionsDialog(void);
  WXIMPORT int GetToolCount(void);
#endif // _OMB_MONOLITHIC

#ifdef __WXMSW__
  // Calls to module ombopt.dll
  #ifdef _OMB_MONOLITHIC
    extern wxString GetInstallationPath(void);
    extern wxString GetDataDir(void);
  #else
    WXIMPORT wxString GetInstallationPath(void);
    WXIMPORT wxString GetDataDir(void);
  #endif // _OMB_MONOLITHIC
  extern void About(wxString Name, wxString ShortVer, wxString LongVer, wxString Date);
#endif

#ifdef __WXMAC__
  // Calls to module ombopt.dll
  WXIMPORT wxString GetInstallationPath(void);
  //WXIMPORT wxString GetDataDir(void);
#endif	// __WXMAC__

#ifdef __WXGTK__
	WXIMPORT wxString GetDesktopEnv(void);
#endif // __WXGTK__

#if _OMB_HASNATIVEABOUT == 1
	WXIMPORT wxString GetShareDir(void);
#endif // _OMB_HASNATIVEABOUT

WXIMPORT void ombLogMessage(wxString Text);

#ifdef _OMB_MONOLITHIC
	extern bool TrayActive(void);
#endif // _OMB_MONOLITHIC

	extern bool ShowFundGroups(void);

#endif	// MAINFRAME_H_INCLUDED
