/***************************************************************
 * Name:      objct.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2023-04-30
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wxobjct.h"
#include "objct.h"
#include "../../types.h"
//#include "constants.h"

TObjects *Objects;

TObjects::TObjects(wxWindow *parent):wxTObjects(parent,-1,wxEmptyString,wxDefaultPosition,wxSize( 322,320 ),wxDEFAULT_DIALOG_STYLE/*,_T("Objects")*/){
  Kind=false;
  Focus(false);
  Alarm->SetRange(wxDateTime::Today(),wxInvalidDateTime);}

void TObjects::OKBtnClick(wxCommandEvent& event){
	SetReturnCode(wxID_NONE);
	if(Obj->GetValue().IsEmpty()){
		if(Kind){
			Error(16,wxEmptyString);
			return;}
		else{
			Error(32,wxEmptyString);
			return;}
		Focus(false);}
	if((!Kind)&&Per->GetValue().IsEmpty()){
		Error(33,wxEmptyString);
		Focus(true);
		return;}
	Obj->SetValue(iUpperCase(Obj->GetValue()));
	if(Per->IsShown())Per->SetValue(iUpperCase(Per->GetValue()));
	EndModal(wxID_OK);}

void TObjects::InitLabels(int O){
	int ww,wh,x,y;	// ww, wh: window width and height
									//  x,  y: control size
	if(O<3)HideAlarm();
  switch(O){
    case 1:	// Object received
			SetTitle(_("Receive an object"));
			LObj->SetLabel(_("Insert the received object:"));
			goto Donor;
		case 2:	// Object Given
			SetTitle(_("Give an object"));
			LObj->SetLabel(_("Insert the given object:"));
			goto Receiver;
		case 3:	// Object lent
			SetTitle(_("Lend an object"));
			LObj->SetLabel(_("Insert the lent object:"));
			Receiver:
			LPer->SetLabel(_("Insert the receiver:"));
			break;
		case 4:	// Object borrowed
			SetTitle(_("Borrow an object"));
			LObj->SetLabel(_("Insert the borrowed object:"));
			Donor:
			LPer->SetLabel(_("Insert the donor:"));
			break;

		case 6:	// Common initialization for ShopList operations
			Kind=true;
			GetClientSize(&ww,&wh);
			LPer->GetClientSize(&x,&y);
			wh-=y;
			Per->GetClientSize(&x,&y);
			wh-=y;
			Sizer->Show(size_t(2),false);
			Sizer->Show(size_t(3),false);
			SetClientSize(ww,wh);
			break;
		case 7:	// Add Shop item
			SetTitle(_("Add an item to the shopping list"));
			LObj->SetLabel(_("Insert the item to add:"));
			goto HideTime;
		case 8:	// Remove shop item
			SetTitle(_("Remove an item from the shopping list"));
			LObj->SetLabel(_("Insert the item to remove:"));
			#ifdef __WXMSW__
        MSW_ROCombo();
      #else
        Obj->SetWindowStyle(wxCB_DROPDOWN|wxCB_READONLY);
			#endif // __WXMSW__
			HideAlarm();
			HideTime:
			HideSysTime();
			break;
		default:
			return;}
}

void TObjects::Focus(bool F){
  if(F)Per->SetFocus();
  else Obj->SetFocus();}

void TObjects::HideAlarm(void){
	int x,y;
	AlmCB->Show(false);
	Alarm->Show(false);
	SysTimeCheck->GetPosition(&x,&y);
	SysTimeCheck->SetPosition(wxPoint(x,y-24));
	OKBtn->GetPosition(&x,&y);
	OKBtn->SetPosition(wxPoint(x,y-24));
	CancelBtn->GetPosition(&x,&y);
	CancelBtn->SetPosition(wxPoint(x,y-24));
	GetSize(&x,&y);
	SetSize(wxSize(x,y-24));}

void TObjects::HideSysTime(void){
	int x,y;
	SysTimeCheck->Show(false);
	/*
	Bevel1->GetSize(&x,&y);
	Bevel1->SetSize(wxSize(x,y-13));
	*/
	OKBtn->GetPosition(&x,&y);
	OKBtn->SetPosition(wxPoint(x,y-13));
	CancelBtn->GetPosition(&x,&y);
	CancelBtn->SetPosition(wxPoint(x,y-13));
	GetSize(&x,&y);
	SetSize(wxSize(x,y-13));}

void TObjects::AlmCheckBoxClick(wxCommandEvent& event){
	Alarm->Enable(AlmCB->IsChecked());}

#ifdef __WXMSW__
  // On MSW unexpected horizontal scrollbar is shown when changing wxComboBox stile to wxCB_READONLY
  void TObjects::MSW_ROCombo(void){
    delete Obj;
    Obj = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN|wxCB_READONLY );
    wxSizerFlags flags=wxSizerFlags(0);
    flags.Expand();
    flags.Border(wxRIGHT|wxLEFT,10);
    Sizer->Insert(1,Obj,flags);}
#endif // __WXMSW__

