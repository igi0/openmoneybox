/***************************************************************
 * Name:      main_wx.h
 * Purpose:   Code for OpenMoneyBox fund groups
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-08-09
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef FUNDGROUPS_H_INCLUDED
	#define FUNDGROUPS_H_INCLUDED

	#ifndef WX_PRECOMP
		#include <wx/wx.h>
	#else
		#include <wx/wxprec.h>
	#endif

	#include "ombFundGroups.h"

	ombFundGroups::ombFundGroups( wxWindow* parent )
	:
	wxFundGroups( parent)
	{
		AddedOrModifiedGroups = false;
		DeletedGroups = false;
		FundIndexes = new TFundIndexes[0];
	}

	void ombFundGroups::RefreshControls(void){
		bool GroupSelection = GroupList->GetSelection() >= 0;
		DeleteBtn->Enable(GroupSelection);
		FundList->Enable(GroupSelection);
	}

	void ombFundGroups::AddGroupClick( wxCommandEvent& event ){
		wxString Name = NameEdit->GetValue();

		if(Name.IsEmpty()){
			Error(51, wxEmptyString);
			NameEdit->SetFocus();
			return;
		}

		bool Found = false;
		for(unsigned int i = 0; i < GroupList->GetCount(); i++)
		if(GroupList->GetString(i).CmpNoCase(Name) == 0){
			Found = true;
			break;
		}
		if(Found){
			Error(52, Name);
			NameEdit->SetFocus();
			return;
		}
		else{
			Name = iUpperCase(Name);
			GroupList->Append(Name);
			AddedOrModifiedGroups = true;
			GroupList->SetSelection(GroupList->GetCount() - 1);
			NameEdit->SetValue(wxEmptyString);
			RefreshControls();
		}
	}

	void ombFundGroups::GroupSelected( wxCommandEvent& event ){
		int Sel = GroupList->GetSelection();
		if(Sel > -1){
			wxString GroupName = GroupList->GetString(Sel);
			wxString ChildName;

			int Count = FundList->GetCount();

			for(int i = 0; i < Count; i++){
				ChildName = FundList->GetString(i);
				for(int j = 0; j <= Count; j++){
					if(FundIndexes[j].FundName.CmpNoCase(ChildName) == 0){
						FundList->Check(i, FundIndexes[j].OwnerName.CmpNoCase(GroupName) == 0);
						break;
					}
				}
			}
		}

		RefreshControls();
	}

	void ombFundGroups::DeleteGroupClick( wxCommandEvent& event ){
		int Selection = GroupList->GetSelection();
		if(Selection >= 0){
			GroupList->Delete(Selection);
			DeletedGroups = true;
			RefreshControls();
		}
	}

	void ombFundGroups::FundToggled( wxCommandEvent& event ){
		int Sel = GroupList->GetSelection();
		if(Sel > -1){
			wxString GroupName = GroupList->GetString(Sel);

			int Pos = event.GetSelection();
			bool Checked = FundList->IsChecked(Pos);
			wxString FundName = FundList->GetString(Pos);

//			bool Found = false;

			for(unsigned int i = 0; i <= FundList->GetCount(); i++){
				if(FundName.CmpNoCase(FundIndexes[i].FundName) == 0){
//					Found = true;
					if(Checked)FundIndexes[i].OwnerName = GroupName;
					else{
						FundIndexes[i].OwnerName = wxEmptyString;
						FundIndexes[i].Owner = -1;
					}
					break;
				}
			}

			AddedOrModifiedGroups = true;

		}
	}

	void ombFundGroups::OKBtnClick( wxCommandEvent& event ){
		if(FundList->IsEnabled()){
			// Look for groups with less than 2 funds
			int Pos = 0, Sum = -1;
			int FTG = FundList->GetCount();
			int OwnerID;
			wxString OwnerName;
			while(Pos < FTG){

				if(! FundIndexes[Pos].OwnerName.IsEmpty()){
					OwnerID = FundIndexes[Pos].Owner;
					OwnerName = FundIndexes[Pos].OwnerName;
					Sum = 0;
					for(int i = 0; i < FTG; i++)
						if(((FundIndexes[i].Owner == OwnerID) && (FundIndexes[i].Owner > -1)) || (FundIndexes[i].OwnerName.CmpNoCase(OwnerName) == 0))
							Sum++;
					if(Sum < 2){
						Error(50, wxEmptyString);
						return;
					}
				}

				Pos++;
			}
			if(Sum == -1){
				Error(50, wxEmptyString);
				return;
			}
		}

		EndModal(wxID_OK);
	}

#endif // FUNDGROUPS_H_INCLUDED
