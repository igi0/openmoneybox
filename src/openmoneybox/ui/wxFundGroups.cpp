///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxFundGroups.h"

#include "../../../rsrc/toolbar/24/delete_bin_line.xpm"
#include "../../../rsrc/toolbar/24/greenplus24.xpm"

///////////////////////////////////////////////////////////////////////////

wxFundGroups::wxFundGroups( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 400,450 ), wxDefaultSize );

	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 7, 1, 0, 0 );
	fgSizer1->AddGrowableCol( 0 );
	fgSizer1->AddGrowableRow( 6 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText1 = new wxStaticText( this, wxID_ANY, _("Avaliable groups:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	fgSizer1->Add( m_staticText1, 0, wxALL, 5 );

	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer2->AddGrowableCol( 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	GroupList = new wxListBox( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	fgSizer2->Add( GroupList, 1, wxALL|wxEXPAND, 5 );

	DeleteBtn = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	DeleteBtn->SetBitmap( wxBitmap( delete_bin_line_xpm ) );
	DeleteBtn->SetToolTip( _("Remove the selected group") );

	fgSizer2->Add( DeleteBtn, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	fgSizer1->Add( fgSizer2, 1, wxEXPAND, 5 );

	wxFlexGridSizer* fgSizer4;
	fgSizer4 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer4->AddGrowableCol( 0 );
	fgSizer4->SetFlexibleDirection( wxBOTH );
	fgSizer4->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	NameEdit = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	NameEdit->SetToolTip( _("Insert the name of the group to add") );

	fgSizer4->Add( NameEdit, 1, wxALL|wxEXPAND, 5 );

	m_bpButton1 = new wxBitmapButton( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW|0 );

	m_bpButton1->SetBitmap( wxBitmap( greenplus24_xpm ) );
	m_bpButton1->SetToolTip( _("Add the inserted group to the list") );

	fgSizer4->Add( m_bpButton1, 0, wxALL|wxALIGN_RIGHT, 5 );


	fgSizer1->Add( fgSizer4, 1, wxEXPAND, 5 );

	m_staticText3 = new wxStaticText( this, wxID_ANY, _("Funds:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	fgSizer1->Add( m_staticText3, 0, wxALL, 5 );

	wxArrayString FundListChoices;
	FundList = new wxCheckListBox( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, FundListChoices, wxLB_MULTIPLE|wxLB_SORT );
	FundList->SetMaxSize( wxSize( -1,150 ) );

	fgSizer1->Add( FundList, 1, wxALL|wxEXPAND, 5 );

	m_staticText31 = new wxStaticText( this, wxID_ANY, _("Note: Each fund can be part of only a group.\nNote: the default fund cannot be grouped."), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText31->Wrap( -1 );
	fgSizer1->Add( m_staticText31, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	wxFlexGridSizer* fgSizer3;
	fgSizer3 = new wxFlexGridSizer( 1, 2, 0, 0 );
	fgSizer3->AddGrowableCol( 0 );
	fgSizer3->AddGrowableCol( 1 );
	fgSizer3->AddGrowableRow( 0 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_button3 = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer3->Add( m_button3, 1, wxALL|wxALIGN_RIGHT|wxALIGN_BOTTOM, 5 );

	OKBtn = new wxButton( this, wxID_OK, _("OK"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer3->Add( OKBtn, 0, wxALL|wxALIGN_BOTTOM, 5 );


	fgSizer1->Add( fgSizer3, 1, wxEXPAND, 5 );


	this->SetSizer( fgSizer1 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	GroupList->Connect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( wxFundGroups::GroupSelected ), NULL, this );
	DeleteBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxFundGroups::DeleteGroupClick ), NULL, this );
	m_bpButton1->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxFundGroups::AddGroupClick ), NULL, this );
	FundList->Connect( wxEVT_COMMAND_CHECKLISTBOX_TOGGLED, wxCommandEventHandler( wxFundGroups::FundToggled ), NULL, this );
	OKBtn->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxFundGroups::OKBtnClick ), NULL, this );
}

wxFundGroups::~wxFundGroups()
{
}
