///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/toolbar.h>
#include <wx/stattext.h>
#include <wx/treectrl.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/statbmp.h>
#include <wx/splitter.h>
#include <wx/calctrl.h>
#include <wx/grid.h>
#include <wx/listbox.h>
#include <wx/gauge.h>
#include <wx/statusbr.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxMainFrame
///////////////////////////////////////////////////////////////////////////////
class wxMainFrame : public wxFrame
{
	DECLARE_EVENT_TABLE()
	private:

		// Private event handlers
		void _wxFB_FormClose( wxCloseEvent& event ){ FormClose( event ); }
		void _wxFB_onResize( wxSizeEvent& event ){ onResize( event ); }
		void _wxFB_SaveClick( wxCommandEvent& event ){ SaveClick( event ); }
		void _wxFB_RevertClick( wxCommandEvent& event ){ RevertClick( event ); }
		void _wxFB_TextConvClick( wxCommandEvent& event ){ TextConvClick( event ); }
		void _wxFB_ViewArchiveClick( wxCommandEvent& event ){ ViewArchiveClick( event ); }
		void _wxFB_ModifyPasswordClick( wxCommandEvent& event ){ ModifyPasswordClick( event ); }
		void _wxFB_ExitClick( wxCommandEvent& event ){ ExitClick( event ); }
		void _wxFB_CopyClick( wxCommandEvent& event ){ CopyClick( event ); }
		void _wxFB_ShowFindDialog( wxCommandEvent& event ){ ShowFindDialog( event ); }
		void _wxFB_NewFundClick( wxCommandEvent& event ){ NewFundClick( event ); }
		void _wxFB_RemoveFundClick( wxCommandEvent& event ){ RemoveFundClick( event ); }
		void _wxFB_ResetFundClick( wxCommandEvent& event ){ ResetFundClick( event ); }
		void _wxFB_SetTotalClick( wxCommandEvent& event ){ SetTotalClick( event ); }
		void _wxFB_DefaultFundClick( wxCommandEvent& event ){ DefaultFundClick( event ); }
		void _wxFB_GainClick( wxCommandEvent& event ){ GainClick( event ); }
		void _wxFB_ExpenseClick( wxCommandEvent& event ){ ExpenseClick( event ); }
		void _wxFB_CategoryClick( wxCommandEvent& event ){ CategoryClick( event ); }
		void _wxFB_EditCategoriesClick( wxCommandEvent& event ){ EditCategoriesClick( event ); }
		void _wxFB_ReceivedClick( wxCommandEvent& event ){ ReceivedClick( event ); }
		void _wxFB_GiftedClick( wxCommandEvent& event ){ GiftedClick( event ); }
		void _wxFB_LendClick( wxCommandEvent& event ){ LendClick( event ); }
		void _wxFB_GetBackClick( wxCommandEvent& event ){ GetBackClick( event ); }
		void _wxFB_BorrowClick( wxCommandEvent& event ){ BorrowClick( event ); }
		void _wxFB_GiveBackClick( wxCommandEvent& event ){ GiveBackClick( event ); }
		void _wxFB_NewCreditClick( wxCommandEvent& event ){ NewCreditClick( event ); }
		void _wxFB_RemoveCreditClick( wxCommandEvent& event ){ RemoveCreditClick( event ); }
		void _wxFB_RemitCreditClick( wxCommandEvent& event ){ RemitCreditClick( event ); }
		void _wxFB_NewDebtClick( wxCommandEvent& event ){ NewDebtClick( event ); }
		void _wxFB_RemoveDebtClick( wxCommandEvent& event ){ RemoveDebtClick( event ); }
		void _wxFB_RemitDebtClick( wxCommandEvent& event ){ RemitDebtClick( event ); }
		void _wxFB_AddShopItemClick( wxCommandEvent& event ){ AddShopItemClick( event ); }
		void _wxFB_DelShopItemClick( wxCommandEvent& event ){ DelShopItemClick( event ); }
		void _wxFB_WizClick( wxCommandEvent& event ){ WizClick( event ); }
		void _wxFB_OldConv1Click( wxCommandEvent& event ){ OldConv1Click( event ); }
		void _wxFB_ImportDocument( wxCommandEvent& event ){ ImportDocument( event ); }
		void _wxFB_OptionsClick( wxCommandEvent& event ){ OptionsClick( event ); }
		void _wxFB_HelpMenuClick( wxCommandEvent& event ){ HelpMenuClick( event ); }
		void _wxFB_AuthorClick( wxCommandEvent& event ){ AuthorClick( event ); }
		void _wxFB_WebSiteClick( wxCommandEvent& event ){ WebSiteClick( event ); }
		void _wxFB_DonateClick( wxCommandEvent& event ){ DonateClick( event ); }
		void _wxFB_BugClick( wxCommandEvent& event ){ BugClick( event ); }
		void _wxFB_FundResize( wxSplitterEvent& event ){ FundResize( event ); }
		void _wxFB_CheckDate( wxCalendarEvent& event ){ CheckDate( event ); }
		void _wxFB_ReportSelect( wxGridEvent& event ){ ReportSelect( event ); }
		void _wxFB_PopRepPopup( wxGridEvent& event ){ PopRepPopup( event ); }
		void _wxFB_ReportRangeSelect( wxGridRangeSelectEvent& event ){ ReportRangeSelect( event ); }


	protected:
		enum
		{
			bilSave = 1000,
			bilRevert,
			bilXML,
			bilArchive,
			bilModifyPassword,
			bilCopy,
			bilFind,
			bilNewFund,
			bilRemoveFund,
			bilResetFund,
			bilSetTotal,
			bilDefaultFund,
			bilGain,
			bilExpense,
			ombCategory,
			bilEditCategories,
			bilReceived,
			bilGifted,
			bilLend,
			bilGetBack,
			bilBorrow,
			bilGiveBack,
			bilNewCredit,
			bilRemoveCredit,
			ombRemitCredit,
			bilNewDebt,
			bilRemoveDebt,
			ombRemitDebt,
			bilAddShopItem,
			bilDelShopItem,
			bilWiz,
			bilOldConv,
			bilImport,
			bilOptions,
			bilHelpMenu,
			bilWebSite,
			bilDonate,
			bilBug,
			bilNewDebit,
		};

		wxMenuBar* MAINMENU;
		wxMenu* MainMenu_File_;
		wxMenuItem* Save;
		wxMenuItem* Revert;
		wxMenuItem* XML;
		wxMenuItem* ViewArchive;
		wxMenuItem* ModifyPassword;
		wxMenuItem* Exit;
		wxMenu* MainMenu_Edit_;
		wxMenuItem* Copy;
		wxMenuItem* Find1;
		wxMenu* MainMenu_Funds_;
		wxMenuItem* NewFund;
		wxMenuItem* RemoveFund;
		wxMenuItem* ResetFund;
		wxMenuItem* SetTotal;
		wxMenuItem* DefaultFund;
		wxMenu* MainMenu_Operations_;
		wxMenuItem* Gain;
		wxMenuItem* Expense;
		wxMenuItem* category;
		wxMenu* MainMenu_Objects_;
		wxMenuItem* Received;
		wxMenuItem* Gifted;
		wxMenuItem* Lend;
		wxMenuItem* GetBack;
		wxMenuItem* Borrow;
		wxMenuItem* GiveBack;
		wxMenu* MainMenu_Credits_;
		wxMenuItem* NewCredit;
		wxMenuItem* RemoveCredit;
		wxMenuItem* RemitCredit;
		wxMenu* MainMenu_Debts_;
		wxMenuItem* NewDebt;
		wxMenuItem* RemoveDebt;
		wxMenuItem* RemitDebt;
		wxMenu* MainMenu_ShopList_;
		wxMenuItem* AddShopItem;
		wxMenuItem* DelShopItem;
		wxMenu* MainMenu_Tools_;
		wxMenuItem* Wiz;
		wxMenuItem* OldConv;
		wxMenuItem* Import;
		wxMenu* MainMenu_ExtTools;
		wxMenuItem* Options;
		wxMenu* MainMenu_Help_;
		wxMenuItem* Help1;
		wxMenuItem* Author;
		wxMenuItem* Website;
		wxMenuItem* Donate;
		wxToolBar* ToolBar;
		wxToolBarToolBase* Tool_Save;
		wxToolBarToolBase* Tool_XML;
		wxToolBarToolBase* Tool_NewFund;
		wxToolBarToolBase* Tool_RemoveFund;
		wxToolBarToolBase* Tool_ResetFund;
		wxToolBarToolBase* Tool_Gain;
		wxToolBarToolBase* Tool_Expense;
		wxToolBarToolBase* Tool_Category;
		wxToolBarToolBase* Tool_Received;
		wxToolBarToolBase* Tool_Gifted;
		wxToolBarToolBase* Tool_Lend;
		wxToolBarToolBase* Tool_GetBack;
		wxToolBarToolBase* Tool_Borrow;
		wxToolBarToolBase* Tool_GiveBack;
		wxToolBarToolBase* Tool_NewCredit;
		wxToolBarToolBase* Tool_RemoveCredit;
		wxToolBarToolBase* Tool_RemitCredit;
		wxToolBarToolBase* Tool_NewDebt;
		wxToolBarToolBase* Tool_RemoveDebt;
		wxToolBarToolBase* Tool_RemitDebt;
		wxToolBarToolBase* Tool_AddShopItem;
		wxToolBarToolBase* Tool_RemShopItem;
		wxFlexGridSizer* Container;
		wxStaticText* Title;
		wxSplitterWindow* m_splitter7;
		wxPanel* Panel2;
		wxPanel* m_panel4;
		wxSplitterWindow* m_splitter3;
		wxPanel* m_panel5;
		wxTreeCtrl* ItemList;
		wxMenu* PopList;
		wxMenu* Funds2;
		wxMenu* Cred2;
		wxMenu* Deb2;
		wxMenu* Obj2;
		wxMenu* ShopList2;
		wxPanel* m_panel6;
		wxSplitterWindow* m_splitter4;
		wxPanel* FundPanel;
		wxStaticBitmap* FundChart;
		wxPanel* TrendPanel;
		wxFlexGridSizer* TrendSizer;
		wxStaticBitmap* TrendChart;
		wxFlexGridSizer* fgSizer9;
		wxCalendarCtrl* Calendar;
		wxPanel* m_panel3;
		wxSplitterWindow* m_splitter41;
		wxPanel* m_panel8;
		wxGrid* Report;
		wxMenu* PopRep;
		wxMenuItem* Category2;
		wxPanel* m_panel9;
		wxStaticText* ShopLabel;
		wxListBox* ShopList;
		wxStatusBar* StatusBar;

		// Virtual event handlers, override them in your derived class
		virtual void FormClose( wxCloseEvent& event ) { event.Skip(); }
		virtual void onResize( wxSizeEvent& event ) { event.Skip(); }
		virtual void SaveClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void RevertClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void TextConvClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void ViewArchiveClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void ModifyPasswordClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void ExitClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void CopyClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void ShowFindDialog( wxCommandEvent& event ) { event.Skip(); }
		virtual void NewFundClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void RemoveFundClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void ResetFundClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void SetTotalClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void DefaultFundClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void GainClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void ExpenseClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void CategoryClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void EditCategoriesClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void ReceivedClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void GiftedClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void LendClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void GetBackClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BorrowClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void GiveBackClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void NewCreditClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void RemoveCreditClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void RemitCreditClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void NewDebtClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void RemoveDebtClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void RemitDebtClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void AddShopItemClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void DelShopItemClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void WizClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OldConv1Click( wxCommandEvent& event ) { event.Skip(); }
		virtual void ImportDocument( wxCommandEvent& event ) { event.Skip(); }
		virtual void OptionsClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void HelpMenuClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void AuthorClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void WebSiteClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void DonateClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void BugClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void FundResize( wxSplitterEvent& event ) { event.Skip(); }
		virtual void CheckDate( wxCalendarEvent& event ) { event.Skip(); }
		virtual void ReportSelect( wxGridEvent& event ) { event.Skip(); }
		virtual void PopRepPopup( wxGridEvent& event ) { event.Skip(); }
		virtual void ReportRangeSelect( wxGridRangeSelectEvent& event ) { event.Skip(); }


	public:
		wxFlexGridSizer* FundSizer;
		wxGauge* Progress;

		wxMainFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Budget management"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 790,630 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~wxMainFrame();

		void m_splitter7OnIdle( wxIdleEvent& )
		{
			m_splitter7->SetSashPosition( 0 );
			m_splitter7->Disconnect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter7OnIdle ), NULL, this );
		}

		void m_splitter3OnIdle( wxIdleEvent& )
		{
			m_splitter3->SetSashPosition( 258 );
			m_splitter3->Disconnect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter3OnIdle ), NULL, this );
		}

		void ItemListOnContextMenu( wxMouseEvent &event )
		{
			ItemList->PopupMenu( PopList, event.GetPosition() );
		}

		void m_splitter4OnIdle( wxIdleEvent& )
		{
			m_splitter4->SetSashPosition( 0 );
			m_splitter4->Disconnect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter4OnIdle ), NULL, this );
		}

		void m_splitter41OnIdle( wxIdleEvent& )
		{
			m_splitter41->SetSashPosition( 800 );
			m_splitter41->Disconnect( wxEVT_IDLE, wxIdleEventHandler( wxMainFrame::m_splitter41OnIdle ), NULL, this );
		}

		void ReportOnContextMenu( wxMouseEvent &event )
		{
			Report->PopupMenu( PopRep, event.GetPosition() );
		}

};

