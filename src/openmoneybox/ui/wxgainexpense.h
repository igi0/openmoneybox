///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/combobox.h>
#include <wx/textctrl.h>
#include <wx/valtext.h>
#include <wx/checkbox.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxTOperationF
///////////////////////////////////////////////////////////////////////////////
class wxTOperationF : public wxDialog
{
	private:

	protected:
		wxFlexGridSizer* fgSizer3;
		wxStaticText* LFund;
		wxStaticText* LValue;
		wxPanel* m_panel1;
		wxStaticText* RateLabel;
		wxStaticText* LMat;
		wxButton* OKBtn;
		wxButton* CancelBtn;

		// Virtual event handlers, override them in your derived class
		virtual void FundNameKeyPress( wxKeyEvent& event ) { event.Skip(); }
		virtual void CurrencyClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OKBtnClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxComboBox* FundName;
		wxTextCtrl* OpValue;
		wxCheckBox* CurrencyCheckbox;
		wxTextCtrl* Currency;
		wxTextCtrl* Rate;
		wxComboBox* Matter;
		wxStaticText* LCat;
		wxComboBox* Category;
		wxCheckBox* SysTimeCheck;
		wxString validator_string;
		wxString validator_string1;

		wxTOperationF( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 315,393 ), long style = wxCAPTION|wxDEFAULT_DIALOG_STYLE );

		~wxTOperationF();

};

