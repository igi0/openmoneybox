/***************************************************************
 * Name:      openmoneybix.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-10-13
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OPENMONEYBOX_CPP_INCLUDED
#define OPENMONEYBOX_CPP_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#ifndef __WXMSW__
  #include <signal.h>
#else
	#include <wx/msw/registry.h> // For wxRegKey
#endif // __WXMSW__

#ifdef _OMB_USE_GSETTINGS
	#include <gio/gio.h>	// For GSettings
#else
	#include <wx/fileconf.h> // For wxFileConfig
#endif // OMB_USE_GSETTINGS

#include <wx/app.h>
//#include <wx/cmdline.h>
#include <wx/filename.h>    // For wxFileName
#include <wx/intl.h> // For i18n
#include <wx/snglinst.h>	// for wxSingleInstanceChecker
#include <wx/stdpaths.h> // For wxStandardPathsBase

/*
#if((defined _OMB_USE_GTKHEADERBAR) && (defined wxHAS_NATIVE_CONTAINER_WINDOW))
	#include <gtk/gtk.h>

	#include <wx/nativewin.h>
#endif // _OMB_USE_GTKHEADERBAR
*/
/*
#ifdef _OMB_USE_GTKHEADERBAR
		#include <gtk/gtk.h>
#endif // _OMB_USE_GTKHEADERBAR
*/

#ifdef __WXGTK__
	#include "../platformsetup.h"
#endif // __WXGTK__
#include "../types.h"
#include "openmoneybox.h"
//#include "constants.h"
#include "ui/main_wx.h"

#ifdef _OMB_MONOLITHIC
	#include "../ombtray/ombtaskbar.h"

	#ifdef _OMB_USEINDICATOR
		#include <sys/msg.h>
		#include "../ombtray/mailbox.h"
	#endif // _OMB_USEINDICATOR

	#if ((defined ( __OPENSUSE__ )) || (defined ( __WXMSW__ )))
		#include "../../rsrc/icons/16/logo_pale.xpm"
		#include "../../rsrc/icons/16/logo_disabled.xpm"
		#include "../../rsrc/icons/16/ombtray_attention.xpm"
	#else
		#ifndef __OPENSOLARIS__
			#include "../../rsrc/icons/logo_pale.xpm"
			#include "../../rsrc/icons/logo_disabled.xpm"
		#else
		#include "../../rsrc/icons/logo_dark.xpm"
		#include "../../rsrc/icons/logo_disabled_dark.xpm"
		#endif // __OPENSOLARIS__
		#include "../../rsrc/icons/ombtray_attention.xpm"
	#endif // __OPENSUSE__

	#ifdef __WXMSW__
    #ifndef _OMB_USEREGISTRY
      wxFileConfig *INI;
    #endif // _OMB_USEREGISTRY
		#include "../../rsrc/icons/logo_dark.xpm"
		#include "../../rsrc/icons/logo_disabled_dark.xpm"
	#endif // __WXMSW__

	#ifdef __WXMAC__
		#include <wx/notifmsg.h>
	#endif // __WXMAC__

	#ifdef _OMB_DEBUG
		const int MainTimerInt = 10000;
	#else
		const int MainTimerInt = 600000;			// Data check timer
	#endif // _OMB_DEBUG
	const int TermTimerInt = 3000;				// Kill check timer
	#ifdef _OMB_USEINDICATOR
		const int CommandTimerInt = 1000;		// Command check timer (_OMB_USEINDICATOR)
	#endif // _OMB_USEINDICATOR

	#ifdef _OMB_USE_CIPHER
		extern int ExecuteUpdate(sqlite3 *m_db, const char* sql);
	#endif // _OMB_USE_CIPHER

#endif // _OMB_MONOLITHIC

// Code start

#include "../languages.cpp"	// Contains language definitions

// the arrays must be in sync
wxCOMPILE_TIME_ASSERT( WXSIZEOF(langNames)==WXSIZEOF(langIds),LangArraysMismatch);

#ifndef __WXMSW__
 	bool Killed = false;	// Set to true when SIGTERM is received
  wxTimer *CheckTimer;
#endif // __WXMSW__
wxLanguage Lan;

wxLocale omb_locale;
#ifndef __WXMSW__
	#ifdef _OMB_USE_GSETTINGS
		#ifdef _OMB_MONOLITHIC
			extern GSettings *settings_general;
			extern GSettings *settings_charts;
			extern GSettings *settings_tools;
			#ifdef _OMB_INSTALLEDUPDATE
				extern GSettings *settings_advanced;
			#endif // _OMB_INSTALLEDUPDATE
			#ifdef _OMB_USE_CIPHER
				#ifdef _OMB_SQLCIPHER_v4
					extern GSettings *settings_database;
				#endif // _OMB_SQLCIPHER_v4
			#endif // _OMB_USE_CIPHER
		#else
			GSettings *settings_general;
			GSettings *settings_charts;
			GSettings *settings_tools;
			#ifdef _OMB_INSTALLEDUPDATE
				GSettings *settings_advanced;
			#endif // _OMB_INSTALLEDUPDATE
		#endif // _OMB_MONOLITHIC
	#else
  	wxFileConfig *INI;
	#endif // _OMB_USE_GSETTINGS
#endif // __WXMSW__

#ifdef _OMB_MONOLITHIC
	ombTaskBarIcon *Tray;
#endif // _OMB_MONOLITHIC

TData *Data;

ombMainFrame *frame;
#ifndef NDEBUG
	FILE * m_pLogFile;
#endif // NDEBUG

#ifdef _OMB_MONOLITHIC
	#ifdef _OMB_USEINDICATOR
		omb_msgbuf command;
		wxTimer *CommandTimer;
	#endif // _OMB_USEINDICATOR

	bool HasAlarms = false;

	#ifdef __WXMSW__
		long LightTheme = 1;
	#endif // __WXMSW__

#endif // _OMB_MONOLITHIC

IMPLEMENT_APP(ombApp)

#ifndef __WXMSW__
  void term(int signum){	// Captures the SIGTERM signal
                          // http://airtower.wordpress.com/2010/06/16/catch-sigterm-exit-gracefully/
      Killed = true;}
#endif // __WXMSW__

bool ombApp::OnInit(){

	#ifdef __WXMSW__
		#ifndef NDEBUG
			if (m_pLogFile == NULL)
			{
				m_pLogFile = fopen(GetInstallationPath() + L"\\omb.log", "w" );	// flawfinder: ignore
				delete wxLog::SetActiveTarget(new wxLogStderr(m_pLogFile));
			}
			ombLogMessage("Program starts...");
		#endif // NDEBUG
		#ifndef _OMB_USEREGISTRY
		  wxString homecfg;
		  #ifdef _OMB_PORTABLE_MSW
        wxFileName dir(wxGetCwd());
        dir.RemoveLastDir();
        wxString testFolder = dir.GetPath() + L"\\Data";
        if((! testFolder.IsEmpty()) && (wxDirExists(testFolder))) homecfg = testFolder + L"\\omb.ini";
        else {
      #endif // _OMB_PORTABLE_MSW

			homecfg = wxGetCwd() + L"\\omb.ini";
			if(!::wxFileExists(homecfg)){
				wxFile *f=new wxFile(homecfg,wxFile::write);
				//f->Create(homecfg,false,wxS_DEFAULT);
				if(! f->IsOpened()){
					// FIXME (igor#1#): insert error handling
					return false;
				}
				f->Close();}
      #ifdef _OMB_PORTABLE_MSW
        }
      #endif // _OMB_PORTABLE_MSW
			INI = new wxFileConfig(wxEmptyString, wxEmptyString, homecfg, wxEmptyString, wxCONFIG_USE_LOCAL_FILE, wxConvAuto());
		#endif // _OMB_USEREGISTRY
	#else
		wxString home_dir = wxGetHomeDir();
		wxString homecfg = GetUserConfigDir();

		#ifndef NDEBUG
			// Start logging
			if (m_pLogFile == NULL)
			{
				#ifdef __WXGTK__
					#ifndef __OPENSUSE__
						m_pLogFile = fopen( homecfg + "/omb.log", "w" );	// flawfinder: ignore
					#else
						wxString f = homecfg + L"/omb.log";
						wxString am = L"w";
						m_pLogFile = fopen( f.fn_str(), am.fn_str() );	// flawfinder: ignore
					#endif // __OPENSUSE__
				#endif // __WXGTK__
				delete wxLog::SetActiveTarget(new wxLogStderr(m_pLogFile));
			}
			ombLogMessage(L"Program starts...");
		#endif // NDEBUG

		#ifdef _OMB_USE_GSETTINGS
			settings_general = g_settings_new("org.igisw.openmoneybox.general");
			settings_charts = g_settings_new("org.igisw.openmoneybox.charts");
			settings_tools = g_settings_new("org.igisw.openmoneybox.tools");

			#ifdef _OMB_USE_CIPHER
				#ifdef _OMB_SQLCIPHER_v4
					settings_database = g_settings_new("org.igisw.openmoneybox.database");
				#endif // _OMB_SQLCIPHER_v4
			#endif // _OMB_USE_CIPHER

			#ifdef _OMB_INSTALLEDUPDATE
				settings_advanced = g_settings_new("org.igisw.openmoneybox.advanced");
			#endif // _OMB_INSTALLEDUPDATE
		#else
			homecfg += L"/omb.ini";
			if(!::wxFileExists(homecfg)){
				wxFile *f=new wxFile(homecfg,wxFile::write);
				//f->Create(homecfg,false,wxS_DEFAULT);
				if(! f->IsOpened()){
					// FIXME (igor#1#): insert error handling
					return false;
				}
				f->Close();}
			INI = new wxFileConfig(wxEmptyString, wxEmptyString, homecfg, wxEmptyString, wxCONFIG_USE_LOCAL_FILE, wxConvAuto());
		#endif // _OMB_USE_GSETTINGS

	#endif // __WXMSW__

	#ifdef _OMB_DEBUG
		#ifdef __WXGTK__
      wxLocale::AddCatalogLookupPathPrefix(L".");
    #endif // __WXGTK__
	#endif // _OMB_DEBUG

	#ifdef __WXMSW__
    wxLocale::AddCatalogLookupPathPrefix(GetShareDir());
	#else
    wxLocale::AddCatalogLookupPathPrefix(GetShareDir()+L"locale");
		#ifdef _OMB_DEBUG
			wxLocale::AddCatalogLookupPathPrefix(wxGetCwd());
		#endif // _OMB_DEBUG
	#endif // __WXMSW__
	#ifdef __WXMAC__
			wxLocale::AddCatalogLookupPathPrefix(AppDir);
			wxFileName f(wxStandardPaths::Get().GetExecutablePath());
            wxString appPath(f.GetPath());
			wxLocale::AddCatalogLookupPathPrefix(appPath);
	#endif // __WXMAC__
	omb_locale.Init(wxLANGUAGE_DEFAULT,wxLOCALE_LOAD_DEFAULT);
	omb_locale.AddCatalog(L"openmoneybox");
	Lan = FindLang();

	#ifdef _OMB_MONOLITHIC
		int arg_num = wxTheApp->argc;
		if(arg_num > 1){
			wxString Arg = wxTheApp->argv[1];
			if(Arg.CmpNoCase("--help") == 0)
			{
				const char* output_string_1 = _("\nSyntax: openmoneybox [[--tray] [--force]]\n\n").c_str();
				printf("%s", output_string_1);
				const char* output_string_2 = _("    --tray:  starts minimized in the system tray.\n").c_str();
				printf("%s", output_string_2);
				const char* output_string_3 = _("    --force: forces new instance.\n\n").c_str();
				printf("%s", output_string_3);
				return false;
			}
		}
	#endif // _OMB_MONOLITHIC

  // Check if already running
  wxString user = wxGetUserId();
  const wxString name = wxString::Format(L"omb-%s", user.c_str());

	#ifdef _OMB_MONOLITHIC
		#ifdef __WXGTK__
			if(arg_num > 2){
				wxString Arg = wxTheApp->argv[2];
				if(Arg.CmpNoCase("--force") == 0)
				{
					wxString InstanceCheckerFile = home_dir + L"/" + name;
					if(::wxFileExists(InstanceCheckerFile)) wxRemoveFile (InstanceCheckerFile);
				}
			}
		#endif // __WXGTK__
	#endif // _OMB_MONOLITHIC

	m_checker_omb = new wxSingleInstanceChecker(name);
	if (m_checker_omb->IsAnotherRunning()){
		wxMessageBox(_("OpenMoneyBox is already running!"),_("OpenMoneyBox"), wxICON_EXCLAMATION);
		return false;}

	#if defined (__WXMSW__) || defined (__WXMAC__)
    	bool frame_created = false;
	#endif // defined

	#ifdef _OMB_MONOLITHIC

		MainTimer = new wxTimer();
		MainTimer->SetOwner(this);

		#ifndef __WXMSW__
			// Prepare timer to check SIGTERM
			CheckTimer = new wxTimer();
			CheckTimer->SetOwner(this);
			CheckTimer->Start(TermTimerInt,false);
		#endif // __WXMSW__

	#else

		const wxString name2 = wxString::Format(L"ombtray-%s",user.c_str());
		wxSingleInstanceChecker *m_checker_ombtray = new wxSingleInstanceChecker(name2);
		if (m_checker_ombtray->IsAnotherRunning()){
			wxString Proc;
			#ifdef __WXMSW__
				long processid = 0;
				wxStandardPathsBase& Paths = ::wxStandardPaths::Get();
				wxString hm = Paths.GetTempDir() + L"\\bil";
				wxString tempfile = ::wxFileName::CreateTempFileName(hm);
				Proc = L"tasklist |find \"ombtray.exe\" >" + tempfile;

				// Following lines are necessary to avoid OpenMoneyBox exits because wxShell is recognized as top window
				frame = new ombMainFrame(wxTheApp->GetTopWindow());
				frame_created = true;

				::wxShell(Proc);
				if(::wxFileExists(tempfile)){
					int L, b;
					char *Buffer;
					wxString S = wxEmptyString;
					wxFile *file = new wxFile(tempfile, wxFile::read);	// flawfinder: ignore
					if(! file->IsOpened()){
						// FIXME (igor#1#): insert error handling
						return false;
					}
					L = file->SeekEnd(0);
					file->Seek(0);
					Buffer = new char[L + 1];
					file->Read(Buffer, L);

					b = 0;
					while ((Buffer[b] < char(48)) | (Buffer[b] > char(57))){
						b++;}
					do{
						S += wxChar(Buffer[b]);
						b++;}
					while ((Buffer[b] >= char(48)) & (Buffer[b] <= char(57)));

					S.ToLong(&processid);
					file->Close();
					::wxRemoveFile(tempfile);
					delete m_checker_ombtray;}
				if(processid > 0)wxKill(processid, wxSIGKILL);

			#else
				Proc = L"killall -u " + user + L" ombtray";
				wxExecute(Proc,wxEXEC_ASYNC);
				bool ombtray_killed = false;
				int s=0;
				while(s <= 60){
					if(m_checker_ombtray->IsAnotherRunning()){
						wxSleep(5);
						delete m_checker_ombtray;
						if(::wxFileExists(home_dir + L"/" + name2)) wxRemoveFile (home_dir + L"/" + name2);
						m_checker_ombtray = new wxSingleInstanceChecker(name2);
						s += 5;}
					else{
						delete m_checker_ombtray;
						ombtray_killed = true;
						break;}}
				if(! ombtray_killed) return false;
			#endif
		}
		else delete m_checker_ombtray;
		#ifndef NDEBUG
			ombLogMessage(L"Existing instances cleared");
		#endif // NDEBUG

	#endif // _OMB_MONOLITHIC

	#ifdef _OMB_USEINDICATOR
		if(isIndicator){
			CommandTimer = new wxTimer();
			CommandTimer->SetOwner(this);
		}
	#endif // _OMB_USEINDICATOR

	#ifdef _OMB_MONOLITHIC
		IsTrayCommand = false;
		if(arg_num > 1){
			wxString Arg = wxTheApp->argv[1];
			if(Arg.CmpNoCase("--tray") == 0)
			{
				IsTrayCommand = true;
				StartTray();
			}
		}
	#endif // _OMB_MONOLITHIC

 	// Document memory-space creation
	#ifdef _OMB_MONOLITHIC
		if(Data == NULL)
	#endif // _OMB_MONOLITHIC

	Data = new TData(NULL); // Progress bar is set after frame creation

  #ifndef NDEBUG
		ombLogMessage(L"Data created");
	#endif // NDEBUG

  // Main Window creation
  #if defined (__WXMSW__) || defined (__WXMAC__)
    if(! frame_created)
  #endif // defined

  /*
  #if((defined _OMB_USE_GTKHEADERBAR) && (defined wxHAS_NATIVE_CONTAINER_WINDOW))
			GError *error = NULL;
			GtkBuilder *builder = gtk_builder_new();
			gtk_builder_add_from_file (builder, "./test.glade", &error);
			GtkWidget *widget = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));

			//wxNativeWindow* dummy = new wxNativeWindow(wxTheApp->GetTopWindow(), wxID_ANY, widget);
			wxNativeContainerWindow *dummy = new wxNativeContainerWindow();
			dummy->
			dummy->Show();
  #else
  */
  /* dirty test code
  #ifdef _OMB_USE_GTKHEADERBAR
			GError *error = NULL;
			GtkBuilder *builder = gtk_builder_new();
			gtk_builder_add_from_file (builder, "/home/igor/Personale/Nextcloud/Scrivania/gtkheaderbar/test.glade", &error);
			GtkWidget *widget = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));

			GtkWidget *btn;

			btn = GTK_WIDGET(gtk_builder_get_object(builder, "find_btn"));
			gtk_button_set_image (GTK_BUTTON (btn), gtk_image_new_from_icon_name ("edit-find-symbolic", GTK_ICON_SIZE_BUTTON));

			btn = GTK_WIDGET(gtk_builder_get_object(builder, "settings_btn"));
			gtk_button_set_image (GTK_BUTTON (btn), gtk_image_new_from_icon_name ("configure-symbolic", GTK_ICON_SIZE_BUTTON));

			btn = GTK_WIDGET(gtk_builder_get_object(builder, "help_btn"));
			gtk_button_set_image (GTK_BUTTON (btn), gtk_image_new_from_icon_name ("help-symbolic", GTK_ICON_SIZE_BUTTON));

			btn = GTK_WIDGET(gtk_builder_get_object(builder, "about_btn"));
			gtk_button_set_image (GTK_BUTTON (btn), gtk_image_new_from_icon_name ("help-about-symbolic", GTK_ICON_SIZE_BUTTON));

			btn = GTK_WIDGET(gtk_builder_get_object(builder, "save_btn"));
			gtk_button_set_image (GTK_BUTTON (btn), gtk_image_new_from_icon_name ("document-save-symbolic", GTK_ICON_SIZE_BUTTON));

			btn = GTK_WIDGET(gtk_builder_get_object(builder, "options_btn"));
			gtk_button_set_image (GTK_BUTTON (btn), gtk_image_new_from_icon_name ("open-menu-symbolic", GTK_ICON_SIZE_BUTTON));

			GtkWidget *titlebar = gtk_window_get_titlebar(GTK_WINDOW(widget));

				frame = new ombMainFrame(wxTheApp->GetTopWindow());
				GtkBox *fake = GTK_BOX(gtk_builder_get_object(builder, "frame"));
				gtk_box_pack_start (GTK_BOX (fake), (GtkWidget *)frame, FALSE, FALSE, 0);

			gtk_window_maximize(GTK_WINDOW(widget));
			gtk_widget_show_all (widget);

			//gtk_main();
  #endif // _OMB_USE_GTKHEADERBAR
  */

	frame = new ombMainFrame(wxTheApp->GetTopWindow());
	#ifdef _OMB_MONOLITHIC
		frame->App = this;
		frame->IsTrayCommand = IsTrayCommand;
	#endif // _OMB_MONOLITHIC

	//#endif // _OMB_USE_GTKHEADERBAR

  #ifndef NDEBUG
		ombLogMessage(L"Main window created");
	#endif // NDEBUG

  Data->Progress = frame->Progress;

	if(! frame->AutoOpen()){
		delete Data;
		delete m_checker_omb;
		#if( (defined _OMB_MONOLITHIC) && (defined _OMB_USEINDICATOR) )
			KillIndicator(cmd_close);
		#endif
	  #ifndef NDEBUG
			ombLogMessage(L"Error opening main window!");
		#endif // NDEBUG
		Exit();
		return false;
	}
  #ifndef NDEBUG
		ombLogMessage(L"Auto document opening done");
	#endif // NDEBUG

	Data->Hour = ::wxDateTime::Now();

	frame->UpdateCalendar(wxInvalidDateTime);

	frame->Centre(wxBOTH);
	#ifdef _OMB_MONOLITHIC
		if(! IsTrayCommand){
	#endif // _OMB_MONOLITHIC
	frame->Show();
	#ifdef _OMB_CHART_MATLIBPLOT
		frame->ShowF();
	#endif // _OMB_CHART_MATLIBPLOT
	SetTopWindow(frame);

  #ifndef NDEBUG
		ombLogMessage(L"Main window set to top");
	#endif // NDEBUG
	#ifdef _OMB_MONOLITHIC
		}
	#endif // _OMB_MONOLITHIC

	#ifdef _OMB_INSTALLEDUPDATE
		if(GetCheckUpdates())fnCheckUpdate();	// Version update check
	#endif // _OMB_INSTALLEDUPDATE

	#ifndef __WXMSW__
    // Prepare timer to check SIGTERM
    #if(( (defined _OMB_MONOLITHIC) && defined (_OMB_USEINDICATOR) ) )
			Connect( wxEVT_TIMER, wxTimerEventHandler( ombApp::TimerFire ) );
		#else
    	Connect( wxEVT_TIMER, wxTimerEventHandler( ombApp::CheckTimerFire ) );
		#endif
    CheckTimer=new wxTimer();
    CheckTimer->SetOwner(this);
    CheckTimer->Start(500,false);
  #endif // __WXMSW__

  #ifndef NDEBUG
	 	ombLogMessage(L"Initialization over... starting...");
	#endif // NDEBUG
	return true;}

#ifndef __WXMSW__
  void ombApp::CheckTimerFire( wxTimerEvent& event ){
    #ifdef __WXGTK__
      // Following code prepare the action to capture SIGTERM signals
      struct sigaction action;
      memset(&action, 0, sizeof(struct sigaction));
      action.sa_handler = term;
      sigaction(SIGTERM, &action, NULL);
    #endif

    if(! Killed)return;

	  #ifndef NDEBUG
			ombLogMessage(L"Kill request received.");
		#endif // NDEBUG

		#ifdef _OMB_MONOLITHIC
			wxCloseEvent evt = wxCloseEvent(wxEVT_END_SESSION);
			frame->FormClose(evt);
		#else
			wxCloseEvent evt(wxEVT_CLOSE_WINDOW,0);
			frame->FormClose(evt);
    #endif // _OMB_MONOLITHIC

    return;}
#endif // __WXMSW__

int ombApp::OnExit(){
	#ifndef __WXMSW__
    CheckTimer->Stop();
    delete CheckTimer;
  #endif // __WXMSW__
	delete Data;
	delete m_checker_omb;

	#ifndef _OMB_MONOLITHIC
	if(TrayActive()){
		wxString Proc = wxEmptyString;
    wxStandardPaths std = wxStandardPaths::Get();
    wxString exePath = std.GetExecutablePath();
    wxString path;
    wxFileName::SplitPath (exePath, &path, NULL, NULL, wxPATH_NATIVE);

    #ifdef __WXMSW__
      Proc = "\"";
    #endif // __WXMSW__

    Proc = Proc + path;

    #ifdef __WXMSW__
      Proc = Proc + L"\\";
    #else
      Proc = Proc + L"/";
    #endif // __WXMSW__

    Proc = Proc + L"ombtray";

    #ifdef __WXMSW__
      Proc = Proc + L".exe\"";
    #endif // __WXMSW__

    #ifdef __WXMAC__
      Proc += L"-bin\"";
    #endif // __WXMSW__

	  #ifndef NDEBUG
		  ombLogMessage(L"Launching program " + Proc);
		#endif // NDEBUG
		wxExecute(Proc,wxEXEC_ASYNC);}
	#endif // _OMB_MONOLITHIC

	#ifndef _OMB_USEREGISTRY
		#ifdef _OMB_USE_GSETTINGS
			/*
			delete settings_general;
			delete settings_charts;
			delete settings_tools;
			*/
		#else
    	delete INI;
		#endif // _OMB_USE_GSETTINGS
  #endif // _OMB_USEREGISTRY

	#ifndef NDEBUG
		ombLogMessage(L"Program exiting...");
		delete wxLog::SetActiveTarget(NULL);
		if (m_pLogFile != NULL)
		{
			fclose(m_pLogFile);
		}
	#endif // NDEBUG

	return 0;}

#ifdef _OMB_MONOLITHIC

	int ombApp::StartTray(){

		if(Data == NULL){
			wxString Document = GetDefaultDocument();
			if(Document.IsEmpty()) return 0;
			if(! wxFileExists(Document)) return 0;

			Data = new TData(NULL);
			if(! Data->OpenDatabase(Document)) return 0;
		}

		#ifdef _OMB_USEINDICATOR
			isIndicator = false;
			wxString Desktop = GetDesktopEnv();

			#ifndef NDEBUG
				ombLogMessage(L"Desktop env: " + Desktop);
			#endif // NDEBUG

			/*
			if(Desktop == L"unity") isIndicator = true;												// until Ubuntu Zesty
			else*/ if(Desktop == L"unity:unity7:ubuntu") isIndicator = true;	// from Ubuntu Artful
			else if(Desktop.Right(5) == L"gnome") isIndicator = true;	// for Ubuntu Artful

			if(isIndicator){
				indicator_killed = false;
				//wxString Proc = wxGetCwd();
				wxString Proc = ::wxPathOnly(wxStandardPaths::Get().GetExecutablePath());
				#ifdef _OMB_DEBUG
					Proc += L"/Debug_GTK";
				#endif // _OMB_DEBUG
				Proc += L"/ombindicator";
				if(! wxExecute(Proc, wxEXEC_ASYNC))return false;

				//Create the Indicator Mailbox
				msqidInd = msgget(MAILBOX_INDICATOR, 0600 | IPC_CREAT);
				//Create the Ombtray Mailbox
				msqidOmb = msgget(MAILBOX_OMBTRAY, 0600 | IPC_CREAT);}
			else{
		#endif // _OMB_USEINDICATOR

		Tray = new ombTaskBarIcon();
		Tray->App = this;
		#ifdef _OMB_USE_CIPHER
			#ifdef _OMB_SQLCIPHER_v4
				Tray->DatabaseIsSqlCipher_V4 = frame->GetDatabaseIs_v4();
			#endif // _OMB_SQLCIPHER_v4
		#endif // _OMB_USE_CIPHER

		#ifdef _OMB_USEINDICATOR
			}
		#endif // _OMB_USEINDICATOR

		HasAlarms = Data->HasAlarms();
		#ifdef _OMB_USEINDICATOR
			if(isIndicator){
				Act = HasAlarms;
				SendStatus();
				if(! HasAlarms){
					wxString title, message;
					title = _("OpenMoneyBox");
					message = _("There is no alarm set in your budget document");
					#ifdef _OMB_NOTIFYSENDAPPSWITCH
						wxString proc = L"notify-send -i " + GetShareDir() + L"icons/hicolor/48x48/apps/openmoneybox.png -a " + title + L" \"" + message + L"\"";
					#else
						wxString proc = L"notify-send -i " + GetShareDir() + L"icons/hicolor/48x48/apps/openmoneybox.png " + title + L" \"" + message + L"\"";
					#endif // _OMB_NOTIFYSENDAPPSWITCH
					wxExecute(proc,wxEXEC_ASYNC);}}
			else{
		#endif // _OMB_USEINDICATOR

		#ifdef __WXMSW__
			wxRegKey *ThemeKey = new wxRegKey(wxRegKey::HKCU, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize", wxRegKey::WOW64ViewMode_Default);
			if(ThemeKey->HasValue("SystemUsesLightTheme")) ThemeKey->QueryValue("SystemUsesLightTheme", &LightTheme);
		#endif // __WXMSW__

			Tray->Act = HasAlarms;
			if(HasAlarms){
				#ifdef __WXMSW__
					if(LightTheme) Tray->SetIcon(wxIcon(logo_dark_xpm),_("Alarm Management"));
					else Tray->SetIcon(wxIcon(logo_pale_xpm),_("Alarm Management"));
				#else
					#ifndef __OPENSOLARIS__
						Tray->SetIcon(wxICON(logo_pale), _("Alarm Management"));
					#else
						Tray->SetIcon(wxICON(logo_dark), _("Alarm Management"));
					#endif // __OPENSOLARIS__
				#endif // __WXMSW__
			}
			else{
				wxString title,message;
				title=_("OpenMoneyBox");
				message=_("There is no alarm set in your budget document");
				#ifdef __WXMSW__
					if(LightTheme) Tray->SetIcon(wxIcon(logo_disabled_dark_xpm),_("Alarm Management"));
					else Tray->SetIcon(wxIcon(logo_disabled_xpm),_("Alarm Management"));
					Tray->ShowBalloon(title,message,10000,NIIF_INFO);
				#elif defined ( __WXMAC__ )
					Tray->SetIcon(wxIcon(logo_disabled_xpm),_("Alarm Management"));
					wxNotificationMessage *Notification = new wxNotificationMessage(title, message);
					Notification->Show();
				#else
					#ifndef __OPENSOLARIS__
						Tray->SetIcon(wxICON(logo_disabled), _("Alarm Management"));
					#else
						Tray->SetIcon(wxICON(logo_disabled_dark), _("Alarm Management"));
					#endif // __OPENSOLARIS__
					#ifdef _OMB_NOTIFYSENDAPPSWITCH
						wxString proc = L"notify-send -i " + GetShareDir() + L"icons/hicolor/48x48/apps/openmoneybox.png -a " + title + L" \""
					#else
						wxString proc = L"notify-send -i " + GetShareDir() + L"icons/hicolor/48x48/apps/openmoneybox.png " + title + L" \""
					#endif // _OMB_NOTIFYSENDAPPSWITCH
						+ message + L"\"";
					wxExecute(proc,wxEXEC_ASYNC);
				#endif
			}
		#ifdef _OMB_USEINDICATOR
			}
		#endif // _OMB_USEINDICATOR

		#ifdef _OMB_USEINDICATOR
			if(isIndicator){
				if(CommandTimer == NULL){
					CommandTimer = new wxTimer();
					CommandTimer->SetOwner(this);
				}
				CommandTimer->Start(CommandTimerInt,false);
			}
		#endif // _OMB_USEINDICATOR

		if(HasAlarms){
			if(MainTimer == NULL){
				MainTimer = new wxTimer();
				MainTimer->SetOwner(this);
			}
			MainTimer->Start(MainTimerInt, false);
		}

	return true;}

	void ombApp::TimerFire( wxTimerEvent& event ){
		switch(event.GetInterval()){
			case MainTimerInt:
				DoChecks();
				break;
			#ifndef __WXMSW__
				case TermTimerInt:
					CheckTimerFire(event);
					break;
			#endif // __WXMSW__

			#ifdef _OMB_USEINDICATOR
				case CommandTimerInt:
					if(! isIndicator) break;
					if (msgrcv( msqidInd, &command, sizeof(command), btCmd_do_action, IPC_NOWAIT) != -1){
						switch(command.value){
							case cmd_toggle_enable:
								Act = ! Act;
								SendStatus();
								break;
							case cmd_run_openmoneybox:
								if(ShowFrame()) KillIndicator(cmd_resume);
								break;
							case cmd_open_options:
								if(ShowOptions()){
											delete m_checker_omb;
											KillIndicator(cmd_resume);
											term(SIGTERM);
								}

								omb_msgbuf resume_message;
								resume_message.mtype = btCmd_kill;
								resume_message.value = cmd_resume;
								msgsnd( msqidOmb, &resume_message, sizeof(resume_message), 0);
								break;
							case cmd_close:
								#if (wxCHECK_VERSION(3, 1, 0) )	// FIXME (igor#1#): OpenSuse Leap 15.6
									// FIXME (igor#1#): frame is destroyed when the NDEBUG switch is set.
									if(frame != NULL){
								#endif
								wxCloseEvent evt = wxCloseEvent(wxEVT_END_SESSION, 111);	// Set Id to 111 not to start tray in ombMainFrame::FormClose()
								#ifndef __UBUNTU_2404__	// FIXME (igor#1#): frame is destroyed when the NDEBUG switch is set. / https://bugs.launchpad.net/bilancio/+bug/2056643
									frame->FormClose(evt);
								#endif // __UBUNTU_2404__
								#if (wxCHECK_VERSION(3, 1, 0) )	// FIXME (igor#1#): OpenSuse Leap 15.6
									}
								#endif
								this->ExitMainLoop();
								KillIndicator(cmd_close);
							}
						}
			#endif // _OMB_USEINDICATOR
	}}

	void ombApp::DoChecks(void)
	{
		MainTimer->Stop();
		#ifdef _OMB_USEINDICATOR
			CommandTimer->Stop();

			omb_msgbuf attention_message;
			if(isIndicator){
				attention_message.mtype = btCmd_send_status;
				attention_message.value = tray_attention;
				msgsnd( msqidOmb, &attention_message, sizeof(attention_message), 0);}
			else{
		#endif // _OMB_USEINDICATOR
			Tray->ringing = true;
			#ifdef __WXMSW__
				Tray->SetIcon(wxIcon(ombtray_attention_xpm),_("Alarm Management"));
			#else
				Tray->SetIcon(wxICON(ombtray_attention),_("Alarm Management"));
			#endif // __WXMSW__
		#ifdef _OMB_USEINDICATOR
			}
		#endif // _OMB_USEINDICATOR

		Data->CheckLentObjects();
		Data->CheckBorrowedObjects();
		Data->CheckShopList();

		if(Data->FileData.Modified){
			#ifdef _OMB_USE_CIPHER
				sqlite3_exec(Data->database, "RELEASE rollback;", NULL, NULL, NULL);
			#else
				Data->database->ReleaseSavepoint(L"rollback");
			#endif // _OMB_USE_CIPHER
			Data->FileData.Modified = false;
			#ifdef _OMB_USE_CIPHER
				ExecuteUpdate(Data->database, "SAVEPOINT rollback;");
			#else
				Data->database->Savepoint(L"rollback");
			#endif // _OMB_USE_CIPHER
		}

		#ifdef _OMB_USEINDICATOR
			if(isIndicator){
				attention_message.value = tray_enabled;
				msgsnd( msqidOmb, &attention_message, sizeof(attention_message), 0);}
			else{
		#endif // _OMB_USEINDICATOR
			Tray->ringing = false;
			#ifdef __WXMSW__
				if(LightTheme) Tray->SetIcon(wxIcon(logo_dark_xpm),_("Alarm Management"));
				else Tray->SetIcon(wxIcon(logo_pale_xpm),_("Alarm Management"));
			#else
				#ifndef __OPENSOLARIS__
					Tray->SetIcon(wxICON(logo_pale),_("Alarm Management"));
				#else
					Tray->SetIcon(wxICON(logo_dark),_("Alarm Management"));
				#endif // __OPENSOLARIS__
			#endif // __WXMSW__
		#ifdef _OMB_USEINDICATOR
			}
		#endif // _OMB_USEINDICATOR
		wxTheApp->Yield(false);
		MainTimer->Start(MainTimerInt,false);
		#ifdef _OMB_USEINDICATOR
			CommandTimer->Start(CommandTimerInt, false);
		#endif // _OMB_USEINDICATOR
	}

	#ifdef _OMB_USEINDICATOR
		void ombApp::SendStatus(void){
			omb_msgbuf init_message;
			init_message.mtype = btCmd_send_status;
			if(Act) init_message.value = tray_enabled;
			else init_message.value = tray_disabled;
			msgsnd( msqidOmb, &init_message, sizeof(init_message), 0);}

		void ombApp::KillIndicator(long cmd){
			//CommandTimer->Stop();

			if(indicator_killed) return;
			omb_msgbuf kill_message;
			kill_message.mtype = btCmd_kill;
			kill_message.value = cmd;
			msgsnd( msqidOmb, &kill_message, sizeof(kill_message), 0);
			if(cmd == cmd_close){
				term(SIGTERM);
				indicator_killed = true;}}

	#endif // _OMB_USEINDICATOR

	bool ombApp::ShowFrame(){
		#ifdef _OMB_USE_CIPHER
			sqlite3_exec(Data->database, "RELEASE rollback;", NULL, NULL, NULL);
		#else
			Data->database->ReleaseSavepoint(L"rollback");
		#endif // _OMB_USE_CIPHER

		#ifdef _OMB_MONOLITHIC
			MainTimer->Stop();

			#ifndef __UBUNTU_2404__	// FIXME (igor#1#): frame is destroyed when the NDEBUG switch is set. / https://bugs.launchpad.net/bilancio/+bug/2056643
				if(frame == NULL){
			#endif // __UBUNTU_2404__

				frame = new ombMainFrame(wxTheApp->GetTopWindow());
				frame->App = this;
				frame->IsTrayCommand = IsTrayCommand;

				#ifndef NDEBUG
					ombLogMessage(L"Main window created");
				#endif // NDEBUG

				Data->Progress = frame->Progress;

				if(! frame->AutoOpen()){
					delete Data;
					delete m_checker_omb;
					#ifdef _OMB_USEINDICATOR
						KillIndicator(cmd_close);
					#endif // _OMB_USEINDICATOR
					#ifndef NDEBUG
						ombLogMessage(L"Error opening main window!");
					#endif // NDEBUG
					Exit();
					return false;
				}
				#ifndef NDEBUG
					ombLogMessage(L"Auto document opening done");
				#endif // NDEBUG

				frame->UpdateCalendar(wxInvalidDateTime);

				frame->Centre(wxBOTH);
			#ifndef __UBUNTU_2404__	// FIXME (igor#1#): frame is destroyed when the NDEBUG switch is set. / https://bugs.launchpad.net/bilancio/+bug/2056643
				}
			#endif // __UBUNTU_2404__

			// Close database - reopen later to sync with changes from other devices
			#ifdef _OMB_USE_CIPHER
				sqlite3_close(Data->database);
			#else
				Data->database->Close();
			#endif // _OMB_USE_CIPHER
			// Reopen database
			wxString Document = GetDefaultDocument();
			if(Document.IsEmpty()) return 0;
			if(! wxFileExists(Document)) return 0;
			//Data = new TData(NULL);
			if(! Data->OpenDatabase(Document)) return 0;

			frame->ShowF();

		#endif // _OMB_MONOLITHIC
		frame->Show();
		return true;
	}

	bool ombApp::ShowOptions(){
		wxString Document = GetDefaultDocument();
		if(ShowOptionsDialog(
													#ifdef _OMB_USE_CIPHER
														#ifdef _OMB_SQLCIPHER_v4
															frame->GetDatabaseIs_v4()
														#endif // _OMB_SQLCIPHER_v4
													#endif // _OMB_USE_CIPHER
												)){
			if(Document.Cmp(GetDefaultDocument()) != 0){
				Document = GetDefaultDocument();
				if(Document.IsEmpty()){
					return true;
				}
				else
					Data->OpenDatabase((Document));
			}
		}
		return false;
	}

#endif // _OMB_MONOLITHIC

#endif	// OPENMONEYBOX_CPP_INCLUDED
