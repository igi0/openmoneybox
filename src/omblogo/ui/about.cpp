/***************************************************************
 * Name:      about.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-01-11
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef ABOUT_H_INCLUDED
#define ABOUT_H_INCLUDED

//#include <ShellAPI.hpp> // Per ShellExecute

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "about.h"
#include "../ProductVersions.h"

//TAboutBox *AboutBox;


TAboutBox::TAboutBox(wxWindow* parent): wxAboutBox(parent, wxID_ANY, _("OpenMoneyBox"), wxDefaultPosition, wxSize(318,190), wxDEFAULT_DIALOG_STYLE){
	email_lbl->SetLabel(email);
	Copyright_lbl->SetLabel(Copyright);}

void TAboutBox::FormDestroy(void){
	/* ripristinare?
	while(ComponentCount>0)delete Components[ComponentCount-1];
	Release();
	*/
}

#endif // ABOUT_H_INCLUDED

