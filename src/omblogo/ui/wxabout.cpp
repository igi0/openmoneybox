///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxabout.h"

///////////////////////////////////////////////////////////////////////////

wxAboutBox::wxAboutBox( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	this->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	wxFlexGridSizer* fgSizer3;
	fgSizer3 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer3->AddGrowableCol( 0 );
	fgSizer3->AddGrowableRow( 0 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxFlexGridSizer* fgSizer11;
	fgSizer11 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer11->AddGrowableCol( 0 );
	fgSizer11->AddGrowableRow( 0 );
	fgSizer11->SetFlexibleDirection( wxBOTH );
	fgSizer11->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_panel1 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel1->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	m_panel1->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	wxFlexGridSizer* fgSizer6;
	fgSizer6 = new wxFlexGridSizer( 4, 2, 0, 0 );
	fgSizer6->AddGrowableCol( 0 );
	fgSizer6->AddGrowableRow( 4 );
	fgSizer6->SetFlexibleDirection( wxBOTH );
	fgSizer6->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	Product_lbl = new wxStaticText( m_panel1, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	Product_lbl->Wrap( -1 );
	Product_lbl->SetFont( wxFont( 15, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	Product_lbl->SetForegroundColour( wxColour( 255, 0, 0 ) );

	fgSizer6->Add( Product_lbl, 0, wxTOP|wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );


	fgSizer6->Add( 0, 0, 1, wxEXPAND, 5 );

	ShortVersion_lbl = new wxStaticText( m_panel1, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	ShortVersion_lbl->Wrap( -1 );
	ShortVersion_lbl->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	ShortVersion_lbl->SetForegroundColour( wxColour( 255, 255, 255 ) );

	fgSizer6->Add( ShortVersion_lbl, 0, wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );

	LongVersion_lbl = new wxStaticText( m_panel1, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	LongVersion_lbl->Wrap( -1 );
	LongVersion_lbl->SetFont( wxFont( 8, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_LIGHT, false, wxT("Ubuntu") ) );
	LongVersion_lbl->SetBackgroundColour( wxColour( 128, 128, 128 ) );

	fgSizer6->Add( LongVersion_lbl, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );

	Date_lbl = new wxStaticText( m_panel1, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Date_lbl->Wrap( -1 );
	Date_lbl->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	Date_lbl->SetForegroundColour( wxColour( 255, 255, 255 ) );

	fgSizer6->Add( Date_lbl, 0, wxLEFT, 5 );


	fgSizer6->Add( 0, 0, 1, wxEXPAND, 5 );

	email_lbl = new wxHyperlinkCtrl( m_panel1, wxID_ANY, wxEmptyString, wxT("mailto:"), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE );
	email_lbl->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	email_lbl->SetForegroundColour( wxColour( 255, 255, 255 ) );
	email_lbl->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	fgSizer6->Add( email_lbl, 0, wxALL, 5 );


	fgSizer6->Add( 0, 0, 1, wxEXPAND, 5 );


	m_panel1->SetSizer( fgSizer6 );
	m_panel1->Layout();
	fgSizer6->Fit( m_panel1 );
	fgSizer11->Add( m_panel1, 1, wxEXPAND, 5 );

	m_panel2 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panel2->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );

	Copyright_lbl = new wxStaticText( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	Copyright_lbl->Wrap( -1 );
	Copyright_lbl->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Ubuntu") ) );
	Copyright_lbl->SetForegroundColour( wxColour( 176, 176, 176 ) );
	Copyright_lbl->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	bSizer2->Add( Copyright_lbl, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );


	m_panel2->SetSizer( bSizer2 );
	m_panel2->Layout();
	bSizer2->Fit( m_panel2 );
	fgSizer11->Add( m_panel2, 1, wxEXPAND, 5 );


	fgSizer3->Add( fgSizer11, 1, wxEXPAND, 5 );

	OKButton = new wxButton( this, wxID_OK, _("OK"), wxDefaultPosition, wxDefaultSize, 0 );

	OKButton->SetDefault();
	fgSizer3->Add( OKButton, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	this->SetSizer( fgSizer3 );
	this->Layout();
}

wxAboutBox::~wxAboutBox()
{
}
