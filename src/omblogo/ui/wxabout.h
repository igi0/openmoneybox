///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/hyperlink.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxAboutBox
///////////////////////////////////////////////////////////////////////////////
class wxAboutBox : public wxDialog
{
	private:

	protected:
		wxPanel* m_panel1;
		wxPanel* m_panel2;
		wxStaticText* Copyright_lbl;
		wxButton* OKButton;

	public:
		wxStaticText* Product_lbl;
		wxStaticText* ShortVersion_lbl;
		wxStaticText* LongVersion_lbl;
		wxStaticText* Date_lbl;
		wxHyperlinkCtrl* email_lbl;

		wxAboutBox( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 318,190 ), long style = wxDEFAULT_DIALOG_STYLE );

		~wxAboutBox();

};

