/***************************************************************
 * Name:      omblogolib.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef __OMBCONVERT_BIN__
	#include <wx/wx.h>

	// String manipulation functions
	#if (wxCHECK_VERSION(3, 2, 0) )
		wxString iUpperCase(wxString S);
	#else
		WXEXPORT wxString iUpperCase(wxString S);
	#endif

	//	Dialogs functions
	#if _OMB_HASNATIVEABOUT == 0
		WXEXPORT void About(wxString Name,wxString ShortVer,wxString LongVer,wxString Date);
	#endif // _OMB_HASNATIVEABOUT == 0
	#if (wxCHECK_VERSION(3, 2, 0) )
		void DonateDialog(const wxString& olURL);
	#else
		WXEXPORT void DonateDialog(wxString olURL/*,bool EN*/);
	#endif
#endif // __OMBCONVERT_BIN__

