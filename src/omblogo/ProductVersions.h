/***************************************************************
 * Name:      ProductVersions.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-01-09
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef PRODUCTVERSIONS_H_INCLUDED
#define PRODUCTVERSIONS_H_INCLUDED

// PRODUCT INFORMATION
//const wxString ProductName=L"Logo library";
#if _OMB_HASNATIVEABOUT == 0
  const wxString Copyright=L"© Igor Calì";
#endif // _OMB_HASNATIVEABOUT == 0

// CONTACT INFORMATION
#if _OMB_HASNATIVEABOUT == 0
  const wxString email=L"igor.cali0@gmail.com";
#endif // _OMB_HASNATIVEABOUT == 0
//const wxString WebAddress=L"";	// let to specific application

#ifdef __STANDALONE__
	// VERSION INFORMATION
	static int MajorVersion=4;
	static int MinorVersion=0;
	static int ReleaseVersion=0;
	static int BuildVersion=0;
	const wxString LongVersion=::wxString::Format(L"%d.%d.%d.%d",MajorVersion,MinorVersion,ReleaseVersion,BuildVersion);
	const wxString ShortVersion=::wxString::Format(L"%d.%d",MajorVersion,MinorVersion);
	const wxString BuildDate=L"08/09/2013";
#endif	// __STANDALONE__
#endif // PRODUCTVERSIONS_H_INCLUDED
