/***************************************************************
 * Name:      LogoApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License: GNU
 **************************************************************/

#include <wx/wx.h>

#ifndef __OMBCONVERT_BIN__
#ifdef WX_PRECOMP
	#include "wx_pch.h"
	#endif

#include <math.h>	// for pow function

#include <wx/mstream.h>
#include <wx/image.h>
#include <wx/bitmap.h>

#include "../platformsetup.h"
#include "omblogolib.h"
#include "ProductVersions.h"
#if _OMB_HASNATIVEABOUT == 0
#include "ui/wxabout.h"
#include "ui/about.h"
#endif // _OMB_HASNATIVEABOUT

#ifdef __WXMSW__
		#include "../../rsrc/icons/Icon_Application.xpm"
	#endif // __WXMSW__

#endif // __OMBCONVERT_BIN__

wxString iUpperCase(wxString S) {
	wxString C = S.Mid(0, 1);
	C.MakeUpper();
	S = S.Mid(1, S.Len() - 1);
	S = C + S;
	return S;
}

#ifndef __OMBCONVERT_BIN__
#if _OMB_HASNATIVEABOUT == 0
void About(wxString Name, wxString ShortVer, wxString LongVer, wxString Date) {
	TAboutBox *AboutBox = new TAboutBox(wxTheApp->GetTopWindow());

#ifdef __WXMSW__
			AboutBox->SetIcon(wxIcon(Logo_xpm));
		#endif // __WXMSW__

	AboutBox->Product_lbl->SetLabel(Name);
	AboutBox->ShortVersion_lbl->SetLabel(ShortVer);
	AboutBox->LongVersion_lbl->SetLabel(LongVer);
	AboutBox->Date_lbl->SetLabel(Date);
	AboutBox->email_lbl->SetURL(
			AboutBox->email_lbl->GetURL() + email + L"?subject=" + Name + L"_v"
					+ LongVer);
	AboutBox->ShowModal();
	delete AboutBox;
}
#endif // _OMB_HASNATIVEABOUT == 0

	#ifdef __OPENSUSE__
		#if (wxCHECK_VERSION(3, 1, 5) )
			void DonateDialog(const wxString& olURL) {
		#else
			void DonateDialog(wxString olURL) {
		#endif
	#else
		#if (wxCHECK_VERSION(3, 2, 0) )
			void DonateDialog(const wxString& olURL) {
		#else
			void DonateDialog(wxString olURL) {
		#endif
	#endif // __OPENSUSE__
	wxLaunchDefaultBrowser(olURL);
}

#endif // __OMBCONVERT_BIN__

