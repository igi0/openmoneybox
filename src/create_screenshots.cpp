/***************************************************************
 * Name:      create_screenshots.cpp
 * Purpose:   OpenMoneyBox screenshot generator
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-10-12
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:   GNU
 **************************************************************/

#ifndef SCREENSHOTS_CPP_INCLUDED
#define SCREENSHOTS_CPP_INCLUDED

#include <wx/wx.h>
#include <wx/bitmap.h>
#include <wx/filefn.h>
#include <wx/intl.h> // For i18n

#include "openmoneybox/ui/funds.h"
#include "openmoneybox/ui/objct.h"
#include "openmoneybox/ui/getbackobj.h"
#include "openmoneybox/ui/setcreddeb.h"
#include "openmoneybox/ui/remcreddeb.h"
#include "openmoneybox/ui/gainexpense.h"
#include "ombopt/ui/opzio.h"

class app : public wxApp{
	private:
		void CreateScreenshots(wxString locale_string);
		void SaveScreenshot (wxWindow *window, wxString locale_string, wxString file);
	public:
    virtual bool OnInit();
};

// Code start

#include "languages.cpp"

// the arrays must be in sync
wxCOMPILE_TIME_ASSERT( WXSIZEOF(langNames)==WXSIZEOF(langIds),LangArraysMismatch);

wxLanguage Lan;
wxLocale omb_locale;

IMPLEMENT_APP(app)

bool app::OnInit(){
  #ifdef __WXMSW__
    wxImage::AddHandler(new wxPNGHandler);
  #endif // __WXMSW__

	CreateScreenshots("en");

  wxLocale::AddCatalogLookupPathPrefix(L"/user/share/locale"/*/it/LC_MESSAGES"*/);
  wxLocale::AddCatalogLookupPathPrefix(wxGetCwd() + L"/.");
  //wxLocale::AddCatalogLookupPathPrefix(wxGetCwd() + L"/Debug_GTK");
  wxLocale::AddCatalogLookupPathPrefix(wxGetCwd() + L"/i18n/it");
  #ifdef __WXMSW__
    wxLocale::AddCatalogLookupPathPrefix(wxGetCwd()+L"\\");
  #endif // __WXMSW__
	omb_locale.Init(wxLANGUAGE_ITALIAN,wxLOCALE_LOAD_DEFAULT);
	omb_locale.AddCatalog(L"openmoneybox");
	//Lan=FindLang();

	CreateScreenshots("it");

	return true;
}

void app::CreateScreenshots(wxString locale_string){
	wxString dir = wxGetCwd() + "/help/" + locale_string + "/images_";
	#ifdef __WXGTK__
		dir += "gtk/";
	#elif defined (__WXMSW__)
		dir += "msw/";
	#endif
	if(!wxDirExists(wxGetCwd() + "/help")) wxMkdir(wxGetCwd() + "/help",0777);
	if(!wxDirExists(wxGetCwd() + "/help/" + locale_string)) wxMkdir(wxGetCwd() + "/help/" + locale_string,0777);
	#ifdef __WXGTK__
		if(!wxDirExists(wxGetCwd() + "/help/" + locale_string + "/images_gtk")) wxMkdir(wxGetCwd() + "/help/" + locale_string + "/images_gtk",0777);
	#elif defined (__WXMSW__)
		if(!wxDirExists(wxGetCwd() + "/help/" + locale_string + "/images_gtk")) wxMkdir(wxGetCwd() + "/help/" + locale_string + "/images_msw",0777);
	#endif
	wxString file;
	int i;

	// Fund screenshots
	TOPF *window[5];
	for(i=1; i <= 5; i++){
		file = dir;
		switch(i){
			case 1:
				file += "fund_total.png";
				break;
			case 2:
				file += "fund_new.png";
				break;
			case 3:
				file += "fund_remove.png";
				break;
			case 4:
				file += "fund_reset.png";
				break;
			case 5:
				file += "fund_default.png";}
		window[i-1] = new TOPF(wxTheApp->GetTopWindow());
		window[i-1]->InitLabels(i);

		SaveScreenshot(window[i-1], locale_string, file);
	}

	// Object screenshots
	TObjects *window1[8];
	for(i=1; i <= 8; i++){
		file = dir;
		switch(i){
			case 1:
				file += "object_receive.png";
				break;
			case 2:
				file += "object_give.png";
				break;
			case 3:
				file += "object_lend.png";
				break;
			case 4:
				file += "object_borrow.png";
				break;
			case 7:
				file += "shop_add.png";
				break;
			case 8:
				file += "shop_remove.png";
				break;
			default:
				file += "junk.png";}
		window1[i-1] = new TObjects(wxTheApp->GetTopWindow());
		if(i >= 6)window1[i-1]->InitLabels(6);
		window1[i-1]->InitLabels(i);

		if((i <= 5) || (i >= 7))SaveScreenshot(window1[i-1], locale_string, file);
	}

	// Object screenshots
	TGetBackF *window2[2];
	for(i=1; i <= 2; i++){
		file = dir;
		switch(i){
			case 1:
				file += "object_getback.png";
				break;
			case 2:
				file += "object_giveback.png";
				break;
			default:
				file += "junk.png";}
		window2[i-1] = new TGetBackF(wxTheApp->GetTopWindow());
		window2[i-1]->InitLabels(i);

		SaveScreenshot(window2[i-1], locale_string, file);
	}

	// Credit/Debit screenshots
	TSCredDeb *window3[2];
	for(i=1; i <= 2; i++){
		file = dir;
		switch(i){
			case 1:
				file += "credit_set.png";
				break;
			case 2:
				file += "debt_set.png";
				break;
			default:
				file += "junk.png";}
		window3[i-1] = new TSCredDeb(wxTheApp->GetTopWindow());
		window3[i-1]->InitLabels(i-1);

		SaveScreenshot(window3[i-1], locale_string, file);
	}

	// Credit/Debit screenshots
	TRCredDeb *window4[4];
	for(i=1; i <= 4; i++){
		file = dir;
		switch(i){
			case 1:
				file += "credit_remove.png";
				break;
			case 2:
				file += "debt_remove.png";
				break;
			case 3:
				file += "credit_remit.png";
				break;
			case 4:
				file += "debt_remit.png";
				break;
			case 7:
				file += "shop_add.png";
				break;
			case 8:
				file += "shop_remove.png";
				break;
			default:
				file += "junk.png";}
		window4[i-1] = new TRCredDeb(wxTheApp->GetTopWindow());
		window4[i-1]->InitLabels(i);

		if((i <= 5) || (i >= 7))SaveScreenshot(window4[i-1], locale_string, file);
	}

	// Gain/Expense screenshots
	TOperationF *window5[2];
	for(i=1; i <= 2; i++){
		file = dir;
		switch(i){
			case 1:
				file += "profit.png";
				break;
			case 2:
				file += "expense.png";
				break;
			default:
				file += "junk.png";}
		window5[i-1] = new TOperationF(wxTheApp->GetTopWindow());
		window5[i-1]->InitLabels(i-1);

		SaveScreenshot(window5[i-1], locale_string, file);
	}

	// Options screenshots
	TOptionsF *window6;
	file = dir;
	file += "options.png";
	window6 = new TOptionsF(wxTheApp->GetTopWindow());
	//window5[i-1]->InitLabels(i-1);

	SaveScreenshot(window6, locale_string, file);

	for(i = 1; i <= 5; i++){
		window[i-1]->Show(false);
		window[i-1]->Destroy();}

	for(i = 1; i <= 8; i++){
		window1[i-1]->Show(false);
		window1[i-1]->Destroy();}

	for(i = 1; i <= 2; i++){
		window2[i-1]->Show(false);
		window2[i-1]->Destroy();}

	for(i = 1; i <= 2; i++){
		window3[i-1]->Show(false);
		window3[i-1]->Destroy();}

	for(i = 1; i <= 4; i++){
		window4[i-1]->Show(false);
		window4[i-1]->Destroy();}

	for(i = 1; i <= 2; i++){
		window5[i-1]->Show(false);
		window5[i-1]->Destroy();}

	window6->Show(false);
	window6->Destroy();
}

void app::SaveScreenshot (wxWindow *window, wxString locale_string, wxString file){
	wxSize sz;
	wxMemoryDC bmpDC;

		window->Show(true);
		#ifdef __WXMSW__
      window->Raise();
    #endif // __WXMSW__
		window->Update();
    wxSleep(1);

		#ifndef __WXMSW__
      wxWindowDC dc(window);
      sz = window->GetSize();
    #else
      wxClientDC dc(window);
      sz = window->GetClientSize();
    #endif // __WXMSW__

		wxBitmap scrshoot(sz);
		bmpDC.SelectObject(scrshoot);
		bmpDC.Blit(0, 0, sz.GetWidth(), sz.GetHeight(), &dc, 0, 0);
		scrshoot.SaveFile(file, wxBITMAP_TYPE_PNG, NULL);

		//window->Show(false);
		//delete window;
		//window->Destroy();
}

#endif // SCREENSHOTS_CPP_INCLUDED
