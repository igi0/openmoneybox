/***************************************************************
 * Name:      wxsqlite3.cpp
 * Purpose:   sqlite3 wrapper
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-10-17
 * Source derived from https://github.com/utelle/wxsqlite3/blob/master/src/wxsqlite3.cpp
 * License:		GNU
 **************************************************************/

#ifndef fSQLITE3_CPP
#define fSQLITE3_CPP


#include <sqlite3.h>
#include "wxsqlite3.h"


static double wxSQLite3AtoF(const char *z)
{
  int sign = 1;
  long double v1 = 0.0;
  int nSignificant = 0;
  while (isspace(*(unsigned char*)z))
  {
    ++z;
  }
  if (*z == '-')
  {
    sign = -1;
    ++z;
  }
  else if (*z == '+')
  {
    ++z;
  }
  while (*z == '0')
  {
    ++z;
  }
  while (isdigit(*(unsigned char*)z))
  {
    v1 = v1*10.0 + (*z - '0');
    ++z;
    ++nSignificant;
  }
  if (*z == '.')
  {
    long double divisor = 1.0;
    ++z;
    if (nSignificant == 0)
    {
      while (*z == '0')
      {
        divisor *= 10.0;
        ++z;
      }
    }
    while (isdigit(*(unsigned char*)z))
    {
      if (nSignificant < 18)
      {
        v1 = v1*10.0 + (*z - '0');
        divisor *= 10.0;
        ++nSignificant;
      }
      ++z;
    }
    v1 /= divisor;
  }
  if (*z=='e' || *z=='E')
  {
    int esign = 1;
    int eval = 0;
    long double scale = 1.0;
    ++z;
    if (*z == '-')
    {
      esign = -1;
      ++z;
    }
    else if (*z == '+')
    {
      ++z;
    }
    while (isdigit(*(unsigned char*)z))
    {
      eval = eval*10 + *z - '0';
      ++z;
    }
    while (eval >= 64) { scale *= 1.0e+64; eval -= 64; }
    while (eval >= 16) { scale *= 1.0e+16; eval -= 16; }
    while (eval >=  4) { scale *= 1.0e+4;  eval -= 4; }
    while (eval >=  1) { scale *= 1.0e+1;  eval -= 1; }
    if (esign < 0)
    {
      v1 /= scale;
    }
    else
    {
      v1 *= scale;
    }
  }
  return (double) ((sign < 0) ? -v1 : v1);
}

wxSQLite3Table::wxSQLite3Table()
{
  m_results = 0;
  m_rows = 0;
  m_cols = 0;
  m_currentRow = 0;
}

wxSQLite3Table::wxSQLite3Table(const wxSQLite3Table& table)
{
  m_results = table.m_results;
  // Only one object can own the results
  const_cast<wxSQLite3Table&>(table).m_results = 0;
  m_rows = table.m_rows;
  m_cols = table.m_cols;
  m_currentRow = table.m_currentRow;
}

wxSQLite3Table::wxSQLite3Table(char** results, int rows, int cols)
{
  m_results = results;
  m_rows = rows;
  m_cols = cols;
  m_currentRow = 0;
}

wxSQLite3Table::~wxSQLite3Table()
{
  try
  {
    Finalize();
  }
  catch (...)
  {
  }
}

void wxSQLite3Table::Finalize()
{
  if (m_results)
  {
    sqlite3_free_table(m_results);
    m_results = 0;
  }
}

wxSQLite3Table& wxSQLite3Table::operator=(const wxSQLite3Table& table)
{
  if (this != &table)
  {
    try
    {
      Finalize();
    }
    catch (...)
    {
    }
    m_results = table.m_results;
    // Only one object can own the results
    const_cast<wxSQLite3Table&>(table).m_results = 0;
    m_rows = table.m_rows;
    m_cols = table.m_cols;
    m_currentRow = table.m_currentRow;
  }
  return *this;
}

int wxSQLite3Table::GetRowCount() const
{
  //CheckResults();
  return m_rows;
}

void wxSQLite3Table::SetRow(int row)
{
  /*
  CheckResults();
  if (row < 0 || row > m_rows-1)
  {
    throw wxSQLite3Exception(WXSQLITE_ERROR, wxERRMSG_INVALID_ROW);
  }
	*/

  m_currentRow = row;
}

int wxSQLite3Table::GetInt(const wxString& columnName, int nullValue ) const
{
  if (IsNull(columnName))
  {
    return nullValue;
  }
  else
  {
    long value = nullValue;
    GetAsString(columnName).ToLong(&value);
    return (int) value;
  }
}

int wxSQLite3Table::GetInt(int columnIndex, int nullValue ) const
{
  if (IsNull(columnIndex))
  {
    return nullValue;
  }
  else
  {
    long value = nullValue;
    GetAsString(columnIndex).ToLong(&value);
    return (int) value;
  }
}

bool wxSQLite3Table::IsNull(int columnIndex) const
{
  /*
  CheckResults();
  if (columnIndex < 0 || columnIndex > m_cols-1)
  {
    throw wxSQLite3Exception(WXSQLITE_ERROR, wxERRMSG_INVALID_INDEX);
  }
	*/

  int index = (m_currentRow*m_cols) + m_cols + columnIndex;
  const char* localValue = m_results[index];
  return (localValue == 0);
}

bool wxSQLite3Table::IsNull(const wxString& columnName) const
{
  int index = FindColumnIndex(columnName);
  return IsNull(index);
}

wxString wxSQLite3Table::GetAsString(int columnIndex) const
{
  /*
  if (columnIndex < 0 || columnIndex > m_cols-1)
  {
    throw wxSQLite3Exception(WXSQLITE_ERROR, wxERRMSG_INVALID_INDEX);
  }
	*/

  int nIndex = (m_currentRow*m_cols) + m_cols + columnIndex;
  const char* localValue = m_results[nIndex];
  return wxString::FromUTF8(localValue);
}

wxString wxSQLite3Table::GetAsString(const wxString& columnName) const
{
  int index = FindColumnIndex(columnName);
  return GetAsString(index);
}

int wxSQLite3Table::FindColumnIndex(const wxString& columnName) const
{
  //CheckResults();

  wxCharBuffer strColumnName = columnName.ToUTF8();
  const char* localColumnName = strColumnName;

  if (columnName.Len() > 0)
  {
    for (int columnIndex = 0; columnIndex < m_cols; columnIndex++)
    {
      if (strcmp(localColumnName, m_results[columnIndex]) == 0)
      {
        return columnIndex;
      }
    }
  }

  //throw wxSQLite3Exception(WXSQLITE_ERROR, wxERRMSG_INVALID_NAME);
  return -1;
}

double wxSQLite3Table::GetDouble(int columnIndex, double nullValue ) const
{
  if (IsNull(columnIndex))
  {
    return nullValue;
  }
  else
  {
  	/*
    if (columnIndex < 0 || columnIndex > m_cols-1)
    {
      throw wxSQLite3Exception(WXSQLITE_ERROR, wxERRMSG_INVALID_INDEX);
    }
    */

    int nIndex = (m_currentRow*m_cols) + m_cols + columnIndex;
    return wxSQLite3AtoF(m_results[nIndex]);
  }
}

double wxSQLite3Table::GetDouble(const wxString& columnName, double nullValue ) const
{
  int index = FindColumnIndex(columnName);
  return GetDouble(index, nullValue);
}

wxString wxSQLite3Table::GetString(int columnIndex, const wxString& nullValue ) const
{
  if (IsNull(columnIndex))
  {
    return nullValue;
  }
  else
  {
    return GetAsString(columnIndex);
  }
}

wxString wxSQLite3Table::GetString(const wxString& columnName, const wxString& nullValue ) const
{
  if (IsNull(columnName))
  {
    return nullValue;
  }
  else
  {
    return GetAsString(columnName);
  }
}

//___________________________________________

wxSQLite3Table GetTable(sqlite3 *m_db, const char* sql)
{
  //CheckDatabase();

  char* localError=0;
  char** results=0;
  int rc;
  int rows(0);
  int cols(0);

  wxSQLite3Table Table;

  rc = sqlite3_get_table(m_db, sql, &results, &rows, &cols, &localError);

  if (rc == SQLITE_OK) Table =  wxSQLite3Table(results, rows, cols);
  /*
  else
  {
    throw wxSQLite3Exception(rc, wxString::FromUTF8(localError));
  }
  */

	return Table;
}

bool TableExists(const wxString& tableName, const wxString& databaseName, sqlite3 *m_db)
{
  wxString sql;
  if (databaseName.IsEmpty())
  {
    //sql = wxS("select count(*) from sqlite_master where type='table' and name like ?");
    sql = wxString(wxS("select count(*) from sqlite_master where type='table' and name='") + tableName + L"';");
  }
  else
  {
    //sql = wxString(wxS("select count(*) from ")) + databaseName + wxString(wxS(".sqlite_master where type='table' and name like ?"));
    sql = wxString(wxS("select count(*) from ")) + databaseName + wxString(wxS(".sqlite_master where type='table' and name='") + tableName + L"';");
  }

  /*
  wxSQLite3Statement stmt = PrepareStatement(sql);
  stmt.Bind(1, tableName);
  wxSQLite3ResultSet resultSet = stmt.ExecuteQuery();
  long value = 0;
  resultSet.GetAsString(0).ToLong(&value);
  return (value > 0);
  */

  int res = -1;
  sqlite3_stmt *stmt;
  #ifdef __OPENSUSE__
    if(sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, NULL) == SQLITE_OK){
  #else
    if(sqlite3_prepare_v2(m_db, sql, -1, &stmt, NULL) == SQLITE_OK){
  #endif // __OPENSUSE
  	while(sqlite3_step(stmt) == SQLITE_ROW){
  		res = sqlite3_column_int(stmt, 0);
		}
  }

	return (res > 0);
}

int ExecuteUpdate(sqlite3 *m_db, const char* sql/*, bool saveRC*/)
{
  //CheckDatabase();

  char* localError = 0;

  int rc = sqlite3_exec(m_db, sql, 0, 0, &localError);
  /*
  if (saveRC)
  {
    if (strncmp(sql, "rollback transaction", 20) == 0)
    {
      m_lastRollbackRC = rc;
    }
  }
	*/

  if (rc == SQLITE_OK)
  {
    return sqlite3_changes(m_db);
  }
  else
  {
    //wxString errmsg = wxString::FromUTF8(localError);
    sqlite3_free(localError);
    //throw wxSQLite3Exception(rc, errmsg);
    return -1;
  }
}


int queryUserVersion (sqlite3* db) {
	// https://stackoverflow.com/questions/2659797/how-do-i-use-sqlite3-pragma-user-version-in-objective-c
	// get current database version of schema
	static sqlite3_stmt *stmt_version;
	int databaseVersion = -1;

	if(sqlite3_prepare_v2(db, "PRAGMA user_version;", -1, &stmt_version, NULL) == SQLITE_OK) {
		while(sqlite3_step(stmt_version) == SQLITE_ROW) {
				databaseVersion = sqlite3_column_int(stmt_version, 0);
				//NSLog(@"%s: version %d", __FUNCTION__, databaseVersion);
		}
		//NSLog(@"%s: the databaseVersion is: %d", __FUNCTION__, databaseVersion);
	} else {
			//NSLog(@"%s: ERROR Preparing: , %s", __FUNCTION__, sqlite3_errmsg(db) );
	}
	sqlite3_finalize(stmt_version);

	return databaseVersion;
}

#endif // fSQLITE3_CPP
