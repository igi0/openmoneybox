/***************************************************************
 * Name:      ombmapviewer.c
 * Purpose:   Code for OpenMoneyBox Map Widget
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-04-06
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include <libintl.h>
#include <locale.h>

#include "osm-gps-map.h"

static OsmGpsMapSource_t opt_map_provider = OSM_GPS_MAP_SOURCE_OPENSTREETMAP;
static gboolean opt_friendly_cache = FALSE;
static gboolean opt_no_cache = FALSE;
static char *opt_cache_base_dir = NULL;

static GdkPixbuf *g_star_image = NULL;

static void
on_close (GtkWidget *widget, gpointer user_data)
{
    gtk_widget_destroy(widget);
    gtk_main_quit();
}

int
main (int argc, char **argv)
{
	setlocale(LC_ALL, "");    // decided by environment
	bindtextdomain("openmoneybox", "/usr/share/locale");
	textdomain("openmoneybox");

    GtkBuilder *builder;
    GtkWidget *widget;
    GtkAccelGroup *ag;
    OsmGpsMap *map;
    OsmGpsMapLayer *osd;
    //OsmGpsMapTrack *rightclicktrack;
    const char *repo_uri;
    char *cachedir, *cachebasedir;
    GError *error = NULL;
    //GOptionContext *context;

    gtk_init (&argc, &argv);

/*
    context = g_option_context_new ("- Map browser");
    g_option_context_set_help_enabled(context, FALSE);
    g_option_context_add_main_entries (context, entries, NULL);

    if (!g_option_context_parse (context, &argc, &argv, &error)) {
        usage(context);
        return 1;
    }
*/

    /* Only use the repo_uri to check if the user has supplied a
    valid map source ID */
    repo_uri = osm_gps_map_source_get_repo_uri(opt_map_provider);
    if ( repo_uri == NULL ) {
        //usage(context);
        return 2;
    }

    cachebasedir = osm_gps_map_get_default_cache_directory();

    if (opt_cache_base_dir && g_file_test(opt_cache_base_dir, G_FILE_TEST_IS_DIR)) {
        cachedir = g_strdup(OSM_GPS_MAP_CACHE_AUTO);
        cachebasedir = g_strdup(opt_cache_base_dir);
    } else if (opt_friendly_cache) {
        cachedir = g_strdup(OSM_GPS_MAP_CACHE_FRIENDLY);
    } else if (opt_no_cache) {
        cachedir = g_strdup(OSM_GPS_MAP_CACHE_DISABLED);
    } else {
        cachedir = g_strdup(OSM_GPS_MAP_CACHE_AUTO);
    }

/*
    if (opt_debug)
        gdk_window_set_debug_updates(TRUE);
*/

    g_debug("Map Cache Dir: %s", cachedir);
    g_debug("Map Provider: %s (%d)", osm_gps_map_source_get_friendly_name(opt_map_provider), opt_map_provider);

    map = (OsmGpsMap*) g_object_new (OSM_TYPE_GPS_MAP,
                        "map-source",opt_map_provider,
                        "tile-cache",cachedir,
                        "tile-cache-base", cachebasedir,
                        "proxy-uri",g_getenv("http_proxy"),
                        NULL);

    osd = (OsmGpsMapLayer*) g_object_new (OSM_TYPE_GPS_MAP_OSD,
                        "show-scale",TRUE,
                        "show-coordinates",FALSE,
                        "show-crosshair",FALSE,
                        "show-dpad",TRUE,
                        "show-zoom",TRUE,
                        "show-gps-in-dpad",FALSE,
                        "show-gps-in-zoom",FALSE,
                        "dpad-radius", 30,
                        NULL);
    osm_gps_map_layer_add(OSM_GPS_MAP(map), osd);
    g_object_unref(G_OBJECT(osd));

/*
    //Add a second track for right clicks
    rightclicktrack = osm_gps_map_track_new();

    if(opt_editable_tracks)
        g_object_set(rightclicktrack, "editable", TRUE, NULL);
    osm_gps_map_track_add(OSM_GPS_MAP(map), rightclicktrack);
*/

    g_free(cachedir);
    g_free(cachebasedir);

    //Enable keyboard navigation
    osm_gps_map_set_keyboard_shortcut(map, OSM_GPS_MAP_KEY_FULLSCREEN, GDK_KEY_F11);
    osm_gps_map_set_keyboard_shortcut(map, OSM_GPS_MAP_KEY_UP, GDK_KEY_Up);
    osm_gps_map_set_keyboard_shortcut(map, OSM_GPS_MAP_KEY_DOWN, GDK_KEY_Down);
    osm_gps_map_set_keyboard_shortcut(map, OSM_GPS_MAP_KEY_LEFT, GDK_KEY_Left);
    osm_gps_map_set_keyboard_shortcut(map, OSM_GPS_MAP_KEY_RIGHT, GDK_KEY_Right);

	//Build the UI
	#ifndef __FREEBSD__
		g_star_image = gdk_pixbuf_new_from_file_at_size ("/usr/share/icons/HighContrast/24x24/actions/mark-location.png", 24,24,NULL);
	#else
		g_star_image = gdk_pixbuf_new_from_file_at_size ("/usr/local/share/icons/HighContrast/24x24/actions/mark-location.png", 24,24,NULL);
	#endif

    builder = gtk_builder_new();

    gtk_builder_add_from_file (builder, "/etc/openmoneybox/ombmapviewer.ui", &error);
    if (error)
        g_error ("ERROR: %s\n", error->message);

    gtk_box_pack_start (
                GTK_BOX(gtk_builder_get_object(builder, "map_box")),
                GTK_WIDGET(map), TRUE, TRUE, 0);

    //Init values
    float lw,a;
    GdkRGBA c;
    OsmGpsMapTrack *gpstrack = osm_gps_map_gps_get_track (map);
    g_object_get (gpstrack, "line-width", &lw, "alpha", &a, NULL);
    osm_gps_map_track_get_color(gpstrack, &c);
    gtk_adjustment_set_value (
                GTK_ADJUSTMENT(gtk_builder_get_object(builder, "gps_width_adjustment")),
                lw);
    gtk_adjustment_set_value (
                GTK_ADJUSTMENT(gtk_builder_get_object(builder, "gps_alpha_adjustment")),
                a);
    gtk_adjustment_set_value (
                GTK_ADJUSTMENT(gtk_builder_get_object(builder, "star_xalign_adjustment")),
                0.5);
    gtk_adjustment_set_value (
                GTK_ADJUSTMENT(gtk_builder_get_object(builder, "star_yalign_adjustment")),
                0.5);
#if GTK_CHECK_VERSION(3,4,0)
    gtk_color_chooser_set_rgba (
                GTK_COLOR_CHOOSER(gtk_builder_get_object(builder, "gps_colorbutton")),
                &c);
#else
    gtk_color_button_set_rgba (
                GTK_COLOR_BUTTON(gtk_builder_get_object(builder, "gps_colorbutton")),
                &c);
#endif

    //Connect to signals
/*
    g_signal_connect (
                gtk_builder_get_object(builder, "gps_colorbutton"), "color-set",
                G_CALLBACK (on_gps_color_changed), (gpointer) gpstrack);
    g_signal_connect (
                gtk_builder_get_object(builder, "zoom_in_button"), "clicked",
                G_CALLBACK (on_zoom_in_clicked_event), (gpointer) map);
    g_signal_connect (
                gtk_builder_get_object(builder, "zoom_out_button"), "clicked",
                G_CALLBACK (on_zoom_out_clicked_event), (gpointer) map);
    g_signal_connect (
                gtk_builder_get_object(builder, "home_button"), "clicked",
                G_CALLBACK (on_home_clicked_event), (gpointer) map);
    g_signal_connect (
                gtk_builder_get_object(builder, "cache_button"), "clicked",
                G_CALLBACK (on_cache_clicked_event), (gpointer) map);
    g_signal_connect (
                gtk_builder_get_object(builder, "gps_alpha_adjustment"), "value-changed",
                G_CALLBACK (on_gps_alpha_changed), (gpointer) map);
    g_signal_connect (
                gtk_builder_get_object(builder, "gps_width_adjustment"), "value-changed",
                G_CALLBACK (on_gps_width_changed), (gpointer) map);
    g_signal_connect (
                gtk_builder_get_object(builder, "star_xalign_adjustment"), "value-changed",
                G_CALLBACK (on_star_align_changed), (gpointer) "x-align");
    g_signal_connect (
                gtk_builder_get_object(builder, "star_yalign_adjustment"), "value-changed",
                G_CALLBACK (on_star_align_changed), (gpointer) "y-align");
    g_signal_connect (G_OBJECT (map), "button-press-event",
                G_CALLBACK (on_button_press_event), (gpointer) rightclicktrack);
    g_signal_connect (G_OBJECT (map), "button-release-event",
                G_CALLBACK (on_button_release_event),
                (gpointer) gtk_builder_get_object(builder, "text_entry"));
    g_signal_connect (G_OBJECT (map), "notify::tiles-queued",
                G_CALLBACK (on_tiles_queued_changed),
                (gpointer) gtk_builder_get_object(builder, "cache_label"));
*/

    widget = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));
    g_signal_connect (widget, "destroy",
                      G_CALLBACK (on_close), (gpointer) map);
    //Setup accelerators.
    ag = gtk_accel_group_new();
    gtk_accel_group_connect(ag, GDK_KEY_w, GDK_CONTROL_MASK, GTK_ACCEL_MASK,
                    g_cclosure_new(gtk_main_quit, NULL, NULL));
    gtk_accel_group_connect(ag, GDK_KEY_q, GDK_CONTROL_MASK, GTK_ACCEL_MASK,
                    g_cclosure_new(gtk_main_quit, NULL, NULL));
    gtk_window_add_accel_group(GTK_WINDOW(widget), ag);

	gtk_window_set_title(GTK_WINDOW(widget), _("OpenMoneyBox map viewer"));

    gtk_widget_show_all (widget);

    g_log_set_handler ("OsmGpsMap", G_LOG_LEVEL_MASK, g_log_default_handler, NULL);

	struct point
	{
		float lat;
		float lon;
	};

        float minLat = FLT_MAX;
        float maxLat = FLT_MIN;
        float minLong = FLT_MAX;
        float maxLong = FLT_MIN;

	FILE *fp = fopen("/tmp/ombmapview.dat", "r"); // flawfinder: ignore
	if(fp != NULL){
		struct point pts;
		while(fread(&pts, sizeof(pts), 1, fp) > 0)
		{
			osm_gps_map_image_add (map,
                                                  pts.lat,
                                                  pts.lon,
                                                  g_star_image);

			if (pts.lat < minLat)
				minLat = pts.lat;
			if (pts.lat > maxLat)
				maxLat = pts.lat;
			if (pts.lon < minLong)
				minLong = pts.lon;
			if (pts.lon > maxLong)
				maxLong = pts.lon;

		}

		osm_gps_map_set_center_and_zoom(map, minLat +((maxLat - minLat) / 2), minLong + ((maxLong - minLong) / 2), 11);

		gtk_window_maximize (GTK_WINDOW(widget));

		gboolean Retry;
		int Loops = 1;
		float minShownLat, maxShownLat, minShownLong, maxShownLong;
		OsmGpsMapPoint *TopLeft = osm_gps_map_point_new_degrees(0.0, 0.0),
			*BottomRight= osm_gps_map_point_new_degrees(0.0, 0.0);
		do{
			Retry = FALSE;
			osm_gps_map_get_bbox(map, TopLeft, BottomRight);
			osm_gps_map_point_get_degrees(TopLeft, &minShownLat, &maxShownLong);
			osm_gps_map_point_get_degrees(BottomRight, &maxShownLat, &minShownLong);
			if((minLat < minShownLat) || (maxLat > maxShownLat) || (minLong < minShownLong) || (maxLong > maxShownLong)){
				osm_gps_map_zoom_out (map);
				Retry = TRUE;
				Loops--;
			}
		} while((Retry == TRUE) && (Loops > 0));
	}

	gtk_main ();
	return 0;
}

