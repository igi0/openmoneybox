/***************************************************************
 * Name:      ombupdate.cpp
 * Purpose:   Code for OpenMoneyBox update
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-05-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMBUPDATE_CPP_INCLUDED
#define OMBUPDATE_CPP_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/fileconf.h> // For wxFileConfig

#ifdef _OMB_USE_GSETTINGS
	#include <gio/gio.h>	// For GSettings
#endif // _OMB_USE_GSETTINGS

#ifdef _OMB_USEREGISTRY
  #include <wx/msw/registry.h>
#endif // _OMB_USEREGISTRY

#include "ombupdate.h"
#include "../types.h"

#ifndef __WXMSW__
	#ifndef _OMB_MONOLITHIC
		#include "../productversion.h"
	#endif // _OMB_MONOLITHIC
#endif // __WXMSW__

// Code start

#ifdef __WXGTK__
	#ifdef _OMB_USE_GSETTINGS
		extern GSettings *settings_advanced;
	#else
  	extern wxFileConfig *INI;
	#endif // _OMB_USE_GSETTINGS
#elif defined __WXMSW__
  typedef HRESULT (WINAPI *UDTF)(LPVOID, LPCTSTR, LPCTSTR, DWORD, LPVOID);

	#ifdef _OMB_PORTABLE_MSW
		extern
	#endif // _OMB_MONOLITHIC
  wxFileConfig *INI;
#elif defined __WXMAC__
	#ifdef _OMB_MONOLITHIC
		extern
	#endif // _OMB_MONOLITHIC
  		wxFileConfig *INI;
#endif // __WXGTK__

extern wxString WebAddress;

typedef enum TTupdType{
  utOriginal = 1,

  utBugFix = 10,
  utSecurity,
  utImprovement,
  utRebuild,

  utTranslationNew = 20,
  utTranslationUpdate,

  utSiteChanged = 30,
  utHelpImprovement,

  utInstallerBugFix = 40,
  utInstallerSecurity,
  utInstallImprovement,
  utInstallerRebuild,

  utWxUpdate =50
}TupdType;

void WXEXPORT fnCheckUpdate(void){
	wxString regstr;
	wxDateTime lcheck;
	#ifdef _OMB_USEREGISTRY
		wxString SWKey = GetSWKey();
		wxString Key=_T("HKEY_LOCAL_MACHINE\\")+SWKey;
		wxRegKey *K=new wxRegKey(wxRegKey::HKLM,SWKey,wxRegKey::WOW64ViewMode_Default);
		if(K->HasValue("LastUpdateCheck")){
			K->QueryValue("LastUpdateCheck",regstr);
			lcheck = Omb_StrToDate(regstr);
			if( !lcheck.IsEarlierThan( wxDateTime::Today() )){
				delete K;
				//ReturnValue=1;
				return;}}
	#else
		#ifdef _OMB_USE_GSETTINGS
			regstr = g_settings_get_string (settings_advanced, "lastupdatecheck");
				lcheck = Omb_StrToDate(regstr);
				if(lcheck >= ::wxDateTime::Today().ResetTime()) return;
		#else
			INI->SetPath(L"/Advanced");
			if(INI->HasEntry(L"LastUpdateCheck")){
				regstr=INI->Read(L"LastUpdateCheck",wxEmptyString);
				lcheck = Omb_StrToDate(regstr);
				if(lcheck >= ::wxDateTime::Today().ResetTime()) return;}
		#endif // _OMB_USE_GSETTINGS
  #endif // _OMB_USEREGISTRY

	// Version update check
	#ifdef __WXMSW__
    wxString AppPath=GetInstallationPath()+"\\serverinfo.ini";
	#else
    wxString AppPath = GetUserConfigDir() + L"/serverinfo.ini";
  #endif  // __WXMSW__

  // NOTE (igor#1#): Using wget since wxUrl and wxHTTP does not download properly
  #ifdef __WXMSW__
    wxString Proc = "\"" + GetInstallationPath() + L"\\wget.exe\" -O \"" + AppPath + L"\" " + GetVersionFile();
    wxExecute(Proc, wxEXEC_SYNC);
  #else
    wxString Proc = L"wget -O " + AppPath + L" " + GetVersionFile();
    wxExecute(Proc,wxEXEC_SYNC);
  #endif // __WXMSW__

	TupdType UpdateType;
	int Maj,Min,Rel,Bui, checksum=0;
	wxFileConfig *UpINI=new wxFileConfig(wxEmptyString,wxEmptyString,AppPath,wxEmptyString,wxCONFIG_USE_LOCAL_FILE);
	TVersion instVer;
	long ver;
	UpINI->SetPath(L"VersionInfo");
	UpINI->Read(L"UpdateType",&ver,0);
	UpdateType=(TupdType)ver;
	UpINI->Read(L"Version",&ver,0);
	Maj = (int) ver;
	checksum += ver;
	UpINI->Read(L"SubVersion",&ver,0);
	Min = (int) ver;
	checksum += ver;
	UpINI->Read(L"Release",&ver,0);
	Rel = (int) ver;
	checksum += ver;
	UpINI->Read(L"Build",&ver,0);
	Bui = (int) ver;
	checksum += ver;

	unsigned int os_mask;
	unsigned int os;
	#ifdef __WXGTK__
		os_mask = 0x01;
	#elif (defined __WXMSW__)
		os_mask = 0x02;
	#else
		os_mask = 0x00;	// No toolkit defined
	#endif // __WXGTK__
	UpINI->Read(L"OS", &ver, 0);
	os = (unsigned int) ver;
	os = os & os_mask;

	delete UpINI;

	if ((os == os_mask) && os_mask){
		if(checksum > 0){

			instVer=GetInstalledVersion();
			if((Maj>instVer.Maj) || (Min>instVer.Min) || (Rel>instVer.Rel) || (Bui>instVer.Bui)){
				wxString Msg,type,label;
				type=_("Update type: ");
				switch(UpdateType){
					case utOriginal:
						label=_("New version");
						type+=label;
						break;
					case utBugFix:
						label=_("Bugfix");
						type+=label;
						break;
					case utSecurity:
						label=_("Security fix");
						type+=label;
						break;
					case utImprovement:
						label=_("Improved features");
						type+=label;
						break;
					case utRebuild:
						label=_("Rebuild");
						type+=label;
						break;
					case utInstallerBugFix:
						type+=_("Bugfix (Installer)");
						break;
					case utInstallerSecurity:
						type+=_("Security fix (Installer)");
						break;
					case utInstallImprovement:
						type+=_("Improved features (Installer)");
						break;
					case utInstallerRebuild:
						type+=_("Rebuild (Installer)");
						break;
					case utSiteChanged:
						type+=_("Website update");
						break;
					case utHelpImprovement:
						type+=_("Improved help");
						break;
					case utWxUpdate:
						type+=_("wxWidgets library update");
						break;
					case utTranslationNew:
						type+=_("New translation");
						break;
					case utTranslationUpdate:
						type+=_("Translation update");
						break;
					default:
						type+=_("Unclassified");}
				Msg=_("A new version of OpenMoneyBox is available.\nDo you want to open the website to download it?")+L"\n\n"+type;

				if(wxMessageBox(Msg, _("OpenMoneyBox"), wxYES_NO) == wxYES) wxLaunchDefaultBrowser(WebAddress);
			}
		}
	}

	if(::wxFileExists(AppPath))::wxRemoveFile(AppPath);

	if(checksum){
    lcheck=::wxDateTime::Today();
    #ifdef __WXMSW__
      regstr=lcheck.Format("%d/%m/%Y",wxDateTime::Local);
    #else
      regstr=lcheck.Format(L"%d/%m/%Y",wxDateTime::Local);
    #endif // __WXMSW__
    #ifdef __WXMSW__
      #ifdef _OMB_USEREGISTRY
        K->SetValue("LastUpdateCheck",regstr);
      #endif // _OMB_USEREGISTRY
    #else
    	#ifdef _OMB_USE_GSETTINGS
    		#ifdef __OPENSUSE__
    			g_settings_set_string(settings_advanced, "lastupdatecheck", regstr.c_str());
    		#else
    			g_settings_set_string(settings_advanced, "lastupdatecheck", regstr);
				#endif // __OPENSUSE__
    	#else
				INI->Write(L"LastUpdateCheck",regstr);
				INI->Flush();
      #endif // _OMB_USE_GSETTINGS
    #endif
	}
}

#endif // OMBUPDATE_CPP_INCLUDED
