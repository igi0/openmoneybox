/***************************************************************
 * Name:      ombupdate.cpp
 * Purpose:   Code for OpenMoneyBox update
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-12-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMBUPDATE_HEADER_INCLUDED
	#define OMBUPDATE_HEADER_INCLUDED

	#ifndef WX_PRECOMP
		#include <wx/wx.h>
	#else
		#include <wx/wxprec.h>
	#endif

	#include "../types.h"

  #ifdef _OMB_MONOLITHIC
    extern TVersion GetInstalledVersion(void);
    extern wxDateTime Omb_StrToDate(wxString S);
    extern wxString GetVersionFile(void);
  #else
    // Calls to module igiomb
    WXIMPORT TVersion GetInstalledVersion(void);
    WXIMPORT wxDateTime Omb_StrToDate(wxString S);
    WXIMPORT wxString GetVersionFile(void);
  #endif // _OMB_MONOLITHIC

	WXIMPORT wxLanguage FindLang(void);

	#ifdef __WXMSW__
		// Calls to module bilopt.dll
		#ifdef _OMB_MONOLITHIC
      extern wxString GetInstallationPath(void);
		#else
      WXIMPORT wxString GetInstallationPath(void);
    #endif // _OMB_MONOLITHIC
		#ifdef _OMB_USEREGISTRY
			WXEXPORT wxString GetSWKey(void);
		#endif // _OMB_USEREGISTRY

	#endif  // __WXMSW__

	#ifdef __WXGTK__
		WXIMPORT wxString GetUserConfigDir(void);
	#endif // __WXGTK__

	#ifdef __WXMAC__
		WXIMPORT wxString GetUserConfigDir(void);
	#endif // __WXMAC__

#endif  // OMBUPDATE_HEADER_INCLUDED
