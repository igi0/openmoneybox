/***************************************************************
 * Name:      OmbTrayApp.h
 * Purpose:   OpenMoneyBox TaskBar Header
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License: GNU
 **************************************************************/

#ifndef OMBTASKBAR_H
	#define OMBTASKBAR_H

//	#include <wx/app.h>
	#include <wx/taskbar.h>
	#include <wx/timer.h>
//	#include <vector>

//	#include "../types.h"
	#include "../omb35core.h"

	#include "../openmoneybox/openmoneybox.h"

	class ombTaskBarIcon: public wxTaskBarIcon
	{
		//friend class ombApp;
		private:
			/*
			std::vector<wxIcon>::const_iterator currentIcon;
			std::vector<wxIcon> icons;
			*/
			void ToggleAct(wxCommandEvent& event);
			void RunOmbClick(wxCommandEvent& event);
			void OptionsClick(wxCommandEvent& event);
			//void AuthorClick(wxCommandEvent& event);
			//void DonateClick(wxCommandEvent& event);
		protected:
			enum
			{
				ombAct = 1000,
				ombRun,
				ombOpt,
				//bilAut,
				//bilDon,
				ombExi
			};
		public:
			bool Act; // Active
			bool ringing;	// true when user input is requested
			#ifdef _OMB_USE_CIPHER
				#ifdef _OMB_SQLCIPHER_v4
					bool DatabaseIsSqlCipher_V4;	// Document SqlCipher Compatibility is V4
				#endif // _OMB_SQLCIPHER_v4
			#endif // _OMB_USE_CIPHER

			ombApp *App;

			ombTaskBarIcon();
			virtual wxMenu *CreatePopupMenu();
			void ExitClick(wxCommandEvent& event);

		DECLARE_EVENT_TABLE();
	};

	extern bool ShowOptionsDialog(
																	#ifdef _OMB_USE_CIPHER
																		#ifdef _OMB_SQLCIPHER_v4
																			bool IsOpenDB_v4
																		#else
																			void
																		#endif // _OMB_SQLCIPHER_v4
																	#endif // _OMB_USE_CIPHER
																);
	extern wxString GetDefaultDocument(void);

#endif // OMBTASKBAR_H
