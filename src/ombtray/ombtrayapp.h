/***************************************************************
 * Name:      OmbTrayApp.h
 * Purpose:   Defines Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-01
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License: GNU
 **************************************************************/

#ifndef OMBTRAYAPP_H
	#define OMBTRAYAPP_H

	#include <wx/app.h>
	#include <wx/taskbar.h>
	#include <wx/timer.h>
	#include <vector>

	#include "../types.h"
	#include "../omb35core.h"

	class MyTaskBarIcon: public wxTaskBarIcon
	{
	private:
		/*
		std::vector<wxIcon>::const_iterator currentIcon;
		std::vector<wxIcon> icons;
		*/
		void ToggleAct(wxCommandEvent& event);
		void RunBilClick(wxCommandEvent& event);
		void OptionsClick(wxCommandEvent& event);
		//void AuthorClick(wxCommandEvent& event);
		//void DonateClick(wxCommandEvent& event);
	protected:
		enum
		{
			bilAct = 1000,
			bilRun,
			bilOpt,
			bilAut,
			bilDon,
			bilExi
		};
	public:
		bool Act; // Active
		bool ringing;	// true when user input is requested
		wxMenu *menu;
		MyTaskBarIcon();
		virtual wxMenu *CreatePopupMenu();
		void ExitClick(wxCommandEvent& event);
	DECLARE_EVENT_TABLE();
	};

	class OmbTrayApp : public wxApp
	{
		private:
			#ifdef _OMB_USEINDICATOR
				bool	isIndicator,					// true when the active desktop supports libindicator
							Act,							// Enable status
							indicator_killed;	// true when KillIndicator routine is processed for the first time
				int msqidInd, msqidOmb;
				void SendStatus(void);
				void KillIndicator(long cmd);
			#endif // _OMB_USEINDICATOR
			MyTaskBarIcon *Tray;
			void DoChecks(void);
			void CheckTimerFire();
		public:
			virtual bool OnInit();
			void TimerFire(wxTimerEvent& event);
			int OnExit();
	};

	WXIMPORT wxLanguage FindLang(void);
	WXIMPORT wxString GetDefaultDocument(void);
	WXIMPORT bool ShowOptionsDialog(void);

	WXIMPORT wxString GetShareDir(void);

	#ifdef __WXGTK__
		WXIMPORT wxString GetUserConfigDir(void);
		WXIMPORT wxString GetDesktopEnv(void);
	#endif // __WXGTK__

	#ifdef __WXMAC__
		WXIMPORT wxString GetUserConfigDir(void);
	#endif // __WXMAC__

#endif // OMBTRAYAPP_H
