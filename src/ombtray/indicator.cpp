/***************************************************************
 * Name:      indicator.cpp
 * Purpose:   Code for appindicator application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-05-12
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:   GNU
 **************************************************************/

#include <sys/msg.h>
#include <errno.h>

#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include <libintl.h>
#include <locale.h>

#ifdef __UBUNTU_2204__
	#include <libayatana-appindicator/app-indicator.h>
#else
	#include <libappindicator/app-indicator.h>
#endif

#include "mailbox.h"

int msqidInd, msqidOmb;

bool initialization;	// true during initialization for mailbox on btCmnd_send_status
AppIndicator *indicator;

static void ToggleAct (GtkWidget *widget);
static void RunOmbClick();
static void OptionsClick();
static void ExitClick();
static void Close();
static gboolean CheckCommand(gpointer data);

static void ToggleAct (GtkWidget *widget){
	if(initialization){
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(widget), TRUE);
		return;}
  omb_msgbuf command, init_message;
  command.mtype = btCmd_do_action;
  command.value = cmd_toggle_enable;
	msgsnd( msqidInd, &command, sizeof(command), 0);

	msgrcv( msqidOmb, &init_message, sizeof(init_message), btCmd_send_status, 0);
	if(init_message.value == tray_disabled){
		app_indicator_set_icon_full(indicator,"/usr/share/icons/hicolor/48x48/apps/logo_disabled.png", _("Tray icon disabled"));
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(widget), FALSE);}
	else if(init_message.value == tray_enabled){
		app_indicator_set_icon_full(indicator,"/usr/share/icons/hicolor/48x48/apps/logo_pale.png", _("Tray icon enabled"));
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(widget), TRUE);}}

static void RunOmbClick (){
  omb_msgbuf command, init_message;
  command.mtype = btCmd_do_action;
  command.value = cmd_run_openmoneybox;
	msgsnd( msqidInd, &command, sizeof(command), 0);

	msgrcv( msqidOmb, &init_message, sizeof(init_message), btCmd_kill, 0);
	Close();}

static void OptionsClick (){
  omb_msgbuf command, init_message;
  command.mtype = btCmd_do_action;
  command.value = cmd_open_options;
	msgsnd( msqidInd, &command, sizeof(command), 0);

	app_indicator_set_status (indicator, APP_INDICATOR_STATUS_PASSIVE);

	msgrcv( msqidOmb, &init_message, sizeof(init_message), btCmd_kill, 0);
	if (init_message.value == cmd_close)Close();

	app_indicator_set_status (indicator, APP_INDICATOR_STATUS_ACTIVE);}

static void ExitClick (){
  omb_msgbuf command, init_message;
  command.mtype = btCmd_do_action;
  command.value = cmd_close;
	msgsnd( msqidInd, &command, sizeof(command), 0);

	msgrcv( msqidOmb, &init_message, sizeof(init_message), btCmd_kill, 0);
	Close();}

static void Close (){
	gtk_main_quit();}

static gboolean CheckCommand (gpointer data){
	omb_msgbuf status_message;
	if(msgrcv( msqidOmb, &status_message, sizeof(status_message), btCmd_kill, IPC_NOWAIT) != -1){
		if( status_message.value == cmd_close){
			Close();
			return FALSE;}}
	if(msgrcv( msqidOmb, &status_message, sizeof(status_message), btCmd_send_status, IPC_NOWAIT) != -1) switch(status_message.value){
		case tray_attention:
			app_indicator_set_status (indicator, APP_INDICATOR_STATUS_ATTENTION);
			break;
		default:
			if (app_indicator_get_status(indicator) == APP_INDICATOR_STATUS_ATTENTION)
				app_indicator_set_status (indicator, APP_INDICATOR_STATUS_ACTIVE);}
	return TRUE;}

int main (int argc, char **argv){
	setlocale(LC_ALL, "");    // decided by environment
	#ifdef _OMB_DEBUG
		bindtextdomain("openmoneybox", "./");
	#else
		bindtextdomain("openmoneybox", "/usr/share/locale");
	#endif // _OMB_DEBUG
	textdomain("openmoneybox");

  GtkWidget *window;

  gtk_init (&argc, &argv);

  /* main window */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), _("File"));
  gtk_window_set_icon_name (GTK_WINDOW (window), "indicator-messages-new");
  g_signal_connect (G_OBJECT (window),
                    "destroy",
                    G_CALLBACK (gtk_main_quit),
                    NULL);

	GtkWidget *menu, *enable_menu_item, *item;

  menu = gtk_menu_new();

  enable_menu_item = gtk_check_menu_item_new_with_label(_("Enabled"));
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), enable_menu_item);
  g_signal_connect(G_OBJECT(enable_menu_item), "activate", G_CALLBACK(ToggleAct), NULL);
  gtk_widget_show(enable_menu_item);

	#ifdef _OMB_MONOLITHIC
  	item = gtk_menu_item_new_with_label(_("Show OpenMoneyBox"));
	#else
  	item = gtk_menu_item_new_with_label(_("Start OpenMoneyBox"));
	#endif // _OMB_MONOLITHIC
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
  g_signal_connect(item, "activate", G_CALLBACK(RunOmbClick), NULL);
  gtk_widget_show(item);

	item = gtk_separator_menu_item_new();
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
  gtk_widget_show(item);

  item = gtk_menu_item_new_with_label(_("Options"));
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
  g_signal_connect(item, "activate", G_CALLBACK(OptionsClick), NULL);
  gtk_widget_show(item);

  item = gtk_menu_item_new_with_label(_("Exit"));
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
  g_signal_connect(item, "activate", G_CALLBACK(ExitClick), NULL);
  gtk_widget_show(item);

  /* Indicator */
  indicator = app_indicator_new ("example-simple-client",
                                 "/usr/share/icons/hicolor/48x48/apps/logo_pale.png",
                                 APP_INDICATOR_CATEGORY_APPLICATION_STATUS);

  app_indicator_set_status (indicator, APP_INDICATOR_STATUS_ACTIVE);
  app_indicator_set_attention_icon_full (indicator, "/usr/share/icons/hicolor/48x48/apps/ombtray_attention.png", _("Tray icon needs attention"));

  app_indicator_set_menu (indicator, GTK_MENU (menu));

	//Create the Indicator Mailbox
	msqidInd = msgget(MAILBOX_INDICATOR, 0600 | IPC_CREAT);
	//Create the Ombtray Mailbox
	msqidOmb = msgget(MAILBOX_OMBTRAY, 0600 | IPC_CREAT);

	omb_msgbuf init_message;

	/*
	#ifdef _OMB_DEBUG
		while(msgrcv( msqidOmb, &init_message, sizeof(init_message), btCmd_kill, IPC_NOWAIT) != -1){}
	#endif // _OMB_DEBUG
	*/

	initialization = true;
	msgrcv( msqidOmb, &init_message, sizeof(init_message), btCmd_send_status, 0);
	switch(init_message.value){
		case tray_disabled:
			app_indicator_set_icon_full(indicator,"/usr/share/icons/hicolor/48x48/apps/logo_disabled.png", _("Tray icon disabled"));
			gtk_widget_set_sensitive(enable_menu_item,FALSE);
			break;
		case tray_enabled:
			gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(enable_menu_item),TRUE);
	}
	initialization = false;

	g_timeout_add_seconds (1,
                       CheckCommand,
                       NULL);

  gtk_main ();

  return 0;}

