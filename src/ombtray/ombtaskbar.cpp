/***************************************************************
 * Name:      ombtaskbar.cpp
 * Purpose:   Code for TaskBar Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:   GNU
 **************************************************************/

#ifndef OMBTASKBAR_CPP_INCLUDED
#define OMBTASKBAR_CPP_INCLUDED

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#ifdef __WXMAC__
	#include <wx/notifmsg.h>
#endif // __WXMAC__

#include "../platformsetup.h"
#include "ombtaskbar.h"
#include "../types.h"

#ifdef __OPENSUSE__
	#include "../../rsrc/icons/16/logo_pale.xpm"
	#include "../../rsrc/icons/16/logo_disabled.xpm"
	//#include "../../rsrc/icons/16/ombtray_attention.xpm"
#else
	#include "../../rsrc/icons/logo_pale.xpm"
	#include "../../rsrc/icons/logo_disabled.xpm"
	//#include "../../rsrc/icons/ombtray_attention.xpm"
#endif // __OPENSUSE__

#ifdef _OMB_USEINDICATOR
	#include "mailbox.h"
#endif // _OMB_USEINDICATOR

	#ifdef _OMB_DEBUG
		const int MainTimerInt = 10000;
	#else
		const int MainTimerInt = 600000;			// Data check timer
	#endif // _OMB_DEBUG

// Code start

#ifdef _OMB_USEINDICATOR
	extern omb_msgbuf command;
	extern wxTimer *CommandTimer;
#endif // _OMB_USEINDICATOR

extern bool HasAlarms;
wxTimer *MainTimer;
extern TData *Data;

BEGIN_EVENT_TABLE( ombTaskBarIcon, wxTaskBarIcon )
	EVT_MENU( ombAct, ombTaskBarIcon::ToggleAct )
	EVT_MENU( ombRun, ombTaskBarIcon::RunOmbClick )
	EVT_MENU( ombOpt, ombTaskBarIcon::OptionsClick )
	//EVT_MENU( ombAut, ombTaskBarIcon::AuthorClick )
	//EVT_MENU( ombDon, ombTaskBarIcon::DonateClick )
	EVT_MENU( ombExi, ombTaskBarIcon::ExitClick )
END_EVENT_TABLE()

ombTaskBarIcon::ombTaskBarIcon(){
	App = NULL;
	Act = false;
	ringing = false;
}

wxMenu *ombTaskBarIcon::CreatePopupMenu()
{
	wxMenu *Popup = new wxMenu();
	if(!ringing){
		wxMenuItem* Activated;
		Activated = new wxMenuItem( Popup, ombAct, wxString( _("Enabled") ) , wxEmptyString, wxITEM_CHECK );
		Popup->Append( Activated );
		Activated->Check(Act);
		Activated->Enable(HasAlarms);

		wxMenuItem* RunOmb;
		#ifdef _OMB_MONOLITHIC
			RunOmb = new wxMenuItem( Popup, ombRun, wxString( _("Show OpenMoneyBox") ) , wxEmptyString, wxITEM_NORMAL );
		#else
			RunOmb = new wxMenuItem( Popup, ombRun, wxString( _("Start OpenMoneyBox") ) , wxEmptyString, wxITEM_NORMAL );
		#endif // _OMB_MONOLITHIC
		Popup->Append( RunOmb );

		Popup->AppendSeparator();

		wxMenuItem* Options;
		Options = new wxMenuItem( Popup, ombOpt, wxString( _("Options") ) , wxEmptyString, wxITEM_NORMAL );
		Popup->Append( Options );

		/*
		wxMenuItem* Author;
		Author = new wxMenuItem( Popup, ombAut, wxString( _("Author") ) , wxEmptyString, wxITEM_NORMAL );
		Popup->Append( Author );

		wxMenuItem* Donate;
		Donate = new wxMenuItem( Popup, ombDon, wxString( _("Donate") ) , wxEmptyString, wxITEM_NORMAL );
		Popup->Append( Donate );
    */

		wxMenuItem* Exit;
		Exit = new wxMenuItem( Popup, ombExi, wxString( _("Exit") ) , wxEmptyString, wxITEM_NORMAL );
		Popup->Append( Exit );}
	return Popup;
}

void ombTaskBarIcon::ToggleAct(wxCommandEvent& event){
  Act = !Act;
  if(Act){
  	App->MainTimer->Start(MainTimerInt, false);
  	#ifdef __WXMSW__
      SetIcon(wxIcon(logo_pale_xpm),_("Alarm Management"));
    #else
      SetIcon(wxICON(logo_pale),_("Alarm Management"));
    #endif // __WXMSW__
  }
  else{
  	App->MainTimer->Stop();
  	#ifdef __WXMSW__
      SetIcon(wxIcon(logo_disabled_xpm),_("Alarm Management"));
    #else
      SetIcon(wxICON(logo_disabled),_("Alarm Management"));
    #endif // __WXMSW__
}}

void ombTaskBarIcon::RunOmbClick(wxCommandEvent& event){
	if(App->ShowFrame()) RemoveIcon();
}

void ombTaskBarIcon::OptionsClick(wxCommandEvent& event){
  if(ShowOptionsDialog(
												#ifdef _OMB_USE_CIPHER
													#ifdef _OMB_SQLCIPHER_v4
														DatabaseIsSqlCipher_V4
													#endif // _OMB_SQLCIPHER_v4
												#endif // _OMB_USE_CIPHER
											)){
    wxString Document = GetDefaultDocument();
    if(Document == wxEmptyString)
    	RemoveIcon();
    else
   		Data->OpenDatabase(Document);}}

void ombTaskBarIcon::ExitClick(wxCommandEvent& event){
  RemoveIcon();

  #ifdef __WXMSW__
    wxWindow *frame = wxTheApp->GetTopWindow();
    frame->Close();
  #endif // __WXMSW__

  wxTheApp->ExitMainLoop();
  wxTheApp->Exit();}

#endif // OMBTASKBAR_CPP_INCLUDED
