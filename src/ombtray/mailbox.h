/***************************************************************
 * Name:      mailbox.h
 * Purpose:   Code for appindicator application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2022-11-19
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:   GNU
 **************************************************************/

#ifndef MAILBOX_H_INCLUDED
#define MAILBOX_H_INCLUDED

	#define MAILBOX_INDICATOR 2000
	#define MAILBOX_OMBTRAY 2001

	//int msqidInd, msqidBil;

	enum ombtray_status{
		tray_running	= 1,
		tray_disabled,
		tray_enabled,
		tray_attention};

	long enum Command_value{
		cmd_toggle_enable = 10,
		cmd_run_openmoneybox,
		cmd_open_options,
		cmd_close,
		cmd_resume};

	long enum indicatorCommand{
		btCmd_send_status = 20,
		btCmd_do_action,
		btCmd_kill};

	struct omb_msgbuf{
		long mtype;
		long value;};

#endif // MAILBOX_H_INCLUDED
