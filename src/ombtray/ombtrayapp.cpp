/***************************************************************
 * Name:      OmbTrayApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-06-15
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:   GNU
 **************************************************************/

#ifndef OMBTRAY_CPP_INCLUDED
#define OMBTRAY_CPP_INCLUDED

#ifdef _OMB_USEINDICATOR
	#include <sys/msg.h>
#endif // _OMB_USEINDICATOR

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#ifndef __WXMSW__
  #include <signal.h>
#else
	#include <wx/msw/registry.h> // For wxRegKey
#endif // __WXMSW__

#ifdef _OMB_USE_GSETTINGS
	#include <gio/gio.h>	// For GSettings
#else
	#include <wx/fileconf.h> // For wxFileConfig
#endif // _OMB_USE_GSETTINGS
#include <wx/snglinst.h>	// for wxSingleInstanceChecker
#include <wx/filename.h>
#include <wx/stdpaths.h> // For wxStandardPathsBase

#ifdef __WXMAC__
	#include <wx/notifmsg.h>
#endif // __WXMAC__

#include "../platformsetup.h"
#include "ombtrayapp.h"
#include "../types.h"
//#include "../bil30opt.h"
//#include "../bil30core.h"
//#include "../../rsrc/icons/Icon_Application.xpm"

#ifdef __OPENSUSE__
	#include "../../rsrc/icons/16/logo_pale.xpm"
	#include "../../rsrc/icons/16/logo_disabled.xpm"
	#include "../../rsrc/icons/16/ombtray_attention.xpm"
#else
	#include "../../rsrc/icons/logo_pale.xpm"
	#include "../../rsrc/icons/logo_disabled.xpm"
	#include "../../rsrc/icons/ombtray_attention.xpm"
#endif // __OPENSUSE__

#ifdef __WXMSW__
	#include "../../rsrc/icons/logo_dark.xpm"
	#include "../../rsrc/icons/logo_disabled_dark.xpm"
#endif // __WXMSW__

#ifdef _OMB_USEINDICATOR
	#include "mailbox.h"
#endif // _OMB_USEINDICATOR

#ifdef _OMB_DEBUG
	const int MainTimerInt=10000;
#else
	const int MainTimerInt=600000;			// Data check timer
#endif // _OMB_DEBUG
	const int TermTimerInt=3000;				// Kill check timer
#ifdef _OMB_USEINDICATOR
	const int CommandTimerInt=1000;		// Command check timer (_OMB_USEINDICATOR)
#endif // _OMB_USEINDICATOR

#ifdef _OMB_USE_CIPHER
	extern int ExecuteUpdate(sqlite3 *m_db, const char* sql/*, bool saveRC = false*/);
#endif // _OMB_USE_CIPHER

// Code start
#include "../languages.cpp"	// Contains language definitions

// the arrays must be in sync
wxCOMPILE_TIME_ASSERT( WXSIZEOF(langNames)==WXSIZEOF(langIds),LangArraysMismatch);

wxLanguage Lan;

wxLocale omb_locale;

#ifndef __WXMSW__
  bool Killed=false;	// Set to true when SIGTERM is received
  wxTimer *CheckTimer;
#endif // __WXMSW__

#ifdef _OMB_USEINDICATOR
	omb_msgbuf command;
	wxTimer *CommandTimer;
#endif // _OMB_USEINDICATOR

bool HasAlarms=false;
#ifndef __WXMSW__
	#ifdef _OMB_USE_GSETTINGS
		GSettings *settings_general;
		GSettings *settings_charts;
		GSettings *settings_tools;
	#else
  	wxFileConfig *INI;
	#endif // _OMB_USE_GSETTINGS
#else
	long LightTheme = 1;
#endif // __WXMSW__
wxSingleInstanceChecker *m_checker_ombtray;	// checks if another instance is already running
wxTimer *MainTimer;
TData *Data;

IMPLEMENT_APP(OmbTrayApp);

#ifndef __WXMSW__
  void term(int signum){	// Captures the SIGTERM signal
      Killed = true;}
#endif // __WXMSW__

bool OmbTrayApp::OnInit()
{
	#ifdef __WXMSW__
    // TODO (igor#1#): implement Windows version
	#else
		#ifdef _OMB_USE_GSETTINGS
			settings_general = g_settings_new("org.igisw.openmoneybox.general");
			settings_charts = g_settings_new("org.igisw.openmoneybox.charts");
			settings_tools = g_settings_new("org.igisw.openmoneybox.tools");
		#else
			wxString homecfg = GetUserConfigDir();

			homecfg += L"/omb.ini";
			if(!::wxFileExists(homecfg)){
				wxFile *f=new wxFile(homecfg,wxFile::write);
				if(! f->IsOpened()){
					// FIXME (igor#1#): insert error handling
					return false;
				}
				f->Create(homecfg,false,wxS_DEFAULT);
				f->Close();}
			INI=new wxFileConfig(wxEmptyString,wxEmptyString,homecfg,wxEmptyString,wxCONFIG_USE_LOCAL_FILE,wxConvAuto());
		#endif // _OMB_USE_GSETTINGS
  #endif // __WXMSW__

	//GeneralOptions=new TOpt();

	#ifdef _OMB_DEBUG
		#ifdef __WXGTK__
				wxLocale::AddCatalogLookupPathPrefix(L".");
			#endif
	#endif // _OMB_DEBUG

	#ifdef __WXMSW__
    wxLocale::AddCatalogLookupPathPrefix(GetShareDir());
	#else
		wxLocale::AddCatalogLookupPathPrefix(GetShareDir()+L"locale");
	#endif // __WXMSW__
	#ifdef __WXMAC__
			wxLocale::AddCatalogLookupPathPrefix(AppDir);
	#endif // __WXMAC__
	omb_locale.Init(wxLANGUAGE_DEFAULT,wxLOCALE_LOAD_DEFAULT);
	omb_locale.AddCatalog(L"openmoneybox");
	Lan = FindLang();

	// Check if already running
	wxString user = wxGetUserId();
	const wxString name2 = L"omb-" + user;
	wxSingleInstanceChecker *m_checker_omb = new wxSingleInstanceChecker(name2);
	if ( m_checker_omb->IsAnotherRunning() ){
		wxMessageBox(_("OpenMoneyBox is already running!"),_("OpenMoneyBox"), wxICON_EXCLAMATION);
		return false;}
	else delete m_checker_omb;

	const wxString name = L"ombtray-" + user;
	m_checker_ombtray = new wxSingleInstanceChecker(name);
	if ( m_checker_ombtray->IsAnotherRunning() ){
		wxMessageBox(_("OpenMoneyBox is already running!"),_("OpenMoneyBox"), wxICON_EXCLAMATION);
		return false;}

	Connect( wxEVT_TIMER, wxTimerEventHandler( OmbTrayApp::TimerFire ) );

	wxString Document = GetDefaultDocument();
	if(Document.IsEmpty()){
		delete m_checker_ombtray;
		return 0;}
	if(! wxFileExists(Document)){
		delete m_checker_ombtray;
		return 0;}
	Data = new TData(NULL);

	if(! Data->OpenDatabase(Document)){
		delete m_checker_ombtray;
		return 0;}

	#ifdef _OMB_USEINDICATOR
		isIndicator = false;
		wxString Desktop = GetDesktopEnv();
		/*
		if(Desktop == L"unity") isIndicator = true;											// until Ubuntu Zesty
		else*/ if(Desktop == L"unity:unity7:ubuntu") isIndicator = true;	// from Ubuntu Artful
		else if(Desktop.Right(5) == L"gnome") isIndicator = true;	// for Ubuntu Artful

		if(isIndicator){
			indicator_killed = false;
			wxString Proc=wxGetCwd();
			#ifdef _OMB_DEBUG
				Proc += L"/Debug_GTK";
			#endif // _OMB_DEBUG
			Proc += L"/ombindicator";
			if(! wxExecute(Proc, wxEXEC_ASYNC))return false;

			//Create the Indicator Mailbox
			msqidInd = msgget(MAILBOX_INDICATOR, 0600 | IPC_CREAT);
			//Create the Ombtray Mailbox
			msqidOmb = msgget(MAILBOX_OMBTRAY, 0600 | IPC_CREAT);}
		else
	#endif // _OMB_USEINDICATOR
	Tray=new MyTaskBarIcon();

	HasAlarms=Data->HasAlarms();
	#ifdef _OMB_USEINDICATOR
		if(isIndicator){
			Act = HasAlarms;
			SendStatus();
			if(!HasAlarms){
				wxString title, message;
				title = _("OpenMoneyBox");
				message = _("There is no alarm set in your budget document");
				#ifdef _OMB_NOTIFYSENDAPPSWITCH
					wxString proc = L"notify-send -h string:desktop-entry:openmoneybox -i " + GetShareDir() + L"icons/hicolor/48x48/apps/openmoneybox.png -a " + title + L" \""
				#else
					wxString proc = L"notify-send -h string:desktop-entry:openmoneybox -i " + GetShareDir() + L"icons/hicolor/48x48/apps/openmoneybox.png " + title + L" \""
				#endif // _OMB_NOTIFYSENDAPPSWITCH
					+ message + L"\"";
				wxExecute(proc,wxEXEC_ASYNC);}}
		else{
	#endif // _OMB_USEINDICATOR

        #ifdef __WXMSW__
			wxRegKey *ThemeKey = new wxRegKey(wxRegKey::HKCU, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize", wxRegKey::WOW64ViewMode_Default);
			//ThemeKey->SetName(wxRegKey::HKCU, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize");
			if(ThemeKey->HasValue("SystemUsesLightTheme")) ThemeKey->QueryValue("SystemUsesLightTheme", &LightTheme);
        #endif // __WXMSW__

		Tray->Act = HasAlarms;
		if(HasAlarms){
			#ifdef __WXMSW__
                if(LightTheme) Tray->SetIcon(wxIcon(logo_dark_xpm),_("Alarm Management"));
				else Tray->SetIcon(wxIcon(logo_pale_xpm),_("Alarm Management"));
			#else
				Tray->SetIcon(wxICON(logo_pale),_("Alarm Management"));
			#endif // __WXMSW__
		}
		else{
			wxString title,message;
			title=_("OpenMoneyBox");
			message=_("There is no alarm set in your budget document");
			#ifdef __WXMSW__
				if(LightTheme) Tray->SetIcon(wxIcon(logo_disabled_dark_xpm),_("Alarm Management"));
				else Tray->SetIcon(wxIcon(logo_disabled_xpm),_("Alarm Management"));
				Tray->ShowBalloon(title,message,10000,NIIF_INFO);
			#elif defined ( __WXMAC__ )
				Tray->SetIcon(wxIcon(logo_disabled_xpm),_("Alarm Management"));
				wxNotificationMessage *Notification = new wxNotificationMessage(title, message);
				Notification->Show();
			#else
				Tray->SetIcon(wxICON(logo_disabled), _("Alarm Management"));
				#ifdef _OMB_NOTIFYSENDAPPSWITCH
					wxString proc = L"notify-send -h string:desktop-entry:openmoneybox -i " + GetShareDir() + L"icons/hicolor/48x48/apps/openmoneybox.png -a " + title + L" \""
				#else
					wxString proc = L"notify-send -h string:desktop-entry:openmoneybox -i " + GetShareDir() + L"icons/hicolor/48x48/apps/openmoneybox.png " + title + L" \""
				#endif // _OMB_NOTIFYSENDAPPSWITCH
					+ message + L"\"";
				wxExecute(proc,wxEXEC_ASYNC);
			#endif
		}
	#ifdef _OMB_USEINDICATOR
		}
	#endif // _OMB_USEINDICATOR

	MainTimer=new wxTimer();
	MainTimer->SetOwner(this);
	if(HasAlarms)MainTimer->Start(MainTimerInt,false);

	#ifndef __WXMSW__
    // Prepare timer to check SIGTERM
    CheckTimer=new wxTimer();
    CheckTimer->SetOwner(this);
    CheckTimer->Start(TermTimerInt,false);
  #endif // __WXMSW__

  #ifdef _OMB_USEINDICATOR
  	if(isIndicator){
			CommandTimer=new wxTimer();
			CommandTimer->SetOwner(this);
			CommandTimer->Start(CommandTimerInt,false);}
  #endif // _OMB_USEINDICATOR

	wxTheApp->MainLoop();
	return true;}

void OmbTrayApp::TimerFire( wxTimerEvent& event ){
	switch(event.GetInterval()){
		case MainTimerInt:
			DoChecks();
			break;
		#ifndef __WXMSW__
      case TermTimerInt:
        CheckTimerFire();
        break;
    #endif // __WXMSW__

    #ifdef _OMB_USEINDICATOR
    	case CommandTimerInt:
    		if(! isIndicator) break;
    		if (msgrcv( msqidInd, &command, sizeof(command), btCmd_do_action, IPC_NOWAIT) != -1){
    			wxString Proc;
    			switch(command.value){
						case cmd_toggle_enable:
							Act = !Act;
							SendStatus();
							break;
						case cmd_run_openmoneybox:
							if(Data->FileData.Modified)
								#ifdef _OMB_USE_CIPHER
									sqlite3_exec(Data->database, "RELEASE rollback;", NULL, NULL, NULL);
								#else
									Data->database->ReleaseSavepoint(L"rollback");
								#endif // _OMB_USE_CIPHER
							Proc = wxGetCwd();
							#ifdef _OMB_DEBUG
								Proc += L"/Debug_GTK";
							#endif // _OMB_DEBUG

							Proc = Proc + L"/openmoneybox";

							//delete Data;
							delete m_checker_ombtray;

							if(wxExecute(Proc,wxEXEC_ASYNC))KillIndicator(cmd_close);
							break;
						case cmd_open_options:
							if(ShowOptionsDialog()){
								wxString Document = GetDefaultDocument();
								if(Document == wxEmptyString){
									delete m_checker_ombtray;
									KillIndicator(cmd_resume);
									term(SIGTERM);}
								else
									Data->OpenDatabase((Document));
							}
							omb_msgbuf resume_message;
							resume_message.mtype = btCmd_kill;
							resume_message.value = cmd_resume;
							msgsnd( msqidOmb, &resume_message, sizeof(resume_message), 0);
							break;
						case cmd_close:
							KillIndicator(cmd_close);
						}
					}
    #endif // _OMB_USEINDICATOR
}}

void OmbTrayApp::DoChecks(void)
{
	MainTimer->Stop();
	#ifdef _OMB_USEINDICATOR
		omb_msgbuf attention_message;
		if(isIndicator){
			attention_message.mtype = btCmd_send_status;
			attention_message.value = tray_attention;
			msgsnd( msqidOmb, &attention_message, sizeof(attention_message), 0);}
		else{
	#endif // _OMB_USEINDICATOR
		Tray->ringing = true;
		#ifdef __WXMSW__
			Tray->SetIcon(wxIcon(ombtray_attention_xpm),_("Alarm Management"));
		#else
			Tray->SetIcon(wxICON(ombtray_attention),_("Alarm Management"));
		#endif // __WXMSW__
	#ifdef _OMB_USEINDICATOR
		}
	#endif // _OMB_USEINDICATOR

	Data->CheckLentObjects();
	Data->CheckBorrowedObjects();
	Data->CheckShopList();

	if(Data->FileData.Modified){
		#ifdef _OMB_USE_CIPHER
			sqlite3_exec(Data->database, "RELEASE rollback;", NULL, NULL, NULL);
		#else
			Data->database->ReleaseSavepoint(L"rollback");
		#endif // _OMB_USE_CIPHER
		Data->FileData.Modified = false;
		#ifdef _OMB_USE_CIPHER
			ExecuteUpdate(Data->database, "SAVEPOINT rollback;");
		#else
			Data->database->Savepoint(L"rollback");
		#endif // _OMB_USE_CIPHER
	}

	#ifdef _OMB_USEINDICATOR
		if(isIndicator){
			attention_message.value = tray_enabled;
			msgsnd( msqidOmb, &attention_message, sizeof(attention_message), 0);}
		else{
	#endif // _OMB_USEINDICATOR
		Tray->ringing = false;
		#ifdef __WXMSW__
			if(LightTheme) Tray->SetIcon(wxIcon(logo_dark_xpm),_("Alarm Management"));
			else Tray->SetIcon(wxIcon(logo_pale_xpm),_("Alarm Management"));
		#else
			Tray->SetIcon(wxICON(logo_pale),_("Alarm Management"));
		#endif // __WXMSW__
	#ifdef _OMB_USEINDICATOR
		}
	#endif // _OMB_USEINDICATOR
	wxTheApp->Yield(false);
	MainTimer->Start(MainTimerInt,false);}

int OmbTrayApp::OnExit(){
	delete Data;

	#if ((! defined ( __WXMAC__ )) && (! defined ( __FREEBSD__ )))
		__try{	// To avoid crash on some OS, e.g Ubuntu 22.04
	#endif // __WXMAC__

		if(m_checker_ombtray) delete m_checker_ombtray;

	#if ((! defined ( __WXMAC__ )) && (! defined ( __FREEBSD__ )))
			}
		catch(...){}
	#endif // __WXMAC__

	#ifndef _OMB_USEREGISTRY
		#ifndef _OMB_USE_GSETTINGS
    	delete INI;
		#endif // _OMB_USE_GSETTINGS
  #endif // _OMB_USEREGISTRY

	return 0;}

#ifndef __WXMSW__
  void OmbTrayApp::CheckTimerFire(){
    #ifdef __WXGTK__
			// Following code prepare the action to caputure SIGTERM signals
			struct sigaction action;
			memset(&action, 0, sizeof(struct sigaction));
			action.sa_handler = term;
			sigaction(SIGTERM, &action, NULL);
    #endif

    //int loop = 0;
    if(!Killed){
        //sleep(3);
        //printf("Finished loop run %d.\n", loop++);
          return;}

    // Exit code
    //CheckTimer->Stop();
    #ifdef _OMB_USEINDICATOR
    	if(isIndicator){
				KillIndicator(cmd_close);
				wxTheApp->ExitMainLoop();
				wxTheApp->Exit();}
			else{
		#endif // _OMB_USEINDICATOR
			wxCommandEvent evt(wxEVT_NULL,0);
			Tray->ExitClick(evt);
		#ifdef _OMB_USEINDICATOR
			}
		#endif // _OMB_USEINDICATOR

    return;}
#endif // __WXMSW__

#ifdef _OMB_USEINDICATOR
	void OmbTrayApp::SendStatus(void){
		omb_msgbuf init_message;
		init_message.mtype = btCmd_send_status;
		if(Act) init_message.value = tray_enabled;
		else init_message.value = tray_disabled;
		msgsnd( msqidOmb, &init_message, sizeof(init_message), 0);}

	void OmbTrayApp::KillIndicator(long cmd){
		//CommandTimer->Stop();

		if(indicator_killed) return;
		omb_msgbuf kill_message;
		kill_message.mtype = btCmd_kill;
		kill_message.value = cmd;
		msgsnd( msqidOmb, &kill_message, sizeof(kill_message), 0);
		if(cmd == cmd_close){
			term(SIGTERM);
			indicator_killed = true;}}
#endif // _OMB_USEINDICATOR

BEGIN_EVENT_TABLE( MyTaskBarIcon, wxTaskBarIcon )
	EVT_MENU( bilAct, MyTaskBarIcon::ToggleAct )
	EVT_MENU( bilRun, MyTaskBarIcon::RunBilClick )
	EVT_MENU( bilOpt, MyTaskBarIcon::OptionsClick )
	//EVT_MENU( bilAut, MyTaskBarIcon::AuthorClick )
	//EVT_MENU( bilDon, MyTaskBarIcon::DonateClick )
	EVT_MENU( bilExi, MyTaskBarIcon::ExitClick )
END_EVENT_TABLE()

MyTaskBarIcon::MyTaskBarIcon(){
	ringing = false;}

wxMenu *MyTaskBarIcon::CreatePopupMenu()
{
	wxMenu *Popup = new wxMenu();
	if(!ringing){
		wxMenuItem* Activated;
		Activated = new wxMenuItem( Popup, bilAct, wxString( _("Enabled") ) , wxEmptyString, wxITEM_CHECK );
		Popup->Append( Activated );
		Activated->Check(Act);
		Activated->Enable(HasAlarms);

		wxMenuItem* RunBil;
		RunBil = new wxMenuItem( Popup, bilRun, wxString( _("Start OpenMoneyBox") ) , wxEmptyString, wxITEM_NORMAL );
		Popup->Append( RunBil );

		Popup->AppendSeparator();

		wxMenuItem* Options;
		Options = new wxMenuItem( Popup, bilOpt, wxString( _("Options") ) , wxEmptyString, wxITEM_NORMAL );
		Popup->Append( Options );

		/*
		wxMenuItem* Author;
		Author = new wxMenuItem( Popup, bilAut, wxString( _("Author") ) , wxEmptyString, wxITEM_NORMAL );
		Popup->Append( Author );

		wxMenuItem* Donate;
		Donate = new wxMenuItem( Popup, bilDon, wxString( _("Donate") ) , wxEmptyString, wxITEM_NORMAL );
		Popup->Append( Donate );
    */

		wxMenuItem* Exit;
		Exit = new wxMenuItem( Popup, bilExi, wxString( _("Exit") ) , wxEmptyString, wxITEM_NORMAL );
		Popup->Append( Exit );}
	return Popup;
}

void MyTaskBarIcon::ToggleAct(wxCommandEvent& event){
  Act=!Act;
  if(Act){
  	MainTimer->Start(MainTimerInt,false);
  	#ifdef __WXMSW__
      SetIcon(wxIcon(logo_pale_xpm),_("Alarm Management"));
    #else
      SetIcon(wxICON(logo_pale),_("Alarm Management"));
    #endif // __WXMSW__
  }
  else{
  	MainTimer->Stop();
  	#ifdef __WXMSW__
      SetIcon(wxIcon(logo_disabled_xpm),_("Alarm Management"));
    #else
      SetIcon(wxICON(logo_disabled),_("Alarm Management"));
    #endif // __WXMSW__
}}

void MyTaskBarIcon::RunBilClick(wxCommandEvent& event){
	wxString Proc = wxEmptyString;
	wxStandardPaths std = wxStandardPaths::Get();
  wxString exePath = std.GetExecutablePath();
  wxString path;
  wxFileName::SplitPath (exePath, &path, NULL, NULL, wxPATH_NATIVE);

  Proc = Proc + path;

  #ifdef __WXMSW__
    Proc = Proc + L"\\";
  #else
    Proc = Proc + L"/";
  #endif // __WXMSW__

  Proc = Proc + L"openmoneybox";

  #ifdef __WXMSW__
    Proc = Proc + L".exe";
  #endif // __WXMSW__

  #ifdef __WXMAC__
    Proc += L"-bin";
  #endif // __WXMSW__

	delete m_checker_ombtray;
	if(wxExecute(Proc, wxEXEC_ASYNC)){
		RemoveIcon();}}

void MyTaskBarIcon::OptionsClick(wxCommandEvent& event){
  if(ShowOptionsDialog()){
    wxString Document = GetDefaultDocument();
    if(Document == wxEmptyString){
    	RemoveIcon();
    	delete m_checker_ombtray;}
    else
   		Data->OpenDatabase(Document);}}

void MyTaskBarIcon::ExitClick(wxCommandEvent& event){
  #ifndef __WXMSW__
    delete m_checker_ombtray;
  #endif // __WXMSW__
  RemoveIcon();

  #ifdef __WXMSW__
    wxWindow *frame = wxTheApp->GetTopWindow();
    frame->Close();
  #endif // __WXMSW__

  wxTheApp->ExitMainLoop();
  wxTheApp->Exit();}

#endif // OMBTRAY_CPP_INCLUDED
