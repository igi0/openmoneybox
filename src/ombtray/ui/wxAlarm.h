///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/datectrl.h>
#include <wx/dateevt.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/checkbox.h>
#include <wx/sizer.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class wxAlarmF
///////////////////////////////////////////////////////////////////////////////
class wxAlarmF : public wxDialog
{
	private:

	protected:
		wxButton* CancelBtn;
		wxButton* PostponeBtn;

		// Virtual event handlers, override them in your derived class
		virtual void PostponeBtnClick( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxStaticText* Text;
		wxDatePickerCtrl* DatePicker;
		wxCheckBox* cb_PostponeAll;

		wxAlarmF( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Reminder"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );

		~wxAlarmF();

};

