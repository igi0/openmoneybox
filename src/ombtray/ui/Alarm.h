/***************************************************************
 * Name:      Alarm.h
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-05
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef AlarmH
#define AlarmH

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/string.h>

#include "wxAlarm.h"

class TAlarmF : public wxAlarmF
{
private:
	void PostponeBtnClick(wxCommandEvent& event) override;
public:
	explicit TAlarmF(wxWindow* parent);
};

#ifdef _OMB_MONOLITHIC
  extern void Error(int Err, const wxString &Opt);
#else
  WXIMPORT void Error(int Err, const wxString &Opt);
#endif // _OMB_MONOLITHIC

#endif	// AlarmH

