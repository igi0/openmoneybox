/***************************************************************
 * Name:      wiz.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-02-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include "wiz.h"
#include "../../types.h"

TWizard::TWizard(wxWindow* parent):wxTWiz(parent,-1,_("Wizard"),wxDefaultPosition,wxSize( 400,500 ),wxDEFAULT_DIALOG_STYLE){
	UserChange=true;
	//Application title initialization
	Wel_SheetP=new Wel_Sheet(PageControl,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	PageControl->AddPage(Wel_SheetP,_("Welcome"),true,-1);
	Saved_SheetP=new Saved_Sheet(PageControl,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	PageControl->AddPage(Saved_SheetP,_("Saved money"),true,-1);
	Cash_SheetP=new Cash_Sheet(PageControl,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	PageControl->AddPage(Cash_SheetP,_("Cash money"),true,-1);
	Doc_SheetP=new Doc_Sheet(PageControl,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	wxString DocPath=GetOSDocDir();
	#ifdef __WXMSW__
		DocPath += L"\\openmoneybox.omb";
	#else
		DocPath += L"/openmoneybox.omb";
	#endif // __WXGTK__
	Doc_SheetP->BrowseButton->SetPath(DocPath);
	PageControl->AddPage(Doc_SheetP,_("Default document"),true,-1);
	End_SheetP=new End_Sheet(PageControl,-1,wxDefaultPosition,wxSize(500,300),wxTAB_TRAVERSAL);
	PageControl->AddPage(End_SheetP,_("Finish"),true,-1);
	Doc_SheetP->BrowseButton->SetLabel(_("Browse"));
	PageControl->SetSelection(0);
	UserChange=false;}

void TWizard::ForwardBtnClick(wxCommandEvent& event){
	wxString v;
	switch(PageControl->GetSelection()){
		case 1:
		v=CheckValue(Saved_SheetP->SavedEdit->GetValue());
		if(v.IsEmpty()){
			Error(26,wxEmptyString);
			Saved_SheetP->SavedEdit->SetFocus();
			return;}
		else Saved_SheetP->SavedEdit->SetValue(v);
		break;
		case 2:
		v=CheckValue(Cash_SheetP->CashEdit->GetValue());
		if(v.IsEmpty()){
			Error(26,wxEmptyString);
			Cash_SheetP->CashEdit->SetFocus();
			return;}
		else Cash_SheetP->CashEdit->SetValue(v);
		break;
		case 3:
		if(Doc_SheetP->BrowseButton->GetPath().IsEmpty()){
			Error(47, wxEmptyString);
			Doc_SheetP->BrowseButton->SetFocus();
			return;}}
	UserChange=true;
	PageControl->ChangeSelection(PageControl->GetSelection()+1);
	switch(PageControl->GetSelection()){
		case 1:
		Saved_SheetP->SavedEdit->SetFocus();
		break;
		case 2:
		Cash_SheetP->CashEdit->SetFocus();
		break;
		case 3:
		Doc_SheetP->BrowseButton->SetFocus();}
	UserChange=false;
	BackBtn->Enable(!(PageControl->GetSelection()==0));
	if(PageControl->GetSelection()==4)ForwardBtn->SetId(wxID_OK);
	else ForwardBtn->SetId(ID_forward);}

void TWizard::BackBtnClick(wxCommandEvent& event){
	UserChange=true;
	if(PageControl->GetSelection()==0)return;
	PageControl->SetSelection(PageControl->GetSelection()-1);
	UserChange=false;
	BackBtn->Enable(!(PageControl->GetSelection()==0));
	if(PageControl->GetSelection()==4)ForwardBtn->SetId(wxID_OK);
	else ForwardBtn->SetId(ID_forward);}

/*
void TWizard::PageControlChanging(bool &AllowChange){	// verificare se ancoa necessaria
	AllowChange=UserChange;}
*/

void TWizard::DisablePageChange(wxNotebookEvent& event){
  if(!UserChange)event.Veto();}
