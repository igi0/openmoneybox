///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wxwizard.h"

///////////////////////////////////////////////////////////////////////////

Wel_Sheet::Wel_Sheet( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );

	WelcomeText = new wxStaticText( this, wxID_ANY, _("\"Welcome! This wizard will guide you in the configuration of OpenMoneyBox first run. Press Next to move forward in the configuration.\""), wxDefaultPosition, wxDefaultSize, 0 );
	WelcomeText->Wrap( 380 );
	bSizer1->Add( WelcomeText, 0, wxALL, 5 );


	this->SetSizer( bSizer1 );
	this->Layout();
}

Wel_Sheet::~Wel_Sheet()
{
}

Saved_Sheet::Saved_Sheet( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );

	SavedLabel = new wxStaticText( this, wxID_ANY, _("Insert the total amount of saved money (e.g. bank accounts):"), wxDefaultPosition, wxDefaultSize, 0 );
	SavedLabel->Wrap( 380 );
	bSizer2->Add( SavedLabel, 0, wxALL|wxEXPAND, 5 );

	SavedEdit = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 200,-1 ), 0 );
	SavedEdit->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &validator_string ) );

	bSizer2->Add( SavedEdit, 0, wxALL, 5 );


	this->SetSizer( bSizer2 );
	this->Layout();
}

Saved_Sheet::~Saved_Sheet()
{
}

Cash_Sheet::Cash_Sheet( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );

	CashLabel = new wxStaticText( this, wxID_ANY, _("Insert the total amount of cash money:"), wxDefaultPosition, wxDefaultSize, 0 );
	CashLabel->Wrap( 380 );
	bSizer2->Add( CashLabel, 0, wxALL|wxEXPAND, 5 );

	CashEdit = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 200,-1 ), 0 );
	CashEdit->SetValidator( wxTextValidator( wxFILTER_NUMERIC, &validator_string2 ) );

	bSizer2->Add( CashEdit, 0, wxALL, 5 );


	this->SetSizer( bSizer2 );
	this->Layout();
}

Cash_Sheet::~Cash_Sheet()
{
}

Doc_Sheet::Doc_Sheet( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );

	DocLabel = new wxStaticText( this, wxID_ANY, _("Insert the path for OpenMoneyBox document:"), wxDefaultPosition, wxDefaultSize, 0 );
	DocLabel->Wrap( -1 );
	bSizer5->Add( DocLabel, 0, wxALL|wxEXPAND, 5 );

	BrowseButton = new wxFilePickerCtrl( this, wxID_ANY, wxEmptyString, _("Select a file"), _("*.omb;*.OMB"), wxDefaultPosition, wxDefaultSize, wxFLP_SAVE|wxFLP_USE_TEXTCTRL );
	bSizer5->Add( BrowseButton, 0, wxALL|wxEXPAND, 5 );

	DefaultBox = new wxCheckBox( this, wxID_ANY, _("Set as default document"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer5->Add( DefaultBox, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer5 );
	this->Layout();
}

Doc_Sheet::~Doc_Sheet()
{
}

End_Sheet::End_Sheet( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxVERTICAL );

	FinishLabel = new wxStaticText( this, wxID_ANY, _("The wizard finished to collect necessary information. Click Next to configure the application."), wxDefaultPosition, wxDefaultSize, 0 );
	FinishLabel->Wrap( 380 );
	bSizer6->Add( FinishLabel, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( bSizer6 );
	this->Layout();
}

End_Sheet::~End_Sheet()
{
}

BEGIN_EVENT_TABLE( wxTWiz, wxDialog )
	EVT_NOTEBOOK_PAGE_CHANGING( wxID_ANY, wxTWiz::_wxFB_DisablePageChange )
	EVT_BUTTON( ID_back, wxTWiz::_wxFB_BackBtnClick )
	EVT_BUTTON( ID_forward, wxTWiz::_wxFB_ForwardBtnClick )
END_EVENT_TABLE()

wxTWiz::wxTWiz( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxFlexGridSizer* fgSizer6;
	fgSizer6 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer6->AddGrowableCol( 0 );
	fgSizer6->AddGrowableRow( 0 );
	fgSizer6->SetFlexibleDirection( wxVERTICAL );
	fgSizer6->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	PageControl = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNB_MULTILINE|wxNB_TOP );

	fgSizer6->Add( PageControl, 1, wxALL|wxEXPAND, 5 );

	wxFlexGridSizer* fgSizer7;
	fgSizer7 = new wxFlexGridSizer( 1, 3, 0, 0 );
	fgSizer7->AddGrowableCol( 0 );
	fgSizer7->AddGrowableRow( 0 );
	fgSizer7->SetFlexibleDirection( wxBOTH );
	fgSizer7->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	CancelBtn = new wxButton( this, wxID_CANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer7->Add( CancelBtn, 0, wxALIGN_RIGHT|wxALL, 5 );

	BackBtn = new wxButton( this, ID_back, _("Back"), wxDefaultPosition, wxDefaultSize, 0 );
	BackBtn->Enable( false );

	fgSizer7->Add( BackBtn, 0, wxALL, 5 );

	ForwardBtn = new wxButton( this, ID_forward, _("Next"), wxDefaultPosition, wxDefaultSize, 0 );

	ForwardBtn->SetDefault();
	fgSizer7->Add( ForwardBtn, 0, wxALL, 5 );


	fgSizer6->Add( fgSizer7, 1, wxALIGN_RIGHT|wxEXPAND, 5 );


	this->SetSizer( fgSizer6 );
	this->Layout();

	this->Centre( wxBOTH );
}

wxTWiz::~wxTWiz()
{
}
