///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/textctrl.h>
#include <wx/valtext.h>
#include <wx/filepicker.h>
#include <wx/checkbox.h>
#include <wx/notebook.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class Wel_Sheet
///////////////////////////////////////////////////////////////////////////////
class Wel_Sheet : public wxPanel
{
	private:

	protected:

	public:
		wxStaticText* WelcomeText;

		Wel_Sheet( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 400,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~Wel_Sheet();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Saved_Sheet
///////////////////////////////////////////////////////////////////////////////
class Saved_Sheet : public wxPanel
{
	private:

	protected:

	public:
		wxStaticText* SavedLabel;
		wxTextCtrl* SavedEdit;
		wxString validator_string;

		Saved_Sheet( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 400,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~Saved_Sheet();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Cash_Sheet
///////////////////////////////////////////////////////////////////////////////
class Cash_Sheet : public wxPanel
{
	private:

	protected:

	public:
		wxStaticText* CashLabel;
		wxTextCtrl* CashEdit;
		wxString validator_string2;

		Cash_Sheet( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 400,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~Cash_Sheet();

};

///////////////////////////////////////////////////////////////////////////////
/// Class Doc_Sheet
///////////////////////////////////////////////////////////////////////////////
class Doc_Sheet : public wxPanel
{
	private:

	protected:

	public:
		wxStaticText* DocLabel;
		wxFilePickerCtrl* BrowseButton;
		wxCheckBox* DefaultBox;

		Doc_Sheet( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 400,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~Doc_Sheet();

};

///////////////////////////////////////////////////////////////////////////////
/// Class End_Sheet
///////////////////////////////////////////////////////////////////////////////
class End_Sheet : public wxPanel
{
	private:

	protected:

	public:
		wxStaticText* FinishLabel;

		End_Sheet( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 400,300 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );

		~End_Sheet();

};

///////////////////////////////////////////////////////////////////////////////
/// Class wxTWiz
///////////////////////////////////////////////////////////////////////////////
class wxTWiz : public wxDialog
{
	DECLARE_EVENT_TABLE()
	private:

		// Private event handlers
		void _wxFB_DisablePageChange( wxNotebookEvent& event ){ DisablePageChange( event ); }
		void _wxFB_BackBtnClick( wxCommandEvent& event ){ BackBtnClick( event ); }
		void _wxFB_ForwardBtnClick( wxCommandEvent& event ){ ForwardBtnClick( event ); }


	protected:
		enum
		{
			ID_back = 1000,
			ID_forward,
		};

		wxNotebook* PageControl;
		wxButton* CancelBtn;
		wxButton* BackBtn;
		wxButton* ForwardBtn;

		// Virtual event handlers, override them in your derived class
		virtual void DisablePageChange( wxNotebookEvent& event ) { event.Skip(); }
		virtual void BackBtnClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void ForwardBtnClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		wxTWiz( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Wizard"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 400,500 ), long style = wxDEFAULT_DIALOG_STYLE );

		~wxTWiz();

};

