/***************************************************************
 * Name:      ombwiz.cpp
 * Purpose:   Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-09-01
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/string.h>
#include <wx/gauge.h>

#include "ombwiz.h"

#include "../omb35core.h"

//#include "wiz.h"
#include "ui/wiz.h"

// Code start

 int RunWizard(wxGauge* Prog, wxString &S , bool def){
	int ret = -1;
	wxString DefDocument = wxEmptyString;
	TWizard *Wiz = new TWizard(wxTheApp->GetTopWindow());

	if(def) Wiz->Doc_SheetP->DefaultBox->Enable(false);

	TData *Data_Auto;
	switch(Wiz->ShowModal()){
		case wxID_OK:
			double cur;

			Data_Auto = new TData(Prog);
			Data_Auto->OpenDatabase(Wiz->Doc_SheetP->BrowseButton->GetPath());

			Wiz->Saved_SheetP->SavedEdit->GetValue().ToDouble(&cur);
			Data_Auto->AddValue(tvFou, -1, _("Saved money"),cur);
			Wiz->Cash_SheetP->CashEdit->GetValue().ToDouble(&cur);
			Data_Auto->AddValue(tvFou, -1, _("Cash money"),cur);
			Data_Auto->SetDefaultFund(_("Cash money"));

			#ifdef _OMB_USE_CIPHER
				sqlite3_exec(Data_Auto->database, "RELEASE rollback;", NULL, NULL, NULL);
			#else
				Data_Auto->database->ReleaseSavepoint(L"rollback");
			#endif // _OMB_USE_CIPHER

			DefDocument = Wiz->Doc_SheetP->BrowseButton->GetPath();
			if(Wiz->Doc_SheetP->DefaultBox->IsChecked()) ret=1;
			else ret = 2;
			delete Data_Auto; //}
			break;
		case wxID_CANCEL:
			ret = 3;}
	delete Wiz;
	S=DefDocument.c_str();
	return ret;
}

