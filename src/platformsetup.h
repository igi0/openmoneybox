/***************************************************************
 * Name:      platformsetup.h
 * Purpose:   Setup for OpenMoneyBox application
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-05-04
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

/***************************************************************
Setup settings:

	_OMB_HASNATIVEABOUT		:	Use available OS about dialog box
	_OMB_INSTALLEDCONV		:	Conversion tool for old document format included
	_OMB_INSTALLEDUPDATE	: Update tool included
	_OMB_USEREGISTRY			:	Use Registry instead of INI file - MSW only
 **************************************************************/

#ifndef PLATFORMSETUP_H_INCLUDED
#define PLATFORMSETUP_H_INCLUDED

#if (! wxCHECK_VERSION(3, 0, 0) )
	#error Minimum wxWidget version is 3.0.0!
#endif // wxCHECK_VERSION

#define _OMB_INSTALLEDCONV 1

//#define _OMB_CHART_WXPIECTRL				// set in makefile
// ** #define _OMB_CHART_MATLIBPLOT		//

#if __WXGTK__
	#define _OMB_HASNATIVEABOUT 1
#elif defined ( __WXMSW__ )
	#define _OMB_HASNATIVEABOUT 0
	#ifndef _OMB_PORTABLE_MSW
    #define _OMB_USEREGISTRY
  #endif // _OMB_PORTABLE_MSW
	//#define _OMB_INSTALLEDUPDATE  // switched in makefile
#elif defined ( __WXMAC__ )
	#define _OMB_HASNATIVEABOUT 1
#endif // __WXGTK__

// Deprecated features
	// #define _OMB_FORMAT2x
	// #define _OMB_FORMAT30x
	// #define _OMB_FORMAT31x
	// #define _OMB_FORMAT32x
	// #define _OMB_FORMAT33x
	// #define _OMB_OLDFORMATSAVE_FEATURE
// -------------------

// Patches
	// #define _OMB_PATCH_LP_1529994	// recover from bug lp: #1529994

#endif // PLATFORMSETUP_H_INCLUDED
