/***************************************************************
 * Name:      dataconverter.h
 * Purpose:   convertion Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2023-04-23
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef DATACONVERTER_H
#define DATACONVERTER_H

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/filename.h>

#ifdef _OMB_FORMAT33x
	#include "omb33core.h"
#endif // _OMB_FORMAT33x

#include "../omb34core.h"	// TODO (igor#1#): Change w/ the target header file

class dataconverter : public TData/*33*/		// TODO (igor#1#): Change w/ the target version
{
	public:
		/** Default constructor */
		dataconverter(void);
		/** Default destructor */
		//virtual ~dataconverter();
		#ifdef _OMB_FORMAT30x
			wxString LoadFromFile_0301(wxString AName, bool App);
			bool AddOper_0301(wxDateTime D, wxDateTime O, TOpType T, wxString V, wxString M, wxString N);
			wxString LoadFromFile_0302(/*bool Auto, */wxString AName, /*wxString F, */bool App);
		#endif // _OMB_FORMAT30x
		void BuildDatabase(int num_funds, int num_credits, int num_debits, int num_loans, int num_borrows, int num_transactions,
																	 int num_categories, int num_shoplists, const wxString& defaultFund);

		#ifdef _OMB_FORMAT31x
			bool Upgrade_0301to0302(void);
			bool UpgradeMaster_0301to0302(void);
		#endif // _OMB_FORMAT31x

		#ifdef _OMB_OLDFORMATSAVE_FEATURE
			bool SaveToFile_0301(bool Auto,bool Archive, wxString AFile,wxString F);
			bool SaveToFile_0302(bool Auto, bool Archive, wxString AFile);

			bool SaveToFile_031x();	// TODO (igor#1#): Write code
		#endif // _OMB_OLDFORMATSAVE_FEATURE

		#ifdef _OMB_FORMAT32x
			bool Upgrade_030301to030302(void);
			bool UpgradeMaster_030301to030302(void);
		#endif // _OMB_FORMAT32x

		#ifdef _OMB_FORMAT33x
			bool Upgrade_0303to0304(void);
		#endif // _OMB_FORMAT33x
	protected:

	private:
		#ifdef _OMB_FORMAT30x
			wxDateTime Omb_StrToTime(wxString S);
			bool SubstLocaleDecimalSeparator(wxString &S,char ToSub);
			wxString Crip(wxString S);
		#endif // _OMB_FORMAT30x
};

extern wxString iUpperCase(wxString S);
extern wxString DoubleQuote(wxString S);
// Calls to module igiomb
extern wxDateTime Omb_StrToDate(wxString S);
#endif // DATACONVERTER_H

