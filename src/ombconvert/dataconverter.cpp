/***************************************************************
 * Name:      dataconverter.cpp
 * Purpose:   convertion Code for OpenMoneyBox Application Class
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-06-15
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef DATACONVERTER_CPP_INCLUDED
#define DATACONVERTER_CPP_INCLUDED

#ifdef __LINUX__
	#include <termios.h>
	#include <string>
#endif // __LINUX__

#ifdef __WXMSW__
  #include <wx/wx.h>
  #include <wx/textdlg.h>
#endif // __WXMSW__

#include <wx/file.h>

#include "../constants.h"
#include "dataconverter.h"

extern int dbVersion;
extern wxString cs_funds;
extern wxString cs_credits;
extern wxString cs_debts;
extern wxString cs_loans;
extern wxString cs_borrows;
extern wxString cs_shoplist;

#ifdef _OMB_USE_CIPHER
	#include "../wxsqlite3.h"

	extern bool TableExists(const wxString& tableName, const wxString& databaseName, sqlite3 *m_db);
	extern int ExecuteUpdate(sqlite3 *m_db, const char* sql/*, bool saveRC = false*/);
	extern wxSQLite3Table GetTable(sqlite3 *m_db, const char* sql);
#endif // _OMB_USE_CIPHER

#ifdef __LINUX__

int getch() {
			int ch;
			struct termios t_old, t_new;

			tcgetattr(STDIN_FILENO, &t_old);
			t_new = t_old;
			t_new.c_lflag &= ~(ICANON | ECHO);
			tcsetattr(STDIN_FILENO, TCSANOW, &t_new);

			ch = getchar();	// flawfinder: ignore

			tcsetattr(STDIN_FILENO, TCSANOW, &t_old);
			return ch;
	}

	std::string getpass(const char *prompt, bool show_asterisk = true) // flawfinder: ignore
	{
		const char BACKSPACE=127;
		const char RETURN=10;

		std::string password;
		unsigned char ch=0;

		printf("%s", prompt);

		while((ch=getch())!=RETURN)
		{
			 if(ch==BACKSPACE)
			 {
					if(password.length()!=0)
						{
							 if(show_asterisk)
							 printf("\b \b");
							 password.resize(password.length()-1);
						}
			 }
			 else
			 {
					 password+=ch;
					 if(show_asterisk)
							 printf("*");
			 }
		}
		printf("\n");
		return password;
	}

#endif // __LINUX__

#ifdef _OMB_FORMAT30x
bool inputPassword(wxString p){
	int tries = 3;

	#ifdef __LINUX__
		std::string pwd;
		const char *output_string = (_("Wrong password!\n")).c_str();

		std::string test(p.mb_str(wxConvUTF8));

		while (tries){
			pwd = getpass(_("Enter the document password: ").c_str(), true);	// flawfinder: ignore
			if(pwd == test){
				delete pwd;
				return true;
			}
			else{
				printf("%s", output_string);
				if(tries) tries--;
			}
		}

		delete pwd;
	#endif // __LINUX__

	#ifdef __WXMSW__
    while(tries){
      wxPasswordEntryDialog *dlg = new wxPasswordEntryDialog(NULL, _("Enter the document password: "), "ombconvert",
                                                                     wxEmptyString, wxOK | wxCANCEL | wxCENTRE,
                                                                     wxDefaultPosition);
      if(dlg->ShowModal()){
        if(p == dlg->GetValue())return true;
        else{
          wxMessageBox(_("Wrong password!\n"), "ombconvert", wxICON_ERROR);
          if(tries) tries--;
        }
      }
      else{
        return false;
      }
    }
	#endif // __WXMSW__

	return false;
}
#endif // _OMB_FORMAT30x

dataconverter::dataconverter(void):TData/*33*/()	// TODO (igor#1#): Change w/ the target version
{
	//ctor
}

/*
dataconverter::~dataconverter()
{
	//dtor
}
*/

#ifdef _OMB_FORMAT30x
	wxString dataconverter::LoadFromFile_0301(wxString AName, bool App){
		Parsing = true;

		if(! ::wxFileExists(AName)){
			//delete Dialog;
			Parsing = false;
			return wxEmptyString;}
		wxString Proc;
		switch(CheckFileFormat(AName)){
		case bilFFORMAT_UNKNOWN:	// Unknown Format
			Error(2, AName);
			Parsing = false;
			return wxEmptyString;
		/*
		case 1:
			Error(2,Name);
			Opening=false;
			return wxEmptyString;
		*/
		#ifdef _OMB_FORMAT2x
			case bilFFORMAT_2:	// v2
		#endif // _OMB_FORMAT2x
		case bilFFORMAT_301:	// v3
			break;
		default:
			Parsing = false;
			return wxEmptyString;}
		int L,b,BR; // FH: opened file handler
		long OP;									//  L: file or string lenght
											//  b: bugger index
											// BR: buffer lenght
											// OP: read operation identifier
		char *Buffer;
		wxString N,S,V,Al,Dat;  // N: file name
														// S: operation name
														// V: value
														// Al: alarm
														// Dat: last read date
		double cur;  // cur: long pointer for string convertions
		wxDateTime aldate;             // aldate: wxdatetime pointer for string convertions

		#ifdef __WXMSW__
			Dat=::wxDateTime::Today().Format("%d/%m/%Y",wxDateTime::Local);  // Initialisation
		#else
			Dat=::wxDateTime::Today().Format(L"%d/%m/%Y",wxDateTime::Local);  // Initialisation
		#endif // __WXMSW__

		// file copy into the buffer ------------------ //
		wxFile *file = new wxFile(AName, wxFile::read);	// flawfinder: ignore
		if(! file->IsOpened()) {												//
 			// FIXME (igor#1#): insert error handling			//
 			return wxEmptyString;													//
		}																								//
		L = file->SeekEnd(0);														//
		file->Seek(0);																	//
		Buffer=new char[L + 1];													//
		BR = file->Read(Buffer, L); //----------------- //
		b = 6;

		// Check if file has metadata
		if(Buffer[b] == '#'){
			do b++;
			while (Buffer[b] != '#');
			b++;}

		S=wxEmptyString;
		do{
			S += wxChar(Buffer[b]);
			b++;}
		while((Buffer[b]!=char(255))&&b<BR);
		S=Crip(S);

		if(! inputPassword(S)){
					file->Close();
					delete file;
					Parsing = false;
					delete[] Buffer;
					return wxEmptyString;}

		Initialize(false);
		//FileData.Access=S; // Password storage
		FileData.FileName = AName;
		//wxString ext;
		::wxFileName::SplitPath(AName, NULL, NULL, &N, NULL, wxPATH_NATIVE);
		FileData.FileView=N;
		//::wxBeginBusyCursor(wxHOURGLASS_CURSOR);
		b++;
		while(b<BR){
			//UpdateProgress(b*100/BR);
			S=wxEmptyString;
			while(b<BR&&Buffer[b]!=char(255)){
				S += wxChar(Buffer[b]);
				b++;}
			S.Mid(0,1).ToLong(&OP,10);
			L=S.Len();
			S=S.Mid(1,L-1);
			S=Crip(S);
			b++;
			if(OP<4){ // the operation is a value
				V=wxEmptyString;
				do{
					V += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				V=V.Trim();
				V=Crip(V);

				if(!V.ToCDouble(&cur)){
					SubstLocaleDecimalSeparator(V,',');	// NOTE (igor#1#): Old development documents saved using Italian locale --> ','
					V.ToCDouble(&cur);}

				switch(OP){
					case 1:
						if(AddValue(tvFou, -1, S, cur)){
							Tot_Funds += cur;
							NFun++;}
						break;
					case 2:
						if(AddValue(tvCre, -1, S, cur)) NCre++;
						break;
					case 3:
						if(AddValue(tvDeb, -1, S, cur)) NDeb++;}}
			else if(OP==4||OP==5){ // the operation is an object
				V=wxEmptyString;
				do{
					V += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				V=Crip(V);
				b++;
				Al=wxEmptyString;
				do{
					Al += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				Al=Crip(Al);
				aldate=Omb_StrToDate(Al);
				if(OP==4){
					if(AddObject(toPre, -1, S, V, aldate)) NLen++;}
				else{
					if(AddObject(toInP, -1, S, V, aldate)) NBor++;}}
			else if(OP==6){ // the operation is a shopping list item
				Al=wxEmptyString;
				do{
					Al += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				Al=Crip(Al);
				AddShopItem(-1, S, Omb_StrToDate(Al));
				NSho++;}
			else if(OP==7){ // the operation is a date
				V=wxEmptyString;
				do{
					V += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				V=Crip(V);
				Dat=S;
				V.ToDouble(&cur);
				if(AddDate(-1, Omb_StrToDate(S), cur)) NLin++;}
			else if(OP==9)FileData.DefFund=S; // default fund storage
			else{ 														// the operation is a transaction
				wxString Val, Mot, Not = wxEmptyString;
				wxDateTime Tim = Omb_StrToTime(S);
				TOpType Type=toNULL;
				S = wxEmptyString;
				do{
					S += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				if(S==L"A")Type=toGain;
				else if(S==L"B")Type=toExpe;
				else if(S==L"C")Type=toSetCre;
				else if(S==L"D")Type=toRemCre;
				else if(S==L"E")Type=toConCre;
				else if(S==L"F")Type=toSetDeb;
				else if(S==L"G")Type=toRemDeb;
				else if(S==L"H")Type=toConDeb;
				else if(S==L"I")Type=toGetObj;
				else if(S==L"J")Type=toGivObj;
				else if(S==L"K")Type=toLenObj;
				else if(S==L"L")Type=toBakObj;
				else if(S==L"M")Type=toBorObj;
				else if(S==L"N")Type=toRetObj;
				b++;
				S=wxEmptyString;
				do{
					S += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				Val=Crip(S);
				b++;
				S=wxEmptyString;
				do{
					S += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				Mot=Crip(S);
				b++;
				if(Buffer[b]==char(255))goto NoRemark;
				S=wxEmptyString;
				do{
					S += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				Not = iUpperCase(Crip(S));
				NoRemark:
				AddOper_0301(Omb_StrToDate(Dat),Tim,Type,Val,Mot,Not);
				//NLin++;
			}
			b++;}
		wxDateTime LastDate;
		LastDate=Omb_StrToDate(Dat);
		if(!LastDate.IsValid())LastDate=wxDateTime::Today();
		FileData.Year=LastDate.GetYear();
		//FileData.Month=LastDate.GetMonth();
		wxString temp;  // Workaround necessary on Linux
		#ifdef __WXMSW__
			temp=LastDate.Format("%m",wxDateTime::Local).c_str();
		#else
			temp=LastDate.Format(L"%m",wxDateTime::Local).c_str();
		#endif // __WXMSW__
		temp.ToLong(&FileData.Month,10);

		wxFileName *fattrs=new wxFileName(AName);

		file->Close();

		// File attributes
		FileData.DateStamp=fattrs->GetModificationTime();
		if(!fattrs->IsFileWritable())FileData.ReadOnly=true;

		FileData.Modified=false;
		//delete Dialog;

		Parsing = false;
		delete[] Buffer;
		return AName;}
#endif // _OMB_FORMAT30x

#ifdef _OMB_OLDFORMATSAVE_FEATURE
	bool dataconverter::SaveToFile_0301(bool Auto,bool Archive,wxString AFile,wxString F){
		int x;
		if(Auto)if(!FileData.Modified)return true;
		bool Exist=false;
		wxString File,TempFile,fname,ext;
		wxFileName *fattrs;
		if(Auto){
			File=AFile;
			fattrs=new wxFileName(File);}
		else{
			wxFileDialog *Dialog=new wxFileDialog(wxTheApp->GetTopWindow(),wxEmptyString,wxEmptyString,wxEmptyString,wxEmptyString,wxFD_SAVE,wxDefaultPosition,wxDefaultSize,L"Dialog");
			wxString filter=_("Bilancio documents");
			filter+=L"|*.bil;*.BIL";
			Dialog->SetWildcard(filter);
			if(!(Dialog->ShowModal()==wxID_OK)){
				delete Dialog;
				return false;}
			File=Dialog->GetPath();
			fattrs=new wxFileName(File);
			if(::wxFileExists(File)){ // Old file temporary backup
				if(!fattrs->IsFileWritable()){
					Error(41,File);
					delete Dialog;
					return false;}
				int Num=0;
				wxString vol,FilePath,FileName,ext;
				::wxFileName::SplitPath(File,&vol,&FilePath,&FileName,&ext,wxPATH_NATIVE);
				FilePath=vol+FilePath;
				do{
					Num++;
					ext=L".t";
					#ifdef __WXMSW__
						ext+=::wxString::Format("%d",Num);
					#else
						ext+=::wxString::Format(L"%d",Num);
					#endif // __WXMSW__
					TempFile = FilePath + L"/" + FileName + ext;
				}while(::wxFileExists(TempFile));
				::wxRenameFile(File,TempFile,false);
				Exist=true;}
			delete Dialog;}

		/*
		if(FileData.Access.IsEmpty()){
			int Response;
			TPassF *PassF=new TPassF(wxTheApp->GetTopWindow());
			PassF->Pass2->Show(true);
			PassF->ConfirmLab->Show(true);
			do{
				Response=PassF->ShowModal();}
			while(Response==wxID_RETRY);
			switch(Response){
				case wxID_OK:
					FileData.Access=PassF->Pass->GetValue();
					break;
				case wxID_CANCEL:
					return false;
				default:
					Error(5,wxEmptyString); // This condition should never happen
					return false;}
			delete PassF;}
		*/

		// File creation
		int T = NFon + NCre + NDeb + NLen + NBor + NSho + NLin - 7,
				i=0;
		wxFile *f=new wxFile(File,wxFile::write);
		if(! file->IsOpened()) {
 			// FIXME (igor#1#): insert error handling
 			return false;
		}
		f->Write(L"Bil3.0",wxConvISO8859_1);

		// Write file metadata
		wxString metadata;
		f->Write(L"#\n\n");
		metadata = L"OS: ";
		#ifdef __WXGTK__
			metadata += wxGetLinuxDistributionInfo().Description;
		#elif defined ( __WXMSW__)
			metadata += wxGetOsDescription();
		#else
			,etadata += L"unknown";
		#endif // __WXGTK__
		metadata += L"\n\n";
		f->Write(metadata);
		metadata = wxGetLibraryVersionInfo().ToString() + L"\n";
		f->Write(metadata);
		f->Write(L"#");

		Write(f,FileData.Access);  // Password storage

		wxChar cc=wxChar(255);
		wxString be=wxString(cc,1);  // Block end
		f->Write(be,wxConvISO8859_1);

		for(x=0;x<NFon-1;x++){ // Funds storage
			f->Write(L"1",wxConvISO8859_1);
			Write(f,Funds[x].Name);
			f->Write(be,wxConvISO8859_1);
			Write(f, bilFromCDouble(Funds[x].Value));
			f->Write(be,wxConvISO8859_1);
			UpdateProgress(int((float(i))/T*100));
			i++;}
		for(x=0;x<NCre-1;x++){ // Credits storage
			f->Write(L"2",wxConvISO8859_1);
			Write(f,Credits[x].Name);
			f->Write(be,wxConvISO8859_1);
			Write(f, bilFromCDouble(Credits[x].Value));
			f->Write(be,wxConvISO8859_1);
			UpdateProgress(int((float(i))/T*100));
			i++;}
		for(x=0;x<NDeb-1;x++){ // Debts storage
			f->Write(L"3",wxConvISO8859_1);
			Write(f,Debts[x].Name);
			f->Write(be,wxConvISO8859_1);
			Write(f, bilFromCDouble(Debts[x].Value));
			f->Write(be,wxConvISO8859_1);
			UpdateProgress(int((float(i))/T*100));
			i++;}
		for(x=0;x<NLen-1;x++){ // Lent objects storage
			f->Write(L"4",wxConvISO8859_1);
			Write(f,Lent[x].Name);
			f->Write(be,wxConvISO8859_1);
			Write(f,Lent[x].Object);
			f->Write(be,wxConvISO8859_1);
			#ifdef __WXMSW__
				Write(f,Lent[x].Alarm.Format("%d/%m/%Y",wxDateTime::Local));
			#else
				Write(f,Lent[x].Alarm.Format(L"%d/%m/%Y",wxDateTime::Local));
			#endif // __WXMSW__
			f->Write(be,wxConvISO8859_1);
			UpdateProgress(int((float(i))/T*100));
			i++;}
		for(x=0;x<NBor-1;x++){ // Borrowed objects storage
			f->Write(L"5",wxConvISO8859_1);
			Write(f,Borrowed[x].Name);
			f->Write(be,wxConvISO8859_1);
			Write(f,Borrowed[x].Object);
			f->Write(be,wxConvISO8859_1);
			#ifdef __WXMSW__
				Write(f,Borrowed[x].Alarm.Format("%d/%m/%Y",wxDateTime::Local));
			#else
				Write(f,Borrowed[x].Alarm.Format(L"%d/%m/%Y",wxDateTime::Local));
			#endif // __WXMSW__
			f->Write(be,wxConvISO8859_1);
			UpdateProgress(int((float(i))/T*100));
			i++;}
		if(!Archive)for(x=0;x<NSho-1;x++){ // Shopping List storage
			f->Write(L"6",wxConvISO8859_1);
			Write(f,ShopItems[x].Name);
			f->Write(be,wxConvISO8859_1);
			#ifdef __WXMSW__
				Write(f,ShopItems[x].Alarm.Format("%d/%m/%Y",wxDateTime::Local));
			#else
				Write(f,ShopItems[x].Alarm.Format(L"%d/%m/%Y",wxDateTime::Local));
			#endif // __WXMSW__
			f->Write(be,wxConvISO8859_1);
			UpdateProgress(int((float(i))/T*100));
			i++;}
		for(x=0;x<NLin-1;x++){ // Report storage
			if(Lines[x].IsDate){ // Total storage
				f->Write(L"7",wxConvISO8859_1);
				#ifdef __WXMSW__
					Write(f,Lines[x].Date.Format("%d/%m/%Y",wxDateTime::Local));
				#else
					Write(f,Lines[x].Date.Format(L"%d/%m/%Y",wxDateTime::Local));
				#endif // __WXMSW__
				f->Write(be,wxConvISO8859_1);
				Write(f,Lines[x].Value);
				f->Write(be,wxConvISO8859_1);
				UpdateProgress(int((float(i+x))/T*100));}
			else{  // Operation storage
				f->Write(L"8",wxConvISO8859_1);
				Write(f,Lines[x].Time.FormatTime());
				f->Write(be,wxConvISO8859_1);
				if(Lines[x].Type==toGain)f->Write(L"A",wxConvISO8859_1);
				else if(Lines[x].Type==toExpe)f->Write(L"B",wxConvISO8859_1);
				else if(Lines[x].Type==toSetCre)f->Write(L"C",wxConvISO8859_1);
				else if(Lines[x].Type==toRemCre)f->Write(L"D",wxConvISO8859_1);
				else if(Lines[x].Type==toConCre)f->Write(L"E",wxConvISO8859_1);
				else if(Lines[x].Type==toSetDeb)f->Write(L"F",wxConvISO8859_1);
				else if(Lines[x].Type==toRemDeb)f->Write(L"G",wxConvISO8859_1);
				else if(Lines[x].Type==toConDeb)f->Write(L"H",wxConvISO8859_1);
				else if(Lines[x].Type==toGetObj)f->Write(L"I",wxConvISO8859_1);
				else if(Lines[x].Type==toGivObj)f->Write(L"J",wxConvISO8859_1);
				else if(Lines[x].Type==toLenObj)f->Write(L"K",wxConvISO8859_1);
				else if(Lines[x].Type==toBakObj)f->Write(L"L",wxConvISO8859_1);
				else if(Lines[x].Type==toBorObj)f->Write(L"M",wxConvISO8859_1);
				else if(Lines[x].Type==toRetObj)f->Write(L"N",wxConvISO8859_1);
				f->Write(be,wxConvISO8859_1);
				Write(f,Lines[x].Value);
				f->Write(be,wxConvISO8859_1);
				Write(f,Lines[x].Reason);
				f->Write(be,wxConvISO8859_1);
				Write(f,Lines[x].Remark);
				f->Write(be,wxConvISO8859_1);
				UpdateProgress(int((float(i+x))/T*100));}}
		f->Write(L"9",wxConvISO8859_1);  // Default fund storage
		if(!FileData.DefFund.IsEmpty())Write(f,FileData.DefFund);
		else		Write(f,_("Default"));
		f->Write(be,wxConvISO8859_1);
		f->Close();
		if(!FileData.DateStamp.IsValid())fattrs->Touch();

		if(Archive) fattrs->SetPermissions(wxPOSIX_USER_READ | wxPOSIX_GROUP_READ);

		delete f;
		if(!Auto){
			F=File;
			FileData.FileName=File;
			wxString Tit;
			::wxFileName::SplitPath(File,NULL,NULL,&Tit,NULL,wxPATH_NATIVE);
			//Tit=Tit.Mid(1,Tit.Len()-4);
			FileData.FileView=Tit;}
		if(FileData.ReadOnly)FileData.ReadOnly=false;
		FileData.Modified=false;
		if(Exist)if(::wxFileExists(TempFile))::wxRemoveFile(TempFile);
		UpdateProgress(100);
		UpdateProgress(0);
		return true;}
#endif // _OMB_OLDFORMATSAVE_FEATURE

#ifdef _OMB_FORMAT30x
	bool dataconverter::AddOper_0301(wxDateTime D, wxDateTime O, TOpType T, wxString V, wxString M, wxString N){
		// Last date check
		if(T==toNULL||V.IsEmpty()||M.IsEmpty()){
			return false;}
		bool found = false;
		if(NLin < 1) AddDate(-1, D, Tot_Funds);
		else for(int i = NLin - 1; i >= 0; i--)if(Lines[i].IsDate){
			if( Lines[i].Date.IsSameDate(D)){
				found = true;
				break;}}
		if (! found) AddDate(-1, D, Tot_Funds);
		Lines[NLin].IsDate=false;
		Lines[NLin].Date=D;
		Lines[NLin].Time=O;
		Lines[NLin].Type=T;
		Lines[NLin].Value=V;
		Lines[NLin].Reason=M;
		Lines[NLin].Remark0301 = N;
		NLin++;

		if(! Parsing)
		//Sort();
		FileData.Modified=true;
		return true;}
#endif // _OMB_FORMAT30x

#ifdef _OMB_FORMAT30x
	wxString dataconverter::LoadFromFile_0302(wxString AName, bool App){
		Parsing = true;
		if(!::wxFileExists(AName)){
			//delete Dialog;
			Parsing = false;
			return wxEmptyString;}
		wxString Proc;
		switch(CheckFileFormat(AName)){
			case bilFFORMAT_UNKNOWN:	// Unknown Format
				Error(2, AName);
				Parsing = false;
				return wxEmptyString;
			/*
			case 1:
				Error(2,Name);
				Opening=false;
				return wxEmptyString;
			*/
			case bilFFORMAT_302:
				break;
			default:
				Parsing = false;
				return wxEmptyString;}
		int L,b,BR; // FH: opened file handler
		long CI;	// Category index
		long OP;									//  L: file or string lenght
											//  b: bugger index
											// BR: buffer lenght
											// OP: read operation identifier
		char *Buffer;
		wxString N,S,V,Al,Dat;  // N: file name
														// S: operation name
														// V: value
														// Al: alarm
														// Dat: last read date
		double cur;  // cur: long pointer for string convertions
		wxDateTime aldate;             // aldate: wxdatetime pointer for string convertions

		#ifdef __WXMSW__
			Dat=::wxDateTime::Today().Format("%d/%m/%Y",wxDateTime::Local);  // Initialization
		#else
			Dat=::wxDateTime::Today().Format(L"%d/%m/%Y",wxDateTime::Local);  // Initialization
		#endif // __WXMSW__

		// file copy into the buffer ------------------ //
		wxFile *file = new wxFile(AName, wxFile::read);	// flawfinder: ignore
		if(! file->IsOpened()) {												//
 			// FIXME (igor#1#): insert error handling			//
 			return wxEmptyString;													//
		}																								//
		L = file->SeekEnd(0);														//
		file->Seek(0);																	//
		Buffer=new char[L + 1];													//
		BR = file->Read(Buffer, L); //----------------- //
		b = 6;

		// Check if file has metadata
		if(Buffer[b] == '#'){
			do b++;
			while (Buffer[b] != '#');
			b++;}

		S=wxEmptyString;
		do{
			S += wxChar(Buffer[b]);
			b++;}
		while((Buffer[b]!=char(255))&&b<BR);
		S=Crip(S);

		if(! inputPassword(S)){
					file->Close();
					delete file;
					Parsing = false;
					delete[] Buffer;
					return wxEmptyString;}

		Initialize(false);
		//FileData.Access=S; // Password storage
		FileData.FileName = AName;
		::wxFileName::SplitPath(AName, NULL, NULL, &N, NULL, wxPATH_NATIVE);
		FileData.FileView=N;
		//::wxBeginBusyCursor(wxHOURGLASS_CURSOR);
		b++;
		while(b<BR){
			//UpdateProgress(b*100/BR);
			S=wxEmptyString;
			while(b<BR&&Buffer[b]!=char(255)){
				S += wxChar(Buffer[b]);
				b++;}
			S.Mid(0,2).ToLong(&OP,10);
			L = S.Len();
			S = S.Mid(2, L - 2);
			S = Crip(S);
			b++;
			if(OP<4){ // the operation is a value
				V=wxEmptyString;
				do{
					V += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				V=V.Trim();
				V=Crip(V);

				if(!V.ToCDouble(&cur)){
					SubstLocaleDecimalSeparator(V,',');	// NOTE (igor#1#): Old developement documents saved using Italian locale --> ','
					V.ToCDouble(&cur);}

				switch(OP){
					case 1:
						if(AddValue(tvFou, -1, S, cur)){
							Tot_Funds += cur;
							NFun++;}
						break;
					case 2:
						if(AddValue(tvCre, -1, S, cur)) NCre++;
						break;
					case 3:
						if(AddValue(tvDeb, -1, S,cur)) NDeb++;}}
			else if(OP==4||OP==5){ // the operation is an object
				V=wxEmptyString;
				do{
					V += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				V=Crip(V);
				b++;
				Al=wxEmptyString;
				do{
					Al += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				Al=Crip(Al);
				aldate=Omb_StrToDate(Al);
				if(OP == 4){
					if(AddObject(toPre, -1, S, V, aldate)) NLen++;}
				else{
					if(AddObject(toInP, -1, S, V, aldate)) NBor++;}}
			else if(OP==6){ // the operation is a shopping list item
				Al=wxEmptyString;
				do{
					Al += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				Al=Crip(Al);
				AddShopItem(-1, S, Omb_StrToDate(Al));
				NSho++;}
			else if(OP==7){ // the operation is a date
				V=wxEmptyString;
				do{
					V += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				V=Crip(V);
				Dat=S;
				V.ToDouble(&cur);
				if(AddDate(-1, Omb_StrToDate(S), cur)) NLin++;}
			else if(OP==9) FileData.DefFund=S; // default fund storage

			else if(OP == 10){
				bool exists = false;
				for(int i = 0; i < int (CategoryDB->Count()); i++){
					if(S == CategoryDB->Item(i)){
						exists = true;
						break;
					}
				}

				if(! exists){
					CategoryDB->Add(S);
					NCat++;}

				b--;}

			else{ 														// the operation is a transaction
				wxString Val, Mot, Not = wxEmptyString;
				wxDateTime Tim = Omb_StrToTime(S);
				TOpType Type = toNULL;
				S = wxEmptyString;
				do{
					S += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				if(S==L"A")Type=toGain;
				else if(S==L"B")Type=toExpe;
				else if(S==L"C")Type=toSetCre;
				else if(S==L"D")Type=toRemCre;
				else if(S==L"E")Type=toConCre;
				else if(S==L"F")Type=toSetDeb;
				else if(S==L"G")Type=toRemDeb;
				else if(S==L"H")Type=toConDeb;
				else if(S==L"I")Type=toGetObj;
				else if(S==L"J")Type=toGivObj;
				else if(S==L"K")Type=toLenObj;
				else if(S==L"L")Type=toBakObj;
				else if(S==L"M")Type=toBorObj;
				else if(S==L"N")Type=toRetObj;
				b++;
				S=wxEmptyString;
				do{
					S += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				Val=Crip(S);
				b++;
				S=wxEmptyString;
				do{
					S += wxChar(Buffer[b]);
					b++;}
				while(Buffer[b]!=char(255));
				Mot=Crip(S);
				b++;
				if(Buffer[b] !=char(255)){
					S=wxEmptyString;
					do{
						S += wxChar(Buffer[b]);
						b++;}
					while(Buffer[b]!=char(255));
					Not=Crip(S);
					CI = -1;
					Not.ToLong(&CI);}
				AddOper(-1, Omb_StrToDate(Dat), Tim, Type, Val, Mot, CI);
				NLin++;}
			b++;}
		wxDateTime LastDate;
		LastDate=Omb_StrToDate(Dat);
		if(!LastDate.IsValid())LastDate=wxDateTime::Today();
		FileData.Year=LastDate.GetYear();
		//FileData.Month=LastDate.GetMonth();
		wxString temp;  // Workaround necessary on Linux
		#ifdef __WXMSW__
			temp=LastDate.Format("%m",wxDateTime::Local).c_str();
		#else
			temp=LastDate.Format(L"%m",wxDateTime::Local).c_str();
		#endif // __WXMSW__
		temp.ToLong(&FileData.Month,10);

		wxFileName *fattrs=new wxFileName(AName);

		file->Close();

		// File attributes
		FileData.DateStamp=fattrs->GetModificationTime();
		if(!fattrs->IsFileWritable())FileData.ReadOnly=true;

		FileData.Modified=false;
		//delete Dialog;
		/*Opening*/Parsing = false;
		delete[] Buffer;
		/*
		UpdateProgress(100);
		UpdateProgress(0);
		::wxEndBusyCursor();
		*/
		return AName;}
#endif // _OMB_FORMAT30x

#ifdef _OMB_OLDFORMATSAVE_FEATURE
	bool dataconverter::SaveToFile_0302(bool Auto, bool Archive, wxString AFile){
		int x;
		if(Auto)if(!FileData.Modified)return true;
		#ifndef __OMBCONVERT_BIN__
			bool Exist = false;
		#endif // __OMBCONVERT_BIN__
		wxString File,TempFile,fname,ext;
		wxFileName *fattrs;
		if(Auto){
			File=AFile;
			fattrs=new wxFileName(File);}
		#ifndef __OMBCONVERT_BIN__
		else{
			wxFileDialog *Dialog=new wxFileDialog(wxTheApp->GetTopWindow(),wxEmptyString,wxEmptyString,wxEmptyString,wxEmptyString,wxFD_SAVE,wxDefaultPosition,wxDefaultSize,L"Dialog");
			wxString Path;	// document path
			wxString filter=_("Bilancio documents");
			filter+=L"|*.bil;*.BIL";
			Dialog->SetWildcard(filter);
			if(FileData.FileName.IsEmpty()){
				Path = GetOSDocDir();}
			else wxFileName::SplitPath(FileData.FileName, &Path, NULL, NULL, wxPATH_NATIVE);
			Dialog->SetDirectory(Path);
			if(!(Dialog->ShowModal() == wxID_OK)){
				delete Dialog;
				return false;}
			File=Dialog->GetPath();
			fattrs=new wxFileName(File);
			if(::wxFileExists(File)){ // Old file temporary backup
				if(!fattrs->IsFileWritable()){
					Error(41,File);
					delete Dialog;
					return false;}
				int Num=0;
				wxString vol, FilePath, FileName, ext;
				::wxFileName::SplitPath(File, &vol, &FilePath, &FileName, &ext, wxPATH_NATIVE);
				FilePath = vol + FilePath;
				do{
					Num++;
					ext=L".t";
					#ifdef __WXMSW__
						ext+=::wxString::Format("%d",Num);
					#else
						ext+=::wxString::Format(L"%d",Num);
					#endif // __WXMSW__
					TempFile = FilePath + L"/" + FileName + ext;
				}while(::wxFileExists(TempFile));
				::wxRenameFile(File, TempFile, false);
				Exist=true;}
			delete Dialog;}

			// TODO (igor#1#): Implement error notification in ombconvert (the condition should never happen)
			if(FileData.Access.IsEmpty()){
				int Response;
				TPassF *PassF=new TPassF(wxTheApp->GetTopWindow());
				PassF->Pass2->Show(true);
				PassF->ConfirmLab->Show(true);
				do{
					Response=PassF->ShowModal();}
				while(Response==wxID_RETRY);
				switch(Response){
					case wxID_OK:
						FileData.Access=PassF->Pass->GetValue();
						break;
					case wxID_CANCEL:
						return false;
					default:
						Error(5,wxEmptyString); // This condition should never happen
						return false;}
				delete PassF;}
		#endif // __OMBCONVERT_BIN__

		// File creation
		#ifndef __OMBCONVERT_BIN__
			int T = NFon + NCre + NDeb + NLen + NBor + NSho + NLin - 7 + CategoryDB->Count();
		#endif // __OMBCONVERT_BIN__
		int i=0;

		wxFile *f=new wxFile(File,wxFile::write);
		if(! file->IsOpened()) {
 			return false;
		}
		f->Write(L"Bil302", wxConvISO8859_1);

		// Write file metadata
		wxString metadata;
		f->Write(L"#\n\n");
		metadata = L"OS: ";
		#ifdef __WXGTK__
			metadata += wxGetLinuxDistributionInfo().Description;
		#elif defined ( __WXMSW__)
			metadata += wxGetOsDescription();
		#else
			,etadata += L"unknown";
		#endif // __WXGTK__
		metadata += L"\n\n";
		f->Write(metadata);
		#ifndef __OMBCONVERT_BIN__
			metadata = wxGetLibraryVersionInfo().ToString() + L"\n";
		#else
			metadata = _("Converted with ombconvert\n\n");
		#endif // __OMBCONVERT_BIN__
		f->Write(metadata);
		f->Write(L"#");

		Write(f,FileData.Access);  // Password storage

		wxChar cc=wxChar(255);
		wxString be=wxString(cc,1);  // Block end
		f->Write(be,wxConvISO8859_1);

		for(x=0;x<NFon-1;x++){ // Funds storage
			f->Write(L"01", wxConvISO8859_1);
			Write(f,Funds[x].Name);
			f->Write(be,wxConvISO8859_1);
			Write(f, bilFromCDouble(Funds[x].Value));
			f->Write(be,wxConvISO8859_1);
			#ifndef __OMBCONVERT_BIN__
				UpdateProgress(int((float(i))/T*100));
			#endif // __OMBCONVERT_BIN__
			i++;}
		for(x=0;x<NCre-1;x++){ // Credits storage
			f->Write(L"02", wxConvISO8859_1);
			Write(f,Credits[x].Name);
			f->Write(be,wxConvISO8859_1);
			Write(f, bilFromCDouble(Credits[x].Value));
			f->Write(be,wxConvISO8859_1);
			#ifndef __OMBCONVERT_BIN__
				UpdateProgress(int((float(i))/T*100));
			#endif // __OMBCONVERT_BIN__
			i++;}
		for(x=0;x<NDeb-1;x++){ // Debts storage
			f->Write(L"03", wxConvISO8859_1);
			Write(f,Debts[x].Name);
			f->Write(be,wxConvISO8859_1);
			Write(f, bilFromCDouble(Debts[x].Value));
			f->Write(be,wxConvISO8859_1);
			#ifndef __OMBCONVERT_BIN__
				UpdateProgress(int((float(i))/T*100));
			#endif // __OMBCONVERT_BIN__
			i++;}
		for(x=0;x<NLen-1;x++){ // Lent objects storage
			f->Write(L"04", wxConvISO8859_1);
			Write(f,Lent[x].Name);
			f->Write(be,wxConvISO8859_1);
			Write(f,Lent[x].Object);
			f->Write(be,wxConvISO8859_1);
			#ifdef __WXMSW__
				Write(f,Lent[x].Alarm.Format("%d/%m/%Y",wxDateTime::Local));
			#else
				Write(f,Lent[x].Alarm.Format(L"%d/%m/%Y",wxDateTime::Local));
			#endif // __WXMSW__
			f->Write(be,wxConvISO8859_1);
			#ifndef __OMBCONVERT_BIN__
				UpdateProgress(int((float(i))/T*100));
			#endif // __OMBCONVERT_BIN__
			i++;}
		for(x=0;x<NBor-1;x++){ // Borrowed objects storage
			f->Write(L"05", wxConvISO8859_1);
			Write(f,Borrowed[x].Name);
			f->Write(be,wxConvISO8859_1);
			Write(f,Borrowed[x].Object);
			f->Write(be,wxConvISO8859_1);
			#ifdef __WXMSW__
				Write(f,Borrowed[x].Alarm.Format("%d/%m/%Y",wxDateTime::Local));
			#else
				Write(f,Borrowed[x].Alarm.Format(L"%d/%m/%Y",wxDateTime::Local));
			#endif // __WXMSW__
			f->Write(be,wxConvISO8859_1);
			#ifndef __OMBCONVERT_BIN__
				UpdateProgress(int((float(i))/T*100));
			#endif // __OMBCONVERT_BIN__
			i++;}
		if(!Archive)for(x=0;x<NSho-1;x++){ // Shopping List storage
			f->Write(L"06", wxConvISO8859_1);
			Write(f,ShopItems[x].Name);
			f->Write(be,wxConvISO8859_1);
			#ifdef __WXMSW__
				Write(f,ShopItems[x].Alarm.Format("%d/%m/%Y",wxDateTime::Local));
			#else
				Write(f,ShopItems[x].Alarm.Format(L"%d/%m/%Y",wxDateTime::Local));
			#endif // __WXMSW__
			f->Write(be,wxConvISO8859_1);
			#ifndef __OMBCONVERT_BIN__
				UpdateProgress(int((float(i))/T*100));
			#endif // __OMBCONVERT_BIN__
			i++;}

		for(x = 0; x < (int)CategoryDB->Count(); x++){ // Category database storage
			f->Write(L"10", wxConvISO8859_1);
			Write(f, CategoryDB->Item(x));
			f->Write(be,wxConvISO8859_1);
			#ifndef __OMBCONVERT_BIN__
				UpdateProgress(int((float(i))/T*100));
				i++;
			#endif // __OMBCONVERT_BIN__
		}

		for(x=0;x<NLin-1;x++){ // Report storage
			if(IsDate(x)){ // Total storage
				f->Write(L"07", wxConvISO8859_1);
				#ifdef __WXMSW__
					Write(f,Lines[x].Date.Format("%d/%m/%Y",wxDateTime::Local));
				#else
					Write(f,Lines[x].Date.Format(L"%d/%m/%Y",wxDateTime::Local));
				#endif // __WXMSW__
				f->Write(be,wxConvISO8859_1);
				Write(f,Lines[x].Value);
				f->Write(be,wxConvISO8859_1);
				#ifndef __OMBCONVERT_BIN__
					UpdateProgress(int((float(i+x))/T*100));
				#endif // __OMBCONVERT_BIN__
			}
			else{  // Operation storage
				f->Write(L"08", wxConvISO8859_1);
				Write(f,Lines[x].Time.FormatISOTime());
				f->Write(be,wxConvISO8859_1);
				if(Lines[x].Type==toGain)f->Write(L"A",wxConvISO8859_1);
				else if(Lines[x].Type==toExpe)f->Write(L"B",wxConvISO8859_1);
				else if(Lines[x].Type==toSetCre)f->Write(L"C",wxConvISO8859_1);
				else if(Lines[x].Type==toRemCre)f->Write(L"D",wxConvISO8859_1);
				else if(Lines[x].Type==toConCre)f->Write(L"E",wxConvISO8859_1);
				else if(Lines[x].Type==toSetDeb)f->Write(L"F",wxConvISO8859_1);
				else if(Lines[x].Type==toRemDeb)f->Write(L"G",wxConvISO8859_1);
				else if(Lines[x].Type==toConDeb)f->Write(L"H",wxConvISO8859_1);
				else if(Lines[x].Type==toGetObj)f->Write(L"I",wxConvISO8859_1);
				else if(Lines[x].Type==toGivObj)f->Write(L"J",wxConvISO8859_1);
				else if(Lines[x].Type==toLenObj)f->Write(L"K",wxConvISO8859_1);
				else if(Lines[x].Type==toBakObj)f->Write(L"L",wxConvISO8859_1);
				else if(Lines[x].Type==toBorObj)f->Write(L"M",wxConvISO8859_1);
				else if(Lines[x].Type==toRetObj)f->Write(L"N",wxConvISO8859_1);
				f->Write(be,wxConvISO8859_1);
				Write(f,Lines[x].Value);
				f->Write(be,wxConvISO8859_1);
				Write(f,Lines[x].Reason);
				f->Write(be,wxConvISO8859_1);
				Write(f, ::wxString::Format("%d", (int) Lines[x].CategoryIndex));
				f->Write(be,wxConvISO8859_1);
				#ifndef __OMBCONVERT_BIN__
					UpdateProgress(int((float(i+x))/T*100));
				#endif // __OMBCONVERT_BIN__
			}}
		f->Write(L"09", wxConvISO8859_1);  // Default fund storage
		if(!FileData.DefFund.IsEmpty())Write(f,FileData.DefFund);
		else		Write(f,_("Default"));
		f->Write(be,wxConvISO8859_1);
		f->Close();
		if(!FileData.DateStamp.IsValid())fattrs->Touch();

		if(Archive) fattrs->SetPermissions(wxPOSIX_USER_READ | wxPOSIX_GROUP_READ);

		delete f;
		if(!Auto){
			//F=File;
			FileData.FileName=File;
			wxString Tit;
			::wxFileName::SplitPath(File,NULL,NULL,&Tit,NULL,wxPATH_NATIVE);
			//Tit=Tit.Mid(1,Tit.Len()-4);
			FileData.FileView=Tit;}
		if(FileData.ReadOnly)FileData.ReadOnly=false;
		FileData.Modified=false;
		#ifndef __OMBCONVERT_BIN__
			if(Exist) if(::wxFileExists(TempFile))::wxRemoveFile(TempFile);
			UpdateProgress(100);
			UpdateProgress(0);
		#endif // __OMBCONVERT_BIN__
		return true;}
#endif // _OMB_OLDFORMATSAVE_FEATURE

void dataconverter::BuildDatabase(int num_funds, int num_credits, int num_debits, int num_loans, int num_borrows, int num_transactions,
																		 int num_categories, int num_shoplists, const wxString& defaultFund){
	int i;
	double cur;

	// Write Funds
	#ifdef _OMB_USE_CIPHER
		if(! TableExists(L"Funds", wxEmptyString, database))
      #ifdef __OPENSUSE__
        ExecuteUpdate(database, cs_funds.c_str());
      #else
        ExecuteUpdate(database, cs_funds);
      #endif // __OPENSUSE__
	#else
		if(! database->TableExists(L"Funds")) database->ExecuteUpdate(cs_funds);
	#endif // _OMB_USE_CIPHER
	for(i = 0; i < num_funds; i++)
		#ifdef _OMB_USE_CIPHER
		{
			wxString update = L"insert into Funds values (NULL, '" +
																Funds[i].Name +
																L"', " +
																wxString::FromCDouble(Funds[i].Value, 2) +
																L");";
      #ifdef __OPENSUSE__
        ExecuteUpdate(database, update.c_str());
      #else
        ExecuteUpdate(database, update);
      #endif // __OPENSUSE__
		}
		#else
			database->ExecuteUpdate(L"insert into Funds values (NULL, '" +
																Funds[i].Name +
																L"', " +
																wxString::FromCDouble(Funds[i].Value, 2) +
																L");");
		#endif // _OMB_USE_CIPHER

	// Write default fund
	SetDefaultFund(defaultFund);


	// Write Credits
	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      if(! TableExists(L"Credits", wxEmptyString, database)) ExecuteUpdate(database, cs_credits.c_str());
    #else
      if(! TableExists(L"Credits", wxEmptyString, database)) ExecuteUpdate(database, cs_credits);
    #endif // __OPENSUSE__
	#else
		if(! database->TableExists(L"Credits")) database->ExecuteUpdate(cs_credits);
	#endif // _OMB_USE_CIPHER
	for(i = 0; i < num_credits; i++)
		#ifdef _OMB_USE_CIPHER
			{
				wxString update = L"insert into Credits values (NULL, '" +
																Credits[i].Name +
																L"', " +
																wxString::FromCDouble(Credits[i].Value, 2) +
																L");";
        #ifdef __OPENSUSE__
          ExecuteUpdate(database, update.c_str());
        #else
          ExecuteUpdate(database, update);
        #endif // __OPENSUSE__
			}
		#else
			database->ExecuteUpdate(L"insert into Credits values (NULL, '" +
																Credits[i].Name +
																L"', " +
																wxString::FromCDouble(Credits[i].Value, 2) +
																L");");
		#endif // _OMB_USE_CIPHER

	// Write Debts
	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      if(! TableExists(L"Debts", wxEmptyString, database)) ExecuteUpdate(database, cs_debts.c_str());
    #else
      if(! TableExists(L"Debts", wxEmptyString, database)) ExecuteUpdate(database, cs_debts);
    #endif // __OPENSUSE__
	#else
		if(! database->TableExists(L"Debts")) database->ExecuteUpdate(cs_debts);
	#endif // _OMB_USE_CIPHER

	for(i = 0; i < num_debits; i++)
		#ifdef _OMB_USE_CIPHER
		{
			wxString update = L"insert into Debts values (NULL, '" +
																Debts[i].Name +
																L"', " +
																wxString::FromCDouble(Debts[i].Value, 2) +
																L");";
			#ifdef __OPENSUSE__
        ExecuteUpdate(database, update.c_str());
			#else
        ExecuteUpdate(database, update);
      #endif // __OPENSUSE__
		}
		#else
			database->ExecuteUpdate(L"insert into Debts values (NULL, '" +
																Debts[i].Name +
																L"', " +
																wxString::FromCDouble(Debts[i].Value, 2) +
																L");");
		#endif // _OMB_USE_CIPHER

	// Write Loans
	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      if(! TableExists(L"Loans", wxEmptyString, database)) ExecuteUpdate(database, cs_loans.c_str());
    #else
      if(! TableExists(L"Loans", wxEmptyString, database)) ExecuteUpdate(database, cs_loans);
    #endif // __OPENSUSE__
	#else
		if(! database->TableExists(L"Loans")) database->ExecuteUpdate(cs_loans);
	#endif // _OMB_USE_CIPHER
	for(i = 0; i < num_loans; i++)
		#ifdef _OMB_USE_CIPHER
			{
				wxString update = L"insert into Loans values (NULL, '" +
																Lent[i].Name +
																L"', '" +
																Lent[i].Object +
																L"', " +
																wxString::Format(L"%d", (int) Lent[i].Alarm.GetTicks()) +
																L");";
        #ifdef __OPENSUSE__
          ExecuteUpdate(database, update.c_str());
        #else
          ExecuteUpdate(database, update);
        #endif // __OPENSUSE__
			}
		#else
			database->ExecuteUpdate(L"insert into Loans values (NULL, '" +
																Lent[i].Name +
																L"', '" +
																Lent[i].Object +
																L"', " +
																wxString::Format(L"%d", (int) Lent[i].Alarm.GetTicks()) +
																L");");
		#endif // _OMB_USE_CIPHER

	// Write Borrows
	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      if(! TableExists(L"Borrows", wxEmptyString, database)) ExecuteUpdate(database, cs_borrows.c_str());
    #else
      if(! TableExists(L"Borrows", wxEmptyString, database)) ExecuteUpdate(database, cs_borrows);
    #endif // __OPENSUSE__
	#else
		if(! database->TableExists(L"Borrows")) database->ExecuteUpdate(cs_borrows);
	#endif // _OMB_USE_CIPHER
	for(i = 0; i < num_borrows; i++)
		#ifdef _OMB_USE_CIPHER
			{
				wxString update = L"insert into Borrows values (NULL, '" +
																Borrowed[i].Name +
																L"', '" +
																Borrowed[i].Object +
																L"', " +
																wxString::Format(L"%d", (int) Borrowed[i].Alarm.GetTicks()) +
																L");";
				#ifdef __OPENSUSE__
          ExecuteUpdate(database, update.c_str());
				#else
          ExecuteUpdate(database, update);
        #endif // __OPENSUSE__
			}
		#else
			database->ExecuteUpdate(L"insert into Borrows values (NULL, '" +
																Borrowed[i].Name +
																L"', '" +
																Borrowed[i].Object +
																L"', " +
																wxString::Format(L"%d", (int) Borrowed[i].Alarm.GetTicks()) +
																L");");
		#endif // _OMB_USE_CIPHER

	// Write Categories
	for(i = 0; i < num_categories; i++){
		CategoryDB->Item(i) = DoubleQuote(CategoryDB->Item(i));
		#ifdef _OMB_USE_CIPHER
			wxString Sql = L"insert into Categories values (NULL, '" +
																CategoryDB->Item(i) +
																L"', 1);";
			#ifdef __OPENSUSE__
        ExecuteUpdate(database, Sql.c_str());
			#else
        ExecuteUpdate(database, Sql);
      #endif // __OPENSUSE__
		#else
			database->ExecuteUpdate(L"insert into Categories values (NULL, '" +
																CategoryDB->Item(i) +
																L"', 1);");
		#endif // _OMB_USE_CIPHER
	}

	// Write Transactions
	wxString val;
	for(i = 0; i < num_transactions; i++){
		val = Lines[i].Value;
		if((int) Lines[i].Type < 9){
			val.ToDouble(&cur);
			val = ::wxString::FromCDouble(cur, 2);
		}
		else val = DoubleQuote(val);

		Lines[i].Reason = DoubleQuote(Lines[i].Reason);

		#ifdef _OMB_USE_CIPHER
			wxString Sql = L"insert into Transactions values (NULL, " +
																			wxString::Format(L"%d", Lines[i].IsDate) +
																			L", " +
																			wxString::Format(L"%d", (int) Lines[i].Date.GetTicks()) +
																			L", " +
																			wxString::Format(L"%d", (int) Lines[i].Time.GetTicks()) +
																			L", " +
																			wxString::Format(L"%d", Lines[i].Type) +
																			L", '" +
																			val +
																			L"', '" +
																			Lines[i].Reason +
																			L"', " +
																			wxString::Format(L"%ld", Lines[i].CategoryIndex + 1) +
																			L");";
			#ifdef __OPENSUSE__
        ExecuteUpdate(database, Sql.c_str());
			#else
        ExecuteUpdate(database, Sql);
      #endif // __OPENSUSE__
		#else
			database->ExecuteUpdate(L"insert into Transactions values (NULL, " +
																			wxString::Format(L"%d", Lines[i].IsDate) +
																			L", " +
																			wxString::Format(L"%d", (int) Lines[i].Date.GetTicks()) +
																			L", " +
																			wxString::Format(L"%d", (int) Lines[i].Time.GetTicks()) +
																			L", " +
																			wxString::Format(L"%d", Lines[i].Type) +
																			L", '" +
																			val +
																			L"', '" +
																			Lines[i].Reason +
																			L"', " +
																			wxString::Format(L"%ld", Lines[i].CategoryIndex + 1) +
																			L");");
		#endif // _OMB_USE_CIPHER
	}

	// Write shopping list
	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      if(! TableExists(L"Shoplist", wxEmptyString, database)) ExecuteUpdate(database, cs_shoplist.c_str());
    #else
      if(! TableExists(L"Shoplist", wxEmptyString, database)) ExecuteUpdate(database, cs_shoplist);
    #endif // __OPENSUSE__
	#else
		if(! database->TableExists(L"Shoplist")) database->ExecuteUpdate(cs_shoplist);
	#endif // _OMB_USE_CIPHER
	for(i = 0; i < num_shoplists; i++)
		#ifdef _OMB_USE_CIPHER
			{
				wxString Sql = L"insert into Shoplist values (NULL, '" +
																ShopItems[i].Name +
																L"', " +
																wxString::Format(L"%d", (int) ShopItems[i].Alarm.GetTicks()) +
																L");";
				#ifdef __OPENSUSE__
          ExecuteUpdate(database, Sql.c_str());
				#else
          ExecuteUpdate(database, Sql);
        #endif // __OPENSUSE__
			}
		#else
			database->ExecuteUpdate(L"insert into Shoplist values (NULL, '" +
																ShopItems[i].Name +
																L"', " +
																wxString::Format(L"%d", (int) ShopItems[i].Alarm.GetTicks()) +
																L");");
		#endif // _OMB_USE_CIPHER

	#ifdef _OMB_USE_CIPHER
		sqlite3_exec(database, "RELEASE rollback;", NULL, NULL, NULL);
		sqlite3_close(database);
	#else
		database->ReleaseSavepoint(L"rollback");
		database->Close();
	#endif // _OMB_USE_CIPHER
}

#ifdef _OMB_FORMAT30x
	wxDateTime dataconverter::Omb_StrToTime(wxString S){
		wxDateTime time;
		#ifdef __WXMSW__
			time.ParseFormat(S,"%H:%M:%S",wxDefaultDateTime);
		#else
			time.ParseFormat(S,L"%H:%M:%S",wxDefaultDateTime);
		#endif
		if(time.IsValid())return time;
		return double(NULL);	}
#endif // _OMB_FORMAT30x

#ifdef _OMB_FORMAT30x
	bool dataconverter::SubstLocaleDecimalSeparator(wxString &S,char ToSub){
	int Pos;
	Pos=S.Find(ToSub,false);
	if(Pos==wxNOT_FOUND||Pos==-1)return false;
	else{
		S.Replace(ToSub, '.', true);
		return true;}}
#endif // _OMB_FORMAT30x

/*
wxString dataconverter::DoubleQuote(wxString S){
	if(S.Find('\'') != wxNOT_FOUND) S.Replace(L"'", L"''", true);
	return S;}
*/

#ifdef _OMB_FORMAT30x
	wxString dataconverter::Crip(wxString S){
		int L=S.Len();
		#ifdef __i686__
			wxChar C;
			const wchar_t *buffer = new wchar_t[L];
			buffer = S.wc_str();
		#else
			long letter;
			wxUniChar UC;
			int sz;
			#ifdef __WXMSW__
				sz=2;
			#else
				sz=sizeof(UC);
			#endif // __WXMSW__
			double oper=pow(0x100,sz)-1;	// key
			double corr=oper-0xFF;				// corrector
		#endif // __i686__
		for(int i=0;i<L;i++){
			#ifdef __i686__
				C = buffer[i] ^ char(0xFF);
				if(C < 0) C += 0x100;	// Compatibility line to ensure compatibility with old 2.8 version on Windows platform;
				if(C != char(0xFF)) S.SetChar(i, C);
			#else
				UC=S.GetChar(i);
				letter=long(UC);
				letter^=long(oper);
				if(letter>0xFF)letter-=corr;
				UC=wxUniChar(letter);
				if(UC!=wxUniChar(0xFF))S.SetChar(i,UC);
			#endif // __i686__
		}
		return S;}
#endif // _OMB_FORMAT30x

#ifdef _OMB_FORMAT31x
	bool dataconverter::Upgrade_0301to0302(void){
		#ifdef _OMB_USE_CIPHER
			wxString Update;

			ExecuteUpdate(database, "ALTER TABLE Transactions ADD COLUMN contact_index INTEGER;");
			ExecuteUpdate(database, "ALTER TABLE Transactions ADD COLUMN latitude REAL;");
			ExecuteUpdate(database, "ALTER TABLE Transactions ADD COLUMN longitude REAL;");

			#ifdef __OPENSUSE__
				wxString Sql = L"update Transactions set latitude = \"" +
																	wxString::Format(L"%f", ombInvalidLatitude) +
																	L"\" where isdate = 0;";
				ExecuteUpdate(database, Sql.c_str());
				Sql = L"update Transactions set longitude = \"" +
																	wxString::Format(L"%f", ombInvalidLongitude) +
																	L"\" where isdate = 0;";
				ExecuteUpdate(database, Sql.c_str());

				if(TableExists(L"Credits", wxEmptyString, database)){
					Update = L"ALTER TABLE Credits ADD COLUMN contact_index INTEGER;";
					ExecuteUpdate(database, Update.c_str());
				}
				if(TableExists(L"Debts", wxEmptyString, database)){
					Update = L"ALTER TABLE Debts ADD COLUMN contact_index INTEGER;";
					ExecuteUpdate(database, Update.c_str());
				}
				if(TableExists(L"Borrows", wxEmptyString, database)){
					Update = L"ALTER TABLE Borrows ADD COLUMN contact_index INTEGER;";
					ExecuteUpdate(database, Update.c_str());
				}
				if(TableExists(L"Loans", wxEmptyString, database)){
					Update = L"ALTER TABLE Loans ADD COLUMN contact_index INTEGER;";
					ExecuteUpdate(database, Update.c_str());
				}

				Sql = L"pragma user_version = " +
																							::wxString::Format(L"%d", dbVersion) +
																							L";";
				ExecuteUpdate(database, Sql.c_str());
			#else
				ExecuteUpdate(database, L"update Transactions set latitude = \"" +
																	wxString::Format(L"%f", ombInvalidLatitude) +
																	L"\" where isdate = 0;");
				ExecuteUpdate(database, L"update Transactions set longitude = \"" +
																	wxString::Format(L"%f", ombInvalidLongitude) +
																	L"\" where isdate = 0;");

				if(TableExists(L"Credits", wxEmptyString, database)){
					Update = L"ALTER TABLE Credits ADD COLUMN contact_index INTEGER;";
					ExecuteUpdate(database, Update);
				}
				if(TableExists(L"Debts", wxEmptyString, database)){
					Update = L"ALTER TABLE Debts ADD COLUMN contact_index INTEGER;";
					ExecuteUpdate(database, Update);
				}
				if(TableExists(L"Borrows", wxEmptyString, database)){
					Update = L"ALTER TABLE Borrows ADD COLUMN contact_index INTEGER;";
					ExecuteUpdate(database, Update);
				}
				if(TableExists(L"Loans", wxEmptyString, database)){
					Update = L"ALTER TABLE Loans ADD COLUMN contact_index INTEGER;";
					ExecuteUpdate(database, Update);
				}

				ExecuteUpdate(database, L"pragma user_version = " +
																							::wxString::Format(L"%d", dbVersion) +
																							L";");
			#endif // __OPENSUSE__

			sqlite3_exec(database, "RELEASE rollback;", NULL, NULL, NULL);
		#else
			database->ExecuteUpdate("ALTER TABLE Transactions ADD COLUMN contact_index INTEGER;");
			database->ExecuteUpdate("ALTER TABLE Transactions ADD COLUMN latitude REAL;");
			database->ExecuteUpdate("ALTER TABLE Transactions ADD COLUMN longitude REAL;");

			database->ExecuteUpdate(L"update Transactions set latitude = \"" +
																	wxString::Format(L"%f", ombInvalidLatitude) +
																	L"\" where isdate = 0;");
			database->ExecuteUpdate(L"update Transactions set longitude = \"" +
																	wxString::Format(L"%f", ombInvalidLongitude) +
																	L"\" where isdate = 0;");

			if(database->TableExists(L"Credits"))
				database->ExecuteUpdate(L"ALTER TABLE Credits ADD COLUMN contact_index INTEGER;");
			if(database->TableExists(L"Debts"))
				database->ExecuteUpdate(L"ALTER TABLE Debts ADD COLUMN contact_index INTEGER;");
			if(database->TableExists(L"Borrows"))
				database->ExecuteUpdate(L"ALTER TABLE Borrows ADD COLUMN contact_index INTEGER;");
			if(database->TableExists(L"Loans"))
				database->ExecuteUpdate(L"ALTER TABLE Loans ADD COLUMN contact_index INTEGER;");
			database->ExecuteUpdate(L"pragma user_version = " +
																							::wxString::Format(L"%d", dbVersion) +
																							L";");
			database->ReleaseSavepoint(L"rollback");
		#endif // _OMB_USE_CIPHER

		return true;
	}

	bool dataconverter::UpgradeMaster_0301to0302(void){
		#ifdef _OMB_USE_CIPHER
			#ifdef __OPENSUSE__
				wxString Update = L"ALTER TABLE Transactions ADD COLUMN contact_index INTEGER;";
				ExecuteUpdate(database, Update.c_str());
				Update = L"ALTER TABLE Transactions ADD COLUMN latitude REAL;";
				ExecuteUpdate(database, Update.c_str());
				Update = L"ALTER TABLE Transactions ADD COLUMN longitude REAL;";
				ExecuteUpdate(database, Update.c_str());

				Update = L"update Transactions set latitude = \"" +
																		wxString::Format(L"%f", ombInvalidLatitude) +
																		L"\" where isdate = 0;";
				ExecuteUpdate(database, Update.c_str());

				Update = L"update Transactions set longitude = \"" +
																		wxString::Format(L"%f", ombInvalidLongitude) +
																		L"\" where isdate = 0;";
				ExecuteUpdate(database, Update.c_str());
			#else
				wxString Update = L"ALTER TABLE Transactions ADD COLUMN contact_index INTEGER;";
				ExecuteUpdate(database, Update);
				Update = L"ALTER TABLE Transactions ADD COLUMN latitude REAL;";
				ExecuteUpdate(database, Update);
				Update = L"ALTER TABLE Transactions ADD COLUMN longitude REAL;";
				ExecuteUpdate(database, Update);

				Update = L"update Transactions set latitude = \"" +
																		wxString::Format(L"%f", ombInvalidLatitude) +
																		L"\" where isdate = 0;";
				ExecuteUpdate(database, Update);

				Update = L"update Transactions set longitude = \"" +
																		wxString::Format(L"%f", ombInvalidLongitude) +
																		L"\" where isdate = 0;";
				ExecuteUpdate(database, Update);
			#endif // __OPENSUSE__

			wxSQLite3Table TableList = GetTable(database, "SELECT name FROM sqlite_master WHERE type='table';");
		#else
			database->ExecuteUpdate(L"ALTER TABLE Transactions ADD COLUMN contact_index INTEGER;");
			database->ExecuteUpdate(L"ALTER TABLE Transactions ADD COLUMN latitude REAL;");
			database->ExecuteUpdate(L"ALTER TABLE Transactions ADD COLUMN longitude REAL;");

			database->ExecuteUpdate(L"update Transactions set latitude = \"" +
																	wxString::Format(L"%f", ombInvalidLatitude) +
																	L"\" where isdate = 0;");
			database->ExecuteUpdate(L"update Transactions set longitude = \"" +
																	wxString::Format(L"%f", ombInvalidLongitude) +
																	L"\" where isdate = 0;");

			//wxSQLite3Table TableList = master_db.GetTable("SELECT name FROM my_db.sqlite_master WHERE type='table';");
			wxSQLite3Table TableList = database->GetTable("SELECT name FROM sqlite_master WHERE type='table';");
		#endif // _OMB_USE_CIPHER

		wxString ColName;
		for(int i = 0; i < TableList.GetRowCount(); i++){
			TableList.SetRow(i);
			ColName = TableList.GetString(0, wxEmptyString);
			#ifdef __OPENSUSE__
				wxString Sql;
			#endif // __OPENSUSE__

			if(ColName.StartsWith(L"Credits"))
				#ifdef _OMB_USE_CIPHER
					#ifdef __OPENSUSE__
						{
							Sql = wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName);
							ExecuteUpdate(database, Sql.c_str());
						}
					#else
						ExecuteUpdate(database, wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName));
					#endif // __OPENSUSE__
				#else
					database->ExecuteUpdate(wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName));
				#endif // _OMB_USE_CIPHER
			else if(ColName.StartsWith(L"Debts"))
				#ifdef _OMB_USE_CIPHER
					#ifdef __OPENSUSE__
						{
							Sql = wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName);
							ExecuteUpdate(database, Sql.c_str());
						}
					#else
						ExecuteUpdate(database, wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName));
					#endif // __OPENSUSE__
				#else
					database->ExecuteUpdate(wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName));
				#endif // _OMB_USE_CIPHER
			else if(ColName.StartsWith(L"Borrows"))
				#ifdef _OMB_USE_CIPHER
					#ifdef __OPENSUSE__
						{
							Sql = wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName);
							ExecuteUpdate(database, Sql.c_str());
						}
					#else
						ExecuteUpdate(database, wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName));
					#endif // __OPENSUSE__
				#else
					database->ExecuteUpdate(wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName));
				#endif // _OMB_USE_CIPHER
			else if(ColName.StartsWith(L"Loans"))
				#ifdef _OMB_USE_CIPHER
					#ifdef __OPENSUSE__
						{
							Sql = wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName);
							ExecuteUpdate(database, Sql.c_str());
						}
					#else
						ExecuteUpdate(database, wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName));
					#endif // __OPENSUSE__
				#else
					database->ExecuteUpdate(wxString::Format(L"ALTER TABLE %s ADD COLUMN contact_index INTEGER;", ColName));
				#endif // _OMB_USE_CIPHER
		}

		#ifdef _OMB_USE_CIPHER
			Update = L"pragma user_version = " +
																							::wxString::Format(L"%d", dbVersion) +
																							L";";
			#ifdef __OPENSUSE__
				ExecuteUpdate(database, Update.c_str());
			#else
				ExecuteUpdate(database, Update);
			#endif // __OPENSUSE__
			sqlite3_exec(database, "RELEASE rollback;", NULL, NULL, NULL);
		#else
			database->ExecuteUpdate(L"pragma user_version = " +
																							::wxString::Format(L"%d", dbVersion) +
																							L";");
			database->ReleaseSavepoint(L"rollback");
		#endif // _OMB_USE_CIPHER

		return true;
	}
#endif // _OMB_FORMAT31x

#ifdef _OMB_FORMAT32x
bool dataconverter::Upgrade_030301to030302(void){
	#ifdef _OMB_USE_CIPHER
		//wxString Update;

		ExecuteUpdate(database, "ALTER TABLE Transactions ADD COLUMN currencyid INTEGER;");
		ExecuteUpdate(database, "ALTER TABLE Transactions ADD COLUMN currencyrate REAL;");
		ExecuteUpdate(database, "ALTER TABLE Transactions ADD COLUMN currencysymb TEXT;");

		#ifdef __OPENSUSE__
      wxString Sql = L"update Transactions set currencyid = \"-1\" where isdate = 0;";
      ExecuteUpdate(database, Sql.c_str());

      Sql = L"pragma user_version = " +
																						::wxString::Format(L"%d", dbVersion) +
																						L";";
      ExecuteUpdate(database, Sql.c_str());
		#else
      ExecuteUpdate(database, "update Transactions set currencyid = \"-1\" where isdate = 0;");

      ExecuteUpdate(database, L"pragma user_version = " +
																						::wxString::Format(L"%d", dbVersion) +
																						L";");
    #endif // __OPENSUSE__

		sqlite3_exec(database, "RELEASE rollback;", NULL, NULL, NULL);
	#else
		database->ExecuteUpdate("ALTER TABLE Transactions ADD COLUMN currencyid INTEGER;");
		database->ExecuteUpdate("ALTER TABLE Transactions ADD COLUMN currencyrate REAL;");
		database->ExecuteUpdate("ALTER TABLE Transactions ADD COLUMN currencysymb TEXT;");

		database->ExecuteUpdate(L"update Transactions set currencyid = \"-1\" where isdate = 0;");
		database->ExecuteUpdate(L"pragma user_version = " +
																						::wxString::Format(L"%d", dbVersion) +
																						L";");
		database->ReleaseSavepoint(L"rollback");
	#endif // _OMB_USE_CIPHER

	return true;
}

bool dataconverter::UpgradeMaster_030301to030302(void){
	#ifdef _OMB_USE_CIPHER
    #ifdef __OPENSUSE__
      wxString Update = L"ALTER TABLE Transactions ADD COLUMN currencyid INTEGER;";
      ExecuteUpdate(database, Update.c_str());
      Update = L"ALTER TABLE Transactions ADD COLUMN currencyrate REAL;";
      ExecuteUpdate(database, Update.c_str());
      Update = L"ALTER TABLE Transactions ADD COLUMN currencysymb TEXT;";
      ExecuteUpdate(database, Update.c_str());

      Update = L"update Transactions set currencyid = \"-1\" where isdate = 0;";
      ExecuteUpdate(database, Update.c_str());

    #else
      wxString Update = L"ALTER TABLE Transactions ADD COLUMN currencyid INTEGER;";
      ExecuteUpdate(database, Update);
      Update = L"ALTER TABLE Transactions ADD COLUMN currencyrate REAL;";
      ExecuteUpdate(database, Update);
      Update = L"ALTER TABLE Transactions ADD COLUMN currencysymb TEXT;";
      ExecuteUpdate(database, Update);

      Update = L"update Transactions set currencyid = \"-1\" where isdate = 0;";
      ExecuteUpdate(database, Update);

    #endif // __OPENSUSE__

    // wxSQLite3Table TableList = GetTable(database, "SELECT name FROM sqlite_master WHERE type='table';");

		Update = L"pragma user_version = " +
																						::wxString::Format(L"%d", dbVersion) +
																						L";";
    #ifdef __OPENSUSE__
      ExecuteUpdate(database, Update.c_str());
    #else
      ExecuteUpdate(database, Update);
    #endif // __OPENSUSE__
		sqlite3_exec(database, "RELEASE rollback;", NULL, NULL, NULL);
	#else
		database->ExecuteUpdate(L"ALTER TABLE Transactions ADD COLUMN currencyid INTEGER;");
		database->ExecuteUpdate(L"ALTER TABLE Transactions ADD COLUMN currencyrate REAL;");
		database->ExecuteUpdate(L"ALTER TABLE Transactions ADD COLUMN currencysymb TEXT;");

		database->ExecuteUpdate(L"update Transactions set currencyid = \"-1\" where isdate = 0;");

		// wxSQLite3Table TableList = database->GetTable("SELECT name FROM sqlite_master WHERE type='table';");

		database->ExecuteUpdate(L"pragma user_version = " +
																						::wxString::Format(L"%d", dbVersion) +
																						L";");
		database->ReleaseSavepoint(L"rollback");
	#endif // _OMB_USE_CIPHER

	return true;
}
#endif // _OMB_FORMAT32x

#ifdef _OMB_FORMAT33x
	bool dataconverter::Upgrade_0303to0304(void){
		#ifdef _OMB_USE_CIPHER

			ExecuteUpdate(database, "ALTER TABLE Categories ADD COLUMN iconid INTEGER;");

			#ifdef __OPENSUSE__
				wxString Sql = L"update Categories set iconid = \"-1\";";
				ExecuteUpdate(database, Sql.c_str());

				Sql = L"pragma user_version = " +
																							::wxString::Format(L"%d", dbVersion) +
																							L";";
				ExecuteUpdate(database, Sql.c_str());
			#else
				ExecuteUpdate(database, "update Categories set iconid = \"-1\";");

				ExecuteUpdate(database, L"pragma user_version = " +
																							::wxString::Format(L"%d", dbVersion) +
																							L";");
			#endif // __OPENSUSE__

			sqlite3_exec(database, "RELEASE rollback;", NULL, NULL, NULL);
		#else
			database->ExecuteUpdate("ALTER TABLE Categories ADD COLUMN iconid INTEGER;");

			database->ExecuteUpdate(L"update Categories set iconid = \"-1\";");

			database->ExecuteUpdate(L"pragma user_version = " +
																							::wxString::Format(L"%d", dbVersion) +
																							L";");
			database->ReleaseSavepoint(L"rollback");
		#endif // _OMB_USE_CIPHER

		return true;
	}
#endif // _OMB_FORMAT33x

#endif	// DATACONVERTER_CPP_INCLUDED
