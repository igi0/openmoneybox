/***************************************************************
 * Name:      ombconvert.cpp
 * Purpose:   old format conversion tool for OpenMoneyBox
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2024-11-02
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#include "../constants.h"
#include "ombconvert.h"
#include "dataconverter.h"

wxLocale omb_locale;

extern wxString cs_information;

#ifdef _OMB_USE_CIPHER
	#include "../wxsqlite3.h"

	extern bool TableExists(const wxString& tableName, const wxString& databaseName, sqlite3 *m_db);
	extern int ExecuteUpdate(sqlite3 *m_db, const char* sql);
	extern wxSQLite3Table GetTable(sqlite3 *m_db, const char* sql);
	extern std::string getpass(const char *prompt, bool show_asterisk = true);	// flawfinder: ignore
#endif // _OMB_USE_CIPHER

#ifdef __WXMSW__
  wxIMPLEMENT_APP(ombconvert);
#else
 	wxIMPLEMENT_APP_CONSOLE(ombconvert);
#endif // __WXMSW__

bool ombconvert::OnInit(){

	#ifdef __WXMSW__
    wxLocale::AddCatalogLookupPathPrefix(GetShareDir());
	#else
    wxLocale::AddCatalogLookupPathPrefix(GetShareDir()+L"locale");
	#endif // __WXMSW__
	omb_locale.Init(wxLANGUAGE_DEFAULT,wxLOCALE_LOAD_DEFAULT);
	omb_locale.AddCatalog(L"openmoneybox");

	// TODO (igor#1#): ADD COMMAND LINE PARSER

  #ifdef __WXMSW__
    // Create top level window (hidden)
    wxFrame *frame = new wxFrame (NULL, 100, "ombconvert", wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE,
                         "wxFrameNameStr");
    wxTheApp->SetTopWindow(frame);
  #endif // __WXMSW__

	int arg_num = wxTheApp->argc;
	const char *output_string;

  #ifdef __WXMSW__
    printf("\n");
  #endif // __WXMSW__

	if(arg_num < 2){
		output_string = _("Syntax: ombconvert [-master] <filename>\n  -master: filename is a master database\n").c_str();
		printf("%s", output_string);
		return false;
	}

	int original_format;
	#ifdef _OMB_FORMAT30x
		int nfunds = 0, ncredits = 0, ndebits = 0, nloans = 0, nborrows = 0, ntransactions = 0, ncategories = 0, nshoplists = 0;
	#endif // _OMB_FORMAT30x
//	wxString default_fund = L"default";

	int i;
	bool checkMaster = false;

 	for(i = 1; i < arg_num; i++){
		wxString file = wxTheApp->argv[i];

		if(file == L"-master") checkMaster = true;

 		else if(::wxFileExists(file)){
 			dataconverter *original = new dataconverter();
			wxString result;
			#ifdef _OMB_USE_CIPHER
				wxString pwd = getpass("Enter the document password: ", true);	// flawfinder: ignore
				original_format = CheckFileFormat(file, pwd, false);
			#else
				original_format = CheckFileFormat(file);
			#endif // _OMB_USE_CIPHER
			switch(original_format){
				#ifdef _OMB_FORMAT30x
					case bilFFORMAT_301:
						original->CategoryDB = new wxArrayString();
						original->CategoryDB->Clear();

						result = original->LoadFromFile_0301(file, true);

						nfunds = original->NFun;
						ncredits = original->NCre;
						ndebits = original->NDeb;
						nloans = original->NLen;
						nborrows = original->NBor;
						ntransactions = original->NLin;
						//ncategories = original->NCat;
						nshoplists = original->NSho;
						default_fund = original->FileData.DefFund;
						break;
					case bilFFORMAT_302:
						// trim empty document
						original->CategoryDB = new wxArrayString();
						original->CategoryDB->Clear();
						original->NCat = 0;

						result = original->LoadFromFile_0302(file, true);

						nfunds = original->NFun;
						ncredits = original->NCre;
						ndebits = original->NDeb;
						nloans = original->NLen;
						nborrows = original->NBor;
						ntransactions = original->NLin;
						ncategories = original->NCat;
						nshoplists = original->NSho;
						default_fund = original->FileData.DefFund;
						break;
				#endif // _OMB_FORMAT30x
				#ifdef _OMB_FORMAT31x
					case ombFFORMAT_31:
				#endif // _OMB_FORMAT31x
				#ifdef _OMB_FORMAT32x
					case ombFFORMAT_32:
				#endif // _OMB_FORMAT32x
				#ifdef _OMB_USE_CIPHER
					#ifdef _OMB_FORMAT33x
						case ombFFORMAT_33:
					#endif // _OMB_FORMAT33x
				#endif // _OMB_USE_CIPHER

				case ombFFORMAT_332:

					result = L"ok";
					break;
				default:
					result = wxEmptyString;
			}
			if(result.IsEmpty()){
 			  #ifdef __WXMSW__
          wxMessageBox(_("Error loading the file.\n"), "ombconvert", wxICON_ERROR);
        #else
					output_string = _("Error loading the file.\n").c_str();
					printf("%s", output_string);
 			  #endif // __WXMSW__
 				return false;}
			wxString vol, FilePath, FileName, ext, newfile;
			::wxFileName::SplitPath(file, &vol, &FilePath, &FileName, &ext, wxPATH_NATIVE);
			ext = ".bak";
			#ifdef __WXMSW__
				if(! vol.IsEmpty()) newfile = vol + L":" + FilePath + L"\\";
        newfile = newfile + FileName + ext;
      #else
        newfile = vol + FilePath + L"/" + FileName + ext;
      #endif // __WXMSW__

      switch(original_format){
      	#ifdef _OMB_FORMAT30x
					case bilFFORMAT_301:
					case bilFFORMAT_302:
						::wxRenameFile(file, newfile, false);
						break;
				#endif // _OMB_FORMAT30x
				default:
					if(! ::wxCopyFile(file, newfile)){
						// TODO (igor#1#): Insert code
					}
				}

				#ifdef _OMB_FORMAT30x
				switch(original_format){
					case bilFFORMAT_301:
						// Document conversion from format (3) to (4)
						// Build CategoryDB from Remarks
						int index;
						wxString rmrk;
						original->CategoryDB->Clear();
						for(i = 0; i < original->NLin; i ++){
							if(! original->IsDate(i)){
								index = -1;
								if(! original->Lines[i].Remark0301.IsEmpty()){
									rmrk = original->Lines[i].Remark0301.MakeLower();
									for(unsigned int j = 0; j < original->CategoryDB->Count(); j ++){
										if(rmrk == original->CategoryDB->Item(j)){
											index = j;
											break;
										}
									}
									if(index == -1) index = original->CategoryDB->Add(rmrk, 1);
								}
								original->Lines[i].CategoryIndex = index;
							}
						}
						ncategories = original->CategoryDB->Count();

						original->FileData.Modified = true;
						break;
				}
				#endif // _OMB_FORMAT30x

			// Build database
			if(checkMaster){
				/*
				bool file_exist = ::wxFileExists(File);
				wxString Name;
				*/

				#ifdef _OMB_USE_CIPHER
          #ifdef __OPENSUSE__
            sqlite3_open(file.c_str(), &original->database);
            sqlite3_key(original->database, pwd.c_str(), pwd.length());
          #else
            sqlite3_open(file, &original->database);
            sqlite3_key(original->database, pwd, pwd.length());
          #endif // __OPENSUSE__
				#else
					original->database->Open(file, wxEmptyString, WXSQLITE_OPEN_READWRITE | WXSQLITE_OPEN_CREATE );
				#endif // _OMB_USE_CIPHER

				/*
				if(file_exist){
					FileData.FileName = File;
					::wxFileName::SplitPath(File, NULL, NULL, &Name, NULL, wxPATH_NATIVE);
					FileData.FileView = Name;

				}
				else database->ExecuteUpdate(L"pragma user_version = " +
																									::wxString::Format(L"%d", 31) +
																									L";");
				*/

				// Set restore savepoint
				#ifdef _OMB_USE_CIPHER
					ExecuteUpdate(original->database, "SAVEPOINT rollback;");
				#else
					original->database->Savepoint(L"rollback");
				#endif // _OMB_USE_CIPHER

				// Write file metadata
				wxString metadata = L"OS: ";
				#ifdef __WXGTK__
					#ifndef __FREEBSD__
						metadata += wxGetLinuxDistributionInfo().Description;
					#else
						metadata += wxGetOsDescription();
					#endif // __FREEBSD__
				#elif defined ( __WXMSW__)
					metadata += wxGetOsDescription();
				#else
					metadata += L"unknown";
				#endif // __WXGTK__
				metadata += L"\n\n";
				metadata += _("Converted with ombconvert");

					#ifdef _OMB_USE_CIPHER
						if(! TableExists(L"Information", wxEmptyString, original->database)){
              #ifdef __OPENSUSE__
                ExecuteUpdate(original->database, cs_information.c_str());
              #else
                ExecuteUpdate(original->database, cs_information);
              #endif // __OPENSUSE__
					#else
						if(! original->database->TableExists(L"Information")){
							original->database->ExecuteUpdate(cs_information);
					#endif // _OMB_USE_CIPHER

					#ifdef _OMB_USE_CIPHER
						wxString Update = L"insert into Information values (" +
																		wxString::Format(L"%d", dbMeta_application_info) +
																		L", '" +
																		metadata +
																		L"');";
						#ifdef __OPENSUSE__
							ExecuteUpdate(original->database, Update.c_str());
						#else
							ExecuteUpdate(original->database, Update);
						#endif // __OPENSUSE__
					#else
						original->database->ExecuteUpdate(L"insert into Information values (" +
																		wxString::Format(L"%d", dbMeta_application_info) +
																		L", '" +
																		metadata +
																		L"');");
					#endif // _OMB_USE_CIPHER

					#ifdef _OMB_USE_CIPHER
						Update = L"insert into Information values (" +
																			wxString::Format(L"%d", dbMeta_default_fund) +
																			L", 'default');";
            #ifdef __OPENSUSE__
              ExecuteUpdate(original->database, Update.c_str());
            #else
              ExecuteUpdate(original->database, Update);
            #endif // __OPENSUSE__
						Update = L"insert into Information values (" +
																			wxString::Format(L"%d", dbMeta_mobile_export) +
																			L", '');";
						#ifdef __OPENSUSE__
              ExecuteUpdate(original->database, Update.c_str());
						#else
              ExecuteUpdate(original->database, Update);
            #endif // __OPENSUSE__
					#else
						original->database->ExecuteUpdate(L"insert into Information values (" +
																			wxString::Format(L"%d", dbMeta_default_fund) +
																			L", 'default');");
						original->database->ExecuteUpdate(L"insert into Information values (" +
																		wxString::Format(L"%d", dbMeta_mobile_export) +
																		L", '');");
					#endif // _OMB_USE_CIPHER
				}

				#ifdef _OMB_USE_CIPHER
					else {
						wxString Update = L"update Information set data = \"" +
																			metadata +
																			L"\" where id = " +
																			wxString::Format(L"%d", dbMeta_application_info) +
																			L";";
						#ifdef __OPENSUSE__
							ExecuteUpdate(original->database, Update.c_str());
						#else
							ExecuteUpdate(original->database, Update);
						#endif // __OPENSUSE__
					}
				#else
					else original->database->ExecuteUpdate(L"update Information set data = \"" +
																		metadata +
																		L"\" where id = " +
																		wxString::Format(L"%d", dbMeta_application_info) +
																		L";");
				#endif // _OMB_USE_CIPHER

				/*
				// Set currency when if not present (added later in dbVersion 31)
				wxSQLite3Table currencyTable;
				#ifdef _OMB_USE_CIPHER
					wxString Update = L"select data from Information where id = " +
																		wxString::Format(L"%d", dbMeta_currency) +
																		L";";
					#ifdef __OPENSUSE__
            currencyTable = GetTable(original->database, Update.c_str());
					#else
            currencyTable = GetTable(original->database, Update);
          #endif // __OPENSUSE__
				#else
					currencyTable = original->database->GetTable(L"select data from Information where id = " +
																		wxString::Format(L"%d", dbMeta_currency) +
																		L";");
				#endif // _OMB_USE_CIPHER
				if(currencyTable.GetRowCount() == 0){
					wxString curr = GetCurrencySymbol();
					#ifdef _OMB_USE_CIPHER
						Update = L"insert into Information values (" +
																		wxString::Format(L"%d", dbMeta_currency) +
																		L", '" +
																		curr +
																		L"');";
						#ifdef __OPENSUSE__
              ExecuteUpdate(original->database, Update.c_str());
						#else
              ExecuteUpdate(original->database, Update);
            #endif // __OPENSUSE__
					#else
						original->database->ExecuteUpdate(L"insert into Information values (" +
																		wxString::Format(L"%d", dbMeta_currency) +
																		L", '" +
																		curr +
																		L"');");
					#endif // _OMB_USE_CIPHER
				}
				*/

				/*
				// Transaction table init
				if(! database->TableExists(L"Transactions")) database->ExecuteUpdate(cs_transactions_v31);

				// Category table init
				if(! database->TableExists(L"Categories")) database->ExecuteUpdate(cs_categories);

				ParseDatabase();
				FileData.Modified = false;

				return true;
				*/
			}
			else
				#ifdef _OMB_USE_CIPHER
					original->OpenDatabase(file, pwd);
				#else
					original->OpenDatabase(file);
				#endif // _OMB_USE_CIPHER
			switch(original_format){
				#ifdef _OMB_FORMAT30x
					case bilFFORMAT_301:
						original->LoadFromFile_0301(file, true);
						original->BuildDatabase(nfunds, ncredits, ndebits, nloans, nborrows, ntransactions, ncategories, nshoplists, default_fund);
						break;
					case bilFFORMAT_302:
						original->LoadFromFile_0302(file, true);
						original->BuildDatabase(nfunds, ncredits, ndebits, nloans, nborrows, ntransactions, ncategories, nshoplists, default_fund);
						break;
				#endif // _OMB_FORMAT30x
				#ifdef _OMB_FORMAT31x
					case ombFFORMAT_31:
						if(checkMaster) original->UpgradeMaster_0301to0302();
						else original->Upgrade_0301to0302();
						break;
				#endif // _OMB_FORMAT31x
				#ifdef _OMB_FORMAT32x
					case ombFFORMAT_32:
						if(checkMaster) original->UpgradeMaster_030301to030302();
						else original->Upgrade_030301to030302();
						break;
				#endif // _OMB_FORMAT32x
				#ifdef _OMB_FORMAT33x
					case ombFFORMAT_332:
						original->Upgrade_0303to0304();
						break;
				#endif // _OMB_FORMAT33x

				case ombFFORMAT_34:
					break;
			}

			#ifdef __WXMSW__
				wxMessageBox(_("File successfully converted.\n"), "ombconvert", wxICON_INFORMATION);
			#else
				output_string = _("File successfully converted.\n").c_str();
				printf("%s", output_string);
			#endif // __WXMSW__
 		}
 		else
 		{
				output_string = _("The file does not exist.\n").c_str();
				printf("%s", output_string);
 				return false;
 		}
 	}

	return false;}
