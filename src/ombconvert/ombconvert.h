/***************************************************************
 * Name:      ombconvert.h
 * Purpose:   old format conversion tool for OpenMoneyBox
 * Author:    Igor Calì (igor.cali0@gmail.com)
 * Created:   2020-08-16
 * Copyright: Igor Calì (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

#ifndef OMBCONVERTAPP_H
#define OMBCONVERTAPP_H

#include <wx/app.h>

#include <wx/filename.h>

#ifdef __WXMSW__
  class ombconvert : public wxApp
#else
  class ombconvert : public wxAppConsole
#endif // __WXMSW__
{
    public:
        virtual bool OnInit();
};

WXIMPORT wxString GetShareDir(void);

#endif // OMBCONVERTAPP_H
