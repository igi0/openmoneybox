<!-- last update: 20/02/2025 -->

![Image](https://launchpadlibrarian.net/321444387/logo.png "icon")

OpenMoneyBox
============

## Budget management application
OpenMoneyBox is an application designed to manage small personal money budgets in the easiest way.

[Homepage](http://igisw-bilancio.sourceforge.net/)  
Links to pre-built binary packages can be found in the project homepage.

---
## Build and install on Linux:  
	`> make`  
	`> sudo make install`

Tested on:  
  * **Ubuntu Noble (24.04.2) x64 [v3.5.1.x]**  
  * Ubuntu Jammy (22.04.3) x64 [v3.4.2.x] [v3.4.1.13]  
  * Ubuntu Focal  (20.04.4) x64 [v3.4.1.x]  
  * Ubuntu Bionic (18.04.4) x64 [v3.4.1.3] [v3.3]  
  * Ubuntu Xenial (16.04.4) x64 [v3.2]  
  * Ubuntu Trusty (14.04.3) x64 [v3.1] [v3.0]  
  * **Debian Bookworm (12) [v3.5.1.x]** [v3.4.2.4]  
  * Debian Bullseye (11) [v3.4.2.x] [v3.4.1.11]  
  * Debian Buster (10) [v3.4.1.8]  
  * RaspiOS / Raspbian (**Bookworm 12** down to 11)  
  * Suse Leap (**15.6** down to 15.0) - OpenSuse (42.3, 42.2)  
  * Fedora (**39** down to 25)  
  * Arch Linux (**2025.02.01** down to 2022.10.1)  

---
## Build and install on FreeBSD:

1. Install dependencies:  
	`> pkg install gmake`  
	`> pkg install wx32-gtk3`  
	`> pkg install llvm`  
	`> pkg install sqlcipher`  
	`> pkg install osm-gps-map`  

2. build openmoneybox:  
	`> gmake -f makefile.bsd`  
	`> gmake -f makefile.bsd install`

Tested on:  
  * **FreeBSD 14.x**  
  * FreeBSD 13.x  
  * FreeBSD 12.x 

---
## Build and install on OpenSolaris:

1. Install dependencies:  
	`> pkg install gcc-14`  
	`> pkg install wxwidgets-33`  
	`> pkg install sqlcipher`  

2. build openmoneybox:  
	`> gmake -f makefile.solaris`  
	`> gmake -f makefile.solaris install`

Tested on:  
  * **OpenIndiana 2024.4**  

---
## Build and install on Windows:  
Requirements: install MinGW64 and MSys2

1. build wxWidgets:  
(disable MSYS binary beforehand)  
	`> cd build\msw`  
debug build:  
	`> mingw32-make.exe -f makefile.gcc USE_XRC=1 SHARED=1 MONOLITHIC=0 BUILD=debug UNICODE=1`  
release build mingw32:  
	`> mingw32-make.exe -f makefile.gcc USE_XRC=1 SHARED=1 MONOLITHIC=0 BUILD=release UNICODE=1 DEBUG_FLAG=0`  
	(set wxDEBUG_LEVEL to 0 in /include/wx/debug.h)  
release build mingw64:  
	`> mingw32-make -f makefile.gcc CPP="gcc -E -D_M_AMD64" USE_XRC=1 SHARED=1 DEBUG_FLAG=0 BUILD=release UNICODE=1 MONOLITHIC=0`  
	(set wxDEBUG_LEVEL to 0 in /include/wx/debug.h)

2. build wxsqlite3.dll  
    `> cd build30`  
    `> mingw32-make -f makefile.gcc WX_SHARED=1 WX_DEBUG=0 WX_MONOLITHIC=0 WX_VERSION=32 WX_DIR=<wxWidgets installation folder> SHARED=1`  

    <!-- FIXME: build with latest wxsqlite3 version - v4.9.11
    mingw32-make wxRootDir=E:\Documenti\Programmazione\wxWidgets-3.2.6 config=release_wxdll_win32
    -->

3. build openmoneybox:  
	`> mingw32-make -f makefile.win`  
	`> mingw32-make -f makefile.win install`

Tested on:  
  * **Win11 x64**  
  * Win10 x64 

---
## Build and install on macOS:  
Requirements: install Homebrew

1. Install dependencies:  
	`> brew install wxwidgets`  
    `> brew install sqlcipher`  
2. build openmoneybox:  
	`> make -f makefile.mac`  
	`> make -f makefile.mac install`

Tested on:  
  * **macOS Sierra 10.12.5**  
