;	OpenMoneyBox
;	Last change 08/11/2024

Unicode True

; Include
!include "include\locales.nsi"
!include "include\version.nsi"
;	!include "InstallerFiles\Include\PrevInst.nsi"		; Necessary to VPatch
!include "include\dump.nsi"
;!include "include\dfe.nsi"
;	!include "InstallerFiles\Include\Windows_ver.nsi"
!include "include\registry.nsh"
!insertmacro COPY_REGISTRY_KEY
;__________________________________

;Installer attributes
!ifdef ALPHA
	Name "$(ProductName) ${MAJ_VER_str}.${MIN_VER_str}.${RELEASE_str}.${BUILD_str} alpha"
	OutFile "OpenMoneyBox_${MAJ_VER_str}_${MIN_VER_str}_${RELEASE_str}_${BUILD_str}_a.exe"
!else
	!ifdef BETA
		Name "$(ProductName) ${MAJ_VER_str}.${MIN_VER_str}.${RELEASE_str}.${BUILD_str} beta"
		OutFile "OpenMoneyBox_${MAJ_VER_str}_${MIN_VER_str}_${RELEASE_str}_${BUILD_str}_b.exe"
	!else
		!ifDef RC
			Name "$(ProductName) ${MAJ_VER_str}.${MIN_VER_str}.${RELEASE_str}.${BUILD_str} Release Candidate"
			OutFile "OpenMoneyBox_${MAJ_VER_str}_${MIN_VER_str}_${RELEASE_str}_${BUILD_str}_rc.exe"
		!else
			Name "$(ProductName) ${MAJ_VER_str}.${MIN_VER_str}.${RELEASE_str}.${BUILD_str}"
			OutFile "OpenMoneyBox_${MAJ_VER_str}_${MIN_VER_str}_${RELEASE_str}_${BUILD_str}.exe"
		!endif
	!endif
!endif
RequestExecutionLevel admin

Icon "media\install.ico"
AutoCloseWindow true
InstProgressFlags smooth colored
CRCCheck force
ShowInstDetails nevershow
ShowUninstDetails nevershow
SetOverwrite ifdiff
!ifdef ALPHA
	Caption "$(ProductName) ${MAJ_VER_str}.${MIN_VER_str} alpha"
!else
	!ifdef BETA
		!ifdef DEBUG
			Caption "$(ProductName) ${MAJ_VER_str}.${MIN_VER_str} beta - DEBUG"
		!else
			Caption "$(ProductName) ${MAJ_VER_str}.${MIN_VER_str} beta"
		!endif
	!else
		Caption "$(ProductName) ${MAJ_VER_str}.${MIN_VER_str}"
	!endif
!endif

; License files
LicenseLangString License ${LANG_ENGLISH} "..\licenses\en\license.txt"
LicenseLangString License ${LANG_ITALIAN} "..\licenses\it\licenza.txt"
LicenseData $(License)
LicenseForceSelection checkbox
;XPStyle on

!ifdef BETA
	InstallDir $PROGRAMFILES\OpenMoneyBox_beta
!else
	InstallDir $PROGRAMFILES\OpenMoneyBox
!endif
BrandingText /TRIMRIGHT $(ProductName)

InstType $(TypicalInstallation)
InstType $(MinimumInstallation)
InstType $(CompleteInstallation)
InstType /COMPONENTSONLYONCUSTOM
;_______________________________________________________________________

;Version Info
VIProductVersion "${MAJ_VER}.${MIN_VER}.${RELEASE}.${BUILD}"
VIAddVersionKey "ProductName" "OpenMoneyBox"
VIAddVersionKey "Comments" ""
VIAddVersionKey "CompanyName" "igiSW"
VIAddVersionKey "FileDescription" "Budget Management Utility"
VIAddVersionKey "FileVersion" "${MAJ_VER_str}.${MIN_VER_str}.${RELEASE_str}.${BUILD_str}"
VIAddVersionKey "LegalCopyright" "Igor Cali' <igor.cali0@gmail.com>"
;________________________________________________________________________

;Uninstaller attributes
UninstallIcon "media\uninstall.ico"
UninstallButtonText $(uiText)
UninstallCaption $(uiCaption)
UninstallSubCaption 0 $(uiSub0)
UninstallSubCaption 1 $(uiSub1)
UninstallSubCaption 2 $(uiSub2)
;________________________________________________________________________

;Installer pages
Page license
Page components "" ShowComp
Page directory
Page instfiles
;________________________________________________________________________

;Uninstaller pages
UninstPage uninstConfirm
UninstPage InstFiles
;___________________

;Variables
;	Var OmbTrayInstalled
;	Var ConvInstalled	; Preparation
;	Var UpdateInstalled
;	Var WizInstalled
Var HelpInstalled
Var LangFlags	; bit 1: English installed
		; bit 2: Italian installed
;Var OS
Var InstallType
;_______________

Section "-Main"
	;Initialisation
	;StrCpy $OmbTrayInstalled 0
	;StrCpy $ConvInstalled 0	;Preparation
	;StrCpy $UpdateInstalled 0
	;StrCpy $WizInstalled 0
	StrCpy $HelpInstalled 0
	StrCpy $LangFlags 1

	SetOutPath $INSTDIR

	DetailPrint "[Mingw libraries]"
	File "${Files}\libgcc_s_seh-1.dll"
	File "${Files}\libstdc++-6.dll"
	File "${Files}\libwinpthread-1.dll"

	DetailPrint "[Sqlite3 library]"
	File "${Files}\sqlite3.dll"

	DetailPrint "[wxWidgets libraries]"
	File "${Files}\wxbase32u_gcc_custom.dll"
	File "${Files}\wxmsw32u_adv_gcc_custom.dll"
	File "${Files}\wxmsw32u_aui_gcc_custom.dll"
	File "${Files}\wxmsw32u_core_gcc_custom.dll"
	File "${Files}\wxcode_msw32u_wxsqlite3.dll"

	DetailPrint "[Main]"

	SetOutPath $INSTDIR
;	Call ClearAutoLog

;	Call GetWindowsVersion
;	Pop $0
;	StrCpy $OS $0

	!ifndef MONOLITHIC
		File "${Files}\igiomb.dll"
;		Call ClearAutoLog
		File "${Files}\omberr.dll"
;		Call ClearAutoLog
		File "${Files}\ombopt.dll"
;		Call ClearAutoLog
	!endif

	File "${Files}\openmoneybox.exe"
;	Call ClearAutoLog

	IfErrors Err1
	DetailPrint "Files openmoneybox:			OK"
	GoTo Main2
	Err1:
	DetailPrint "Files openmoneybox:			KO!"

;	Main2:
;	StrCmp $OS "XP" XP_OS

	;Code for OS different than WindowsXP--------------------
	;--------------------------------------------------------

;	XP_OS:

	Main2:
	StrCmp $LANGUAGE ${LANG_ENGLISH} 0 +3
	File "..\licenses\en\license.txt"
;	Call ClearAutoLog
	StrCmp $LANGUAGE ${LANG_ITALIAN} 0 +3
	File "..\licenses\it\licenza.txt"
;	Call ClearAutoLog

	File "..\LICENSE"
;	Call ClearAutoLog

;	SetOutPath $INSTDIR\Thanks
;	Call ClearAutoLog
;	File "${Files}\Thanks\*.txt"
;	Call ClearAutoLog
;	Call ClearAutoLog
;	File "${Files}\Thanks\*.ico"
;	Call ClearAutoLog
;	Call ClearAutoLog

	SetOutPath $INSTDIR
	SetRebootFlag true
	DetailPrint "[Alarm]"

	!ifndef MONOLITHIC
		File "${Files}\ombtray.exe"
;		StrCpy $OmbTrayInstalled 1
;		Call ClearAutoLog
		IfErrors Err2
		DetailPrint "OmbTray:			OK"
		GoTo Alarm2
		Err2:
		DetailPrint "OmbTray:			KO!"

		Alarm2:
	!endif

	File "${Files}\alarm_clock.wav"
;	Call ClearAutoLog
	IfErrors Err3
	DetailPrint "Wave:				OK"
	GoTo AlarmEnd
	Err3:
	DetailPrint "Wave:				KO!"

	AlarmEnd:
	DetailPrint ""

	!ifndef MONOLITHIC
		DetailPrint "[Wizard]"

		File "${Files}\ombwizard.dll"
;		StrCpy $WizInstalled 1
;		Call ClearAutoLog
		IfErrors Err4
		DetailPrint "OmbWiz:				OK"
		GoTo Alarm3
		Err4:
		DetailPrint "OmbWiz:				KO!"
		Alarm3:

		DetailPrint ""

		DetailPrint "[Update]"

		File "${Files}\ombupdate.dll"
;		StrCpy $UpdateInstalled 1
;		Call ClearAutoLog
		IfErrors Err5
		DetailPrint "OmbUpdate:			OK"
		GoTo Update2
		Err5:
		DetailPrint "OmbUpdate:			KO!"
		Update2:

		DetailPrint ""
	!endif

	DetailPrint "[XML files]"
	SetOutPath $INSTDIR\data
	File "${Files}\data\*"
	SetOutPath $INSTDIR\data\en
	File "${Files}\data\en\*"
	SetOutPath $INSTDIR\data\it
	File "${Files}\data\it\*"

	DetailPrint "[wget]"
	SetOutPath $INSTDIR
	File "${Files}\wget.exe"	

	DetailPrint "[resources]"
	SetOutPath $INSTDIR\24
	File "${Files}\24\mark-location.png"	
	SetOutPath $INSTDIR\48
	File "${Files}\48\mark-location.png"	
	SetOutPath $INSTDIR\categories
	File "${Files}\categories\*"

;	CreateDirectory "$INSTDIR\Update"
;	Call ClearAutoLog

	DetailPrint ""
SectionEnd


Section "-Dummy"	; To be removed whenever a real Section diversify minimum Typical
			; and Complete installation types
	SectionIn 3
SectionEnd

Section $(HelpFile)
	SectionIn 1 3
	DetailPrint "[Help]"

	SetOutPath $INSTDIR
;	Call ClearAutoLog

	; English
	File "${Files}\openmoneybox-msw_en.html"
;	Call ClearAutoLog

	SetOutPath $INSTDIR\images
	File "${Files}\images\*"		; generic contact icon copied as well here
	SetOutPath $INSTDIR\en\images
	File "${Files}\en\images\*"	

	StrCpy $HelpInstalled 1
;	IntOp $LangFlags $LangFlags | 1
	IfErrors Err1

	DetailPrint "Help file:			OK"
	DetailPrint ""
	Return
	Err1:
	DetailPrint "Help file:			KO!"
	DetailPrint ""
SectionEnd

!ifdef CONVERT_BUILD
;SectionGroup /e $(Utilities)

	;Preparation
	Section -$(Conv)
		SectionIn 1 3
		DetailPrint "[ombconvert]"

		SetOutPath $INSTDIR
		File "${Files}\ombconvert.exe"
	;	StrCpy $ConvInstalled 1
	;	Call ClearAutoLog
		IfErrors Err1
		DetailPrint "ombconvert:				OK"
		Goto ConvOK
		Err1:
		DetailPrint "ombconvert:				KO!"
		ConvOK:

		DetailPrint ""
	SectionEnd

;SectionGroupEnd
!endif

SectionGroup /e $(LanguagePackets)

;	Section "-English_guide"
;		SectionIn 1 2 3
;		DetailPrint "[LANGUAGE: ENGLISH]"
;
;		DetailPrint "English language pack:		OK"
;		DetailPrint ""
;		Return
;		DetailPrint "English language pack:		KO!"
;		DetailPrint ""
;	SectionEnd

	Section $(Language_ITA)
		SectionIn 3
		DetailPrint "[LANGUAGE: ITALIAN]"

		; Help
		StrCmp $HelpInstalled "1" +1 NoHelp
		SetOutPath $INSTDIR
;		Call ClearAutoLog
		File "${Files}\openmoneybox-msw_it.html"
;		Call ClearAutoLog

		SetOutPath $INSTDIR\it\images
		File "${Files}\it\images\*"	

		IntOp $LangFlags $LangFlags | 2
		IfErrors Err1

		NoHelp:
		CreateDirectory "$INSTDIR\it"
;		Call ClearAutoLog
		SetOutPath "$INSTDIR\it"
;		Call ClearAutoLog
		File "${Files}\it\openmoneybox.mo"
;		Call ClearAutoLog
		IfErrors Err1
		DetailPrint "Italian language pack:		OK"
		DetailPrint ""
		Return
		Err1:
		DetailPrint "Italian language pack:		KO!"
		DetailPrint ""
	SectionEnd

	Section $(Language_FRE)
		SectionIn 3
		DetailPrint "[LANGUAGE: FRENCH]"

		; Help
		;StrCmp $HelpInstalled "1" +1 NoHelp
		;SetOutPath $INSTDIR
		;Call ClearAutoLog
		;File "${Files}\openmoneybox-msw_it.html"
		;Call ClearAutoLog

		;SetOutPath $INSTDIR\it\images
		;File "${Files}\it\images\*"	

		;IntOp $LangFlags $LangFlags | 2
		;IfErrors Err1

		;NoHelp:
		CreateDirectory "$INSTDIR\fr"
;		Call ClearAutoLog
		SetOutPath "$INSTDIR\fr"
;		Call ClearAutoLog
		File "${Files}\fr\openmoneybox.mo"
;		Call ClearAutoLog
		IfErrors Err1
		DetailPrint "French language pack:		OK"
		DetailPrint ""
		Return
		Err1:
		DetailPrint "French language pack:		KO!"
		DetailPrint ""
	SectionEnd

	Section $(Language_SWE)
		SectionIn 3
		DetailPrint "[LANGUAGE: SWEDISH]"

		; Help
		;StrCmp $HelpInstalled "1" +1 NoHelp
		;SetOutPath $INSTDIR
		;Call ClearAutoLog
		;File "${Files}\openmoneybox-msw_it.html"
		;Call ClearAutoLog

		;SetOutPath $INSTDIR\it\images
		;File "${Files}\it\images\*"	

		;IntOp $LangFlags $LangFlags | 2
		;IfErrors Err1

		;NoHelp:
		CreateDirectory "$INSTDIR\sv"
;		Call ClearAutoLog
		SetOutPath "$INSTDIR\sv"
;		Call ClearAutoLog
		File "${Files}\sv\openmoneybox.mo"
;		Call ClearAutoLog
		IfErrors Err1
		DetailPrint "Swedish language pack:		OK"
		DetailPrint ""
		Return
		Err1:
		DetailPrint "Swedish language pack:		KO!"
		DetailPrint ""
	SectionEnd

SectionGroupEnd

Section "-Shortcuts"
	StrCpy $0 ""
	DetailPrint "[SHORTCUTS]"
	SetShellVarContext all
	CreateDirectory '$SMPROGRAMS\OpenMoneyBox'
;	Call ClearAutoLog
	IfFileExists '$SMPROGRAMS\OpenMoneyBox\*.*' +1 Err
	DetailPrint "Shortcut folder:		OK"
	SetOutPath $INSTDIR
	CreateShortCut "$SMPROGRAMS\OpenMoneyBox\$(ProductName).lnk" "$INSTDIR\OpenMoneyBox.exe"
	IfErrors Err1
;	Call ClearAutoLog
	DetailPrint "OpenMoneyBox:			OK"
	GoTo Short1
	Err1:
;	Call ClearAutoLog
	DetailPrint "OpenMoneyBox:			KO!"
	StrCpy $0 "ERR"
	Short1:
	StrCmp $LANGUAGE ${LANG_ENGLISH} 0 +2
	CreateShortCut "$SMPROGRAMS\OpenMoneyBox\License.lnk" "$INSTDIR\License.txt"
	StrCmp $LANGUAGE ${LANG_ITALIAN} 0 +2
	CreateShortCut "$SMPROGRAMS\OpenMoneyBox\Licenza.lnk" "$INSTDIR\Licenza.txt"
	IfErrors Err3
;	Call ClearAutoLog
	DetailPrint "License:			OK"
	GoTo Short3
	Err3:
;	Call ClearAutoLog
	DetailPrint "License:			KO!"
	StrCpy $0 "ERR"
	Short3:
	StrCmp $0 "ERR" Err +1
	DetailPrint ""
	Return

	Err:
;	Call ClearAutoLog
	DetailPrint "Shortcuts:			KO!"
	Call ErrLOG
	DetailPrint ""
SectionEnd

Section "-Registry"
	DetailPrint "[REGISTRY]"
	WriteRegStr "HKLM" "${SW_Key}" "Path" "$INSTDIR"
	IfErrors Err1
	DetailPrint "Installation path:		OK"
	GoTo Reg2
	Err1:
	DetailPrint "Installation path:		KO!"
	Reg2:

	; Install type storage
;	Inst:
	WriteRegDWORD "HKLM" "${SW_Key}" "InstallType" $InstallType

	; Installed Languages
	WriteRegDWORD "HKLM" "${SW_Key}" "Languages" $LangFlags

	;Class:
	WriteRegStr "HKCR" ".omb" "" "omb_file"
	IfErrors ErrClass
	StrCmp $LANGUAGE ${LANG_ENGLISH} 0 +2
	WriteRegStr "HKCR" "omb_file" "" "OpenMoneyBox document"
	StrCmp $LANGUAGE ${LANG_ITALIAN} 0 +2
	WriteRegStr "HKCR" "omb_file" "" "$\"Documento di Portamonete$\""
	IfErrors ErrClass
	WriteRegStr "HKCR" "omb_file\DefaultIcon" "" "$INSTDIR\openmoneybox.exe,1"
	IfErrors ErrClass
	WriteRegStr "HKCR" "omb_file\Shell\Open\Command" "" "$\"$INSTDIR\openmoneybox.exe$\" $\"%1$\""
	IfErrors ErrClass
	DetailPrint "Class:				OK"
	DetailPrint ""
	Goto Version
	ErrClass:
	DetailPrint "Class:				KO!"
	DetailPrint ""
	Version:
	Call SaveInstallerVersion

	; Skip registry refresh if latest key is available
	ReadRegStr $0 "HKCU" "SOFTWARE\igiSW\OpenMoneyBox\3.4\General" "Default"
	StrCmp $0 "" RefreshDone +1

	; Update HKCU registry for previous installations
	; v3.4
	;FirstRun
	ReadRegStr $0 "HKCU" "SOFTWARE\igiSW\OpenMoneyBox\3.4" "FirstRun"
	StrCmp $0 "" HKCUDone +1
	${COPY_REGISTRY_KEY} "HKCU" "SOFTWARE\igiSW\OpenMoneyBox\3.4" "HKCU" "${SW_Key}"
	Goto HKCUDone

	; Update HKCU registry for previous installations
	; v3.2
	;FirstRun
	ReadRegStr $0 "HKCU" "SOFTWARE\igiSW\OpenMoneyBox\3.2" "FirstRun"
	StrCmp $0 "" HKCUDone +1
	${COPY_REGISTRY_KEY} "HKCU" "SOFTWARE\igiSW\OpenMoneyBox\3.2" "HKCU" "${SW_Key}"
	Goto HKCUDone

	; v3.1
	;FirstRun
	ReadRegStr $0 "HKCU" "SOFTWARE\igiSW\OpenMoneyBox\3.1" "FirstRun"
	StrCmp $0 "" HKCUDone +1
	${COPY_REGISTRY_KEY} "HKCU" "SOFTWARE\igiSW\OpenMoneyBox\3.1" "HKCU" "${SW_Key}"
	;Goto HKCUDone

	HKCUDone:

	;Run
	!ifdef MONOLITHIC
		WriteRegStr HKCU Software\Microsoft\Windows\CurrentVersion\Run igisw_openmoneybox "$\"$INSTDIR\openmoneybox.exe$\" --tray --force"
	!else
		WriteRegStr HKCU Software\Microsoft\Windows\CurrentVersion\Run igisw_openmoneybox "$INSTDIR\ombtray.exe"
	!endif

	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox0304"

	RefreshDone:
SectionEnd

Function SaveInstallerVersion
	WriteRegDWord "HKLM" "${SW_Key}" "InstallerVersion" "${REG_VER}"
FunctionEnd

Section "-Control Panel"
	;Control Panel
	DetailPrint "[CONTROL PANEL]"
	ClearErrors
	!ifdef ALPHA
		WriteRegStr HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str} DisplayName "OpenMoneyBox ${MAJ_VER_str}.${MIN_VER_str} alpha"
	!else
		!ifdef BETA
			WriteRegStr HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str} DisplayName "OpenMoneyBox ${MAJ_VER_str}.${MIN_VER_str} beta"
		!else
			WriteRegStr HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str} DisplayName "OpenMoneyBox ${MAJ_VER_str}.${MIN_VER_str}"
		!endif
	!endif
	IfErrors Err1
	DetailPrint "Registry key:			OK"
	GoTo Reg2
	Err1:
	DetailPrint "Registry key:			KO!"
	GoTo RegEnd
	Reg2:
	WriteRegStr HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str} UninstallString "$INSTDIR\Uninst.exe"
	IfErrors Err2
	DetailPrint "Uninstall key:			OK"
	GoTo Reg3
	Err2:
	DetailPrint "Uninstall key:			KO!"
	Reg3:
	WriteRegDWORD HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str} NoModify 1
	WriteRegStr HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str} ModifyPath "FALSE"
	WriteRegStr HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str} DisplayIcon "$INSTDIR\openmoneybox.exe"
	WriteRegStr HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str} Publisher "igiSW"
	WriteRegStr HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str} DisplayVersion "${MAJ_VER_str}.${MIN_VER_str}.${RELEASE_str}.${BUILD_str}"
	WriteRegStr HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str} URLInfoAbout "https://launchpad.net/bilancio"
	RegEnd:
	DetailPrint ""
SectionEnd

Section "-UI"
	;Uninstaller
	DetailPrint "[UNINSTALL]"
	WriteUninstaller $INSTDIR\Uninst.exe
	IfErrors Err1
;	Call ClearAutoLog
	DetailPrint "Uninstaller:			OK"
	GoTo Un2
	Err1:
;	Call ClearAutoLog
	DetailPrint "Uninstaller:			KO!"
	Un2:
	SetShellVarContext current
	;Call ClearAutoLog
	CreateDirectory '$SMPROGRAMS\OpenMoneyBox'
	StrCmp $LANGUAGE ${LANG_ENGLISH} 0 +2
	CreateShortCut "$SMPROGRAMS\OpenMoneyBox\Uninstall.lnk" "$INSTDIR\Uninst.EXE"
	StrCmp $LANGUAGE ${LANG_ITALIAN} 0 +2
	CreateShortCut "$SMPROGRAMS\OpenMoneyBox\Disinstalla.lnk" "$INSTDIR\Uninst.EXE"
;	Call ClearAutoLog
	IfErrors Err2
	DetailPrint "Uninstall shortcut:		OK"
	Goto UIEnd
	Err2:
	DetailPrint "Uninstall shortcut:		KO!"
	UIEnd:
	DetailPrint ""
	Call SaveLOG
;	IntCmp $OmbTrayInstalled 1 RunTray +1
;	Return

;	RunTray:
	!ifdef MONOLITHIC
		Exec "$INSTDIR\openmoneybox.exe --tray --force"
	!else
		Exec "$INSTDIR\ombtray.exe"
	!endif
SectionEnd

Section "Uninstall"
	SetAutoClose true
	;Shortcuts
	SetShellVarContext current
	Delete "$SMPROGRAMS\OpenMoneyBox\*"
	RMDir "$SMPROGRAMS\OpenMoneyBox"
;	Delete "$SMSTARTUP\Ombtray.lnk"
	;Common files
	;Program
	SetOutPath "$TEMP"
	RMDir /r /REBOOTOK "$INSTDIR"
	;Classes
	DeleteRegKey HKCR ".omb"
	DeleteRegKey HKCR "omb_file"
	;Control Panel
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\igiSW_OpenMoneyBox${MAJ_VER_str}${MIN_VER_str}"
	MessageBox MB_OK $(Success)
SectionEnd

;Function ClearAutoLog
;	Push "1"
;	Call DeleteFromEnd
;FunctionEnd

Function ErrLOG
	Call SaveLOG
	Abort $(InstallKO)
FunctionEnd

Function SaveLOG
	GetTempFileName $0
	Push $0
	Call DumpLog
	IfFileExists "$INSTDIR\install.log" +1 Save
	Delete "$INSTDIR\install.log"
	Save:
	Rename $0 "$INSTDIR\install.log"
FunctionEnd

Function .onInit
	;Language selection dialog

	Push ""
	Push ${LANG_ENGLISH}
	Push English
	Push ${LANG_ITALIAN}
	Push Italiano
	Push A ; A means auto count languages
	       ; for the auto count to work the first empty push (Push "") must remain
	LangDLL::LangDialog "Installer Language" "Please select the language of the installer"

	Pop $LANGUAGE
	StrCmp $LANGUAGE "cancel" 0 +2
	Abort

	; Update HKLM registry for previous installations
	; v3.2
	ReadRegStr $0 "HKLM" "SOFTWARE\igiSW\OpenMoneyBox\3.4" "Path"
	StrCmp $0 "" UpdateDone +1
	${COPY_REGISTRY_KEY} "HKLM" "SOFTWARE\igiSW\OpenMoneyBox\3.4" "HKLM" "${SW_Key}"
	Goto UpdateDone

	; Update HKLM registry for previous installations
	; v3.2
	ReadRegStr $0 "HKLM" "SOFTWARE\igiSW\OpenMoneyBox\3.2" "Path"
	StrCmp $0 "" UpdateDone +1
	${COPY_REGISTRY_KEY} "HKLM" "SOFTWARE\igiSW\OpenMoneyBox\3.2" "HKLM" "${SW_Key}"
	Goto UpdateDone

	; v3.1
	ReadRegStr $0 "HKLM" "SOFTWARE\igiSW\OpenMoneyBox\3.1" "Path"
	StrCmp $0 "" UpdateDone +1
	${COPY_REGISTRY_KEY} "HKLM" "SOFTWARE\igiSW\OpenMoneyBox\3.1" "HKLM" "${SW_Key}"
	;Goto UpdateDone

	UpdateDone:
	StrCpy $InstallType 0
	Call CheckInstallationParameters

;	Call CheckPrevInstallation	;To install patches only.
;	Pop $0
;	StrCmp $0 "Patched" +1 +3
;	MessageBox MB_OK $(PatchInstalled)
;	Quit
	Return
FunctionEnd

Function .onInstFailed
	MessageBox MB_OK $(InstallUncomplete)
	Quit
FunctionEnd

Function .onSelChange
	GetCurInstType $InstallType
FunctionEnd

Function un.onUninstFailed
	MessageBox MB_OK $(UninstallUncomplete)
	Quit
FunctionEnd

Function CheckInstallationParameters
	; Last installation path check
	ReadRegStr $0 "HKLM" "${SW_Key}" "Path"
	StrCmp $0 "" +1 AlreadyInstalled
	StrCpy $INSTDIR "$PROGRAMFILES\OpenMoneyBox"
	Goto Type
	AlreadyInstalled:
	StrCpy $INSTDIR $0

	; Last installation type check
	Type:
	ReadRegStr $0 "HKLM" "${SW_Key}" "InstallType"
	StrCmp $0 "" +1 RestoreType
	Return
	RestoreType:
	StrCpy $InstallType $0
FunctionEnd

;	Function CreateGUID
;		System::Call 'ole32::CoCreateGuid(g .s)'
;	FunctionEnd

Function ShowComp
	SetCurInstType $InstallType	; to be fixed
FunctionEnd
