# (C) Igor Calì 15/02/2025 (igor.cali0@gmail.com)

#Product identification
# DEVMARKER COULD BE EITHER b (beta) or rc (release candidate)
MAJVER = 3.5
DEVMARKER =
VERSION = $(MAJVER)$(DEVMARKER).1

OMBNAME = libigiomb-$(MAJVER).so
BERRNAME = libomberr-$(MAJVER).so
OPTNAME = libombopt-$(MAJVER).so
WIZARDNAME = libombwizard-$(MAJVER).so
UPDATENAME = libombupdate-$(MAJVER).so
APPNAME = openmoneybox
INDNAME = ombindicator
TRAYNAME = ombtray
CONVERTNAME = ombconvert
MAPVIEWERNAME = ombmapviewer

# Build information
# Override DEBUG with 1 to create debug build
DEBUG ?= 0
# override DEBIAN with 0 for non Debian installation
DEBIAN ?= 1
# Override INCREMENT with 1 to increment the build number 
INCREMENT ?= 0
# override WXPATH with path folder for custom build
WXPATH ?=
# Override with _OMB_CHART_MATLIBPLOT to build with MathPlotLib
#CHART_LIB ?= _OMB_CHART_MATLIBPLOT
CHART_LIB ?= _OMB_CHART_WXPIECTRL
# Override FLATPAK with 1 to package Flatpak
FLATPAK ?= 0
# Override APPIMAGE with 1 to package AppImage
APPIMAGE ?= 0
# Override CONVERT_BUILD with 1 to build ombconvert
CONVERT_BUILD ?= 0
# Override WARNING_ONLY with 1 not to stop building on warnings
WARNING_ONLY ?= 0
# SqlCipher v4 compatibility
OMB_SQLCIPHER_V4 ?= 0

# Android
ANDROID_FOLDER ?= android/
#override ANDROID_API_LEVEL for other Android versions
ANDROID_API_LEVEL ?= API33

SRCDIR = src/
includes = $(wildcard src/*.h)
ifeq ($(DEBUG),1)
	OBJDIR = _gtkd/
else
	OBJDIR = _gtk/
endif
BUILDDIR = 
# Name of text file containing build number.
BUILD_NUMBER_FILE=build-number.txt
# Create an auto-incrementing build number.
BUILD_NUMBER_LDFLAGS = -Xlinker --defsym -Xlinker __BUILD_DATE=$$(date +'%Y%m%d')
BUILD_NUMBER_LDFLAGS += -Xlinker --defsym -Xlinker __BUILD_NUMBER=$$(cat $(BUILD_NUMBER_FILE))

# wxWidgets configuration;
CXX = $(shell wx-config --cxx)
ifeq ($(DEBUG),1)
	CXXFLAGS = `wx-config --debug --cxxflags`
	LDLIBS = `wx-config --debug --libs`
else
	CXXFLAGS = `$(WXPATH)wx-config --cxxflags`
	LDLIBS = `$(WXPATH)wx-config --libs`
endif
WXVER = $(shell wx-config --release)

# determine linux distribution
DISTRO ?= $(shell lsb_release -i -s 2>/dev/null)
CODENAME = $(shell lsb_release -c -s 2>/dev/null)

# default features
SETUP_SHELL =
SETUP_POSTIFIX = 
GTK_VERSION = gtk2u
APPINDICATOR = 1
TOOL_ICON_SIZE = 24
FLAVOUR_FLAGS = 
WXSQLITE_VERSIONPOSTFIX = -3.0
INDICATOR_FLAGS = 
CONVERT_FLAGS = -DwxUSE_GUI=0
OMBUPDATE = 0
UPDATE_FLAGS =
UPDATE_LIBS =
USE_GSETTINGS = 1
GSETTINGS_FLAGS =
GSETTINGS_LIBS =
OMB_MAPVIEWER = 1
OMB_USE_SQLCIPHER = 1
OPTION_FLAGS =
OMB_MONOLITHIC = 1
YELP = 1
USE_LIBXML2 = 1
NOTIFYSEND_APPSWITCH = 0

# compiler flags
ifeq ($(WARNING_ONLY),0)
COMPILE_FLAGS += -Werror
endif
# Disable debug asserts in release builds
ifeq ($(DEBUG),0)
# Disabled to avoid wxMainFrame crashes with wxWidgets 3.0.5; set for specific distros
#	CXXFLAGS += -DNDEBUG
endif
ifeq ($(DEBUG),1)
# Enable to include debug information
#COMPILE_FLAGS += -g
endif

# OS flavour configuration
# step1: distro check
ifneq ($(DISTRO),Ubuntu)
	APPINDICATOR = 0
endif
ifeq ($(DISTRO),Debian)
	APPINDICATOR = 1
# Determine if ayatana appindicator lib is installed
	AYATANA_INDICATOR_LIB_INFO := $(shell pkg-config ayatana-appindicator3-0.1 --cflags; echo $$?)

	BUILD_NUMBER_LDFLAGS += -no-pie
ifeq ($(CODENAME),bookworm)
	CXXFLAGS += -DNDEBUG
endif
endif
ifeq ($(DISTRO),Raspbian)
	BUILD_NUMBER_LDFLAGS += -no-pie
	YELP = 0
ifeq ($(CODENAME),bookworm)
	CXXFLAGS += -DNDEBUG
endif
endif
ifeq ($(DISTRO),Fedora)
	SETUP_POSTIFIX = .fedora
	GTK_VERSION = gtk3u
	FLAVOUR_FLAGS += ${CFLAGS}
	WXSQLITE_VERSIONPOSTFIX = 
	OMBUPDATE = 1
	APPINDICATOR = 1
	
	OMB_SQLCIPHER_V4 = 1
	
	# wxWidgets 3.2
	CXXFLAGS += -DNDEBUG
endif
ifeq ($(DISTRO),openSUSE)
	APPINDICATOR = 1
	FLAVOUR_FLAGS += -D__OPENSUSE__
	OMBUPDATE = 1

	OMB_SQLCIPHER_V4 = 1
	
	# wxWidgets 3.2
	CXXFLAGS += -DNDEBUG
endif
# old SUSE distro name
ifeq ($(DISTRO),openSUSE project)
	FLAVOUR_FLAGS += -D__OPENSUSE__
	OMBUPDATE = 1
endif
ifeq ($(DISTRO),Arch)
	OMB_MAPVIEWER = 0
	BUILD_NUMBER_LDFLAGS += -no-pie
	OMBUPDATE = 1
	
	OMB_SQLCIPHER_V4 = 1
	
	# wxWidgets 3.2
	CXXFLAGS += -DNDEBUG
	YELP = 0
	
	NOTIFYSEND_APPSWITCH = 1
endif
ifeq ($(FLATPAK),1)
	USE_GSETTINGS = 0
	OMB_MAPVIEWER = 0
	BUILD_NUMBER_LDFLAGS += -no-pie
	FLAVOUR_FLAGS += -D__FLATPAK__
	
	OMB_SQLCIPHER_V4 = 1
	
	# wxWidgets 3.2
	CXXFLAGS += -DNDEBUG
endif
ifeq ($(APPIMAGE),1)
	USE_GSETTINGS = 0
#	OMB_MAPVIEWER = 0
	YELP = 0
	BUILD_NUMBER_LDFLAGS += -no-pie
	FLAVOUR_FLAGS += -D__APPIMAGE__
	
	OMB_SQLCIPHER_V4 = 1
	
	# wxWidgets 3.2
	CXXFLAGS += -DNDEBUG
endif
# step 2: codename check
ifeq ($(CODENAME),plucky)
# -no-pie is to make sure build number link symbol is in a fixed address - https://stackoverflow.com/questions/40095236/symbols-defined-with-defsym-give-incorrect-address-on-ubuntu-16-10
	BUILD_NUMBER_LDFLAGS += -no-pie
# Determine if ayatana appindicator lib is installed
	AYATANA_INDICATOR_LIB_INFO := $(shell pkg-config ayatana-appindicator3-0.1 --cflags; echo $$?)
	NOTIFYSEND_APPSWITCH = 1
	# wxWidgets 3.2
	CXXFLAGS += -DNDEBUG -D__UBUNTU_2404__
	
	UPDATE_YARU_THEME = 1
	
	OMB_SQLCIPHER_V4 = 1
endif
ifeq ($(CODENAME),oracular)
# -no-pie is to make sure build number link symbol is in a fixed address - https://stackoverflow.com/questions/40095236/symbols-defined-with-defsym-give-incorrect-address-on-ubuntu-16-10
	BUILD_NUMBER_LDFLAGS += -no-pie
# Determine if ayatana appindicator lib is installed
	AYATANA_INDICATOR_LIB_INFO := $(shell pkg-config ayatana-appindicator3-0.1 --cflags; echo $$?)
	NOTIFYSEND_APPSWITCH = 1
	# wxWidgets 3.2
	CXXFLAGS += -DNDEBUG -D__UBUNTU_2404__
	
	UPDATE_YARU_THEME = 1
	
	OMB_SQLCIPHER_V4 = 1
endif
ifeq ($(CODENAME),noble)
# -no-pie is to make sure build number link symbol is in a fixed address - https://stackoverflow.com/questions/40095236/symbols-defined-with-defsym-give-incorrect-address-on-ubuntu-16-10
	BUILD_NUMBER_LDFLAGS += -no-pie
# Determine if ayatana appindicator lib is installed
	AYATANA_INDICATOR_LIB_INFO := $(shell pkg-config ayatana-appindicator3-0.1 --cflags; echo $$?)
	NOTIFYSEND_APPSWITCH = 1
	# wxWidgets 3.2
	CXXFLAGS += -DNDEBUG -D__UBUNTU_2404__
	
	UPDATE_YARU_THEME = 1
	
	OMB_SQLCIPHER_V4 = 1
endif
ifeq ($(CODENAME),jammy)
# -no-pie is to make sure build number link symbol is in a fixed address - https://stackoverflow.com/questions/40095236/symbols-defined-with-defsym-give-incorrect-address-on-ubuntu-16-10
	BUILD_NUMBER_LDFLAGS += -no-pie
# Determine if ayatana appindicator lib is installed
	AYATANA_INDICATOR_LIB_INFO := $(shell pkg-config ayatana-appindicator3-0.1 --cflags; echo $$?)
endif
ifeq ($(CODENAME),focal)
# -no-pie is to make sure build number link symbol is in a fixed address - https://stackoverflow.com/questions/40095236/symbols-defined-with-defsym-give-incorrect-address-on-ubuntu-16-10
	BUILD_NUMBER_LDFLAGS += -no-pie
endif
ifeq ($(CODENAME),bionic)
# -no-pie is to make sure build number link symbol is in a fixed address - https://stackoverflow.com/questions/40095236/symbols-defined-with-defsym-give-incorrect-address-on-ubuntu-16-10
	BUILD_NUMBER_LDFLAGS += -no-pie
endif
ifeq ($(CODENAME),xenial)
	OMB_USE_SQLCIPHER = 0
	USE_LIBXML2 = 0
	CXXFLAGS += -std=c++11
endif
ifeq ($(CODENAME),trusty)
	OMB_MAPVIEWER = 0
	OMB_USE_SQLCIPHER = 0
	USE_LIBXML2 = 0
	WARNING_ONLY = 1
	CXXFLAGS += -std=c++11
endif

# step 3: configuration

#wxsqlite3 configuration
ifeq ($(OMB_USE_SQLCIPHER),1)
	SQL3FLAGS = `pkg-config sqlcipher --cflags` -DSQLITE_HAS_CODEC -D_OMB_USE_CIPHER
	SQL3LIBS = `pkg-config sqlcipher --libs`
	OPTION_FLAGS += -D_OMB_USE_CIPHER
else
ifneq ($(CODENAME),trusty)
	SQL3FLAGS = `pkg-config wxsqlite3$(WXSQLITE_VERSIONPOSTFIX) --cflags`
	SQL3LIBS = `pkg-config wxsqlite3$(WXSQLITE_VERSIONPOSTFIX) --libs`
else
	WXSQLITE_VERSION = 3.4.1
	SQL3FLAGS = -I3rdparty/wxsqlite3-$(WXSQLITE_VERSION)/include
	SQL3LIBS = lib/libwxcode_$(GTK_VERSION)_wxsqlite3-$(WXVER).so
endif
endif

ifeq ($(APPINDICATOR),1)
	INDICATOR_FLAGS = -D_OMB_USEINDICATOR
endif

ifeq ($(OMB_MONOLITHIC),1)
OMBNAME = 
BERRNAME = 
OPTNAME = 
WIZARDNAME = 
UPDATENAME = 
TRAYNAME = 

COMPILE_FLAGS += -D_OMB_MONOLITHIC
endif

ifeq ($(YELP),1)
COMPILE_FLAGS += -D_YELP
endif

ifeq ($(OMB_SQLCIPHER_V4),1)
	COMPILE_FLAGS += -D_OMB_SQLCIPHER_v4
endif

# Chart build configuration
ifeq ($(CHART_LIB),_OMB_CHART_MATLIBPLOT)
	CHART_FLAGS = -D_OMB_CHART_MATLIBPLOT
else
	CHART_FLAGS = -D_OMB_CHART_WXPIECTRL
endif

ifeq ($(OMBUPDATE),1)
	UPDATE_FLAGS = -D_OMB_INSTALLEDUPDATE
	UPDATE_LIBS = $(BUILDDIR)$(UPDATENAME)
endif
ifeq ($(USE_GSETTINGS),1)
	GSETTINGS_FLAGS = `pkg-config gio-2.0 --cflags` -D_OMB_USE_GSETTINGS
	GSETTINGS_LIBS = `pkg-config gio-2.0 --libs`
endif

# libxml2 configuration
ifeq ($(USE_LIBXML2),1)
	LIBXML_FLAGS = `pkg-config libxml-2.0 --cflags` -D_OMB_USE_LIBXML2
	LIBXML_LIBS = `pkg-config libxml-2.0 --libs`
endif

# notify-send configuration
ifeq ($(NOTIFYSEND_APPSWITCH),1)
	COMPILE_FLAGS += -D_OMB_NOTIFYSENDAPPSWITCH
endif

# Sources and Objects definition
ifeq ($(OMB_MONOLITHIC),0)
OBJECTS_OMB = $(OBJDIR)igiomb/igiomb.o $(OBJDIR)platformsetup.o
OBJECTS_BERR = $(OBJDIR)omberr/omberr.o
OBJECTS_OPT = $(OBJDIR)ombopt/ombopt.o $(OBJDIR)ombopt/ui/opzio.o $(OBJDIR)ombopt/ui/wxoption.o $(OBJDIR)ombopt/omb35opt.o
ifeq ($(OMBUPDATE),1)
	OBJECTS_OPT += $(OBJDIR)ombopt/ui/wxadvsheet.o
endif
OBJECTS_WIZ = $(OBJDIR)ombwizard/ombwiz.o $(OBJDIR)ombwizard/ui/wiz.o $(OBJDIR)ombwizard/ui/wxwizard.o
ifeq ($(OMBUPDATE),1)
	OBJECTS_UPD = $(OBJDIR)ombupdate/ombupdate.o
endif
endif
OBJECTS_APP = $(OBJDIR)omb35core.o $(OBJDIR)dbformat.o $(OBJDIR)openmoneybox/openmoneybox.o $(OBJDIR)openmoneybox/ui/funds.o $(OBJDIR)openmoneybox/ui/gainexpense.o \
	$(OBJDIR)openmoneybox/ui/getbackobj.o $(OBJDIR)openmoneybox/ui/main_wx.o $(OBJDIR)openmoneybox/ui/objct.o $(OBJDIR)openmoneybox/ui/remcreddeb.o $(OBJDIR)openmoneybox/ui/setcreddeb.o \
	$(OBJDIR)openmoneybox/ui/wxgainexpense.o $(OBJDIR)openmoneybox/ui/wxgetbackf.o $(OBJDIR)openmoneybox/ui/wxobjct.o $(OBJDIR)openmoneybox/ui/wxremcreddeb.o $(OBJDIR)openmoneybox/ui/wxsetcreddeb.o \
	$(OBJDIR)openmoneybox/ui/wxtopf.o $(OBJDIR)ui/password.o $(OBJDIR)ui/wxpassword.o \
	$(OBJDIR)openmoneybox/ui/editcategories.o $(OBJDIR)openmoneybox/ui/wxeditcategories.o $(OBJDIR)openmoneybox/ui/wxgridimagerenderer.o $(OBJDIR)productversion.o \
	$(OBJDIR)omblogo/omblogolib.o $(OBJDIR)openmoneybox/ui/topexpensepanel.o $(OBJDIR)ombtray/ui/Alarm.o $(OBJDIR)ombtray/ui/wxAlarm.o
ifeq ($(CHART_LIB),_OMB_CHART_WXPIECTRL)
	OBJECTS_APP += $(OBJDIR)openmoneybox/ui/wxPieCtrl.o $(OBJDIR)openmoneybox/ui/viewstatistics.o
endif
ifeq ($(CHART_LIB),_OMB_CHART_MATLIBPLOT)
	OBJECTS_APP += $(OBJDIR)openmoneybox/ui/wxmainframe_$(TOOL_ICON_SIZE)_mpl.o
else
	OBJECTS_APP += $(OBJDIR)openmoneybox/ui/wxmainframe_$(TOOL_ICON_SIZE).o
endif
ifeq ($(OMB_USE_SQLCIPHER),1)
	OBJECTS_APP += $(OBJDIR)wxsqlite3.o
endif
ifeq ($(OMB_MONOLITHIC),1)
	OBJECTS_APP += $(OBJDIR)igiomb/igiomb.o $(OBJDIR)platformsetup.o
	OBJECTS_APP += $(OBJDIR)omberr/omberr.o
	OBJECTS_APP += $(OBJDIR)ombopt/ombopt.o $(OBJDIR)ombopt/ui/opzio.o $(OBJDIR)ombopt/ui/wxoption.o $(OBJDIR)ombopt/omb35opt.o
	OBJECTS_APP += $(OBJDIR)ombwizard/ombwiz.o $(OBJDIR)ombwizard/ui/wiz.o $(OBJDIR)ombwizard/ui/wxwizard.o
	OBJECTS_APP += $(OBJDIR)ombtray/ombtaskbar.o
ifeq ($(OMBUPDATE),1)
	OBJECTS_APP += $(OBJDIR)ombopt/ui/wxadvsheet.o
endif
endif
ifeq ($(OMBUPDATE),1)
	OBJECTS_APP += $(OBJDIR)ombupdate/ombupdate.o
endif
OBJECTS_APP += $(OBJDIR)openmoneybox/ui/wxFundGroups.o $(OBJDIR)openmoneybox/ui/ombFundGroups.o

ifeq ($(OMB_MONOLITHIC),0)
OBJECTS_TRAY = $(OBJDIR)ombtray/ui/Alarm.o $(OBJDIR)ombtray/ombtrayapp.o $(OBJDIR)ombtray/ui/wxAlarm.o $(OBJDIR)dbformat.o
ifeq ($(OMB_USE_SQLCIPHER),1)
	OBJECTS_TRAY += $(OBJDIR)wxsqlite3.o
endif
endif

ifeq ($(CONVERT_BUILD),1)
OBJECTS_CONV = $(OBJDIR)ombconvert/ombconvert.o $(OBJDIR)ombconvert/dataconverter.o $(OBJDIR)dbformat.o $(OBJDIR)ombconvert/omb35core.o
ifeq ($(OMB_USE_SQLCIPHER),1)
	OBJECTS_CONV += $(OBJDIR)wxsqlite3.o
endif
endif

# Install information
DESTDIR ?= 
ifeq ($(DISTRO),Arch)
DESTPATH = /usr/bin
BINDIR= $(DESTDIR)$(DESTPATH)
DATADIR = $(DESTDIR)/etc/openmoneybox/
LIBDIR = $(DESTDIR)/usr/lib/
SHAREDIR =$(DESTDIR)/usr/share/
GNOMEDIR =$(DESTDIR)/etc/gnome/
else
DESTPATH = /bin
BINDIR= $(DESTDIR)$(DESTPATH)
DATADIR = $(DESTDIR)/etc/openmoneybox/
LIBDIR = $(DESTDIR)/lib/
ifeq ($(FLATPAK),0)
ifeq ($(APPIMAGE),1)
SHAREDIR =$(DESTDIR)/share/
else
SHAREDIR =$(DESTDIR)/usr/share/
endif
else
SHAREDIR =$(DESTDIR)/share/
endif
GNOMEDIR =$(DESTDIR)/etc/gnome/
endif
YELPDIR = $(SHAREDIR)help/

# Manual docbook templates
MANUAL_POTS = help/en/AddShopItem.pot help/en/Expense_operations.pot \
	help/en/Alarms.pot help/en/Export_file.pot help/en/Options.pot \
	help/en/Author.pot help/en/Find.pot help/en/Password.pot \
	help/en/ombtray.pot help/en/Gain_operations.pot help/en/category_select.pot help/en/category_edit.pot \
	help/en/Borrow.pot help/en/GetBack.pot help/en/Remove_fund.pot \
	help/en/Copy.pot help/en/GiveBack.pot help/en/Remove_item.pot \
	help/en/Credit_remit.pot help/en/Global_datetime.pot help/en/Revert_file.pot \
	help/en/Credit_remove.pot help/en/History.pot \
	help/en/Credit_set.pot help/en/Introduction.pot help/en/Save_file.pot \
	help/en/Debt_remit.pot help/en/Lend.pot help/en/Shortcuts.pot \
	help/en/Debt_remove.pot help/en/Manual_time.pot help/en/Total_fund.pot \
	help/en/Debt_set.pot help/en/Update.pot \
	help/en/Default_fund.pot help/en/New_fund.pot help/en/Wizard.pot \
	help/en/Edit_fund.pot help/en/Object_Get.pot \
	help/en/Exit.pot help/en/Object_Given.pot \
	help/en/browse_archive.pot help/en/convert.pot help/en/import.pot help/en/map.pot help/en/custom_currency.pot help/en/command_line.pot \
	help/en/FundGroups.pot

MANUAL_TRANS = AddShopItem History Alarms Introduction Author Lend ombtray Manual_time Borrow Copy New_fund Credit_remit Object_Get Credit_remove Object_Given \
	Credit_set Debt_remit Options Debt_remove Password Debt_set category_edit category_select Default_fund Remove_fund Edit_fund Remove_item Exit Revert_file \
	Expense_operations Export_file Save_file Find Shortcuts Gain_operations Total_fund GetBack Update GiveBack Wizard Global_datetime \
	browse_archive convert import map custom_currency keep_budget command_line FundGroups

all: createbuildfolders $(LOGONAME) $(OMBNAME) $(BERRNAME) $(OPTNAME) $(WIZARDNAME) $(UPDATENAME) $(APPNAME) $(INDNAME) $(TRAYNAME) $(CONVERTNAME) \
	$(MAPVIEWERNAME)
	@echo
	@echo $(APPNAME) v. $(VERSION).$$(($$(cat $(BUILD_NUMBER_FILE)))) built.

toolchainsetup:
ifeq ($(DEBIAN),0)
	echo $(DISTRO)
else
ifeq ($(CODENAME),noble)
	scripts/setup_noble
else
ifeq ($(CODENAME),jammy)
	scripts/setup_jammy
else
ifeq ($(CODENAME),bookworm)
	scripts/setup_noble
else
ifeq ($(DISTRO),Raspbian)
	scripts/setup_raspbian
else
	scripts/setup
endif
endif
endif
endif
endif

createbuildfolders:
# object folders
	@if ! test -d $(OBJDIR)openmoneybox/ui; then mkdir -p $(OBJDIR)openmoneybox/ui; fi
	@if ! test -d $(OBJDIR)ombopt/ui; then mkdir -p $(OBJDIR)ombopt/ui; fi
	@if ! test -d $(OBJDIR)ombtray/ui; then mkdir -p $(OBJDIR)ombtray/ui; fi
	@if ! test -d $(OBJDIR)ombwizard/ui; then mkdir -p $(OBJDIR)ombwizard/ui; fi
	@if ! test -d $(OBJDIR)ombupdate; then mkdir -p $(OBJDIR)ombupdate; fi
	@if ! test -d $(OBJDIR)omberr; then mkdir -p $(OBJDIR)omberr; fi
	@if ! test -d $(OBJDIR)igiomb; then mkdir -p $(OBJDIR)igiomb; fi
	@if ! test -d $(OBJDIR)omblogo/ui; then mkdir -p $(OBJDIR)omblogo/ui; fi
	@if ! test -d $(OBJDIR)ui; then mkdir -p $(OBJDIR)ui; fi
	@if ! test -d $(OBJDIR)ombconvert; then mkdir -p $(OBJDIR)ombconvert; fi
	@if ! test -d $(OBJDIR)ombmapviewer; then mkdir -p $(OBJDIR)ombmapviewer; fi
ifeq ($(CODENAME),trusty)
	@if ! test -f lib/libwxcode_$(GTK_VERSION)_wxsqlite3-$(WXVER).so.0.0.0; then scripts/sqlite3;fi
endif

#TODO separate logo if built with __STANDALONE__
#omblogo
#$(LOGONAME): $(OBJECTS_LOGO) $(BUILD_NUMBER_FILE)
#	$(CXX) -shared $(OBJECTS_LOGO) -o $(BUILDDIR)$(LOGONAME) $(LDLIBS) $(BUILD_NUMBER_LDFLAGS) -Wl,-soname,$(LOGONAME)

#igiomb
$(OMBNAME): $(OBJECTS_OMB)
ifeq ($(OMB_MONOLITHIC),0)
ifeq ($(OMBUPDATE),1)
	$(CXX) $(COMPILE_FLAGS) -Wall -fPIC -O2 $(CHART_FLAGS) $(UPDATE_FLAGS) $(GSETTINGS_FLAGS) $(FLAVOUR_FLAGS) $(CXXFLAGS) -c $(SRCDIR)productversion.cpp -o $(BUILDDIR)$(OBJDIR)igiomb/productversion.o
	$(CXX) -shared $(OBJECTS_OMB) -o $(BUILDDIR)$(OBJDIR)igiomb/productversion.o -o $(BUILDDIR)$(OMBNAME) $(LDLIBS) $(GSETTINGS_LIBS) $(BUILD_NUMBER_LDFLAGS) -Wl,-soname,$(OMBNAME)
else
	$(CXX) -shared $(OBJECTS_OMB) -o $(BUILDDIR)$(OMBNAME) $(LDLIBS) $(GSETTINGS_LIBS) -Wl,-soname,$(OMBNAME)
endif
endif

#omberr
$(BERRNAME): $(OBJECTS_BERR)
ifeq ($(OMB_MONOLITHIC),0)
	$(CXX) -shared $(OBJECTS_BERR) -o $(BUILDDIR)$(BERRNAME) $(LDLIBS) -Wl,-soname,$(BERRNAME)
endif

#ombopt
$(OPTNAME): $(OBJECTS_OPT)
ifeq ($(OMB_MONOLITHIC),0)
	$(CXX) -shared $(OBJECTS_OPT) -o $(BUILDDIR)$(OPTNAME) $(LDLIBS) $(GSETTINGS_LIBS) -Wl,-soname,$(OPTNAME)
endif

#ombwizard
$(WIZARDNAME): $(OBJECTS_WIZ)
ifeq ($(OMB_MONOLITHIC),0)
	$(CXX) -shared $(OBJECTS_WIZ) -o $(BUILDDIR)$(WIZARDNAME) $(LDLIBS) -Wl,-soname,$(WIZARDNAME)
endif

#ombupdate
$(UPDATENAME): $(OBJECTS_UPD)
ifeq ($(OMB_MONOLITHIC),0)
ifeq ($(OMBUPDATE),1)
#$(BUILD_NUMBER_FILE)
	$(CXX) -shared $(OBJECTS_UPD) -o $(BUILDDIR)$(UPDATENAME) $(LDLIBS) $(GSETTINGS_LIBS) -Wl,-soname,$(UPDATENAME)
endif
endif

#openmoneybox
$(APPNAME): $(OBJECTS_APP) $(BUILD_NUMBER_FILE)
ifeq ($(DEBUG),1)
	$(CXX) $(OBJECTS_APP) -o $(BUILDDIR)$(APPNAME) $(LDLIBS) $(SQL3LIBS) $(GSETTINGS_LIBS) $(BUILD_NUMBER_LDFLAGS) \
		$(CHART_LIBS) $(BUILDDIR)$(LOGONAME) $(BUILDDIR)$(OMBNAME) $(BUILDDIR)$(BERRNAME) $(BUILDDIR)$(OPTNAME) $(BUILDDIR)$(WIZARDNAME) $(UPDATE_LIBS)
else
ifeq ($(OMB_USE_SQLCIPHER),1)
	$(CXX) $(OBJECTS_APP) -o $(BUILDDIR)$(APPNAME) $(LDLIBS) $(SQL3LIBS) $(GSETTINGS_LIBS) $(LIBXML_LIBS) $(BUILD_NUMBER_LDFLAGS) \
		$(CHART_LIBS) $(BUILDDIR)$(LOGONAME) $(BUILDDIR)$(OMBNAME) $(BUILDDIR)$(BERRNAME) $(BUILDDIR)$(OPTNAME) $(BUILDDIR)$(WIZARDNAME) $(UPDATE_LIBS)
else
	$(CXX) $(OBJECTS_APP) -o $(BUILDDIR)$(APPNAME) $(LDLIBS) $(SQL3LIBS) $(GSETTINGS_LIBS) $(BUILD_NUMBER_LDFLAGS) \
		$(CHART_LIBS) $(BUILDDIR)$(LOGONAME) $(BUILDDIR)$(OMBNAME) $(BUILDDIR)$(BERRNAME) $(BUILDDIR)$(OPTNAME) $(BUILDDIR)$(WIZARDNAME) $(UPDATE_LIBS)
endif
endif

#indicator
$(INDNAME): $(SRCDIR)ombtray/indicator.cpp
ifeq ($(APPINDICATOR),1)
ifeq ($(CODENAME),plucky)
# Consider what libappindicator package is installed (AYATANA_INDICATOR_LIB_INFO is 1 if ayatana not installed)
ifneq ($(AYATANA_INDICATOR_LIB_INFO),1)
	$(CXX) $(COMPILE_FLAGS) -Wall -D__UBUNTU_2204__ `pkg-config ayatana-appindicator3-0.1 --cflags` -fpermissive -O2 -c $(SRCDIR)ombtray/indicator.cpp -o $(OBJDIR)ombtray/indicator.o
	$(CXX) -o $(INDNAME) $(OBJDIR)ombtray/indicator.o `pkg-config ayatana-appindicator3-0.1 --libs` -s
else
	$(CXX) $(COMPILE_FLAGS) -Wall `pkg-config appindicator3-0.1 --cflags` -fpermissive -O2 -c $(SRCDIR)ombtray/indicator.cpp -o $(OBJDIR)ombtray/indicator.o
	$(CXX) -o $(INDNAME) $(OBJDIR)ombtray/indicator.o `pkg-config appindicator3-0.1 --libs` -s
endif
else
ifeq ($(CODENAME),oracular)
# Consider what libappindicator package is installed (AYATANA_INDICATOR_LIB_INFO is 1 if ayatana not installed)
ifneq ($(AYATANA_INDICATOR_LIB_INFO),1)
	$(CXX) $(COMPILE_FLAGS) -Wall -D__UBUNTU_2204__ `pkg-config ayatana-appindicator3-0.1 --cflags` -fpermissive -O2 -c $(SRCDIR)ombtray/indicator.cpp -o $(OBJDIR)ombtray/indicator.o
	$(CXX) -o $(INDNAME) $(OBJDIR)ombtray/indicator.o `pkg-config ayatana-appindicator3-0.1 --libs` -s
else
	$(CXX) $(COMPILE_FLAGS) -Wall `pkg-config appindicator3-0.1 --cflags` -fpermissive -O2 -c $(SRCDIR)ombtray/indicator.cpp -o $(OBJDIR)ombtray/indicator.o
	$(CXX) -o $(INDNAME) $(OBJDIR)ombtray/indicator.o `pkg-config appindicator3-0.1 --libs` -s
endif
else
ifeq ($(CODENAME),noble)
# Consider what libappindicator package is installed (AYATANA_INDICATOR_LIB_INFO is 1 if ayatana not installed)
ifneq ($(AYATANA_INDICATOR_LIB_INFO),1)
	$(CXX) $(COMPILE_FLAGS) -Wall -D__UBUNTU_2204__ `pkg-config ayatana-appindicator3-0.1 --cflags` -fpermissive -O2 -c $(SRCDIR)ombtray/indicator.cpp -o $(OBJDIR)ombtray/indicator.o
	$(CXX) -o $(INDNAME) $(OBJDIR)ombtray/indicator.o `pkg-config ayatana-appindicator3-0.1 --libs` -s
else
	$(CXX) $(COMPILE_FLAGS) -Wall `pkg-config appindicator3-0.1 --cflags` -fpermissive -O2 -c $(SRCDIR)ombtray/indicator.cpp -o $(OBJDIR)ombtray/indicator.o
	$(CXX) -o $(INDNAME) $(OBJDIR)ombtray/indicator.o `pkg-config appindicator3-0.1 --libs` -s
endif
else
ifeq ($(CODENAME),jammy)
# Consider what libappindicator package is installed (AYATANA_INDICATOR_LIB_INFO is 1 if ayatana not installed)
ifneq ($(AYATANA_INDICATOR_LIB_INFO),1)
	$(CXX) $(COMPILE_FLAGS) -Wall -D__UBUNTU_2204__ `pkg-config ayatana-appindicator3-0.1 --cflags` -fpermissive -O2 -c $(SRCDIR)ombtray/indicator.cpp -o $(OBJDIR)ombtray/indicator.o
	$(CXX) -o $(INDNAME) $(OBJDIR)ombtray/indicator.o `pkg-config ayatana-appindicator3-0.1 --libs` -s
else
	$(CXX) $(COMPILE_FLAGS) -Wall `pkg-config appindicator3-0.1 --cflags` -fpermissive -O2 -c $(SRCDIR)ombtray/indicator.cpp -o $(OBJDIR)ombtray/indicator.o
	$(CXX) -o $(INDNAME) $(OBJDIR)ombtray/indicator.o `pkg-config appindicator3-0.1 --libs` -s
endif
else
ifeq ($(DISTRO),Debian)
	$(CXX) $(COMPILE_FLAGS) -Wall -D__UBUNTU_2204__ `pkg-config ayatana-appindicator3-0.1 --cflags` -fpermissive -O2 -c $(SRCDIR)ombtray/indicator.cpp -o $(OBJDIR)ombtray/indicator.o
	$(CXX) -o $(INDNAME) $(OBJDIR)ombtray/indicator.o `pkg-config ayatana-appindicator3-0.1 --libs` -s
else
	$(CXX) $(COMPILE_FLAGS) -Wall `pkg-config appindicator3-0.1 --cflags` -fpermissive -O2 -c $(SRCDIR)ombtray/indicator.cpp -o $(OBJDIR)ombtray/indicator.o
	$(CXX) -o $(INDNAME) $(OBJDIR)ombtray/indicator.o `pkg-config appindicator3-0.1 --libs` -s
endif
endif
endif
endif
endif
endif

#tray
$(TRAYNAME): $(OBJECTS_TRAY) $(BUILD_NUMBER_FILE)
ifeq ($(OMB_MONOLITHIC),0)
	$(CXX) $(COMPILE_FLAGS) -Wall -D__OMBTRAY_EXE__ $(SQL3FLAGS) $(GSETTINGS_FLAGS) $(FLAVOUR_FLAGS) -g $(CXXFLAGS) -c $(SRCDIR)omb35core.cpp -o $(BUILDDIR)$(OBJDIR)ombtray/omb35core.o
	$(CXX) $(COMPILE_FLAGS) -Wall -D__OMBTRAY_EXE__ $(SQL3FLAGS) $(GSETTINGS_FLAGS) $(FLAVOUR_FLAGS) -g $(CXXFLAGS) -c $(SRCDIR)ombopt/omb35opt.cpp -o $(BUILDDIR)$(OBJDIR)ombtray/omb35opt.o
	$(CXX) $(COMPILE_FLAGS) -Wall -D__OMBTRAY_EXE__ $(SQL3FLAGS) $(GSETTINGS_FLAGS) $(FLAVOUR_FLAGS) -g $(CXXFLAGS) -c $(SRCDIR)ui/password.cpp -o $(BUILDDIR)$(OBJDIR)ombtray/ui/password.o
	$(CXX) $(COMPILE_FLAGS) -Wall -D__OMBTRAY_EXE__ $(SQL3FLAGS) $(GSETTINGS_FLAGS) $(FLAVOUR_FLAGS) -g $(CXXFLAGS) -c $(SRCDIR)ui/wxpassword.cpp -o $(BUILDDIR)$(OBJDIR)ombtray/ui/wxpassword.o
	$(CXX) $(COMPILE_FLAGS) -Wall -D__OMBTRAY_EXE__ $(SQL3FLAGS) $(GSETTINGS_FLAGS) $(FLAVOUR_FLAGS) -g $(CXXFLAGS) -c $(SRCDIR)productversion.cpp -o $(BUILDDIR)$(OBJDIR)ombtray/productversion.o
	$(CXX) $(OBJECTS_TRAY) $(OBJDIR)ombtray/omb35core.o $(OBJDIR)ombtray/omb35opt.o $(OBJDIR)ombtray/ui/password.o $(OBJDIR)ombtray/ui/wxpassword.o \
		$(BUILDDIR)$(OBJDIR)ombtray/productversion.o -o $(BUILDDIR)$(TRAYNAME) $(LDLIBS) $(SQL3LIBS) $(GSETTINGS_LIBS) $(BUILDDIR)$(OMBNAME) $(BUILDDIR)$(BERRNAME) \
		$(BUILDDIR)$(OPTNAME) $(BUILD_NUMBER_LDFLAGS)
endif

#ombconvert
$(CONVERTNAME): $(OBJECTS_CONV)
ifeq ($(CONVERT_BUILD),1)
	$(CXX) $(COMPILE_FLAGS) -Wall $(CONVERT_FLAGS) $(FLAVOUR_FLAGS) -D__OMBCONVERT_BIN__ $(SQL3FLAGS) -g $(CXXFLAGS) -c $(SRCDIR)omb35core.cpp -o $(BUILDDIR)$(OBJDIR)ombconvert/omb35core.o
	$(CXX) $(COMPILE_FLAGS) -Wall $(CONVERT_FLAGS) $(FLAVOUR_FLAGS) -D__OMBCONVERT_BIN__ $(SQL3FLAGS) -g $(CXXFLAGS) -c $(SRCDIR)omblogo/omblogolib.cpp -o $(BUILDDIR)$(OBJDIR)ombconvert/omblogolib.o
	$(CXX) $(COMPILE_FLAGS) -Wall $(CONVERT_FLAGS) $(FLAVOUR_FLAGS) -D__OMBCONVERT_BIN__ $(SQL3FLAGS) -g $(CXXFLAGS) -c $(SRCDIR)omberr/omberr.cpp -o $(BUILDDIR)$(OBJDIR)ombconvert/omberr.o
	$(CXX) $(COMPILE_FLAGS) -Wall $(CONVERT_FLAGS) $(FLAVOUR_FLAGS) -D__OMBCONVERT_BIN__ $(SQL3FLAGS) -g $(CXXFLAGS) -c $(SRCDIR)igiomb/igiomb.cpp -o $(BUILDDIR)$(OBJDIR)ombconvert/igiomb.o
	$(CXX) $(COMPILE_FLAGS) -Wall $(CONVERT_FLAGS) $(FLAVOUR_FLAGS) -D__OMBCONVERT_BIN__ $(SQL3FLAGS) -g $(CXXFLAGS) -c $(SRCDIR)platformsetup.cpp -o $(BUILDDIR)$(OBJDIR)ombconvert/platformsetup.o
ifeq ($(FLATPAK),0)
		$(CXX) $(OBJECTS_CONV) $(BUILDDIR)$(OBJDIR)ombconvert/omblogolib.o $(BUILDDIR)$(OBJDIR)ombconvert/omberr.o $(BUILDDIR)$(OBJDIR)ombconvert/igiomb.o \
			$(BUILDDIR)$(OBJDIR)ombconvert/omb35core.o $(BUILDDIR)$(OBJDIR)ombconvert/platformsetup.o -o $(BUILDDIR)$(CONVERTNAME) \
			$(SQL3LIBS) -lwx_baseu-$(WXVER)
else
		$(CXX) $(OBJECTS_CONV) $(BUILDDIR)$(OBJDIR)ombconvert/omblogolib.o $(BUILDDIR)$(OBJDIR)ombconvert/omberr.o $(BUILDDIR)$(OBJDIR)ombconvert/igiomb.o \
			$(BUILDDIR)$(OBJDIR)ombconvert/omb35core.o $(BUILDDIR)$(OBJDIR)ombconvert/platformsetup.o -o $(BUILDDIR)$(CONVERTNAME) \
			-L/app/lib $(SQL3LIBS) -lwx_baseu-$(WXVER)
endif
endif

#mapviewer
$(MAPVIEWERNAME): $(SRCDIR)ombmapviewer/ombmapviewer.c
ifeq ($(OMB_MAPVIEWER),1)
	$(CXX) $(COMPILE_FLAGS) -Wall `pkg-config osmgpsmap-1.0 --cflags` -fpermissive -O2 -c $(SRCDIR)ombmapviewer/ombmapviewer.c -o $(OBJDIR)ombmapviewer/ombmapviewer.o
	$(CXX) -o $(MAPVIEWERNAME) $(OBJDIR)ombmapviewer/ombmapviewer.o `pkg-config osmgpsmap-1.0 --libs` -s
endif

# Compiler configuration
ifeq ($(DEBUG),1)
$(OBJECTS_LOGO): CFLAGS := -Wall -fPIC -g
$(OBJECTS_OMB): CFLAGS := -Wall -fPIC -g $(CHART_FLAGS) $(UPDATE_FLAGS) $(GSETTINGS_FLAGS) $(FLAVOUR_FLAGS)
$(OBJECTS_BERR): CFLAGS := -Wall -fPIC -g $(SQL3FLAGS) $(FLAVOUR_FLAGS)
$(OBJECTS_OPT): CFLAGS := -Wall -fPIC -g $(UPDATE_FLAGS) $(FLAVOUR_FLAGS) $(OPTION_FLAGS)
$(OBJECTS_WIZ): CFLAGS := -Wall -fPIC -g $(SQL3FLAGS)
ifeq ($(OMB_MONOLITHIC),1)
$(OBJECTS_APP): CFLAGS := $(COMPILE_FLAGS) -Wall -D__OPENMONEYBOX_EXE__ -g $(CHART_FLAGS) $(FLAVOUR_FLAGS) $(UPDATE_FLAGS) $(SQL3FLAGS) \
	$(GSETTINGS_FLAGS) $(INDICATOR_FLAGS)
else
$(OBJECTS_APP): CFLAGS := $(COMPILE_FLAGS) -Wall -D__OPENMONEYBOX_EXE__ -g $(CHART_FLAGS) $(FLAVOUR_FLAGS) $(UPDATE_FLAGS) $(SQL3FLAGS) \
	$(GSETTINGS_FLAGS)
endif
$(OBJECTS_TRAY): CFLAGS := $(COMPILE_FLAGS) -Wall -D__OMBTRAY_EXE__ $(INDICATOR_FLAGS) $(FLAVOUR_FLAGS) $(GSETTINGS_FLAGS) $(SQL3FLAGS) -g
$(OBJECTS_CONV): CFLAGS := $(COMPILE_FLAGS) -Wall -D__LINUX__ $(CONVERT_FLAGS) $(FLAVOUR_FLAGS) -D__OMBCONVERT_BIN__ $(SQL3FLAGS) -g
else
$(OBJECTS_LOGO): CFLAGS := $(COMPILE_FLAGS) -Wall -fPIC -O2
$(OBJECTS_OMB): CFLAGS := $(COMPILE_FLAGS) -Wall -fPIC -O2 $(CHART_FLAGS) $(UPDATE_FLAGS) $(GSETTINGS_FLAGS) $(FLAVOUR_FLAGS)
$(OBJECTS_BERR): CFLAGS := $(COMPILE_FLAGS) -Wall -fPIC -O2 $(SQL3FLAGS) $(FLAVOUR_FLAGS)
$(OBJECTS_OPT): CFLAGS := $(COMPILE_FLAGS) -Wall -fPIC -O2 $(UPDATE_FLAGS) $(GSETTINGS_FLAGS) $(FLAVOUR_FLAGS) $(OPTION_FLAGS)
$(OBJECTS_WIZ): CFLAGS := $(COMPILE_FLAGS) -Wall -fPIC -O2 $(SQL3FLAGS)
$(OBJECTS_UPD): CFLAGS := $(COMPILE_FLAGS) -Wall -fPIC -O2 $(GSETTINGS_FLAGS) $(FLAVOUR_FLAGS)
ifeq ($(OMB_MONOLITHIC),1)
$(OBJECTS_APP): CFLAGS := $(COMPILE_FLAGS) -Wall -D__OPENMONEYBOX_EXE__ $(CHART_FLAGS) $(FLAVOUR_FLAGS) $(UPDATE_FLAGS) $(SQL3FLAGS) -O2 \
	 $(GSETTINGS_FLAGS) $(INDICATOR_FLAGS) $(LIBXML_FLAGS)
else
$(OBJECTS_APP): CFLAGS := $(COMPILE_FLAGS) -Wall -D__OPENMONEYBOX_EXE__ $(CHART_FLAGS) $(FLAVOUR_FLAGS) $(UPDATE_FLAGS) $(SQL3FLAGS) -O2 \
	 $(GSETTINGS_FLAGS)
endif
$(OBJECTS_TRAY): CFLAGS := $(COMPILE_FLAGS) -Wall -D__OMBTRAY_EXE__ $(INDICATOR_FLAGS) $(FLAVOUR_FLAGS) $(GSETTINGS_FLAGS) $(SQL3FLAGS) -O2
$(OBJECTS_CONV): CFLAGS := $(COMPILE_FLAGS) -Wall -D__LINUX__ $(CONVERT_FLAGS) $(FLAVOUR_FLAGS) -D__OMBCONVERT_BIN__ $(SQL3FLAGS) -O2
endif

$(OBJDIR)%.o: $(SRCDIR)%.cpp ${includes}
	$(CXX) $(CFLAGS) $(CXXFLAGS) -c $< -o $@

# Build number file. Increment if any object file changes.
$(BUILD_NUMBER_FILE): $(OBJECTS_LOGO) $(OBJECTS_OMB) $(OBJECTS_BERR) $(OBJECTS_OPT) $(OBJECTS_WIZ) $(OBJECTS_APP) $(OBJECTS_TRAY)
ifeq ($(INCREMENT),0)
	@if ! test -f $(BUILD_NUMBER_FILE); then echo 1 > $(BUILD_NUMBER_FILE); fi
else
	@if ! test -f $(BUILD_NUMBER_FILE); then echo 0 > $(BUILD_NUMBER_FILE); fi
endif
ifeq ($(INCREMENT),1)
	@echo $$(($$(cat $(BUILD_NUMBER_FILE)) + 1)) > $(BUILD_NUMBER_FILE)
endif

clean:
	$(RM) $(OBJECTS_LOGO) $(OBJECTS_OMB) $(OBJECTS_BERR) $(OBJECTS_OPT) $(OBJECTS_WIZ) $(OBJECTS_APP) $(OBJECTS_TRAY) $(OBJECTS_CONV)
	$(RM) $(OBJDIR)ombtray/omb35core.o
	$(RM) $(OBJDIR)ombtray/omb35opt.o
	$(RM) $(OBJDIR)ombtray/ui/password.o
ifeq ($(APPINDICATOR),1)
	$(RM) $(OBJDIR)ombtray/indicator.o
	$(RM) $(INDNAME)
endif
ifeq ($(OMBUPDATE),1)
	$(RM)  $(OBJECTS_OPT)
	$(RM) $(UPDATENAME)
endif
	$(RM) $(OBJDIR)ombtray/ui/wxpassword.o
	$(RM) $(BUILDDIR)$(LOGONAME) $(BUILDDIR)$(OMBNAME) $(BUILDDIR)$(BERRNAME) $(BUILDDIR)$(OPTNAME) $(BUILDDIR)$(WIZARDNAME) $(BUILDDIR)$(APPNAME) $(BUILDDIR)$(TRAYNAME) \
		$(CONVERTNAME) $(MAPVIEWERNAME)
	$(RM) $(BUILDDIR)$(APPNAME).desktop
	$(RM) $(OBJDIR)ombconvert/omb35core.o
	rm -rf $(OBJDIR)

create_translation_template_application:
	(cd src && xgettext --from-code=utf-8 -k_ -o ../i18n/en/en.pot -f ../lint/files.txt --copyright-holder="Igor Calì <igor.cali0@gmail.com>" --package-name=OpenMoneyBox \
		--package-version=3.4 --msgid-bugs-address=igor.cali0@gmail.com)

create_translation_messages_application:
# Italian
	(cd i18n && msgfmt en/it.po -o it/openmoneybox.mo)
# French
	(cd i18n && msgfmt en/fr.po -o fr/openmoneybox.mo)
# Swedish
	(cd i18n && msgfmt en/sv.po -o sv/openmoneybox.mo)
# Brazilian
#	(cd i18n && msgfmt en/pt_BR.po -o pt_BR/openmoneybox.mo)

create_translation_templates_manual: $(MANUAL_POTS)
	xml2pot help/omb_en.xml > help/omb_en.pot

help/en/%.pot: help/en/%.xml
	xml2pot $< > $@

create_translation_messages_manual: $(MANUAL_TRANS)
# After creating messages it is necessary to manually update:
#      - main file: paths for entries and lang id
# Italian
	po2xml help/omb_en.xml help/omb_it.po > help/omb_it.xml

#help/it/%.xml: help/en/%_it.pot
%:
# Italian
	mkdir -p help/it
	@if test -f help/en/$@.xml; then po2xml help/en/$@.xml help/en/$@_it.po > help/it/$@.xml; fi
	

create_manual:
	xsltproc -o help/$(APPNAME)_en.html help/linux.xsl help/omb_en.xml
	xsltproc -o help/$(APPNAME)_it.html help/linux.xsl help/omb_it.xml
	xsltproc -o help/$(APPNAME)-msw_en.html help/msw.xsl help/omb_en.xml
	xsltproc -o help/$(APPNAME)-msw_it.html help/msw.xsl help/omb_it.xml
	xsltproc -o help/$(APPNAME)-mac_en.html help/mac.xsl help/omb_en.xml
	xsltproc -o help/$(APPNAME)-mac_it.html help/mac.xsl help/omb_it.xml

install:
# Program folder creation
	mkdir -p $(BINDIR)
	mkdir -p $(LIBDIR)
	mkdir -p $(DATADIR)
# Binary install
#	install $(BUILDDIR)$(LOGONAME) $(LIBDIR)
ifeq ($(OMB_MONOLITHIC),0)
	install $(BUILDDIR)$(OMBNAME) $(LIBDIR)
	install $(BUILDDIR)$(BERRNAME) $(LIBDIR)
	install $(BUILDDIR)$(OPTNAME) $(LIBDIR)
	install $(BUILDDIR)$(WIZARDNAME) $(LIBDIR)
ifeq ($(OMBUPDATE),1)
	install $(BUILDDIR)$(UPDATENAME) $(LIBDIR)
endif
endif
	install $(BUILDDIR)$(APPNAME) $(BINDIR)
ifeq ($(CONVERT_BUILD),1)
	install $(BUILDDIR)$(CONVERTNAME) $(BINDIR)
endif
ifeq ($(APPINDICATOR),1)
	install $(BUILDDIR)$(INDNAME) $(BINDIR)
endif
ifeq ($(OMB_MONOLITHIC),0)
	install $(BUILDDIR)$(TRAYNAME) $(BINDIR)
endif
ifeq ($(OMB_MAPVIEWER),1)
	install $(BUILDDIR)$(MAPVIEWERNAME) $(BINDIR)
	install etc/openmoneybox/ombmapviewer.ui $(DATADIR)
endif

# Chart library install
ifeq ($(CHART_LIB),_OMB_CHART_MATLIBPLOT)
	install etc/openmoneybox/pie.py $(DATADIR)
	install etc/openmoneybox/trend.py $(DATADIR)
endif

# wxSQLite3 library install
ifeq ($(CODENAME),trusty)
	install lib/libwxcode_$(GTK_VERSION)_wxsqlite3-$(WXVER).so.0.0.0 $(LIBDIR)
	(cd $(LIBDIR) && ln -f -s libwxcode_$(GTK_VERSION)_wxsqlite3-$(WXVER).so.0.0.0 libwxcode_$(GTK_VERSION)_wxsqlite3-$(WXVER).so.0)
	(cd $(LIBDIR) && ln -f -s libwxcode_$(GTK_VERSION)_wxsqlite3-$(WXVER).so.0 libwxcode_$(GTK_VERSION)_wxsqlite3-$(WXVER).so)
endif

# Setting schemas install
# http://linux-commands-examples.com/glib-compile-schemas
ifeq ($(USE_GSETTINGS),1)
	mkdir -p $(SHAREDIR)glib-2.0/schemas
	cp etc/openmoneybox/org.igisw.openmoneybox.gschema.xml $(SHAREDIR)glib-2.0/schemas
ifeq ($(OMBUPDATE),1)
	cp etc/openmoneybox/org.igisw.openmoneybox-advanced.gschema.xml $(SHAREDIR)glib-2.0/schemas
endif
ifneq ($(DISTRO),Arch)
	glib-compile-schemas $(SHAREDIR)glib-2.0/schemas
endif
endif
# Dictionary install
# Italian
	mkdir -p $(SHAREDIR)locale/it/LC_MESSAGES
	cp i18n/it/openmoneybox.mo $(SHAREDIR)locale/it/LC_MESSAGES
# French
	mkdir -p $(SHAREDIR)locale/fr/LC_MESSAGES
	cp i18n/fr/openmoneybox.mo $(SHAREDIR)locale/fr/LC_MESSAGES
# Swedish
	mkdir -p $(SHAREDIR)locale/sv/LC_MESSAGES
	cp i18n/sv/openmoneybox.mo $(SHAREDIR)locale/sv/LC_MESSAGES
# Brazilian
#	mkdir -p $(SHAREDIR)locale/pt_BR/LC_MESSAGES
#	cp i18n/pt_BR/openmoneybox.mo $(SHAREDIR)locale/pt_BR/LC_MESSAGES
# Icon install
	mkdir -p $(SHAREDIR)icons/hicolor/16x16/apps
	mkdir -p $(SHAREDIR)icons/hicolor/48x48/apps
	mkdir -p $(SHAREDIR)icons/hicolor/scalable/apps
	install rsrc/icons/16/openmoneybox.png $(SHAREDIR)icons/hicolor/16x16/apps
	install rsrc/icons/48/openmoneybox.png $(SHAREDIR)icons/hicolor/48x48/apps
	cp rsrc/icons/wallet-open.svg $(SHAREDIR)icons/hicolor/scalable/apps/$(APPNAME).svg
	install rsrc/icons/logo.png $(DATADIR)
	mkdir -p $(SHAREDIR)icons/hicolor/24x24/actions
	mkdir -p $(SHAREDIR)icons/hicolor/48x48/actions
	install rsrc/icons/24/mark-location.png $(SHAREDIR)icons/hicolor/24x24/actions
	install rsrc/icons/48/mark-location.png $(SHAREDIR)icons/hicolor/48x48/actions
ifeq ($(APPINDICATOR),1)
	install rsrc/icons/48/ombtray_attention.png $(SHAREDIR)icons/hicolor/48x48/apps
	install rsrc/icons/48/logo_pale.png $(SHAREDIR)icons/hicolor/48x48/apps
	install rsrc/icons/48/logo_disabled.png $(SHAREDIR)icons/hicolor/48x48/apps
endif
# Category icons
	mkdir -p $(DATADIR)categories/
	install rsrc/icons/categories/book-2-line.png     $(DATADIR)categories/
	install rsrc/icons/categories/briefcase-line.png  $(DATADIR)categories/
	install rsrc/icons/categories/car-line.png        $(DATADIR)categories/
	install rsrc/icons/categories/community-line.png  $(DATADIR)categories/
	install rsrc/icons/categories/empty.png           $(DATADIR)categories/
	install rsrc/icons/categories/goblet-line.png     $(DATADIR)categories/
	install rsrc/icons/categories/home-8-line.png     $(DATADIR)categories/
	install rsrc/icons/categories/hospital-line.png   $(DATADIR)categories/
	install rsrc/icons/categories/phone-line.png      $(DATADIR)categories/
	install rsrc/icons/categories/restaurant-line.png $(DATADIR)categories/
	install rsrc/icons/categories/t-shirt-line.png    $(DATADIR)categories/
	install rsrc/icons/categories/add-box-line.png    $(DATADIR)categories/

# User icon
	mkdir -p $(DATADIR)images
	install rsrc/icons/account-box-line.png     $(DATADIR)images/

# MimeType creation
	mkdir -p $(SHAREDIR)mime/packages
	cp share/openmoneybox.xml $(SHAREDIR)mime/packages
	cp rsrc/icons/application-openmoneybox.svg $(SHAREDIR)icons/hicolor/scalable/apps
	chmod o+r $(SHAREDIR)icons/hicolor/scalable/apps/application-openmoneybox.svg
ifeq ($(UPDATE_YARU_THEME),1)
	mkdir -p $(SHAREDIR)icons/hicolor/scalable/mimetypes
	@if ! test -f $(BUILD_NUMBER_FILE); then \
		ln -s $(SHAREDIR)icons/hicolor/scalable/apps/application-openmoneybox.svg $(SHAREDIR)icons/hicolor/scalable/mimetypes/application-openmoneybox.svg \
		; fi
endif
# Shortcut creation
	@echo [Desktop Entry] > $(BUILDDIR)$(APPNAME).desktop
	@echo Version=1.0 >> $(BUILDDIR)$(APPNAME).desktop
	@echo Type=Application >> $(BUILDDIR)$(APPNAME).desktop
	@echo Terminal=false >> $(BUILDDIR)$(APPNAME).desktop
	@echo Categories=Utility >> $(BUILDDIR)$(APPNAME).desktop
	@echo Name=OpenMoneyBox >> $(BUILDDIR)$(APPNAME).desktop
# Italian translation in shortcut
	@echo Name[it]=Portamonete >> $(BUILDDIR)$(APPNAME).desktop
	@echo Comment=Budget management >> $(BUILDDIR)$(APPNAME).desktop
	@echo Comment[it]=Gestione del bilancio >> $(BUILDDIR)$(APPNAME).desktop
ifeq ($(APPIMAGE),1)
	@echo Exec=$(APPNAME) %U >> $(BUILDDIR)$(APPNAME).desktop
else
	@echo Exec=$(DESTPATH)/$(APPNAME) %U >> $(BUILDDIR)$(APPNAME).desktop
endif
	@echo Path=$(DESTPATH)/ >> $(BUILDDIR)$(APPNAME).desktop
	@echo Icon=$(APPNAME) >> $(BUILDDIR)$(APPNAME).desktop
	@echo MimeType=application/openmoneybox >> $(BUILDDIR)$(APPNAME).desktop
	mkdir -p $(SHAREDIR)applications
	desktop-file-install --dir=$(SHAREDIR)applications $(BUILDDIR)$(APPNAME).desktop
	$(RM) $(BUILDDIR)$(APPNAME).desktop
# OS file explorer update
ifeq ($(UPDATE_YARU_THEME),1)
	@if test -f /usr/share/icons/Yaru; then sudo gtk-update-icon-cache /usr/share/icons/Yaru; fi
endif
ifeq ($(DEBIAN),0)
	sudo update-mime-database /usr/share/mime
endif
# Manual creation
ifeq ($(YELP),0)
	mkdir -p $(DATADIR)images
	install help/images/* $(DATADIR)images
endif
# English
ifeq ($(YELP),0)
	cp help/$(APPNAME)_en.html $(DATADIR)
	mkdir -p $(DATADIR)en/images
	install help/en/images_gtk/* $(DATADIR)en/images
else
	mkdir -p $(YELPDIR)en_GB/openmoneybox
	cp help/omb_en.xml $(YELPDIR)en_GB/openmoneybox
	mkdir -p $(YELPDIR)en_GB/openmoneybox/images
	install help/images/* $(YELPDIR)en_GB/openmoneybox/images
	mkdir -p $(YELPDIR)en_GB/openmoneybox/en/images
	cp help/en/*.xml $(YELPDIR)en_GB/openmoneybox/en
	install help/en/images_gtk/* $(YELPDIR)en_GB/openmoneybox/en/images
endif
# Italian
ifeq ($(YELP),0)
	cp help/$(APPNAME)_it.html $(DATADIR)
	mkdir -p $(DATADIR)it/images
	install help/it/images_gtk/* $(DATADIR)it/images
else
	mkdir -p $(YELPDIR)it/openmoneybox
	cp help/omb_it.xml $(YELPDIR)it/openmoneybox
	mkdir -p $(YELPDIR)it/openmoneybox/images
	install help/images/* $(YELPDIR)it/openmoneybox/images
	mkdir -p $(YELPDIR)it/openmoneybox/it/images
	cp help/it/*.xml $(YELPDIR)it/openmoneybox/it
	install help/it/images_gtk/* $(YELPDIR)it/openmoneybox/it/images
endif

# XSL templates install	
	mkdir -p $(DATADIR)en
	install etc/openmoneybox/en/ombexport.xsl $(DATADIR)en/ombexport.xsl
	mkdir -p $(DATADIR)it
	install etc/openmoneybox/it/ombexport.xsl $(DATADIR)it/ombexport.xsl
# Alarm file install
	cp rsrc/snd/alarm_clock.wav $(DATADIR)

# License install
# English
	mkdir -p $(SHAREDIR)doc/openmoneybox/licenses/en
	install licenses/en/license.txt $(SHAREDIR)doc/openmoneybox/licenses/en
# Italian
	mkdir -p $(SHAREDIR)doc/openmoneybox/licenses/it
	install licenses/it/licenza.txt $(SHAREDIR)doc/openmoneybox/licenses/it

# Convert autostart tray launcher
ifeq ($(OMB_MONOLITHIC),1)
	scripts/update_autostart.sh
endif

	@echo
	@echo $(APPNAME) v. $(VERSION).$$(($$(cat $(BUILD_NUMBER_FILE)))) installed.

# Install Debian Lintian overrides
ifeq ($(DISTRO),Ubuntu)
	mkdir -p $(SHAREDIR)lintian/overrides
ifeq ($(CODENAME),noble)
	cp share/lintian/openmoneybox_bookworm $(SHAREDIR)lintian/overrides/openmoneybox
	cp share/lintian/openmoneybox-mapviewer_bookworm $(SHAREDIR)lintian/overrides/openmoneybox-mapviewer
else
	cp share/lintian/openmoneybox $(SHAREDIR)lintian/overrides
	cp share/lintian/openmoneybox-mapviewer $(SHAREDIR)lintian/overrides
endif
endif
ifeq ($(DISTRO),Debian)
	mkdir -p $(SHAREDIR)lintian/overrides
ifeq ($(CODENAME),bookworm)
	cp share/lintian/openmoneybox_bookworm $(SHAREDIR)lintian/overrides/openmoneybox
	cp share/lintian/openmoneybox-mapviewer_bookworm $(SHAREDIR)lintian/overrides/openmoneybox-mapviewer
else
	cp share/lintian/openmoneybox_debian $(SHAREDIR)lintian/overrides/openmoneybox
	cp share/lintian/openmoneybox-mapviewer $(SHAREDIR)lintian/overrides
endif
endif
ifeq ($(DISTRO),Raspbian)
	mkdir -p $(SHAREDIR)lintian/overrides
	cp share/lintian/openmoneybox_raspbian $(SHAREDIR)lintian/overrides/openmoneybox
	cp share/lintian/openmoneybox-mapviewer_bookworm $(SHAREDIR)lintian/overrides/openmoneybox-mapviewer
endif

uninstall:
# Binary uninstall
ifeq ($(OMB_MONOLITHIC),0)
#	$(RM) $(LIBDIR)/$(LOGONAME)
	$(RM) $(LIBDIR)/$(OMBNAME)
	$(RM) $(LIBDIR)/$(BERRNAME)
	$(RM) $(LIBDIR)/$(OPTNAME)
	$(RM) $(LIBDIR)/$(WIZARDNAME)
ifeq ($(OMBUPDATE),1)
	$(RM) $(LIBDIR)/$(UPDATENAME)
endif
endif
	$(RM) $(BINDIR)/$(APPNAME)
ifeq ($(CONVERT_BUILD),1)
	$(RM) $(BINDIR)/$(CONVERTNAME)
endif
ifeq ($(APPINDICATOR),1)
	$(RM) $(BINDIR)/$(INDNAME)
endif
ifeq ($(OMB_MONOLITHIC),0)
	$(RM) $(BINDIR)/$(TRAYNAME)
endif
ifeq ($(OMB_MAPVIEWER),1)
	$(RM) $(BINDIR)/$(MAPVIEWERNAME)
	$(RM) $(DATADIR)ombmapviewer.ui
endif

# Chart library uninstall
ifeq ($(CHART_LIB),_OMB_CHART_MATLIBPLOT)
	$(RM) $(DATADIR)pie.py
	$(RM) $(DATADIR)trend.py
endif

# wxSQLite3 library uninstall
ifeq ($(CODENAME),trusty)
	$(RM) /lib/libwxcode_$(GTK_VERSION)_wxsqlite3-$(WXVER).so
	$(RM) /lib/libwxcode_$(GTK_VERSION)_wxsqlite3-$(WXVER).so.0
	$(RM) /lib/libwxcode_$(GTK_VERSION)_wxsqlite3-$(WXVER).so.0.0.0
endif

# Setting schemas uninstall
ifeq ($(USE_GSETTINGS),1)
	$(RM) $(SHAREDIR)glib-2.0/schemas/org.igisw.openmoneybox.gschema.xml
ifeq ($(OMBUPDATE),1)
	$(RM) $(SHAREDIR)glib-2.0/schemas/org.igisw.openmoneybox-advanced.gschema.xml
endif
	glib-compile-schemas $(SHAREDIR)glib-2.0/schemas
endif
# Dictionary uninstall
	$(RM) $(SHAREDIR)locale/it/LC_MESSAGES/openmoneybox.mo
	$(RM) $(SHAREDIR)locale/fr/LC_MESSAGES/openmoneybox.mo
	$(RM) $(SHAREDIR)locale/sv/LC_MESSAGES/openmoneybox.mo
# Icon uninstall
	$(RM) $(SHAREDIR)icons/hicolor/16x16/apps/openmoneybox.png
	$(RM) $(SHAREDIR)icons/hicolor/48x48/apps/openmoneybox.png
	$(RM) $(SHAREDIR)icons/hicolor/scalable/apps/$(APPNAME).svg
	$(RM) $(DATADIR)logo.png
	$(RM) $(SHAREDIR)icons/hicolor/24x24/actions/mark-location.png
	$(RM) $(SHAREDIR)icons/hicolor/48x48/actions/mark-location.png
ifeq ($(APPINDICATOR),1)
	$(RM) $(SHAREDIR)icons/hicolor/48x48/apps/ombtray_attention.png
	$(RM) $(SHAREDIR)icons/hicolor/48x48/apps/logo_pale.png
	$(RM) $(SHAREDIR)icons/hicolor/48x48/apps/logo_disabled.png
endif
# Category icons
	$(RM) $(DATADIR)categories/book-2-line.png
	$(RM) $(DATADIR)categories/briefcase-line.png
	$(RM) $(DATADIR)categories/car-line.png
	$(RM) $(DATADIR)categories/community-line.png
	$(RM) $(DATADIR)categories/empty.png
	$(RM) $(DATADIR)categories/goblet-line.png
	$(RM) $(DATADIR)categories/home-8-line.png
	$(RM) $(DATADIR)categories/hospital-line.png
	$(RM) $(DATADIR)categories/phone-line.png
	$(RM) $(DATADIR)categories/restaurant-line.png
	$(RM) $(DATADIR)categories/t-shirt-line.png
	$(RM) $(DATADIR)categories/add-box-line.png

# User icon
	$(RM) $(DATADIR)images/account-box-line.png

# MimeType uninstall
	$(RM) $(SHAREDIR)mime/application/openmoneybox.xml
	$(RM) $(SHAREDIR)icons/hicolor/scalable/apps/application-openmoneybox.svg
# Shortcut uninstall
	$(RM) $(SHAREDIR)applications/$(APPNAME).desktop
# OS file explorer update
ifeq ($(DEBIAN),0)
	sudo update-mime-database /usr/share/mime
endif
ifeq ($(UPDATE_YARU_THEME),1)
	sudo gtk-update-icon-cache /usr/share/icons/Yaru
endif
# Manual uninstall
ifeq ($(YELP),0)
	$(RM) $(DATADIR)$(APPNAME)_it.html
	$(RM) $(DATADIR)$(APPNAME)_en.html
	$(RM) $(DATADIR)images/*
	rm -d $(DATADIR)images
	$(RM) $(DATADIR)en/images/*
	rm -d $(DATADIR)en/images
	$(RM) $(DATADIR)it/images/*
	rm -d $(DATADIR)it/images
else
	$(RM) $(YELPDIR)en_GB/openmoneybox/omb_en.xml
	$(RM) $(YELPDIR)it/openmoneybox/omb_it.xml
	$(RM) $(YELPDIR)en_GB/openmoneybox/images/*
	rm -d $(YELPDIR)en_GB/openmoneybox/images
	$(RM) $(YELPDIR)it/openmoneybox/images/*
	rm -d $(YELPDIR)it/openmoneybox/images
	$(RM) $(YELPDIR)en_GB/openmoneybox/en/images/*
	rm -d $(YELPDIR)en_GB/openmoneybox/en/images
	$(RM) $(YELPDIR)it/openmoneybox/it/images/*
	rm -d $(YELPDIR)it/openmoneybox/it/images
	$(RM) $(YELPDIR)en_GB/openmoneybox/en/*
	rm -d $(YELPDIR)en_GB/openmoneybox/en
	$(RM) $(YELPDIR)it/openmoneybox/it/*
	rm -d $(YELPDIR)it/openmoneybox/it
	rm -d $(YELPDIR)en_GB/openmoneybox
	rm -d $(YELPDIR)it/openmoneybox
endif
# XSL templates uninstall
	$(RM) $(DATADIR)en/ombexport.xsl
	rmdir $(DATADIR)en
	$(RM) $(DATADIR)it/ombexport.xsl
	rmdir $(DATADIR)it
# Alarm file uninstall
	$(RM) $(DATADIR)alarm_clock.wav
# Program folders removal
#	rmdir $(BINDIR)
	rmdir $(DATADIR)
#License uninstall
	$(RM) $(SHAREDIR)doc/openmoneybox/licenses/en/license.txt
	$(RM) -d $(SHAREDIR)doc/openmoneybox/licenses/en
	$(RM) $(SHAREDIR)doc/openmoneybox/licenses/it/licenza.txt
	$(RM) -d $(SHAREDIR)doc/openmoneybox/licenses/it
	$(RM) -d $(SHAREDIR)doc/openmoneybox/license

	@echo
	@echo $(APPNAME) v. $(VERSION).$$(($$(cat $(BUILD_NUMBER_FILE)))) uninstalled.
# Uninstall Debian Lintian overrides
	$(RM) $(SHAREDIR)lintian/overrides/openmoneybox
	$(RM) $(SHAREDIR)lintian/overrides/openmoneybox-mapviewer
	$(RM) -d $(SHAREDIR)lintian/overrides

tar: clean
	mkdir -p ~/tmp/openmoneybox
	cp -r scripts/ ~/tmp/openmoneybox
	cp -r etc/ ~/tmp/openmoneybox
	cp -r fastlane/ ~/tmp/openmoneybox
	cp -r help/ ~/tmp/openmoneybox
	cp -r i18n/ ~/tmp/openmoneybox
	cp -r lint/ ~/tmp/openmoneybox
	cp -r rsrc/ ~/tmp/openmoneybox
	cp -r share/ ~/tmp/openmoneybox
	cp -r src/ ~/tmp/openmoneybox
	cp -r doc/ ~/tmp/openmoneybox
#	mkdir -p ~/tmp/openmoneybox/lib
# C::B project files
	mkdir -p ~/tmp/openmoneybox/cb
	cp cb/*.cbp ~/tmp/openmoneybox/cb
	cp cb/openmoneybox.workspace ~/tmp/openmoneybox/cb
	cp -r installer_win/ ~/tmp/openmoneybox
	cp -r installer_mac/ ~/tmp/openmoneybox
	cp build-number.txt ~/tmp/openmoneybox
	cp Makefile ~/tmp/openmoneybox
	cp makefile.win ~/tmp/openmoneybox
	cp makefile.mac ~/tmp/openmoneybox
	cp makefile.bsd ~/tmp/openmoneybox
	cp makefile.solaris ~/tmp/openmoneybox
	cp readme.txt ~/tmp/openmoneybox
	cp README.md ~/tmp/openmoneybox
	cp LICENSE ~/tmp/openmoneybox
# License files
	cp -r licenses/ ~/tmp/openmoneybox
# Android files
	mkdir -p ~/tmp/openmoneybox/$(ANDROID_FOLDER)
	cp -r $(ANDROID_FOLDER)$(ANDROID_API_LEVEL)/ ~/tmp/openmoneybox/$(ANDROID_FOLDER)
	# Remove Android gradle folder
	@if test -f $(~/tmp/openmoneybox/$(ANDROID_FOLDER)$(ANDROID_API_LEVEL)/.gradle); then rm -r ~/tmp/openmoneybox/$(ANDROID_FOLDER)$(ANDROID_API_LEVEL)/.gradle; fi
	# Remove Android build folder
	@if test -f $(~/tmp/openmoneybox/$(ANDROID_FOLDER)$(ANDROID_API_LEVEL)/build); then rm -r ~/tmp/openmoneybox/$(ANDROID_FOLDER)$(ANDROID_API_LEVEL)/build; fi
	cp -r $(ANDROID_FOLDER)help  ~/tmp/openmoneybox/$(ANDROID_FOLDER)
	cp makefile.android ~/tmp/openmoneybox
	mv ~/tmp/openmoneybox ~/tmp/openmoneybox-$(VERSION)
	(cd ~/tmp/ && tar czvf openmoneybox.tar.gz openmoneybox-$(VERSION))
	mv ~/tmp/openmoneybox.tar.gz ../$(APPNAME)_$(VERSION).$$(cat $(BUILD_NUMBER_FILE)).tar.gz
	rm -r ~/tmp/openmoneybox-$(VERSION)
	@echo
	@echo Created source tarball for $(APPNAME) v. $(VERSION).$$(($$(cat $(BUILD_NUMBER_FILE)))) [Linux, Android, Windows, FreeBSD, MacOs].

create_release_checklist_template:
	@echo "<!-- template generated on $$(date +'%Y/%m/%d %H:%M') -->" | cat - doc/dev/release_checklist.md > omb_release_checklist_v$(VERSION).$$(($$(cat $(BUILD_NUMBER_FILE)))).md
	@echo
	@echo Checklist template for $(APPNAME) v. $(VERSION).$$(($$(cat $(BUILD_NUMBER_FILE)))) created:
	@echo "    --> $$(pwd)/omb_release_checklist_v$(VERSION).$$(($$(cat $(BUILD_NUMBER_FILE)))).md"
	@echo

