<!-- last update: 15/02/2025
Unchecked: &#9744;
Checked: &#9745;
-->

Tasks to do before releasing a new version of OpenMoneyBox.
===========================================================

## All:  
 1. &#9744; Create release checklist (`make create_release_checklist_template`)  
 2. &#9744; Update `History.*` in `/help` and `android/help`; update help files  
 3. &#9744; Increase/check `buildnumber.txt`  
 4. &#9744; Sync `packaging`, `trunk` and `stable` branches in [Launchpad](https://launchpad.net/bilancio)  
 5. &#9744; Update and upload tarball in [Launchpad](https://launchpad.net/bilancio)  
 6. &#9744; Update `version.txt` on [SourceForge](https://sourceforge.net/projects/igisw-bilancio/)  
    1. &#9744; check update type  
    2. &#9744; check applicable OS’s  
 7. &#9744; Close open bugs in [Launchpad](https://launchpad.net/bilancio) and [GitLab](https://gitlab.com/igi0/openmoneybox)  
 8. &#9744; Review Flawfinder results:  
    1. &#9744; `flawfinder --falsepositive --savehitlist=flawfinder.log src`  
    2. &#9744; `flawfinder --savehitlist=flawfinder.log src`  
    3. &#9744; Update `TechDetails.ods`  
 9. &#9744; For new main releases: deactivate old milestones in [Launchpad](https://launchpad.net/bilancio)
 10. &#9744; Review and update document `TechDetails.ods`

## Linux:  
 1. &#9744; Review Cppcheck results  
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <u>AppImage</u>:  
  * &#9744; Download new version of [**LinuxDeploy**](https://github.com/linuxdeploy/linuxdeploy/releases) (current version: `Release 1-alpha-20250213-2`)  

## MSW:  
 1. &#9744; Check for [**Mingw64**](https://winlibs.com/#download-release) updates (current version: `14.2.0 arch:X86_64 thread:posix exception:seh build:0`)  
 2. &#9744; Check for [**MSys2**](https://www.msys2.org/) updates (current version: `20240727`)  
 3. &#9744; Check for **sqlite3.dll** updates (current version: `sqlite-dll-win64-x64-3470000.zip`)  
 4. &#9744; Check for **wget** updates (current version: `1.21.4`)  
 5. &#9744; Update `\src\openmoneybox\openmoneybox.rc`  
 6. &#9744; Update `\installer_win\include\version.nsi`  
    1. &#9744; Change registry key on major version change
       
## OpenSolaris:  
 1. &#9744; Update __BUILD_NUMBER and BuildVersion in `productversion.cpp`  

## Android:  
 1. &#9744; Increase `versionCode` in manifest file  
 2. &#9744; Increase `versionName` in manifest file  
 3. &#9744; Check library updates  
 4. &#9744; Update online manual  
 5. &#9744; Update FastLane changelog and string `updates` for all languages  
 6. &#9744; Update online Android changelog  
 7. &#9744; Sync [GitLab](https://gitlab.com/igi0/openmoneybox) `master` branch  
 8. &#9744; Review insider results: `~/Apps/insider_3.0.0_linux_x86_64/insider -tech android -target android/API33/main/java/ -v -no-html -no-json > insider.log`  
    1. &#9744; Update `TechDetails.ods`  
 9. &#9744; Update [Exodus](https://reports.exodus-privacy.eu.org/it/reports/search/com.igisw.openmoneybox/) permission  
    1. &#9744; Update `TechDetails.ods`  

## MacOs:  
 1. &#9744; Update BuildVersion in `productversion.cpp`  
 2. &#9744; Update versions in files in `installer_mac` folder  
 3. &#9744; Check for **Brew** updates (`brew update && brew upgrade`)  

