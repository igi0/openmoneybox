<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://igisw-bilancio.sourceforge.net">
<xsl:template match="/">
  <html>
	  <head>
		  <style type="text/css">
		  body 
		  {
			font-family:ubuntu,sans-serif;
			color:#000;
			font-size:13px;
			color:#333;
		  }
		  table 
		  {
			font-size:1em;
			margin:0 0 1em;
			border-collapse:collapse;
			border-width:0;
			empty-cells:show;
		  }
		  td,th 
		  {
			border:1px solid #ccc;
			padding:6px 12px;
			vertical-align:top;
			background-color:inherit;
		  }
		td
		{
			text-align:right;
		}
		  th 
		  {
			background-color:#dee8f1;
			text-align:center;
		  }
		  </style>
	  </head>
	  <body>

<h2>
<table border="0">
<tr>
<td width="90%">
<center>
<xsl:for-each select="groups/headers/title"><xsl:value-of select="@heading"/><xsl:value-of select="@month"/></xsl:for-each>
</center></td>
<td width="10%">
<img src="logo.png"></img>
</td>
</tr>
</table>
</h2>

<table>
	<tr>
		  <th>Date</th>
		  <th>Hour</th>
		  <th>Operation</th>
		  <th>Value (&#8364;)</th>
		  <th>Reason</th>
		  <th>Category</th>
		</tr>

		<xsl:for-each select="groups/days/day">
		<tr>
		<td><b><xsl:value-of select="@date"/></b></td>
		<td></td>
		<td><b>TOTAL</b></td>
		<td><b><xsl:value-of select="@total"/></b></td>
		<td></td>
		<td></td>
		</tr>

			<xsl:for-each select="item">
			<tr>
			<td></td>
			<td><xsl:value-of select="@time"/></td>
			<td>
				<xsl:if test="@type = 1">
				Profit
				</xsl:if>
				<xsl:if test="@type = 2">
				Expense
				</xsl:if>
				<xsl:if test="@type = 3">
				Credit set
				</xsl:if>
				<xsl:if test="@type = 4">
				Credit removed
				</xsl:if>
				<xsl:if test="@type = 5">
				Credit remitted
				</xsl:if>
				<xsl:if test="@type = 6">
				Debt set
				</xsl:if>
				<xsl:if test="@type = 7">
				Debt removed
				</xsl:if>
				<xsl:if test="@type = 8">
				Debt remitted
				</xsl:if>
				<xsl:if test="@type = 9">
				Object received
				</xsl:if>
				<xsl:if test="@type = 10">
				Object given
				</xsl:if>
				<xsl:if test="@type = 11">
				Object lent
				</xsl:if>
				<xsl:if test="@type = 12">
				Object got back
				</xsl:if>
				<xsl:if test="@type = 13">
				Object borrowed
				</xsl:if>
				<xsl:if test="@type = 14">
				Object given back
				</xsl:if>
			</td>
			<td><xsl:value-of select="@value"/></td>
			<td><xsl:value-of select="@reason"/></td>
			<td><xsl:value-of select="@category"/></td>
			</tr>
			</xsl:for-each>
		</xsl:for-each>
</table>

<img src="chart1.png"></img>
<img src="chart2.png"></img>
	  </body>
  </html>
</xsl:template>
</xsl:stylesheet>
