"""
/***************************************************************
 * Name:      pie.py
 * Purpose:   OpenMoneyBox - Generate fund chart image
 * Author:    Igor Cali' (igor.cali0@gmail.com)
 * Created:   2017-02-25
 * Copyright: Igor Cali' (igor.cali0@gmail.com)
 * License:		GNU
 **************************************************************/

Derived from sample at http://matplotlib.org/examples/pie_and_polar_charts/pie_demo_features.html
"""

import matplotlib.pyplot as plt
import numpy as np
import gettext
import sys

def print_syntax():
	print ('Syntax: python pie.py locale_dir input_file output_file\n')
	print ('    locale_dir: path to system locale folder')
	print ('    input_file: csv input file with chart data')
	sys.exit('    output_file: output chart image')

args = len(sys.argv)

if args < 4:
	print_syntax();

locale_dir = sys.argv[1:][0]
input_file = sys.argv[2:][0]
output_file = sys.argv[3:][0]

if not locale_dir:
	print_syntax();

if not input_file:
	print_syntax();

if not output_file:
	print_syntax();

# default for locale_dir should be 'openmoneybox', '/usr/share/locale'
gettext.bindtextdomain(locale_dir)
gettext.textdomain('openmoneybox')
_ = gettext.gettext

# The slices will be ordered and plotted counter-clockwise.
# Colors are in HTML names
colors = ['yellowgreen', 'gold', 'lightskyblue', 'lightcoral', 'LightSlateGrey', 'Plum']
#explode = (0, 0.1, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

col_names = ["name", "value", "explode"]
dtypes = ["S50", "float", "float"]
data = np.genfromtxt(input_file, delimiter=',', names=col_names, dtype=dtypes)

# Size in inches
plt.figure(figsize=(3, 3), dpi=100)

#plt.rc("font", size=14)

plt.pie(data['value'], explode=data['explode'], labels=data['name'], colors=colors, autopct='%1.1f%%', shadow=True, startangle=0)
# Set aspect ratio to be equal so that pie is drawn as a circle.
plt.axis('equal')

hfont = {'fontname':'Ubuntu'}
plt.title(_('Fund Chart'), **hfont)
	
#plt.show()	
plt.savefig(output_file, bbox_inches='tight')

